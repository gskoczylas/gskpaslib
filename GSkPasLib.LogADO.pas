﻿{: @abstract Rejestrowanie komponentów ADO.

   Moduł @name zawiera definicję klasy pomocniczej dla klasy @link(TGSkLog).
   Po umieszczeniu go w sekcji @code(uses) razem z modułem @link(GSkPasLib.Log) możliwe
   jest wygodne monitorowanie komponentów ADO. }
unit  GSkPasLib.LogADO;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Classes, ADODB,
  GSkPasLib.Log;


type
  {: @abstract Rozszerza klasę @link(TGSkLog) o monitorowanie komponentów SQL. }
  TGSkLogHlpADO = class helper for TGSkLog
  public

    {: @abstract Monitoruje wybrane cechy komponentu TADOQuery. }
    function  LogSQL(const   qryValue:  TADOQuery;
                     const   lmMsgType:  TGSkLogMessageType = lmInformation)
                     : TGSkLog;                                      overload;

    {: @abstract Monitoruje wybrane cechy komponentu TADOCommand. }
    function  LogSQL(const   cmdValue:  TADOCommand;
                     const   lmMsgType:  TGSkLogMessageType = lmInformation)
                     : TGSkLog;                                      overload;

  end { TGSkLogHlpADO };



implementation


{$REGION 'TGSkLogHlpADO'}

function  TGSkLogHlpADO.LogSQL(const   qryValue:  TADOQuery;
                               const   lmMsgType:  TGSkLogMessageType)
                               : TGSkLog;
var
  nInd:  Integer;

begin  { LogSQL }
  Result := Self;
  {-}
  BeginLog();
  with  qryValue  do
    try
      LogValue(Name + '.SQL', SQL, lmMsgType);
      BeginLevel();
      try
        with  Parameters  do
          for  nInd := 0  to  Count - 1  do
            with  Items[nInd]  do
              LogValue(qryValue.Name + '[' + Name + ']', Value, lmMsgType);
        if  ConnectionString <> ''  then
          LogValue('ConnectionString', ConnectionString, lmMsgType);
        if  CacheSize <> 1  then
          LogValue('CacheSize', CacheSize, lmMsgType);
        if  CommandTimeout <> 30  then
          LogValue('CommandTimeout', CommandTimeout, lmMsgType);
        if  ConnectionString <> ''  then
          LogValue('ConnectionString', ConnectionString, lmMsgType)
      finally
        EndLevel()
      end { try-finally }
    finally
      EndLog()
    end { try-finally }
end { LogSQL };


function  TGSkLogHlpADO.LogSQL(const   cmdValue:  TADOCommand;
                               const   lmMsgType:  TGSkLogMessageType)
                               : TGSkLog;
var
  nInd:  Integer;
  lBuf:  TStringList;

begin  { LogSQL }
  Result := Self;
  {-}
  BeginLog();
  with  cmdValue  do
    try
      lBuf := TStringList.Create();
      try
        lBuf.Text := CommandText;
        LogValue(Name + '.SQL', lBuf, lmMsgType);
      finally
        lBuf.Free()
      end { try-finally };
      BeginLevel();
      try
        with  Parameters  do
          for  nInd := 0  to  Count - 1  do
            with  Items[nInd]  do
              LogValue(cmdValue.Name + '[' + Name + ']', Value, lmMsgType);
        if  ConnectionString <> ''  then
          LogValue('ConnectionString', ConnectionString, lmMsgType);
        if  CommandTimeout <> 30  then
          LogValue('CommandTimeout', CommandTimeout, lmMsgType);
        if  ConnectionString <> ''  then
          LogValue('ConnectionString', ConnectionString, lmMsgType)
      finally
        EndLevel()
      end { try-finally }
    finally
      EndLog()
    end { try-finally }
end { LogSQL };

{$ENDREGION 'TGSkLogHlpADO'}


end.
