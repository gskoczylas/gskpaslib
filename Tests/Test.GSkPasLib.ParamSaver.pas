﻿unit  Test.GSkPasLib.ParamSaver;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}


uses
  GSkPasLib.ParamSaver, TestFramework;


type
  TParamSaver_Test = class(TTestCase)
  published
    procedure  TestCreateDefault();
    procedure  TestCreateReg();
    procedure  TestCreateXML();
    procedure  TestParamReadWrite();
    procedure  TestSerieReg();
    procedure  TestSerieXML();
  end { TParamSaver_Test };


implementation


uses
  SysUtils, Math, Forms, Graphics, GSkPasLib.ComponentUtils, JclMath,
  GSkPasLib.Dialogs;


{ TParamSaver_Test }

procedure  TParamSaver_Test.TestCreateDefault();

var
  lPar:  TGSkCustomParamSaver;

begin  { TestCreateDefault }
  lPar := CreateStdParamSaver();
  lPar.Free()
end { TestCreateDefault };


procedure  TParamSaver_Test.TestCreateReg();

var
  lPar:  TGSkCustomParamSaver;

begin  { TestCreateReg }
  lPar := TGSkParamSaverRegistry.Create(nil);
  lPar.Free()
end { TestCreateReg };


procedure  TParamSaver_Test.TestCreateXML();

var
  lPar:  TGSkCustomParamSaver;

begin  { TestCreateXML }
  lPar := TGSkParamSaverXML.Create(nil);
  lPar.Free()
end { TestCreateXML };


procedure  TParamSaver_Test.TestParamReadWrite();

const
  csTestData:  PChar = 'To są przykładowe informacje.';

var
  tmCurrTime:  TDateTime;

  procedure  SaveParams(const   SaverClass:  TGSkParamSaverClass);

  var
    lPar:  TGSkCustomParamSaver;
    szTestData:  packed array[0..255] of Char;

  begin  { SaveParams }
    FillChar(szTestData, SizeOf(szTestData), #0);
    StrCopy(szTestData, csTestData);
    lPar := SaverClass.Create(nil);
    try
      if  lPar.OpenKey('korzeń\węzeł')  then
        try
          lPar.WriteBinaryData('binarne', szTestData, StrLen(szTestData));
          lPar.WriteBool('logiczne', True);
          lPar.WriteCurrency('kwota', 1.2345);
          lPar.WriteDate('data', tmCurrTime);
          lPar.WriteTime('czas', tmCurrTime);
          lPar.WriteDateTime('data i czas', tmCurrTime);
          lPar.WriteFloat('rzeczywista', Pi);
          lPar.WriteInteger('całkowita', MaxInt);
          lPar.WriteString('napis', szTestData);
          lPar.WriteChar('znak', 'x');
          lPar.WriteColor('kolor', clBlue);
          lPar.WriteFont('czcionka', Screen.Forms[0].Font);
          lPar.WriteHex('szesnastkowo', $abcdef)
        finally
          lPar.CloseKey()
        end { try-finally; if OpenKey(...) }
    finally
      lPar.Free()
    end { try-finally }
  end { SaveParams };

  procedure  RestParams(const   SaverClass:  TGSkParamSaverClass);

  var
    lPar:  TGSkCustomParamSaver;
    szTestData:  packed array[0..255] of Char;
    lFont:  TFont;

  begin  { RestParams }
    lPar := SaverClass.Create(nil);
    try
      if  lPar.OpenKey('korzeń\węzeł')  then
        try
          Check(lPar.ReadBinaryData('binarne', szTestData, SizeOf(szTestData))
                    = Integer(StrLen(csTestData)), 'ReadBinaryData');
          Check(lPar.ReadBool('logiczne'), 'ReadBool');
          Check(lPar.ReadCurrency('kwota') = 1.2345, 'ReadCurrency');
          Check(lPar.ReadDate('data') = tmCurrTime, 'ReadDate');
          Check(lPar.ReadTime('czas') = tmCurrTime, 'ReadTime');
          Check(lPar.ReadDateTime('data i czas') = tmCurrTime, 'ReadDateTime');
          Check(SameValue(lPar.ReadFloat('rzeczywista'),
                          Pi, 0.00000001),
                'ReadFloat');
          Check(lPar.ReadInteger('całkowita') = MaxInt, 'ReadInteger');
          Check(lPar.ReadString('napis') = csTestData, 'ReadString');
          Check(lPar.ReadChar('znak') = 'x', 'ReadChar');
          Check(lPar.ReadColor('kolor') = clBlue, 'ReadColor');
          lFont := lPar.ReadFont('czcionka', Screen.Forms[0].Font);
          try
            Check(FontToString(lFont) = FontToString(Screen.Forms[0].Font),
                  'ReadFont')
          finally
            lFont.Free()
          end { try-finally };
          Check(lPar.ReadInteger('szesnastkowo') = $abcdef, 'ReadInteger[Hex]')
        finally
          lPar.CloseKey()
        end { try-finally; if OpenKey(...) }
    finally
      lPar.Free()
    end { try-finally }
  end { RestParams };

begin  { TestParamReadWrite }
  tmCurrTime := Now();
  SaveParams(TGSkParamSaverRegistry);
  SaveParams(TGSkParamSaverXML);
  RestParams(TGSkParamSaverRegistry);
  RestParams(TGSkParamSaverXML)
end { TestParamReadWrite };


procedure  TParamSaver_Test.TestSerieReg();

var
  nCur:  Currency;

begin  { TestSerieReg }
  with  TGSkParamSaverRegistry.Create(nil)  do
    try
      if  OpenKey('korzeń\węzeł', False)  then
        try
          if  ValueExists('kwota')  then
            begin
              nCur := ReadCurrency('kwota');
              if  nCur < 0  then
                MsgDlg('Kwota jest ujemna', mtWarning)
            end { ValueExists('kwota') }
        finally
          CloseKey()
        end { try-finally }
    finally
      Free()
    end { try-finally };
  with  TGSkParamSaverRegistry.Create(nil)  do
    try
      if  OpenKey('korzeń\węzeł', True)  then
        try
          WriteCurrency('kwota', 102938.4756)
        finally
          CloseKey()
        end { try-finally }
    finally
      Free()
    end { try-finally }
end { TestSerieReg };


procedure  TParamSaver_Test.TestSerieXML();

var
  nCur:  Currency;

begin  { TestSerieXML }
  with  TGSkParamSaverXML.Create(nil)  do
    try
      if  OpenKey('korzeń\węzeł', False)  then
        try
          if  ValueExists('kwota')  then
            begin
              nCur := ReadCurrency('kwota');
              if  nCur < 0  then
                MsgDlg('Kwota jest ujemna', mtWarning)
            end { ValueExists('kwota') }
        finally
          CloseKey()
        end { try-finally }
    finally
      Free()
    end { try-finally };
  with  TGSkParamSaverXML.Create(nil)  do
    try
      if  OpenKey('korzeń\węzeł', True)  then
        try
          WriteCurrency('kwota', 102938.4756)
        finally
          CloseKey()
        end { try-finally }
    finally
      Free()
    end { try-finally }
end { TestSerieXML };


initialization
  RegisterTest(TParamSaver_Test.Suite())


end.
