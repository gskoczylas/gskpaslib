#!/bin/bash

# Pobierz listę wszystkich zdalnych repozytoriów
remotes=$(git remote)

# Iteruj przez każde zdalne repozytorium i wykonaj git push
for remote in $remotes; do
    echo ""
	echo -n "$remote: "
	git push $remote
done
