﻿program  GSkPasLib_XE;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


uses
  Forms,
  TestFrameWork,
  GUITestRunner,
  GSkPasLib.PasLib in '..\GSkPasLib.PasLib.pas',
  GSkPasLib.Tokens in '..\GSkPasLib.Tokens.pas',
  GSkPasLib.ASCII in '..\GSkPasLib.ASCII.pas',
  GSkPasLib.SystemUtils in '..\GSkPasLib.SystemUtils.pas',
  GSkPasLib.FileUtils in '..\GSkPasLib.FileUtils.pas',
  GSkPasLib.PasLibInternals in '..\GSkPasLib.PasLibInternals.pas',
  GSkPasLib.FormUtils in '..\GSkPasLib.FormUtils.pas',
  GSkPasLib.CustomComponents in '..\GSkPasLib.CustomComponents.pas',
  GSkPasLib.SynchroUtils in '..\GSkPasLib.SynchroUtils.pas',
  GSkPasLib.Log in '..\GSkPasLib.Log.pas',
  GSkPasLib.LogFile in '..\GSkPasLib.LogFile.pas',
  GSkPasLib.AppUtils in '..\GSkPasLib.AppUtils.pas',
  GSkPasLib.Dialogs in '..\GSkPasLib.Dialogs.pas',
  GSkPasLib.StrUtils in '..\GSkPasLib.StrUtils.pas',
  GSkPasLib.ComponentUtils in '..\GSkPasLib.ComponentUtils.pas',
  GSkPasLib.PropEdAboutForm in '..\GSkPasLib.PropEdAboutForm.pas' {PropEdAboutForm},
  GSkPasLib.Check in '..\GSkPasLib.Check.pas',
  GSkPasLib.PrevInst in '..\GSkPasLib.PrevInst.pas',
  GSkPasLib.Math in '..\GSkPasLib.Math.pas',
  GSkPasLib.DataEmbedded in '..\GSkPasLib.DataEmbedded.pas',
  GSkPasLib.SystemFirewall in '..\GSkPasLib.SystemFirewall.pas',
  GSkPasLib.Speaker in '..\GSkPasLib.Speaker.pas',
  GSkPasLib.ParamSaver in '..\GSkPasLib.ParamSaver.pas',
  GSkPasLib.XMLUtils in '..\GSkPasLib.XMLUtils.pas',
  GSkPasLib.CustomDataModule in '..\GSkPasLib.CustomDataModule.pas' {zzGSkCustomDataModule: TDataModule},
  GSkPasLib.CustomMainDataModule in '..\GSkPasLib.CustomMainDataModule.pas' {zzGSkCustomMainDataModule: TDataModule},
  GSkPasLib.DatabaseLoginDlg in '..\GSkPasLib.DatabaseLoginDlg.pas' {zzFormLoginDlg},
  GSkPasLib.MiscUtils in '..\GSkPasLib.MiscUtils.pas',
  GSkPasLib.CustomForm in '..\GSkPasLib.CustomForm.pas' {zzGSkCustomForm},
  GSkPasLib.Form in '..\GSkPasLib.Form.pas' {zzGSkForm},
  GSkPasLib.SpeedBarCtl in '..\GSkPasLib.SpeedBarCtl.pas',
  GSkPasLib.CustomMainForm in '..\GSkPasLib.CustomMainForm.pas' {zzGSkCustomMainForm},
  GSkPasLib.DbUtils in '..\GSkPasLib.DbUtils.pas',
  GSkPasLib.ClientDataSet in '..\GSkPasLib.ClientDataSet.pas',
  GSkPasLib.LogDialog in '..\GSkPasLib.LogDialog.pas',
  GSkPasLib.TimeUtils in '..\GSkPasLib.TimeUtils.pas',
  GSkPasLib.CmdLineOptionParser in '..\GSkPasLib.CmdLineOptionParser.pas',
  GSkPasLib.Thread in '..\GSkPasLib.Thread.pas',
  GSkPasLib.Classes in '..\GSkPasLib.Classes.pas',
  GSkPasLib.CSV in '..\GSkPasLib.CSV.pas',
  Test.GSkPasLib.Classes in 'Test.GSkPasLib.Classes.pas',
  Test.GSkPasLib.ComponentUtils in 'Test.GSkPasLib.ComponentUtils.pas',
  Test.GSkPasLib.CSV in 'Test.GSkPasLib.CSV.pas',
  Test.GSkPasLib.DataEmbedded in 'Test.GSkPasLib.DataEmbedded.pas',
  Test.GSkPasLib.CustomComponents in 'Test.GSkPasLib.CustomComponents.pas',
  Test.GSkPasLib.FileUtils in 'Test.GSkPasLib.FileUtils.pas',
  Test.GSkPasLib.ParamSaver in 'Test.GSkPasLib.ParamSaver.pas',
  Test.GSkPasLib.Registry in 'Test.GSkPasLib.Registry.pas',
  Test.GSkPasLib.Tokens in 'Test.GSkPasLib.Tokens.pas',
  Test.GSkPasLib.Log in 'Test.GSkPasLib.Log.pas',
  Test.GSkPasLib.StrUtils in 'Test.GSkPasLib.StrUtils.pas',
  Test.GSkPasLib.TimeUtils in 'Test.GSkPasLib.TimeUtils.pas',
  GSkPasLib.Ident in '..\GSkPasLib.Ident.pas',
  Test.GSkPasLib.Ident in 'Test.GSkPasLib.Ident.pas';

{$R *.res}


begin
  Application.Initialize();  // <-- bez tego XML nie działa!
  Application.Title := 'GSkPasLib Test';
  Randomize();
  // ReportMemoryLeaksOnShutdown := True;
  GUITestRunner.RunTest(TestFramework.RegisteredTests)
end.
