object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 166
  ClientWidth = 319
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnGetSources: TButton
    Left = 8
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Get Sources'
    TabOrder = 0
    OnClick = btnGetSourcesClick
  end
  object edtList: TMemo
    Left = 8
    Top = 8
    Width = 305
    Height = 114
    ScrollBars = ssBoth
    TabOrder = 1
  end
end
