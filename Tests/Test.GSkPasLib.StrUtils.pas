﻿unit  Test.GSkPasLib.StrUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}


uses
  TestFrameWork, GSkPasLib.StrUtils,
  Graphics, Windows, SysUtils;


type
  TStrUtils_Test = class(TTestCase)
  strict private
    procedure  BinToIntGrpErr();
    procedure  BinToIntNoDigits();
  published
    procedure  VerifyQuoteStr();
    procedure  VerifyCondQuoteStr();
//    procedure  VerifyStrOEM();
    procedure  VerifyOdmiana();
    procedure  VerifyConvByteToBin();
    procedure  VerifyConvWordToBin();
    procedure  VerifyConvLongWordToBin();
    procedure  VerifyConvInt64ToBin();
    procedure  VerifyBinToInt();
    procedure  VerifyNumberToStr();
    procedure  VerifyCharsOnly();
    procedure  VerifyCharsBut();
  end { TStrUtils_Test };


implementation


{ TStrUtils_Test }

procedure  TStrUtils_Test.BinToIntGrpErr();
begin
  BinToInt('0001-0010-0011-0100')
end { BinToIntGrpErr };


procedure  TStrUtils_Test.BinToIntNoDigits();
begin
  BinToInt('Bez cyfr')
end { BinToIntNoDigits };


procedure  TStrUtils_Test.VerifyBinToInt();
begin
  CheckEqualsHex($12, BinToInt('10010'), 'Tylko cyfry', 2);
  CheckEqualsHex($12, BinToInt('1 0010'), 'Pogrupowane cyfry', 2);
  CheckEquals(0, BinToInt(''), 'Pusty napis powinien dać zero');
  CheckException(BinToIntGrpErr, EConvertError, 'Grupowanie inne niż odstępy');
  CheckException(BinToIntNoDigits, EConvertError, 'Niepoprawna liczba');
end { VerifyBinToInt };


procedure  TStrUtils_Test.VerifyCharsBut();
begin
  CheckEquals('9372188329',   CharsOnly('PL 937-21-88-329', ['0'..'9']));
  CheckEquals('PL9372188329', CharsOnly('PL 937-21-88-329', ['0'..'9', 'A'..'Z']))
end { VerifyCharsBut };


procedure  TStrUtils_Test.VerifyCharsOnly();
begin
  CheckEquals('+48338285701', CharsBut('+48 (33) 82 85 701', ['(', ')', ' ']));
  CheckEquals('338285701', CharsBut('tel. 338 285 701', ['a'..'z', 'A'..'Z', #0..Pred('0')]))
end { VerifyCharsOnly };


procedure  TStrUtils_Test.VerifyCondQuoteStr();

const
  csSimpleTextInp = 'Prosty tekst';
  csSimpleTextOut = 'Prosty tekst';
  csTwoLinesImp = 'First line'#13#10'Second line';
  csTwoLinesOut = '"First line"#13#10"Second line"';
  csStartSpaceInp = ' Od odstępu';
  csStartSpaceOut = '" Od odstępu"';
  csDelimInsideInp = 'Pseudonim "GSk" to ja';
  csDelimInsideOut = '"Pseudonim ""GSk"" to ja"';
  csDelimStartImp = '"GSk" to ja';
  csDelimStartOut = '"""GSk"" to ja"';
  csDelimStopInp = 'Ja to "GSk"';
  csDelimStopOut = '"Ja to ""GSk"""';
  csSingleDelimInp = '"';
  csSingleDelimOut = '""""';
  csEmptyInp = '';
  csEmptyOut = '""';

begin  { VerifyCondQuoteStr }
  CheckEquals(csSimpleTextOut, CondQuoteStr(csSimpleTextInp, '"'));
  CheckEquals(csTwoLinesOut, CondQuoteStr(csTwoLinesImp, '"'));
  CheckEquals(csStartSpaceOut, CondQuoteStr(csStartSpaceInp, '"'));
  CheckEquals(csDelimInsideOut, CondQuoteStr(csDelimInsideInp, '"'));
  CheckEquals(csDelimStartOut, CondQuoteStr(csDelimStartImp, '"'));
  CheckEquals(csDelimStopOut, CondQuoteStr(csDelimStopInp, '"'));
  CheckEquals(csSingleDelimOut, CondQuoteStr(csSingleDelimInp, '"'));
  CheckEquals(csEmptyOut, CondQuoteStr(csEmptyInp, '"'));
end { VerifyCondQuoteStr };


procedure  TStrUtils_Test.VerifyConvByteToBin();
begin
  CheckEquals('1010 1011', IntToBin($AB), 'ByteToBinary');
  CheckEquals('10101011', IntToBin($AB, 8, []), 'ByteToBinary bez grupowania');
  CheckEquals('1 0101', IntToBin($15, 1, [bfGrouping]), 'ByteToBinary bez nieznaczących zer');
  CheckEquals('10101', IntToBin($15, 1, [bfTrimEmptyGroups, bfTrimLeadingZeros]), 'ByteToBinary bez nieznaczących zer i grupowania');
  CheckEquals('01 1000', IntToBin($18, 6), 'ByteToBinary - 6 cyfr');
  CheckEquals('1000 0001', IntToBin($81, 6), 'BytToBinary - 6-->8 cyfr')
end { VerifyConvByteToBin };


procedure  TStrUtils_Test.VerifyConvInt64ToBin();

var
  nBuf:  Int64;

begin  { VerifyConvInt64ToBin }
  CheckEquals('0001 0010 0011 0100 0101 0110 0111 1000', IntToBin($12345678),
              'Int64ToBin');
  CheckEquals('00010010001101000101011001111000', IntToBin($12345678, 64, []),
              'Int64ToBin bez grupowania');
  CheckEquals('1 0010 0011 0100 0101 0110 0111 1000', IntToBin($12345678, 1),
              'Int64ToBin bez nieznaczących zer');
  nBuf := $12;
  CheckEquals('1 0010', IntToBin(nBuf, 1), 'Int64ToBin (variable)')
end { VerifyConvInt64ToBin };


procedure  TStrUtils_Test.VerifyConvLongWordToBin();

var
  nBuf:  LongWord;

begin  { VerifyConvLongWordToBin }
  CheckEquals('0000 0000 1010 1011 1100 1101 1110 1111', IntToBin($ABCDEF),
              'LongWordToBinary');
  CheckEquals('00000000101010111100110111101111', IntToBin($ABCDEF, 32, []),
              'LongWordToBinary bez grupowania');
  CheckEquals('1010 1011 1100 1101 1110 1111', IntToBin($ABCDEF, 1),
              'LongWordToBinary bez nieznaczących zer');
  nBuf := $76543210;
  CheckEquals('0111 0110 0101 0100 0011 0010 0001 0000', IntToBin(nBuf),
              'LongWordToBin');
  nBuf := $12;
  CheckEquals('1 0010', IntToBin(nBuf, 1), 'IntToBin bez nieznaczących zer')
end { VerifyConvLongWordToBin };


procedure  TStrUtils_Test.VerifyConvWordToBin();

var
  nBuf:  Word;

begin  { VerifyConvWordToBin }
  CheckEquals('1010 1011 1100 1101', IntToBin($ABCD), 'WordToBinary');
  CheckEquals('1010101111001101', IntToBin($ABCD, 16, []), 'WordToBinary bez grupowania');
  CheckEquals('1 0010 0011 0100', IntToBin($1234, 1), 'WordToBinary bez nieznaczących zer (v.1)');
  CheckEquals('10 0011 0100', IntToBin($234, 1), 'WordToBinary bez nieznaczących zer (v.2)');
  CheckEquals('010 0011 0100', IntToBin($234, 11), 'WordToBinary - 11 cyfr');
  CheckEquals('1000 0100 0010 0001', IntToBin($8421, 11), 'WordToBinary - 11-->16 cyfr');
  nBuf := $12;
  CheckEquals('1 0010', IntToBin(nBuf, 1), 'WordToBinary (variable)')
end { VerifyConvWordToBin };


procedure  TStrUtils_Test.VerifyNumberToStr();

const
  cnMinVal = 0;
  cnMaxVal = 1000;

var
  nValue:  LongWord;
  nBase:   TNumberToStrBase;
  sValue:  string;
  nResult: UInt64;

begin  { VerifyNumberToStr }
  for  nValue := cnMinVal  to  cnMaxVal  do
    for  nBase := Low(TNumberToStrBase)  to  High(TNumberToStrBase)  do
      begin
        sValue := NumberToStr(nValue, nBase);
        nResult := StrToNumber(sValue, nBase);
        CheckEquals(nValue, nResult,
                    Format('Value = $%0:x (%0:u), Text = "%1:s", Result = $%2:x (%2:u)',
                           [nValue, sValue, nResult]))
      end
end { VerifyNumberToStr };


procedure  TStrUtils_Test.VerifyOdmiana();

  procedure  Verify(const   nGodz:  Integer;
                    const   sExpected:  string);
  begin
    CheckEquals(Format('%d %s', [nGodz, sExpected]),
                Format('%d %s',
                       [nGodz,
                        Odmiana(nGodz, 'godzinę', 'godziny', 'godzin')]));
  end { Verify };

begin  { VerifyOdmiana }
  Verify(1, 'godzinę');
  Verify(2, 'godziny');
  Verify(3, 'godziny');
  Verify(4, 'godziny');
  Verify(5, 'godzin');
  Verify(6, 'godzin');
  Verify(7, 'godzin');
  Verify(8, 'godzin');
  Verify(9, 'godzin');
  Verify(10, 'godzin');
  Verify(11, 'godzin');
  Verify(12, 'godzin');
  Verify(20, 'godzin');
  Verify(21, 'godzin');
  Verify(22, 'godziny');
  Verify(25, 'godzin')
end { VerifyOdmiana };


procedure  TStrUtils_Test.VerifyQuoteStr();

const
  csSimpleTextInp = 'Prosty tekst';
  csSimpleTextOut = '"Prosty tekst"';
  csTwoLinesImp = 'First line'#13#10'Second line';
  csTwoLinesOut = '"First line"#13#10"Second line"';
  csStartSpaceInp = ' Od odstępu';
  csStartSpaceOut = '" Od odstępu"';
  csDelimInsideInp = 'Pseudonim "GSk" to ja';
  csDelimInsideOut = '"Pseudonim ""GSk"" to ja"';
  csDelimStartImp = '"GSk" to ja';
  csDelimStartOut = '"""GSk"" to ja"';
  csDelimStopInp = 'Ja to "GSk"';
  csDelimStopOut = '"Ja to ""GSk"""';
  csSingleDelimInp = '"';
  csSingleDelimOut = '""""';
  csDelimCtrlInp = 'Test "'#9'".';
  csDelimCtrlOut = '"Test """#9"""."';
  csCtrlDelimInp = 'Test '#8'"BS".';
  csCtrlDelimOut = '"Test "#8"""BS""."';
  csEmptyInp = '';
  csEmptyOut = '""';

begin  { VerifyQuoteStr }
  CheckEquals(csSimpleTextOut, QuoteStr(csSimpleTextInp, '"'));
  CheckEquals(csTwoLinesOut, QuoteStr(csTwoLinesImp, '"'));
  CheckEquals(csStartSpaceOut, QuoteStr(csStartSpaceInp, '"'));
  CheckEquals(csDelimInsideOut, QuoteStr(csDelimInsideInp, '"'));
  CheckEquals(csDelimStartOut, QuoteStr(csDelimStartImp, '"'));
  CheckEquals(csDelimStopOut, QuoteStr(csDelimStopInp, '"'));
  CheckEquals(csSingleDelimOut, QuoteStr(csSingleDelimInp, '"'));
  CheckEquals(csDelimCtrlOut, QuoteStr(csDelimCtrlInp, '"'));
  CheckEquals(csCtrlDelimOut, QuoteStr(csCtrlDelimInp, '"'));
  CheckEquals(csEmptyOut, QuoteStr(csEmptyInp, '"'))
end { VerifyQuoteStr };


//procedure  TStrUtils_Test.VerifyStrOEM();
//begin
//  CheckEqualsString('aĄc†e©l'#$88'näo˘s'#$98'x«zľ', string(StringToOEM('aącćeęlłnńoósśxźzż')));
//  CheckEqualsString('A¤CŹE¨LťNăOŕS—XŤZ˝', string(StringToOEM('AĄCĆEĘLŁNŃOÓSŚXŹZŻ')));
//  CheckEqualsString('aącćeęlłnńoósśxźzż', OEMToString('aĄc†e©l'#$88'näo˘s'#$98'x«zľ'));
//  CheckEqualsString('AĄCĆEĘLŁNŃOÓSŚXŹZŻ', OEMToString('A¤CŹE¨LťNăOŕS—XŤZ˝'))
//end { VerifyStrOEM };
// ^^^ Ten test jest poprawny pod warunkiem, że w Windows jest ustawiona polska strona kodowa.


initialization
  RegisterTest(TStrUtils_Test.Suite())


end.
