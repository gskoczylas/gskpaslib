﻿  // Do porównywania ze standardową stałą „CompilerVersion”
  gcnCompilerVersionDelphi6    = 14.0;
  gcnCompilerVersionDelphi7    = 15.0;
  gcnCompilerVersionDelphi8    = 16.0;
  gcnCompilerVersionDelphi2005 = 17.0;
  gcnCompilerVersionDelphi2006 = 18.0;
  gcnCompilerVersionDelphi2007 = 18.5;
  gcnCompilerVersionDelphi2009 = 20.0;
  gcnCompilerVersionDelphi2010 = 21.0;
  gcnCompilerVersionDelphiXE   = 22.0;
  gcnCompilerVersionDelphiXE2  = 23.0;
  gcnCompilerVersionDelphiXE3  = 24.0;
  gcnCompilerVersionDelphiXE4  = 25.0;
  gcnCompilerVersionDelphiXE5  = 26.0;
  gcnCompilerVersionDelphiXE6  = 27.0;
  gcnCompilerVersionDelphiXE7  = 28.0;
  gcnCompilerVersionDelphiXE8  = 29.0;
//gcnCompilerVersionDelphiXE9  =
  gcnCompilerVersionDelphi10   = 30.0;   // 10.0 Seattle
  gcnCompilerVersionDelphi10_0 = gcnCompilerVersionDelphi10;
  gcnCompilerVersionDelphiXE10 = gcnCompilerVersionDelphi10;
  gcnCompilerVersionDelphi10_1 = 31.0;   // 10.1 Berlin
  gcnCompilerVersionDelphi10_2 = 32.0;   // 10.2 Tokyo
  gcnCompilerVersionDelphi10_3 = 33.0;   // 10.3 Rio
  gcnCompilerVersionDelphi10_4 = 34.0;   // 10.4 Sydney
  gcnCompilerVersionDelphi11   = 35.0;   // 11.0 Alexandria
  gcnCompilerVersionDelphi11_0 = gcnCompilerVersionDelphi11;


