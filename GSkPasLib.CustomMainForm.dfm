inherited zzGSkCustomMainForm: TzzGSkCustomMainForm
  BorderStyle = bsSingle
  ClientHeight = 59
  ClientWidth = 379
  Menu = mnMainMenu
  PixelsPerInch = 96
  TextHeight = 13
  inherited sbStatus: TStatusBar
    Top = 40
    Width = 379
    Panels = <
      item
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Width = 50
      end>
    SizeGrip = False
    ExplicitTop = 40
    ExplicitWidth = 379
  end
  object sbSpeedBar: TJvSpeedBar
    Left = 0
    Top = 0
    Width = 379
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowResize, sbGrayedBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 34
    BtnHeight = 34
    BevelOuter = bvNone
    TabOrder = 1
    InternalVer = 1
  end
  object mnMainMenu: TMainMenu
    Left = 136
    Top = 8
    object mnPlik: TMenuItem
      Caption = '&Plik'
      object mnTerminate: TMenuItem
        Action = aTerminate
      end
    end
  end
  object alActions: TActionList
    Left = 168
    Top = 8
    object aTerminate: TAction
      Caption = 'Konie&c'
      Hint = 'Koniec pracy programu'
      ShortCut = 32883
      OnExecute = aTerminateExecute
    end
  end
end
