﻿{: @abstract Prototyp głównego modułu danych aplikacji.

   W aplikacjach główny moduł danych powinien być potomkiem zdefiniowanego w
   module @name prototypu @link(TzzGSkCustomMainDataModule). Zawiera on komponent
   @code(TIBDatabase) oraz mechanizmy logowania do bazy danych.

   @seeAlso(GSkPasLib.CustomDataModule)  }
unit  GSkPasLib.CustomMainDataModule;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBDatabase, GSkPasLib.CustomDataModule;


type
  {: @abstract Prototyp głównego modułu danych aplikacji.

     W aplikacjach główny moduł danych powinien być potomkiem prototypu
     @name. Zawiera on komponent @code(TIBDatabase) oraz mechanizmy logowania
     do bazy danych.

     W typowej aplikacji powinien być jeden moduł danych będący potomkiem
     obiektu @name.

     @seeAlso(TzzGSkCustomDataModule)  }
  TzzGSkCustomMainDataModule = class(TzzGSkCustomDataModule)
    dbDatabase:  TIBDatabase;
  private
    fsAlias:  string;
    function  GetUserName() : string;
    procedure  SetAlias(const   sAlias:  string);
  protected

    {: @abstract Sprawdza zgodność programu z bazą danych.

       Metoda @name służy do sprawdzania zgodności programu z wersją struktur
       danych w bazie danych. Jest ona automatycznie wywoływana tuż po połączeniu
       z bazą danych.

       W obiekcie @className metoda @name nic nie robi. W aplikacji należy
       używać moduł danych zawierający obiekt będący potomkiem obiektu
       @className. W tym potomnym obiekcie można nadpisać metodę @name i zawrzeć
       w niej instrukcje sprawdzające zgodność aplikacji ze strukturami bazy
       danych.  }
    procedure  CheckMetadataVersion();                               dynamic;

  public

    {: @exclude }
    procedure  AfterConstruction();                                  override;

    {: @abstract Alias wybranej bazy danych.

       Mechanizm logowania do bazy pobiera dane z pliku konfiguracyjnego, który
       może zawierać informacje o dowolnie wielu bazach danych. Właściwość @name
       wskazuje alias bazy danych wybranej przez użytkownika.

       @seeAlso(UserName)  }
    property  DatabaseAlias:  string
              read  fsAlias;

    {: @abstract Użytkownik bazy danych.

       Właściwość zwraca identyfikator użytkownika zalogowanego do bazy danych.

       @seeAlso(DatabaseAlias)  }
    property  UserName:  string
              read  GetUserName;
  end { TzzGSkCustomMainDataModule };


// var
//   zzGSkCustomMainDataModule:  TzzGSkCustomMainDataModule;


function  GetMainDataModule() : TzzGSkCustomMainDataModule;


implementation


uses
  GSkPasLib.DatabaseLoginDlg, GSkPasLib.CustomMainForm;


{$R *.dfm}


var
  dmMainDataModule:  TzzGSkCustomMainDataModule;


function  GetMainDataModule() : TzzGSkCustomMainDataModule;
begin
  Result := dmMainDataModule
end { GetMainDataModule() };


{ TdmGSkCustomMainDataModule }

procedure  TzzGSkCustomMainDataModule.AfterConstruction();
begin
  dmMainDataModule := Self;
  inherited;
  TzzFormLoginDlg.Login(dbDatabase, SetAlias);
  CheckMetadataVersion()
end { AfterConstruction };


procedure  TzzGSkCustomMainDataModule.CheckMetadataVersion();
begin
  { override in application }
end { CheckMetadataVersion };


function  TzzGSkCustomMainDataModule.GetUserName() : string;
begin
  Result := dbDatabase.Params.Values['user_name']
end { GetUserName };


procedure  TzzGSkCustomMainDataModule.SetAlias(const   sAlias:  string);
begin
  fsAlias := sAlias;
  if  Application.MainForm <> nil  then
    if  Application.MainForm is TzzGSkCustomMainForm  then
      (Application.MainForm as TzzGSkCustomMainForm).ShowDatabaseParams(fsAlias,
                                                                        UserName)
end { SetAlias };


end.
