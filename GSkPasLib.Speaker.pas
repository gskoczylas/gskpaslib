﻿{: @abstract Moduł obsługi standardowego głośnika komputerów PC

   A sygnał dźwiękowy jest jednym ze skutecznych sposobów zwrócenia uwagi
   użytkownika na niektóre wyjątkowe sytuacje, na przykład zakończenie
   długotrwałego działania.

   Obecnie prawie każdy nowy komputer jest wyposażony w kartę dźwiękową. Jednak
   starsze komputery nadal często są karty dźwiękowej pozbawione. W tej sytuacji
   możemy wydobyć z komputera proste dźwięki przy pomocy wbudowanego w każdy
   komputer prostego głośniczka (niezależnego od karty dźwiękowej).

   W standardowym module @code(Windows) dostępna jest funkcja @code(Beep)
   przeznaczona do sterowania tym standardowym głośnikiem. Niestety @bold(nie)
   jest ona dostępna w systemach @italic(Windows 95), @italic(Windows 98) oraz
   @italic(Windows Me). Jeżeli chcemy aby nasz program działał również
   w wymienionych systemach operacyjnych można skorzystać z podprogramów
   oferowanych przez moduł @name.  }
unit  GSkPasLib.Speaker;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Windows, SysUtils;


type

  {: @abstract Definicja pojedynczego dźwięku.

     @member nPitch Pole @name oznacza częstotliwość dźwięku.
     @member nDuration Pole @name oznacza czas trwania dźwięku --- liczba milisekund.
  }
  TGSkTone = record
    nPitch:     Cardinal;
    nDuration:  Cardinal
  end { TGSkTone };

  {: @abstract Dynamiczna tablica dźwięków.

     Typ @name definiuje dynamiczną tablicę dźwięków. Parametry tego typu
     używane są w jednej z wersji procedury @link(Melody).

     @seeAlso(Melody)
  }
  TGSkToneDynArray = array of TGSkTone;

  {: @abstract Standardowe melodie generowane przez wbudowany głośnik.

     Standardowe melodia możliwe do odtworzenia przez wbudowany głośnik --- przy
     pomocy jednej z wersji procedury @link(Melody).

     @value snError     Błąd
     @value snUrgent    Pilne
     @value snAttention Uwaga
     @value snSuccess   Sukces
     @value snCorrect   Poprawnie

     @seeAlso(Melody)
  }
  TGSkStandardNoise = (snError, snUrgent, snAttention, snSuccess, snCorrect);


{: @abstract Zezwala na generowanie dźwięków.

   Generowanie dźwięków przez procedury @link(GSkPasLib.Speaker tego modułu) może
   zostać zablokowane przez wywołanie procedury @link(DisableSounds).
   Aby przywrócić możliwość generowania dźwięków należy wywołać procedurę @name.

   Jeżeli procedura @link(DisableSounds) została wywołana wielokrotnie to
   procedurę @name trzeba wywołać tyle samo razy aby przywrócić generowanie
   dźwięków.

   Generowanie dźwięków jest kontrolowane przez wewnętrzną zmienną. Na początku
   ta zmienna jest wyzerowana. Każde wywołanie procedury @name zwiększa wartość
   tej zmiennej o jeden, a każde wywołanie procedury @link(DisableSounds)
   zmniejsza jej wartość o jeden. Dźwięki są generowane tylko wtedy gdy wartość
   tej wewnętrznej zmiennej jest nieujemna.

   @seeAlso(Tone)
   @seeAlso(Melody)
}
procedure  EnableSounds();

{: @abstract Blokuje generowanie dźwięków.

   Generowanie dźwięków przez procedury @link(GSkPasLib.Speaker tego modułu) może
   zostać zablokowane przez wywołanie procedury @name. Aby przywrócić możliwość
   generowania dźwięków należy wywołać procedurę @link(EnableSounds).

   Jeżeli procedura @name została wywołana wielokrotnie to procedurę
   @link(EnableSounds) trzeba wywołać tyle samo razy aby przywrócić generowanie
   dźwięków.

   Generowanie dźwięków jest kontrolowane przez wewnętrzną zmienną. Na początku
   ta zmienna jest wyzerowana. Każde wywołanie procedury @link(EnableSounds)
   zwiększa wartość tej zmiennej o jeden, a każde wywołanie procedury @name
   zmniejsza jej wartość o jeden. Dźwięki są generowane tylko wtedy gdy wartość
   tej wewnętrznej zmiennej jest nieujemna.

   @seeAlso(Tone)
   @seeAlso(Melody)
}
procedure  DisableSounds();

{: @abstract Procedura @name generuje pojedynczy dźwięk.

   Procedura @name generuje dźwięk o wskazanych parametrach.

   @param nFrequency Częstotliwość generowanego dźwięku.
   @param Duration Czas trwania dźwięku --- w milisekundach.

   @seeAlso(EnableSounds)
   @seeAlso(DisableSounds)
   @seeAlso(Melody)
}
procedure  Tone(const   nFrequency:  Cardinal;
                const   nDuration:  Cardinal);                       overload;


{: @abstract Procedura @name generuje pojedynczy dźwięk.

   Procedura @name generuje dźwięk o wskazanych parametrach.

   @param Częstotliwość i czas trwania dźwięku.

   @seeAlso(EnableSounds)
   @seeAlso(DisableSounds)
   @seeAlso(Melody)
}
procedure  Tone(const   Param:  TGSkTone);                           overload;

{: @abstract Procedura @name odtwarza sekwencję dźwięków.

   Procedura @name odtwarza sekwencję dźwięków zdefiniowaną przez tablicę tonów.

   @param Tones Dowolna tablica definiujące sekwencję dźwięków.

   @seeAlso(EnableSounds)
   @seeAlso(DisableSounds)
   @seeAlso(Tone)
}
procedure  Melody(const   Tones:  array of TGSkTone);                overload;

{: @abstract Procedura @name odtwarza sekwencję dźwięków.

   Procedura @name odtwarza sekwencję dźwięków zdefiniowaną przez tablicę tonów.

   @param Tones Dynamiczna tablica definiujące sekwencję dźwięków.

   @seeAlso(EnableSounds)
   @seeAlso(DisableSounds)
   @seeAlso(Tone)
}
procedure  Melody(const   Tones:  TGSkToneDynArray);                 overload;

{: @abstract Procedura @name odtwarza sekwencję dźwięków.

   Procedura @name odtwarza jedną ze @link(TGSkStandardNoise standardowych
   sekwencji dźwięków).

   @param StdNoise Identyfikator jednej ze standardowych melodii.

   @seeAlso(EnableSounds)
   @seeAlso(DisableSounds)
   @seeAlso(Tone)
}
procedure  Melody(const   StdNoise:  TGSkStandardNoise);             overload;


implementation


var
  gnSoundsLevel:  Integer;


procedure  EnableSounds();
begin
  InterlockedIncrement(gnSoundsLevel)
end { EnableSounds };


procedure  DisableSounds();
begin
  InterlockedDecrement(gnSoundsLevel)
end { DisableSounds };


{$IF Defined(WIN32) and not Defined(UNICODE)}

procedure  Beep9x(const   nFreq, nDuration:  Cardinal);

  procedure  Sound(const   nFreq:  Cardinal);

  var
    nTmp:  Word;

  begin  { Sound }
    nTmp := Word(1193181 div nFreq);
    asm
      MOV  AL,  3
      OUT  $61, AL
      MOV  AL,  $B6
      OUT  $43, AL
      MOV  AX,  nTmp
      OUT  $42, AL
      MOV  AL,  AH
      OUT  $42, AL
    end { asm }
  end { Sound };

  procedure  NoSound();
  asm
    MOV  AL, 0
    OUT  $61, AL
  end { NoSound };

begin  { Beep9x }
  Sound(nFreq);
  Sleep(nDuration);
  NoSound()
end { Beep9x };

{$IFEND}


procedure  Tone(const   nFrequency:  Cardinal;
                const   nDuration:  Cardinal);

begin
  if  gnSoundsLevel < 0  then
    Exit;
  if  nFrequency > 20000  then
    raise  ERangeError.Create('Frequency > 20 kHz');
  {$IFDEF UNICODE}
    if  nFrequency < $25  then
      raise  ERangeError.CreateFmt('Frequency < %d Hz', [$25]);
    Windows.Beep(nFrequency, nDuration)
  {$ELSE}
    if  Win32Platform = VER_PLATFORM_WIN32_NT  then
      begin
        if  nFrequency < $25  then
          raise  ERangeError.CreateFmt('Frequency < %d Hz', [$25]);
        Windows.Beep(nFrequency, nDuration)
      end { WinNT }
    else
      begin
        if  nFrequency < 20  then
          raise  ERangeError.Create('Frequency < 20 Hz');
        Beep9x(nFrequency, nDuration)
      end { Winows 9x/Me }
  {$ENDIF -UNICODE}
end { Tone };


procedure  Tone(const   Param:  TGSkTone);
begin
  with  Param  do
    Tone(nPitch, nDuration)
end { Tone };


procedure  Melody(const   Tones:  array of TGSkTone);

var
  nInd:  Integer;

begin  { Melody }
  for  nInd := Low(Tones)  to  High(Tones)  do
    Tone(Tones[nInd])
end { Melody };


procedure  Melody(const   Tones:  TGSkToneDynArray);

var
  nInd:  Integer;

begin  { Melody }
  for  nInd := Low(Tones)  to  High(Tones)  do
    Tone(Tones[nInd])
end { Melody };


procedure  Melody(const   StdNoise:  TGSkStandardNoise);

const
  cErrorNoise:  array[1..4] of  TGSkTone = (
                    (nPitch:  750;  nDuration: 10),
                    (nPitch: 1000;  nDuration: 10),
                    (nPitch: 1500;  nDuration: 20),
                    (nPitch: 2250;  nDuration: 40));
  cUrgentNoise: array[1..3] of  TGSkTone = (
                    (nPitch:  450;  nDuration:  30),
                    (nPitch: 3000;  nDuration: 300),
                    (nPitch: 4500;  nDuration:  30));
  cAttentionNoise:  array[1..5] of  TGSkTone = (
                        (nPitch:  800;  nDuration: 30),
                        (nPitch:  600;  nDuration: 40),
                        (nPitch: 1200;  nDuration: 10),
                        (nPitch:  600;  nDuration: 40),
                        (nPitch: 1400;  nDuration: 10));
  cSuccessNoise:  array[1..2] of  TGSkTone = (
                      (nPitch: 100;  nDuration: 10),
                      (nPitch: 200;  nDuration: 10));
  cCorrectNoise:  array[1..3] of  TGSkTone = (
                      (nPitch: 200;  nDuration: 100),
                      (nPitch: 100;  nDuration: 100),
                      (nPitch: 200;  nDuration: 100));

begin  { Melody }
  case  StdNoise  of
    snError:
      Melody(cErrorNoise);
    snUrgent:
      Melody(cUrgentNoise);
    snAttention:
      Melody(cAttentionNoise);
    snSuccess:
      Melody(cSuccessNoise);
    snCorrect:
      Melody(cCorrectNoise)
  end { case Noise }
end { Melody };


initialization
  gnSoundsLevel := 0;


end.
