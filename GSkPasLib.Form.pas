﻿{: @abstract Definicja okna podstawowego.

   Moduł @name zawiera definicję okna podstawowego. Jest ono potomkiem
   typu @link(TzzGSkCustomForm) i, pośrednio, standardowego typu @code(TForm)
   --- wprowadza do niego szereg kolejnych usprawnień.

   Wszystkie okna typowej aplikacji powinny być potomkami (bezpośrednimi lub
   pośrednimi) klasy @link(TzzGSkForm) (lub @link(TzzGSkCustomForm)).  }
unit  GSkPasLib.Form;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdActns,
  GSkPasLib.CustomForm;


type
  {: @abstract Okno podstawowe.

     Wszystkie okna typowej aplikacji powinny być bezpośrednimi lub pośrednimi
     potomkami okna @name (lub ewentualnie @link(TzzGSkCustomForm).

     Okno @name jest bezpośrednim potomkiem klasy @link(TzzGSkCustomForm).
     Wprowadza ono szereg usprawnień:
     @unorderedList(@itemSpacing(Compact)
       @item(Na dole okna jest pasek statusu. Jego właściwość @code(SizeGrip)
             jest, zależności od rodzaju obramowania okna, automatycznie
             ustawiana (@code(bsSizeable) lub @code(bsSizeToolWin)) lub
             wyłączana (w pozostałych przypadkach). Ponadto po zadokowaniu okna
             pasek statusu jest ukrywany, a po oddokowaniu ponownie wyświetlany.)
       @item(Metody @link(Status) oraz @link(SimpleStatus) ułatwiają
             wyświetlanie informacji na pasku statusu.)
       @item(Podpowiedzi (@italic(hints)) są automatycznie wyświetlane na pasku
             statusu (w zerowym panelu).))  }
  TzzGSkForm = class(TzzGSkCustomForm)
    sbStatus:  TStatusBar;
  private
    procedure  SetStatusSizeGrip();
  protected
    {: @exclude }
    procedure  SetBorderStyle(const   Value:  TFormBorderStyle);     override;
    {: @exclude }
    procedure  DoDock(NewDockSize:  TWinControl;
                      var   Rect:  TRect);                           override;
    {: @exclude }
    procedure  DoShow();                                             override;
  public
    {: @exclude}
    constructor  Create(aOwner:  TComponent);                        override;
    {: @exclude}
    function  ExecuteAction(Action:  TBasicAction)
                            : Boolean;                               override;
    {: @abstract Ułatwia wyświetlanie tekstu w pasku statusu.

       Metoda @name ułatwia wyświetlanie tekstu w pasku statusu. Metoda sprawdza
       właściwości paska statusu. Jeżeli włączona jest właściwość
       @code(SimplePanel) to wykorzystywana jest właściwość @code(SimpleText).
       W przeciwnym razie tekst wyświetlany jest we wskazanym panelu.

       Jeżeli okno jest zadokowane to metoda poszukuje okna nadrzędnego. Jeżeli
       znalezione okno jest typu @className lub pochodnego to tekst jest
       wyświetlany w pasku statusu tego okna.

       @param sInfo(Tekst wyświetlany na panelu.)
       @param nPanel(Numer panelu, w którym ma być wyświetlony tekst.
                     Jeżeli numer panelu jest ujemny to tekst jest wyświetlany
                     przy pomocy właściwości @code(SimpleText).
                     Jeżeli numer panelu ma wartość @code(MaxInt) to tekst jest
                     wyświetlany na ostatnim dostępnym panelu. )

       @seeAlso(SimpleStatus)  }
    procedure  Status(const   sInfo:  string;
                     {const}  nPanel:  Integer = MaxInt);

    {: @abstract Wyświetla tekst w prostym panelu.

       Metoda @name wywołuje metodę @link(Status) z numerem panelu równym
       @code(-1). W efekcie wskazany tekst jest wyświetlony w pasku statusu
       przy pomocy właściwości @code(SimpleText).)  }
    procedure  SimpleStatus(const   sInfo:  string);
  end { TGSkForm };


// var
//   GSkForm:  TGSkForm;


implementation


uses
  GSkPasLib.PasLibInternals;


{$R *.dfm}


{$REGION 'TGSkForm'}

constructor  TzzGSkForm.Create(aOwner:  TComponent);
begin
  inherited;
  { Uwaga!
  | ~~~~~~
  | Zdarza się od czasu do czasu, że po skompilowaniu programu (Delphi XE) nie
  | da się go uruchomić. Sygnalizowany jest błąd czytania ze strumienia.
  | Zdaje się, że pomaga następująca kolejność magicznych działań:
  | 1. Wyświetlić plik GSkPasLib.CustomForm (przełączyć między kodem i projektem).
  | 2. Wyświetlić plik GSkPasLib.Form (przełączyć między kodem i projektem).
  | 3. Wyświetlić plik u_MainForm (przełączyć między kodem i projektem).
  | 4. Przebudować projekt (Build).
  | Po wykonaniu powyższych magicznych działań program uruchamia się bez problemów.
  +------------------------------------------------------------------------------}
  sbStatus.AutoHint := False   // zobacz: ExecuteAction
end { Create };


procedure  TzzGSkForm.DoDock(NewDockSize:  TWinControl;
                             var   Rect:  TRect);
begin
  inherited;
  sbStatus.Visible := Floating
end { DoDock };


procedure  TzzGSkForm.DoShow();
begin
  inherited;
  SetStatusSizeGrip()
end { DoShow };


function  TzzGSkForm.ExecuteAction(Action:  TBasicAction)
                                   : Boolean;
begin
  if  Action is THintAction  then
    begin
      if  (sbStatus.SimplePanel)
          or (sbStatus.Panels.Count = 0)  then
        sbStatus.SimpleText := THintAction(Action).Hint
      else
        sbStatus.Panels[sbStatus.Panels.Count - 1].Text := THintAction(Action).Hint;
      Result := True
    end
  else
    Result := inherited ExecuteAction(Action)
end { ExecuteAction };


procedure  TzzGSkForm.SetBorderStyle(const   Value:  TFormBorderStyle);
begin
  inherited;
  SetStatusSizeGrip()
end { SetBorderStyle };


procedure  TzzGSkForm.SetStatusSizeGrip();
begin
  sbStatus.SizeGrip := (BorderStyle in [bsSizeable, bsSizeToolWin])
end { SetStatusSizeGrip };


procedure  TzzGSkForm.SimpleStatus(const   sInfo:  string);
begin
  Status(sInfo, -1)
end { SimpleStatus };


procedure  TzzGSkForm.Status(const   sInfo:  string;
                            {const}  nPanel:  Integer);
var
  Panel:  TStatusBar;
  Ctrl:  TControl;

begin  { Status }
  Panel := nil;
  if  Floating  then
    Panel := sbStatus
  else
    begin
      Ctrl := Self;
      repeat
        Ctrl := Ctrl.Parent
      until  (Ctrl is TzzGSkForm)
               and Ctrl.Floating
             or  (Ctrl = nil);
      if  Assigned(Ctrl)  then
        Panel := (Ctrl as TzzGSkForm).sbStatus
    end { not Floating };
  if  Assigned(Panel)  then
    if  (nPanel < 0)
        or (Panel.Panels.Count = 0)  then
      Panel.SimpleText := sInfo
    else
      begin
        if  nPanel = MaxInt  then
          nPanel := Panel.Panels.Count - 1
        else if  nPanel >= Panel.Panels.Count  then
          raise  EGSkPasLibError.CreateFmt('TGSkForm.Status',
                                           'Niepoprawny numer panelu (%d)',
                                           [nPanel]);
        Panel.Panels[nPanel].Text := sInfo
      end { nPanel >= 0 }
end { Status };

{$ENDREGION 'TzzGSkForm'}


end.
