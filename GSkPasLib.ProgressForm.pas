﻿{: @abstract Okno do wyświetlania postępu }
unit  GSkPasLib.ProgressForm;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Forms, StdCtrls, ComCtrls
  {$IFDEF GetText},
    JvGnugettext
  {$ENDIF GetText};




type
  {: @abstract Okno do wyświetlania paska postępu.

     W oknie są dostępne następujące komponenty: @unorderedList(@itemSpacing(Compact)
       @item(Tytuł okna.)
       @item(Tekst powyżej paska postępu.)
       @item(Pasek postępu.)
       @item(Dwa wiersze dodatkowych informacji poniżej paska postępu.)
       @item(Przycisk do anulowania.)) }
  TGSkProgressForm = class
  strict private                {$REGION 'private'}
    var
      fnWidth:       Integer;
      fnMin:         Integer;
      fnMax:         Integer;
      fnPosition:    Integer;
      fbSmooth:      Boolean;
      fbShowCancel:  Boolean;
      fbCancelled:   Boolean;
      fsCaption:     string;
      fsText:        string;
      fsExtraInfo:   string;
      fsExtraInfo2:  string;
      ffrmDialog:    TForm;
      flblText:      TLabel;
      flblInfo:      TLabel;
      flblInfo2:     TLabel;
      fpbrProgress:  TProgressBar;
      fbtnCancel:    TButton;
    function  GetCancelled() : Boolean;
    procedure  SetMin(const   nValue:  Integer);
    procedure  SetMax(const   nValue:  Integer);
    procedure  SetCaption(const   sValue:  string);
    procedure  SetPosition(const   nValue:  Integer);
    procedure  SetShowCancel(const   bValue:  Boolean);
    procedure  SetSmooth(const   bValue:  Boolean);
    procedure  SetText(const   sValue:  string);
    procedure  SetExtraInfo(const   sValue:  string);
    procedure  SetExtraInfo2(const   sValue:  string);
    procedure  SetWidth(const   nValue:  Integer);
    procedure  OnCancel(Sender:  TObject);    {$ENDREGION 'private'}
  public
    {: @exclude - standardowy konstruktor }
    constructor  Create();
    {: @exclude - standardowy destruktor }
    destructor  Destroy();                                           override;

    {: @abstract Wyświetla okno z paskiem postępu. }
    procedure  Show();

    {: @abstract Ukrywa (zamyka) okno z paskiem postępu. }
    procedure  Hide();

    {: @abstract Minimalna wartość paska postępu. }
    property  Min:  Integer         read fnMin         write SetMin         default 0;

    {: @abstract Maksymalna wartość paska postępu. }
    property  Max:  Integer         read fnMax         write SetMax         default 100;

    {: @abstract Bieżąca wartość paska postępu. }
    property  Position:  Integer    read fnPosition    write SetPosition    default 0;

    {: @abstract Tytuł okna. }
    property  Caption:  string      read fsCaption     write SetCaption;

    {: @abstract Tekst powyżej paska postępu. }
    property  Text:  string         read fsText        write SetText;

    {: @abstract Tekst poniżej paska postępu. }
    property  ExtraInfo:  string    read fsExtraInfo   write SetExtraInfo;

    {: @abstract Drugi wiersz tekstu poniżej paska postępu. }
    property  ExtraInfo2:  string   read fsExtraInfo2  write SetExtraInfo2;

    {: @abstract Sposób wyświetlania paska postępu. }
    property  Smooth:  Boolean      read fbSmooth      write SetSmooth      default False;

    {: @abstract Czy przycisk anulowania ma być widoczny. }
    property  ShowCancel:  Boolean  read fbShowCancel  write SetShowCancel  default False;

    {: @abstract Szerokość okna z paskiem postępu.
       Domyślnie: 80% szerokości ekranu }
    property  Width:  Integer       read fnWidth       write SetWidth;

    {: @abstract Informuje czy użytkownik anulował działania.

       Właściwość @name ma wartość @true, jeżeli użytkownik kliknął przycisk
       anulowania. }
    property  Cancelled:  Boolean   read GetCancelled;

  end { TGSkProgressForm };


implementation


uses
  GSkPasLib.PasLib;


{$REGION 'TGSkProgressForm'}

constructor  TGSkProgressForm.Create();
begin
  inherited;
  fnMax := 100
end { Create };


destructor  TGSkProgressForm.Destroy();
begin
  Hide();
  inherited
end { Destroy };


function  TGSkProgressForm.GetCancelled() : Boolean;
begin
  Application.ProcessMessages();
  Result := fbCancelled
end { GetCancelled };


procedure  TGSkProgressForm.Hide();
begin
  if  ffrmDialog <> nil  then
    begin
      ffrmDialog.Release();
      Application.ProcessMessages();
      ffrmDialog := nil;
      flblText := nil;
      fpbrProgress := nil;
      flblInfo := nil;
      flblInfo2 := nil;
      fbtnCancel := nil
    end { ffrmDialog <> nil }
end { Hide };


procedure  TGSkProgressForm.OnCancel(Sender:  TObject);
begin
  fbCancelled := True;
  fbtnCancel.Enabled := False
end { OnCancel };


procedure  TGSkProgressForm.SetCaption(const   sValue:  string);
begin
  fsCaption := sValue;
  if  ffrmDialog <> nil  then
    begin
      ffrmDialog.Caption := sValue;
      ffrmDialog.Update()
    end { ffrmDialog <> nil }
end { SetCaption };


procedure  TGSkProgressForm.SetExtraInfo(const   sValue:  string);
begin
  fsExtraInfo := sValue;
  if  flblInfo <> nil  then
    begin
      flblInfo.Caption := sValue;
      flblInfo.Update();
      ffrmDialog.Update()
    end { flblInfo <> nil }
end { SetExtraInfo };


procedure  TGSkProgressForm.SetExtraInfo2(const   sValue:  string);
begin
  fsExtraInfo2 := sValue;
  if  flblInfo2 <> nil  then
    begin
      flblInfo2.Caption := sValue;
      flblInfo2.Update();
      ffrmDialog.Update()
    end { flblInfo2 <> nil }
end { SetExtraInfo2 };


procedure  TGSkProgressForm.SetMax(const   nValue:  Integer);
begin
  fnMax := nValue;
  if  fpbrProgress <> nil  then
    begin
      fpbrProgress.Max := nValue;
      fpbrProgress.Update();
      ffrmDialog.Update()
    end { fpbrProgress <> nil }
end { SetMax };


procedure  TGSkProgressForm.SetMin(const   nValue:  Integer);
begin
  fnMin := nValue;
  if  fpbrProgress <> nil  then
    begin
      fpbrProgress.Min := nValue;
      fpbrProgress.Update();
      ffrmDialog.Update()
    end { fpbrProgress <> nil }
end { SetMin };


procedure  TGSkProgressForm.SetPosition(const   nValue:  Integer);
begin
  fnPosition := nValue;
  if  fpbrProgress <> nil  then
    begin
      fpbrProgress.Position := nValue;
      fpbrProgress.Update();
      ffrmDialog.Update()
    end { fpbrProgress <> nil }
end { SetPosition };


procedure  TGSkProgressForm.SetShowCancel(const   bValue:  Boolean);
begin
  fbShowCancel := bValue;
  if  fbtnCancel <> nil  then
    begin
      fbtnCancel.Visible := bValue;
      fbtnCancel.Update();
      ffrmDialog.Update();
    end { fbtnCancel <> nil }
end { SetShowCancel };


procedure  TGSkProgressForm.SetSmooth(const   bValue:  Boolean);
begin
  fbSmooth := bValue;
  if  fpbrProgress <> nil  then
    begin
      fpbrProgress.Smooth := bValue;
      fpbrProgress.SmoothReverse := bValue;
      fpbrProgress.Update();
      ffrmDialog.Update()
    end { fpbrProgress <> nil }
end { SetSmooth };


procedure  TGSkProgressForm.SetText(const   sValue:  string);
begin
  fsText := sValue;
  if  flblText <> nil  then
    begin
      flblText.Caption := sValue;
      flblText.Update();
      ffrmDialog.Update()
    end { flblText <> nil }
end { SetText };


procedure  TGSkProgressForm.SetWidth(const   nValue:  Integer);
begin
  fnWidth := nValue;
  if  ffrmDialog <> nil  then
    begin
      ffrmDialog.Width := nValue;
      ffrmDialog.Update()
    end { ffrmDialog <> nil }
end { SetWidth };


procedure  TGSkProgressForm.Show();
begin
  if  ffrmDialog <> nil  then
    ffrmDialog.Release();
  ffrmDialog := TForm.Create(nil);
  fnWidth := Screen.WorkAreaWidth * 8 div 10;
  ffrmDialog.Width := fnWidth;
  ffrmDialog.Height := 154;
  ffrmDialog.BorderStyle := bsToolWindow;
  ffrmDialog.BorderIcons := [];
  ffrmDialog.Position := poScreenCenter;
  ffrmDialog.Caption := fsCaption;
  {-}
  flblText := TLabel.Create(ffrmDialog);
  with  flblText  do
    begin
      Parent := ffrmDialog;
      Left := 12;
      Top := 12;
      Caption := fsText
    end { with flblText };
  flblInfo := TLabel.Create(ffrmDialog);
  with  flblInfo  do
    begin
      Parent := ffrmDialog;
      Left := 12;
      Top := 72;
      Caption := ''
    end { with flblInfo };
  flblInfo2 := TLabel.Create(ffrmDialog);
  with  flblInfo2  do
    begin
      Parent := ffrmDialog;
      Left := 12;
      Top := 85;
      Caption := ''
    end { with flblInfo2 };
  fpbrProgress := TProgressBar.Create(ffrmDialog);
  with  fpbrProgress  do
    begin
      Parent := ffrmDialog;
      SetBounds(12, 36, ffrmDialog.ClientWidth - 24{2*12}, 25);
      Min := fnMin;
      Max := fnMax;
      Smooth := fbSmooth;
      TabStop := False;
      SmoothReverse := fbSmooth;
      Position := fnPosition
    end { with fpbrProgress };
  fbtnCancel := TButton.Create(ffrmDialog);
  with  fbtnCancel  do
    begin
      Parent := ffrmDialog;
      SetBounds(ffrmDialog.ClientWidth - 87{75+12}, 82, 75, 25);
      TabStop := False;
      Caption := dgettext('GSkPasLib', 'Cancel');
      Visible := fbShowCancel;
      OnClick := OnCancel
    end { with fbtnCancel };
  {-}
  ffrmDialog.Show();
  ffrmDialog.Update()
end { Show };

{$ENDREGION 'TGSkProgressForm'}


end.
