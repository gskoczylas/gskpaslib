﻿{: @abstract Moduł @name rejestruje monitorowanie zdarzenia poprzez okna dialogowe.

   Moduł zawiera definicję obiektu @link(TGSkLogDialog). Jest to
   @link(GSkPasLib.Log.TGSkCustomLogAdapter adapter logowania) czyli obiekt pomocniczy
   dla komponentu @link(GSkPasLib.Log.TGSkLog).

   Ten adapter rejestruje informacje poprzez okna dialogowe. Zazwyczaj taki
   sposób monitorowania pracy aplikacji jest zbyt uciążliwy. Zdarzają się jednak
   sytuacje, gdy chcemy zawiesić działanie programu lub biblioteki podczas
   wykonywania monitorowanych działań. Wtedy przydać się może moduł @name.

   @seeAlso(GSkPasLib.Log)  }
unit  GSkPasLib.LogDialog;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Classes, SysUtils,
  GSkPasLib.Log;


type
  {: @abstract(@link(GSkPasLib.Log.TGSkCustomLogAdapter Adapter) rejestrujący
               informacje przy pomocy okien dialogowych.) }
  TGSkLogDialog = class(TGSkCustomLogAdapter)
  public
    {: @abstract Standardowy konstruktor.

       @param(AOwner Właściciel obiektu.

                     Jeżeli właścicielem obiektu @name jest ten sam obiekt, który
                     jest właścicielem obiektu @link(TGSkLog), obiekt @name zostanie
                     automatycznie @link(TGSkLog.AddLogAdapter dodany do listy
                     adapterów) obiektu @link(TGSkLog).

                     Jeżeli właścicielem obiektu @name jest obiekt @link(TGSkLog),
                     obiekt @name zostanie automatycznie @link(TGSkLog.AddLogAdapter
                     dodany do listy adapterów) obiektu @link(TGSkLog). Ponadto
                     z chwilą zwolnienia obiektu @link(TGSkLog) automatycznie
                     zwolniony zostanie również obiekt @name. ) }
    constructor  Create(AOwner:  TComponent);                        override;

    {: @exclude }
    destructor  Destroy();                                           override;

    {** TGSkCustomLogAdapter **}

    {: @abstract Rozpoczyna buforowanie informacji

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.BeginLog). W przypadku obiektu @className
       metoda @name nic nie robi. }
    procedure  BeginLog();                                           override;

    {: @abstract Rejestruje informację

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.LogMessage).

       W przypadku obiektu @className metoda @name wyświetla okno dialogowe
       z monitorowaną treścią. Do czasu zamknięcia tego okna działanie
       monitorowanego procesu jest wstrzymane. }
    procedure  LogMessage(const   Msg:  TGSkLogMessage);             override;

    {: @abstract Kończy buforowanie informacji

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.EndLog). W przypadku obiektu @className
       metoda @name nic nie robi. }
    procedure  EndLog();                                             override;

    {: @abstract Czyści (usuwa) zarejestrowane infromacje

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.Clear). W przypadku obiektu @className
       metoda @name nic nie robi. }
    procedure  Clear();                                              override;

  published
    {: @abstract Nazwa dziennika.

       Właściwość @name pozwala wskazać nazwę dziennika. W adapterze @className
       właściwość ta reprezentuje tytuł okna ialogowego.  }
    property  LogName;
  end { TGSkLogDialog };


implementation


uses
  GSkPasLib.Dialogs, GSkPasLib.StrUtils;


{ TGSkLogDialog }

procedure  TGSkLogDialog.BeginLog();
begin
  { OK }   // inherited;
end { BeginLog };


procedure  TGSkLogDialog.Clear();
begin
  { OK }   // inherited;
end { Clear };


constructor  TGSkLogDialog.Create(AOwner:  TComponent);
begin
  inherited;
end { Create };


destructor  TGSkLogDialog.Destroy();
begin
  inherited
end { Destroy };


procedure  TGSkLogDialog.EndLog();
begin
  { OK }   // inherited;
end { EndLog };


procedure  TGSkLogDialog.LogMessage(const   Msg:  TGSkLogMessage);

var
  nFlags:  Cardinal;

begin  { LogMessage }
  inherited;
  case  Msg.MessageType  of
    lmInformation:
      nFlags := MB_ICONINFORMATION;
    lmWarning:
      nFlags := MB_ICONWARNING;
    lmError:
      nFlags := MB_ICONERROR
    else
      nFlags := 0
  end { case Msg.MessageType };
  if  mfMemo in Msg.Flags  then
    MsgBox(Msg.Prefix + StringOfChar('-', 10) + #13#10
             + Msg.FormatLogTime() + #13#10
             + Msg.MessageText,
           LogName,
           nFlags or MB_OK or MB_SETFOREGROUND or MB_TOPMOST or MB_TASKMODAL)
  else
    MsgBox(Msg.Prefix
             + StringOfChar(' ', Msg.MessageOffset)
             + Msg.FormatLogTime()
             + #13#10 + CondQuoteStr(Msg.MessageText),
           LogName,
           nFlags or MB_OK or MB_SETFOREGROUND or MB_TOPMOST or MB_TASKMODAL)
end { LogMessage };


end.
