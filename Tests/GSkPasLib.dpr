﻿program  GSkPasLib;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


uses
  FastMM4 in 'FastMM\FastMM4.pas',
  FastMM4Messages in 'FastMM\FastMM4Messages.pas',
  madExcept,
  madLinkDisAsm,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  TestFrameWork,
  GUITestRunner,
  GSkPasLib.PasLib in '..\GSkPasLib.PasLib.pas',
  GSkPasLib.Tokens in '..\GSkPasLib.Tokens.pas',
  GSkPasLib.ASCII in '..\GSkPasLib.ASCII.pas',
  GSkPasLib.SystemUtils in '..\GSkPasLib.SystemUtils.pas',
  GSkPasLib.FileUtils in '..\GSkPasLib.FileUtils.pas',
  GSkPasLib.PasLibInternals in '..\GSkPasLib.PasLibInternals.pas',
  GSkPasLib.FormUtils in '..\GSkPasLib.FormUtils.pas',
  GSkPasLib.CustomComponents in '..\GSkPasLib.CustomComponents.pas',
  GSkPasLib.SynchroUtils in '..\GSkPasLib.SynchroUtils.pas',
  GSkPasLib.Log in '..\GSkPasLib.Log.pas',
  GSkPasLib.LogFile in '..\GSkPasLib.LogFile.pas',
  GSkPasLib.LogGExperts in '..\GSkPasLib.LogGExperts.pas',
  GSkPasLib.LogTraceTool in '..\GSkPasLib.LogTraceTool.pas',
  GSkPasLib.AppUtils in '..\GSkPasLib.AppUtils.pas',
  GSkPasLib.Dialogs in '..\GSkPasLib.Dialogs.pas',
  GSkPasLib.StrUtils in '..\GSkPasLib.StrUtils.pas',
  GSkPasLib.CRC in '..\GSkPasLib.CRC.pas',
  GSkPasLib.ComponentUtils in '..\GSkPasLib.ComponentUtils.pas',
  GSkPasLib.PropEdAboutForm in '..\GSkPasLib.PropEdAboutForm.pas' {PropEdAboutForm},
  GSkPasLib.Check in '..\GSkPasLib.Check.pas',
  GSkPasLib.PrevInst in '..\GSkPasLib.PrevInst.pas',
  GSkPasLib.Math in '..\GSkPasLib.Math.pas',
  GSkPasLib.DataEmbedded in '..\GSkPasLib.DataEmbedded.pas',
  GSkPasLib.SystemFirewall in '..\GSkPasLib.SystemFirewall.pas',
  GSkPasLib.Speaker in '..\GSkPasLib.Speaker.pas',
  GSkPasLib.ParamSaver in '..\GSkPasLib.ParamSaver.pas',
  GSkPasLib.XMLUtils in '..\GSkPasLib.XMLUtils.pas',
  GSkPasLib.CustomDataModule in '..\GSkPasLib.CustomDataModule.pas' {zzGSkCustomDataModule: TDataModule},
  GSkPasLib.CustomMainDataModule in '..\GSkPasLib.CustomMainDataModule.pas' {zzGSkCustomMainDataModule: TDataModule},
  GSkPasLib.DatabaseLoginDlg in '..\GSkPasLib.DatabaseLoginDlg.pas' {zzFormLoginDlg},
  GSkPasLib.MiscUtils in '..\GSkPasLib.MiscUtils.pas',
  GSkPasLib.CustomForm in '..\GSkPasLib.CustomForm.pas' {zzGSkCustomForm},
  GSkPasLib.Form in '..\GSkPasLib.Form.pas' {zzGSkForm},
  GSkPasLib.SpeedBarCtl in '..\GSkPasLib.SpeedBarCtl.pas',
  GSkPasLib.CustomMainForm in '..\GSkPasLib.CustomMainForm.pas' {zzGSkCustomMainForm},
  GSkPasLib.DbUtils in '..\GSkPasLib.DbUtils.pas',
  GSkPasLib.ClientDataSet in '..\GSkPasLib.ClientDataSet.pas',
  GSkPasLib.LogDialog in '..\GSkPasLib.LogDialog.pas',
  GSkPasLib.TimeUtils in '..\GSkPasLib.TimeUtils.pas',
  GSkPasLib.CmdLineOptionParser in '..\GSkPasLib.CmdLineOptionParser.pas',
  GSkPasLib.Thread in '..\GSkPasLib.Thread.pas',
  GSkPasLib.Classes in '..\GSkPasLib.Classes.pas',
  GSkPasLib.CSV in '..\GSkPasLib.CSV.pas',
  GSkPasLib.Registry in '..\GSkPasLib.Registry.pas',
  GSkPasLib.LogServiceInternals in '..\GSkPasLib.LogServiceInternals.pas',
  GSkPasLib.LogService in '..\GSkPasLib.LogService.pas',
  GSkPasLib.Pipes in '..\GSkPasLib.Pipes.pas',
  Test.GSkPasLib.Tokens in 'Test.GSkPasLib.Tokens.pas',
  Test.GSkPasLib.FileUtils in 'Test.GSkPasLib.FileUtils.pas',
  Test.GSkPasLib.CustomComponents in 'Test.GSkPasLib.CustomComponents.pas',
  yy_GSLog in 'yy_GSLog.pas',
  yy_DataEmbedded in 'yy_DataEmbedded.pas',
  yy_ComponentUtils in 'yy_ComponentUtils.pas',
  yy_StrUtils in 'yy_StrUtils.pas',
  Test.GSkPasLib.ParamSaver in 'Test.GSkPasLib.ParamSaver.pas',
  yy_TimeUtils in 'yy_TimeUtils.pas',
  Test.GSkPasLib.CSV in 'Test.GSkPasLib.CSV.pas',
  Test.GSkPasLib.Registry in 'Test.GSkPasLib.Registry.pas',
  Test.GSkPasLib.Pipes in 'Test.GSkPasLib.Pipes.pas';

  
{$R *.res}


begin
  Application.Initialize();  // <-- bez tego XML nie działa!
  Application.Title := 'GSkPasLib Test';
  Randomize();
  // ReportMemoryLeaksOnShutdown := True;
  GUITestRunner.RunTest(TestFramework.RegisteredTests)
end.
