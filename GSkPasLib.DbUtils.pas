﻿{: @abstract Podprogramy związane z bazami danych.

   Moduł @name zawiera procedury i funkcje związane z bazami danych. }
unit  GSkPasLib.DbUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  IBX.IBServices, IBX.IBUtils, SysUtils;


const
  TCP       = IBX.IBUtils.TCP;
  SPX       = IBX.IBUtils.SPX;
  NamedPipe = IBX.IBUtils.NamedPipe;
  Local     = IBX.IBUtils.Local;


type
  TProtocol = IBX.IBServices.TProtocol;


{: @abstract Dzieli ścieżkę do bazy danych na serwer, protokół i plik.

   Procedura @name dzieli ścieżkę do bazy danych na adres lub nazwę serwera,
   stosowany protokół i plik lub alias bazy danych.

   @param sDatabasePath Analizowana ścieżka do bazy danych.
   @param sHostName Nazwa lub adres serwera, lub pusty napis.
   @param Protocol Protokół połączenia z bazą danych.
   @param sFileName Ścieżka do pliku bazy danych lub alias bazy danych.

   @seeAlso(ExtractDbHost)
   @seeAlso(ExtractDbProtocol)
   @seeAlso(ExtractDbFileName)
   @seeAlso(MakeDbPath)  }
procedure  ProcessDbPath(const   sDatabasePath:  string;
                         out     sHostName:  string;
                         out     Protocol:  TProtocol;
                         out     sFileName:  string);

{: @abstract Zwraca adres lub nazwę serwera.

   Wynikiem funkcji @name jest adres lub nazwa komputera zawierającego serwer
   Firebird lub InterBase. Jeżeli w ścieżce nie ma wskazanego serwera to wynikiem
   funkcji jest pusty napis.

   @seeAlso(ExtractDbProtocol)
   @seeAlso(ExtractDbFileName)
   @seeAlso(ProcessDbPath)  }
function  ExtractDbHost(const   sDatabasePath:  string)
                        : string;

{: @abstract Zwraca protokół połączenia z bazą danych.

   Wynikiem funkcji @name jest protokół używany do połączenia z bazą danych.
   Jeżeli parametr wskazuje wyłącznie ścieżkę do pliku lub alias bazy danych to
   wynikiem funkcji jest @link(Local).

   @seeAlso(ExtractDbHost)
   @seeAlso(ExtractDbFileName)
   @seeAlso(ProcessDbPath)  }
function  ExtractDbProtocol(const   sDatabasePath:  string)
                            : TProtocol;

{: @abstract Zwraca ścieżkę do pliku lub alias bazy danych.

   Wynikiem funkcji @name jest ścieżka do pliku lub alias bazy danych.

   @seeAlso(ExtractDbHost)
   @seeAlso(ExtractDbProtocol)
   @seeAlso(ProcessDbPath)  }
function  ExtractDbFileName(const   sDatabasePath:  string)
                            : string;

{: @abstract Tworzy z dostarczonych elementów ścieżkę do bazy.

   Funkcja @name zwraca ścieżkę do bazy danych utworzoną z danych dostarczonych
   przez parametry.

   @param(sHost Adres lub nazwa komputera, lub pusty napis --- dla połączeń
                lokalnych. )
   @param(Prot  Protokół połączenia z bazą danych. )
   @param(sFile Ścieżka do pliku lub alias bazy danych. )

   @seeAlso(ProcessDbPath)  }
function  MakeDbPath(const   sHost:  string;
                     const   Prot:   TProtocol;
                     const   sFile:  string)
                     : string;

implementation


procedure  ProcessDbPath(const   sDatabasePath:  string;
                         out     sHostName:  string;
                         out     Protocol:  TProtocol;
                         out     sFileName:  string);
var
  sBuf:  string;
  nPos:  Integer;

begin  { ProcessDbPath }
  if  Copy(sDatabasePath, 1, 2) = '\\'  then
    begin
      sBuf := Copy(sDatabasePath, 3, MaxInt);
      nPos := Pos('\', sBuf);
      if  nPos <> 0  then
        begin
          sHostName := Copy(sBuf, 1, nPos - 1);
          Protocol := NamedPipe;
          sFileName := Copy(sBuf, nPos + 1, MaxInt);
          Exit
        end { nPos <> 0 };
    end { Copy(..., 1, 2) = '\\' };
  nPos := Pos('@', sDatabasePath);
  if  nPos <> 0  then
    begin
      sHostName := Copy(sDatabasePath, 1, nPos - 1);
      Protocol := SPX;
      sFileName := Copy(sDatabasePath, nPos + 1, MaxInt);
      Exit
    end { nPos <> 0 };
  nPos := Pos(':', sDatabasePath);
  if  not(nPos in [0, 2])  then
    begin
      sHostName := Copy(sDatabasePath, 1, nPos - 1);
      Protocol := TCP;
      sFileName := Copy(sDatabasePath, nPos + 1, MaxInt);
      Exit
    end { nPos <> 0  and  nPos <> 2 };
  sHostName := '';
  Protocol := Local;
  sFileName := sDatabasePath
end { ProcessDbPath };


function  ExtractDbHost(const   sDatabasePath:  string)
                        : string;
var
  lProt:  TProtocol;
  sFile:  string;

begin  { ExtractDbHost }
  ProcessDbPath(sDatabasePath, Result, lProt, sFile)
end { ExtractDbHost };


function  ExtractDbProtocol(const   sDatabasePath:  string)
                            : TProtocol;
var
  sDummy:  string;

begin  { ExtractDbProtocol }
  ProcessDbPath(sDatabasePath, sDummy, Result, sDummy)
end { ExtractDbProtocol };


function  ExtractDbFileName(const   sDatabasePath:  string)
                            : string;
var
  sHost:  string;
  lProt:  TProtocol;

begin  { ExtractDbFileName }
  ProcessDbPath(sDatabasePath, sHost, lProt, Result)
end { ExtractDbFileName };


function  MakeDbPath(const   sHost:  string;
                     const   Prot:   TProtocol;
                     const   sFile:  string)
                     : string;
var
  sFmt:  string;

begin  { MakeDbPath }
  Assert((Prot = Local) = (sHost = ''));
  case  Prot  of
    TCP:
      sFmt := '%s:%s';
    SPX:
      sFmt := '%s@%s';
    NamedPipe:
      sFmt := '\\%s\%s';
    else
      sFmt := '%s%s'
  end { case Prot };
  Result := Format(sFmt, [sHost, sFile])
end { MakeDbPath };


end.
