﻿unit  GSkPasLib.ODBC;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  SysUtils, Classes;


{: @abstract Informuje, czy jest zdefiniowane połączenie ODBC.

   Funkcja @name informuje, czy jest zdefiniowane połączenie ODBC o wskazanej
   nazwie. W progamach skompilowanych jako Win32 sprawdza połączenia ODBC32,
   a w programach skompilowanych jako Win64 - połączenia ODBC64). }
function  IsDSNDefined(const   sName:  string)
                       : Boolean;

{: @abstract Zwraca listę zdefiniowanych połączeń ODBC.

   Funkcja @name zwraca listę zdefiniowanych połączeń ODBC. W programach
   skompilowanych jako Win32 zwraca listę połączeń ODBC32, a w programach
   skompilowanych jako Win64 - listę połączeń ODBC64.

   @param(List Lista zdefiniowanych połączeń ODBC.)
   @param (sSNType Jeżeli parametr nie jest pustym napisem, to zwraca tylko
                   połączenie ODBC, którego opis jest taki sam, jak w tym
                   parametrze (wielkość liter nie ma znaczenia).) }
procedure  GetDSNList(const   List:  TStrings;
                      const   sSNType:  string = '');


implementation


const
  SQL_MAX_DSN_LENGTH           =  32;
  SQL_MAX_OPTION_STRING_LENGTH = 256;
  {-}
  SQL_FETCH_FIRST = 2;
  SQL_FETCH_NEXT  = 1;
  {-}
  SQL_SUCCESS =   0;
  SQL_NO_DATA = 100;


type
  SQLSMALLINT  = SmallInt;
  SQLUSMALLINT = Word;
  SQLCHAR      = AnsiChar;
  PSQLCHAR     = PAnsiChar;
  SQLHANDLE    = Pointer;
  SQLHENV      = SQLHANDLE;
  SQLRETURN    = SQLSMALLINT;


{$REGION 'External subroutines'}

function  SQLAllocEnv(var   EnvironmentHandle:  SQLHENV)
                      : SQLRETURN;                                   stdcall;
    external 'odbc32.dll';


function  SQLFreeEnv(EnvironmentHandle:  SQLHENV)
                     : SQLRETURN;                                    stdcall;
    external 'odbc32.dll';


function  SQLDataSources(EnvironmentHandle:  SQLHENV;
                         Direction:  SQLUSMALLINT;
                         ServerName:  PSQLCHAR;
                         BufferLength1:  SQLSMALLINT;
                         var   NameLength1:  SQLSMALLINT;
                         Description:  PSQLCHAR;
                         BufferLength2:  SQLSMALLINT;
                         var   NameLength2:  SQLSMALLINT)
                         : SQLRETURN;                                stdcall;
    external 'odbc32.dll';

{$ENDREGION 'External subroutines'}


{$REGION 'Public subroutines'}

function  IsDSNDefined(const   sName:  string)
                       : Boolean;
var
  lBuf:  TStringList;

begin  { IsDSNDefined }
  lBuf := TStringList.Create();
  try
    GetDSNList(lBuf);
    Result := lBuf.IndexOf(sName) >= 0
  finally
    lBuf.Free()
  end { try-finally }
end { IsDSNDefined };


procedure  GetDSNList(const   List:  TStrings;
                      const   sSNType:  string);
var
  hEnvironment:  SQLHANDLE;
  lBuf1:  array[0..SQL_MAX_DSN_LENGTH] of SQLCHAR;
  lBuf2: array[0..SQL_MAX_OPTION_STRING_LENGTH] of SQLCHAR;
  nLen1, nLen2:  SQLSMALLINT;
  sServerName,  sDescription:  string;

begin  { GetDSNList }
  if  SQLAllocEnv(hEnvironment) = SQL_SUCCESS  then
    try
      if SQLDataSources(hEnvironment, SQL_FETCH_FIRST,
                        lBuf1, SizeOf(lBuf1), nLen1,
                        lBuf2, SizeOf(lBuf2), nLen2) = SQL_SUCCESS then
        repeat
          SetString(sServerName, lBuf1, nLen1);
          SetString(sDescription, lBuf2, nLen2);
          if  (sSNType = '')
              or SameText(sSNType, sDescription)  then
            List.Add(sServerName);
        until  SQLDataSources(hEnvironment, SQL_FETCH_NEXT,
                              lBuf1, SizeOf(lBuf1), nLen1,
                              lBuf2, SizeOf(lBuf2), nLen2) = SQL_NO_DATA;
    finally
      SQLFreeEnv(hEnvironment)
    end { try-finally }
end { GetDSNList };

{$ENDREGION 'Public subroutines'}


end.
