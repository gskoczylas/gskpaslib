﻿{: @abstract Pozostałe podprogramy.

   Moduł @name zawiera definicje funkcji i procedur, które trudno było
   zakwalifikować do któregokolwiek z „tematycznych” modułów.  }
unit  GSkPasLib.MiscUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


{$warn  SYMBOL_PLATFORM off }


interface


uses
  {$IF CompilerVersion > 22}
    System.SysUtils;
  {$ELSE}
    SysUtils, Windows;
  {$IFEND}


var
  {: @abstract Parametry formatowania niezależne od ustawień regionalnych.

     Zmienna @name zawiera parametry formatowania (daty, czasu, liczb) niezależne
     od bieżących ustawień regionalnych. Można ją użyć jako parametr standardowych
     funkcji, takich jak @code(DateToStr), @code(StrToDateTime), itp. }
  gInternalFormatSettings:  TFormatSettings;


{: @abstract Czy program działa pod kontrolą Delphi?

   @returns(@true jeżeli program działa pod kontrolą Delphi. @br
            @false jeżeli program działa samodzielnie.) }
function  IsInsideDelphi() : Boolean;


implementation


function  GetInternalFormatSettings() : TFormatSettings;
begin
  {$IF CompilerVersion >= 22}  // XE
    Result := FormatSettings;
  {$ELSE}
    GetLocaleFormatSettings(GetThreadLocale(), Result);
  {$IFEND}
  Result.ShortDateFormat := 'yyyy-mm-dd';
  Result.LongTimeFormat := 'hh:nn:ss.zzz';
  Result.DateSeparator := '-';
  Result.TimeSeparator := ':';
  Result.DecimalSeparator := '.'
end { GetInternalFormatSettings };


function  IsInsideDelphi() : Boolean;
begin
  Result := DebugHook <> 0
end { IsInsideDelphi };


initialization
  gInternalFormatSettings := GetInternalFormatSettings();


end.
