﻿unit  Test.GSkPasLib.Generics;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}


uses
  TestFramework, GSkPasLib.Generics;


type
  TGenerics_Test = class(TTestCase)
//   public
//     procedure  SetUp();                                              override;
//     procedure  TearDown();                                           override;
  published
    procedure  TestSet1B();
    procedure  TestSet2B();
  end { TGenerics_Test };


implementation


type
  T1BEnumA = (One, Two, Three, Four);
  T1BSetA  = set of T1BEnumA;
  T1BEnumB = (item1, item2, item3, item4, item5, item6, item7, item8);
  T1BSetB  = set of T1BEnumB;
  T1BEnumC = (Jeden = 1, Dwa, Trzy, Siedem=7, Osiem);
  T1BSetC  = set of T1BEnumC;
  T2BEnum  = (Uno, Dos, Tres, Cuatro, Cinco, Seis, Siete, Ocho, Nueve, Diez);
  T2BSet   = set of T2BEnum;
//   TLargeEnum = (OPOS_SUCCESS,
//                 OPOS_EFPTR_COVER_OPEN = 201,
//                 OPOS_EFPTR_JRN_EMPTY, OPOS_EFPTR_REC_EMPTY, OPOS_EFPTR_SLP_EMPTY,
//                 OPOS_EFPTR_SLP_FORM, OPOS_EFPTR_MISSING_DEVICES, OPOS_EFPTR_WRONG_STATE,
//                 OPOS_EFPTR_TECHNICAL_ASSISTANCE, OPOS_EFPTR_CLOCK_ERROR, OPOS_EFPTR_FISCAL_MEMORY_FULL,
//                 OPOS_EFPTR_FISCAL_MEMORY_DISCONNECTED, OPOS_EFPTR_FISCAL_TOTALS_ERROR,
//                 OPOS_EFPTR_BAD_ITEM_QUANTITY, OPOS_EFPTR_BAD_ITEM_AMOUNT, OPOS_EFPTR_BAD_ITEM_DESCRIPTION,
//                 OPOS_EFPTR_RECEIPT_TOTAL_OVERFLOW, OPOS_EFPTR_BAD_VAT, OPOS_EFPTR_BAD_PRICE,
//                 OPOS_EFPTR_BAD_DATE, OPOS_EFPTR_NEGATIVE_TOTAL, OPOS_EFPTR_WORD_NOT_ALLOWED,
//                 OPOS_EFPTR_BAD_LENGTH, OPOS_EFPTR_MISSING_SET_CURRENCY, OPOS_EFPTR_DAY_END_REQUIRED);
//   TLargeSet  = set of TLargeEnum;


{$REGION 'TGenerics_Test'}

// procedure  TGenerics_Test.TestLargeSet();
// begin
//   CheckEquals('[]', G.SetToStr<TLargeSet>([]), 'Empty large set');
//   CheckEquals('[SUCCESS, EFPTR_DAY_END_REQUIRED]',
//               G.SetToStr<TLargeSet>([OPOS_SUCCESS, OPOS_EFPTR_DAY_END_REQUIRED],
//               'OPOS'),
//               'Large set #1');
//   CheckEquals('[OPOS_SUCCESS, DAY_END_REQUIRED]',
//               G.SetToStr<TLargeSet>([OPOS_SUCCESS, OPOS_EFPTR_DAY_END_REQUIRED],
//               'OPOS_EFPTR'),
//               'Large set #2');
// end { TestLargeSet };


procedure  TGenerics_Test.TestSet1B();
begin
  CheckEquals('[]', G.SetToStr<T1BSetA>([]), 'Empty set 1B #1');
  CheckEquals('[One, Four]', G.SetToStr<T1BSetA>([One, Four]), 'Set 1B #1');
  CheckEquals('[1, 8]', G.SetToStr<T1BSetB>([item1, Item8], 'item'), 'Set 1B #2');
  CheckEquals('[set-of-T1BSetC]', G.SetToStr<T1BSetC>([Jeden, Dwa]), 'Set 1B #3');
  CheckEquals('[set-of-T1BSetC]', G.SetToStr<T1BSetC>([Jeden, Trzy, Siedem, Osiem]), 'Set 1B #4');
  CheckEquals('[set-of-T1BSetC]', G.SetToStr<T1BSetC>([Jeden..Osiem]), 'Set 1B #5')
end { TestSet1B };


procedure  TGenerics_Test.TestSet2B;
begin
  CheckEquals('[]', G.SetToStr<T2BSet>([]), 'Empty set 2B');
  CheckEquals('[Uno, Diez]', G.SetToStr<T2BSet>([Uno, Diez]), 'Set 2B')
              // Uno, Dos, Tres, Cuatro, Cinco, Seis, Siete, Ocho, Nueve, Diez
end { TestSet2B };

{$ENDREGION 'TGenerics_Test'}


initialization
  RegisterTest(TGenerics_Test.Suite());



end.
