﻿{: @abstract Rejestrowanie obiektów XML.

   Moduł @name zawiera definicję klasy pomocniczej dla klasy @link(TGSkLog).
   Po umieszczeniu go w sekcji @code(uses) razem z modułem @link(GSkPasLib.Log) możliwe
   jest wygodne monitorowanie obiektów XML. }
unit  GSkPasLib.LogXML;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    System.Classes, System.TypInfo, XML.XMLIntf,
  {$ELSE}
    Classes, TypInfo, XMLIntf,
  {$IFEND}
  GSkPasLib.Log;


type
  {: @abstract Rozszerza klasę @link(TGSkLog) o monitorowanie obiektów XML. }
  TGSkLogHlpXML = class helper for TGSkLog
  public

    {: @abstract Rejestruje wskazany węzeł XML.

       Ta wersja metody przeznaczona jest do rejestrowania dowolnych węzłów XML.

       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   iValue:  IXMLNode;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestruje wskazany dokument XML.

       Ta wersja metody przeznaczona jest do rejestrowania dokumentów XML.

       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   ixmlValue:  IXMLDocument;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;
  end { TGSkLogHlpXML };


implementation


uses
  GSkPasLib.StrUtils;


{$REGION 'TGSkLogHlpXML'}

function  TGSkLogHlpXML.LogValue(const   sName:  string;
                                 const   iValue:  IXMLNode;
                                 const   lmMsgType:  TGSkLogMessageType)
                                 : TGSkLog;
var
  bLogTime:  Boolean;
  lBuf:  TStringList;

begin  { LogValue }
  if  iValue = nil  then
    Result := LogMessage(sName + ' = nil', lmMsgType)
  else
    begin
      BeginLog();
      try
        LogEnter(sName + ' = XML Node(', [iValue.NodeName], lmMsgType);
        bLogTime := LogMessageTime;
        LogMessageTime := False;
        try
          if  iValue.OwnerDocument.FileName <> ''  then
            LogMessage('OwnerDocument = %s',
                       [QuoteStr(iValue.OwnerDocument.FileName, '"')],
                       lmMsgType);
          if  iValue.ParentNode <> nil  then
            LogMessage('ParentNode = <%s>', [iValue.ParentNode.NodeName], lmMsgType);
          if  iValue.LocalName <> ''  then
            LogValue('LocalName', iValue.LocalName, lmMsgType);
          if  iValue.IsTextElement  then
            LogValue('Text', iValue.Text, lmMsgType);
          if  iValue.Prefix <> ''  then
            LogValue('Prefix', iValue.Prefix, lmMsgType);
          try
            Result := LogMessage('NodeType = ' + GetEnumName(TypeInfo(TNodeType), Ord(iValue.NodeType)),
                                 lmMsgType)
          except
            Result := LogMessage('NodeType = [%d]', [Ord(iValue.NodeType)],
                                 lmMsgType)
          end { try-except };
          lBuf := TStringList.Create();
          try
            lBuf.Text := iValue.XML;
            LogValue('XML', lBuf, lmMsgType)
          finally
            lBuf.Free()
          end { try-finally };
          LogExit(') ' + iValue.NodeName, lmMsgType)
        finally
          LogMessageTime := bLogTime
        end { try-finally };
      finally
        EndLog()
      end { try-finally }
    end { iValue <> nil }
end { LogValue };


function  TGSkLogHlpXML.LogValue(const   sName:  string;
                                 const   ixmlValue:  IXMLDocument;
                                 const   lmMsgType:  TGSkLogMessageType)
                                 : TGSkLog;
var
  bLogTime:  Boolean;

begin  { LogValue }
  if  ixmlValue = nil  then
    Result := LogMessage(sName + ' = nil', lmMsgType)
  else
    begin
      BeginLog();
      try
        LogEnter(sName + ' = XML Document(', lmMsgType);
        bLogTime := LogMessageTime;
        LogMessageTime := False;
        try
          LogValue('IsEmptyDoc', ixmlValue.IsEmptyDoc(), lmMsgType);
          if  ixmlValue.Encoding <> ''  then
            LogValue('Encoding', ixmlValue.Encoding, lmMsgType);
          if  ixmlValue.FileName <> ''  then
            LogMessage('FileName = %s',
                       [QuoteStr(ixmlValue.FileName, '"')],
                       lmMsgType);
          LogValue('Modified', ixmlValue.Modified, lmMsgType);
          if  ixmlValue.Version <> ''  then
            LogValue('Version', ixmlValue.Version, lmMsgType);
          Result := LogValue('XML', ixmlValue.XML, lmMsgType);
          LogExit(')', lmMsgType)
        finally
          LogMessageTime := bLogTime
        end { try-finally }
      finally
        EndLog()
      end { try-finally }
    end { ixmlValue <> nil }
end { LogValue };

{$ENDREGION 'TGSkLogHlpXML'}


end.
