﻿{: @exclude
   Wewnętrznych modułów biblioteki nie komentuję (na razie).  }
unit  GSkPasLib.PropEdAboutForm;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Windows, SysUtils, DateUtils, Controls, StdCtrls, Buttons, Graphics,
  ExtCtrls, Classes, Dialogs, IniFiles, Forms, Math, Messages,
  JvLabel, JvJCLUtils, JvAnimatedImage, JvExControls;


type
  TPropEdAboutForm = class(TForm)
    imAuthor:       TImage;
    lbTitleShadow:  TLabel;
    lbTitle:        TLabel;
    lbAddress:      TLabel;
    lbAuthorName:   TLabel;
    bvLine:         TBevel;
    btOK:           TButton;
    lbCopyRight:    TLabel;
    lbRights:       TLabel;
    btSelDir:       TSpeedButton;
    btHelp:         TSpeedButton;
    dlgOpen:        TOpenDialog;
    imLogo:         TJvAnimatedImage;
    lbMail:         TJvLabel;
    procedure  FormCreate(Sender:  TObject);
    procedure  FormShow(Sender:  TObject);
    procedure  FormMouseDown(Sender:  TObject;
                             Button:  TMouseButton;
                             Shift:  TShiftState;
                             X, Y:  Integer);
    procedure  btHelpClick(Sender:  TObject);
    procedure  btSelDirClick(Sender:  TObject);
    procedure  CheckHelp();
  private
    FHelpFile, FHelpDir:  TFileName;
    FCompClass:  string;
    procedure  LoadOptions();
    procedure  SaveOptions();
  public
    class procedure  Execute(const   sClassName:  string);
  end { TPropEdAboutForm };


// var
//   PropEdAboutForm: TPropEdAboutForm;


implementation


uses
  GSkPasLib.PasLibInternals;


{$R *.dfm}


const
  csIniFileName = gcsLibName + '.ini';
  csOptions     = 'Opcje';
  csLeft        = 'Od.lewej';
  csTop         = 'Od.góry';
  csHelpFile    = 'Plik.pomocy';
  csStdHelpFile = gcsStdHelpDir + PathDelim + gcsStdHelpFile;
  csHintHlpComp = 'Wyświetl Pomoc do komponentu „%s”';


{ TPropEdAboutForm }

class procedure  TPropEdAboutForm.Execute(const   sClassName:  string);
begin
  with  Self.Create(Application)  do
    try
      FCompClass := sClassName;
      LoadOptions();
      ShowModal();
      SaveOptions()
    finally
      Free()
    end { try-finally }
end { Execute };


procedure  TPropEdAboutForm.LoadOptions();
begin
  with  TIniFile.Create(ExtractFilePath(Application.ExeName) + csIniFileName)  do
    try
      Left := EnsureRange(ReadInteger(csOptions, csLeft, Left),
                          Screen.DesktopLeft,
                          Screen.DesktopWidth - Width);
      Top := EnsureRange(ReadInteger(csOptions, csTop, Top),
                         Screen.DesktopTop,
                         Screen.DesktopHeight - Height);
      FHelpFile := ReadString(csOptions, csHelpFile, FHelpFile);
      if  (FHelpFile = '')
          or not FileExists(FHelpFile)  then
        begin
          FHelpFile := csStdHelpFile;
          if  not FileExists(FHelpFile)  then
            FHelpFile := ''
        end ;
      FHelpDir := ExtractFileDir(FHelpFile);
      FHelpFile := ExtractFileName(FHelpFile);
      CheckHelp()
    finally
      Free()
    end { try-finally }
end { LoadOptions };


procedure  TPropEdAboutForm.SaveOptions();
begin
  with  TIniFile.Create(ExtractFilePath(Application.ExeName) + csIniFileName)  do
    try
      WriteInteger(csOptions, csLeft, Left);
      WriteInteger(csOptions, csTop, Top);
      if  FHelpFile <> ''  then
        WriteString(csOptions, csHelpFile,
                    IncludeTrailingPathDelimiter(FHelpDir) + FHelpFile)
      else
        DeleteKey(csOptions, csHelpFile)   // FHelpFile = ''
    finally
      Free()
    end { try-finally }
end { SaveOptions };


procedure  TPropEdAboutForm.FormCreate(Sender:  TObject);
begin
  with  lbCopyRight  do
    Caption := Format(Caption, [YearOf(Date())]);
  Left := Screen.DesktopLeft
          + (Screen.DesktopWidth - Width) div 2;
  Top := Screen.DesktopTop
         + (Screen.Height - Height) div 2;
  if  FileExists(csStdHelpFile)  then
    begin
      FHelpDir := gcsStdHelpDir;
      FHelpFile := gcsStdHelpFile
    end
end { FormCreate };


procedure  TPropEdAboutForm.FormShow(Sender:  TObject);
begin
  CheckHelp()
end { FormShow };


procedure  TPropEdAboutForm.FormMouseDown(Sender:  TObject;
                                          Button:  TMouseButton;
                                          Shift:  TShiftState;
                                          X, Y:  Integer);
begin
  ReleaseCapture();
  Perform(WM_SYSCOMMAND, SC_MOVE + 2, 0)
  { Powyższe instrukcje powodują, że okno można przesuwać myszą w dowolnym
    miejscu, za wyjątkiem przycisków.  }
end { FormMouseDown };


procedure  TPropEdAboutForm.btHelpClick(Sender:  TObject);
begin
  Exec(FHelpFile, '', FHelpDir)
end { btHelpClick };


procedure  TPropEdAboutForm.btSelDirClick(Sender:  TObject);
begin
  dlgOpen.InitialDir := FHelpDir;
  dlgOpen.FileName := FHelpFile;
  if  dlgOpen.Execute()  then
    begin
      FHelpFile := ExtractFileName(dlgOpen.FileName);
      FHelpDir := ExtractFileDir(dlgOpen.FileName);
      CheckHelp()
    end { dlgOpen.Execute() }
end { btSelDirClick };


procedure  TPropEdAboutForm.CheckHelp();
begin
  btHelp.Enabled := (FHelpFile <> '')
                    and FileExists(IncludeTrailingPathDelimiter(FHelpDir)
                                     + FHelpFile)
end { CheckHelp };


end.
