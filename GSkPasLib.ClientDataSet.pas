﻿{: @abstract Definicja komponentu @link(TGSkClientDataSet).

   Moduł @name zawiera definicję komponentu @link(TGSkClientDataSet). Jest to
   komponent dodający dodatkowe możliwości do standardowego komponentu
   @code(TClientDataSet).  }
unit  GSkPasLib.ClientDataSet;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  DBClient, Classes, Variants, DB, Math, SysUtils;


type
  {: @exclude }
  TGSkClientDataSetContext = record
    fbFiltered:         Boolean;
    fbLogChanges:       Boolean;
    fsFiler:            string;
    fsIndexFieldNames:  string;
    fsIndexName:        string;
    fnRecNo:            Integer
  end { TGSkClientDataSetContext };

  {: @abstract Dodatkowe możliwości standardowego komponentu @code(TClientDataSet).

     Komponent @name umożliwia zapamiętanie i przywrócenie kontekstu. Zapamiętywane
     są następujące cechy: @unorderedList(
       @item(Flaga @code(Filtered),)
       @item(Właściwość @code(Filter),)
       @item(Nazwa bieżącego indeksu --- właściwość @code(IndexName),)
       @item(Bieżące pola sortowania --- właściwość @code(IndexFieldNames).)
     )   }
  TGSkClientDataSet = class(TClientDataSet)
  private
    var
      fContexts:  array of TGSkClientDataSetContext;
      fValues:  Variant;
      fbStreamedActive:  Boolean;
      fbAllowStreamedActive:  Boolean;
      fbLogChanges:  Boolean;
    function  GetContextLevel() : Integer;
    function  InternalSaveContext() : Integer;
    procedure  SetLogChanges(const   bValue:  Boolean);
  protected
    {: @exclude }
    procedure  DoOnNewRecord();                                      override;
    {: @exclude }
    procedure  Loaded();                                             override;
    {: @exclude }
    procedure  SetActive(Value:  Boolean);                           override;

    {: @abstract Sprawdza czy komponent jest w stanie @code(dsBrowse).

       Metoda @name jest wykorzystywana wewnętrznie do sprawdzenia czy właściwość
       @code(State) komponentu ma wartość @code(dsBrowse).  }
    procedure  CheckIfBrowseMode();

    {: @abstract Właściwość używana wewnętrznie.

       Właściwość @name używana jest wewnętrznie aby zapewnić otwieranie źródła
       danych w odpowiednim momencie, jeżeli spełnione są wymagane warunki.

       @seeAlso(AllowStreamedActive)  }
    property  StreamedActive:  Boolean
              read  fbStreamedActive
              write fbStreamedActive;
  public
    {: @exclude }
    constructor  Create(AOwner:  TComponent);                        override;
    {: @exclude }
    destructor  Destroy();                                           override;

    {: @abstract Zapamiętuje bieżący kontekst.

       Każde wywołanie metody @name powoduje zapamiętanie bieżącego kontekstu.
       Można go później odtworzyć przy pomocy metody @link(RestoreContext).

       @param(sNewIndex     Wskazuje nazwę nowego indeksu.)
       @param(sNewFilter    Nowy filtr. Jeżeli jest niepustym napisem to
                            definiowany jest odpowiedni filtr i ustawiona zostaje
                            właściwość @code(Filtered). Jeżeli jest pustym napisem
                            to dotychczasowy filtr oraz właściwość @code(Filtered)
                            pozostaje bez zmian.)
       @param(bGotoFirstRec Jeżeli wskazano również nazwę nowego indeksu to po
                            zmianie indeksu przechodzi do pierwszego rekordu.)

       @returns(Wynikiem funkcji jest poziom kontekstu czyli liczba wywołań
                metody @name nie zakończonych wywołaniem metody
                @link(RestoreContext).)

       @seeAlso(ContextLevel)  }
    function  SaveContext(const   sNewIndex:  string = '';
                          const   sNewFilter:  string = '';
                          const   bGotoFirstRec:  Boolean = True)
                          : Integer;                                 overload;

    {: @abstract Zapamiętuje bieżący kontekst.

       Każde wywołanie metody @name powoduje zapamiętanie bieżącego kontekstu.
       Można go później odtworzyć przy pomocy metody @link(RestoreContext).

       @param(sNewIndex     Wskazuje nazwę nowego indeksu.)
       @param(bClearFilter  Jeżeli ten parametr jest ustawiony oraz wskazano
                            również nazwę nowego indeksu to właściwość
                            @code(Filtered) przyjmuje wartość @false.)
       @param(bGotoFirstRec Jeżeli wskazano również nazwę nowego indeksu to po
                            zmianie indeksu przechodzi do pierwszego rekordu.)

       @returns(Wynikiem funkcji jest poziom kontekstu czyli liczba wywołań
                metody @name nie zakończonych wywołaniem metody
                @link(RestoreContext).)

       @seeAlso(ContextLevel)
       @seeAlso(RestoreContext)
       @seeAlso(DropContext)  }
    function  SaveContext(const   sNewIndex:  string;
                          const   bClearFilter:  Boolean;
                          const   bGotoFirstRec:  Boolean = True)
                          : Integer;                                 overload;

    {: @abstract Zapamiętuje bieżący kontekst.

       Każde wywołanie metody @name powoduje zapamiętanie bieżącego kontekstu.
       Można go później odtworzyć przy pomocy metody @link(RestoreContext).

       @param(sNewIndex  Wskazuje nazwę nowego indeksu.)
       @param(nRecNo     Numer rekordu, do którego należy przejść po wybraniu
                         nowego indeksu.

                         @bold(Uwaga!) Numer rekordu zależy od aktualnie
                         obowiązującego warunku filtrowania.)
       @param(sNewFilter Wskazuje nowy warunek filtrowania.)

       @returns(Wynikiem funkcji jest poziom kontekstu czyli liczba wywołań
                metody @name nie zakończonych wywołaniem metody
                @link(RestoreContext).)

       @seeAlso(ContextLevel)
       @seeAlso(RestoreContext)
       @seeAlso(DropContext)  }
    function  SaveContext(const   sNewIndex:  string;
                          const   nRecNo:  Integer;
                          const   sNewFilter:  string = '')
                          : Integer;                                 overload;

    {: @abstract Zapamiętuje bieżący kontekst.

       Każde wywołanie metody @name powoduje zapamiętanie bieżącego kontekstu.
       Można go później odtworzyć przy pomocy metody @link(RestoreContext).

       @param(nRecNo     Numer rekordu, do którego należy przejść po wybraniu
                         nowego indeksu.

                         @bold(Uwaga!) Numer rekordu zależy od aktualnie
                         obowiązującego warunku filtrowania. )
       @param(sNewFilter Wskazuje nowy warunek filtrowania.)

       @returns(Wynikiem funkcji jest poziom kontekstu czyli liczba wywołań
                metody @name nie zakończonych wywołaniem metody
                @link(RestoreContext).)

       @seeAlso(ContextLevel)
       @seeAlso(RestoreContext)
       @seeAlso(DropContext)  }
    function  SaveContext(const   nRecNo:  Integer;
                          const   sNewFilter:  string = '')
                          : Integer;                                 overload;

    {: @abstract Odtwarza zapamiętany kontekst.

       Każde wywołanie metody @link(SaveContext) powoduje zapamiętanie bieżącego
       kontekstu. Przy pomocy metody @name można przywrócić zapamiętany kontekst.

       @param(nLevel Wskazuje poziom odtwarzanego kontekstu. Domyślnie odtwarzany
                     jest ostatnio zapamiętany kontekst.

                     Jeżeli zostanie wskazany poziom odtwarzanego kontekstu to
                     konteksty o wyższych poziomach są pomijane.)

       @seeAlso(ContextLevel)
       @seeAlso(SaveContext)
       @seeAlso(DropContext)  }
    procedure  RestoreContext(nLevel:  Integer = 0);

    {: @abstract Usuwa zapamiętany kontekst bez jego przywracania.

       Każde wywołanie metody @link(SaveContext) powoduje zapamiętanie bieżącego
       kontekstu. Przy pomocy metody link(RestoreContex) można przywrócić
       zapamiętany kontekst. Natomiast przy pomocy metody @name można zapamiętany
       kontekst usunąć bez jego przywracania.

       @param(nLevel Wskazuje poziom usuwanego kontekstu. Domyślnie usuwany
                     jest ostatnio zapamiętany kontekst.

                     Jeżeli zostanie wskazany poziom usuwanego kontekstu to
                     konteksty o wyższych poziomach są również usuwane.)

       @seeAlso(ContextLevel)
       @seeAlso(SaveContext)
       @seeAlso(RestoreContext)  }
    procedure  DropContext(nLevel:  Integer = 0);

    {: @abstract Kopiuje bieżący rekord.

       Metoda @name działa analogicznie jak standardowa @code(Insert) ale
       wartości początkowe pól są takie same jak wartości bieżącego rekordu.  }
    procedure  CopyRecord();

    {: @abstract Poziom kontekstu.

       Każde wywołanie metody @link(SaveContext) powoduje zapamiętanie bieżącego
       kontekstu. Można go później odtworzyć przy pomocy metody
       @link(RestoreContext).

       Właściwość @name informuje o bieżącym poziomie kontekstu, czyli o liczbie
       wywołań metody link(SaveContext) bez wywołania metody @link(RestoreContext).

       @seeAlso(SaveContext)
       @seeAlso(RestoreContext)  }
    property  ContextLevel : Integer
              read  GetContextLevel;

  published
    {: @abstract Steruje automatycznym aktywowaniem komponentu.

       Standardowy komponent @inheritedClass działa tak, że jeżeli w trakcie
       projektowania jego właściwość @code(Active) zostanie ustawiona to
       w trakcie działania programu też jest automatycznie ustawiana.

       W komponencie @className tym działaniem steruje właściwość @name. Jeżeli
       ma wartość @true to komponent zachowuje się identycznie jak standardowy
       komponent @inheritedClass. Jeżeli natomiast właściwość @name ma wartość
       @False (wartość domyślna) to właściwość @code(Active) @bold(nie) jest
       ustawiania automatycznie.  }
    property  AllowStreamedActive:  Boolean
              read  fbAllowStreamedActive
              write fbAllowStreamedActive
              default  False;

    property  LogChanges:  Boolean
              read  fbLogChanges
              write SetLogChanges
              default  False;
  end { TGSkClientDataSet };


implementation


uses
  GSkPasLib.PasLibInternals;


{ TGSkClientDataSet }

procedure  TGSkClientDataSet.CheckIfBrowseMode();
begin
  if  State <> dsBrowse  then
    DatabaseError('Component is not in the ''dsBrowse'' mode', Self)
end { CheckIfBrowseMode };


procedure  TGSkClientDataSet.CopyRecord();

var
  nInd:  Integer;

begin  { CopyRecord }
  fValues := VarArrayCreate([0, Pred(FieldCount)], varVariant);
  for  nInd := FieldCount - 1  downto  0  do
    if  Fields[nInd].ReadOnly  then
      fValues[nInd] := Unassigned
    else
      if  (Fields[nInd].AutoGenerateValue = arNone)
          or (Fields[nInd].AutoGenerateValue = arDefault)
             and not Fields[nInd].IsNull  then
        fValues[nInd] := Fields[nInd].Value
      else
        fValues[nInd] := Unassigned;
  Insert()
end { CopyRecord };


constructor  TGSkClientDataSet.Create(AOwner:  TComponent);
begin
  inherited;
  SetLength(fContexts, 0);
  VarClear(fValues)
end { Create };


destructor  TGSkClientDataSet.Destroy();
begin
  SetLength(fContexts, 0);
  VarClear(fValues);
  inherited
end { Destroy };


procedure  TGSkClientDataSet.DoOnNewRecord();

var
  nInd:  Integer;

begin  { DoOnNewRecord }
  if  not VarIsEmpty(fValues)  then
    try
      Assert(VarArrayLowBound(fValues, 1) = 0);
      Assert(VarArrayHighBound(fValues, 1) = FieldCount - 1);
      for  nInd := FieldCount - 1  downto  0  do
        if  Fields[nInd].IsNull
            and Fields[nInd].CanModify
            and (Fields[nInd].AutoGenerateValue <> arAutoInc)
            and not VarIsEmpty(fValues[nInd])  then
          Fields[nInd].Value := fValues[nInd]
    finally
      VarClear(fValues)
    end { try-finally; not VarIsEmpty(fValues) };
  inherited
end { DoOnNewRecord };


procedure  TGSkClientDataSet.DropContext(nLevel:  Integer);
begin
  if  nLevel = 0  then
    nLevel := Length(fContexts);
  if  (nLevel < 0)
      or (nLevel > Length(fContexts))  then
    raise  EGSkPasLibError.Create('TGSkClientDataSet.RestoreContext',
                                  'Parameter ''nLevel'' is out of range');
  SetLength(fContexts, Pred(nLevel))
end { DropContext };


function  TGSkClientDataSet.GetContextLevel() : Integer;
begin
  Result := Length(fContexts)
end { GetContextLevel };


function  TGSkClientDataSet.InternalSaveContext() : Integer;
begin
  CheckIfBrowseMode();
  SetLength(fContexts, Succ(Length(fContexts)));
  Result := Length(fContexts);
  with  fContexts[High(fContexts)]  do
    begin
      fbFiltered := Filtered;
      fsFiler    := Filter;
      fsIndexFieldNames := IndexFieldNames;
      fsIndexName       := IndexName;
      fnRecNo := RecNo
    end { with }
end { InternalSaveContext };


procedure  TGSkClientDataSet.Loaded();
begin
  inherited;
  try
    if  not fbAllowStreamedActive
        and not(csDesigning in ComponentState)  then
      StreamedActive := False;
    if  StreamedActive
        and  not Active  then
      begin
        if  fbStreamedActive  then
          Active := True;
        StreamedActive := False
      end { if }
  except
    if  csDesigning in ComponentState  then
      if  Assigned(Classes.ApplicationHandleException)  then
        Classes.ApplicationHandleException(ExceptObject)
      else
        ShowException(ExceptObject, ExceptAddr)
    else
      raise
  end { try-except }
end { Loaded };


procedure  TGSkClientDataSet.RestoreContext(nLevel:  Integer);
begin
  CheckIfBrowseMode();
  if  nLevel = 0  then
    nLevel := Length(fContexts);
  if  (nLevel <= 0)
      or (nLevel > Length(fContexts))  then
    raise  EGSkPasLibError.Create('TGSkClientDataSet.RestoreContext',
                                  'Parameter ''nLevel'' is out of range');
  Dec(nLevel);
  with  fContexts[nLevel]  do
    begin
      Filter := fsFiler;
      Filtered := fbFiltered;
      if  fsIndexName <> ''  then
        IndexName := fsIndexName
      else
        IndexFieldNames := fsIndexFieldNames;
      if  fnRecNo > 0  then
        RecNo := Min(fnRecNo, RecordCount)
    end { with };
  SetLength(fContexts, nLevel)
end { RestoreContext };


function  TGSkClientDataSet.SaveContext(const   sNewIndex:  string;
                                        const   sNewFilter:  string;
                                        const   bGotoFirstRec:  Boolean)
                                        : Integer;
begin
  Result := InternalSaveContext();
  if  sNewIndex <> ''  then
    begin
      IndexName := sNewIndex;
      if  sNewFilter <> ''  then
        begin
          Filter := sNewFilter;
          Filtered := True
        end { sNewFilter <> '' };
      if  bGotoFirstRec  then
        First()
    end { sNewIndex <> '' }
end { SaveContext };


function  TGSkClientDataSet.SaveContext(const   sNewIndex:  string;
                                        const   bClearFilter, bGotoFirstRec:  Boolean)
                                        : Integer;
begin
  Result := InternalSaveContext();
  if  sNewIndex <> ''  then
    begin
      IndexName := sNewIndex;
      if  bClearFilter  then
        Filtered := False;
      if  bGotoFirstRec  then
        First()
    end { sNewIndex <> '' }
end { SaveContext };


function  TGSkClientDataSet.SaveContext(const   sNewIndex:  string;
                                        const   nRecNo:  Integer;
                                        const   sNewFilter:  string)
                                        : Integer;
begin
  Result := InternalSaveContext();
  if  sNewIndex <> ''  then
    IndexName := sNewIndex;
  if  sNewFilter <> ''  then
    Filter := sNewFilter;
  if  nRecNo <> 0  then
    RecNo := nRecNo
end { SaveContext };


function  TGSkClientDataSet.SaveContext(const   nRecNo:  Integer;
                                        const   sNewFilter:  string)
                                        : Integer;
begin
  Result := InternalSaveContext();
  if  sNewFilter <> ''  then
    Filter := sNewFilter;
  if  nRecNo <> 0  then
    RecNo := nRecNo
end { SaveContext };


procedure  TGSkClientDataSet.SetActive(Value:  Boolean);
begin
  if  csReading in ComponentState  then
    fbStreamedActive := Value
  else
    inherited;
  SetLogChanges(fbLogChanges)
end { SetActive };


procedure  TGSkClientDataSet.SetLogChanges(const   bValue:  Boolean);
begin
  fbLogChanges := bValue;
  if  Active  then
    inherited LogChanges := bValue
end { SetLogChanges };


end.
