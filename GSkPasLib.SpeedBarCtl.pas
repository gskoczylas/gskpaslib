﻿{: @abstract Moduł zawiera definicję komponentu konfigurowania paska przycisków.

   Moduł @name zawiera definicję komponentu @link(TGSkCustomSpeedBarCtl).
   Umożliwia on użytkownikowi definiowanie układu przycisków w pasku przycisków
   w postaci komponentu @code(TJvSpeedBar) z pakietu
   @italic(JEDI VCL) [http://homepages.borland.com/jedi/jvcl]. }
unit  GSkPasLib.SpeedBarCtl;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Forms, Classes, SysUtils, Types, Windows, ComCtrls, Menus, Controls,
  JvSpeedBar, {$IFDEF GetText} JvGnugettext, {$ENDIF}
  GSkPasLib.CustomComponents, GSkPasLib.ParamSaver;


type
  {: @abstract Bazowy komponent konfiguracji paska przycisków.

     Komponent @name umożliwia użytkownikowi definiowanie układu przycisków w
     pasku przycisków w postaci komponentu @code(TJvSpeedBar) z pakietu
     @italic(JEDI VCL) [http://homepages.borland.com/jedi/jvcl].

     Z komponentu @name nie można korzystać bezpośrednio ponieważ zawiera on
     abstrakcyjne metody. Zamiast @name należy skorzystać z komponentu
     potomnego definiującego działanie abstrakcyjnych metod.

     @seeAlso(TGSkSpeedBarCtl)  }
  TGSkCustomSpeedBarCtl = class(TGSkCustomComponent)
  private
    fSpeedBar:         TJvSpeedBar;
    fbAdjustFormSize:  Boolean;
    fFormBorderStyle:  TFormBorderStyle;
    fnFormPosLeft:     Integer;
    fnFormPosTop:      Integer;
    procedure  SetSpeedBar(const   NewSpeedBar:  TJvSpeedBar);
    procedure  SpeedBarCustomize(Sender:  TObject);
    procedure  ChkDefSpeedBar();
    procedure  ConfigureSpeedBar(Sender:  TObject);
  protected
    {: @exclude }
    procedure  Notification(AComponent:  TComponent;
                            Operation:  TOperation);                 override;
  public
    {: @exclude }
    constructor  Create(Owner:  TComponent);                         override;
    {: @exclude }
    destructor  Destroy();                                           override;

    {: @abstract Wyświetla okno konfiguracji paska przycisków.

       Metoda @name wyświetla okno konfiguracji paska przycisków. Jest to
       okno wbudowane w komponent @code(TJvSpeedBar).

       Jeżeli okno zawierające komponent @code(TJvSpeedBar) ma obramowanie typu
       @code(bsSingle), @code(bsDialog), @code(bsToolWindow) lub @code(bsNone)
       to przed wyświetleniem dialogu zostanie zmienione na @code(bsSizeable)
       lub @code(bsSizeToolWin).

       Po zakończeniu konfigurowania paska przycisków przywrócone zostanie
       położenie okna i rodzaj obramowania. Jeżeli obramowanie okna jest
       typu @code(bsSingle), @code(bsDialog), @code(bsToolWindow) lub
       @code(bsNone) to automatycznie wywołana zostanie metoda
       @link(AdjustFormSize) w celu ustalenia aktualnej szerokości i
       wysokości okna.

       @seeAlso(AdjustFormSize)
       @seeAlso(SaveLayout) }
    procedure  StartCustomize(const   HelpCtx:  THelpContext = 0);

    {: @abstract Ustala aktualną szerokość i wysokość okna.

       Metoda @name zmienia szerokość i wysokość okna uwzględniając pozycje menu,
       pasek statusu oraz położenie przycisków na pasku przycisków.

       Metoda @name wywoływana jest automatycznie po zakończeniu konfigurowania
       paska przycisków oraz po odtworzeniu układu przycisków na pasku przycisków.

       @seeAlso(StartCustomize)
       @seeAlso(SaveLayout)  }
    procedure  AdjustFormSize();

    {: @abstract Zapamiętuje bieżący układ przycisków.

       Po wywołaniu metody @name zapamiętywany jest bieżący układ przycisków.
       Można go przywrócić wywołując metodę @link(RestoreLayout).

       Zazwyczaj aplikacja nigdy nie wywołuje tej metody --- jest wywoływana
       automatycznie w odpowiednich miejscach.

       W komponencie @className metoda @name jest abstrakcyjna. W aplikacjach
       należy używać komponenty potomne definiujące tę metodę.

       @seeAlso(RestoreLayout)
       @seeAlso(StartCustomize)
       @seeAlso(TGSkSpeedBarCtl)  }
    procedure  SaveLayout();                               virtual; abstract;

    {: @abstract Przywraca układ przycisków.

       Po wywołaniu metody @name przywracany jest układ przycisków zapamiętany
       przez uprzednie wywołanie metody @link(SaveLayout).

       W komponencie @className metoda @name jest abstrakcyjna. W aplikacjach
       należy używać komponenty potomne definiujące tę metodę.

       @seeAlso(RestoreLayout)
       @seeAlso(StartCustomize)
       @seeAlso(TGSkSpeedBarCtl)  }
    procedure  RestoreLayout();                            virtual; abstract;
  published

    {: @abstract Pasek przycisków.

       Właściwość @name wskazuje pasek przycisków, którym steruje komponent
       @className.

       Metoda @link(StartCustomize) wyświetla dialog pozwalający użytkownikowi
       zmienić układ przycisków na pasku.

       Metoda @link(SaveLayout) pozwala zapamiętać układ przycisków. Można go
       później przywrócić przy pomocy metody @link(RestoreLayout).

       @seeAlso(TGSkSpeedBarCtl)  }
    property  SpeedBar:  TJvSpeedBar
              read  FSpeedBar
              write SetSpeedBar
              default nil;
  end { TGSkCustomSpeedBarCtl };

  {: @abstract Komponent implementujący konfigurowanie paska przycisków.

     Komponent @name umożliwia użytkownikowi definiowanie układu przycisków w
     pasku przycisków w postaci komponentu @code(TJvSpeedBar) z pakietu
     @italic(JEDI VCL) [http://homepages.borland.com/jedi/jvcl].

     Komponent @name implementuje abstrakcyjne metody komponentu @inherited
     wykorzystując komponent typu @link(TGSkCustomParamSaver) zwrócony przez
     funkcję @link(CreateStdParamSaver).

     Układ przycisków zapamiętywany jest w @link(GetFormRegistryKey kontekście)
     odpowiedniego okna oraz rozdzielczości ekranu.

     @seeAlso(TGSkCustomSpeedBarCtl)  }
  TGSkSpeedBarCtl = class(TGSkCustomSpeedBarCtl)
  private
    fParamSaver:  TGSkCustomParamSaver;
  public
    {: @exclude }
    constructor  Create(Owner:  TComponent);                         override;
    {: @exclude }
    destructor  Destroy();                                           override;
    {: @exclude }
    procedure  SaveLayout();                                         override;
    {: @exclude }
    procedure  RestoreLayout();                                      override;
  end { TGSkSpeedBarCtl };


implementation


uses
  GSkPasLib.Tokens, GSkPasLib.FormUtils, GSkPasLib.PasLib;


const
  {: @exclude }
  {$IFNDEF GetText}
    gcsSpeedBarConfigure = 'Dostosuj pasek narzędzi...';
  {$ELSE}
    gcsSpeedBarConfigure = 'Configure toolbar...';
  {$ENDIF -GetText}


type
  TGSkSpeedBarItemProc = procedure(const  btItem:  TJvSpeedItem;
                                   const  Param:  TGSkCustomParamSaver);


procedure  ForEachItem(const   SpeedBar:  TJvSpeedBar;
                       const   fnItemProc:  TGSkSpeedBarItemProc;
                       const   Param:  TGSkCustomParamSaver);
var
  nSecInd, nBtnInd:  integer;

begin  { ForEachItem }
  Assert(Assigned(SpeedBar));
  with  SpeedBar  do
    for  nSecInd := 0  to  SectionCount - 1  do
      for  nBtnInd := 0  to  ItemsCount(nSecInd) - 1  do
        fnItemProc(Items(nSecInd, nBtnInd), Param)
end { ForEachItem };


procedure  SaveSpeedBarItem(const   btItem:  TJvSpeedItem;
                            const   Param:  TGSkCustomParamSaver);
begin  { SaveSpeedBarItem }
  with  btItem  do
    Param.WriteString(Name,
                      Format('%d,%d,%d',
                             [Ord(Visible), Left, Top]))
end { SaveSpeedBarItem };


procedure  RestSpeedBarItem(const   btItem:  TJvSpeedItem;
                            const   Param:  TGSkCustomParamSaver);
begin
  with  TGSkCharTokens.Create(nil)  do
    try
      Text := Param.ReadString(btItem.Name);
      AllowEmptyTokens := False;
      SetDelimiter(',');
      if  Count >= 3  then
        with  btItem  do
          begin
            Visible := (StrToIntDef(Token[0], Ord(Visible)) <> Ord(False));
            Left := StrToIntDef(Token[1], Left);
            Top := StrToIntDef(Token[2], Top);
          end { Count = 3; with btItem }
    finally
      Free()
    end { try-finally }
end { RestSpeedBarItem };


procedure  HideSpeedBarItem(const   btItem:  TJvSpeedItem;
                            const   Param:  TGSkCustomParamSaver);
begin
  btItem.Visible := False
end { HideSpeedBarItem };


{ TGSkCustomSpeedBarCtl }

procedure  TGSkCustomSpeedBarCtl.ChkDefSpeedBar();
begin
  CheckObjProp(fSpeedBar, 'SpeedBar');
end { ChkDefSpeedBar };


constructor  TGSkCustomSpeedBarCtl.Create(Owner:  TComponent);
begin
  inherited
end { Create };


destructor  TGSkCustomSpeedBarCtl.Destroy();
begin
  inherited
end { Destroy };


procedure  TGSkCustomSpeedBarCtl.Notification(AComponent:  TComponent;
                                              Operation:  TOperation);
begin
  inherited;
  if  (AComponent = FSpeedBar)
      and (Operation = opRemove)  then
    fSpeedBar := nil
end { Notification };


procedure  TGSkCustomSpeedBarCtl.AdjustFormSize();

var
  nIndSec, nIndBtn, nMaxW, nMaxH:  Integer;
  lForm:  TForm;
  lMenuRect:  TRect;

begin  { AdjustFormSize }
  ChkDefSpeedBar();
  lForm := FindComponentForm(FSpeedBar);
  { Adjust form size depending on menu items positions }
  if  Assigned(lForm.Menu)  then
    begin
      GetMenuItemRect(lForm.Handle, lForm.Menu.Handle, 0, lMenuRect);
      nIndBtn := lMenuRect.Top;
      nMaxW := -1;
      for  nMaxH := lForm.Menu.Items.Count - 1  downto  0  do
        if  lForm.Menu.Items[nMaxH].Visible  then
          Inc(nMaxW);
      if  nMaxW >= 0  then
        begin
          GetMenuItemRect(lForm.Handle, lForm.Menu.Handle, nMaxW, lMenuRect);
          while  nIndBtn <> lMenuRect.Top  do
            begin
              lForm.Width := lForm.Width + lMenuRect.Right;
              GetMenuItemRect(lForm.Handle, lForm.Menu.Handle, nMaxW, lMenuRect)
            end { while };
          lForm.ClientWidth := lForm.ClientWidth
                                 - lForm.ClientRect.Right
                                 + lMenuRect.Right
                                 - lForm.ClientOrigin.x
                                 + 9 { wartość dobrana eksperymentalnie }
        end { nMaxW >= 0 }
    end { Assigned(Form.Menu };
  { Rethink form size depending on speed bar }
  nMaxW := 0;   nMaxH := 0;
  with  fSpeedBar  do
    begin
      for  nIndSec := 0  to  SectionCount - 1  do
        for  nIndBtn := 0  to  ItemsCount(nIndSec) - 1  do
          with  Items(nIndSec, nIndBtn)  do
            if  Visible  then
              begin
                if  Left > nMaxW  then
                  nMaxW := Left;
                if  Top > nMaxH  then
                  nMaxH := Top
              end { Visible };
      if  nMaxW > 0  then
        Inc(nMaxW, BtnWidth);
      if  nMaxH > 0  then
        Inc(nMaxH, BtnHeight)
      else
        Inc(nMaxH, 2);
      Assert(Assigned(lForm));
      with  lForm  do
        begin
          for  nIndSec := ComponentCount - 1  downto  0  do
            if  Components[nIndSec] is TStatusBar  then
              begin
                Inc(nMaxH, (Components[nIndSec] as TStatusBar).Height);
                Break
              end { TStatusBar };
          Inc(nMaxW, fSpeedBar.BorderWidth);
          Inc(nMaxH, fSpeedBar.BorderWidth{ + 2});
          if  Assigned(Menu)  then
            if  ClientWidth > nMaxW  then
              nMaxW := ClientWidth;
          fSpeedBar.Align := alClient;
          ClientWidth := nMaxW;
          ClientHeight := nMaxH
        end { with lForm };
    end { with fSpeedBar };
  CheckFormPos(lForm)
end { AdjustFormSize };


procedure  TGSkCustomSpeedBarCtl.SetSpeedBar(const   NewSpeedBar:  TJvSpeedBar);

var
  lMenu:  TMenuItem;

begin  { SetSpeedBar }
  if  NewSpeedBar <> fSpeedBar  then
    begin
      if  not (csDesigning in ComponentState)  then
        if  Assigned(fSpeedBar)  then
          begin
            fSpeedBar.RemoveFreeNotification(Self);
            if  (fSpeedBar.PopupMenu <> nil)
                and (fSpeedBar.PopupMenu.Items.Count = 1)
                and (fSpeedBar.PopupMenu.Items[0].Caption = dgettext('GSkPasLib',
                                                                     gcsSpeedBarConfigure))  then
              begin
                fSpeedBar.PopupMenu.Items[0].Free();
                fSpeedBar.PopupMenu.Free();
                fSpeedBar.PopupMenu := nil
              end
          end ;
      fSpeedBar := NewSpeedBar;
      if  not (csDesigning in ComponentState)  then
        if  Assigned(fSpeedBar)  then
          begin
            fSpeedBar.FreeNotification(Self);
            fSpeedBar.OnCustomize := SpeedBarCustomize;
            if  fSpeedBar.PopupMenu = nil  then
              begin
                fSpeedBar.PopupMenu := TPopupMenu.Create(fSpeedBar);
                lMenu := TMenuItem.Create(fSpeedBar.PopupMenu);
                lMenu.Caption := dgettext('GSkPasLib', gcsSpeedBarConfigure);
                lMenu.OnClick := ConfigureSpeedBar;
                fSpeedBar.PopupMenu.Items.Add(lMenu)
              end { fSpeedBar.PopupMenu = nil }
          end
    end { NewSpeedBar <> FSpeedBar }
end { SetSpeedBar };


procedure  TGSkCustomSpeedBarCtl.SpeedBarCustomize(Sender:  TObject);

var
  lForm:  TForm;

begin  { SpeedBarCustomize }
  lForm := FindComponentForm(FSpeedBar);
  Assert(Assigned(lForm));
  lForm.BorderStyle := fFormBorderStyle;
  lForm.Left := fnFormPosLeft;
  lForm.Top := fnFormPosTop;
  fSpeedBar.Align := alTop;
  if  fbAdjustFormSize  then
    AdjustFormSize()
  else
    CheckFormPos(lForm)
end { SpeedBarCustomize };


procedure  TGSkCustomSpeedBarCtl.StartCustomize(const   HelpCtx:  THelpContext);

var
  lForm:  TForm;

begin  { StartCustomize }
  ChkDefSpeedBar();
  lForm := FindComponentForm(FSpeedBar);
  Assert(Assigned(lForm));
  fbAdjustFormSize := (lForm.BorderStyle in [bsSingle, bsDialog,
                                             bsToolWindow, bsNone]);
  fnFormPosLeft := lForm.Left;
  fnFormPosTop := lForm.Top;
  lForm.Height := lForm.Height + fSpeedBar.BtnHeight + 20;
  lForm.Width := lForm.Width + fSpeedBar.BtnWidth + 20;
  fFormBorderStyle := lForm.BorderStyle;
  case  lForm.BorderStyle  of
    bsSingle, bsNone:
      lForm.BorderStyle := bsSizeable;
    bsToolWindow:
      lForm.BorderStyle := bsSizeToolWin
  end { case Form.BorderStyle };
  CheckFormPos(lForm);
  fSpeedBar.Position := bpCustom;
  fSpeedBar.Align := alClient;
  fSpeedBar.Customize(HelpCtx)
end { StartCustomize };


procedure  TGSkCustomSpeedBarCtl.ConfigureSpeedBar(Sender:  TObject);
begin
  StartCustomize()
end { ConfigureSpeedBar };


{ TGSkSpeedBarCtl }

constructor  TGSkSpeedBarCtl.Create(Owner:  TComponent);
begin
  inherited;
  fParamSaver := CreateStdParamSaver(Self)
end { Create };


destructor  TGSkSpeedBarCtl.Destroy();
begin
  fParamSaver.Free();
  inherited
end { Destroy };


procedure  TGSkSpeedBarCtl.RestoreLayout();

var
  lForm:  TForm;
  lParam:  TGSkCustomParamSaver;

  procedure  DeleteEmptySpeedBarSections();

  var
    nInd:  Integer;

  begin  { DeleteEmptySpeedBarSections }
    with  fSpeedBar  do
      for  nInd := SectionCount - 1  downto  0  do
        if  ItemsCount(nInd) = 0  then
          RemoveSection(nInd)
  end { DeleteEmptySpeedBarSections };

begin  { RestoreLayout }
  inherited;
  ChkDefSpeedBar();
  lForm := FindComponentForm(fSpeedBar);
  Assert(Assigned(lForm));
  lParam := CreateStdParamSaver(nil);
  with  lParam  do
    try
      if  OpenKey(GetFormRegistryKey(lForm, True,
                                     ComponentDisplayName(fSpeedBar)),
                  False)  then
        try
          ForEachItem(fSpeedBar, HideSpeedBarItem, lParam);
          ForEachItem(fSpeedBar, RestSpeedBarItem, lParam)
        finally
          CloseKey()
        end { try-finally }
    finally
      Free()
    end { try-finally; with lParam };
  DeleteEmptySpeedBarSections();
  if  lForm.BorderStyle in [bsSingle, bsDialog, bsToolWindow, bsNone]  then
    AdjustFormSize()
end { RestoreLayout };


procedure  TGSkSpeedBarCtl.SaveLayout();

var
  lParam:  TGSkCustomParamSaver;

begin  { SaveLayout }
  inherited;
  ChkDefSpeedBar();
  lParam := CreateStdParamSaver(nil);
  with  lParam  do
    try
      if  OpenKey(GetFormRegistryKey(FindComponentForm(fSpeedBar), True,
                                     ComponentDisplayName(fSpeedBar)),
                  True)  then
        try
          ForEachItem(fSpeedBar, SaveSpeedBarItem, lParam)
        finally
          CloseKey()
        end { try-finally }
    finally
      Free()
    end { try-finally; with lParam }
end { SaveLayout };


end.
