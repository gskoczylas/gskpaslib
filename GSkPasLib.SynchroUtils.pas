﻿{: @abstract(Moduł definiuje dodatkowe obiekty synchronizacji)

   Standardowo w Delphi dostępne są niektóre obiekty synchronizacji, na przykład
   @code(TCriticalSection), @code(TEvent) lub @code(TMultiReadExclusiveWriteSynchronizer).
   W module @name zdefiniowane są dodatkowe obiekty synchronizacji dostępne
   poprzez API Windows.  }
unit  GSkPasLib.SynchroUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Windows, SyncObjs, SysUtils,
  JclSysUtils;


type
  {: @abstract Przodek wszystkich obiektów synchronizacji zdefiniowanych w tym module.

     Obiekt udostępnia pola i metody wspólne dla wszystkich obiektów
     synchronizacji zdefiniowanych w tym module.

     @seeAlso(TGSkMutex)
     @seeAlso(TGSkSemaphore)  }
  TGSkCustomSynchroObject = class(THandleObject)
  private
    function  GetLastErrorMsg() : string;
  protected
    procedure  Error(const   nErrorNo:  Integer);                    overload;
    procedure  Error();                                              overload;
  public
    constructor  Create();
    destructor  Destroy();                                           override;

    {: @abstract Komunikat błędu --- jeżeli działanie zakończyło się błędem.

       Kod błędu definiuje właściwość @code(LastError) (zdefiniowana u przodka
       komponentu @className).  }
    property  LastErrorMessage:  string
              read  GetLastErrorMsg;
  end { TGSkCustomSynchroObject };

  {: @abstract(Udostępnia obiekt synchronizacji MUTEX)

     Takie obiekty są szczegółowo opisane w Pomocy @italic(Win32 Developer's
     Reference) oraz na stronie internetowej http://msdn.microsoft.com/en-us/library/ms682411.
     Są tam również informacje o tym, jakie znaki mogą występować w nazwie muteksu,
     w zależności od wersji systemu operacyjnego oraz innych elementów środowiska. }
  TGSkMutex = class(TGSkCustomSynchroObject)
  public
    {: @abstract(Tworzy mutex)

       @param(sName Nazwa mutexu musi być unikalna w całym systemie operacyjnym.
                    Muteks zdefiniowany przez dowolny proces jest dostępny dla
                    wszystkich procesów w systemie. Wielkość liter w nazwie
                    muteksu @bold(ma) znaczenie.) }
    constructor  Create(const   sName:  string);

    {: Ta metoda jest równoważna wywołaniu @code(WaitFor(INFINITE)). }
    procedure  Acquire();                                            override;
    procedure  Release();                                            override;
  end { TGSkMutex };

  {: @abstract(Udostępnia obiekt synchronizacji SEMAFOR)

     Takie obiekty są szczegółowo opisane w Pomocy @italic(Win32 Developer's
     Reference) oraz na stronie internetowej http://msdn.microsoft.com/en-us/library/ms682438.
     Są tam również informacje o tym, jakie znaki mogą występować w nazwie semafora,
     w zależności od wersji systemu operacyjnego oraz innych elementów środowiska. }
  TGSkSemaphore = class(TGSkCustomSynchroObject)
  private
    procedure  Init(const   sName:  string;
                    const   nInitCnt, nMaxCnt:  Integer);
  public
    {: @abstract(Tworzy semafor)

       @param(sName Nazwa semafora musi być unikalna w całym systemie operacyjnym.
                    Semafor zdefiniowany przez dowolny proces jest dostępny dla
                    wszystkich procesów w systemie. Wielkość liter w nazwie
                    semafora @bold(ma) znaczenie.)
       @param(nInitCnt To jest początkowa wartość semafora.)
       @param(nMaxCnt To jest maksymalna wartość semafora.) }
    constructor  Create(const   sName:  string;
                        const   nMaxCnt:  Integer);                  overload;
    constructor  Create(const   sName:  string;
                        const   nInitCnt:  Integer;
                        const   nMaxCnt:  Integer);                  overload;
    destructor  Destroy();                                           override;
    {: Ta metoda jest równoważna wywołaniu @code(WaitFor(INFINITE)).  }
    procedure  Acquire();                                            override;
    {: Ta metoda jest równoważna wywołaniu @link(ReleaseCount)@code((1)).  }
    procedure  Release();                                            override;
    {: @abstract(Zwiększa wartość semafor o @code(nCnt)) }
    procedure  ReleaseCount(const   nCnt:  Integer);                 overload;
    {: @abstract(Zwiększa wartość semafor o @code(nCnt))

       @param(nCnt Wskazuje o ile zwiększyć wartość semafora.)
       @param(nPrevVal informuje o dotychczasowej wartości semafora.) }
    procedure  ReleaseCount(const   nCnt:  Integer;
                            out   nPrevVal:  Integer);               overload;
  end { TGSkSemaphore };


implementation


{$REGION 'TGSkCustomSynchroObject'}

constructor  TGSkCustomSynchroObject.Create();
begin
  inherited Create(False)
end { Create };


destructor  TGSkCustomSynchroObject.Destroy();
begin
  inherited
end { Destroy };


procedure  TGSkCustomSynchroObject.Error(const   nErrorNo:  Integer);
begin
  FLastError := nErrorNo;
  if  nErrorNo <> NO_ERROR  then
    raise  Exception.CreateFmt('Error %d: %s',
                               [nErrorNo, SysErrorMessage(nErrorNo)])
end { Error };


procedure  TGSkCustomSynchroObject.Error();
begin
  Error(GetLastError())
end { Error };


function  TGSkCustomSynchroObject.GetLastErrorMsg() : string;
begin
  Result := SysErrorMessage(LastError)
end { GetLastErrorMsg };

{$ENDREGION}


{$REGION 'TGSkMutex'}

procedure  TGSkMutex.Acquire();
begin
  inherited Acquire();
  if  WaitFor(INFINITE) <> wrSignaled  then
    Error()
end { Acquire };


constructor  TGSkMutex.Create(const   sName:  string);
begin
  inherited Create();
  FHandle := CreateMutex(nil, False, PCharOrNil(sName));
  if  FHandle = 0  then
    Error()
end { Create };


procedure  TGSkMutex.Release();
begin
  inherited;
  if  not ReleaseMutex(FHandle)  then
    Error()
end { Release };

{$ENDREGION}


{$REGION 'TGSkSemaphore'}

procedure  TGSkSemaphore.Acquire();
begin
  inherited;
  if  WaitFor(INFINITE) <> wrSignaled  then
    Error()
end { Acquire };


constructor  TGSkSemaphore.Create(const   sName:  string;
                                  const   nMaxCnt:  Integer);
begin
  inherited  Create();
  Init(sName, nMaxCnt, nMaxCnt)
end { Create };


constructor  TGSkSemaphore.Create(const   sName:  string;
                                  const   nInitCnt, nMaxCnt:  Integer);
begin
  inherited Create();
  Init(sName, nInitCnt, nMaxCnt)
end { Create };


destructor  TGSkSemaphore.Destroy();
begin
  inherited
end { Destroy };


procedure  TGSkSemaphore.Init(const   sName:  string;
                              const   nInitCnt, nMaxCnt:  Integer);
begin
  FHandle := CreateSemaphore(nil, nInitCnt, nMaxCnt, PChar(sName));
  if  FHandle = 0  then
    Error()
end { Init };


procedure  TGSkSemaphore.Release();
begin
  inherited;
  ReleaseCount(1)
end { Release };


procedure  TGSkSemaphore.ReleaseCount(const   nCnt:  Integer);
begin
  if  not ReleaseSemaphore(FHandle, nCnt, nil)  then
    Error()
end { ReleaseCount };


procedure  TGSkSemaphore.ReleaseCount(const   nCnt:  Integer;
                                      out   nPrevVal:  Integer);
begin
  if  not ReleaseSemaphore(FHandle, nCnt, Addr(nPrevVal))  then
    Error()
end { ReleaseCount };

{$ENDREGION}


end.
