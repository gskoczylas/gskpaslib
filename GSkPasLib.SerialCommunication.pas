﻿unit  GSkPasLib.SerialCommunication;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    WinAPI.Windows,
    System.SysUtils, System.Classes, System.Math, System.Generics.Defaults,
  {$ELSE}
    Windows,
    SysUtils, Classes, Math, Generics.Defaults,
  {$IFEND}
  JvGnugettext,
  GSkPasLib.PasLib, GSkPasLib.PasLibInternals, GSkPasLib.Log,
  GSkPasLib.CustomComponents;


const
  gcnErrSerComm_AlreadyConnected = -1;
  gcnErrSerComm_InternalError    = -2;
  gcnErrSerComm_NotConnectedYet  = -3;
  gcnErrSerComm_ConnectError     = -4;
  gcnErrSerComm_WriteError       = -5;
  gcnErrSerComm_ReadError        = -6;
  gcnErrSerComm_Timeout          = -7;
  gcnErrSerComm_LAST             = gcnErrSerComm_Timeout;
  {-}
  gcStdLogLevel = Low(TGSkLogMessageType);
  {-}
  gcsErrSerComm_AlreadyConnected = 'Serial port is already connected';
  gcsErrSerComm_NotConnectedYet  = 'Serial port is not connected yet';
  gcsErrSerComm_Timeout          = 'Time-out';


type
  TGSkSerialCommFlowControl = (fcNone, fcRtsCts, fcXonXoff);
  TGSkSerialCommBaudRate = (br110    = CBR_110,
                            br300    = CBR_300,
                            br600    = CBR_600,
                            br1200   = CBR_1200,
                            br2400   = CBR_2400,
                            br4800   = CBR_4800,
                            br9600   = CBR_9600,
                            br14400  = CBR_14400,
                            br19200  = CBR_19200,
                            br38400  = CBR_38400,
                            br57600  = CBR_57600,
                            br115200 = CBR_115200,
                            br128000 = CBR_128000,
                            br256000 = CBR_256000);
  TGSkSerialCommDataBits = 5..8;
  TGSkSerialCommParityBits = (pbNone  = NOPARITY,
                              pbOdd   = ODDPARITY,
                              pbEven  = EVENPARITY,
                              pbMark  = MARKPARITY,
                              pbSpace = SPACEPARITY);

  TGSkSerialCommStopBits = (sbOne        = ONESTOPBIT,
                            sbOneDotFive = ONE5STOPBITS,
                            sbTwo        = TWOSTOPBITS);

  TGSkSerialCommTimeout = Cardinal;
  TGSkSerialCommPort = 1..256;

  TGSkSerialCommunicationErrorEvent = procedure(Sender:  TObject;
                                                const   sMethod:  string;
                                                const   nErrCode:  Integer;
                                                const   sErrMsg:  string) of object;

  TGSkSerialCommunication = class(TGSkCustomComponent)
  strict private
    var
      fFlowControl: TGSkSerialCommFlowControl;
      fBaudRate:    TGSkSerialCommBaudRate;
      fDataBits:    TGSkSerialCommDataBits;
      fParityBits:  TGSkSerialCommParityBits;
      fStopBits:    TGSkSerialCommStopBits;
      fTimeout:     TGSkSerialCommTimeout;
      fPort:        TGSkSerialCommPort;
      fSerialComm:  THandle;
      fLog:         TGSkLog;
      fnErrorNum:   Integer;
      fsErrorMsg:   string;
      fOnError:     TGSkSerialCommunicationErrorEvent;
    function  GetInputCount() : Integer;
    procedure  SetProperty<T>(var    Field:  T;
                              const   Value:  T;
                              const   sMehotd:  string);
    procedure  SetFlowControl(const   Value:  TGSkSerialCommFlowControl);
    procedure  SetBaudRate(const   Value:  TGSkSerialCommBaudRate);
    procedure  SetDataBits(const   Value:  TGSkSerialCommDataBits);
    procedure  SetParityBits(const   Value:  TGSkSerialCommParityBits);
    procedure  SetStopBits(const   Value:  TGSkSerialCommStopBits);
    procedure  SetPort(const   Value:  TGSkSerialCommPort);
    procedure  SetLog(const   Log:  TGSkLog);
    procedure  CheckResultOfAPI(const   bResultOfAPI:  BOOL;
                                const   sMethod:  string;
                                const   nErrCode:  Integer);
    {-}
    procedure  LogEnter(const   sMessage:  string);                  overload;
    procedure  LogEnter(const   sMessageFmt:  string;
                        const   MessageArgs:  array of const);       overload;
    procedure  LogMessage(const   sMessage:  string);                overload;
    procedure  LogMessage(const   sMessageFmt:  string;
                          const   MessageArgs:  array of const);     overload;
    procedure  LogExit(const   sMessage:  string);                   overload;
//     procedure  LogExit(const   sMessageFmt:  string;
//                        const   MessageArgs:  array of const);        overload;
    procedure  LogResultExit(const   Result:  Variant;
                             const   sMessage:  string);
//     procedure  BeginLog();
//     procedure  EndLog();
//     procedure  AbandonLog();
  strict protected
    function  GetConnectionString() : string;
    procedure  CheckIfConnected(const   sMethod:  string);
    procedure  CheckIfNotConnected(const   sMethod:  string);
    procedure  DoError(const   sMethod:  string;
                       const   nErrCode:  Integer;
                       const   sErrMsg:  string);                    overload; virtual;
    procedure  DoError(const   sMethod:  string;
                       const   nErrCode:  Integer;
                       const   sErrFmt:  string;
                       const   ErrArgs:  array of const);            overload; virtual;
    procedure  Notification(AComponent:  TComponent;
                            Operation:  TOperation);                 override;
  public
    constructor  Create();                                           reintroduce;
    destructor  Destroy();                                           override;
    {-}
    class procedure  EnumPorts(const   Ports:  TStrings;
                               const   bErrorIfNone:  Boolean = True);  // for backward compatibility
    {-}
    function  ToString() : string;                                   override;
    function  IsConnected() : Boolean;
    function  ReadByte() : Byte;
    function  ReadData(var   Data:  TBytes;
                       const   nBytesToRead:  Integer)
                       : Integer;
    procedure  WriteByte(const   nData:  Byte);
    procedure  WriteData(const   sData:  AnsiString);                overload;
    procedure  WriteData(const   Data:  TBytes);                     overload;
    procedure  Connect();
    procedure  Disconnect();
    procedure  Purge();
    procedure  SetError(const   nErrorCode:  Integer;
                        const   sErrorInfo:  string);
    procedure  ClearError();
    {-}
    property  Port:  TGSkSerialCommPort
              read  fPort
              write SetPort;
    property  FlowControl:  TGSkSerialCommFlowControl
              read  fFlowControl
              write SetFlowControl;
    property  BaudRate:  TGSkSerialCommBaudRate
              read  fBaudRate
              write SetBaudRate;
    property  DataBits:  TGSkSerialCommDataBits
              read  fDataBits
              write SetDataBits;
    property  ParityBits:  TGSkSerialCommParityBits
              read  fParityBits
              write SetParityBits;
    property  StopBits:  TGSkSerialCommStopBits
              read  fStopBits
              write SetStopBits;
    property  Timeout:  TGSkSerialCommTimeout
              read  fTimeout
              write fTimeout;
    property  Log:  TGSkLog
              read  fLog
              write SetLog;
    property  InputCount:  Integer
              read  GetInputCount;
    property  LastErrorCode:  Integer
              read  fnErrorNum;
    property  LastErrorInfo:  string
              read  fsErrorMsg;
    { Events }
    property  OnError:  TGSkSerialCommunicationErrorEvent
              read  fOnError
              write fOnError;
  end { TGSkSerialCommunication };

  EGSkSerialCommunicationError = class(EGSkPasLibErrorEx)
  public
    constructor  Create(const   sMethod:  string;
                        const   nErrCode:  Integer;
                        const   sErrMsg:  string = '');
    constructor  CreateFmt(const   sMethod:  string;
                           const   nErrCode:  Integer;
                           const   sErrMsgFmt:  string;
                           const   ErrMsgArgs:  array of const);
  end { EGSkSerialCommunicationError };


const
  gcDefaultFlowControl = fcNone;
  gcDefaultBaudRate    = br9600;
  gcDefaultDataBits    = 8;
  gcDefaultParityBits  = pbNone;
  gcDefaultStopBits    = sbOne;
  gcDefaultTimeout     = 1000;   // 1000 ms = 1 s;
  gcDefaultCommPort    = 1;


implementation


uses
  GSkPasLib.StrUtils;


const
  gcTimeoutThreshold = 500;


{$REGION 'TGSkSerialCommunication'}

// procedure  TGSkSerialCommunication.AbandonLog();
// begin
//   if  Log <> nil  then
//     Log.AbandonLog()
// end { AbandonLog };


// procedure  TGSkSerialCommunication.BeginLog();
// begin
//   if  Log <> nil  then
//     Log.BeginLog()
// end { BeginLog };


procedure  TGSkSerialCommunication.CheckIfConnected(const   sMethod:  string);
begin
  if  not IsConnected()  then
    DoError(sMethod, gcnErrSerComm_NotConnectedYet,
            dgettext('GSkPasLib', gcsErrSerComm_NotConnectedYet))
end { CheckIfConnected };


procedure  TGSkSerialCommunication.CheckIfNotConnected(const   sMethod:  string);
begin
  if  IsConnected()  then
    DoError(sMethod, gcnErrSerComm_AlreadyConnected,
            dgettext('GSkPasLib', gcsErrSerComm_AlreadyConnected))
end { CheckIfNotConnected };


procedure  TGSkSerialCommunication.CheckResultOfAPI(const   bResultOfAPI:  BOOL;
                                                    const   sMethod:  string;
                                                    const   nErrCode:  Integer);
var
  nSysErrNo:  Cardinal;

begin  { CheckResultOfAPI }
  if  not bResultOfAPI  then
    begin
      nSysErrNo := GetLastError();
      DoError(sMethod, nErrCode,
              Format('%u: %s',
                     [nSysErrNo, SysErrorMessage(nSysErrNo)]))
    end { not bResultOfAPI }
end { CheckResultOfAPI };


procedure  TGSkSerialCommunication.ClearError();
begin
  SetError(0, '')
end { ClearError };


procedure  TGSkSerialCommunication.Connect();

const
  dcb_Binary              = $00000001;
  dcb_ParityCheck         = $00000002;
  dcb_OutxCtsFlow         = $00000004;
  dcb_OutxDsrFlow         = $00000008;
  dcb_DtrControlMask      = $00000030;
  dcb_DtrControlDisable   = $00000000;
  dcb_DtrControlEnable    = $00000010;
  dcb_DtrControlHandshake = $00000020;
  dcb_DsrSensivity        = $00000040;
  dcb_TXContinueOnXoff    = $00000080;
  dcb_OutX                = $00000100;
  dcb_InX                 = $00000200;
  dcb_ErrorChar           = $00000400;
  dcb_NullStrip           = $00000800;
  dcb_RtsControlMask      = $00003000;
  dcb_RtsControlDisable   = $00000000;
  dcb_RtsControlEnable    = $00001000;
  dcb_RtsControlHandshake = $00002000;
  dcb_RtsControlToggle    = $00003000;
  dcb_AbortOnError        = $00004000;
  dcb_Reserveds           = $FFFF8000;

var
  lCommTimeout:  TCommTimeouts;
  lCommParams:   TDCB;

  procedure  ChkResOfAPI(const   bResultOfAPI:  BOOL);
  begin
    CheckResultOfAPI(bResultOfAPI, 'Connect', gcnErrSerComm_ConnectError)
  end { ChkResOfAPI };

begin  { Connect }
  LogEnter('TGSkSerialCommunication.Connect [%s]', [ToString()]);
  CheckIfNotConnected('Connect');
  {-}
  fSerialComm := CreateFile(PChar(GetConnectionString()),
                            GENERIC_READ or GENERIC_WRITE,
                            0, nil, OPEN_EXISTING, {FILE_FLAG_OVERLAPPED}0, 0);
  if  fSerialComm = INVALID_HANDLE_VALUE  then
    DoError('Connect', gcnErrSerComm_ConnectError,
            SysErrorMessage(GetLastError()));
  {-}
  lCommTimeout.ReadIntervalTimeout := 100;
  lCommTimeout.ReadTotalTimeoutMultiplier := 100;
  lCommTimeout.ReadTotalTimeoutConstant := gcTimeoutThreshold;
  lCommTimeout.WriteTotalTimeoutMultiplier := 100;
  lCommTimeout.WriteTotalTimeoutConstant := gcDefaultTimeout;
  ChkResOfAPI(SetCommTimeouts(fSerialComm, lCommTimeout));
  {-}
  ChkResOfAPI(GetCommState(fSerialComm, lCommParams));
  lCommParams.BaudRate := Ord(fBaudRate);
  lCommParams.Parity := Ord(fParityBits);
  lCommParams.ByteSize := fDataBits;
  lCommParams.StopBits := Ord(fStopBits);
  case  fFlowControl  of
    fcXonXoff:
      lCommParams.Flags := dcb_Binary or dcb_OutX or dcb_InX
                           or dcb_RtsControlHandshake or dcb_RtsControlEnable
                           or dcb_DtrControlEnable;
    fcRtsCts:
      lCommParams.Flags := dcb_Binary or dcb_OutxCtsFlow or dcb_RtsControlHandshake
                           or dcb_DtrControlEnable;
    fcNone:
      lCommParams.Flags := dcb_Binary or dcb_RtsControlEnable or dcb_DtrControlEnable
  end { case fHandshake };
  if  fParityBits <> pbNone  then
    with  lCommParams  do
      Flags := Flags or dcb_ParityCheck;
  ChkResOfAPI(SetCommState(fSerialComm, lCommParams));
  LogExit('TGSkSerialCommunication.Connect')
end { Connect };


constructor  TGSkSerialCommunication.Create();
begin
  inherited Create(nil);
  fFlowControl := gcDefaultFlowControl;
  fBaudRate := gcDefaultBaudRate;
  fDataBits := gcDefaultDataBits;
  fParityBits := gcDefaultParityBits;
  fStopBits := gcDefaultStopBits;
  fTimeout := gcDefaultTimeout;
  fPort := gcDefaultCommPort;
  fSerialComm := INVALID_HANDLE_VALUE
end { Create };


destructor  TGSkSerialCommunication.Destroy();
begin
  if  IsConnected()  then
    Disconnect();
  LogMessage('*** STOP Service object');
  inherited
end { Destroy };


procedure  TGSkSerialCommunication.Disconnect();
begin
  LogEnter('TGSkSerialCommunication.Disconnect');
  CheckIfConnected('Disconnect');
  FileClose(fSerialComm);
  fSerialComm := INVALID_HANDLE_VALUE;
  LogExit('TGSkSerialCommunication.Disconnect')
end { Disconnect };


procedure  TGSkSerialCommunication.DoError(const   sMethod:  string;
                                           const   nErrCode:  Integer;
                                           const   sErrMsg:  string);
begin
  SetError(nErrCode, sErrMsg);
  {-}
  if  Log <> nil  then
    Log.LogMessage('Error %d --> %s', [nErrCode, sErrMsg], lmError);
  {-}
  if  Assigned(fOnError)  then
    fOnError(Self, sMethod, nErrCode, sErrMsg);
  raise  EGSkSerialCommunicationError.Create(sMethod, nErrCode, sErrMsg)
end { DoError };


procedure  TGSkSerialCommunication.DoError(const   sMethod:  string;
                                           const   nErrCode:  Integer;
                                           const   sErrFmt:  string;
                                           const   ErrArgs:  array of const);
begin
  DoError(sMethod, nErrCode, Format(sErrFmt, ErrArgs))
end { DoError };


// procedure  TGSkSerialCommunication.EndLog();
// begin
//   if  Log <> nil  then
//     Log.EndLog()
// end { EndLog };


class procedure  TGSkSerialCommunication.EnumPorts(const   Ports:  TStrings;
                                                   const   bErrorIfNone:  Boolean);
var
  nPort:  Integer;
  sPort:  string;
  hPort:  THandle;

begin  { EnumPorts }
  Ports.BeginUpdate();
  try
    Ports.Create();
    for  nPort :=  1  to  256  do
      begin
        sPort := 'COM' + IntToStr(nPort);
        hPort := CreateFile(PChar('\\.\' + sPort),
                            GENERIC_READ or GENERIC_WRITE,
                            0, nil, OPEN_EXISTING, 0, 0);
        if  hPort <> INVALID_HANDLE_VALUE  then
          begin
            Ports.Add(sPort);
            CloseHandle(hPort)
          end { hPort <> INVALID_HANDLE_VALUE }
      end { for nPort }
  finally
    Ports.EndUpdate()
  end { try-finally };
  if  bErrorIfNone  then
    if  Ports.Count = 0  then
      with  TGSkSerialCommunication.Create()  do
        try
          DoError('EnumPorts', gcnErrSerComm_InternalError,
                  dgettext('GSkPasLib', 'There are no serial ports on this computer'))
        finally
          Free()
        end { try-finally }
end { EnumPorts };


function  TGSkSerialCommunication.GetConnectionString() : string;
begin
  Result := Format('\\.\COM%u', [fPort])
end { GetConnectionString };


function  TGSkSerialCommunication.GetInputCount() : Integer;

var
  nErr:  DWORD;
  lStat:  TComStat;

begin  { GetInputCount }
  CheckIfConnected('GetInputCount');
  CheckResultOfAPI(ClearCommError(fSerialComm, nErr, Addr(lStat)),
                   'GetInputCount', gcnErrSerComm_InternalError);
  Result := lStat.cbInQue
end { GetInputCount };


function  TGSkSerialCommunication.IsConnected() : Boolean;
begin
  Result := fSerialComm <> INVALID_HANDLE_VALUE
end { IsConnected };


procedure  TGSkSerialCommunication.LogEnter(const   sMessage:  string);
begin
  if  Log <> nil  then
    Log.LogEnter(sMessage, gcStdLogLevel)
end { LogEnter };


procedure  TGSkSerialCommunication.LogEnter(const   sMessageFmt:  string;
                                            const   MessageArgs:  array of const);
begin
  if  Log <> nil  then
    Log.LogEnter(sMessageFmt, MessageArgs, gcStdLogLevel)
end { LogEnter };


procedure  TGSkSerialCommunication.LogExit(const   sMessage:  string);
begin
  if  Log <> nil  then
    Log.LogExit(sMessage, gcStdLogLevel)
end { LogExit };


// procedure  TGSkSerialCommunication.LogExit(const   sMessageFmt:  string;
//                                            const   MessageArgs:  array of const);
// begin
//   if  Log <> nil  then
//     Log.LogExit(sMessageFmt, MessageArgs, LogLevel)
// end { LogExit };


procedure  TGSkSerialCommunication.LogMessage(const   sMessageFmt:  string;
                                              const   MessageArgs:  array of const);
begin
  if  Log <> nil  then
    Log.LogMessage(sMessageFmt, MessageArgs, gcStdLogLevel)
end { LogMessage };


procedure  TGSkSerialCommunication.LogResultExit(const   Result:  Variant;
                                                 const   sMessage:  string);
begin
  if  Log <> nil  then
    Log.LogResultExit(Result, sMessage, gcStdLogLevel)
end { LogResultExit };


procedure  TGSkSerialCommunication.Notification(AComponent:  TComponent;
                                                Operation:  TOperation);
begin
  inherited;
  if  (Operation = opRemove)
      and (AComponent = fLog)  then
    fLog := nil
end { Notification };


procedure  TGSkSerialCommunication.LogMessage(const   sMessage:  string);
begin
  if  Log <> nil  then
    Log.LogMessage(sMessage, gcStdLogLevel)
end { LogMessage };


procedure  TGSkSerialCommunication.Purge();
begin
  LogEnter('TGSkSerialCommunication.Purge');
  CheckIfConnected('Purge');
  CheckResultOfAPI(PurgeComm(fSerialComm,
                             PURGE_TXABORT or PURGE_RXABORT
                               or PURGE_TXCLEAR or PURGE_RXCLEAR),
                   'Purge', gcnErrSerComm_InternalError);
  LogExit('TGSkSerialCommunication.Purge')
end { Purge };


function  TGSkSerialCommunication.ReadByte() : Byte;

var
  nTmp:  Cardinal;
  nCnt:  Integer;

begin  { ReadByte }
  Result := 0;
  LogEnter('TGSkSerialCommunication.ReadByte');
  CheckIfConnected('ReadByte');
  nCnt := fTimeout div gcTimeoutThreshold;
  repeat
    CheckResultOfAPI(ReadFile(fSerialComm, Result, 1, nTmp, nil),
                     'ReadByte', gcnErrSerComm_ReadError);
    Dec(nCnt);
  until  (nTmp <> 0)
         or  (nCnt <= 0);
  if  nTmp = 0  then
    try
      DoError('ReadByte', gcnErrSerComm_Timeout,
              dgettext('GSkPasLib', gcsErrSerComm_Timeout))
    except
      LogExit('TGSkSerialCommunication.ReadByte');
      raise
    end { try-except }
  else
    LogMessage('ReadByte --> $%0:2.2x   // %1:s, %0:d',
               [Result, QuoteStr(Chr(Result), '"')]);
  LogResultExit(Result, 'TGSkSerialCommunication.ReadByte');
end { ReadByte };


function  TGSkSerialCommunication.ReadData(var   Data:  TBytes;
                                           const   nBytesToRead:  Integer)
                                           : Integer;
var
  nBuf:  Byte;

begin  { ReadData }
  SetLength(Data, nBytesToRead);
  Result := 0;
  if  nBytesToRead > 0  then
    try
      repeat
        nBuf := ReadByte();
        Data[Result] := nBuf;
        Inc(Result)
      until  Result = nBytesToRead
    except
      on  eErrSer : EGSkSerialCommunicationError  do
        if  -eErrSer.ErrorCode in [-gcnErrSerComm_ReadError, -gcnErrSerComm_Timeout]  then
          SetLength(Data, Result)
        else
          raise
      else
        raise
    end { try-except }
end { ReadData };


procedure  TGSkSerialCommunication.SetBaudRate(const   Value:  TGSkSerialCommBaudRate);
begin
  SetProperty<TGSkSerialCommBaudRate>(fBaudRate, Value, 'SetBaudRate')
end { SetBaudRate };


procedure  TGSkSerialCommunication.SetDataBits(const   Value:  TGSkSerialCommDataBits);
begin
  SetProperty<TGSkSerialCommDataBits>(fDataBits, Value, 'SetDataBits')
end { SetDataBits };


procedure  TGSkSerialCommunication.SetError(const   nErrorCode:  Integer;
                                            const   sErrorInfo:  string);
begin
  fnErrorNum := nErrorCode;
  fsErrorMsg := sErrorInfo
end { SetError };


procedure  TGSkSerialCommunication.SetFlowControl(const   Value:  TGSkSerialCommFlowControl);
begin
  SetProperty<TGSkSerialCommFlowControl>(fFlowControl, Value, 'SetFlowControl')
end { SetFlowControl };


procedure  TGSkSerialCommunication.SetLog(const   Log:  TGSkLog);
begin
  if  fLog <> Log  then
    begin
      if  fLog <> nil  then
        fLog.RemoveFreeNotification(Self);
      fLog := Log;
      if  fLog <> nil  then
        fLog.FreeNotification(Self)
    end { fLog <> Log }
end { SetLog };


procedure  TGSkSerialCommunication.SetParityBits(const   Value:  TGSkSerialCommParityBits);
begin
  SetProperty<TGSkSerialCommParityBits>(fParityBits, Value, 'SetParityBits')
end { SetParityBits };


procedure  TGSkSerialCommunication.SetPort(const   Value:  TGSkSerialCommPort);
begin
  SetProperty<TGSkSerialCommPort>(fPort, Value, 'SetPort')
end { SetPort };


procedure  TGSkSerialCommunication.SetProperty<T>(var    Field:  T;
                                                  const   Value:  T;
                                                  const   sMehotd:  string);
begin
  if  not TEqualityComparer<T>.Default.Equals(Value, Field)  then
    begin
      CheckIfNotConnected(sMehotd);
      Field := Value
    end { Value <> Field }
end { SetProperty };


procedure  TGSkSerialCommunication.SetStopBits(const   Value:  TGSkSerialCommStopBits);
begin
  SetProperty<TGSkSerialCommStopBits>(fStopBits, Value, 'SetStopBits')
end { SetStopBits };


function  TGSkSerialCommunication.ToString() : string;

const
  csStopBits:  array[TGSkSerialCommStopBits] of  string = (
                   '1', '1.5', '2');
  csParity:  array[TGSkSerialCommParityBits] of  Char = (
                 'n', 'o', 'e', 'm', 's');
  csFlowControl:  array[TGSkSerialCommFlowControl] of  string = (
                      '-', 'RTS/CTS', 'XON/XOFF');
begin  { ToString }
  Result := Format('COM%d,%d,%d,%s,%s,%s',
                   [Port, Ord(BaudRate), DataBits, csStopBits[StopBits],
                    csParity[ParityBits], csFlowControl[FlowControl]])
end { ToString };


procedure  TGSkSerialCommunication.WriteByte(const   nData:  Byte);

var
  nTmp:  Cardinal;

begin  { WriteByte }
  LogEnter('TGSkSerialCommunication.WriteByte($%0:2.2x)   // %1:s, %0:u',
           [nData, QuoteStr(Chr(nData), '"')]);
  CheckIfConnected('WriteByte');
  CheckResultOfAPI(WriteFile(fSerialComm, nData, 1, nTmp, nil),
                   'WriteByte', gcnErrSerComm_WriteError);
  LogExit('TGSkSerialCommunication.WriteByte')
end { WriteByte };


procedure  TGSkSerialCommunication.WriteData(const   sData:  AnsiString);

var
  nWrt:  Cardinal;

begin  { WriteData }
  LogEnter('TGSkSerialCommunication.WriteData(%s)', [QuoteStr(UnicodeString(sData), '"')]);
  CheckIfConnected('WriteData(AnsiString)');
  CheckResultOfAPI(WriteFile(fSerialComm, sData[1], Length(sData), nWrt, nil),
                   'WriteData(AnsiString)', gcnErrSerComm_WriteError);
  LogExit('TGSkSerialCommunication.WriteData')
end { WriteData };


procedure  TGSkSerialCommunication.WriteData(const   Data:  TBytes);

const
  cnMaxLen = 75;

var
  nWrt:  Cardinal;

  function  DataToHex(const   Data:  TBytes)
                      : string;
  var
    nTmp:  Byte;
    nInp, nOut:  Integer;

    function  Digit(const   nValue:  Byte)
                    : Char;
    begin
      if  nValue <= 9  then
        Result := Chr(Ord('0') + nValue)
      else
        Result := Chr(Ord('A') + nValue - 10)
    end { Digit };

  begin  { DataToHex }
    SetLength(Result, Min(Length(Data), cnMaxLen) * 2);
    nOut := 0;
    for  nInp := Low(Data)  to  High(Data)  do
      begin
        nTmp := Data[nInp];
        Result[nOut + 1] := Digit(nTmp shr 4);
        Result[nOut + 2] := Digit(nTmp and $f);
        Inc(nOut, 2);
        if  nOut >= Length(Result)  then
          Break
      end { for nInp }
  end { DataToHex };

  function  DataToStr(const   Data:  TBytes)
                      : AnsiString;
  var
    nLen:  Integer;

  begin  { DataToStr }
    nLen := Min(Length(Data), cnMaxLen);
    SetLength(Result, nLen);
    Move(Data[0], Result[1], nLen)
  end { DataToStr };

begin  { WriteData }
  LogEnter('TGSkSerialCommunication.WriteData({%s})   // %s',
           [DataToHex(Data), QuoteStr(UnicodeString(DataToStr(Data)), '"')]);
  CheckIfConnected('WriteData(TBytes)');
  CheckResultOfAPI(WriteFile(fSerialComm, Data[0], Length(Data), nWrt, nil),
                   'WriteData(TBytes)', gcnErrSerComm_WriteError);
  LogExit('TGSkSerialCommunication.WriteData')
end { WriteData };

{$ENDREGION 'TGSkSerialCommunication'}


{$REGION 'EGSkSerialCommunicationError'}

constructor  EGSkSerialCommunicationError.Create(const   sMethod:  string;
                                                 const   nErrCode:  Integer;
                                                 const   sErrMsg:  string);
begin
  inherited Create('EGSkSerialCommunicationError.' + sMethod,
                   nErrCode, sErrMsg)
end { Create };


constructor  EGSkSerialCommunicationError.CreateFmt(const   sMethod:  string;
                                                    const   nErrCode:  Integer;
                                                    const   sErrMsgFmt:  string;
                                                    const   ErrMsgArgs:  array of const);
begin
  inherited CreateFmt('EGSkSerialCommunicationError.' + sMethod,
                      nErrCode, sErrMsgFmt, ErrMsgArgs)
end { CreateFmt };

{$ENDREGION 'EGSkSerialCommunicationError'}


end.

