﻿unit  Test.GSkPasLib.JSON;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


{$TYPEINFO ON}


uses
  SysUtils, DateUtils,
  TestFramework,
  GSkPasLib.JSON;


type
  TJSON_Test = class(TTestCase)
  strict private
    fJSON:  TGSkJSON2;
  public
    procedure  SetUp();                                              override;
    procedure  TearDown();                                           override;
  published
    procedure  TestEmptyObject();
    procedure  TestStringValue();
    procedure  TestIntegerValue();
    procedure  TestFloatValue();
    procedure  TestCurrencyValue();
    procedure  TestDateTimeValue();
    procedure  TestEscapeChar();
    procedure  TestQuotationChar();
    procedure  TestControlChars();
    procedure  TestObject();
    procedure  TestArray();
    procedure  TestArrayOfObjects();
  end { TJSON_Test };


implementation


uses
  GSkPasLib.ASCII;


{$REGION 'TJSON_Test'}

procedure  TJSON_Test.SetUp();
begin
  inherited;
  fJSON := TGSkJSON2.Create(False);  // single line, without indents
end { SetUp };


procedure  TJSON_Test.TearDown();
begin
  inherited;
  fJSON.Free()
end { TearDown };


procedure  TJSON_Test.TestArray();
begin
  Assert(not fJSON.FormattingIndented);
  fJSON.WriteStartArray()
         .WriteValue('Jeden')
         .WriteValue('Dwa')
         .WriteValue('Trzy')
       .WriteEndArray();
  CheckEqualsString('["Jeden","Dwa","Trzy"]', fJSON.ToString(), '#1');
  {-}
  fJSON.Clear();
  fJSON.WriteStartArray().WriteValue('Test').WriteEndArray();
  CheckEqualsString('["Test"]', fJSON.ToString(), '#2');

  fJSON.FormattingIndented := True;
  {-}
  fJSON.Clear();
  fJSON.WriteStartArray()
         .WriteValue('Jeden')
         .WriteValue('Dwa')
         .WriteValue('Trzy')
       .WriteEndArray();
  CheckEqualsString('['                        + sLineBreak
                  + '  "Jeden", "Dwa", "Trzy"' + sLineBreak
                  + ']',
                    fJSON.ToString(), '#3')
end { TestArray };


procedure  TJSON_Test.TestArrayOfObjects();
begin
  Assert(not fJSON.FormattingIndented);
  fJSON.WriteStartArray()
         .WriteStartObject()
           .WritePropertyValue('Name', 'Greg')
           .WritePropertyValue('Surname', 'Jump')
         .WriteEndObject()
         .WriteStartObject()
           .WritePropertyValue('Profession', 'Programmer')
           .WritePropertyValue('Experience', 'Great')
         .WriteEndObject()
       .WriteEndArray();
  CheckEqualsString('[{"Name":"Greg","Surname":"Jump"},{"Profession":"Programmer","Experience":"Great"}]',
                    fJSON.ToString(), '#1');

  {-}
  fJSON.FormattingIndented := True;
  fJSON.Clear();
  fJSON.WriteStartArray()
         .WriteStartObject()
           .WritePropertyValue('Name', 'Greg')
           .WritePropertyValue('Surname', 'Jump')
         .WriteEndObject()
         .WriteStartObject()
           .WritePropertyValue('Profession', 'Programmer')
           .WritePropertyValue('Experience', 'Great')
         .WriteEndObject()
       .WriteEndArray();
  CheckEqualsString('['                               + sLineBreak
                  + '  {'                             + sLineBreak
                  + '    "Name": "Greg",'             + sLineBreak
                  + '    "Surname": "Jump"'           + sLineBreak
                  + '  }, '                           + sLineBreak
                  + '  {'                             + sLineBreak
                  + '    "Profession": "Programmer",' + sLineBreak
                  + '    "Experience": "Great"'       + sLineBreak
                  + '  }'                             + sLineBreak
                  + ']',
                    fJSON.ToString(), '#2')
end { TestArrayOfObjects };


procedure  TJSON_Test.TestControlChars();

  procedure  CheckRange(const   sName:  string;
                        const   chMin, chMax:  Char);
  var
    sInp:  string;
    sExp:  string;
    chVal: Char;

  begin  { CheckRange }
    sInp := '';
    sExp := '';
    for  chVal := chMin  to  chMax  do
      begin
        sInp := sInp + chVal;
        case  chVal  of
          BS:  sExp := sExp + '\b';
          FF:  sExp := sExp + '\f';
          LF:  sExp := sExp + '\n';
          CR:  sExp := sExp + '\r';
          TAB: sExp := sExp + '\t';
          else sExp := sExp + Format('\u%4.4x', [Ord(chVal)])
        end { case chVal }
      end { for chVal };
    fJSON.Clear()
         .WriteStartObject()
           .WritePropertyValue(sName, sInp)
         .WriteEndObject();
    CheckEqualsString(Format('{"%s":"%s"}',
                             [sName, sExp]),
                      fJSON.ToString())
  end { CheckRange };

begin  { TestControlChars }
  fJSON.WriteStartObject()
         .WritePropertyValue('Backspace', 'Tekst z ' + BS + ' w napisie')
       .WriteEndObject();
  CheckEqualsString('{"Backspace":"Tekst z \b w napisie"}',
                    fJSON.ToString());
  fJSON.Clear()
       .WriteStartObject()
         .WritePropertyValue('FormFeed', 'Tekst z ' + FF + ' w napisie')
       .WriteEndObject();
  CheckEqualsString('{"FormFeed":"Tekst z \f w napisie"}',
                    fJSON.ToString());
  fJSON.Clear()
       .WriteStartObject()
         .WritePropertyValue('Powrót', 'Tekst z ' + CR + ' w napisie')
         .WritePropertyValue('Wysuw', 'Tekst z ' + LF + ' w napisie')
         .WritePropertyValue('Tabulacja', 'Tekst z ' + TAB + ' w napisie')
       .WriteEndObject();
  CheckEqualsString('{"Powrót":"Tekst z \r w napisie","Wysuw":"Tekst z \n w napisie","Tabulacja":"Tekst z \t w napisie"}',
                    fJSON.ToString());
  CheckRange('Dolne', #0, Pred(' '));
  CheckRange('DEL', DEL, DEL);
  CheckRange('Górne', #$80, #$9F)
end { TestControlChars };


procedure  TJSON_Test.TestCurrencyValue();

const
  csName = 'Liczba';
  cnValue = 2.4;

var
  lFmt:  TFormatSettings;
  sExp:  string;

begin  { TestFloatValue }
  fJSON.WriteStartObject()
         .WritePropertyValue(csName, cnValue)
       .WriteEndObject();
  {-}
  lFmt := TFormatSettings.Create();
  lFmt.DecimalSeparator := '.';
  sExp := Format('{"%s":%.1f}',
                 [csName, cnValue],
                 lFmt);
  {-}
  CheckEqualsString(sExp, fJSON.ToString())
end { TestCurrencyValue };


procedure  TJSON_Test.TestDateTimeValue();

const
  csNameDT = 'Pora';

var
  tmNow: TDateTime;
  sExp:  string;

begin  { TestDateTimeValue }
  tmNow := Now();
  fJSON.WriteStartObject()
         .WritePropertyValue(csNameDT, tmNow)
       .WriteEndObject();
  sExp := DateToISO8601(tmNow, False);   // yyyy-mm-ddThh:mm:ss.zzz+hh:mm
  Delete(sExp, 20, 4);                   //                    ^^^^
  CheckEqualsString(Format('{"%s":"%s"}',
                           [csNameDT, sExp]),
                    fJSON.ToString())
end { TestDateTimeValue };


procedure  TJSON_Test.TestEmptyObject();
begin
  fJSON.WriteStartObject()
       .WriteEndObject();
  CheckEqualsString('{}', fJSON.ToString());
end { TestEmptyObject };


procedure  TJSON_Test.TestEscapeChar();
begin
  fJSON.WriteStartObject()
         .WritePropertyValue('Ucieczka', 'Znak ''\'' jest wyjątkowy')
       .WriteEndObject();
  CheckEqualsString('{"Ucieczka":"Znak ''\\'' jest wyjątkowy"}',
                    fJSON.ToString())
end { TestEscapeChar };


procedure  TJSON_Test.TestFloatValue();

const
  csName = 'Kwotas';
  cnValue: Currency = 3.1415936;

var
  lFmt:  TFormatSettings;
  sExp:  string;

begin  { TestFloatValue }
  fJSON.WriteStartObject()
         .WritePropertyValue(csName, cnValue)
       .WriteEndObject();
  {-}
  lFmt := TFormatSettings.Create();
  lFmt.DecimalSeparator := '.';
  sExp := Format('{"%s":%.4f}',
                 [csName, cnValue],
                 lFmt);
  {-}
  CheckEqualsString(sExp, fJSON.ToString())
end { TestFloatValue };


procedure  TJSON_Test.TestIntegerValue();

const
  csName = 'Liczba';
  cnValue = 123;

begin  { TestIntegerValue }
  fJSON.WriteStartObject()
         .WritePropertyValue(csName, cnValue)
       .WriteEndObject();
  CheckEqualsString(Format('{"%s":%d}',
                           [csName, cnValue]),
                    fJSON.ToString())
end { TestIntegerValue };


procedure  TJSON_Test.TestObject();
begin
  Assert(not fJSON.FormattingIndented);
  fJSON.WriteStartObject()
         .WritePropertyValue('Name', 'Greg')
         .WritePropertyValue('Surname', 'Jump')
       .WriteEndObject();
  CheckEqualsString('{"Name":"Greg","Surname":"Jump"}', fJSON.ToString(), '#1');
  {-}
  fJSON.Clear();
  fJSON.FormattingIndented := True;
  fJSON.WriteStartObject()
         .WritePropertyValue('Name', 'Greg')
         .WritePropertyValue('Surname', 'Jump')
       .WriteEndObject();
  CheckEqualsString('{'                    + sLineBreak
                   +'  "Name": "Greg",'    + sLineBreak
                   +'  "Surname": "Jump"' + sLineBreak
                   + '}',
                    fJSON.ToString(), '#2')
end { TestObject };


procedure  TJSON_Test.TestQuotationChar();
begin
  fJSON.WriteStartObject()
         .WritePropertyValue('Nazwa', 'Znak " jest znakiem specjalnym')
       .WriteEndObject();
  CheckEqualsString('{"Nazwa":"Znak \" jest znakiem specjalnym"}',
                    fJSON.ToString())
end { TestQuotationChar };


procedure  TJSON_Test.TestStringValue();

const
  csName = 'Text';
  csValue = 'String value';

begin  { TestStringValue }
  fJSON.WriteStartObject()
         .WritePropertyValue(csName, csValue)
       .WriteEndObject();
  CheckEqualsString(Format('{"%s":"%s"}',
                           [csName, csValue]),
                    fJSON.ToString())
end { TestStringValue };

{$ENDREGION 'TJSON_Test'}


initialization
  RegisterTest(TJSON_Test.Suite())


end.

