﻿{: @exclude
   Wewnętrznych modułów biblioteki nie komentuję (na razie).

   @abstract Definiuje edytor właściwości dla komponentu TGSkDataEmbedded.  }
unit  GSkPasLib.PropEdDataEmbedded;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  DesignIntf, DesignEditors, SysUtils, Dialogs, Classes, TypInfo;


const
  csDataEmbedded   = 'Dane osadzone';
  csAllFilesFilter = 'Wszystkie pliki|*.*';


type
  TPropEdDataEmbedded = class(TPropertyEditor)
  public
    function  GetAttributes() : TPropertyAttributes;                 override;
    function  GetValue() : string;                                   override;
    procedure  SetValue(const   Value:  string);                     override;
    procedure  Edit();                                               override;
  end { TPropEdDataEmbedded };


function  GetData() : TFileStream;


implementation


uses
  GSkPasLib.PasLibReg;


function  GetData() : TFileStream;
begin
  Result := nil;
  with  TOpenDialog.Create(nil)  do
    try
      Filter := csAllFilesFilter;
      Options := [ofPathMustExist, ofFileMustExist, ofShareAware,
                  ofNoChangeDir, ofEnableSizing];
      if  Execute()  then
        Result := TFileStream.Create(FileName,
                                     fmOpenRead or fmShareDenyWrite)
    finally
      Free()
    end { try-finally }
end { GetData };


{ TPropEdDataEmbedded }

procedure  TPropEdDataEmbedded.Edit();

var
  lFile:  TFileStream;

begin  { Edit }
  inherited;
  try
    lFile := GetData();
    if  lFile <> nil  then
      try
        SetOrdValue(Integer(lFile))
      finally
        lFile.Free()
      end { try-finally }
  except
    on  eErr : Exception  do
      ShowDesingError(eErr)
  end { try-except }
end { Edit };


function  TPropEdDataEmbedded.GetAttributes() : TPropertyAttributes;
begin
  Result := [paMultiSelect, paDialog, paReadOnly{, paRevertable}]
end { GetAttributes };


function  TPropEdDataEmbedded.GetValue() : string;
begin
  Result := '(' + csDataEmbedded + ')'
end { GetValue };


procedure  TPropEdDataEmbedded.SetValue(const   Value:  string);
begin
  inherited;
  SetStrValue(csDataEmbedded)
end { SetValue };


end.
