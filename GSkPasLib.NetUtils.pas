﻿{: @abstract Podprogramy pomocnicze dla programów sieciowych.

   Moduł @name zawiera podprogramy pomocnicze, które mogą się przydać do pisania
   programów komunikujących się przez sieć. }
unit  GSkPasLib.NetUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


uses
  IdTCPClient;


{: @abstract Sprawdza, czy port jest dostępny.

   Port może być niedostępny z różnych powodów. Na przykład serwer może nie działać
   albo komunikacja z serwerem może być blokowana przez zaporę internetową,
   albo serwer korzysta z innego portu.

   @param(sHost Adres lub nazwa komputera, na którym powinien działać badany serwer.)
   @param(nPort Numer portu, poprzez który można komunikować się z badanym serwerem.)

   @returns(Wynik funkcji wskazuje, czy pod wskazanym adresem i portem jest jakiś
            serwer.) }
function  IsPortActive(const   sHost:  string;
                       const   nPort:  Word)
                       : Boolean;


implementation


function  IsPortActive(const   sHost:  string;
                       const   nPort:  Word)
                       : Boolean;
var
  tcpClient:  TIdTCPClient;

begin  { IsPortActive }
  Result := False;
  try
    tcpClient := TIdTCPClient.Create(nil);
    try
      tcpClient.Host := sHost;
      tcpClient.Port := nPort;
      tcpClient.Connect();
      Result := True
    finally
      tcpClient.DisposeOf()
    end { try-finally }
  except
    { OK }
  end { try-except }
end { IsPortActive };


end.

