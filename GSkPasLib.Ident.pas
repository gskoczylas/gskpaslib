﻿{: @abstract Weryfikacja różnych identyfikatorów.

   Moduł zawiera funkcje sprawdzające poprawność różnych identyfikatorów,
   takich jak PESEL, NIP, REGON, numer konta bankowego itp. }
unit  GSkPasLib.Ident;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$IF CompilerVersion > 22} // XE
  {$LEGACYIFEND ON}
{$IFEND}


uses
  {$IF CompilerVersion > 22} // XE
    System.SysUtils;
  {$ELSE}
    SysUtils;
  {$IFEND}


type
  {$IF CompilerVersion < 22} // XE
    {: @exclude }
    TDate = TDateTime;
  {$IFEND}

  TStatusNIP = (snCorrect, snEmpty, snWrongChecksum, snIncorrect);

  {: @abstract Płeć osoby.
     @seeAlso(PESELToSex) }
  TSex = (sexUnknown, sexMale, sexFemale);


{: @abstract Wylicza sumę kontrolną NIP.
   @seeAlso(NIPisCorrect) }
function  NIPchecksum({const}   sNIP:  string)
                      : Char;

{: @abstract Informuje czy NIP jest poprawny.
   @seeAlso(StatusNIP) }
function  NIPisCorrect(const   sNIP:  string)
                       : Boolean;

{: @abstract Zwraca status identyfikatora NIP.
   @seeAlso(NIPisCorrect) }
function  StatusNIP({const}   sNIP:  string)
                    : TStatusNIP;

{: @abstract Wylicza sumę kontrolną REGON.
   @seeAlso(REGONisCorrect) }
function  REGONchecksum({const}   sREGON:  string)
                        : Char;

{: @abstract Informuje czy REGON jest poprawny. }
function  REGONisCorrect({const}   sREGON:  string)
                         : Boolean;

{: @abstract Sprawdza, czy numer konta bankowego jest poprawny.

   Numer konta bankowego może być opcjonalnie poprzedzony literami „PL” (konto
   w formacie IBAN). }
function  NRBisCorrect({const}   sNRB:  string)
                       : Boolean;

{: @abstract Wylicza sumę kontrolną PESEL.
   @seeAlso(PESELisCorrect) }
function  PESELchecksum({const}   sPESEL:  string)
                        : Char;

{: @abstract Sprawdza, czy PESEL jest poprawny.

   Funkcja @name sprawdza, czy PESEL jest poprawny.

   @param(sPESEL Sprawdzany identyfikator PESEL.)
   @param(tmDate Jeżeli ta data jest różna od zera, to funkcja sprawdza, czy
                 data zakodowana w identyfikatorze PESEL jest taka sama.)
   @param(Sex Jeżeli ten parametr ma wartość różną od @code(sexUnknown), to
              funkcja sprawdza, czy płeć zakodowana w identyfikatorze PESEL jest
              taka sama.)

   @seeAlso(PESELtoDate)
   @seeAlso(PESELtoSex) }
function  PESELisCorrect({const} sPESEL:  string;
                         const   tmDate:  TDate = 0;
                         const   Sex:  TSex = sexUnknown)
                         : Boolean;

{: @abstract Zwraca datę urodzenia osoby.
   @seeAlso(PESELisCorrect) }
function  PESELtoDate({const}   sPESEL:  string)
                      : TDate;

{: @abstract Zwraca płeć osoby.

   Funkcja @name zwraca informację o płci osoby. Jeżeli PESEL nie jest poprawny,
   to funkcja może zwrócić @code(sexUnknown) (lub niepoprawną informację).
   @seeAlso(PESELisCorrect) }
function  PESELtoSex({const}   sPESEL:  string)
                     : TSex;


implementation


uses
  GSkPasLib.PasLibInternals, GSkPasLib.ASCII, GSkPasLib.StrUtils;


const
  gcnLengthNIP   = 10;
  gcnLengthREGON =  9;
  gcnLengthNRB   = 26;
  gcnLengthPESEL = 11;


{$REGION 'Local subroutines'}

function  CharToDigit(const   chValue:  Char)
                      : Integer;                                     inline;
begin
  Result := Ord(chValue) - Ord('0')
end { CharToDigit };


function  Checksum(const   sText, sFactors:  string)
                   : Char;
var
  nSum, nInd:  Integer;

begin  { Checksum }
  if  Length(sText) < (Length(sFactors) - 1)  then
    Result := #0
  else
    begin
      nSum := 0;
      for  nInd := 1  to  Length(sFactors)  do
        Inc(nSum, CharToDigit(sText[nInd]) * CharToDigit(sFactors[nInd]));
      Result := Chr(Ord('0') + (nSum mod 11) mod 10)
    end { Length(sTest) >= Length(sWagi) - 1 }
end { CyfKontr };

{$ENDREGION 'Local subroutines'}


{$REGION 'Global subroutines'}

function  NIPchecksum({const}  sNIP:  string)
                      : Char;
const
  csFactors = '657234567';

begin  { NIPchecksum }
  sNIP := CharsOnly(sNIP, ['0'..'9']);
  if  not(Length(sNIP) in [gcnLengthNIP-1, gcnLengthNIP])  then
    raise  EGSkPasLibError.Create('NIPChecksum', 'Incorrect length of the NIP');
  Result := Checksum(sNIP, csFactors)
end { NIPchecksum };


function  NIPisCorrect(const   sNIP:  string)
                       : Boolean;
begin
  Result := StatusNIP(sNIP) = snCorrect
end { NIPisCorrect };


function  StatusNIP({const}   sNIP:  string)
                    : TStatusNIP;
var
  chInd:  Char;

begin  { StatusNIP }
  sNIP := CharsOnly(sNIP, ['0'..'9', 'A'..'Z', 'a'..'z']);
  {-}
  if  SameText(Copy(sNIP, 1, 2), 'PL')  then
    Delete(sNIP, 1, 2);
  {-}
  if  sNIP = ''  then
    Result := snEmpty
  else if  Length(sNIP) <> gcnLengthNIP  then
    Result := snIncorrect
  else
    begin
      try
        if  NIPchecksum(sNIP) = sNIP[gcnLengthNIP]  then
          Result := snCorrect
        else
          Result := snWrongChecksum
      except
        on  eErr : EGSkPasLibError  do
          Result := snIncorrect
        else
          raise
      end { try-except };
      {-}
      if  Result = snCorrect  then
        begin
          if  sNIP = '1234567890'  then
            Exit(snIncorrect);
          for  chInd in ['0'..'9']  do
            if  sNIP = StringOfChar(chInd, gcnLengthNIP)  then
              Exit(snIncorrect)
        end { Result = snCorrect }
    end { Length(sNIP) = gcnLengthNIP }
end { StatusNIP };


function  REGONchecksum({const}   sREGON:  string)
                        : Char;
const
  csFactors = '89234567';

begin  { REGONChecksum }
  sREGON := CharsOnly(sREGON, ['0'..'9']);
  if  not(Length(sREGON) in [gcnLengthREGON-1, gcnLengthREGON])  then
    raise  EGSkPasLibError.Create('REGONChecksum', 'Incorrect length of the REGION');
  Result := Checksum(sREGON, csFactors)
end { REGONchecksum };


function  REGONisCorrect({const}   sREGON:  string)
                         : Boolean;
begin
  sREGON := CharsOnly(sREGON, ['0'..'9']);
  Result := (Length(sREGON) = gcnLengthREGON)
            and (REGONchecksum(sREGON) = sREGON[gcnLengthREGON])
end { REGONisCorrect };


function  NRBisCorrect({const}   sNRB:  string)
                       : Boolean;
const
  cFactors:  array[1..30] of  Integer = (1, 10, 3, 30, 9, 90, 27, 76, 81, 34, 49,
                                         5, 50, 15, 53, 45, 62, 38, 89, 17, 73,
                                         51, 25, 56, 75, 71, 31, 19, 93, 57);
var
  nSum:  Integer;
  nInd:  Integer;

begin  { NRBisCorrect }
  Result := False;
  sNRB := CharsBut(sNRB, [' ', '-']);
  if  SameText(Copy(sNRB, 1, 2), 'PL')  then
    Delete(sNRB, 1, 2);
  if  Length(sNRB) <> gcnLengthNRB  then
    Exit;
  sNRB := Copy(sNRB, 3, MaxInt)
          + '2521'   // '25' = zakodowane 'PL'
          + Copy(sNRB, 1, 2);
  nSum := 0;
  for  nInd := 1  to  30  do
    try
      nSum := nSum
              + cFactors[31 - nInd]
                * CharToDigit(sNRB[nInd])
    except
      Exit
    end { try-except };
  Result := (nSum mod 97) = 1
end { NRBisCorrect };


function  PESELchecksum({const}   sPESEL:  string)
                        : Char;
begin
  sPESEL := CharsOnly(sPESEL, ['0'..'9']);
  if  not(Length(sPESEL) in [gcnLengthPESEL-1, gcnLengthPESEL])  then
    raise  EGSkPasLibError.Create('PESELChecksum', 'Incorrect length of the PESEL');
  Result := Chr(Ord('0')
            + (10 - ((CharToDigit(sPESEL[1])    )        +
                     (CharToDigit(sPESEL[2]) * 3) mod 10 +
                     (CharToDigit(sPESEL[3]) * 7) mod 10 +
                     (CharToDigit(sPESEL[4]) * 9) mod 10 +
                     (CharToDigit(sPESEL[5])    )        +
                     (CharToDigit(sPESEL[6]) * 3) mod 10 +
                     (CharToDigit(sPESEL[7]) * 7) mod 10 +
                     (CharToDigit(sPESEL[8]) * 9) mod 10 +
                     (CharToDigit(sPESEL[9])    )        +
                     (CharToDigit(sPESEL[10])* 3) mod 10) mod 10) mod 10)
end { PESELchecksum };


function  PESELisCorrect({const} sPESEL:  string;
                         const   tmDate:  TDate;
                         const   Sex:  TSex)
                         : Boolean;
begin
  sPESEL := CharsOnly(sPESEL, ['0'..'9']);
  Result := (Length(sPESEL) = gcnLengthPESEL)
            and (PESELchecksum(sPESEL) = sPESEL[gcnLengthPESEL]);
  if  Result
      and (tmDate <> 0)  then
    Result := PESELtoDate(sPESEL) = tmDate;
  if  Result
      and (Sex <> sexUnknown)  then
    Result := PESELtoSex(sPESEL) = Sex
end { PESELisCorrect };


function  PESELtoDate({const}   sPESEL:  string)
                      : TDate;
var
  nDay, nMonth, nYear:  Word;

begin  { PESELtoDate }
  sPESEL := CharsOnly(sPESEL, ['0'..'9']);
  if  Length(sPESEL) <> gcnLengthPESEL  then
    raise  EGSkPasLibError.Create('PESELToDate', 'Incorrect length of the PESEL');
  try
    nYear := StrToInt(Copy(sPESEL, 1, 2));
    nMonth := StrToInt(Copy(sPESEL, 3, 2));
    nDay := StrToInt(Copy(sPESEL, 5, 2));
    case  nMonth  of
      1..12:
        begin
          Inc(nYear, 1900)
        end { 1..12 --> XX century };
      21..32:
        begin
          Dec(nMonth, 20);
          Inc(nYear, 2000)
        end { 21..32 --> XXI century };
      41..52:
        begin
          Dec(nMonth, 40);
          Inc(nYear, 2100)
        end { 41..52 --> XXII century };
      61..72:
        begin
          Dec(nMonth, 60);
          Inc(nYear, 2200)
        end { 61..72 --> XXIII century  };
      81..92:
        begin
          Dec(nMonth, 80);
          Inc(nYear, 1800)
        end { 81..92 --> XIX century }
    end { case nMonth };
    Result := EncodeDate(nYear, nMonth, nDay)
  except
    raise  EGSkPasLibError.Create('PESELToDate', 'PESEL is not correct')
  end { try-except }
end { PESELtoDate };


function  PESELtoSex({const}   sPESEL:  string)
                     : TSex;
begin
  sPESEL := CharsOnly(sPESEL, ['0'..'9']);
  if  Length(sPESEL) = gcnLengthPESEL  then
    if  CharInSet(sPESEL[gcnLengthPESEL - 1], ['1', '3', '5', '7', '9'])  then
      Result := sexMale
    else
      Result := sexFemale
  else
    Result := sexUnknown
end { PESELtoSex };


{$ENDREGION 'Global subroutines'}


end.
