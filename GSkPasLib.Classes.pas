﻿{: @abstract Klasy rozszerzające standardowe klasy Delphi.

   Moduł @name zawiera definicje klas lub klas pomocniczych (@code(class helper))
   rozszerzających funkcjonalność standardowych klas Delphi. }
unit  GSkPasLib.Classes;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    System.Classes, System.SysUtils, System.StrUtils,
  {$ELSE}
    Classes, SysUtils, StrUtils,
  {$IFEND}
  {$IFDEF UNICODE}
    {$IF CompilerVersion > 22} // XE
      System.Generics.Collections,
    {$ELSE}
      Generics.Collections,
    {$IFEND}
  {$ENDIF UNICODE}
  {$IF CompilerVersion = 22}
    AnsiStrings,
  {$ELSEIF CompilerVersion > 22}
    System.AnsiStrings,
  {$IFEND}
  {$IF CompilerVersion > 22}
    System.Win.Registry, System.IniFiles;
  {$ELSE}
    Registry, IniFiles;
  {$IFEND}


type
  {: @abstract(Lista napisów z obiektami interpretowanymi jako liczby całkowite
               lub wartości logiczne.)}
  TStringsHlp = class helper for TStrings
  strict private
    function  GetAsInteger(const   nIndex:  Integer)
                           : Integer;
    function  GetAsBoolean(const   nIndex:  Integer)
                           : Boolean;
    procedure  SetAsInteger(const   nIndex, nValue:  Integer);
    procedure  SetAsBoolean(const   nIndex:  Integer;
                            const   bValue:  Boolean);
  public
    function  List(const   sDelimiter:  string = ', ')
                   : string;
    function  AddInteger(const   sValue:  string;
                         const   nValue:  Integer)
                         : Integer;                                  inline;
    function  AddBoolean(const   sValue:  string;
                         const   bValue:  Boolean)
                         : Integer;
    function  IndexOfInteger(const   nValue:  Integer)
                             : Integer;
    function  IndexOfBoolean(const   bValue:  Boolean)
                             : Integer;
    function  NameExists(const   sName:  string)
                         : Boolean;                                  inline;
    function  ObjectExists(const   aObject:  TObject)
                           : Boolean;                                inline;
    function  ValueExists(const   sValue:  string)
                          : Boolean;                                 overload;
    function  ValueExists(const   nValue:  Integer)
                          : Boolean;                                 overload; inline;
    function  ValueExists(const   bValue:  Boolean)
                          : Boolean;                                 overload; inline;
    function  Contains(const   sValue:  string)
                       : Boolean;                                    overload;
    procedure  InsertInteger(const   nIndex:  Integer;
                             const   sValue:  string;
                             const   nValue:  Integer);
    procedure  InsertBoolean(const   nIndex:  Integer;
                             const   sValue:  string;
                             const   bValue:  Boolean);
    procedure  ReplaceObject(const   nIndex:  Integer;
                             const   sValue:  string;
                             const   aValue:  TObject);
    procedure  ReplaceInteger(const   nIndex:  Integer;
                              const   sValue:  string;
                              const   nValue:  Integer);
    procedure  ReplaceBoolean(const   nIndex:  Integer;
                              const   sValue:  string;
                              const   bValue:  Boolean);
    procedure  DeleteName(const   sName:  string);
    {-}
    property  ObjAsInteger[const   nIndex:  Integer] : Integer
              read  GetAsInteger
              write SetAsInteger;
    property  ObjAsBoolean[const   nIndex:  Integer] : Boolean
              read  GetAsBoolean
              write SetAsBoolean;
  end { TStringsHlp };

  {: @abstract Dodatkowe metody ułatwiające korzystanie ze strumieni. }
  TStreamHlp = class helper for TStream
  public
    function  CopyFrom(Source:  TStream;
                       Count:  Int64 = 0)
                       : Int64;                                      inline;
    function  Eof() : Boolean;                                       inline;
    function  ReadDataByte() : Byte;                                 inline;
    function  ReadDataChar() : Char;                                 inline;
   {$IFDEF UNICODE}
    function  ReadDataAnsiChar() : AnsiChar;                         inline;
   {$ENDIF UNICODE}
    function  ReadDataString({const}   nLength:  Integer)
                             : string;                               overload;
    function  ReadDataString() : string;                             overload;
   {$IFDEF UNICODE}
    function  ReadDataAnsiString({const}   nLength:  Integer)
                                 : AnsiString;                       overload;
    function  ReadDataAnsiString() : AnsiString;                     overload;
   {$ENDIF UNICODE}
    function  WriteData(const   Buffer:  TBytes)
                        : NativeInt;                                 overload; inline;
    procedure  WriteDataChar(const   chData:  Char);                 inline;
    procedure  WriteDataString(const   sData:  string);              overload;
    procedure  WriteDataString(const   pszData:  PChar);             overload;
   {$IFDEF UNICODE}
    procedure  WriteDataAnsiString(const   sData:  AnsiString);      overload;
    procedure  WriteDataAnsiString(const   pszData:  PAnsiChar);     overload;
   {$ENDIF UNICODE}
  end { TStreamHlp };

  {: @abstract Czytanie z Registry z wartościami domyślnymi.

     Klasa @name rozszerza metody odczytujące dane z Registry, zdefiniowane przez
     standardową klasę @code(TRegistry), dodając opcjonalny parametr wskazujący
     wartość domyślną. Wartość domyślna zwracana jest wtedy, gdy w Registry nie
     ma wartości o wskazanej nazwie. }
  TRegistryHlp = class helper for TRegistry
  public
    function  ReadCurrency(const   sName:  string;
                           const   nDefault:  Currency = 0.0)
                           : Currency;
    function  ReadBool(const   sName:  string;
                       const   bDefault:  Boolean = False)
                       : Boolean;
    {: @abstract Odczytuje wartość logiczną zapisaną jako napis.

       Metoda @name zwraca wartość logiczną, która została zapisana w Registry
       jako napis (jako wynik funkcji @code(BoolToStr(Value, True)) lub jako
       wynik funkcji @code(BoolToStr(Value, False)). }
    function  ReadBoolStr(const   sName:  string;
                          const   bDefault:  Boolean = False)
                          : Boolean;
    function  ReadDateTime(const   sName:  string;
                           const   tmDefault:  TDateTime = 0)
                           : TDateTime;
    function  ReadFloat(const   sName:  string;
                        const   nDefault:  Double = 0.0)
                        : Double;
    {: @abstract Odczytuje liczbę zapisaną jako napis.

       Metoda @name zwraca liczbę zapisaną w Registry jako napis. }
    function  ReadFloatStr(const   sName:  string;
                           const   nDefault:  Double = 0.0)
                           : Double;
    function  ReadInteger(const   sName:  string;
                          const   nDefault:  Integer = 0)
                          : Integer;
    {: @abstract Odczytuje liczbę całkowitą zapisaną jako napis.

       Metoda @name zwraca liczbę zapisaną w Registry jako napis. }
    function  ReadIntegerStr(const   sName:  string;
                             const   nDefault:  Integer = 0)
                             : Integer;
    function  ReadString(const   sName:  string;
                         const   sDefault:  string = '')
                         : string;
    {: @abstract Zapisuje wartość logiczną jako napis.

       Metoda @name zapisuje w Registry wskazaną wartość jak wynik funkcji
       @code(BoolToStr(bValue, True)). }
    procedure  WriteBoolStr(const   sName:  string;
                            const   bValue:  Boolean);
    {: @abstract Zapisuje wartość jako napis.

       Metoda @name zapisuje w Registry wskazaną wartość jako napis. }
    procedure  WriteIntegerStr(const   sName:  string;
                               const   nValue:  Integer);
    {: @abstract Zapisuje wartość jako napis.

       Metoda @name zapisuje w Registry wskazaną wartość jako napis. }
    procedure  WriteFloatStr(const   sName:  string;
                             const   nValue:  Double);
    {: @abstract Pobiera nazwy i wartości kluczy.

       Metoda @name zwraca nazwy i wartości wszystkich @bold(wartosci). W wyniku
       zwracana jest lista napisów w postaci @code(Nazwa=Wartość). }
    procedure  GetValuesData(const   lData:  TStrings);
  end { TRegistryHlp };

  {: @abstract Wyjątek z numerem błędu (oprócz opisu błędu). }
  EGSkExceptionErrNo = class(Exception)
  public
    type
      TExceptionFlag = (efWarning, efAlreadyLogged);
      TExceptionFlags = set of  TExceptionFlag;
  strict private
    var
      fsMessageEnglish:  string;
      fnErrorCode:  Integer;
      fsetFlags:    TExceptionFLags;
    function  GetMessageNative() : string;                           inline;
    function  GetIsWarning() : Boolean;                              inline;
    function  GetIsLogged() : Boolean;                               inline;
    procedure  SetIsLogged(const   Value:  Boolean);                 inline;
  public
    constructor  Create(const   sMsg:  string);                      overload;
                        {$IFNDEF GetText} deprecated 'Use the bilingual version'; {$ENDIF}
    constructor  CreateFmt(const   sFmt:  string;
                           const   Args:  array of const);           overload;
                           deprecated 'Use the bilingual version';
    constructor  Create(const   nErrCode:  Integer;
                        const   sErrInfo:  string;
                        const   setFlags:  TExceptionFlags = []);    overload;
                        {$IFNDEF GetText} deprecated 'Use the bilingual version'; {$ENDIF}
    constructor  Create(const   nErrCode:  Integer;
                        const   sErrInfoNative, sErrInfoEnglish:  string;
                        const   setFlags:  TExceptionFlags = []);    overload;
    constructor  CreateFmt(const   nErrCode:  Integer;
                           const   sErrInfoFmt:  string;
                           const   ErrInfoArgs:  array of const;
                           const   setFlags:  TExceptionFlags = []); overload;
                           {$IFNDEF GetText} deprecated 'Use the bilingual version'; {$ENDIF}
    constructor  CreateFmt(const   nErrCode:  Integer;
                           const   sErrInfoNativeFmt, sErrInfoEnglisthFmt:  string;
                           const   ErrInfoArgs:  array of const;
                           const   setFlags:  TExceptionFlags = []); overload;
    {-}
    class procedure  Throw(const   nErrCode:  Integer;
                           const   sErrMsg:   string;
                           const   setFlags:  TExceptionFlags = []); overload;
                           {$IFNDEF GetText} deprecated 'Use the bilingual version'; {$ENDIF}
    class procedure  Throw(const   nErrCode:  Integer;
                           const   sErrFmt:   string;
                           const   ErrArgs:   array of const;
                           const   setFlags:  TExceptionFlags = []); overload;
                           {$IFNDEF GetText} deprecated 'Use the bilingual version'; {$ENDIF}
    class procedure  Throw(const   nErrCode:  Integer;
                           const   sErrFmt:   string;
                           const   sErrArg:   string;
                           const   setFlags:  TExceptionFlags = []); overload;
                           {$IFNDEF GetText} deprecated 'Use the bilingual version'; {$ENDIF}
    {-}
    property  ErrorCode:  Integer
              read  fnErrorCode;
    property  MessageNative:  string
              read  GetMessageNative;
    property  MessageEnglish:  string
              read  fsMessageEnglish;
    property  Flags:  TExceptionFlags
              read  fsetFlags;
    property  IsWaring:  Boolean
              read  GetIsWarning;
    property  IsLogged:  Boolean
              read  GetIsLogged
              write SetIsLogged;
  end { EGSkExceptionErrNo };

  TExceptionClass = class of Exception;
  TGSkExceptionErrNoClass = class of EGSkExceptionErrNo;

  TGSkExceptionErrNo = EGSkExceptionErrNo
                       deprecated 'Use the EGSkExceptionErrNo instead';

  {: @abstract Dodatkowe funkcje ułatwiające generowanie wyjątków. }
  TExceptionHlp = class helper for Exception
  public
    class procedure  Throw(const   sErrMsg:  string);                overload;
    class procedure  Throw(const   sErrFmt:  string;
                           const   ErrArgs:  array of const);        overload;
    class procedure  Throw(const   sErrFmt:  string;
                           const   sErrArg:  string);                overload; // inline;
  end { TExceptionHlp };

  {: @abstract(Ułatwia zapisywanie i pobieranie wartości typu @code(Currency)
               z pliku konfiguracyjnego.) }
  TCustomIniFileHlp = class helper for TCustomIniFile
  public
    function  ReadCurrency(const   sSection, sName:  string;
                           const   nDefault:  Currency)
                           : Currency;
    procedure  WriteCurrency(const   sSection, sName:  string;
                             const   nValue:  Currency);
  end { TIniFileHlp };

  TIniFileHlp = class helper for TIniFIle
  public
    class function  ReadStringEx(const   sFilePath, sSection, sName:  string;
                                 const   sDefault:  string = '')
                                 : string;
    class function  ReadIntegerEx(const   sFilePath, sSection, sName:  string;
                                  const   nDefault:  Integer = 0)
                                  : Integer;
    class function  ReadBoolEx(const   sFilePath, sSection, sName:  string;
                               const   bDefault:  Boolean = False)
                               : Boolean;
    class function  ReadCurrencyEx(const   sFilePath, sSection, sName:  string;
                                   const   nDefault:  Currency = 0)
                                   : Currency;
    class procedure  WriteStringEx(const   sFilePath, sSection, sName:  string;
                                   const   sValue:  string);
    class procedure  WriteIntegerEx(const   sFilePath, sSection, sName:  string;
                                    const   nValue:  Integer);
    class procedure  WriteBoolEx(const   sFilePath, sSection, sName:  string;
                                 const   bValue:  Boolean);
    class procedure  WriteCurrencyEx(const   sFilePath, sSection, sName:  string;
                                     const   nValue:  Currency);
  end { TIniFileHlp };

{$IF Declared(TArray)}

  {: @abstract Dodatkowe funkcja ułatwiające korzystanie z tablic @code(TArray). }
  TArrayHlp = class helper for TArray
  public
    class function  ToList<T>(const   Values:  array of T;
                              const   fnToString:  TFunc<T,string>;
                              const   sSeparator:  string = ', ')
                              : string;
  end { TArrayHlp };

{$IFEND}


implementation


uses
  GSkPasLib.ASCII, GSkPasLib.StrUtils;


{$REGION 'TStringsHlp'}

function  TStringsHlp.AddBoolean(const   sValue:  string;
                                 const   bValue:  Boolean)
                                 : Integer;
begin
  Result := AddObject(sValue, TObject(LongBool(bValue)))
end { AddBoolean };


function  TStringsHlp.AddInteger(const   sValue:  string;
                                 const   nValue:  Integer)
                                 : Integer;
begin
  Result := AddObject(sValue, TObject(nValue))
end { AddInteger };


function  TStringsHlp.Contains(const   sValue:  string)
                               : Boolean;
begin
  Result := IndexOf(sValue) >= 0
end { TStringsHlp };


procedure  TStringsHlp.DeleteName(const   sName:  string);

var
  nInd:  Integer;

begin  { DeleteName }
  nInd := IndexOfName(sName);
  if  nInd >= 0  then
    Delete(nInd)
end { DeleteName };


function  TStringsHlp.GetAsBoolean(const   nIndex:  Integer)
                                   : Boolean;
begin
  Result := LongBool(Objects[nIndex])
end { GetAsBoolean };


function  TStringsHlp.GetAsInteger(const   nIndex:  Integer)
                                   : Integer;
begin
  Result := Integer(Objects[nIndex])
end { GetAsInteger };


function  TStringsHlp.IndexOfBoolean(const   bValue:  Boolean)
                                     : Integer;
begin
  Result := IndexOfObject(TObject(LongBool(bValue)))
end { IndexOfBoolean };


function  TStringsHlp.IndexOfInteger(const   nValue:  Integer)
                                     : Integer;
begin
  Result := IndexOfObject(TObject(nValue))
end { IndexOfInteger };


procedure  TStringsHlp.InsertBoolean(const   nIndex:  Integer;
                                     const   sValue:  string;
                                     const   bValue:  Boolean);
begin
  InsertObject(nIndex, sValue, TObject(LongBool(bValue)))
end { InsertBoolean };


procedure  TStringsHlp.InsertInteger(const   nIndex:  Integer;
                                     const   sValue:  string;
                                     const   nValue:  Integer);
begin
  InsertObject(nIndex, sValue, TObject(nValue))
end { InsertInteger };


function  TStringsHlp.List(const   sDelimiter:  string)
                           : string;
var
  nInd:  Integer;

begin  { List }
  if  Count = 0  then
    Result := ''
  else
    begin
      Result := Strings[0];
      for  nInd := 1  to  Count - 1  do
        Result := Result + sDelimiter + Strings[nInd]
    end { Count <> 0 }
end { List };


function  TStringsHlp.NameExists(const   sName:  string)
                                 : Boolean;
begin
  Result := IndexOfName(sName) >= 0
end { NameExists };


function  TStringsHlp.ObjectExists(const   aObject:  TObject)
                                   : Boolean;
begin
  Result := IndexOfObject(aObject) >= 0
end { ObjectExists };


procedure  TStringsHlp.ReplaceBoolean(const   nIndex:  Integer;
                                      const   sValue:  string;
                                      const   bValue:  Boolean);
begin
  Strings[nIndex] := sValue;
  ObjAsBoolean[nIndex] := bValue
end { ReplaceBoolean };


procedure  TStringsHlp.ReplaceInteger(const   nIndex:  Integer;
                                      const   sValue:  string;
                                      const   nValue:  Integer);
begin
  Strings[nIndex] := sValue;
  ObjAsInteger[nIndex] := nValue
end { ReplaceInteger };


procedure  TStringsHlp.ReplaceObject(const   nIndex:  Integer;
                                     const   sValue:  string;
                                     const   aValue:  TObject);
begin
  Strings[nIndex] := sValue;
  Objects[nIndex] := aValue
end { ReplaceObject };


procedure  TStringsHlp.SetAsBoolean(const   nIndex:  Integer;
                                    const   bValue:  Boolean);
begin
  Objects[nIndex] := TObject(LongBool(bValue))
end { SetAsBoolean };


procedure  TStringsHlp.SetAsInteger(const   nIndex, nValue:  Integer);
begin
  Objects[nIndex] := TObject(nValue)
end { SetAsInteger };


function  TStringsHlp.ValueExists(const   sValue:  string)
                                  : Boolean;
var
  nInd:  Integer;

begin  { ValueExists }
  Result := True;
  for  nInd := Count - 1  downto  0  do
    if  CompareStrings(ValueFromIndex[nInd], sValue) = 0  then
      Exit;
  Result := False
end { ValueExists };


function  TStringsHlp.ValueExists(const   bValue:  Boolean)
                                  : Boolean;
begin
  Result := IndexOfBoolean(bValue) >= 0
end { ValueExists };


function  TStringsHlp.ValueExists(const   nValue:  Integer)
                                  : Boolean;
begin
  Result := IndexOfInteger(nValue) >= 0
end { ValueExists };

{$ENDREGION 'TStringsHlp'}


{$REGION 'TStreamHlp'}

function  TStreamHlp.CopyFrom(Source:  TStream;
                              Count:  Int64)
                              : Int64;
begin
  Result := inherited CopyFrom(Source, Count)
end { CopyFrom };


function  TStreamHlp.Eof() : Boolean;
begin
  Result := Position = Size
end { Eof };


{$IFDEF UNICODE}

function  TStreamHlp.ReadDataAnsiChar() : AnsiChar;
begin
  ReadBuffer(Result, SizeOf(AnsiChar))
end { ReadDataAnsiChar };


function  TStreamHlp.ReadDataAnsiString() : AnsiString;

var
  chBuf:  AnsiChar;


begin  { ReadDataString }
  Result := '';
  while  not Eof  do
    begin
      chBuf := ReadDataAnsiChar();
      if  chBuf = NUL  then
        Break;
      Result := Result + chBuf
    end { while not Eof }
end { ReadDataAnsiString };

{$ENDIF UNICODE}


function  TStreamHlp.ReadDataByte() : Byte;
begin
  ReadBuffer(Result, SizeOf(Byte))
end { ReadDataByte };


{$IFDEF UNICODE}

function  TStreamHlp.ReadDataAnsiString({const}   nLength:  Integer)
                                        : AnsiString;
var
  nInd:  Integer;

begin  { ReadDataString }
  SetLength(Result, nLength);
  nInd := 0;
  while  (nInd < nLength)
         and not Eof  do
    begin
      Inc(nInd);
      Result[nInd] := ReadDataAnsiChar()
    end { while };
  if  nInd < nLength  then
    SetLength(Result, nInd)
end { ReadDataAnsiString };

{$ENDIF UNICODE}


function  TStreamHlp.ReadDataChar() : Char;
begin
  ReadBuffer(Result, SizeOf(Char))
end { ReadDataChar };


function  TStreamHlp.ReadDataString() : string;

var
  chBuf:  Char;
 {$IFDEF UNICODE}
  lBuf:  TStringBuilder;
 {$ENDIF UNICODE}


begin  { ReadDataString }
  {$IFDEF UNICODE}
    lBuf := TStringBuilder.Create();
  {$ELSE}
    Result := '';
  {$ENDIF -UNICODE}
  try
    while  not Eof  do
      begin
        chBuf := ReadDataChar();
        if  chBuf = NUL  then
          Break;
        {$IFDEF UNICODE}
          lBuf.Append(chBuf)
        {$ELSE}
          Result := Result + chBuf
        {$ENDIF -UNICODE}
      end { while not Eof };
    {$IFDEF UNICODE}
      Result := lBuf.ToString()
    {$ENDIF UNICODE}
  finally
    {$IFDEF UNICODE}
      lBuf.Free()
    {$ENDIF UNICODE}
  end { try-finally }
end { ReadDataString };


function  TStreamHlp.ReadDataString({const}   nLength:  Integer)
                                    : string;
var
  nInd:  Integer;

begin  { ReadDataString }
  SetLength(Result, nLength);
  nInd := 0;
  while  (nInd < nLength)
         and not Eof  do
    begin
      Inc(nInd);
      Result[nInd] := ReadDataChar()
    end { while };
  if  nInd < nLength  then
    SetLength(Result, nInd)
end { ReadDataString };


{$IFDEF UNICODE}

procedure  TStreamHlp.WriteDataAnsiString(const   sData:  AnsiString);
begin
  WriteBuffer(PAnsiChar(sData)^, Length(sData) * SizeOf(AnsiChar))
end { WriteDataAnsiString };


function  TStreamHlp.WriteData(const   Buffer:  TBytes)
                               : NativeInt;
begin
  Result := WriteData(Buffer, Length(Buffer))
end { WriteData };


procedure  TStreamHlp.WriteDataAnsiString(const   pszData:  PAnsiChar);
begin
  WriteBuffer(pszData^,
              {$IF CompilerVersion > 22}   // XE
                 System.AnsiStrings.StrLen(pszData) * SizeOf(AnsiChar)
              {$ELSE}
                SysUtils.StrLen(pszData) * SizeOf(AnsiChar)
              {$IFEND})
end { WriteDataAnsiString };

{$ENDIF UNICODE}


procedure  TStreamHlp.WriteDataChar(const   chData:  Char);
begin
  WriteBuffer(chData, SizeOf(chData))
end { WriteDataChar };


procedure  TStreamHlp.WriteDataString(const   sData:  string);
begin
  WriteBuffer(PChar(sData)^, Length(sData) * SizeOf(Char))
end { WriteData };


procedure  TStreamHlp.WriteDataString(const   pszData:  PChar);
begin
  WriteBuffer(pszData^, StrLen(pszData) * SizeOf(Char))
end { WriteData };

{$ENDREGION 'TStreamHlp'}


{$REGION 'TRegistryHlp'}

procedure  TRegistryHlp.GetValuesData(const   lData:  TStrings);

var
  nInd:  Integer;
  sLbl:  string;
 {$IFNDEF UNICODE}
  sVal:  string;
  sBuf:  string;
  nMax:  Integer;
  nPos:  Integer;
  nVal:  Integer;
  lBuf:  packed array of Byte;
 {$ENDIF UNICODE}

begin  { GetValuesData }
  lData.BeginUpdate();
  try
    GetValueNames(lData);
    for  nInd := lData.Count - 1  downto  0  do
      begin
        sLbl := lData.Strings[nInd];
        {$IFDEF UNICODE}
          lData.Strings[nInd] := sLbl + lData.NameValueSeparator + GetDataAsString(sLbl)
        {$ELSE}
          case  GetDataType(sLbl)  of
            rdString,
            rdExpandString:
              sVal := ReadString(sLbl);
            rdInteger:
              sVal := IntToStr(ReadInteger(sLbl));
            rdUnknown,
            rdBinary:
              begin
                nMax := GetDataSize(sLbl);
                SetLength(lBuf, nMax);
                ReadBinaryData(sLbl, Addr(lBuf)^, nMax);
                SetLength(sVal, nMax * 3 - 1);
                nVal := 1;
                Dec(nMax);
                for  nPos := 0 to  nMax  do
                  begin
                    sBuf := IntToHex(lBuf[nInd], 2);
                    sVal[nVal + 0] := sBuf[1];
                    sVal[nVal + 1] := sBuf[2];
                    if  nPos < nMax  then
                      sVal[nVal + 2] := ',';
                    Inc(nVal, 3)
                  end { for nPos }
              end { rdBinary, rdUnknown }
          end { case GetDataType(sLbl) };
          lData.Strings[nInd] := sLbl + lData.NameValueSeparator + sVal;
        {$ENDIF UNICODE}
      end { for nInd }
  finally
    lData.EndUpdate()
  end { try-finally }
end { GetValuesData };


function  TRegistryHlp.ReadBool(const   sName:  string;
                                const   bDefault:  Boolean)
                                : Boolean;
begin
  if  ValueExists(sName)  then
    Result := inherited ReadBool(sName)
  else
    Result := bDefault
end { ReadBool };


function  TRegistryHlp.ReadBoolStr(const   sName:  string;
                                   const   bDefault:  Boolean)
                                   : Boolean;
begin
  Result := StrToBool(ReadString(sName, BoolToStr(bDefault, True)))
end { ReadBoolStr };


function  TRegistryHlp.ReadCurrency(const   sName:  string;
                                    const   nDefault:  Currency)
                                    : Currency;
begin
  if  ValueExists(sName)  then
    Result := inherited ReadCurrency(sName)
  else
    Result := nDefault
end { ReadCurrency };


function  TRegistryHlp.ReadDateTime(const   sName:  string;
                                    const   tmDefault:  TDateTime)
                                    : TDateTime;
begin
  if  ValueExists(sName)  then
    Result := inherited ReadDateTime(sName)
  else
    Result := tmDefault
end { ReadDateTime };


function  TRegistryHlp.ReadFloat(const   sName:  string;
                                 const   nDefault:  Double)
                                 : Double;
begin
  if  ValueExists(sName)  then
    Result := inherited ReadFloat(sName)
  else
    Result := nDefault
end { ReadFloat };


function  TRegistryHlp.ReadFloatStr(const   sName:  string;
                                    const   nDefault:  Double)
                                    : Double;
var
  sBufA:  string[10];
  sBufU:  string;
  nErr:  Integer;

begin  { ReadFloatStr }
  Str(nDefault:1:1, sBufA);
  sBufU := ReadString(sName, string(sBufA));
  Val(sBufU, Result, nErr);
  if  nErr <> 0  then
    raise  EConvertError.CreateFmt('''%0:s'' is not correct number (char #%1:d)',
                                   [sBufU, nErr])
end { ReadFloatStr };


function  TRegistryHlp.ReadInteger(const   sName:  string;
                                   const   nDefault:  Integer)
                                   : Integer;
begin
  if  ValueExists(sName)  then
    Result := inherited ReadInteger(sName)
  else
    Result := nDefault
end { ReadInteger };


function  TRegistryHlp.ReadIntegerStr(const   sName:  string;
                                      const   nDefault:  Integer)
                                      : Integer;
begin
  Result := StrToInt(ReadString(sName, IntToStr(nDefault)))
end { ReadIntegerStr };


function  TRegistryHlp.ReadString(const   sName, sDefault:  string)
                                  : string;
begin
  if  ValueExists(sName)  then
    Result := inherited ReadString(sName)
  else
    Result := sDefault
end { ReadString };


procedure  TRegistryHlp.WriteBoolStr(const   sName:  string;
                                     const   bValue:  Boolean);
begin
  WriteString(sName, BoolToStr(bValue, True))
end { WriteBoolStr };


procedure  TRegistryHlp.WriteFloatStr(const   sName:  string;
                                      const   nValue:  Double);
var
  sValue:  string[10];

begin  { WriteFloatStr }
  Str(nValue:1:1, sValue);
  WriteString(sName, string(sValue))
end { WriteFloatStr };


procedure  TRegistryHlp.WriteIntegerStr(const   sName:  string;
                                        const   nValue:  Integer);
begin
  WriteString(sName, IntToStr(nValue))
end { WriteIntegerStr };


{$ENDREGION 'TRegistryHlp'}


{$REGION 'TCustomIniFileHlp'}

function  TCustomIniFileHlp.ReadCurrency(const   sSection, sName:  string;
                                   const   nDefault:  Currency)
                                   : Currency;
var
  sResult:  string;

begin  { ReadCurrency }
  sResult := ReadString(sSection, sName, sResult);
  Result := nDefault;
  if  sResult <> ''  then
    try
      Result := StrToCurrency(sResult)
    except
      on  EConvertError  do
        { Ignore EConvertError exceptions }
      else
        raise
    end { try-except }
end { ReadCurrency };


procedure  TCustomIniFileHlp.WriteCurrency(const   sSection, sName:  string;
                                     const   nValue:  Currency);
begin
  WriteString(sSection, sName, CurrToStr(nValue))
end { WriteCurrency };

{$ENDREGION 'TCustomIniFileHlp'}


{$REGION 'TIniFileHlp'}

class function  TIniFileHlp.ReadBoolEx(const   sFilePath, sSection, sName:  string;
                                       const   bDefault:  Boolean)
                                       : Boolean;
begin
  with  TIniFile.Create(sFilePath)  do
    try
      Result := ReadBool(sSection, sName, bDefault)
    finally
      Free()
    end { try-finally }
end { ReadBoolEx };


class function  TIniFileHlp.ReadCurrencyEx(const   sFilePath, sSection, sName:  string;
                                           const   nDefault:  Currency)
                                           : Currency;
begin
  with  TIniFile.Create(sFilePath)  do
    try
      Result := ReadCurrency(sSection, sName, nDefault)
    finally
      Free()
    end { try-finally }
end { ReadCurrencyEx };


class function  TIniFileHlp.ReadIntegerEx(const   sFilePath, sSection, sName:  string;
                                          const   nDefault:  Integer)
                                          : Integer;
begin
  with  TIniFile.Create(sFilePath)  do
    try
      Result := ReadInteger(sSection, sName, nDefault)
    finally
      Free()
    end { try-finally }
end { ReadIntegerEx };


class function  TIniFileHlp.ReadStringEx(const   sFilePath, sSection, sName, sDefault:  string)
                                         : string;
begin
  with  TIniFile.Create(sFilePath)  do
    try
      Result := ReadString(sSection, sName, sDefault)
    finally
      Free()
    end { try-finally }
end { ReadStringEx };


class procedure  TIniFileHlp.WriteBoolEx(const   sFilePath, sSection, sName:  string;
                                         const   bValue:  Boolean);
begin
  with  TIniFile.Create(sFilePath)  do
    try
      WriteBool(sSection, sName, bValue)
    finally
      Free()
    end { try-finally }
end { WriteBoolEx };


class procedure  TIniFileHlp.WriteCurrencyEx(const   sFilePath, sSection, sName:  string;
                                             const   nValue:  Currency);
begin
  with  TIniFile.Create(sFilePath)  do
    try
      WriteCurrency(sSection, sName, nValue)
    finally
      Free()
    end { try-finally }
end { WriteCurrencyEx };


class procedure  TIniFileHlp.WriteIntegerEx(const   sFilePath, sSection, sName:  string;
                                            const   nValue:  Integer);
begin
  with  TIniFile.Create(sFilePath)  do
    try
      WriteInteger(sSection, sName, nValue)
    finally
      Free()
    end { try-finally }
end { WriteIntegerEx };


class procedure  TIniFileHlp.WriteStringEx(const   sFilePath, sSection, sName, sValue:  string);
begin
  with  TIniFile.Create(sFilePath)  do
    try
      WriteString(sSection, sName, sValue)
    finally
      Free()
    end { try-finally }
end { WriteStringEx };

{$ENDREGION 'TIniFileHlp'}


{$REGION 'TExceptionHlp'}

class procedure  TExceptionHlp.Throw(const   sErrMsg:  string);
begin
  raise  Exception(Self.Create(sErrMsg))
end { Throw };


class procedure  TExceptionHlp.Throw(const   sErrFmt:  string;
                                     const   ErrArgs:  array of const);
begin
  Throw(Format(sErrFmt, ErrArgs))
end { Throw };


class procedure  TExceptionHlp.Throw(const   sErrFmt, sErrArg:  string);
begin
  Throw(sErrFmt, [sErrArg])
end { Throw };

{$ENDREGION 'TExceptionHlp'}


{$REGION 'EGSkExceptionErrNo'}

constructor  EGSkExceptionErrNo.Create(const   nErrCode:  Integer;
                                       const   sErrInfo:  string;
                                       const   setFlags:  TExceptionFlags);
begin
  inherited Create(sErrInfo);
  fsMessageEnglish := sErrInfo;
  fnErrorCode := nErrCode;
  fsetFlags := setFlags;
end { Create };


constructor  EGSkExceptionErrNo.Create(const   nErrCode:  Integer;
                                       const   sErrInfoNative, sErrInfoEnglish:  string;
                                       const   setFlags:  TExceptionFlags);
begin
 {$WARN SYMBOL_DEPRECATED OFF}
  Create(nErrCode, sErrInfoNative, setFlags);
 {$WARN SYMBOL_DEPRECATED ON}
  fsMessageEnglish := sErrInfoEnglish
end { Create };


constructor  EGSkExceptionErrNo.Create(const   sMsg:  string);
begin
  inherited
end { Create };


constructor  EGSkExceptionErrNo.CreateFmt(const   sFmt:  string;
                                          const   Args:  array of const);
begin
  inherited;
end { CreateFmt };


constructor  EGSkExceptionErrNo.CreateFmt(const   nErrCode:  Integer;
                                          const   sErrInfoNativeFmt, sErrInfoEnglisthFmt:  string;
                                          const   ErrInfoArgs:  array of const;
                                          const   setFlags:  TExceptionFlags);
begin
  Create(nErrCode,
         Format(IfThen(sErrInfoNativeFmt <> '', sErrInfoNativeFmt, sErrInfoEnglisthFmt),
                ErrInfoArgs),
         Format(IfThen(sErrInfoEnglisthFmt <> '', sErrInfoEnglisthFmt, sErrInfoNativeFmt),
                ErrInfoArgs),
         setFlags)
end { CreateFmt };


constructor  EGSkExceptionErrNo.CreateFmt(const   nErrCode:  Integer;
                                          const   sErrInfoFmt:  string;
                                          const   ErrInfoArgs:  array of const;
                                          const   setFlags:  TExceptionFlags);
begin
 {$WARN SYMBOL_DEPRECATED OFF}
  Create(nErrCode, Format(sErrInfoFmt, ErrInfoArgs), setFlags)
 {$WARN SYMBOL_DEPRECATED ON}
end { CreateFmt };


function  EGSkExceptionErrNo.GetIsLogged() : Boolean;
begin
  Result := efAlreadyLogged in fsetFlags
end { GetIsLogged };


function  EGSkExceptionErrNo.GetIsWarning() : Boolean;
begin
  Result := efWarning in fsetFlags
end { GetIsWarning };


function  EGSkExceptionErrNo.GetMessageNative() : string;
begin
  Result := inherited Message
end { GetMessageNative };


procedure  EGSkExceptionErrNo.SetIsLogged(const   Value:  Boolean);
begin
  if  Value  then
    Include(fsetFlags, efAlreadyLogged)
  else
    Exclude(fsetFlags, efAlreadyLogged)
end { SetIsLogged };


class procedure  EGSkExceptionErrNo.Throw(const   nErrCode:  Integer;
                                          const   sErrMsg:  string;
                                          const   setFlags:  TExceptionFlags);
begin
 {$WARN SYMBOL_DEPRECATED OFF}
  raise  Self.Create(nErrCode, sErrMsg, setFlags)
 {$WARN SYMBOL_DEPRECATED ON}
end { Throw };


class procedure  EGSkExceptionErrNo.Throw(const   nErrCode:  Integer;
                                          const   sErrFmt, sErrArg:  string;
                                          const   setFlags:  TExceptionFlags);
begin
 {$WARN SYMBOL_DEPRECATED OFF}
  Throw(nErrCode, Format(sErrFmt, [sErrArg]), setFlags)
 {$WARN SYMBOL_DEPRECATED ON}
end { Throw };


class procedure  EGSkExceptionErrNo.Throw(const   nErrCode:  Integer;
                                          const   sErrFmt:  string;
                                          const   ErrArgs:  array of const;
                                          const   setFlags:  TExceptionFlags);
begin
 {$WARN SYMBOL_DEPRECATED OFF}
  Throw(nErrCode, Format(sErrFmt, ErrArgs), setFlags)
 {$WARN SYMBOL_DEPRECATED ON}
end { Throw };

{$ENDREGION 'EGSkExceptionErrNo'}


{$REGION 'TArrayHlp'}

{$IF Declared(TArray)}

class function  TArrayHlp.ToList<T>(const   Values:  array of T;
                                    const   fnToString:  TFunc<T,string>;
                                    const   sSeparator:  string)
                                    : string;
var
  nInd:  Integer;

begin  { ToList }
  if  Length(Values) = 0  then
    Result := ''
  else
    begin
      Result :=  fnToString(Values[Low(Values)]);
      for  nInd := Succ(Low(Values))  to  High(Values)  do
        Result := Result + sSeparator + fnToString(Values[nInd])
    end { Length(Values) <> 0 }
end { ToList };

{$IFEND}

{$ENDREGION 'TArrayHlp'}


end.

