﻿(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)


//  Definicje biblioteki


//  *** Wersje DELPHI:
//       D7    - Delphi 7
//       D2007 - Delphi 2007
//       DXE1  - Delphi XE1
//       DXE2  - Delphi XE2
//       DXE10 - Delphi XE10 Seattle


{$IF CompilerVersion >= 25} // Delphi XE 4
  {$LEGACYIFEND ON}
{$IFEND}


{$IFDEF Ver150}             // Borland Delphi 7.0
  {$DEFINE D7}
  {$DEFINE Delphi7}
  {$DEFINE D7UP}
{$ENDIF Ver150}

{$IFDEF Ver185}             // CodeGear Delphi 2007
  {$DEFINE D2007}
  {$DEFINE Delphi2007}
  {$DEFINE D11}
  {$DEFINE D7UP}
  {$DEFINE D2007UP}
{$ENDIF Ver185}

{$IFDEF Ver220}             // Embarcadero Delphi XE
  {$DEFINE DXE}
  {$DEFINE DXE1}
  {$DEFINE DelphiXE}
  {$DEFINE DelphiXE1}
  {$DEFINE D15}
  {$DEFINE D7UP}
  {$DEFINE D2007UP}
  {$DEFINE DXE1UP}
{$ENDIF Ver220}

{$IFDEF VER230}             // Embarcadero Delphi XE2
  {$DEFINE DXE2}
  {$DEFINE DelphiXE2}
  {$DEFINE D16}
  {$DEFINE D7UP}
  {$DEFINE D2007UP}
  {$DEFINE DXE1UP}
  {$DEFINE DXE2UP}
{$ENDIF Ver230}

{$IFDEF Ver300}             // Embarcadero Delphi 10.0 Seattle
  {$DEFINE DXE10}
  {$DEFINE DelphiXE10}
  {$DEFINE Delphi10}
  {$DEFINE D23}
  {$DEFINE D7UP}
  {$DEFINE D2007UP}
  {$DEFINE DXE1UP}
  {$DEFINE DXE2UP}
  {$DEFINE DXE10UP}
  {$DEFINE D10UP}
{$ENDIF Ver300}

{$IFDEF Ver310}             // Embarcadero Delphi 10.1 Berlin
  {$DEFINE DXE10_1}
  {$DEFINE DelphiXE10}
  {$DEFINE Delphi10_1}
  {$DEFINE D24}
  {$DEFINE D7UP}
  {$DEFINE D2007UP}
  {$DEFINE DXE1UP}
  {$DEFINE DXE2UP}
  {$DEFINE DXE10UP}
  {$DEFINE D10UP}
  {$DEFINE D10_1UP}
{$ENDIF Ver310}

{$IfNDef IBX}
  {$IfNDef BDE}
    {$IfNDef DBX}
      {$DEFINE IBX}
    {$ENDIF -DBX}
  {$ENDIF -BDE}
{$ENDIF -IBX}


{$WARN  UNSAFE_TYPE OFF}
{$WARN  UNSAFE_CODE OFF}
{$WARN  UNSAFE_CAST OFF}


{$IFDEF JEDI}
  {$DEFINE USE_JEDI_JCL}    // Wspłpraca JEDI z MemCheck oraz DUnit
{$ENDIF JEDI}

{$IfOpt D-}
  {$UNDEF GExperts}
{$ENDIF D-}


//  End of CONDITIONALS.INC
