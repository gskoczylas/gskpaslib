﻿unit  Test.GSkPasLib.CSV;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}


uses
  Classes, SysUtils, Windows, Math, Contnrs,
  TestFramework,
  GSkPasLib.CSV;


type
  CSV_Test = class(TTestCase)
  strict private
    fCSV:  TGSkCSV;
  public
    procedure  SetUp();                                              override;
    procedure  TearDown();                                           override;
  published
    procedure  TestLoad();
    procedure  TestSave();
  end { CSV_Test };


implementation


uses
  GSkPasLib.Classes;


procedure  CSV_Test.SetUp();
begin
  fCSV := TGSkCSV.Create()
end { SetUp };


procedure  CSV_Test.TearDown();
begin
  FreeAndNil(fCSV)
end { TearDown };


procedure  CSV_Test.TestLoad();

const
  csTestFile = 'TestLoad.csv';

  procedure  TestIt(const   sData:  string;
                    const   Fields1:  array of string;
                    const   Fields2:  array of string;
                    const   sCase:  string);
  var
    nField:   Integer;

    procedure  SaveData(const   sData:  string);

    var
      lOutput:  TFileStream;

    begin
      lOutput := TFileStream.Create(csTestFile, fmCreate);
      try
        lOutput.WriteDataString(sData)
      finally
        lOutput.Free()
      end { try-finally }
    end { SaveData };

  begin  { TestIt }
    { Write test data }
    SaveData(sData);
    { Try to laod test data }
    fCSV.Load(csTestFile);
    { Verify loaded data }
    CheckEquals(IfThen(Length(Fields2) <> 0, 2, 1),
                fCSV.RecordCount, sCase + ': RecordCount');
    CheckEquals(Length(Fields1), fCSV.FieldCount[0], sCase + ': FieldCount[0]');
    for  nField := 0  to  fCSV.FieldCount[0] - 1  do
      CheckEqualsString(fCSV.Field[0, nField], Fields1[nField],
                        sCase + ': Row #0, Field[' + IntToStr(nField) + ']');
    if  Length(Fields2) <> 0  then
      begin
        CheckEquals(Length(Fields2), fCSV.FieldCount[1], sCase + ': FieldCount[1]');
        for  nField := 0  to  fCSV.FieldCount[1] - 1  do
          CheckEqualsString(fCSV.Field[1, nField], Fields2[nField],
                            sCase + ': Row #1, Field[' + IntToStr(nField) + ']')
      end { Length(Fields2) <> 0 };
    DeleteFile(csTestFile)
  end { TestIt };

begin  { TestLoad }
  TestIt('1,True,Trzy,Cztery',
         ['1', 'True', 'Trzy', 'Cztery'],
         [],
         'Proste dane');
  TestIt('1,True,Trzy,Cztery' + sLineBreak,
         ['1', 'True', 'Trzy', 'Cztery'],
         [],
         'Proste dane + EOL');
  TestIt('"Na końcu "",""",""","" na początku","znak "","" w środku"',
         ['Na końcu ","', '"," na początku', 'znak "," w środku'],
         [],
         'Cudzysłowy i przecinki w polu');
  TestIt('"Pierwszy wiersz.' + sLineBreak + 'Drugi wiersz", "dwa pola"',
         ['Pierwszy wiersz.' + sLineBreak + 'Drugi wiersz', 'dwa pola'],
         [],
         'Pole dwuwierszowe');
  TestIt('Jeden,Dwa,',
         ['Jeden', 'Dwa', ''],
         [],
         'Puste pole na końcu (przypadek nr 1)');
  TestIt('Jeden,Dwa,' + sLineBreak,
         ['Jeden', 'Dwa', ''],
         [],
         'Puste pole na końcu (przypadek nr 1) + EOL');
  TestIt('"Pierwsze","Drugie",' + sLineBreak,
         ['Pierwsze', 'Drugie', ''],
         [],
         'Puste pole na końcu (przypadek nr 2)');
  TestIt(' Alfa , Beta ' + sLineBreak,
         ['Alfa', 'Beta'],
         [],
         'Odstępy (proste pola)');
  TestIt(' "Abc" , "Def" ' + sLineBreak,
         ['Abc', 'Def'],
         [],
         'Odstępy (pola w cudzysłowach)');
  TestIt('A,B' + sLineBreak + 'C,D,E' + sLineBreak,
         ['A', 'B'],
         ['C', 'D', 'E'],
         'Dwa wiersze (wariant 1)');
  TestIt('"Ala i As.","Znak "" wewnątrz napisu"' + sLineBreak + ',Środkowe,' + sLineBreak,
         ['Ala i As.', 'Znak " wewnątrz napisu'],
         ['', 'Środkowe', ''],
         'Dwa wiersze (wariant 2)')
end { TestLoad };


procedure  CSV_Test.TestSave();

const
  csTestFile = 'TestSave.csv';

var
  lData:  array of
            array of string;
  nRow:  Integer;
  nFld:  Integer;

  procedure  AddData(const   Fields:  array of string);

  var
    nInd:  Integer;

  begin  { AddData }
    SetLength(lData, Length(lData) + 1);
    SetLength(lData[High(lData)], Length(Fields));
    for  nInd := Low(Fields)  to  High(Fields)  do
      lData[High(lData)][nInd] := Fields[nInd]
  end { AddData };

begin  { TestSave }
  { Przygotowanie danych do testu }
  SetLength(lData, 0);
  AddData(['Alą', 'się', 'ćmi']);                     // 0
  AddData(['Ala i As.', 'Znak " wewnątrz napisu']);   // 1
  AddData(['', 'Środkowe', '']);                      // 2
  AddData(['  Odstępy przed i po polu  ']);           // 3
  { Przygotowanie danych CSV CSV }
  fCSV.AddRecord(lData[0]);
  fCSV.AddRecord(lData[2]);
  fCSV.AddRecord(lData[3]);
  fCSV.InsertRecord(1, lData[1]);
  { Zapisuję plik CSV }
  // TODO 3 -c Poprawić: Kodowanie UTF8 nie działa poprawnie
  { Problem jest w procedurze „Load” w analizie wczytanego tekstu }
  fCSV.Save(csTestFile {$IFDEF UNICODE}, TEncoding.Unicode{$ENDIF UNICODE});
  { Metoda „Load” jest już przetestowana, więc moge ją tutaj wykorzystać }
  CheckEquals(Length(lData), fCSV.RecordCount, 'Liczba rekordów tuż po zapisaniu danych do pliku');
  fCSV.Clear();
  CheckEquals(0, fCSV.RecordCount, 'Liczba rekordów po wykonaniu metody „Clear”');
  fCSV.Load(csTestFile {$IFDEF UNICODE}, TEncoding.Unicode{$ENDIF UNICODE});
  CheckEquals(Length(lData), fCSV.RecordCount, 'Liczba rekordów wczytanych do weryfikacji');
  for  nRow := Low(lData)  to  High(lData)  do
    begin
      CheckEquals(Length(lData[nRow]), fCSV.FieldCount[nRow],
                  Format('Liczba pól w wierszu %d', [nRow]));
      for  nFld := Low(lData[nRow])  to  High(lData[nRow])  do
        CheckEquals(lData[nRow][nFld], fCSV.Field[nRow, nFld],
                    Format('Wiersz nr %d, pole nr %d',
                           [nRow, nFld]))
    end { for nRow };
  DeleteFile(csTestFile)
end { TestSave };


initialization
  RegisterTest(CSV_Test.Suite);


end.


