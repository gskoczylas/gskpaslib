﻿{: @abstract Rejestrowanie obiektów JSON.

   Moduł @name zawiera definicję klasy pomocniczej dla klasy @link(TGSkLog).
   Po umieszczeniu go w sekcji @code(uses) razem z modułem @link(GSkPasLib.Log)
   możliwe jest wygodne monitorowanie obiektów JSON. }
unit  GSkPasLib.LogJSON;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    System.JSON, REST.JSON,
  {$ELSE}
    DBXJSON,
  {$IFEND}
  GSkPasLib.Log, GSkPasLib.JSON;


type
  {: @abstract Rozszerza klasę @link(TGSkLog) o monitorowanie obiektów JSON. }
  TGSkLogHlpJSON = class helper for TGSkLog
  public

    {: @abstract Rejestruje wskazany obiekt JSON.

       Ta wersja metody przeznaczona jest do rejestrowania obiektów JSON.

       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   Value:  TJSONValue;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestruje wskazany obiekt JSON.

       Ta wersja metody przeznaczona jest do rejestrowania obiektów JSON.

       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   Value:  TJSONObject;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestruje wskazany obiekt TGSkJSON (lub pochodny).

       Ta wersja metody przeznaczona jest do rejestrowania obiektów TGSkJSON
       oraz obiektów pochodnych.

       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   Value:  TGSkCustomJSON;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;
  end { TGSkLogHlpJSON };


implementation


{$REGION 'TGSkLogHlpJSON'}

function  TGSkLogHlpJSON.LogValue(const   sName:  string;
                                  const   Value:  TJSONValue;
                                  const   lmMsgType:  TGSkLogMessageType)
                                  : TGSkLog;
begin
  Result := LogValue(sName, Value as TJSONObject, lmMsgType)
end { LogValue };


function  TGSkLogHlpJSON.LogValue(const   sName:  string;
                                  const   Value:  TJSONObject;
                                  const   lmMsgType:  TGSkLogMessageType)
                                  : TGSkLog;
begin
  Result := LogMemo(sName, FormatJSON(Value), lmMsgType)
end { LogValue };


function  TGSkLogHlpJSON.LogValue(const   sName:  string;
                                  const   Value:  TGSkCustomJSON;
                                  const   lmMsgType:  TGSkLogMessageType)
                                  : TGSkLog;
begin
  Result := LogMemo(sName, Value.ToString(), lmMsgType)
end { LogValue };

{$ENDREGION 'TGSkLogHlpJSON'}


end.

