﻿{: @abstract(Moduł @name definiuje obiekty i podprogramy ułatwiające dzielenie
             napisów na segmenty.)

   W tym module dostępne są komponenty ułatwiające dzielenie napisu na segmenty.
   Dostępne są również podprogramy, które ułatwiają kodowanie w prostych
   przypadkach.

   Separatorami segmentów mogą być @link(TGSkCharTokens znaki) lub
   @link(TGSkStringTokens napisy).

   @seealso(GSkPasLib.StrUtils)  }
unit  GSkPasLib.Tokens;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22}
    System.Classes, System.SysUtils,
  {$ELSE}
    Classes, SysUtils,
  {$IFEND}
  GSkPasLib.CustomComponents, GSkPasLib.ASCII;


const
  {: @abstract(Standardowe znaki końca wiersza.) }
  gcsetStdCharSep = gcsetNewLine;


type
  {: @abstract Parametry przetwarzania segmentów.

     @value(tfAllowEmptyTokens Wskazuje sposób postępowania wobec pustych segmentów.
                               Jeżeli w napisie separatory segmentów występują
                               obok siebie to klasa może je zredukować do
                               pojedynczego separatora (czyli zignorować puste
                               segmenty) lub uznać, że w napisie występują puste
                               segmenty.)
     @value(tfTrimTokens Informuje, że z segmentów mają być automatycznie usuwane
                         początkowe i końcowe spacje.) }
  TGSkTokenFlag = (tfAllowEmptyTokens, tfTrimTokens);
  TGSkTokenFlags = set of  TGSkTokenFlag;

  TGSkTokensEnumerator = class;

  {: @abstract(Bazowy komponent dla komponentów dzielących napis na segmenty)

     Ten komponent jest wspólnym przodkiem komponentów @link(TGSkCharTokens)
     oraz @link(TGSkStringTokens). Separatorami segmentów mogą być znaki lub
     napisy.  }
  TGSkCustomTokens = class abstract(TGSkCustomComponent)
  private
    var
      fsText:        string;
      fsetFlags:     TGSkTokenFlags;
      fTokens:       TStringList;
      fbSplitReq:    Boolean;
    function  GetAllowEmpty() : Boolean;
    procedure  SetAllowEmpty(const   bValue:  Boolean);
    procedure  SetText(const   sValue:  string);
    procedure  SetFlags(const   setValue:  TGSkTokenFlags);
    function  GetTokenCount() : Integer;
    function  GetToken(const   nIndex:  Integer)
                       : string;
    function  GetTokenPos(const   nIndex:  Integer)
                          : Integer;
  protected
    function  SepLen(const   nPos:  Integer)
                     : Integer;                                      virtual; abstract;
    procedure  InitSplitTokens();                                    virtual;
    procedure  NewToken(const   nTokenPos:  Integer;
                        const   nSepPos:  Integer;
                        const   nSepLen:  Integer);                  virtual;
    procedure  RemoveToken(const   nIndex:  Integer);                virtual;
    procedure  SplitTokens();
  public
    {: @exclude }
    constructor  Create(AOwner:  TComponent);                        override;
    {: @exclude }
    destructor  Destroy();                                           override;

    {: @abstract(Metoda @name zwraca tablicę segmentów) }
    function  ToArray() : TArray<string>;

    {: @abstract(Metoda @name zwraca enumerator)

       Metoda @name powoduje, że wobec obiektu można użyć również istrukcję
       @code(for sToken in Tokens do ...). }
    function  GetEnumerator() : TGSkTokensEnumerator;

    {: @abstract(Informuje o liczbie segmentów w napisie) }
    property  Count:  Integer
              read  GetTokenCount;

    {: @abstract(Udostępnia segmenty napisu) }
    property  Token[const   nIndex:  Integer] : string
              read  GetToken;
              default;

    {: @abstract(Udostępnia pozycje segmentów w napisie)

       Właściwość informuje o pozycji pierwszego znaku segmentu w oryginalnym
       napisie.

       Pierwszy znak w napisie ma numer 1. }
    property  TokenPos[const   nIndex:  Integer] : Integer
              read  GetTokenPos;

    {: @abstract(Flagi kontrolujące działanie klasy)

    Właściwość @name definiuje flagi sterujące działaniem klasy.

    @seeAlso(TGSkTokenFlag) }
    property  Flags:  TGSkTokenFlags
              read  fsetFlags
              write SetFlags;
  published
    {: @abstract(Napis dzielony na segmenty)

       Poszczególne segmenty napisu dostępne są dzięki właściwości @link(Token).
       Ich pozycje w napisie są udostępniane przez właściwość @link(TokenPos). }
    property  Text:  string
              read  fsText
              write SetText;

    {: @deprecated Zamiast @name użyj właściwość @link(Flags) }
    property  AllowEmptyTokens:  Boolean
              read    GetAllowEmpty
              write   SetAllowEmpty
              default True;
  end { TGSkCustomTokens };


  {: @abstract(Komponent dzieli napis na segmenty; separatorem segmentów są znaki.)

     Komponent @name jest potomkiem komponentu @link(TGSkCustomTokens).

     @seealso(TGSkStringTokens) }
  TGSkCharTokens = class(TGSkCustomTokens)
  private
    var
      fsetDelim:  TSysCharSet;
    function  GetTokenSep(const   nIndex:  Integer)
                          : Char;
  protected
    function  SepLen(const   nPos:  Integer)
                     : Integer;                                      override;
  public
    {: @exclude }
    constructor  Create(AOwner:  TComponent);                        overload; override;
    constructor  Create(const   sText:  string;
                        const   chSep:  Char;
                        const   setFlags:  TGSkTokenFlags = []);        reintroduce; overload;
    constructor  Create(const   sText:  string;
                        const   chSep:  Char;
                        const   bAllowEmptyTokens:  Boolean);        reintroduce; overload;
    constructor  Create(const   sText:  string;
                        const   setSep:  TSysCharSet = gcsetStdCharSep;
                        const   setFlags:  TGSkTokenFlags = []);        reintroduce; overload;
    constructor  Create(const   sText:  string;
                        const   setSep:  TSysCharSet;
                        const   bAllowEmptyTokens:  Boolean);        reintroduce; overload;
    {: @exclude }
    destructor  Destroy();                                           override;

    {: @abstract(Definiuje separatory segmentów)

       @param(setDelim Zbiór znaków będących separatorem segmentów)

       Separatorem segmentów jest dowolny znak ze wskazanego w parametrze zbioru
       znaków. }
    procedure  SetDelimiters(const   setDelim:  TSysCharSet);

    {: @abstract(Definiuje separator segmentów) }
    procedure  SetDelimiter(const   chDelim:  Char);

    {: @abstract(Informuje o aktualnym zestawie znaków będących separatorami
                 segmentów) }
    function  GetDelimiters() : TSysCharSet;

    {: @abstract(Wskazuje znak będący separatorem danego segmentu)

       Separatorem segmentu jest znak oddzielający dany segment od następnego
       segmentu. Separatorem ostatniego segmentu jest pusty znak (@code(#0)). }
    property  TokenSep[const   nIndex:  Integer] : Char
              read  GetTokenSep;
  end { TGSkCharTokens };


  {: @abstract(Komponent dzieli napis na segmenty; separatorem segmentów są dowolne napisy.)

     Komponent @name jest potomkiem komponentu @link(TGSkCustomTokens).

     @seealso(TGSkCharTokens) }
  TGSkStringTokens = class(TGSkCustomTokens)
  private
    var
      fDelim:  array of string;
      fbCaseSensitive:  Boolean;
      fTokenSep:  TStringList;
    function  GetTokenSep(const   nIndex:  Integer)
                          : string;
    function  GetDelimiterCount() : Integer;
    procedure  SetCaseSensitive(const bValue:  Boolean);
  protected
    function  SepLen(const   nPos:  Integer)
                     : Integer;                                      override;
    procedure  InitSplitTokens();                                    override;
    procedure  NewToken(const   nTokenPos:  Integer;
                        const   nSepPos:  Integer;
                        const   nSepLen:  Integer);                  override;
    procedure  RemoveToken(const   nIndex:  Integer);                override;
  public
    {: @exclude }
    constructor  Create(AOwner:  TComponent);                        overload; override;
    constructor  Create(const   sText:  string;
                        const   sSep:   string;
                        const   setFlags:  TGSkTokenFlags = []);        reintroduce; overload;
    constructor  Create(const   sText:  string;
                        const   sSep:   string;
                        const   bAllowEmptyTokens:  Boolean);        reintroduce; overload;
    constructor  Create(const   sText:  string;
                        const   asSep:  array of string;
                        const   setFlags:  TGSkTokenFlags = []);        reintroduce; overload;
    constructor  Create(const   sText:  string;
                        const   asSep:  array of string;
                        const   bAllowEmptyTokens:  Boolean);        reintroduce; overload;

    {: @exclude }
    destructor  Destroy();                                           override;

    {: @abstract(Zwraca napis będący separatorem segmentów)

       Można zdefiniować dowolnie wiele napisów będących separatorem segmentów.
       Ta metoda zwraca jeden z takich napisów -- separatorów. }
    function  GetDelimiter(const   nIndex:  Integer)
                           : string;

    {: @abstract(Zmienia napis będący separatorem o wskazanym indeksie)

       Można zdefiniować dowolnie wiele napisów -- separatorów segmentów.
       Ta metoda pozwala zmienić jeden z nich. }
    procedure  SetDelimiter(const   sDelim:  string;
                            const   nIndex:  Integer);               overload;

    {: @abstract(Definiuje napis będący separatorem segmentów napisu) }
    procedure  SetDelimiter(const   sDelim:  string);                overload;

    {: @abstract(Definiuje zestaw napisów będących separatorami segmentów napisu)

       Separatorem segmentów jest dowolny spośród wskazanych napisów. }
    procedure  SetDelimiters(const   Delim:  array of string);

    {: @abstract(Separator segmentu)

       Właściwość zwraca napis oddzielający dany segment od następnego segmentu.
       Separatorem ostatniego segmentu jest pusty napis. }
    property  TokenSep[const   nIndex:  Integer] : string
              read  GetTokenSep;

    {: @abstract(Liczba separatorów)

       Można zdefiniować dowolnie wiele separatorów napisów. Ta właściwość
       informuje o liczbie zdefiniowanych separatorów. }
    property  DelimiterCount:  Integer
              read  GetDelimiterCount;

  published
    {: @abstract(Czy wielkość liter w separatorach ma znaczenie)

       Właściwość @name wskazuje czy w napisach będących separatorami segmentów
       wielkość liter ma znacznie. }
    property  CaseSensitive:  Boolean
              read  fbCaseSensitive
              write SetCaseSensitive
              default  False;
  end { TGSkStringTokens };

  {: @exclude }
  TGSkTokensEnumerator = class
  strict private
    var
      fnIndex:  Integer;
      fTokens:  TGSkCustomTokens;
    function  GetCurrent() : string;
  public
    constructor  Create(const   Tokens:  TGSkCustomTokens);
    {-}
    function  MoveNext() : Boolean;
    {-}
    property  Current:  string
              read  GetCurrent;
  end { TGSkTokensEnumerator };


{: @deprecated Użyaj funkcję z parametrem typu @link(TGSkTokenFlags). }
function  TokenChr(const   sText:  string;
                   const   nIndex:  Integer;
                   const   setSep:  TSysCharSet;
                   const   bAllowEmptyTokens:  Boolean)
                   : string;                                         overload;
                   deprecated 'Instead of this version of the function, use the version with the parameter “TGSkTokenFlags”';

{: @abstract(Zwraca wskazany segment napisu)

   Ta funkcja oferuje uproszczoną funkcjonalność komponentu @link(TGSkCharTokens).

   @param(sText Tekst, z którego funkcja powinna wyodrębnić jeden z segmentów.)
   @param(sIndex Numer zwracanego segmentu.)
   @param(setSep Zbiór znaków będących separatorem segmentów. Separatorem jest
                 dowolny znak z tego zbioru.)
   @param(sDefault Wartość domyślna, jeżeli nie istnieje segment o wskazanym indeksie.)
   @param(setFlags Steruje sposobem przetwarzania segmentów.)
   @return(Wynikiem jest wskazany w parametrach segment napisu.)

   @seeAlso(TGSkTokenFlag) }
function  TokenChr(const   sText:  string;
                   const   nIndex:  Integer;
                   const   setSep:  TSysCharSet = gcsetStdCharSep;
                   const   sDefault:  string = '';
                   const   setFlags:  TGSkTokenFlags = [tfAllowEmptyTokens])
                   : string;                                         overload;

{: @deprecated Zamiast tej funkcji użyj wersję z parametrem @code(TGSkTokenFlags). }
function  TokenChr(const   sText:  string;
                   const   nIndex:  Integer;
                   const   cSep:  Char;
                   const   bAllowEmptyTokens:  Boolean)
                   : string;                                         overload;
                   deprecated 'Instead of this version of the function, use the version with the parameter “TGSkTokenFlags”';

{: @abstract(Zwraca wskazany segment napisu)

   Ta funkcja oferuje uproszczoną funkcjonalność komponentu @link(TGSkCharTokens).

   @param(sText Tekst, z którego funkcja powinna wyodrębnić jeden z segmentów.)
   @param(sIndex Numer zwracanego segmentu.)
   @param(cSep Znak będących separatorem segmentów.)
   @param(sDefault Wartość domyślna, jeżeli nie istnieje segment o wskazanym indeksie.)
   @param(setFlags Steruje sposobem przetwarzania segmentów.)
   @return(Wynikiem jest wskazany w parametrach segment napisu.) }
function  TokenChr(const   sText:  string;
                   const   nIndex:  Integer;
                   const   cSep:  Char;
                   const   sDefault:  string;
                   const   setFlags:  TGSkTokenFlags = [tfAllowEmptyTokens])
                   : string;                                         overload;

{: @deprecated Zamiast tego użyj funkcję z parametrem typu @link(TGSkTokenFlags). }
function  TokenStr(const   sText:  string;
                   const   nIndex:  integer;
                   const   asSep:  array of string;
                   const   bAllowEmptyTokens:  Boolean)
                   : string;                                         overload;
                   deprecated 'Instead of this version of the function, use the version with the parameter “TGSkTokenFlags”';

{: @abstract(Zwraca wskazany segment napisu)

   Ta funkcja oferuje uproszczoną funkcjonalność komponentu @link(TGSkStringTokens).

   @param(sText Tekst, z którego funkcja powinna wyodrębnić jeden z segmentów.)
   @param(sIndex Numer zwracanego segmentu.)
   @param(asSep Tablica napisów. Separatorem jest dowolny napis z tego zestawu.)
   @param(sDefault Wartość domyślna, zwracana jeżeli nie istnieje segment o wskazanym indeksie.)
   @param(setFlags Steruje sposobem przetwarzania segmentów.)
   @return(Wynikiem jest wskazany w parametrach segment napisu.)

   @seeAlso(TGSkTokenFlag) }
function  TokenStr(const   sText:  string;
                   const   nIndex:  integer;
                   const   asSep:  array of string;
                   const   sDefault:  string = '';
                   const   setFlags:  TGSkTokenFlags = [tfAllowEmptyTokens])
                   : string;                                         overload;

{: @deprecated Zamiast tej funkcji użyj wersję z parametrem typu @link(TGSkTokenFlags). }
function  TokenStr(const   sText:  string;
                   const   nIndex:  integer;
                   const   sSep:  string;
                   const   bAllowEmptyTokens:  Boolean)
                   : string;                                         overload;
                   deprecated 'Instead of this version of the function, use the version with the parameter “TGSkTokenFlags”';

{: @abstract(Zwraca wskazany segment napisu)

   Ta funkcja oferuje uproszczoną funkcjonalność komponentu @link(TGSkStringTokens).

   @param(sText Tekst, z którego funkcja powinna wyodrębnić jeden z segmentów.)
   @param(sIndex Numer zwracanego segmentu.)
   @param(sSep Napis będący separatorem segmentów.)
   @param(sDefault Wartość domyślna, zwracana jeżeli nie istnieje segment o wskazanym indeksie.)
   @param(setFlags Steruje sposobem przetwarzania segmentów.)
   @return(Wynikiem jest wskazany w parametrach segment napisu.) }
function  TokenStr(const   sText:  string;
                   const   nIndex:  integer;
                   const   sSep:  string;
                   const   sDefault:  string = '';
                   const   setFlags:  TGSkTokenFlags = [tfAllowEmptyTokens])
                   : string;                                         overload;


implementation


{$REGION 'Global subroutines'}


function  TokenChr(const   sText:  string;
                   const   nIndex:  Integer;
                   const   setSep:  TSysCharSet;
                   const   sDefault:  string;
                   const   setFlags:  TGSkTokenFlags)
                   : string;
begin
  with  TGSkCharTokens.Create(sText, setSep, setFlags)  do
    try
      if  (nIndex >= 0)
          and (nIndex < Count)  then
        Result := Token[nIndex]
      else
        Result := sDefault
    finally
      Free()
    end { try-finally }
end { TokenChr };


function  TokenChr(const   sText:  string;
                   const   nIndex:  Integer;
                   const   setSep:  TSysCharSet;
                   const   bAllowEmptyTokens:  Boolean)
                   : string;
begin
  with  TGSkCharTokens.Create(sText, setSep, bAllowEmptyTokens)  do
    try
      Result := Token[nIndex]
    finally
      Free()
    end { try-finally }
end { TokenChr };


function  TokenChr(const   sText:  string;
                   const   nIndex:  Integer;
                   const   cSep:  Char;
                   const   bAllowEmptyTokens:  Boolean)
                   : string;
begin
  with  TGSkCharTokens.Create(sText, cSep, bAllowEmptyTokens)  do
    try
      Result := Token[nIndex]
    finally
      Free()
    end { try-finally }
end { TokenChr };


function  TokenChr(const   sText:  string;
                   const   nIndex:  Integer;
                   const   cSep:  Char;
                   const   sDefault:  string;
                   const   setFlags:  TGSkTokenFlags)
                   : string;
begin
  with  TGSkCharTokens.Create(sText, cSep, setFlags)  do
    try
      if  (nIndex >= 0)
          and (nIndex < Count)  then
        Result := Token[nIndex]
      else
        Result := sDefault
    finally
      Free()
    end { try-finally }
end { TokenChr };


function  TokenStr(const   sText:  string;
                   const   nIndex:  Integer;
                   const   asSep:  array of string;
                   const   bAllowEmptyTokens:  Boolean)
                   : string;
begin
  with  TGSkStringTokens.Create(sText, asSep, bAllowEmptyTokens)  do
    try
      Result := Token[nIndex]
    finally
      Free()
    end { try-finally }
end { TokenStr };


function  TokenStr(const   sText:  string;
                   const   nIndex:  integer;
                   const   asSep:  array of string;
                     const   sDefault:  string;
                   const   setFlags:  TGSkTokenFlags)
                   : string;
begin
  with  TGSkStringTokens.Create(sText, asSep, setFlags)  do
    try
      if  (nIndex >= 0)
          and (nIndex < Count)  then
        Result := Token[nIndex]
      else
        Result := sDefault
    finally
      Free()
    end { try-finally }
end { TokenStr };


function  TokenStr(const   sText:  string;
                   const   nIndex:  Integer;
                   const   sSep:  string;
                   const   bAllowEmptyTokens:  Boolean)
                   : string;
begin
  with  TGSkStringTokens.Create(sText, sSep, bAllowEmptyTokens)  do
    try
      Result := Token[nIndex]
    finally
      Free()
    end { try-finally }
end { TokenStr };


function  TokenStr(const   sText:  string;
                   const   nIndex:  integer;
                   const   sSep:  string;
                   const   sDefault:  string;
                   const   setFlags:  TGSkTokenFlags)
                   : string;
begin
  with  TGSkStringTokens.Create(sText, sSep, setFlags)  do
    try
      if  (nIndex >= 0)
          and (nIndex < Count)  then
       Result := Token[nIndex]
      else
        Result := sDefault
    finally
      Free()
    end { try-finally }
end { TokenStr };

{$ENDREGION 'Global subroutines'}


{$REGION 'TGSkCustomTokens'}

constructor  TGSkCustomTokens.Create(AOwner:  TComponent);
begin
  inherited;
  fsetFlags := [tfAllowEmptyTokens];
  fTokens := TStringList.Create()
end { Create };


destructor  TGSkCustomTokens.Destroy();
begin
  fTokens.Free();
  inherited
end { Destroy };


function  TGSkCustomTokens.GetAllowEmpty() : Boolean;
begin
  Result := tfAllowEmptyTokens in fsetFlags
end { GetAllowEmpty };


function  TGSkCustomTokens.GetEnumerator() : TGSkTokensEnumerator;
begin
  Result := TGSkTokensEnumerator.Create(Self)
end { GetEnumerator };


function  TGSkCustomTokens.GetToken(const   nIndex:  Integer)
                                    : string;
begin
  SplitTokens();
  Result := FTokens.Strings[nIndex]
end { GetToken };


function  TGSkCustomTokens.GetTokenCount() : Integer;
begin
  SplitTokens();
  Result := FTokens.Count
end { GetTokenCount };


function  TGSkCustomTokens.GetTokenPos(const   nIndex:  Integer)
                                       : Integer;
begin
  SplitTokens();
  Result := Integer(FTokens.Objects[nIndex])
end { GetTokenPos };


procedure  TGSkCustomTokens.InitSplitTokens();
begin
  FTokens.Clear()
end { InitSplitTokens };


procedure  TGSkCustomTokens.NewToken(const   nTokenPos, nSepPos, nSepLen:  Integer);

var
  sBuf:  string;

begin  { NewToken }
  sBuf := Copy(fsText, nTokenPos, nSepPos - nTokenPos);
  if  tfTrimTokens in fsetFlags  then
    sBuf := Trim(sBuf);
  FTokens.AddObject(sBuf, TObject(nTokenPos))
end { NewToken };


procedure  TGSkCustomTokens.RemoveToken(const   nIndex:  Integer);
begin
  FTokens.Delete(nIndex)
end { RemoveToken };


procedure  TGSkCustomTokens.SetAllowEmpty(const   bValue:  Boolean);
begin
  if  bValue <> (tfAllowEmptyTokens in fsetFlags)  then
    begin
      if  bValue  then
        Include(fsetFlags, tfAllowEmptyTokens)
      else
        Exclude(fsetFlags, tfAllowEmptyTokens);
      fbSplitReq := True
    end { FAllowEmpty <> bValue }
end { SetAllowEmpty };


procedure  TGSkCustomTokens.SetFlags(const   setValue:  TGSkTokenFlags);
begin
  if  fsetFlags <> setValue  then
    begin
      fsetFlags := setValue;
      fbSplitReq := True
    end { fsetFlags <> setValue }
end { SetFlags };


procedure  TGSkCustomTokens.SetText(const   sValue:  string);
begin
  if  fsText <> sValue  then
    begin
      fsText := sValue;
      fbSplitReq := True
    end { FText <> sValue }
end { SetText };


procedure  TGSkCustomTokens.SplitTokens();

var
  nStart, nInd, nTextLen, nSepLen:  Integer;

begin  { SplitTokens }
  if  not fbSplitReq  then
    Exit;
  InitSplitTokens();
  nTextLen := Length(fsText);
  if  nTextLen > 0  then
    begin
      nStart := 1;
      nInd := 0;
      repeat
        Inc(nInd);
        nSepLen := SepLen(nInd);
        if  nSepLen <> 0  then
          begin
            NewToken(nStart, nInd, nSepLen);
            nStart := nInd + nSepLen;
            nInd := Pred(nStart)
          end { nLen <> 0 }
      until  nInd >= nTextLen;
      if  nStart <= nInd  then
        NewToken(nStart, Succ(nTextLen), 0)
      else if  nSepLen <> 0  then
        NewToken(nStart, nStart, nSepLen);
      if  not (tfAllowEmptyTokens in fsetFlags)  then
        for  nInd := FTokens.Count - 1  downto  0  do
          if  FTokens.Strings[nInd] = ''  then
            RemoveToken(nInd)
    end { nTextLen > 0 };
  fbSplitReq := False
end { SplitTokens };


function  TGSkCustomTokens.ToArray() : TArray<string>;

var
  nInd:  Integer;

begin  { ToArray }
  SetLength(Result, Count);
  for  nInd :=  0  to  Count - 1  do
    Result[nInd] := Token[nInd]
end { ToArray };

{$ENDREGION 'TGSkCustomTokens'}


{$REGION 'TGSkCharTokens'}

constructor  TGSkCharTokens.Create(AOwner:  TComponent);
begin
  inherited
end { Create };


constructor  TGSkCharTokens.Create(const   sText:  string;
                                   const   chSep:  Char;
                                   const   bAllowEmptyTokens:  Boolean);
begin
  Create(sText, [chSep], bAllowEmptyTokens)
end { Create };


constructor TGSkCharTokens.Create(const   sText:  string;
                                  const   setSep:  TSysCharSet;
                                  const   bAllowEmptyTokens:  Boolean);
begin
  Create(nil);
  Text := sText;
  SetDelimiters(setSep);
  if  bAllowEmptyTokens  then
    Flags := [tfAllowEmptyTokens]
  else
    Flags := [tfTrimTokens]
end { Create };


constructor  TGSkCharTokens.Create(const   sText:  string;
                                   const   setSep:  TSysCharSet;
                                   const   setFlags:  TGSkTokenFlags);
begin
  Create(nil);
  Text := sText;
  SetDelimiters(setSep);
  Flags := setFlags
end { Create };


constructor  TGSkCharTokens.Create(const   sText:  string;
                                   const   chSep:  Char;
                                   const   setFlags:  TGSkTokenFlags);
begin
  Create(nil);
  Text := sText;
  SetDelimiter(chSep);
  Flags := setFlags
end { Create };


destructor  TGSkCharTokens.Destroy();
begin
  inherited
end { Destroy };


function  TGSkCharTokens.GetDelimiters() : TSysCharSet;
begin
  Result := fsetDelim
end { GetDelimiters };


function  TGSkCharTokens.GetTokenSep(const   nIndex:  Integer)
                                     : Char;
var
  nPos:  Integer;

begin  { GetTokenSep }
  SplitTokens();
  if  nIndex < Pred(FTokens.Count)  then
    begin
      nPos := Integer(FTokens.Objects[nIndex+1])-1;
      Assert(CharInSet(Text[nPos], fsetDelim));
      if  not (tfAllowEmptyTokens in fsetFlags)  then
        while  (nPos > 1)
               and CharInSet(Text[Pred(nPos)], fsetDelim)  do
          Dec(nPos);
      Result := Text[nPos]
    end { nIndex < Pred(FTokens.Count) }
  else
    Result := #0
end { GetTokenSep };


function  TGSkCharTokens.SepLen(const   nPos:  Integer)
                                : Integer;
begin
  if  (Text <> '')
      and CharInSet(Text[nPos], fsetDelim)  then
    Result := 1
  else
    Result := 0
end { SepLen };


procedure  TGSkCharTokens.SetDelimiter(const   chDelim:  Char);
begin
  SetDelimiters([chDelim])
end { SetDelimiter };


procedure  TGSkCharTokens.SetDelimiters(const   setDelim:  TSysCharSet);
begin
  if  fsetDelim <> setDelim  then
    begin
      fsetDelim := setDelim;
      fbSplitReq := True
    end { fDelim <> setDelim }
end { SetDelimiters };

{$ENDREGION 'TGSkCharTokens'}


{$REGION 'TGSkStringTokens'}

constructor  TGSkStringTokens.Create(AOwner: TComponent);
begin
  inherited;
  fTokenSep := TStringList.Create()
end { Create };


constructor  TGSkStringTokens.Create(const   sText, sSep:  string;
                                     const   bAllowEmptyTokens:  Boolean);
begin
  Create(sText, [sSep], bAllowEmptyTokens)
end { Create };


constructor  TGSkStringTokens.Create(const   sText:  string;
                                     const   asSep:  array of string;
                                     const   bAllowEmptyTokens:  Boolean);
begin
  Create(nil);
  Text := sText;
  SetDelimiters(asSep);
  AllowEmptyTokens := bAllowEmptyTokens
end { Create };


constructor  TGSkStringTokens.Create(const   sText:  string;
                                     const   asSep:  array of string;
                                     const   setFlags:  TGSkTokenFlags);
begin
  Create(nil);
  Text := sText;
  SetDelimiters(asSep);
  Flags := setFlags
end { Create };


constructor  TGSkStringTokens.Create(const   sText, sSep:  string;
                                     const   setFlags:  TGSkTokenFlags);
begin
  Create(nil);
  Text := sText;
  SetDelimiter(sSep);
  Flags := setFlags
end { Create };


destructor  TGSkStringTokens.Destroy();
begin
  fTokenSep.Free();
  inherited
end { Destroy };


function  TGSkStringTokens.GetDelimiter(const   nIndex:  Integer)
                                        : string;
begin
  Result := fDelim[nIndex]
end { GetDelimiter };


function  TGSkStringTokens.GetDelimiterCount() : Integer;
begin
  Result := Length(fDelim)
end { GetDelimiterCount };


function  TGSkStringTokens.GetTokenSep(const   nIndex:  Integer)
                                       : string;
begin
  SplitTokens();
  if  nIndex < fTokenSep.Count  then
    Result := fTokenSep[nIndex]
  else
    Result := ''
end { GetTokenSep };


procedure  TGSkStringTokens.InitSplitTokens();

  procedure  SortDelim();

  var
    nIndL, nIndH:  Integer;
    sBuf:  string;

  begin  { SortDelim }
    for  nIndL := Low(fDelim)  to  Pred(High(fDelim))  do
      for  nIndH := Succ(nIndL)  to  High(fDelim)  do
        if  (Length(fDelim[nIndL]) > Length(fDelim[nIndH]))
            or (Length(fDelim[nIndL]) = Length(fDelim[nIndH]))
               and (fDelim[nIndL] > fDelim[nIndH])  then
          begin
            sBuf := fDelim[nIndL];
            fDelim[nIndL] := fDelim[nIndH];
            fDelim[nIndH] := sBuf
          end
  end { SortDelim };

begin  { InitSplitTokens }
  inherited;
  fTokenSep.Clear();
  SortDelim()
end { InitSplitTokens };


procedure  TGSkStringTokens.NewToken(const   nTokenPos, nSepPos, nSepLen:  Integer);
begin
  inherited;
  fTokenSep.Add(Copy(fsText, nSepPos, nSepLen))
end { NewToken };


procedure  TGSkStringTokens.RemoveToken(const   nIndex:  Integer);
begin
  inherited;
  fTokenSep.Delete(nIndex)
end { RemoveToken };


function  TGSkStringTokens.SepLen(const   nPos:  Integer)
                                  : Integer;
var
  fnComp:  function(const   sBuf1, sBuf2:  string)
                    : Boolean;
  nInd:  Integer;
  sSep:  string;

begin  { SepLen }
  {$IFDEF UNICODE}
    if  fbCaseSensitive  then
      fnComp := SameStr
    else
      fnComp := SameText;
  {$ELSE}
    {$IFDEF UNICODE}
      if  fbCaseSensitive  then
        fnComp := AnsiSameText
      else
        fnComp := AnsiSameText;
    {$ELSE}
      if  fbCaseSensitive  then
        fnComp := SameStr
      else
        fnComp := SameText
      {$ELSE}
      {$ENDIF -UNICODE}
  {$ENDIF -UNICODE};
  for  nInd := High(fDelim)  downto  Low(fDelim)  do
    begin
      sSep := fDelim[nInd];
      Result := Length(sSep);
      if  fnComp(sSep,
                 Copy(fsText, nPos, Result))  then
        Exit
    end { for nInd };
  Result := 0
end { SepLen };


procedure  TGSkStringTokens.SetDelimiter(const   sDelim:  string;
                                         const   nIndex:  Integer);
begin
  if  fDelim[nIndex] <> sDelim  then
    begin
      fDelim[nIndex] := sDelim;
      fbSplitReq := True
    end { fDelim[nIndex] <> sDelim }
end { SetDelimiter };


procedure  TGSkStringTokens.SetCaseSensitive(const   bValue:  Boolean);
begin
  if  fbCaseSensitive <> bValue  then
    begin
      fbCaseSensitive := bValue;
      fbSplitReq := True
    end { fbCaseSensitive <> bValue }
end { SetCaseSensitive };


procedure  TGSkStringTokens.SetDelimiter(const   sDelim:  string);
begin
  SetDelimiters([sDelim])
end { SetDelimiter };


procedure  TGSkStringTokens.SetDelimiters(const   Delim:  array of string);

var
  nInd:  Integer;

begin  { SetDelimiters }
  fbSplitReq := Length(fDelim) <> Length(Delim);
  SetLength(fDelim, Length(Delim));
  for  nInd := High(Delim)  downto  Low(Delim)  do
    SetDelimiter(Delim[nInd], nInd)
end { SetDelimiters };

{$ENDREGION 'TGSkStringTokens'}


{$REGION 'TGSkTokensEnumerator'}

constructor  TGSkTokensEnumerator.Create(const   Tokens:  TGSkCustomTokens);
begin
  inherited Create();
  fTokens := Tokens;
  fnIndex := -1
end { Create };


function  TGSkTokensEnumerator.GetCurrent() : string;
begin
  Result := fTokens.Token[fnIndex]
end { GetCurrent };


function  TGSkTokensEnumerator.MoveNext() : Boolean;
begin
  Result := fnIndex < fTokens.Count - 1;
  if  Result  then
    Inc(fnIndex)
end { MoveNext };

{$ENDREGION 'TGSkTokensEnumerator'}


end.

