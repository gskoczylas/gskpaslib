﻿{: @abstract Podprogramy wspomagające działanie w trybie znakowym. }
unit  GSkPasLib.Console;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


uses
  WinAPI.Windows, Winapi.ShellAPI,
  System.SysUtils;


type
  {: @abstract Flagi sterujące działaniem funkcji @link(ReadKey).

     @value(rfEcho Funkcja wyświetla wciśnięty znak.)
     @value(rfUpperCase Funkcja automatycznie zamienia małe litery na wielkie.)
     @value(rfIncludeCtrls Funkcja ma zwracać wynik również po naciśnięciu klawiszy
                           sterujących, takich jak Shift, Ctrl, Alt, ScrollLock,
                           NumLock, itp.)

     @seeAlso(ReadKey) }
  TReadKeyFlag = (rfEcho, rfUpperCase, rfIncludeCtrls);
  {: Flagi sterujące działaniem funkcji @link(ReadKey).

     @seeAlso(TReadKeyFlag) }
  TReadKeyFlags = set of  TReadKeyFlag;


{: @abstract Oczekiwanie na wciśnięcie klawisza.

   Funkcja @name czeka na wciśnięcie dowolnego klawisza. Wynikiem funkcji jest
   znak odpowiadający wiśniętemu klawiszowi.

   @param(sPrompt Ten tekst jest wyświetlany na wyjściu StdErr.)
   @param(setFlags Flagi sterujące działaniem funkcji.)

   @seeAlso(TReadKeyFlag) }
function  ReadKey(const   sPrompt:  string = '';
                  const   setFlags:  TReadKeyFlags = [])
                  : Char;                                            overload;

{: @abstract Oczekiwanie na wciśnięcie klawisza.

   Funkcja @name czeka na wciśnięcie dowolnego klawisza. Wynikiem funkcji jest
   znak odpowiadający wiśniętemu klawiszowi.

   @param(setFlags Flagi sterujące działaniem funkcji.)

   @seeAlso(TReadKeyFlag) }
function  ReadKey(const   setFlags:  TReadKeyFlags)
                  : Char;                                            overload;

{: @abstract Uruchamia inny program w tej samej konsoli.

   Funkcja @name uruchamia inny program w tej samej konsoli i czeka na jego
   zakończenie.

   @param(sCommandLine Linia poleceń uruchamianego programu.)
   @param (sErrorMsg Jeżeli podczas wykonywania programu wystąpiły błedy, to
                     ten parametr zwraca komunikat błędu.)
   @return(Jeżeli parametr @code(sErrorMsg) zwraca niepusty tekst, to wynikiem
           funkcji jest kod błędu wykonania programu (jeden z błędów Windows).

           Jeżeli parametr @code(sErrorMsg) zwraca pusty napis, to wynikiem
           funkcji jest kod powrotu (@code(ExitCode)) wykonywanego programu.) }
function  RunConsoleApp(const   sCommandLine:  string;
                        out     sErrorMsg:  string)
                        : DWORD;


implementation


function  ReadKey(const   sPrompt:  string;
                  const   setFlags:  TReadKeyFlags)
                  : Char;

var
  lInp:  TInputRecord;
  nEvn:  DWORD;

begin  { ReadKey }
  if  sPrompt <> ''  then
    begin
      Write(ErrOutput, sPrompt);
      if  sPrompt[Length(sPrompt)] <> ' '  then
        Write(ErrOutput, ' ')
    end { sPrompt <> '' };
  {-}
  repeat
  until  ReadConsoleInput(GetStdHandle(STD_INPUT_HANDLE),
                          lInp, 1, nEvn)
         and (lInp.EventType = KEY_EVENT)
         and (lInp.Event.KeyEvent.bKeyDown)
         and ((rfIncludeCtrls in setFlags)
              or not(lInp.Event.KeyEvent.wVirtualKeyCode in
                       [VK_SHIFT, VK_CONTROL, VK_MENU, VK_PAUSE, VK_CAPITAL,
                        VK_PRINT, VK_SNAPSHOT, VK_INSERT, VK_DELETE, VK_SLEEP,
                        VK_NUMLOCK, VK_SCROLL, VK_MEDIA_NEXT_TRACK,
                        VK_MEDIA_PREV_TRACK, VK_MEDIA_STOP, VK_MEDIA_PLAY_PAUSE,
                        VK_LAUNCH_MAIL, VK_LAUNCH_MEDIA_SELECT, VK_LAUNCH_APP1,
                        VK_LAUNCH_APP2, VK_PLAY, VK_ZOOM]));
  {-}
  Result := lInp.Event.KeyEvent.UnicodeChar;
  if  rfUpperCase in setFlags  then
    Result := UpCase(Result);
  if  rfEcho in setFlags  then
    Write(ErrOutput, Result)
end { ReadKey };


function  ReadKey(const   setFlags:  TReadKeyFlags)
                  : Char;
begin
  Result := ReadKey('', setFlags)
end { ReadKey };


function  RunConsoleApp(const   sCommandLine:  string;
                        out     sErrorMsg:  string)
                        : DWORD;
var
  lStartInfo:  TStartupInfo;
  lProcInfo:   TProcessInformation;

begin  { RunConsoleApp }
  ZeroMemory(Addr(lStartInfo), SizeOf(lStartInfo));
  lStartInfo.cb := SizeOf(lStartInfo);
  lStartInfo.dwFlags := STARTF_USESTDHANDLES;
  lStartInfo.hStdInput := GetStdHandle(STD_INPUT_HANDLE);
  lStartInfo.hStdOutput := GetStdHandle(STD_OUTPUT_HANDLE);
  lStartInfo.hStdError := GetStdHandle(STD_ERROR_HANDLE);
  {-}
  if  not CreateProcess(nil, PChar(sCommandLine),
                        nil, nil, True, 0, nil, nil,
                        lStartInfo, lProcInfo)  then
    begin
      Result := GetLastError();
      sErrorMsg := SysErrorMessage(Result);
      Exit
    end { not CreateProcess(...) };
  {-}
  WaitForSingleObject(lProcInfo.hProcess, INFINITE);
  if  not GetExitCodeProcess(lProcInfo.hProcess, Result)  then
    begin
      Result := GetLastError();
      sErrorMsg := SysErrorMessage(Result)
    end { not GetExitCodeProcess(...) };
  {-}
  CloseHandle(lProcInfo.hProcess);
  CloseHandle(lProcInfo.hThread)
end { RunConsoleApp };


end.
