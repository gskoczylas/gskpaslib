unit TestODBC.Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    btnGetSources: TButton;
    edtList: TMemo;
    procedure btnGetSourcesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  GSkPasLib.ODBC;

{$R *.dfm}

procedure TForm1.btnGetSourcesClick(Sender: TObject);
begin
  GetDSNList(edtList.Lines);
end;

end.

