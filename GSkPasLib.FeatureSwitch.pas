﻿{: @abstract Moduł @name definiuje klasę pozwalającą łatwo definiować przełączniki funkcji.

   Czasem w programach potrzebujemy zaimplementować eksperymentalny kod,
   który ma być aktywny tylko w niektórych przypadkach. W takim przypadku
   klasa @link(TGSkFeatureSwitch) pozwala łatwo zaimplementować przełączniki
   aktywujące taki kod. }
unit  GSkPasLib.FeatureSwitch;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$IF CompilerVersion > 22} // XE
  {$LEGACYIFEND ON}
{$IFEND}


uses
  {$IF CompilerVersion > 22} // XE
    System.IniFiles, System.SysUtils;
  {$ELSE}
    IniFiles, SysUtils;
  {$IFEND}


const
  {: @abstract Domyślna nazwa sekcji w pliku konfiguracyjnym. }
  gcsDefaultFeaturesSectionName = 'Features';


type
  {: @abstract Implementacja przełączników aktywujących dodatkowe działania.

   Czasem w programach potrzebujemy zaimplementować eksperymentalny kod,
   który ma być aktywny tylko w niektórych przypadkach. W takim przypadku
   klasa @name pozwala łatwo zaimplementować przełączniki aktywujące taki kod. }
  TGSkFeatureSwitch = class
  strict private
    class var
      fFeatures:      TIniFile;
      fsSectionName:  string;
    class function  GetEffectiveConfFileName(const   sFileName:  string)
                                             : string;
  public
    {: @exclude - standardowy destruktor }
    class destructor  Destroy();
    {-}

    {: @abstract Wskazuje plik i sekcję, w którym są zdefiniowane przełączniki.

       @param(sFileName Ścieżka do pliku konfiguracyjnego.)
       @param(sSectionName Domyślna sekcja przełączników.)

       @warning(Jeżeli zdecydujesz się wskazać niestanradrdową sekcję, to musisz
                zachować dużą ostrożność. Może to spowodować, że inne przełączniki,
                zdefiniowane w innych modułach kodu, mogą przestać działać zgodnie
                z oczekiwaniami.)

      @seeAlso(Feature) }
    class procedure  InitializeFeatures(const   sFileName:  string = '';
                                        const   sSectionName:  string = gcsDefaultFeaturesSectionName);

    {: @abstract Zwraca wartość przełącznika w domyślnej sekcji.

       Jeżeli wcześniej nie została wywołana metoda @link(InitializeFeatures),
       to funkcja zwraca wartość domyślną.

       @param(sName Nazwa przełącznika.)
       @param(bDefault Wartość domyślna, jeżeli przełącznik nie jest zdefiniowany.)

       @return(Wynikiem jest wartość przełącznika lub wartość domyślna.)

       @seeAlso(InitializeFeatures) }
    class function  Feature(const   sName:  string;
                            const   bDefault:  Boolean = False)
                            : Boolean;                               overload;

    {: @abstract Zwraca wartość przełącznika w domyślnej sekcji.

       Jeżeli wcześniej nie została wywołana metoda @link(InitializeFeatures),
       to funkcja zwraca wartość domyślną.

       @param(sName Nazwa przełącznika.)
       @param(sSection Nazwa sekcji przełączników.)
       @param(bDefault Wartość domyślna, jeżeli przełącznik nie jest zdefiniowany.)

       @return(Wynikiem jest wartość przełącznika lub wartość domyślna.)

       @seeAlso(InitializeFeatures) }
    class function  Feature(const   sName:  string;
                            const   sSection:  string;
                            const   bDefault:  Boolean = False)
                            : Boolean;                               overload;
  end { TGSkFeatureSwitch };


implementation


uses
  GSkPasLib.FileUtils;


{$REGION 'TGSkFeatureSwitch'}

class destructor  TGSkFeatureSwitch.Destroy();
begin
  FreeAndNil(fFeatures)
end { Destroy };


class function  TGSkFeatureSwitch.Feature(const   sName:  string;
                                          const   bDefault:  Boolean)
                                          : Boolean;
begin
  Result := Feature(sName, fsSectionName, bDefault)
end { Feature };


class function   TGSkFeatureSwitch.Feature(const   sName, sSection:  string;
                                           const   bDefault:  Boolean)
                                           : Boolean;
begin
  if  fFeatures <> nil  then
    Result := fFeatures.ReadBool(sSection, sName, bDefault)
  else
    Result := bDefault
end { Feature };


class function  TGSkFeatureSwitch.GetEffectiveConfFileName(const   sFileName:  string)
                                                           : string;
begin
  if  sFileName <> ''  then
    Exit(sFileName);
  Result := ChangeFileExt(GetModuleFileName(), '.ini')
end { GetEffectiveConfFileName };


class procedure  TGSkFeatureSwitch.InitializeFeatures(const   sFileName, sSectionName:  string);
begin
  fFeatures := TIniFile.Create(GetEffectiveConfFileName(sFileName));
  fsSectionName := sSectionName
end { InitializeFeatures };

{$ENDREGION 'TGSkFeatureSwitch'}


end.

