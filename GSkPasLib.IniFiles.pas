﻿{: @abstract Definicja klasy osbługującej pliki INI w aplikacjach wielowątkowych.

   Moduł @name definiuje klasę @link(TGSKThreadIniFile), który można bezpiecznie
   używać w aplikacjach wielowątkowych. }
unit  GSkPasLib.IniFiles;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


uses
  System.IniFiles, System.SysUtils, System.Classes;


type
  TCustomIniFileClass = class of TCustomIniFile;

  {: @abstract Klasa obsługująca pliki INI w aplikacjach wielowątkowych.

     Klasa @name może być bezpiecznie używana w aplikacjach wielowątkowych
     (jest @italic(Thread Safe)). Klasa automatycznie synchronizuje dostęp
     do pliku INI.

     @warning(W niektórych przypadkach wskazana jest samodzielna synchronizacja
              dostępu do pliku. Na przykład załóżmy, że chcemy sprawdzić istnienie
              jakiegoś parametru i usunąć go, jeżeli istnieje. W takim
              przypadku algorytm wydaje się bardzo prosty: @longCode(#
               var
                 lConfig:  @name;
               begin
                 if lConfig.ValueExists('sekcja', 'parametr') then
                   lConfig.DeleteKey('sekcja, 'parametr') #)

              W tym przykładzie klasa @name automatycznie zabezpieczy wywołanie
              metody @code(ValueExists) oraz metody @code(DeleteKey). Jednak
              w aplikacjach wielowątkowych może się zdarzyć, że pomiędzy wywołaniami
              tych metod jakiś inny wątek może zmienić ten parametr.
              Aby się zabezpieczyć przed taką sytuacją prawidłowy algorym powinnien
              wyglądać tak: @longCode(#
               var
                 lConfig:  @name;
               begin
                 lConfig.BeginWrite();
                 try
                   if  lConfig.ValueExists('sekcja', 'parametr') then
                     lConfig.DeleteKey('sekcja', 'parametr');
                 finally
                   lConfig.EndWrite()
                 end #)) }
  TGSkThreadIniFile = class(TCustomIniFile)
  strict private
    type
      TIniFileHack = class(TCustomIniFile);
    var
      fMREW:     TMultiReadExclusiveWriteSynchronizer;
      fINI:      TCustomIniFile;
    function  GetFileName() : string;
  strict protected
    procedure  InternalReadSections(const   sSection:  string;
                                    Strings:  TStrings;
                                    bSubSectionNamesOnly:  Boolean;
                                    bRecurse:  Boolean);             override;
  public
    {: @abstract Tworzy klasę analogiczną do standardowego komponentu @code(TIniFile) }
    constructor  Create(const   sFileName:  string);                 overload;
    {: @abstract Tworzy klase bazującą na wskazanym standardowym komponencie.

       @param(sFileName Nazwa pliku INI.)
       @param(BaseClass Klasa potomna po @code(TCustomIniFile).

              Najczęściej tym parametrem jest albo klasa @code(TIniFile),
              albo @code(TMemIniFile).) }
    constructor  Create(const   sFileName:  string;
                        const   BaseClass:  TCustomIniFileClass);    overload;
    {: @exclude }
    destructor  Destroy();                                           override;
    {-}
    {: Abstract Rozpoczyna modyfikowanie pliku INI

       Funkcja działa do takiej samej metody w standardowym komponencie
       @code(TMultiReadExclusiveWriteSynchronizer). }
    function  BeginWrite() : Boolean;                                inline;
    procedure  EndWrite();                                           inline;
    procedure  BeginRead();                                          inline;
    procedure  EndRead();                                            inline;
    {-}
    function  SectionExists(const  sSection:  string)
                            : Boolean;                               override;
    function  ReadString(const   sSection, sIdent:  string;
                         const   sDefault:  string = '')
                         : string;                                   override;
    function  ReadInteger(const   sSection, sIdent:  string;
                          nDefault:  Integer = 0)
                          : Integer;                                 override;
    function  ReadInt64(const   sSection, sIdent:  string;
                        nDefault:  Int64 = 0)
                        : Int64;                                     override;
    function  ReadBool(const   sSection, sIdent:  string;
                       bDefault:  Boolean = False)
                       : Boolean;                                    override;
    function  ReadBinaryStream(const   sSection, sName:  string;
                               Value:  TStream)
                               : Integer;                            override;
    function  ReadDate(const   sSection, sName:  string;
                       tmDefault:  TDateTime = 0)
                       : TDateTime;                                  override;
    function  ReadDateTime(const   sSection, sName:  string;
                           tmDefault:  TDateTime = 0)
                           : TDateTime;                              override;
    function  ReadFloat(const   sSection, sName:  string;
                        nDefault:  Double = 0.0)
                        : Double;                                    override;
    function  ReadTime(const   sSection, sName:  string;
                       tmDefault:  TDateTime = 0.0)
                       : TDateTime;                                  override;
    function  ValueExists(const   sSection, sIdent:  string)
                          : Boolean;                                 override;
    procedure  ReadSection(const   sSection:  string;
                           Strings:  TStrings);                      override;
    procedure  ReadSections(Strings:  TStrings);                     override;
    procedure  ReadSections(const   sSection:  string;
                            Strings:  TStrings);                     override;
    procedure  ReadSubSections(const   sSection:  string;
                               Strings:  TStrings;
                               bRecurse: Boolean = False);           override;
    procedure  ReadSectionValues(const   sSection:  string;
                                 Strings:  TStrings);                override;
    procedure  WriteString(const   sSection, sIdent:  string;
                           const   sValue:  string);                 override;
    procedure  WriteInteger(const   sSection, sIdent:  string;
                            nValue:  Integer);                       override;
    procedure  WriteInt64(const   sSection, sIdent:  string;
                          nValue:  Int64);                           override;
    procedure  WriteBool(const   sSection, sIdent:  string;
                         bValue:  Boolean);                          override;
    procedure  WriteBinaryStream(const   sSection, sName:  string;
                                 Value:  TStream);                   override;
    procedure  WriteDate(const   sSection, sName:  string;
                         tmValue:  TDateTime);                       override;
    procedure  WriteDateTime(const   sSection, sName:  string;
                             tmValue:  TDateTime);                   override;
    procedure  WriteFloat(const   sSection, sName:  string;
                          nValue:  Double);                          override;
    procedure  WriteTime(const   sSection, sName:  string;
                         tmValue:  TDateTime);                       override;
    procedure  EraseSection(const   sSection:  string);              override;
    procedure  DeleteKey(const   sSection, sIdent:  string);         override;
    procedure  UpdateFile();                                         override;
    {-}
    property  FileName:  string
              read  GetFileName;
  end { TGSkThreadIniFile };


implementation


{$REGION 'TGSkThreadIniFile'}

procedure  TGSkThreadIniFile.BeginRead();
begin
  fMREW.BeginRead()
end { BeginRead };


function  TGSkThreadIniFile.BeginWrite() : Boolean;
begin
  Result := fMREW.BeginWrite()
end { BeginWrite };


constructor  TGSkThreadIniFile.Create(const   sFileName:  string);
begin
  Create(sFileName, TIniFile)
end { Create };


constructor  TGSkThreadIniFile.Create(const   sFileName:  string;
                                      const   BaseClass:  TCustomIniFileClass);
begin
  Assert(BaseClass.InheritsFrom(TIniFile) or BaseClass.InheritsFrom(TMemIniFile));
  inherited Create(sFileName);
  fMREW := TMultiReadExclusiveWriteSynchronizer.Create();
  fINI := BaseClass.Create(sFileName)
end { Create };


procedure  TGSkThreadIniFile.DeleteKey(const   sSection, sIdent:  string);
begin
  BeginWrite();
  try
    fINI.DeleteKey(sSection, sIdent)
  finally
    EndWrite()
  end { try-finally }
end { DeleteKey };


destructor  TGSkThreadIniFile.Destroy();
begin
  fINI.Free();
  fMREW.Free();
  inherited
end { Destroy };


procedure  TGSkThreadIniFile.EndRead();
begin
  fMREW.EndRead()
end { EndRead };


procedure  TGSkThreadIniFile.EndWrite();
begin
  fMREW.EndWrite()
end { EndWrite };

procedure  TGSkThreadIniFile.EraseSection(const   sSection:  string);
begin
  BeginWrite();
  try
    fINI.EraseSection(sSection)
  finally
    EndWrite()
  end { try-finally }
end { EraseSection };


function  TGSkThreadIniFile.GetFileName() : string;
begin
  BeginRead();
  try
    Result := fINI.FileName
  finally
    EndRead()
  end { try-finally }
end { GetFileName };


procedure  TGSkThreadIniFile.InternalReadSections(const   sSection:  string;
                                                  Strings:  TStrings;
                                                  bSubSectionNamesOnly, bRecurse:  Boolean);
begin
  BeginRead();
  try
    TIniFileHack(fINI).InternalReadSections(sSection, Strings, bSubSectionNamesOnly, bRecurse)
  finally
    EndRead()
  end { try-finally }
end { InternalReadSections };


function  TGSkThreadIniFile.ReadBinaryStream(const   sSection, sName:  string;
                                             Value:  TStream)
                                             : Integer;
begin
  BeginWrite();
  try
    Result := fINI.ReadBinaryStream(sSection, sName, Value)
  finally
    EndWrite()
  end { try-finally }
end { ReadBinaryStream };


function  TGSkThreadIniFile.ReadBool(const   sSection, sIdent:  string;
                                     bDefault:  Boolean)
                                     : Boolean;
begin
  BeginRead();
  try
    Result := fINI.ReadBool(sSection, sIdent, bDefault)
  finally
    EndRead()
  end { try-finally }
end { ReadBool };


function  TGSkThreadIniFile.ReadDate(const   sSection, sName:  string;
                                     tmDefault:  TDateTime)
                                     : TDateTime;
begin
  BeginWrite();
  try
    Result := fINI.ReadDate(sSection, sName, tmDefault)
  finally
    EndWrite()
  end { try-finally }
end { ReadDate };


function  TGSkThreadIniFile.ReadDateTime(const   sSection, sName:  string;
                                         tmDefault:  TDateTime)
                                         : TDateTime;
begin
  BeginRead();
  try
    Result := fINI.ReadDateTime(sSection, sName, tmDefault)
  finally
    EndRead()
  end { try-finally }
end { ReadDateTime };


function  TGSkThreadIniFile.ReadFloat(const   sSection, sName:  string;
                                      nDefault:  Double)
                                      : Double;
begin
  BeginRead();
  try
    Result := fINI.ReadFloat(sSection, sName, nDefault)
  finally
    EndRead()
  end { try-finally }
end { ReadFloat };


function  TGSkThreadIniFile.ReadInt64(const   sSection, sIdent:  string;
                                      nDefault:  Int64)
                                      : Int64;
begin
  BeginRead();
  try
    Result := fINI.ReadInt64(sSection, sIdent, nDefault)
  finally
    EndRead()
  end { try-finally }
end { ReadInt64 };


function  TGSkThreadIniFile.ReadInteger(const   sSection, sIdent:  string;
                                        nDefault:  Integer)
                                        : Integer;
begin
  BeginRead();
  try
    Result := fINI.ReadInteger(sSection, sIdent, nDefault)
  finally
    EndRead()
  end { try-finally }
end { ReadInteger };


procedure  TGSkThreadIniFile.ReadSection(const   sSection:  string;
                                         Strings:  TStrings);
begin
  BeginRead();
  try
    fINI.ReadSection(sSection, Strings)
  finally
    EndRead()
  end { try-finally }
end { ReadSection };


procedure  TGSkThreadIniFile.ReadSections(Strings:  TStrings);
begin
  BeginRead();
  try
    fINI.ReadSections(Strings)
  finally
    EndRead()
  end { try-finally }
end { ReadSections };


procedure  TGSkThreadIniFile.ReadSections(const   sSection:  string;
                                          Strings:  TStrings);
begin
  BeginRead();
  try
    fINI.ReadSections(sSection, Strings)
  finally
    EndRead()
  end { try-finally }
end { ReadSections };


procedure  TGSkThreadIniFile.ReadSectionValues(const   sSection:  string;
                                               Strings:  TStrings);
begin
  BeginRead();
  try
    fINI.ReadSectionValues(sSection, Strings)
  finally
    EndRead()
  end { try-finally }
end { ReadSectionValues };


function  TGSkThreadIniFile.ReadString(const   sSection, sIdent, sDefault:  string)
                                       : string;
begin
  BeginRead();
  try
    Result := fINI.ReadString(sSection, sIdent, sDefault)
  finally
    EndRead()
  end { try-finally }
end { ReadString };


procedure  TGSkThreadIniFile.ReadSubSections(const   sSection:  string;
                                             Strings:  TStrings;
                                             bRecurse:  Boolean);
begin
  BeginRead();
  try
    fINI.ReadSubSections(sSection, Strings, bRecurse)
  finally
    EndRead()
  end { try-finally }
end { ReadSubSections };


function  TGSkThreadIniFile.ReadTime(const   sSection, sName:  string;
                                     tmDefault:  TDateTime)
                                     : TDateTime;
begin
  BeginRead();
  try
    Result := fINI.ReadTime(sSection, sName, tmDefault)
  finally
    EndRead()
  end { try-finally }
end { ReadTime };


function  TGSkThreadIniFile.SectionExists(const   sSection:  string)
                                          : Boolean;
begin
  BeginRead();
  try
    Result := fINI.SectionExists(sSection)
  finally
    EndRead()
  end { try-finally }
end { SectionExists };


procedure  TGSkThreadIniFile.UpdateFile();
begin
  BeginWrite();
  try
    fINI.UpdateFile()
  finally
    EndWrite()
  end { try-finally }
end { UpdateFile };


function  TGSkThreadIniFile.ValueExists(const   sSection, sIdent:  string)
                                        : Boolean;
begin
  BeginRead();
  try
    Result := fINI.ValueExists(sSection, sIdent)
  finally
    EndRead()
  end { try-finally }
end { ValueExists };


procedure  TGSkThreadIniFile.WriteBinaryStream(const   sSection, sName:  string;
                                               Value:  TStream);
begin
  BeginWrite();
  try
    fINI.WriteBinaryStream(sSection, sName, Value)
  finally
    EndWrite()
  end { try-finally }
end { WriteBinaryStream };


procedure  TGSkThreadIniFile.WriteBool(const   sSection, sIdent:  string;
                                       bValue:  Boolean);
begin
  BeginWrite();
  try
    fINI.WriteBool(sSection, sIdent, bValue)
  finally
    EndWrite()
  end { try-finally }
end { WriteBool };


procedure  TGSkThreadIniFile.WriteDate(const   sSection, sName:  string;
                                       tmValue:  TDateTime);
begin
  BeginWrite();
  try
    fINI.WriteDate(sSection, sName, tmValue)
  finally
    EndWrite()
  end { try-finally }
end { WriteDate };


procedure  TGSkThreadIniFile.WriteDateTime(const   sSection, sName:  string;
                                           tmValue:  TDateTime);
begin
  BeginWrite();
  try
    fINI.WriteDateTime(sSection, sName, tmValue)
  finally
    EndWrite()
  end { try-finally }
end { WriteDateTime };


procedure  TGSkThreadIniFile.WriteFloat(const   sSection, sName:  string;
                                        nValue:  Double);
begin
  BeginWrite();
  try
    fINI.WriteFloat(sSection, sName, nValue)
  finally
    EndWrite()
  end { try-finally }
end { WriteFloat };


procedure  TGSkThreadIniFile.WriteInt64(const   sSection, sIdent:  string;
                                        nValue:  Int64);
begin
  BeginWrite();
  try
    fINI.WriteInt64(sSection, sIdent, nValue)
  finally
    EndWrite()
  end { try-finally }
end { WriteInt64 };


procedure  TGSkThreadIniFile.WriteInteger(const   sSection, sIdent:  string;
                                          nValue:  Integer);
begin
  BeginWrite();
  try
    fINI.WriteInteger(sSection, sIdent, nValue)
  finally
    EndWrite()
  end { try-finally }
end { WriteInteger };


procedure  TGSkThreadIniFile.WriteString(const   sSection, sIdent, sValue:  string);
begin
  BeginWrite();
  try
    fINI.WriteString(sSection, sIdent, sValue)
  finally
    EndWrite()
  end { try-finally }
end { WriteString };


procedure  TGSkThreadIniFile.WriteTime(const   sSection, sName:  string;
                                       tmValue:  TDateTime);
begin
  BeginWrite();
  try
    fINI.WriteTime(sSection, sName, tmValue)
  finally
    EndWrite()
  end { try-finally }
end { WriteTime };

{$ENDREGION 'TGSkThreadIniFile'}


end.
