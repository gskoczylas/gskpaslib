﻿unit  GSkPasLib.PingHostImpl;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


uses
  WinAPI.Windows, WinAPI.WinSock,
  System.SysUtils,
  GSkPasLib.PingHost;


type
  TGSkPingHostImpl = class(TInterfacedObject, IGSkPingInterface)
  strict private
    type
      TEchoReply = packed record
        Addr: In_Addr;
        Status: DWORD;
        RoundTripTime: DWORD;
      end { TEchoReply };
      PEchoReply = ^TEchoReply;
    var
      fWSAData:   TWSAData;
      fsErrMsg:   string;
      fnErrCode:  Integer;
    procedure  RaiseError(const   sMethod:  string;
                          const   nErrCode:  Integer;
                          const   sErrInfo:  string);                overload;
    procedure  RaiseError(const   sMethod:  string;
                          const   nErrCode:  Integer;
                          const   sErrFrmt:  string;
                          const   ErrArgs:  array of const);         overload;
    procedure  RaiseLastOSError(const   sMethod:  string);
    procedure  CheckStatus(const   sMethod:  string;
                           const   nStatus:  Integer);
    procedure  Startup();
    procedure  Cleanup();
    { IPingInterface }
    function  GetErrorMessage() : string;
    function  GetErrorCode() : Integer;
  public
    { IPingInterface }
    function  PingHost(const   sHost:  string;
                       const   nTimeoutMS:  Cardinal = gcnDefaultPingTimeout)
                       : Boolean;
  end { TGSkPingHostImpl };


implementation


uses
  GSkPasLib.PasLibInternals;


{ W Winows XP lub Windows Server 2003 poniższe funkcje były w innej bibliotece. }

function  IcmpCreateFile() : THandle;                                stdcall;
  external 'iphlpapi.dll';


function  IcmpCloseHandle(icmpHandle:  THandle)
                          : Boolean;                                 stdcall;
  external 'iphlpapi.dll';


function  IcmpSendEcho(icmpHandle:  THandle;
                       DestinationAddress:  In_Addr;
                       RequestData:  Pointer;
                       RequestSize:  SmallInt;
                       RequestOptions:  Pointer;
                       ReplyBuffer:  Pointer;
                       ReplySize:  DWORD;
                       Timeout:  DWORD)
                       : DWORD;                                      stdcall;
  external 'iphlpapi.dll';


{$REGION 'TGSkPingHostImpl'}

procedure  TGSkPingHostImpl.CheckStatus(const   sMethod:  string;
                                        const   nStatus:  Integer);
begin
  if  nStatus <> 0  then
    RaiseError(sMethod, nStatus, 'Intermal method "%0:s" returned error code %1:d',
               [sMethod, nStatus])
end { CheckStatus };


procedure  TGSkPingHostImpl.Cleanup();
begin
  CheckStatus('WSACleanup',
              WSACleanup())
end { Cleanup };


function  TGSkPingHostImpl.GetErrorCode() : Integer;
begin
  Result := fnErrCode
end { GetErrorCode };


function  TGSkPingHostImpl.GetErrorMessage() : string;
begin
  Result := fsErrMsg
end { GetErrorMessage };


function  TGSkPingHostImpl.PingHost(const   sHost:  string;
                                    const   nTimeoutMS:  Cardinal)
                                    : Boolean;
const
  rSize = $400;

var
  pHost:  PHostEnt;
  pHostAddr:  PInAddr;
  hIcmp:  THandle;
  nStatus:  Cardinal;
  sSendBuf:  string;
  lReplyBuf:  array [0..rSize-1] of  Byte;

begin  { PingHost }
  fsErrMsg := '';
  fnErrCode := 0;
  try
    Startup();
    try
      pHost := GetHostByName(PAnsiChar(AnsiString(sHost)));
      if  pHost = nil  then
        RaiseLastOSError('GetHostByName');
      if  pHost.h_AddrType <> AF_INET  then
        RaiseError('GetHostByName', MaxInt,
                   '"%s" doesn''t resolve to an IPv4 address',
                   [sHost]);
      pHostAddr := PInAddr(pHost.h_addr^);
      hIcmp := IcmpCreateFile();
      if   hIcmp = INVALID_HANDLE_VALUE  then
        RaiseLastOSError('IcmpCreateFile');
      try
        sSendBuf := FormatDateTime('yyyymmddhhnnsszzz', Now());
        nStatus := IcmpSendEcho(hIcmp, pHostAddr^, PChar(sSendBuf), Length(sSendBuf),
                                nil, @lReplyBuf[0], rSize, nTimeoutMS);
        Result := (nStatus <> 0)
                  and (PEchoReply(@lReplyBuf[0]).Status = 0);
        if  not Result  then
          CheckStatus('IcmpSendEcho', PEchoReply(@lReplyBuf[0]).Status)
      finally
        IcmpCloseHandle(hIcmp)
      end { try-finally }
    finally
      Cleanup()
    end { try-finally }
  except
    on  eErrNo : EGSkPasLibErrorEx  do
      begin
        fsErrMsg := eErrNo.Message;
        fnErrCode := eErrNo.ErrorCode;
        Result := False
      end { PingHost };
    on  eErr : Exception  do
      begin
        fsErrMsg := eErr.Message;
        fnErrCode := MaxInt;
        Result := False
      end { on Exception }
  end { try-except }
end { PingHost };


procedure  TGSkPingHostImpl.RaiseError(const   sMethod:  string;
                                       const   nErrCode:  Integer;
                                       const   sErrInfo:  string);
begin
  raise  EGSkPasLibErrorEx.Create('PingHost', nErrCode,
                                  sMethod + ': ' + sErrInfo);
end { RaiseError };


procedure  TGSkPingHostImpl.RaiseError(const   sMethod:  string;
                                       const   nErrCode:  Integer;
                                       const   sErrFrmt:  string;
                                       const   ErrArgs:  array of const);
begin
  RaiseError(sMethod, nErrCode,
             Format(sErrFrmt, ErrArgs))
end { RaiseError };


procedure  TGSkPingHostImpl.RaiseLastOSError(const   sMethod:  string);

var
  nErr:  Cardinal;

begin  { RaiseLastOSError }
  nErr := GetLastError();
  RaiseError(sMethod, Integer(nErr), SysErrorMessage(nErr))
end { RaiseLastOSError };


procedure  TGSkPingHostImpl.Startup();
begin
  CheckStatus('WSAStartup',
              WSAStartup($0101, fWSAData))
end { Startup };

{$ENDREGION 'TPingImpl1'}


end.

