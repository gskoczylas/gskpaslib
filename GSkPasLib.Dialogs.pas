﻿{: @abstract Kilka różnych okien dialogowych.

   Moduł @name zawiera definicje kilku wygodnych podprogramów przewidzianych do
   wyświetlania okien dialogowych z komunikatami. }
unit  GSkPasLib.Dialogs;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$IF CompilerVersion > 22} // XE
  {$LEGACYIFEND ON}
{$IFEND}


uses
  {$IF CompilerVersion > 22} // XE
    WinAPI.Windows, System.SysUtils, System.StrUtils, System.Math, System.Classes,
    System.UITypes,
    {$IFDEF FMX}
      FMX.Forms,
    {$ELSE}
      Vcl.Controls, Vcl.Dialogs, Vcl.Graphics, Vcl.Forms, Vcl.StdCtrls, Vcl.ExtCtrls,
    {$ENDIF -FMX}
    Data.DB,
    {$IF not Declared(TCustomButton)}System.TypInfo,{$IFEND}
  {$ELSE}
    Windows, SysUtils,
    Controls, Dialogs, Graphics, Forms, StdCtrls, StrUtils,
    Math, Classes, DB, ExtCtrls,
    {$IF not Declared(TCustomButton)}TypInfo,{$IFEND}
  {$IFEND}
  {$IFDEF GetText}
    JvGnugettext,
  {$ENDIF GetText}
  {$IFNDEF FMX}
   JvDSADialogs, JvDynControlEngineJVCL,
  {$ENDIF -FMX}
  GSkPasLib.PasLib;


const
 {$IFNDEF FMX}
  {: @exclude }mtWarning      = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mtWarning;
  {: @exclude }mtError        = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mtError;
  {: @exclude }mtInformation  = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mtInformation;
  {: @exclude }mtConfirmation = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mtConfirmation;
  {: @exclude }mtCustom       = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mtCustom;

  {: @exclude }mbYes      = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbYes;
  {: @exclude }mbNo       = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbNo;
  {: @exclude }mbOK       = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbOK;
  {: @exclude }mbCancel   = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbCancel;
  {: @exclude }mbAbort    = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbAbort;
  {: @exclude }mbRetry    = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbRetry;
  {: @exclude }mbIgnore   = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbIgnore;
  {: @exclude }mbAll      = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbAll;
  {: @exclude }mbNoToAll  = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbNoToAll;
  {: @exclude }mbYesToAll = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbYesToAll;
  {: @exclude }mbHelp     = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbHelp;    {$IF CompilerVersion >= gcnCompilerVersionDelphiXE}
  {: @exclude }mbClose    = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbClose;   {$IFEND}
  {$IF CompilerVersion > gcnCompilerVersionDelphiAlexandria}
    {$MESSAGE HINT 'Sprawdź, czy nie trzeba uzupełnić powyższe stałe'}
  {$IFEND}

  {: @exclude }mbYesNoCancel       = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbYesNoCancel;
  {: @exclude }mbYesAllNoAllCancel = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbYesAllNoAllCancel;
  {: @exclude }mbOKCancel          = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbOKCancel;
  {: @exclude }mbAbortRetryIgnore  = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbAbortRetryIgnore;
  {: @exclude }mbAbortIgnore       = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.mbAbortIgnore;
  {: @exclude }mbYesNo             = [mbYes, mbNo];
  {: @exclude }mbAbortRetry        = [mbAbort, mbRetry];

  {: @exclude }mrNone     = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrNone;
  {: @exclude }mrOk       = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrOk;
  {: @exclude }mrCancel   = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrCancel;
  {: @exclude }mrAbort    = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrAbort;
  {: @exclude }mrRetry    = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrRetry;
  {: @exclude }mrIgnore   = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrIgnore;
  {: @exclude }mrYes      = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrYes;
  {: @exclude }mrNo       = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrNo;
  {: @exclude }mrAll      = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrAll;
  {: @exclude }mrNoToAll  = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrNoToAll;
  {: @exclude }mrYesToAll = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrYesToAll;   {$IF CompilerVersion >= gcnCompilerVersionDelphiXE}
  {: @exclude }mrClose    = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrClose;      {$IFEND}
  {$IF CompilerVersion > gcnCompilerVersionDelphiAlexandria}
    {$MESSAGE HINT 'Sprawdź, czy nie trzeba uzupełnić powyższe stałe'}
  {$IFEND}

  {: @exclude }mbDefault = JvDSADialogs.mbDefault;
 {$ENDIF -FMX}

  {: @exclude }MB_OK                = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_OK;
  {: @exclude }MB_OKCANCEL          = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_OKCANCEL;
  {: @exclude }MB_ABORTRETRYIGNORE  = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_ABORTRETRYIGNORE;
  {: @exclude }MB_YESNOCANCEL       = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_YESNOCANCEL;
  {: @exclude }MB_YESNO             = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_YESNO;
  {: @exclude }MB_RETRYCANCEL       = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_RETRYCANCEL;
  {: @exclude }MB_CANCELTRYCONTINUE = $00000006;
               { - }
  {: @exclude }MB_ICONINFORMATION   = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_ICONINFORMATION;
  {: @exclude }MB_ICONQUESTION      = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_ICONQUESTION;
  {: @exclude }MB_ICONWARNING       = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_ICONWARNING;
  {: @exclude }MB_ICONERROR         = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_ICONERROR;
               { - }
  {: @exclude }MB_DEFBUTTON1        = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_DEFBUTTON1;
  {: @exclude }MB_DEFBUTTON2        = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_DEFBUTTON2;
  {: @exclude }MB_DEFBUTTON3        = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_DEFBUTTON3;
  {: @exclude }MB_DEFBUTTON4        = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_DEFBUTTON4;
               { - }
  {: @exclude }MB_APPLMODAL         = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_APPLMODAL;
  {: @exclude }MB_SYSTEMMODAL       = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_SYSTEMMODAL;
  {: @exclude }MB_TASKMODAL         = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_TASKMODAL;
  {: @exclude }MB_HELP              = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_HELP;   // Help Button
               { - }
  {: @exclude }MB_NOFOCUS           = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_NOFOCUS;
  {: @exclude }MB_SETFOREGROUND     = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_SETFOREGROUND;
  {: @exclude }MB_DEFAULT_DESKTOP_ONLY = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_DEFAULT_DESKTOP_ONLY;
               { - }
  {: @exclude }MB_TOPMOST           = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_TOPMOST;
  {: @exclude }MB_SERVICE_NOTIFICATION = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_SERVICE_NOTIFICATION;
               { - }
  {: @exclude }MB_TYPEMASK          = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_TYPEMASK;
  {: @exclude }MB_ICONMASK          = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_ICONMASK;
  {: @exclude }MB_DEFMASK           = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_DEFMASK;
  {: @exclude }MB_MODEMASK          = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_MODEMASK;
  {: @exclude }MB_MISCMASK          = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.MB_MISCMASK;

  {: @exclude }IDOK        = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDOK;
  {: @exclude }ID_OK       = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.ID_OK;
  {: @exclude }IDCANCEL    = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDCANCEL;
  {: @exclude }ID_CANCEL   = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.ID_CANCEL;
  {: @exclude }IDABORT     = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDABORT;
  {: @exclude }ID_ABORT    = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.ID_ABORT;
  {: @exclude }IDRETRY     = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDRETRY;
  {: @exclude }ID_RETRY    = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.ID_RETRY;
  {: @exclude }IDIGNORE    = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDIGNORE;
  {: @exclude }ID_IGNORE   = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.ID_IGNORE;
  {: @exclude }IDYES       = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDYES;
  {: @exclude }ID_YES      = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.ID_YES;
  {: @exclude }IDNO        = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDNO;
  {: @exclude }ID_NO       = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.ID_NO;
  {: @exclude }IDCLOSE     = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDCLOSE;
  {: @exclude }ID_CLOSE    = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.ID_CLOSE;
  {: @exclude }IDHELP      = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDHELP;
  {: @exclude }ID_HELP     = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.ID_HELP;
  {: @exclude }IDTRYAGAIN  = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDTRYAGAIN;
  {: @exclude }ID_TRYAGAIN = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDTRYAGAIN;
  {: @exclude }IDCONTINUE  = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDCONTINUE;
  {: @exclude }ID_CONTINUE = {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.IDCONTINUE;

 {$IFNDEF FMX}
  {: abstract Minimalny czas wyświetlania komunikatów.

     Stała @name reprezentuje domyślny minimalny czas wyświetlania komunikatów,
     wyświetlanych przez funkcje @link(MsgDlg) oraz @link(MsgDlgEx).

     Domyślnie komunikaty wyświetlane są co najmniej przez 500 milisekund (pół
     sekundy). }
  gcnDefaultTimeMin = 500;

  { for InputText function }
  {: @exclude }ecNormal    = {$IF CompilerVersion > 22}VCL.{$IFEND}StdCtrls.ecNormal;
  {: @exclude }ecUpperCase = {$IF CompilerVersion > 22}VCL.{$IFEND}StdCtrls.ecUpperCase;
  {: @exclude }ecLowerCase = {$IF CompilerVersion > 22}VCL.{$IFEND}StdCtrls.ecLowerCase;

  {: @abstract Domyślny napis na klawiszu akceptacji funkcji @link(InputText). }
  gcsDefBtnAcceptCaption = 'OK';
  {: @abstract Domyślny napis na klawiszu rezygnacji funkcji @link(InputText). }
  {$IFNDEF GetText}
    gcsDefBtnCancelCaption = 'Anuluj';
  {$ELSE}
    gcsDefBtnCancelCaption = 'Cancel';
  {$ENDIF GetText}
  {: @abstract Domyślna długość pola edycji funkcji @link(InputText). }
  gcnDefMaxLength = 255;
 {$ENDIF -FMX}

{$IFNDEF FMX}

type
  {: @exclude }TMsgDlgType    = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.TMsgDlgType;
  {: @exclude }TMsgDlgBtn     = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.TMsgDlgBtn;
  {: @exclude }TMsgDlgButtons = {$IF CompilerVersion > 22}VCL.{$IFEND}Dialogs.TMsgDlgButtons;
  {: @exclude }TModalResult   = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.TModalResult;
  {-}
  {: @exclude }TMsgTimeFormatter = TJvDSATimeFormatter;

  {: @abstract Funkcja sprawdzająca poprawność danych.

     Funkcja typu @name może być parametrem wywołania funkcji @link(InputText).
     Funkcja ta będzie wykorzystana do sprawdzania poprawności danych.

     @param(sText Tekst wprowadzony przez użytkownika.)
     @param(bFinal Jeżeli parametr ma wartość @false, to dane są sprawdzane
                   w chwili, gdy kursor opuszcza pole, w którym dane są wpisywane
                   przez użytkownika. Natomiast jeżeli parametr ma wartość @true,
                   to jest to finalne sprawdzanie danych — gdy użytkownik próbuje
                   zamknąć okno dialogowe.)

     @returns(Wynik funkcji informuje, czy dane mogą zostać zaakceptowane.)

     @seeAlso(InputText)
     @seeAlso(TInputTextValidate.ValidInteger)
     @seeAlso(TInputTextValidate.ValidFloat)
     @seeAlso(TInputTextValidate.ValidDate) }
  TInputTextValidateFn = function(const   sText:  string;
                                  const   bFinal:  Boolean)
                                  : Boolean                          of object;

{: @abstract Metody do wykorzystania w wywołaniu funkcji @link(InputText).

   Metody z klasy @className mogą być użyte jako parametr „fnValidate” w wywołaniu
   funkcji @link(InputText).

   Przykład @longCode(#

     if  InputText('Caption', 'Prompt', sValue, True,
                   TInputTextValidate.ValidInteger)  then
       nValue := StrToInt(sValue)
     else
       RaiseError();
   #)

   @seeAlso(InputText)
   @seeAlso(TInputTextValidateFn) }
  TInputTextValidate = class abstract
  public
    class function  ValidInteger(const   sText:  string;
                                 const   bFinal:  Boolean)
                                 : Boolean;
    class function  ValidFloat(const   sText:  string;
                               const   bFinal:  Boolean)
                               : Boolean;
    class function  ValidDate(const   sText:  string;
                              const   bFinal:  Boolean)
                              : Boolean;
  end { TInputTextValidate };

{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Tytuł oraz ikona dialogu dobierane są automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sMsgFmt Format wiadomości zgodny ze standardową funkcją @code(Format).)
   @param(MsgArgs Parametry zgodne ze standardową funkcją @code(Format).)
   @param(mtMsgType Typ komunikatu.)
   @param(setMsgButtons Zestaw przycisków; domyślnie: [OK].)
   @param(ctlMsgCtrl Kontrolka, w stosunku do której odnosi się komunikat.
                     Jeżeli ten parametr nie jest @nil to komunikat będzie
                     wyświetlony pod, nad lub obok tej kontrolki - pod warunkiem,
                     że miejsce na ekranie pozwala na to.)
   @param(mbMsgDefault Który przycisk ma być przyciskiem domyślnym.)
   @param(mbMsgCancel Który przycisk ma reagować na naciśnięcie klawisza [Esc].)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlg(const   sMsgFmt:  string;
                 const   MsgArgs:  array of const;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons;
                 const   ctlMsgCtrl:  TControl;
                 const   mbMsgDefault:  TMsgDlgBtn = mbDefault;
                 const   mbMsgCancel:  TMsgDlgBtn = mbDefault;
                 const   nTimeOut:  Integer = 0;
                 const   nTimeMin:  Integer = gcnDefaultTimeMin;
                 const   fnOnCreate:  TNotifyEvent = nil;
                 const   fnOnDestroy:  TNotifyEvent = nil;
                 const   fnOnActivate: TNotifyEvent = nil)
                 : TModalResult;                                     overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Tytuł oraz ikona dialogu dobierane są automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sMsgFmt Format wiadomości zgodny ze standardową funkcją @code(Format).)
   @param(MsgArgs Parametry zgodne ze standardową funkcją @code(Format).)
   @param(mtMsgType Typ komunikatu.)
   @param(setMsgButtons Zestaw przycisków; domyślnie: [OK].)
   @param(nLeft Pozycja lewej krawędzi okna dialogowego. Jeżeli parametr ma
                wartość @code(-1) to okno zostanie umieszczone na środku ekranu
                (w poziomie).)
   @param(nTop Pozycja górnej krawędzi okna dialogowego. Jeżeli parametr ma
               wartość @code(-1) to okno zostanie umieszczone na środku ekranu
               (w pionie).)
   @param(mbMsgDefault Który przycisk ma być przyciskiem domyślnym.)
   @param(mbMsgCancel Który przycisk ma reagować na naciśnięcie klawisza [Esc].)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlg(const   sMsgFmt:  string;
                 const   MsgArgs:  array of const;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons = [mbOk];
                 const   nLeft:  Integer = -1;
                 const   nTop:  Integer = -1;
                 const   mbMsgDefault:  TMsgDlgBtn = mbDefault;
                 const   mbMsgCancel:  TMsgDlgBtn = mbDefault;
                 const   nTimeOut:  Integer = 0;
                 const   nTimeMin:  Integer = gcnDefaultTimeMin;
                 const   fnOnCreate:  TNotifyEvent = nil;
                 const   fnOnDestroy:  TNotifyEvent = nil;
                 const   fnOnActivate: TNotifyEvent = nil)
                 : TModalResult;                                     overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Tytuł oraz ikona dialogu dobierane są automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sMsg Treść wiadomości.)
   @param(mtMsgType Typ komunikatu.)
   @param(setMsgButtons Zestaw przycisków; domyślnie: [OK].)
   @param(ctlMsgCtrl Kontrolka, w stosunku do której odnosi się komunikat.
                     Jeżeli ten parametr nie jest @nil to komunikat będzie
                     wyświetlony pod, nad lub obok tej kontrolki - pod warunkiem,
                     że miejsce na ekranie pozwala na to.)
   @param(mbMsgDefault Który przycisk ma być przyciskiem domyślnym.)
   @param(mbMsgCancel Który przycisk ma reagować na naciśnięcie klawisza [Esc].)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlg(const   sMsg:  string;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons;
                 const   ctlMsgCtrl:  TControl;
                 const   mbMsgDefault:  TMsgDlgBtn = mbDefault;
                 const   mbMsgCancel:  TMsgDlgBtn = mbDefault;
                 const   nTimeOut:  Integer = 0;
                 const   nTimeMin:  Integer = gcnDefaultTimeMin;
                 const   fnOnCreate:  TNotifyEvent = nil;
                 const   fnOnDestroy:  TNotifyEvent = nil;
                 const   fnOnActivate: TNotifyEvent = nil)
                 : TModalResult;                                     overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Tytuł oraz ikona dialogu dobierane są automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sMsg Treść wiadomości.)
   @param(mtMsgType Typ komunikatu.)
   @param(setMsgButtons Zestaw przycisków; domyślnie: [OK].)
   @param(nLeft Pozycja lewej krawędzi okna dialogowego. Jeżeli parametr ma
                wartość @code(-1) to okno zostanie umieszczone na środku ekranu
                (w poziomie).)
   @param(nTop Pozycja górnej krawędzi okna dialogowego. Jeżeli parametr ma
               wartość @code(-1) to okno zostanie umieszczone na środku ekranu
               (w pionie).)
   @param(mbMsgDefault Który przycisk ma być przyciskiem domyślnym.)
   @param(mbMsgCancel Który przycisk ma reagować na naciśnięcie klawisza [Esc].)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlg(const   sMsg:  string;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons = [mbOk];
                 const   nLeft:  Integer = -1;
                 const   nTop:  Integer = -1;
                 const   mbMsgDefault:  TMsgDlgBtn = mbDefault;
                 const   mbMsgCancel:  TMsgDlgBtn = mbDefault;
                 const   nTimeOut:  Integer = 0;
                 const   nTimeMin:  Integer = gcnDefaultTimeMin;
                 const   fnOnCreate:  TNotifyEvent = nil;
                 const   fnOnDestroy:  TNotifyEvent = nil;
                 const   fnOnActivate: TNotifyEvent = nil)
                 : TModalResult;                                     overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Tytuł oraz ikona dialogu dobierane są automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sMsg Treść wiadomości.)
   @param(mtMsgType Typ komunikatu.)
   @param(setMsgButtons Zestaw przycisków; domyślnie: [OK].)
   @param(ctlMsgCtrl Kontrolka, w stosunku do której odnosi się komunikat.
                     Jeżeli ten parametr nie jest @nil to komunikat będzie
                     wyświetlony pod, nad lub obok tej kontrolki - pod warunkiem,
                     że miejsce na ekranie pozwala na to.)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlg(const   sMsg:  string;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons;
                 const   ctlMsgCtrl:  TControl;
                 const   nTimeOut:  Integer;
                 const   fnOnCreate:  TNotifyEvent = nil;
                 const   fnOnDestroy:  TNotifyEvent = nil;
                 const   fnOnActivate: TNotifyEvent = nil)
                 : TModalResult;                                     overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Tytuł oraz ikona dialogu dobierane są automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sMsg Treść wiadomości.)
   @param(mtMsgType Typ komunikatu.)
   @param(setMsgButtons Zestaw przycisków; domyślnie: [OK].)
   @param(nLeft Pozycja lewej krawędzi okna dialogowego. Jeżeli parametr ma
                wartość @code(-1) to okno zostanie umieszczone na środku ekranu
                (w poziomie).)
   @param(nTop Pozycja górnej krawędzi okna dialogowego. Jeżeli parametr ma
               wartość @code(-1) to okno zostanie umieszczone na środku ekranu
               (w pionie).)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlg(const   sMsg:  string;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons;
                 const   nLeft:  Integer;
                 const   nTop:  Integer;
                 const   nTimeOut:  Integer;
                 const   fnOnCreate:  TNotifyEvent = nil;
                 const   fnOnDestroy:  TNotifyEvent = nil;
                 const   fnOnActivate: TNotifyEvent = nil)
                 : TModalResult;                                     overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Tytuł oraz ikona dialogu dobierane są automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sMsg Treść wiadomości.)
   @param(mtMsgType Typ komunikatu.)
   @param(Buttons Zbiór przycisków. Jeżeli ten zbiór jest pusty, to będzie
                  wyświetlony przycisk z napisem „OK”.)
   @param(ctlMsgCtrl Kontrolka, w stosunku do której odnosi się komunikat.
                     Jeżeli ten parametr nie jest @nil to komunikat będzie
                     wyświetlony pod, nad lub obok tej kontrolki - pod warunkiem,
                     że miejsce na ekranie pozwala na to.)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(nDefaultBtn Który przycisk ma być przyciskiem domyślnym; wartość -1
                      oznacza brak takiego przycisku. Przyciski są numerowane od 1.)
   @param(nCancelBtn Który przycisk ma reagować na naciśnięcie klawisza [Esc];
                     wartość -1 oznacza brak takiego przycisku.)
   @param(nHelpBtn Który przycisk jest przyciskiem Pomocy.; wartość -1
                   oznacza brak takiego przycisku.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlgEx(const   sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons:  TMsgDlgButtons;
                   const   ctlMsgCtrl:  TControl = nil;
                   const   nTimeOut:  Integer = 0;
                   const   nTimeMin:  Integer = gcnDefaultTimeMin;
                   const   nDefaultBtn:  Integer = 0;
                   const   nCancelBtn:  Integer = 1;
                   const   nHelpBtn:  Integer = -1;
                   const   fnOnCreate:  TNotifyEvent = nil;
                   const   fnOnDestroy:  TNotifyEvent = nil;
                   const   fnOnActivate: TNotifyEvent = nil)
                   : TModalResult;                                   overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Tytuł oraz ikona dialogu dobierane są automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sCaption Tytuł okna z komunikatem.)
   @param(sMsg Treść wiadomości.)
   @param(mtMsgType Typ komunikatu.)
   @param(Buttons Zbiór przycisków. Jeżeli ten zbiór jest pusty, to będzie
                  wyświetlony przycisk z napisem „OK”.)
   @param(ctlMsgCtrl Kontrolka, w stosunku do której odnosi się komunikat.
                     Jeżeli ten parametr nie jest @nil to komunikat będzie
                     wyświetlony pod, nad lub obok tej kontrolki - pod warunkiem,
                     że miejsce na ekranie pozwala na to.)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(nDefaultBtn Który przycisk ma być przyciskiem domyślnym; wartość -1
                      oznacza brak takiego przycisku. Przyciski są numerowane od 1.)
   @param(nCancelBtn Który przycisk ma reagować na naciśnięcie klawisza [Esc];
                     wartość -1 oznacza brak takiego przycisku.)
   @param(nHelpBtn Który przycisk jest przyciskiem Pomocy.; wartość -1
                   oznacza brak takiego przycisku.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlgEx(const   sCaption:  string;
                   const   sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons:  TMsgDlgButtons;
                   const   ctlMsgCtrl:  TControl = nil;
                   const   nTimeOut:  Integer = 0;
                   const   nTimeMin:  Integer = gcnDefaultTimeMin;
                   const   nDefaultBtn:  Integer = 0;
                   const   nCancelBtn:  Integer = 1;
                   const   nHelpBtn:  Integer = -1;
                   const   fnOnCreate:  TNotifyEvent = nil;
                   const   fnOnDestroy:  TNotifyEvent = nil;
                   const   fnOnActivate: TNotifyEvent = nil)
                   : TModalResult;                                   overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Tytuł oraz ikona dialogu dobierane są automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sMsg Treść wiadomości.)
   @param(mtMsgType Typ komunikatu.)
   @param(Buttons Tablica napisów na przyciski. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Results).)
   @param(Results Tablica wartości zwracanych po wybraniu przez użytkownika
                  jednego z przycisków. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Buttons).)
   @param(ctlMsgCtrl Kontrolka, w stosunku do której odnosi się komunikat.
                     Jeżeli ten parametr nie jest @nil to komunikat będzie
                     wyświetlony pod, nad lub obok tej kontrolki - pod warunkiem,
                     że miejsce na ekranie pozwala na to.)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(nDefaultBtn Który przycisk ma być przyciskiem domyślnym; wartość -1
                      oznacza brak takiego przycisku. Przyciski są numerowane od 1.)
   @param(nCancelBtn Który przycisk ma reagować na naciśnięcie klawisza [Esc];
                     wartość -1 oznacza brak takiego przycisku.)
   @param(nHelpBtn Który przycisk jest przyciskiem Pomocy.; wartość -1
                   oznacza brak takiego przycisku.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlgEx(const   sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   ctlMsgCtrl:  TControl;
                   const   nTimeOut:  Integer = 0;
                   const   nTimeMin:  Integer = gcnDefaultTimeMin;
                   const   nDefaultBtn:  Integer = 0;
                   const   nCancelBtn:  Integer = 1;
                   const   nHelpBtn:  Integer = -1;
                   const   fnOnCreate:  TNotifyEvent = nil;
                   const   fnOnDestroy:  TNotifyEvent = nil;
                   const   fnOnActivate: TNotifyEvent = nil)
                   : TModalResult;                                   overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Tytuł oraz ikona dialogu dobierane są automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sMsg Treść wiadomości.)
   @param(mtMsgType Typ komunikatu.)
   @param(Buttons Tablica napisów na przyciski. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Results).)
   @param(Results Tablica wartości zwracanych po wybraniu przez użytkownika
                  jednego z przycisków. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Buttons).)
   @param(nLeft Pozycja lewej krawędzi okna dialogowego. Jeżeli parametr ma
                wartość @code(-1) to okno zostanie umieszczone na środku ekranu
                (w poziomie).)
   @param(nTop Pozycja górnej krawędzi okna dialogowego. Jeżeli parametr ma
               wartość @code(-1) to okno zostanie umieszczone na środku ekranu
               (w pionie).)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(nDefaultBtn Który przycisk ma być przyciskiem domyślnym; wartość -1
                      oznacza brak takiego przycisku. Przyciski są numerowane od 1.)
   @param(nCancelBtn Który przycisk ma reagować na naciśnięcie klawisza [Esc];
                     wartość -1 oznacza brak takiego przycisku.)
   @param(nHelpBtn Który przycisk jest przyciskiem Pomocy.; wartość -1
                   oznacza brak takiego przycisku.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlgEx(const   sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   nLeft:  Integer = -1;
                   const   nTop:  Integer = -1;
                   const   nTimeOut:  Integer = 0;
                   const   nTimeMin:  Integer = gcnDefaultTimeMin;
                   const   nDefaultBtn:  Integer = 0;
                   const   nCancelBtn:  Integer = 1;
                   const   nHelpBtn:  Integer = -1;
                   const   fnOnCreate:  TNotifyEvent = nil;
                   const   fnOnDestroy:  TNotifyEvent = nil;
                   const   fnOnActivate: TNotifyEvent = nil)
                   : TModalResult;                                   overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Ikona dialogu dobierana jest automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sCaption Tytuł okna z komunikatem.)
   @param(sMsg Treść wiadomości.)
   @param(mtMsgType Typ komunikatu.)
   @param(Buttons Tablica napisów na przyciski. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Results).)
   @param(Results Tablica wartości zwracanych po wybraniu przez użytkownika
                  jednego z przycisków. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Buttons).)
   @param(ctlMsgCtrl Kontrolka, w stosunku do której odnosi się komunikat.
                     Jeżeli ten parametr nie jest @nil to komunikat będzie
                     wyświetlony pod, nad lub obok tej kontrolki - pod warunkiem,
                     że miejsce na ekranie pozwala na to.)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(nDefaultBtn Który przycisk ma być przyciskiem domyślnym; wartość -1
                      oznacza brak takiego przycisku. Przyciski są numerowane od 1.)
   @param(nCancelBtn Który przycisk ma reagować na naciśnięcie klawisza [Esc];
                     wartość -1 oznacza brak takiego przycisku.)
   @param(nHelpBtn Który przycisk jest przyciskiem Pomocy.; wartość -1
                   oznacza brak takiego przycisku.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlgEx(const   sCaption, sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   ctlMsgCtrl:  TControl;
                   const   nTimeOut:  Integer = 0;
                   const   nTimeMin:  Integer = gcnDefaultTimeMin;
                   const   nDefaultBtn:  Integer = 0;
                   const   nCancelBtn:   Integer = 1;
                   const   nHelpBtn:  Integer = -1;
                   const   fnOnCreate:  TNotifyEvent = nil;
                   const   fnOnDestroy:  TNotifyEvent = nil;
                   const   fnOnActivate: TNotifyEvent = nil)
                   : TModalResult;                                   overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   Ikona dialogu dobierana jest automatycznie na podstawie parametru
   @code(mtMsgType).

   @param(sCaption Tytuł okna z komunikatem.)
   @param(sMsg Treść wiadomości.)
   @param(mtMsgType Typ komunikatu.)
   @param(Buttons Tablica napisów na przyciski. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Results).)
   @param(Results Tablica wartości zwracanych po wybraniu przez użytkownika
                  jednego z przycisków. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Buttons).)
   @param(nLeft Pozycja lewej krawędzi okna dialogowego. Jeżeli parametr ma
                wartość @code(-1) to okno zostanie umieszczone na środku ekranu
                (w poziomie).)
   @param(nTop Pozycja górnej krawędzi okna dialogowego. Jeżeli parametr ma
               wartość @code(-1) to okno zostanie umieszczone na środku ekranu
               (w pionie).)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(nDefaultBtn Który przycisk ma być przyciskiem domyślnym; wartość -1
                      oznacza brak takiego przycisku. Przyciski są numerowane od 1.)
   @param(nCancelBtn Który przycisk ma reagować na naciśnięcie klawisza [Esc];
                     wartość -1 oznacza brak takiego przycisku.)
   @param(nHelpBtn Który przycisk jest przyciskiem Pomocy.; wartość -1
                   oznacza brak takiego przycisku.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlgEx(const   sCaption, sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   nLeft:  Integer = -1;
                   const   nTop:  Integer = -1;
                   const   nTimeOut:  Integer = 0;
                   const   nTimeMin:  Integer = gcnDefaultTimeMin;
                   const   nDefaultBtn:  Integer = 0;
                   const   nCancelBtn:   Integer = 1;
                   const   nHelpBtn:  Integer = -1;
                   const   fnOnCreate:  TNotifyEvent = nil;
                   const   fnOnDestroy:  TNotifyEvent = nil;
                   const   fnOnActivate: TNotifyEvent = nil)
                   : TModalResult;                                   overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   @param(sCaption Tytuł okna z komunikatem.)
   @param(sMsg Treść wiadomości.)
   @param(Picture Ikona komunikatu (może być @nil).)
   @param(Buttons Tablica napisów na przyciski. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Results).)
   @param(Results Tablica wartości zwracanych po wybraniu przez użytkownika
                  jednego z przycisków. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Buttons).)
   @param(ctlMsgCtrl Kontrolka, w stosunku do której odnosi się komunikat.
                     Jeżeli ten parametr nie jest @nil to komunikat będzie
                     wyświetlony pod, nad lub obok tej kontrolki - pod warunkiem,
                     że miejsce na ekranie pozwala na to.)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(nDefaultBtn Który przycisk ma być przyciskiem domyślnym; wartość -1
                      oznacza brak takiego przycisku. Przyciski są numerowane od 1.)
   @param(nCancelBtn Który przycisk ma reagować na naciśnięcie klawisza [Esc];
                     wartość -1 oznacza brak takiego przycisku.)
   @param(nHelpBtn Który przycisk jest przyciskiem Pomocy.; wartość -1
                   oznacza brak takiego przycisku.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlgEx(const   sCaption, sMsg:  string;
                   const   Picture:  TGraphic;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   ctlMsgCtrl:  TControl;
                   const   nTimeOut:  Integer = 0;
                   const   nTimeMin:  Integer = gcnDefaultTimeMin;
                   const   nDefaultBtn:  Integer = 0;
                   const   nCancelBtn:   Integer = 1;
                   const   nHelpBtn:  Integer = -1;
                   const   fnOnCreate:  TNotifyEvent = nil;
                   const   fnOnDestroy:  TNotifyEvent = nil;
                   const   fnOnActivate: TNotifyEvent = nil)
                   : TModalResult;                                   overload;


{: @abstract Wyświetla komunikat dla użytkownika.

   Funkcja ma działanie bardzo podobne do standardowej funkcji @code(MessageDlg).
   Ma kilka dodatkowych parametrów umożliwiających lepsze dostosowanie działania
   funkcji do potrzeb.

   @param(sCaption Tytuł okna z komunikatem.)
   @param(sMsg Treść wiadomości.)
   @param(Picture Ikona komunikatu (może być @nil).)
   @param(Buttons Tablica napisów na przyciski. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Results).)
   @param(Results Tablica wartości zwracanych po wybraniu przez użytkownika
                  jednego z przycisków. Rozmiar tej tablicy musi być
                  taki sam jak rozmiar tablicy w parametrze @code(Buttons).)
   @param(nLeft Pozycja lewej krawędzi okna dialogowego. Jeżeli parametr ma
                wartość @code(-1) to okno zostanie umieszczone na środku ekranu
                (w poziomie).)
   @param(nTop Pozycja górnej krawędzi okna dialogowego. Jeżeli parametr ma
               wartość @code(-1) to okno zostanie umieszczone na środku ekranu
               (w pionie).)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie zamknięte.
                   Wynikiem funkcji będzie wartość odpowiadająca przyciskowi
                   domyślnemu.)
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.)
   @param(nDefaultBtn Który przycisk ma być przyciskiem domyślnym; wartość -1
                      oznacza brak takiego przycisku. Przyciski są numerowane od 1.)
   @param(nCancelBtn Który przycisk ma reagować na naciśnięcie klawisza [Esc];
                     wartość -1 oznacza brak takiego przycisku.)
   @param(nHelpBtn Który przycisk jest przyciskiem Pomocy.; wartość -1
                   oznacza brak takiego przycisku.)
   @param(fnOnCreate Pozwala wskazać metodę, która będzie wywołana po utworzeniu
                     okna dialogowego (przed jego wyświetleniem). Parametrem
                     metody jest utworzone okno - obiekt typu @code(TForm).)
   @param(fnOnDestroy Pozwala wskazać metodę, która będzie wywołana po zamknięciu
                      okna dialogowego (tuż przed jego zwolnieniem). Parametrem
                      metody jest zwalniane okno - obiekt typu @code(TForm).)
   @param(fnOnActivate Pozwala wskazać metodę, która będzie wywołana po aktywacji
                       okna dialogowego. Parametrem metody jest aktywowane okno
                       - obiekt typu @code(TForm).)

   @returns(Wynikiem funkcji jest wartość odpowiadająca przyciskowi wybranemu
            przez użytkownika. (Zobacz też opis parametru @code(nTimeout))) }
function  MsgDlgEx(const   sCaption, sMsg:  string;
                   const   Picture:  TGraphic;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   nLeft:  Integer = -1;
                   const   nTop:  Integer = -1;
                   const   nTimeOut:  Integer = 0;
                   const   nTimeMin:  Integer = gcnDefaultTimeMin;
                   const   nDefaultBtn:  Integer = 0;
                   const   nCancelBtn:   Integer = 1;
                   const   nHelpBtn:  Integer = -1;
                   const   fnOnCreate:  TNotifyEvent = nil;
                   const   fnOnDestroy:  TNotifyEvent = nil;
                   const   fnOnActivate: TNotifyEvent = nil)
                   : TModalResult;                                   overload;


{: @abstract Wyświetla komunikat błędu.

   Jest to specjalizowana wersja funkcji @link(MsgDlg) przewidziana do
   wyświetlania komunikatów błędu.

   Jeżeli parametr @code(bAbort) ma wartość @true i parametr @code(ctlErrCtrl)
   ma wskazuje obiekt typu @code(TWinControl) to po wyświetleniu komunikatu
   błędu procedura spróbuje umieścić kursor w kontrolce wskazanej przez parametr
   @code(ctlErrCtrl). Następnie zostanie wywołana standardowa procedura
   @code(Abort).

   @param(sErrMsg Komunikat błędu.)
   @param(ctlErrCtrl Kontrolka, w stosunku do której sygnalizowany jest błąd.
                     Musi to być albo obiekt typu @code(TControl), albo typu
                     @code(TField), albo @nil. @br
                     Jeżeli ten parametr jest typu @code(TControl) to komunikat
                     będzie wyświetlony pod, nad lub obok tej kontrolki - pod
                     warunkiem, że miejsce na ekranie pozwala na to.)
   @param(bAbort Czy po wyświetleniu komunikatu wywołać standardową procedurę
                 @code(Abort).)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie
                   zamknięte. )
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.) }
procedure  ErrDlg(const   sErrMsg:  string;
                  const   ctlErrCtrl:  TComponent = nil;
                  const   bAbort:  Boolean = True;
                  const   nTimeOut:  Integer = 0;
                  const   nTimeMin:  Integer = gcnDefaultTimeMin);   overload;


{: @abstract Wyświetla komunikat błędu.

   Jest to specjalizowana wersja funkcji @link(MsgDlg) przewidziana do
   wyświetlania komunikatów błędu.

   Jeżeli parametr @code(bAbort) ma wartość @true i parametr @code(ctlErrCtrl)
   ma wskazuje obiekt typu @code(TWinControl) to po wyświetleniu komunikatu
   błędu procedura spróbuje umieścić kursor w kontrolce wskazanej przez parametr
   @code(ctlErrCtrl). Następnie zostanie wywołana standardowa procedura
   @code(Abort).

   @param(sErrFmt Komunikat błędu w postaci formatu zgodnego ze standardową
                  funkcją @code(Format).)
   @param(Args Parametry komunikatu błędu zgodne ze standardową funkcją
               @code(Format).)
   @param(ctlErrCtrl Kontrolka, w stosunku do której sygnalizowany jest błąd.
                     Musi to być albo obiekt typu @code(TControl), albo typu
                     @code(TField), albo @nil. @br
                     Jeżeli ten parametr jest typu @code(TControl) to komunikat
                     będzie wyświetlony pod, nad lub obok tej kontrolki - pod
                     warunkiem, że miejsce na ekranie pozwala na to.)
   @param(bAbort Czy po wyświetleniu komunikatu wywołać standardową procedurę
                 @code(Abort).)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie
                   zamknięte. )
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.) }
procedure  ErrDlg(const   sErrFmt:  string;
                  const   Args:  array of const;
                  const   ctlErrCtrl:  TComponent = nil;
                  const   bAbort:  Boolean = True;
                  const   nTimeOut:  Integer = 0;
                  const   nTimeMin:  Integer = gcnDefaultTimeMin);   overload;


{: @abstract Wyświetla komunikat błędu.

   Jest to specjalizowana wersja funkcji @link(MsgDlg) przewidziana do
   wyświetlania komunikatów błędu.

   @param(sErrMsg Komunikat błędu.)
   @param(bAbort Czy po wyświetleniu komunikatu wywołać standardową procedurę
                 @code(Abort).)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie
                   zamknięte. )
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.) }
procedure  ErrDlg(const   sErrMsg:  string;
                  const   bAbort:  Boolean;
                  const   nTimeOut:  Integer = 0;
                  const   nTimeMin:  Integer = gcnDefaultTimeMin);   overload;


{: @abstract Wyświetla komunikat błędu.

   Jest to specjalizowana wersja funkcji @link(MsgDlg) przewidziana do
   wyświetlania komunikatów błędu.

   @param(sErrFmt Komunikat błędu w postaci formatu zgodnego ze standardową
                  funkcją @code(Format).)
   @param(Args Parametry komunikatu błędu zgodne ze standardową funkcją
               @code(Format).)
   @param(bAbort Czy po wyświetleniu komunikatu wywołać standardową procedurę
                 @code(Abort).)
   @param(nTimeout Jak długo ma być wyświetlany komunikat. Ten czas podany jest
                   w @bold(sekundach). Jeżeli jest różny od zera to po upływie
                   wskazanej liczby sekund okno zostanie automatycznie
                   zamknięte. )
   @param(nTimeMin Minimalny czas wyświetlania komunikatu. Ten czas podany jest
                   w @bold(milisekundach). Jeżeli jest różny od zera to przez
                   wskazaną liczbę milisekund okna dialogowego nie da się zamknąć.
                   Mechanizm ten ma za zadanie zapobiegać przypadkowemu (zbyt
                   szybkiemu) zamknięciu okna z komunikatem.) }
procedure  ErrDlg(const   sErrFmt:  string;
                  const   Args:  array of const;
                  const   bAbort:  Boolean;
                  const   nTimeOut:  Integer = 0;
                  const   nTimeMin:  Integer = gcnDefaultTimeMin);   overload;
{$ENDIF -FMX}

{: @abstract Wyświetla komunikat przy pomocy systemowej funkcji @code(MessageBox).

   @param(sInfo Treść komunikatu.)
   @param(sCaption Tytuł okna z komunikatem. @br
                   Jeżeli parametr jest pustym napisem to stosowany jest
                   tytuł aplikacji pobierany przy pomocy funkcji
                   @link(GetAppTitle).)
   @param(nFlags Flagi definiujące przyciski, ikonę itp. @br
                 Do wskazanych flag zawsze automatycznie dodawane są stałe
                 @code(MB_TASKMODAL or MB_SETFOREGROUND or MB_TOPMOST).)

   @returns(Wynik funkcji jest identyczny jak wynik standardowej systemowej
            funkcji @code(MessageBox).) }
function  MsgBox(const   sInfo:  string;
                 const   sCaption:  string = '';
                 const   nFlags:  LongWord = 0)
                 : Integer;                                          overload;


{: @abstract Wyświetla komunikat przy pomocy systemowej funkcji @code(MessageBox).

   @param(sMsgFmt Format wiadomości zgodny ze standardową funkcją @code(Format).)
   @param(MsgArgs Parametry zgodne ze standardową funkcją @code(Format).)
   @param(sCaption Tytuł okna z komunikatem. @br
                   Jeżeli parametr jest pustym napisem to stosowany jest
                   tytuł aplikacji pobierany przy pomocy funkcji
                   @link(GetAppTitle).)
   @param(nFlags Flagi definiujące przyciski, ikonę itp. @br
                 Do wskazanych flag zawsze automatycznie dodawane są stałe
                 @code(MB_TASKMODAL or MB_SETFOREGROUND or MB_TOPMOST).)

   @returns(Wynik funkcji jest identyczny jak wynik standardowej systemowej
            funkcji @code(MessageBox).) }
function  MsgBox(const   sMsgFmt:  string;
                 const   Args:  array of const;
                 const   sCaption:  string = '';
                 const   nFlags:  LongWord = 0)
                 : Integer;                                          overload;


{: @abstract Wyświetla komunikat przy pomocy systemowej funkcji @code(MessageBox).

   Jako tytuł okna z komunikatem stosowany jest tytuł aplikacji pobierany przy
   pomocy funkcji @link(GetAppTitle).

   @param(sInfo Treść komunikatu.)
   @param(nFlags Flagi definiujące przyciski, ikonę itp. @br
                 Do wskazanych flag zawsze automatycznie dodawane są stałe
                 @code(MB_TASKMODAL or MB_SETFOREGROUND or MB_TOPMOST).)

   @returns(Wynik funkcji jest identyczny jak wynik standardowej systemowej
            funkcji @code(MessageBox).) }
function  MsgBox(const   sInfo:  string;
                 const   nFlags:  LongWord)
                 : Integer;                                          overload;


{: @abstract Wyświetla komunikat przy pomocy systemowej funkcji @code(MessageBox).

   Jako tytuł okna z komunikatem stosowany jest tytuł aplikacji pobierany przy
   pomocy funkcji @link(GetAppTitle).)

   @param(sMsgFmt Format wiadomości zgodny ze standardową funkcją @code(Format).)
   @param(MsgArgs Parametry zgodne ze standardową funkcją @code(Format).)
   @param(nFlags Flagi definiujące przyciski, ikonę itp. @br
                 Do wskazanych flag zawsze automatycznie dodawane są stałe
                 @code(MB_TASKMODAL or MB_SETFOREGROUND or MB_TOPMOST).)

   @returns(Wynik funkcji jest identyczny jak wynik standardowej systemowej
            funkcji @code(MessageBox).) }
function  MsgBox(const   sMsgFmt:  string;
                 const   Args:  array of const;
                 const   nFlags:  LongWord)
                 : Integer;                                          overload;

{: @abstract Wyświetla komunikat błędu.

   Wywołanie procedury @code(ErrBox('Komunikat błędu')) jest równoważne wywołaniu
   funkcji @code(MsgBox('Komunikat błędu', MB_ICONERROR or MB_OK)).

   @seeAlso(MsgBox) }
procedure  ErrBox(const   sInfo:  string;
                  const   bAbort:  Boolean = False);                 overload;

{: @abstract Wyświetla komunikat błędu.

   Wywołanie procedury @code(ErrBox(Format, Parametry)) jest równoważne wywołaniu
   funkcji @code(MsgBox(Format, Parametry, MB_ICONERROR or MB_OK)).

   @seeAlso(MsgBox) }
procedure  ErrBox(const   sInfoFmt:  string;
                  const   InfoArgs:  array of const;
                  const   bAbort:  Boolean = False);                 overload;


{$IFNDEF FMX}

{: @abstract Wyświetla okno dialogowe zawierające pytanie o dane.

   Funkcja @name wyświetla okno dialogowe z jednym polem edycyjnym, w które
   użytkownik może wpisać wymagane dane.

   @param(sCaption Tytuł okna.)
   @param(sPrompt Etykieta danych (tekst przed polem edycyjnym).)
   @param(sValue Wywołując funkcję ten parametr zawiera wartość domyślną danych.
                 Po wywołaniu funkcji ten parametr zawiera dane wpisane przez
                 użytkownika.)
   @param(bAllowEmpty Czy pozwolić, aby użytkownik nie wpisał żadnych danych.)
   @param(CharCase Czy automatycznie zamieniać litery wpisane przez użytkownika
                   na ich małe lub wielkie odpowiedniki.)
   @param(fnValidate Funkcja sprawdzająca poprawność danych.)
   @param(nMaxLength Maksymalna długość pola danych (liczba znaków).
                     Jeżeli parametr ma wartość zero, to przyjmowana jest
                     @link(gcnDefMaxLength długość domyślna).)
   @param(sBtnAcceptCaption Tekst wyświetlany na klawiszu akceptacji danych.
                            Jeżeli parametr jest pustym napisem, to wyświetlany
                            jest @link(gcsDefBtnAcceptCaption domyślny tekst).)
   @param(sBtnCancelCaption Tekst wyświetlany na klawiszu rezygnacji.
                            Jeżeli parametr jest pustym napisem, to wyświetlany
                            jest @link(gcsDefBtnCancelCaption domyślny tekst).)

   @returns(Wynik funkcji informuje, czy dane zostały zaakceptowane. Wartość @true
            oznacza, że parametr @code(sValue) zawiera dane spełniające warunki
            zdefiniowane przez pozostałe parametry funkcji.)

   @seeAlso(TInputTextValidateFn)
   @seeAlso(TInputTextValidate) }
function  InputText(const   sCaption, sPrompt:  string;
                    var     sValue:  string;
                    const   bAllowEmpty:  Boolean = False;
                    const   CharCase:  TEditCharCase = ecNormal;
                    const   fnValidate:  TInputTextValidateFn = nil;
                    const   nMaxLength:  Integer = gcnDefMaxLength;
                   {const}  sBtnAcceptCaption:  string = '';
                   {const}  sBtnCancelCaption:  string = '')
                    : Boolean;                                       overload;

function  InputText(const   sCaption, sPrompt:  string;
                    var     sValue:  string;
                    const   bAllowEmpty:  Boolean;
                    const   CharCase:  TEditCharCase;
                    const   fnValidate:  TFunc<string,Boolean,Boolean>;
                    const   nMaxLength:  Integer = gcnDefMaxLength;
                   {const}  sBtnAcceptCaption:  string = '';
                   {const}  sBtnCancelCaption:  string = '')
                    : Boolean;                                       overload;

{: @abstract Wyświetla okno dialogowe zawierające pytanie o dane.

   Funkcja @name wyświetla okno dialogowe z jednym polem edycyjnym, w które
   użytkownik może wpisać wymagane dane.

   @param(sCaption Tytuł okna.)
   @param(sPrompt Etykieta danych (tekst przed polem edycyjnym).)
   @param(sValue Wywołując funkcję ten parametr zawiera wartość domyślną danych.
                 Po wywołaniu funkcji ten parametr zawiera dane wpisane przez
                 użytkownika.)
   @param(bAllowEmpty Czy pozwolić, aby użytkownik nie wpisał żadnych danych.)
   @param(fnValidate Funkcja sprawdzająca poprawność danych.)

   @returns(Wynik funkcji informuje, czy dane zostały zaakceptowane. Wartość @true
            oznacza, że parametr @code(sValue) zawiera dane spełniające warunki
            zdefiniowane przez pozostałe parametry funkcji.)

   @seeAlso(TInputTextValidateFn) }
function  InputText(const   sCaption, sPrompt:  string;
                    var     sValue:  string;
                    const   bAllowEmpty:  Boolean;
                    const   fnValidate:  TInputTextValidateFn)
                    : Boolean;                                       overload;


function  GetStdMsgTimeFormatter() : TMsgTimeFormatter;

procedure  SetMsgTimeFormatter(const   fnTimeFormatter:  TMsgTimeFormatter);

{$ENDIF -FMX}


implementation


uses
 {$IFNDEF GetText}
  GSkPasLib.StrUtils,
 {$ENDIF -GetText}
  GSkPasLib.SystemUtils, GSkPasLib.AppUtils, GSkPasLib.ComponentUtils;


const
 {$IFNDEF FMX}
  gcModalResults:  array[TMsgDlgBtn] of Integer =
     (mrYes, mrNo, mrOk, mrCancel, mrAbort, mrRetry, mrIgnore, mrAll, mrNoToAll,
      mrYesToAll, 0 {$IF CompilerVersion >= gcnCompilerVersionDelphiXE}, mrClose {$IFEND});
 {$ENDIF -FMX}
  gcIconIDs:  array[TMsgDlgType] of PChar =
      (IDI_EXCLAMATION, IDI_HAND, IDI_ASTERISK, IDI_QUESTION, nil);


{$IFNDEF FMX}

var
  gButtonCaptions:  array[TMsgDlgBtn] of string;
  gCaptions:  array[TMsgDlgType] of string;


type
  TControlArea = record
    lPos:  TPoint;
    nWidth, nHeight:  Integer
  end { TControlArea };

  THolder = class(TObject)
  private
    fnTimeMin:   Integer;
    fOnActivate: TNotifyEvent;
  public
    class procedure  MsgDlgActivate(Sender:  TObject);
    {-}
    property  TimeMin:  Integer
              read  fnTimeMin
              write fnTimeMin;
    property  OnActivate:  TNotifyEvent
              read  fOnActivate
              write fOnActivate;
  end { THolder };

  TInputTextForm = class(TForm)
  private
    var
      fbAllowEmpty:  Boolean;
      fedtValue:     TEdit;
      fbtnOK:        TButton;
      ffnCheckValid: TFunc<string, Boolean, Boolean>;
    function  GetEditText() : string;
    procedure  OnEditChange(Sender:  TObject);
    procedure  BtnAcceptClick(Sender:  TObject);
    procedure  FormShow(Sender:  TObject);
    procedure  FormClose(Sender:  TObject;
                         var   Action:  TCloseAction);
  protected
    procedure  Activate();                                           override;
  public
    constructor  Create(const   sCaption, sPrompt, sDefault,
                                sBtnOkCaption, sBtnCancelCaption:  string;
                        const   CharCase:  TEditCharCase;
                        const   nMaxLength:  Integer);               reintroduce;
    {-}
    property  AllowEmpty:  Boolean
              read  fbAllowEmpty
              write fbAllowEmpty;
    property  CheckValid:  TFunc<string, Boolean, Boolean>
              read  ffnCheckValid
              write ffnCheckValid;
    property  EditText:  string
              read  GetEditText;
  end { TInputTextForm };

{$ENDIF -FMX}


{$REGION 'Local subroutines'}

{$IFNDEF FMX}

procedure  TimerCallBack(hWnd:    HWND;
                         Msg:     Word;
                         TimerID: Word;
                         SysTime: LongInt);                          stdcall;
  forward;


function  ControlArea(const   Ctrl:  TControl)
                      : TControlArea;                                overload;
begin
  if  Ctrl <> nil  then
    begin
      SetControlPage(Ctrl);
      if  Ctrl is TWinControl  then
        try
          (Ctrl as TWinControl).SetFocus()
        except
          { ignore }
        end { try-except };
      Result.lPos := Ctrl.ClientOrigin;
      Result.nWidth := Ctrl.Width;
      Result.nHeight := Ctrl.Height
    end { Ctrl <> nil }
  else
    FillChar(Result, SizeOf(Result), 0)
end { ControlArea };


function  ControlArea(const   nLeft, nTop:  Integer)
                      : TControlArea;                                overload;
begin
  Result.lPos.X := nLeft;
  Result.lPos.Y := nTop;
  Result.nWidth := 0;
  Result.nHeight := 0
end  { ControlArea };


procedure  TimerCallBack(hWnd:    HWND;
                         Msg:     Word;
                         TimerID: Word;
                         SysTime: LongInt);                          stdcall;
type
  TButtonType = {$IF Declared(TCustomButton)}TCustomButton{$else}TButtonControl{$IFEND};

var
  fDialog:  TForm;
  nInd:  Integer;
  btnFirst:  TButtonType;
  lCmp:  TComponent;
  bDefault:  Boolean;

begin  { TimerCallBack }
  fDialog := FindControl(hWnd) as TDSAMessageForm;
  Assert(fDialog <> nil);
  with  fDialog  do
    begin
      CheckOSError(KillTimer(hWnd, TimerID));
      btnFirst := nil;
      for  nInd := ComponentCount - 1  downto  0  do
        begin
          lCmp := Components[nInd];
          if  lCmp is TButtonType  then
            with  TButtonType(lCmp)  do
              begin
                if  btnFirst = nil  then
                  btnFirst := TButtonType(lCmp);
                Enabled := True;
                bDefault := {$IF Declared(TCustomButton)}
                              Default
                            {$ELSE}
                              GetPropValue(lCmp, 'Default', False)
                            {$IFEND};
                if  bDefault
                    or (Tag = 1)  then
                  begin
                    {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.SetFocus(Handle);
                    btnFirst := nil
                  end { if Default }
              end { with }
        end { for nInd };
      if  btnFirst <> nil  then
        {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.SetFocus(btnFirst.Handle)
    end {  with fDialog }
end { TimerCallBack };


function  InternalCreateMsgDlg(const   sCaption:     string;
                               const   sMessage:     string;
                               const   Picture:      TGraphic;
                               const   Buttons:      array of string;
                               const   Results:      array of Integer{TModalResult};
                               const   nHelpCtx:     Integer;
                               const   sChkCaption:  string;
                               const   DlgCenter:    TDlgCenterKind;
                               const   nTimeout:     Integer;
                               const   nDefaultBtn:  Integer;
                               const   nCancelBtn:   Integer;
                               const   nHelpBtn:     Integer;
                               const   ctrlArea:     TControlArea)
                               : TDSAMessageForm;
var
  lScr:  TRect;
 {$IFDEF DEBUG}
  nInd:  Integer;
 {$ENDIF DEBUG}

begin  { InternalCreateMsgDlg }
  {$IFDEF DEBUG}
    for  nInd := High(Results)  downto  Low(Results)  do
      if  Results[nInd] = mrNone  then
        begin
          MsgBox('Nie używaj „mrNone”{0} jako wartości wynikowej!',
                 'Błędny parametr',
                 MB_ICONERROR or MB_OK);
          Break
        end { if; for };
  {$ENDIF DEBUG}
  Result := CreateDSAMessageForm(sCaption, sMessage, Picture, Buttons, Results,
                                 nHelpCtx, sChkCaption, DlgCenter, nTimeout,
                                 nDefaultBtn, nCancelBtn, nHelpBtn);
  lScr := Screen.WorkAreaRect;
  if  (ctrlArea.lPos.X <> 0)
      or (ctrlArea.lPos.Y <> 0)
      or (ctrlArea.nWidth <> 0)
      or (ctrlArea.nHeight <> 0)  then
    if  (ctrlArea.nWidth = 0)
        and (ctrlArea.nHeight = 0)  then
      begin
        Result.Position := poDesigned;
        if  ctrlArea.lPos.X >= 0  then
          begin
            Result.Left := ctrlArea.lPos.X;
            if  Result.Left + Result.Width > lScr.Right  then
              Result.Left := Max(lScr.Right - Result.Width, 0)
          end { ctrlArea.lPos.X >= 0 };
        if  ctrlArea.lPos.Y >= 0  then
          begin
            Result.Top := ctrlArea.lPos.Y;
            if  Result.Top + Result.Height > lScr.Bottom  then
              Result.Top := Max(lScr.Bottom - Result.Height, 0)
          end { ctrlArea.lPos.Y >= 0 }
      end
    else
      begin
        Result.Left := ctrlArea.lPos.X;
        if  Result.Left + Result.Width > lScr.Right  then
          begin
            Result.Left := lScr.Right - Result.Width;
            if  Result.Left < lScr.Left  then
              Result.Left := lScr.Left
          end { Left };
        Result.Top := ctrlArea.lPos.Y + ctrlArea.nHeight;
        if  Result.Top + Result.Height > lScr.Bottom  then
          begin
            { pod kontrolką nie ma miejsca - spróbuję nad kontrolką }
            Result.Top := ctrlArea.lPos.Y - Result.Height;
            if  Result.Top < lScr.Top  then
              begin
                { nad kontrolką też nie ma miejsca - spróbuję obok }
                Result.Top := ctrlArea.lPos.Y;
                { Sprawdzę z prawej strony kontrolki }
                Result.Left := ctrlArea.lPos.X + ctrlArea.nWidth;
                if  Result.Left + Result.Width > lScr.Right  then
                  begin
                    { z prawej strony nie ma miejsca - spróbuję z lewej }
                    Result.Left := ctrlArea.lPos.X - Result.Width;
                    if  Result.Left < lScr.Left  then
                      { nie mieści się ani z dołu, ani z góry,
                        ani z lewej, ani z prawej strony - ustawiam na środku }
                      Result.Position := poScreenCenter
                  end
              end
          end
      end { if [Area] <> [0,0,0,0] };
end { InternalCreateMsgDlg };


function  InternalMsgDlg(const   sCaption:     string;
                         const   sMessage:     string;
                         const   Picture:      TGraphic;
                         const   Buttons:      array of string;
                         const   Results:      array of Integer{TModalResult};
                         const   nHelpCtx:     Integer;
                         const   sChkCaption:  string;
                         const   DlgCenter:    TDlgCenterKind;
                         const   nTimeout:     Integer;
                         const   nTimeMin:     Integer;
                         const   DefaultBtn:   Integer;
                         const   CancelBtn:    Integer;
                         const   HelpBtn:      Integer;
                         const   ctrlArea:     TControlArea;
                         const   fnOnCreate:   TNotifyEvent;
                         const   fnOnDestroy:  TNotifyEvent;
                         const   fnOnActivate: TNotifyEvent)
                         : TModalResult;
var
  bTmr:  Boolean;
  fDlg:  TDSAMessageForm;

begin  { InternalMsgDlg }
  fDlg := InternalCreateMsgDlg(sCaption, sMessage, Picture, Buttons, Results,
                               nHelpCtx, sChkCaption, DlgCenter, nTimeout,
                               DefaultBtn, CancelBtn, HelpBtn, ctrlArea);
  with  fDlg  do
    try
      Assert(Tag = 0);
      Tag := Integer(THolder.Create());
      Assert(@OnActivate = nil);
      OnActivate := THolder.MsgDlgActivate;
      bTmr := (nTimeMin > 0)
           and ((nTimeMin div 1000) <= nTimeout);
      if  bTmr  then
        THolder(Tag).TimeMin := nTimeMin;
      if  Assigned(fnOnCreate)  then
        fnOnCreate(fDlg);
      THolder(Tag).OnActivate := fnOnActivate;
      Result := ShowModal();
      if  bTmr  then
        KillTimer(Handle, 0);
      if  Assigned(fnOnDestroy)  then
        fnOnDestroy(fDlg)
    finally
      THolder(Tag).Free();
      Free()
    end { try-finally }
end { InternalMsgDlg };


function  DSATimeFormatter(nSecs:  Integer)
                           : string;
var
  nMins:  Integer;

begin { DSATimeFormatter }
  nMins := nSecs div 60;
  nSecs := nSecs mod 60;
  {$IFDEF GetText}
    if  nMins <> 0  then
      Result := Format(dgettext('GSkPasLib', 'This dialog is closing in %0:d %1:s and %2:d %3:s.'),
                       [nMins, dngettext('GSkPasLibEx', 'minute', 'minutes', nMins),
                        nSecs, dngettext('GSkPasLibEx', 'second', 'seconds', nSecs)])
    else
      Result := Format(dgettext('GSkPasLib', 'This dialog is closing in %0:d %1:s.'),
                       [nSecs, dngettext('GSkPasLibEx', 'second', 'seconds', nSecs)])
  {$ELSE}
    if  nMins <> 0  then
      Result := Format('%d %s i ',
                       [nMins,
                        Odmiana(nMins, 'minutę', 'minuty', 'minut')])
    else
      Result := '';
    Result := Format('Ten komunikat zostanie zamknięty za %s%d %s.',
                     [Result,
                      nSecs,
                      Odmiana(nSecs, 'sekundę', 'sekundy', 'sekund')])
  {$ENDIF -GetText}
end { DSATimeFormatter };

{$ENDIF -FMX}

{$ENDREGION 'Local subroutines'}


{$REGION 'Global subroutines'}

{$IFNDEF FMX}

function  GetStdMsgTimeFormatter() : TMsgTimeFormatter;
begin
  Result := DSATimeFormatter
end { GetStdMsgTimeFormatter };


procedure  SetMsgTimeFormatter(const   fnTimeFormatter:  TMsgTimeFormatter);
begin
  SetDSATimeFormatter(fnTimeFormatter)
end { SetMsgTimeFormatter };

{$ENDIF -FMX}

{$ENDREGION 'Global subroutines'}


{$REGION 'ErrDlg'}

{$IFNDEF FMX}

procedure  ErrDlg(const   sErrMsg:  string;
                  const   ctlErrCtrl:  TComponent;
                  const   bAbort:  Boolean;
                  const   nTimeOut:  Integer;
                  const   nTimeMin:  Integer);
var
  lCtrl:  TControl;

begin  { ErrDlg }
  if  ctlErrCtrl is TControl  then
    lCtrl := ctlErrCtrl as TControl
  else
    lCtrl := nil;
  MsgDlg(sErrMsg, mtError, [mbOk], lCtrl, mbOk, mbOk, nTimeOut, nTimeMin);
  if  ctlErrCtrl <> nil  then
    try
      if  ctlErrCtrl is TWinControl  then
        (ctlErrCtrl as TWinControl).SetFocus()
      else if  ctlErrCtrl is TField  then
        (ctlErrCtrl as TField).FocusControl()
    except
      { OK }
    end { try-except; if ctlErrCtrl <> nil };
  if  bAbort  then
    Abort()
end { ErrDlg };


procedure  ErrDlg(const   sErrFmt:  string;
                  const   Args:  array of const;
                  const   ctlErrCtrl:  TComponent;
                  const   bAbort:  Boolean;
                  const   nTimeOut:  Integer;
                  const   nTimeMin:  Integer);
begin
  ErrDlg(Format(sErrFmt, Args),
         ctlErrCtrl, bAbort, nTimeOut, nTimeMin)
end { ErrDlg };


procedure  ErrDlg(const   sErrMsg:  string;
                  const   bAbort:  Boolean;
                  const   nTimeOut:  Integer;
                  const   nTimeMin:  Integer);
begin
  ErrDlg(sErrMsg, nil, bAbort, nTimeOut, nTimeMin)
end { ErrDlg };


procedure  ErrDlg(const   sErrFmt:  string;
                  const   Args:  array of const;
                  const   bAbort:  Boolean;
                  const   nTimeOut:  Integer;
                  const   nTimeMin:  Integer);
begin
  ErrDlg(Format(sErrFmt, Args),
         bAbort, nTimeOut, nTimeMin)
end { ErrDlg };

{$ENDIF -FMX}

{$ENDREGION 'ErrDlg'}


{$REGION 'MsgDlg'}

{$IFNDEF FMX}

function  MsgDlg(const   sMsgFmt:  string;
                 const   MsgArgs:  array of const;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons;
                 const   ctlMsgCtrl:  TControl;
                 const   mbMsgDefault:  TMsgDlgBtn;
                 const   mbMsgCancel:  TMsgDlgBtn;
                 const   nTimeOut:  Integer;
                 const   nTimeMin:  Integer;
                 const   fnOnCreate:  TNotifyEvent;
                 const   fnOnDestroy:  TNotifyEvent;
                 const   fnOnActivate: TNotifyEvent)
                 : TModalResult;
begin
  Result := MsgDlg(Format(sMsgFmt, MsgArgs),
                   mtMsgType, setMsgButtons, ctlMsgCtrl,
                   mbMsgDefault, mbMsgCancel, nTimeOut, nTimeMin,
                   fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlg };


function  MsgDlg(const   sMsgFmt:  string;
                 const   MsgArgs:  array of const;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons;
                 const   nLeft:  Integer;
                 const   nTop:  Integer;
                 const   mbMsgDefault:  TMsgDlgBtn;
                 const   mbMsgCancel:  TMsgDlgBtn;
                 const   nTimeOut:  Integer;
                 const   nTimeMin:  Integer;
                 const   fnOnCreate:  TNotifyEvent;
                 const   fnOnDestroy:  TNotifyEvent;
                 const   fnOnActivate: TNotifyEvent)
                 : TModalResult;
begin
  Result := MsgDlg(Format(sMsgFmt, MsgArgs),
                   mtMsgType, setMsgButtons, nLeft, nTop,
                   mbMsgDefault, mbMsgCancel, nTimeOut, nTimeMin,
                   fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlg };


function  MsgDlg(const   sMsg:  string;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons;
                 const   ctlMsgCtrl:  TControl;
                 const   nTimeOut:  Integer;
                 const   fnOnCreate:  TNotifyEvent;
                 const   fnOnDestroy:  TNotifyEvent;
                 const   fnOnActivate: TNotifyEvent)
                 : TModalResult;
begin
  Result := MsgDlg(sMsg, mtMsgType, setMsgButtons, ctlMsgCtrl,
                   mbDefault, mbDefault, nTimeOut, gcnDefaultTimeMin,
                   fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlg };


function  MsgDlg(const   sMsg:  string;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons;
                 const   nLeft:  Integer;
                 const   nTop:  Integer;
                 const   nTimeOut:  Integer;
                 const   fnOnCreate:  TNotifyEvent;
                 const   fnOnDestroy:  TNotifyEvent;
                 const   fnOnActivate: TNotifyEvent)
                 : TModalResult;
begin
  Result := MsgDlg(sMsg, mtMsgType, setMsgButtons, nLeft, nTop,
                   mbDefault, mbCancel, nTimeOut, gcnDefaultTimeMin,
                   fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlg };


function  MsgDlg(const   sMsg:  string;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons;
                 const   ctlMsgCtrl:  TControl;
                 const   mbMsgDefault:  TMsgDlgBtn;
                 const   mbMsgCancel:  TMsgDlgBtn;
                 const   nTimeOut:  Integer;
                 const   nTimeMin:  Integer;
                 const   fnOnCreate:  TNotifyEvent;
                 const   fnOnDestroy:  TNotifyEvent;
                 const   fnOnActivate: TNotifyEvent)
                 : TModalResult;
var
  lButtons:  array of  string;
  lResults:  array of  Integer;
  nInd, nCnt:  Integer;
  nDefaultBtn, nCancelBtn, nHelpBtn:  Integer;
  lInd:  TMsgDlgBtn;
  lDefaultBtn, lCancelBtn:  TMsgDlgBtn;

begin  { MsgDlg }
  if  mbMsgDefault = mbDefault  then
    if  mbOK in setMsgButtons  then
      lDefaultBtn := mbOK
    else if  mbYes in setMsgButtons  then
      lDefaultBtn := mbYes
    else
      lDefaultBtn := mbRetry
  else
    lDefaultBtn := mbMsgDefault;
  if  mbMsgCancel = mbDefault  then
    if  mbCancel in setMsgButtons  then
      lCancelBtn := mbCancel
    else if  mbNo in setMsgButtons  then
      lCancelBtn := mbNo
    else
      lCancelBtn := mbOK
  else
    lCancelBtn := mbMsgCancel;
  nCnt := 0;
  for  lInd := High(TMsgDlgBtn)  downto  Low(TMsgDlgBtn)  do
    if  lInd in setMsgButtons  then
      Inc(nCnt);
  SetLength(lButtons, nCnt);
  SetLength(lResults, nCnt);
  nDefaultBtn := -1;
  nCancelBtn := -1;
  nHelpBtn := -1;
  nInd := 0;
  for  lInd := Low(TMsgDlgBtn)  to  High(TMsgDlgBtn)  do
    if  lInd in setMsgButtons  then
      begin
        lButtons[nInd] := gButtonCaptions[lInd];
        lResults[nInd] := gcModalResults[lInd];
        if  lInd = lDefaultBtn  then
          nDefaultBtn := nInd;
        if  lInd = lCancelBtn  then
          nCancelBtn := nInd;
        if  lInd = mbHelp  then
          nHelpBtn := nInd;
        Inc(nInd)
      end { if; for };
  if  nDefaultBtn = -1  then
    nDefaultBtn := Low(lButtons);
  if  nCancelBtn = -1  then
    nCancelBtn := Min(1, High(lButtons));
  Result := MsgDlgEx(sMsg, mtMsgType, lButtons, lResults, ctlMsgCtrl,
                     nTimeOut, nTimeMin, nDefaultBtn, nCancelBtn, nHelpBtn,
                     fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlg };


function  MsgDlg(const   sMsg:  string;
                 const   mtMsgType:  TMsgDlgType;
                 const   setMsgButtons:  TMsgDlgButtons;
                 const   nLeft:  Integer;
                 const   nTop:  Integer;
                 const   mbMsgDefault:  TMsgDlgBtn;
                 const   mbMsgCancel:  TMsgDlgBtn;
                 const   nTimeOut:  Integer;
                 const   nTimeMin:  Integer;
                 const   fnOnCreate:  TNotifyEvent;
                 const   fnOnDestroy:  TNotifyEvent;
                 const   fnOnActivate: TNotifyEvent)
                 : TModalResult;
var
  lButtons:  array of  string;
  lResults:  array of  Integer;
  nInd, nCnt:  Integer;
  nDefaultBtn, nCancelBtn, nHelpBtn:  Integer;
  lInd:  TMsgDlgBtn;
  lDefaultBtn, lCancelBtn:  TMsgDlgBtn;

begin  { MsgDlg }
  if  mbMsgDefault = mbDefault  then
    if  mbOK in setMsgButtons  then
      lDefaultBtn := mbOK
    else if  mbYes in setMsgButtons  then
      lDefaultBtn := mbYes
    else
      lDefaultBtn := mbRetry
  else
    lDefaultBtn := mbMsgDefault;
  if  mbMsgCancel = mbDefault  then
    if  mbCancel in setMsgButtons  then
      lCancelBtn := mbCancel
    else if  mbNo in setMsgButtons  then
      lCancelBtn := mbNo
    else
      lCancelBtn := mbOK
  else
    lCancelBtn := mbMsgCancel;
  nCnt := 0;
  for  lInd := High(TMsgDlgBtn)  downto  Low(TMsgDlgBtn)  do
    if  lInd in setMsgButtons  then
      Inc(nCnt);
  SetLength(lButtons, nCnt);
  SetLength(lResults, nCnt);
  nDefaultBtn := -1;
  nCancelBtn := -1;
  nHelpBtn := -1;
  nInd := 0;
  for  lInd := Low(TMsgDlgBtn)  to  High(TMsgDlgBtn)  do
    if  lInd in setMsgButtons  then
      begin
        lButtons[nInd] := gButtonCaptions[lInd];
        lResults[nInd] := gcModalResults[lInd];
        if  lInd = lDefaultBtn  then
          nDefaultBtn := nInd;
        if  lInd = lCancelBtn  then
          nCancelBtn := nInd;
        if  lInd = mbHelp  then
          nHelpBtn := nInd;
        Inc(nInd)
      end { if; for };
  if  nDefaultBtn = -1  then
    nDefaultBtn := Low(lButtons);
  if  nCancelBtn = -1  then
    nCancelBtn := Min(1, High(lButtons));
  Result := MsgDlgEx(sMsg, mtMsgType, lButtons, lResults, nLeft, nTop,
                     nTimeOut, nTimeMin, nDefaultBtn, nCancelBtn, nHelpBtn,
                     fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlg };

{$ENDIF -FMX}

{$ENDREGION 'MsgDlg'}


{$REGION 'MsgDlgEx'}

{$IFNDEF FMX}

function  MsgDlgEx(const   sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons:  TMsgDlgButtons;
                   const   ctlMsgCtrl:  TControl;
                   const   nTimeOut:  Integer;
                   const   nTimeMin:  Integer;
                   const   nDefaultBtn:  Integer;
                   const   nCancelBtn:  Integer;
                   const   nHelpBtn:  Integer;
                   const   fnOnCreate:  TNotifyEvent;
                   const   fnOnDestroy:  TNotifyEvent;
                   const   fnOnActivate: TNotifyEvent)
                   : TModalResult;
begin
  Result := MsgDlgEx(gCaptions[mtMsgType], sMsg, mtMsgType, Buttons,
                     ctlMsgCtrl, nTimeOut, nTimeMin, nDefaultBtn, nCancelBtn, nHelpBtn,
                     fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlgEx };


function  MsgDlgEx(const   sCaption:  string;
                   const   sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons:  TMsgDlgButtons;
                   const   ctlMsgCtrl:  TControl = nil;
                   const   nTimeOut:  Integer = 0;
                   const   nTimeMin:  Integer = gcnDefaultTimeMin;
                   const   nDefaultBtn:  Integer = 0;
                   const   nCancelBtn:  Integer = 1;
                   const   nHelpBtn:  Integer = -1;
                   const   fnOnCreate:  TNotifyEvent = nil;
                   const   fnOnDestroy:  TNotifyEvent = nil;
                   const   fnOnActivate: TNotifyEvent = nil)
                   : TModalResult;
const
  cBtnRslt:  array[TMsgDlgBtn] of  TModalResult = (
                mrYes, mrNo, mrOK, mrCancel, mrAbort, mrRetry, mrIgnore, mrAll,
                mrNoToAll, mrYesToAll, -1, mrClose);
var
  lInd:  TMsgDlgBtn;
  lBtns: TArray<string>;
  lRslt: TArray<Integer>;

begin  { MsgDlg }
  if  Buttons <> []  then
    begin
      SetLength(lBtns, 0);
      SetLength(lRslt, 0);
      for  lInd :=  Low(TMsgDlgBtn)  to  High(TMsgDlgBtn)  do
        if  lInd in Buttons  then
          begin
            SetLength(lBtns, Length(lBtns) + 1);
            SetLength(lRslt, Length(lRslt) + 1);
            lBtns[High(lBtns)] := gButtonCaptions[lInd];
            lRslt[High(lRslt)] := cBtnRslt[lInd]
          end { if; for lInd }
    end { MsgBtns <> [] }
  else
    begin
      SetLength(lBtns, 1);
      SetLength(lRslt, 1);
      lBtns[Low(lBtns)] := gButtonCaptions[mbOK];
      lRslt[Low(lRslt)] := mrOk
    end { MsgBtns = [] };
  Result := MsgDlgEx(sCaption, sMsg, mtMsgType, lBtns, lRslt,
                     ctlMsgCtrl, nTimeOut, nTimeMin, nDefaultBtn, nCancelBtn, nHelpBtn,
                     fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlgEx };



function  MsgDlgEx(const   sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   ctlMsgCtrl:  TControl;
                   const   nTimeOut:  Integer;
                   const   nTimeMin:  Integer;
                   const   nDefaultBtn:  Integer;
                   const   nCancelBtn:   Integer;
                   const   nHelpBtn:  Integer;
                   const   fnOnCreate:  TNotifyEvent;
                   const   fnOnDestroy:  TNotifyEvent;
                   const   fnOnActivate: TNotifyEvent)
                   : TModalResult;
begin
  Result := MsgDlgEx(gCaptions[mtMsgType], sMsg, mtMsgType, Buttons, Results,
                     ctlMsgCtrl, nTimeOut, nTimeMin,
                     nDefaultBtn, nCancelBtn, nHelpBtn,
                     fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlgEx };


function  MsgDlgEx(const   sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   nLeft:  Integer;
                   const   nTop:  Integer;
                   const   nTimeOut:  Integer;
                   const   nTimeMin:  Integer;
                   const   nDefaultBtn:  Integer;
                   const   nCancelBtn:  Integer;
                   const   nHelpBtn:  Integer;
                   const   fnOnCreate:  TNotifyEvent;
                   const   fnOnDestroy:  TNotifyEvent;
                   const   fnOnActivate: TNotifyEvent)
                   : TModalResult;
begin
  Result := MsgDlgEx(gCaptions[mtMsgType], sMsg, mtMsgType, Buttons, Results,
                     nLeft, nTop, nTimeOut, nTimeMin,
                     nDefaultBtn, nCancelBtn, nHelpBtn,
                     fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlgEx };


function  MsgDlgEx(const   sCaption, sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   ctlMsgCtrl:  TControl;
                   const   nTimeOut:  Integer;
                   const   nTimeMin:  Integer;
                   const   nDefaultBtn:  Integer;
                   const   nCancelBtn:   Integer;
                   const   nHelpBtn:  Integer;
                   const   fnOnCreate:  TNotifyEvent;
                   const   fnOnDestroy:  TNotifyEvent;
                   const   fnOnActivate: TNotifyEvent)
                   : TModalResult;
var
  lPict:  TGraphic;

begin  { MsgDlgEx }
  if  gcIconIDs[mtMsgType] <> nil  then
    begin
      lPict := TIcon.Create();
      try
        TIcon(lPict).Handle := LoadIcon(0, gcIconIDs[mtMsgType])
      except
        lPict.Free();
        raise
      end { try-except }
    end { if }
  else
    lPict := nil;
  try
    case  mtMsgType  of
      mtWarning:
        MessageBeep(MB_ICONWARNING);
      mtError:
        MessageBeep(MB_ICONERROR);
      mtInformation:
        MessageBeep(MB_ICONINFORMATION);
      mtConfirmation:
        MessageBeep(MB_ICONQUESTION);
      mtCustom:
        MessageBeep(MB_OK)
    end { case mtMsgType };
    Result := MsgDlgEx(sCaption, sMsg, lPict, Buttons, Results, ctlMsgCtrl,
                       nTimeOut, nTimeMin, nDefaultBtn, nCancelBtn, nHelpBtn,
                       fnOnCreate, fnOnDestroy, fnOnActivate)
  finally
    if  lPict <> nil  then
      lPict.Free()
  end { try-finally }
end { MsgDlgEx };


function  MsgDlgEx(const   sCaption, sMsg:  string;
                   const   mtMsgType:  TMsgDlgType;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   nLeft:  Integer;
                   const   nTop:  Integer;
                   const   nTimeOut:  Integer;
                   const   nTimeMin:  Integer;
                   const   nDefaultBtn:  Integer;
                   const   nCancelBtn:   Integer;
                   const   nHelpBtn:  Integer;
                   const   fnOnCreate:  TNotifyEvent;
                   const   fnOnDestroy:  TNotifyEvent;
                   const   fnOnActivate: TNotifyEvent)
                   : TModalResult;
var
  lPict:  TGraphic;

begin  { MsgDlg }
  if  gcIconIDs[mtMsgType] <> nil  then
    begin
      lPict := TIcon.Create();
      try
        TIcon(lPict).Handle := LoadIcon(0, gcIconIDs[mtMsgType])
      except
        lPict.Free();
        raise
      end { try-except }
    end { if }
  else
    lPict := nil;
  try
    case  mtMsgType  of
      mtWarning:
        MessageBeep(MB_ICONWARNING);
      mtError:
        MessageBeep(MB_ICONERROR);
      mtInformation:
        MessageBeep(MB_ICONINFORMATION);
      mtConfirmation:
        MessageBeep(MB_ICONQUESTION);
      mtCustom:
        MessageBeep(MB_OK)
    end { case mtMsgType };
    Result := MsgDlgEx(sCaption, sMsg, lPict, Buttons, Results, nLeft, nTop,
                       nTimeOut, nTimeMin, nDefaultBtn, nCancelBtn, nHelpBtn,
                       fnOnCreate, fnOnDestroy, fnOnActivate)
  finally
    if  lPict <> nil  then
      lPict.Free()
  end { try-finally }
end { MsgDlgEx };


function  MsgDlgEx(const   sCaption, sMsg:  string;
                   const   Picture:  TGraphic;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   ctlMsgCtrl:  TControl;
                   const   nTimeOut:  Integer;
                   const   nTimeMin:  Integer;
                   const   nDefaultBtn:  Integer;
                   const   nCancelBtn:   Integer;
                   const   nHelpBtn:  Integer;
                   const   fnOnCreate:  TNotifyEvent;
                   const   fnOnDestroy:  TNotifyEvent;
                   const   fnOnActivate: TNotifyEvent)
                   : TModalResult;
begin
  Result := InternalMsgDlg(sCaption, sMsg, Picture, Buttons, Results,
                           0, '', dckScreen,
                           nTimeOut, nTimeMin, nDefaultBtn, nCancelBtn, nHelpBtn,
                           ControlArea(ctlMsgCtrl),
                           fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlgEx };


function  MsgDlgEx(const   sCaption, sMsg:  string;
                   const   Picture:  TGraphic;
                   const   Buttons: array of string;
                   const   Results: array of Integer;
                   const   nLeft:  Integer;
                   const   nTop:  Integer;
                   const   nTimeOut:  Integer;
                   const   nTimeMin:  Integer;
                   const   nDefaultBtn:  Integer;
                   const   nCancelBtn:   Integer;
                   const   nHelpBtn:  Integer;
                   const   fnOnCreate:  TNotifyEvent;
                   const   fnOnDestroy:  TNotifyEvent;
                   const   fnOnActivate: TNotifyEvent)
                   : TModalResult;
begin
  Result := InternalMsgDlg(sCaption, sMsg, Picture, Buttons, Results,
                           0, '', dckScreen,
                           nTimeOut, nTimeMin, nDefaultBtn, nCancelBtn, nHelpBtn,
                           ControlArea(nLeft, nTop),
                           fnOnCreate, fnOnDestroy, fnOnActivate)
end { MsgDlgEx };

{$ENDIF -FMX}

{$ENDREGION 'MsgDlgEx'}


{$REGION 'MsgBox'}

function  MsgBox(const   sInfo:  string;
                 const   sCaption:  string;
                 const   nFlags:  LongWord)
                 : Integer;
begin
  {$IFDEF FMX}
    Result := MessageBox(0, PChar(sInfo),
                         PChar(IfThen(sCaption <> '',
                                      sCaption,
                                      GetAppTitle())),
                         nFlags
                           or MB_TASKMODAL
                           or MB_SETFOREGROUND
                           or MB_TOPMOST)
  {$ELSE}
    Application.NormalizeAllTopMosts();
    try
      Result := Application.MessageBox(PChar(sInfo),
                                       PChar(IfThen(sCaption <> '',
                                                    sCaption,
                                                    GetAppTitle())),
                                       nFlags
                                         or MB_TASKMODAL
                                         or MB_SETFOREGROUND
                                         or MB_TOPMOST)
    finally
      Application.RestoreTopMosts()
    end { try-finally }
  {$ENDIF -FMX}
end { MsgBox };


function  MsgBox(const   sMsgFmt:  string;
                 const   Args:  array of const;
                 const   sCaption:  string;
                 const   nFlags:  LongWord)
                 : Integer;
begin
  Result := MsgBox(Format(sMsgFmt, Args), sCaption, nFlags)
end { MsgBox };


function  MsgBox(const   sInfo:  string;
                 const   nFlags:  LongWord)
                 : Integer;
begin
  Result := MsgBox(sInfo, '', nFlags)
end { MsgBox };


function  MsgBox(const   sMsgFmt:  string;
                 const   Args:  array of const;
                 const   nFlags:  LongWord)
                 : Integer;
begin
  Result := MsgBox(Format(sMsgFmt, Args), nFlags)
end { MsgBox };

{$ENDREGION 'MsgDlgEx'}


{$REGION 'ErrBox'}

procedure  ErrBox(const   sInfo:  string;
                  const   bAbort:  Boolean);
begin
  MsgBox(sInfo, MB_ICONERROR or MB_OK);
  if  bAbort  then
    Abort()
end { ErrBox };


procedure  ErrBox(const   sInfoFmt:  string;
                  const   InfoArgs:  array of const;
                  const   bAbort:  Boolean);
begin
  MsgBox(sInfoFmt, InfoArgs, MB_ICONERROR or MB_OK);
  if  bAbort  then
    Abort()
end { ErrBox };

{$ENDREGION 'ErrBox'}


{$REGION 'InputText'}

{$IFNDEF FMX}

function  InputText(const   sCaption, sPrompt:  string;
                    var     sValue:  string;
                    const   bAllowEmpty:  Boolean;
                    const   CharCase:  TEditCharCase;
                    const   fnValidate:  TFunc<string,Boolean,Boolean>;
                    const   nMaxLength:  Integer;
                   {const}  sBtnAcceptCaption:  string;
                   {const}  sBtnCancelCaption:  string)
                    : Boolean;
var
  hForegroundWindow:  HWND;

begin  { InputText }
  if  sBtnAcceptCaption = ''  then
    sBtnAcceptCaption := dgettext('GSkPasLib', gcsDefBtnAcceptCaption);
  if  sBtnCancelCaption = ''  then
    sBtnCancelCaption := dgettext('GSkPasLib', gcsDefBtnCancelCaption);
  hForegroundWindow := GetForegroundWindow();
  try
    with  TInputTextForm.Create(sCaption, sPrompt, sValue,
                                sBtnAcceptCaption, sBtnCancelCaption,
                                CharCase, nMaxLength)  do
      try
        AllowEmpty := fbAllowEmpty;
        CheckValid := fnValidate;
        Result := ShowModal() = mrOk;
        if  Result  then
          sValue := EditText
      finally
        Free()
      end { try-finally }
  finally
    SetForegroundWindow(hForegroundWindow);
    BringWindowToTop(hForegroundWindow)
  end { try-finally }
end { InputText };


function  InputText(const   sCaption, sPrompt:  string;
                    var     sValue:  string;
                    const   bAllowEmpty:  Boolean;
                    const   CharCase:  TEditCharCase;
                    const   fnValidate:  TInputTextValidateFn;
                    const   nMaxLength:  Integer;
                   {const}  sBtnAcceptCaption, sBtnCancelCaption:  string)
                    : Boolean;
begin
  Result := InputText(sCaption, sPrompt, sValue, bAllowEmpty, CharCase,
                      function(sText:  string;
                               bFinal:  Boolean)
                               : Boolean
                        begin
                          if  Assigned(fnValidate)  then
                            Result := fnValidate(sText, bFinal)
                          else
                            Result := True
                        end,
                      nMaxLength, sBtnAcceptCaption, sBtnCancelCaption)

end { InputText };


function  InputText(const   sCaption, sPrompt:  string;
                    var     sValue:  string;
                    const   bAllowEmpty:  Boolean;
                    const   fnValidate:  TInputTextValidateFn)
                    : Boolean;
begin
  Result := InputText(sCaption, sPrompt, sValue, bAllowEmpty, ecNormal, fnValidate, gcnDefMaxLength,
                      dgettext('GSkPasLib', gcsDefBtnAcceptCaption),
                      dgettext('GSkPasLib', gcsDefBtnCancelCaption))
end { InputText };

{$ENDIF -FMX}

{$ENDREGION 'InputText'}


{$REGION 'THolder'}

{$IFNDEF FMX}

class procedure  THolder.MsgDlgActivate(Sender:  TObject);

type
  TButtonType = {$IF Declared(TCustomButton)}TCustomButton{$ELSE}TButtonControl{$IFEND};

var
  nInd:  Integer;
  lCmp:  TComponent;

begin  { MsgDlgActivate }
  Assert(Sender is TForm);
  with  Sender as TForm  do
    begin
      if  THolder(Tag).TimeMin > 0  then
        for  nInd := ComponentCount - 1  downto  0  do
          begin
            lCmp := Components[nInd];
            if  lCmp is TButtonType  then
              with  TButtonType(lCmp)  do
                begin
                  Enabled := False;
                  Tag := {$IF Declared(TCustomButton)}Ord(Default){$ELSE}GetPropValue(lCmp, 'Default', False){$IFEND}
                end
          end { for nInd };
      { - }
      SetForegroundWindow(Handle);
      BringWindowToTop(Handle);
      { - }
      Assert(TObject(Tag) <> nil);
      with  THolder(Tag)  do
        begin
          if  Assigned(fOnActivate)  then
            fOnActivate(Sender);
          if  TimeMin > 0  then
            SetTimer(Handle, 0, TimeMin, Addr(TimerCallBack))
        end { with }
    end { with };
end { MsgDlgActivate };

{$ENDIF -FMX}

{$ENDREGION 'THolder'}


{$REGION 'TInputTextForm'}

{$IFNDEF FMX}

procedure  TInputTextForm.Activate();
begin
  inherited;
  SetForegroundWindow(Handle);
  BringWindowToTop(Handle)
end { Activate };


procedure  TInputTextForm.BtnAcceptClick(Sender:  TObject);
begin
  if  Assigned(ffnCheckValid)  then
    if  not ffnCheckValid(EditText, True)  then
      Abort();
  ModalResult := mrOk
end { BtnAcceptClick };


constructor  TInputTextForm.Create(const   sCaption, sPrompt, sDefault,
                                           sBtnOkCaption, sBtnCancelCaption:  string;
                                   const   CharCase:  TEditCharCase;
                                   const   nMaxLength:  Integer);
var
  lDialogUnits:   TPoint;
  lPromptLabel:   TLabel;
  nButtonTop:     Integer;
  nButtonWidth:   Integer;
  nButtonHeight:  Integer;
  sBtnAccept:     string;
  sBtnCancel:     string;

  function  GetAveCharSize() : TPoint;

  var
    sBuf:  string;
    cInd:  Char;

  begin  { GetAveCharSize }
    SetLength(sBuf,
              Ord('Z') - Ord('A') + 1
                + Ord('z') - Ord('a') + 1);
    for  cInd := 'A'  to  'Z'  do
      sBuf[Ord(cInd) - Ord(Pred('A'))] := cInd;
    for  cInd := 'a'  to  'z'  do
      sBuf[(Ord('Z') - Ord('A') + 1) + Ord(cInd) - Ord('a') + 1] := cInd;
    with  Canvas.TextExtent(sBuf)  do
      begin
        Result.X := cx div Length(sBuf);
        Result.Y := cy
      end { with }
  end { GetAveCharSize };

begin  { Create }
  sBtnAccept := sBtnOkCaption;
  if  sBtnAccept = ''  then
    sBtnAccept := dgettext('GSkPasLib', gcsDefBtnAcceptCaption);
  sBtnCancel := sBtnCancelCaption;
  if  sBtnCancel = ''  then
    sBtnCancel := dgettext('GSkPasLib', gcsDefBtnCancelCaption);
  inherited CreateNew(Application);
  Canvas.Font := Font;
  lDialogUnits := GetAveCharSize();
  BorderStyle := bsDialog;
  Caption := sCaption;
  ClientWidth := Max(MulDiv(IfThen(nMaxLength > 0, nMaxLength, gcnDefMaxLength),
                            lDialogUnits.X, 4),
                     MulDiv(180, lDialogUnits.X, 4));
  if  Width >= Screen.DesktopWidth  then
    Width := MulDiv(Screen.DesktopWidth, 4, 5);
  Position := poScreenCenter;
  OnShow := FormShow;
  OnClose := FormClose;
  FormStyle := fsStayOnTop;
  { Prompt }
  lPromptLabel := TLabel.Create(Self);
  with  lPromptLabel  do
    begin
      Parent := Self;
      Caption := sPrompt;
      Left := MulDiv(8, lDialogUnits.X, 4);
      Top := MulDiv(8, lDialogUnits.Y, 8);
      Constraints.MaxWidth := Self.ClientWidth - 2 * Left;
      WordWrap := True;
    end { with lPromptLabel };
  { Edit }
  fedtValue := TEdit.Create(Self);
  with  fedtValue  do
    begin
      Parent := Self;
      Left := lPromptLabel.Left;
      Top := lPromptLabel.Top + lPromptLabel.Height + 5;
      Width := Self.ClientWidth - 2 * Left;
      MaxLength := IfThen(nMaxLength >= 0, nMaxLength, 0);
      fedtValue.CharCase := CharCase;
      Text := sDefault;
      OnChange := OnEditChange;
      SelectAll()
    end { with fedtValue };
  { Buttons }
  nButtonTop := fedtValue.Top + fedtValue.Height + 15;
  nButtonWidth := (Max(Length(sBtnAccept), Length(sBtnCancel)) + 4)
                  * lDialogUnits.X;
  nButtonHeight := MulDiv(14, lDialogUnits.Y, 8);
  { [OK] }
  fbtnOK := TButton.Create(Self);
  with  fbtnOK  do
    begin
      Parent := Self;
      Caption := sBtnOkCaption;
      ModalResult := mrNone;
      Default := True;
      SetBounds(Self.ClientWidth - 2 * (lPromptLabel.Left + nButtonWidth),
                nButtonTop, nButtonWidth, nButtonHeight);
      OnClick := BtnAcceptClick
    end { with fbtnOK };
  { [Cancel] }
  with  TButton.Create(Self)  do
    begin
      Parent := Self;
      Caption := sBtnCancelCaption;
      ModalResult := mrCancel;
      Cancel := True;
      SetBounds(Self.ClientWidth - lPromptLabel.Left - nButtonWidth,
                fedtValue.Top + fedtValue.Height + 15,
                nButtonWidth, nButtonHeight);
      Self.ClientHeight := Top + Height + 13;
    end { Cancel };
  {-}
  OnEditChange(nil);
  ActiveControl := fedtValue;
  {-}
  SetWindowLong(Handle, GWL_EXSTYLE,
                GetWindowLong(Handle, GWL_EXSTYLE)
                  or WS_EX_APPWINDOW or WS_EX_TOPMOST);
  SetWindowLong(Handle, GWL_HWNDPARENT, GetDesktopWindow())
end { Create };


procedure  TInputTextForm.FormClose(Sender:  TObject;
                                    var   Action:  TCloseAction);
begin
  Application.RestoreTopMosts()
end { FormClose };


procedure TInputTextForm.FormShow(Sender: TObject);
begin
  Application.NormalizeAllTopMosts()
end { FormShow };


function  TInputTextForm.GetEditText() : string;
begin
  Result := Trim(fedtValue.Text)
end { GetEditText };


procedure  TInputTextForm.OnEditChange(Sender:  TObject);

var
  sText:   string;
  bValid:  Boolean;

begin  { OnEditChange }
  sText := EditText;
  bValid := fbAllowEmpty
            or (sText <> '');
  if  bValid  then
    if  Assigned(ffnCheckValid)  then
      bValid := ffnCheckValid(sText, False);
  fbtnOK.Enabled := bValid
end { OnEditChange };

{$ENDIF -FMX}

{$ENDREGION 'TInputTextForm'}


{$REGION 'TInputTextValidate'}

{$IFNDEF FMX}

class function  TInputTextValidate.ValidDate(const   sText:  string;
                                             const   bFinal:  Boolean)
                                             : Boolean;
begin
  if  bFinal  then
    try
      StrToDate(sText);
      Result := True
    except
      Result := False
    end { try-except }
  else
    Result := True
end { ValidDate };


class function  TInputTextValidate.ValidFloat(const   sText:  string;
                                              const   bFinal:  Boolean)
                                              : Boolean;
begin
  if  bFinal  then
    try
      StrToFloat(sText);
      Result := True
    except
      Result := False
    end { try-except }
  else
    Result := True
end { ValidFloat };


class function  TInputTextValidate.ValidInteger(const   sText:  string;
                                                const   bFinal:  Boolean)
                                                : Boolean;
begin
  if  bFinal  then
    try
      StrToInt64(sText);
      Result := True
    except
      Result := False
    end { try-except }
  else
    Result := True
end { ValidInteger };

{$ENDIF -FMX}

{$ENDREGION 'TInputTextValidate'}


initialization
  {$IFNDEF FMX}
    SetDSATimeFormatter(DSATimeFormatter);
    {$IFNDEF GetText}
      gButtonCaptions[mbYes] := '&Tak';
      gButtonCaptions[mbNo] := '&Nie';
      gButtonCaptions[mbOK] := 'OK';
      gButtonCaptions[mbCancel] := '&Anuluj';
      gButtonCaptions[mbAbort] := 'P&rzerwij';
      gButtonCaptions[mbRetry] := '&Powtórz';
      gButtonCaptions[mbIgnore] := '&Ignoruj';
      gButtonCaptions[mbAll] := '&Wszystko';
      gButtonCaptions[mbNoToAll] := 'Zawsze NI&E';
      gButtonCaptions[mbYesToAll] := 'Zawsze TA&K';
      gButtonCaptions[mbHelp] := 'Pomo&c';
      {$IF CompilerVersion >= gcnCompilerVersionDelphiXE}
        gButtonCaptions[mbClose] := 'Zamknij';
      {$IFEND}
      gCaptions[mtWarning] := 'Ostrzeżenie';
      gCaptions[mtError] := 'Błąd';
      gCaptions[mtInformation] := 'Informacja';
      gCaptions[mtConfirmation] := 'Potwierdź';
      gCaptions[mtCustom] := '';
    {$ELSE}
      gButtonCaptions[mbYes] := dgettext('GSkPasLib', '&Yes');
      gButtonCaptions[mbNo] := dgettext('GSkPasLib', '&No');
      gButtonCaptions[mbOK] := dgettext('GSkPasLib', 'OK');
      gButtonCaptions[mbCancel] := dgettext('GSkPasLib', '&Cancel');
      gButtonCaptions[mbAbort] := dgettext('GSkPasLib', '&Abort');
      gButtonCaptions[mbRetry] := dgettext('GSkPasLib', '&Retry');
      gButtonCaptions[mbIgnore] := dgettext('GSkPasLib', '&Ignore');
      gButtonCaptions[mbAll] := dgettext('GSkPasLib', '&All');
      gButtonCaptions[mbNoToAll] := dgettext('GSkPasLib', 'N&o to All');
      gButtonCaptions[mbYesToAll] := dgettext('GSkPasLib', 'Yes to &All');
      gButtonCaptions[mbHelp] := dgettext('GSkPasLib', '&Help');
      {$IF CompilerVersion >= gcnCompilerVersionDelphiXE}
        gButtonCaptions[mbClose] := dgettext('GSkPasLib', '&Close');
      {$IFEND}
      gCaptions[mtWarning] := dgettext('GSkPasLib', 'Warning');
      gCaptions[mtError] := dgettext('GSkPasLib', 'Error');
      gCaptions[mtInformation] := dgettext('GSkPasLib', 'Information');
      gCaptions[mtConfirmation] := dgettext('GSkPasLib', 'Confirm');
      gCaptions[mtCustom] := '';
    {$ENDIF GetText}
  {$ENDIF -FMX}


end.

