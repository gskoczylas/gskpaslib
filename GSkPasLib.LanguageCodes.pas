﻿{: @abstract Obsługa GNU GetText. }
unit  GSkPasLib.LanguageCodes;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{ Jeżeli trzeba będzie powiązać język z LANGID, to być może przydatna bedzie strona
  http://msdn.microsoft.com/en-us/library/windows/desktop/dd318693(v=vs.85).aspx }


{: @abstract Dla danego kodu zwraca nazwę języka.

   Funkcja @name dla danego kodu języka zwraca angielską nazwę tego języka.
   Na przykład wywołanie @code(GetLanguage('pl')) daje w wyniku @code('Polish'). }
function  GetLanguageName(const   sLangCode:  string)
                          :string;


implementation


function  GetLanguageName(const   sLangCode:  string)
                          :string;
const
  cCodeName:  array[1..193] of
                record
                  fsCode:  string;
                  fsName:  string
                end = ((fsCode: 'aa'; fsName: 'Afar'),
                       (fsCode: 'ab'; fsName: 'Abkhazian'),
                       (fsCode: 'ae'; fsName: 'Avestan'),
                       (fsCode: 'af'; fsName: 'Afrikaans'),
                       (fsCode: 'ak'; fsName: 'Akan'),
                       (fsCode: 'am'; fsName: 'Amharic'),
                       (fsCode: 'an'; fsName: 'Aragonese'),
                       (fsCode: 'ar'; fsName: 'Arabic'),
                       (fsCode: 'as'; fsName: 'Assamese'),
                       (fsCode: 'av'; fsName: 'Avaric'),
                       (fsCode: 'ay'; fsName: 'Aymara'),
                       (fsCode: 'az'; fsName: 'Azerbaijani'),
                       (fsCode: 'ba'; fsName: 'Bashkir'),
                       (fsCode: 'be'; fsName: 'Belarusian'),
                       (fsCode: 'bg'; fsName: 'Bulgarian'),
                       (fsCode: 'bh'; fsName: 'Bihari'),
                       (fsCode: 'bi'; fsName: 'Bislama'),
                       (fsCode: 'bm'; fsName: 'Bambara'),
                       (fsCode: 'bn'; fsName: 'Bengali'),
                       (fsCode: 'bo'; fsName: 'Tibetan'),
                       (fsCode: 'br'; fsName: 'Breton'),
                       (fsCode: 'bs'; fsName: 'Bosnian'),
                       (fsCode: 'ca'; fsName: 'Catalan'),
                       (fsCode: 'ce'; fsName: 'Chechen'),
                       (fsCode: 'ch'; fsName: 'Chamorro'),
                       (fsCode: 'co'; fsName: 'Corsican'),
                       (fsCode: 'cr'; fsName: 'Cree'),
                       (fsCode: 'cs'; fsName: 'Czech'),
                       (fsCode: 'cv'; fsName: 'Chuvash'),
                       (fsCode: 'cy'; fsName: 'Welsh'),
                       (fsCode: 'da'; fsName: 'Danish'),
                       (fsCode: 'de'; fsName: 'German'),
                       (fsCode: 'de_AT'; fsName: 'Austrian German'),
                       (fsCode: 'de_CH'; fsName: 'Swiss German'),
                       (fsCode: 'dv'; fsName: 'Divehi'),
                       (fsCode: 'dz'; fsName: 'Dzongkha'),
                       (fsCode: 'ee'; fsName: 'Ewe'),
                       (fsCode: 'el'; fsName: 'Greek'),
                       (fsCode: 'en'; fsName: 'English'),
                       (fsCode: 'en_AU'; fsName: 'Australian English'),
                       (fsCode: 'en_CA'; fsName: 'Canadian English'),
                       (fsCode: 'en_GB'; fsName: 'British English'),
                       (fsCode: 'en_US'; fsName: 'American English'),
                       (fsCode: 'eo'; fsName: 'Esperanto'),
                       (fsCode: 'es'; fsName: 'Spanish'),
                       (fsCode: 'et'; fsName: 'Estonian'),
                       (fsCode: 'eu'; fsName: 'Basque'),
                       (fsCode: 'fa'; fsName: 'Persian'),
                       (fsCode: 'ff'; fsName: 'Fulah'),
                       (fsCode: 'fi'; fsName: 'Finnish'),
                       (fsCode: 'fj'; fsName: 'Fijian'),
                       (fsCode: 'fo'; fsName: 'Faroese'),
                       (fsCode: 'fr'; fsName: 'French'),
                       (fsCode: 'fr_BE'; fsName: 'Walloon'),
                       (fsCode: 'fy'; fsName: 'Frisian'),
                       (fsCode: 'ga'; fsName: 'Irish'),
                       (fsCode: 'gd'; fsName: 'Gaelic'),
                       (fsCode: 'gl'; fsName: 'Gallegan'),
                       (fsCode: 'gn'; fsName: 'Guarani'),
                       (fsCode: 'gu'; fsName: 'Gujarati'),
                       (fsCode: 'gv'; fsName: 'Manx'),
                       (fsCode: 'ha'; fsName: 'Hausa'),
                       (fsCode: 'he'; fsName: 'Hebrew'),
                       (fsCode: 'hi'; fsName: 'Hindi'),
                       (fsCode: 'ho'; fsName: 'Hiri Motu'),
                       (fsCode: 'hr'; fsName: 'Croatian'),
                       (fsCode: 'ht'; fsName: 'Haitian'),
                       (fsCode: 'hu'; fsName: 'Hungarian'),
                       (fsCode: 'hy'; fsName: 'Armenian'),
                       (fsCode: 'hz'; fsName: 'Herero'),
                       (fsCode: 'ia'; fsName: 'Interlingua'),
                       (fsCode: 'id'; fsName: 'Indonesian'),
                       (fsCode: 'ie'; fsName: 'Interlingue'),
                       (fsCode: 'ig'; fsName: 'Igbo'),
                       (fsCode: 'ii'; fsName: 'Sichuan Yi'),
                       (fsCode: 'ik'; fsName: 'Inupiaq'),
                       (fsCode: 'io'; fsName: 'Ido'),
                       (fsCode: 'is'; fsName: 'Icelandic'),
                       (fsCode: 'it'; fsName: 'Italian'),
                       (fsCode: 'iu'; fsName: 'Inuktitut'),
                       (fsCode: 'ja'; fsName: 'Japanese'),
                       (fsCode: 'jv'; fsName: 'Javanese'),
                       (fsCode: 'ka'; fsName: 'Georgian'),
                       (fsCode: 'kg'; fsName: 'Kongo'),
                       (fsCode: 'ki'; fsName: 'Kikuyu'),
                       (fsCode: 'kj'; fsName: 'Kuanyama'),
                       (fsCode: 'kk'; fsName: 'Kazakh'),
                       (fsCode: 'kl'; fsName: 'Greenlandic'),
                       (fsCode: 'km'; fsName: 'Khmer'),
                       (fsCode: 'kn'; fsName: 'Kannada'),
                       (fsCode: 'ko'; fsName: 'Korean'),
                       (fsCode: 'kr'; fsName: 'Kanuri'),
                       (fsCode: 'ks'; fsName: 'Kashmiri'),
                       (fsCode: 'ku'; fsName: 'Kurdish'),
                       (fsCode: 'kv'; fsName: 'Komi'),
                       (fsCode: 'kw'; fsName: 'Cornish'),
                       (fsCode: 'ky'; fsName: 'Kirghiz'),
                       (fsCode: 'la'; fsName: 'Latin'),
                       (fsCode: 'lb'; fsName: 'Luxembourgish'),
                       (fsCode: 'lg'; fsName: 'Ganda'),
                       (fsCode: 'li'; fsName: 'Limburgan'),
                       (fsCode: 'ln'; fsName: 'Lingala'),
                       (fsCode: 'lo'; fsName: 'Lao'),
                       (fsCode: 'lt'; fsName: 'Lithuanian'),
                       (fsCode: 'lu'; fsName: 'Luba-Katanga'),
                       (fsCode: 'lv'; fsName: 'Latvian'),
                       (fsCode: 'mg'; fsName: 'Malagasy'),
                       (fsCode: 'mh'; fsName: 'Marshallese'),
                       (fsCode: 'mi'; fsName: 'Maori'),
                       (fsCode: 'mk'; fsName: 'Macedonian'),
                       (fsCode: 'ml'; fsName: 'Malayalam'),
                       (fsCode: 'mn'; fsName: 'Mongolian'),
                       (fsCode: 'mo'; fsName: 'Moldavian'),
                       (fsCode: 'mr'; fsName: 'Marathi'),
                       (fsCode: 'ms'; fsName: 'Malay'),
                       (fsCode: 'mt'; fsName: 'Maltese'),
                       (fsCode: 'my'; fsName: 'Burmese'),
                       (fsCode: 'na'; fsName: 'Nauru'),
                       (fsCode: 'nb'; fsName: 'Norwegian Bokmaal'),
                       (fsCode: 'nd'; fsName: 'Ndebele, North'),
                       (fsCode: 'ne'; fsName: 'Nepali'),
                       (fsCode: 'ng'; fsName: 'Ndonga'),
                       (fsCode: 'nl'; fsName: 'Dutch'),
                       (fsCode: 'nl_BE'; fsName: 'Flemish'),
                       (fsCode: 'nn'; fsName: 'Norwegian Nynorsk'),
                       (fsCode: 'no'; fsName: 'Norwegian'),
                       (fsCode: 'nr'; fsName: 'Ndebele, South'),
                       (fsCode: 'nv'; fsName: 'Navajo'),
                       (fsCode: 'ny'; fsName: 'Chichewa'),
                       (fsCode: 'oc'; fsName: 'Occitan'),
                       (fsCode: 'oj'; fsName: 'Ojibwa'),
                       (fsCode: 'om'; fsName: 'Oromo'),
                       (fsCode: 'or'; fsName: 'Oriya'),
                       (fsCode: 'os'; fsName: 'Ossetian'),
                       (fsCode: 'pa'; fsName: 'Panjabi'),
                       (fsCode: 'pi'; fsName: 'Pali'),
                       (fsCode: 'pl'; fsName: 'Polish'),
                       (fsCode: 'ps'; fsName: 'Pushto'),
                       (fsCode: 'pt'; fsName: 'Portuguese'),
                       (fsCode: 'pt_BR'; fsName: 'Brazilian Portuguese'),
                       (fsCode: 'qu'; fsName: 'Quechua'),
                       (fsCode: 'rm'; fsName: 'Raeto-Romance'),
                       (fsCode: 'rn'; fsName: 'Rundi'),
                       (fsCode: 'ro'; fsName: 'Romanian'),
                       (fsCode: 'ru'; fsName: 'Russian'),
                       (fsCode: 'rw'; fsName: 'Kinyarwanda'),
                       (fsCode: 'sa'; fsName: 'Sanskrit'),
                       (fsCode: 'sc'; fsName: 'Sardinian'),
                       (fsCode: 'sd'; fsName: 'Sindhi'),
                       (fsCode: 'se'; fsName: 'Northern Sami'),
                       (fsCode: 'sg'; fsName: 'Sango'),
                       (fsCode: 'si'; fsName: 'Sinhalese'),
                       (fsCode: 'sk'; fsName: 'Slovak'),
                       (fsCode: 'sl'; fsName: 'Slovenian'),
                       (fsCode: 'sm'; fsName: 'Samoan'),
                       (fsCode: 'sn'; fsName: 'Shona'),
                       (fsCode: 'so'; fsName: 'Somali'),
                       (fsCode: 'sq'; fsName: 'Albanian'),
                       (fsCode: 'sr'; fsName: 'Serbian'),
                       (fsCode: 'ss'; fsName: 'Swati'),
                       (fsCode: 'st'; fsName: 'Sotho, Southern'),
                       (fsCode: 'su'; fsName: 'Sundanese'),
                       (fsCode: 'sv'; fsName: 'Swedish'),
                       (fsCode: 'sw'; fsName: 'Swahili'),
                       (fsCode: 'ta'; fsName: 'Tamil'),
                       (fsCode: 'te'; fsName: 'Telugu'),
                       (fsCode: 'tg'; fsName: 'Tajik'),
                       (fsCode: 'th'; fsName: 'Thai'),
                       (fsCode: 'ti'; fsName: 'Tigrinya'),
                       (fsCode: 'tk'; fsName: 'Turkmen'),
                       (fsCode: 'tl'; fsName: 'Tagalog'),
                       (fsCode: 'tn'; fsName: 'Tswana'),
                       (fsCode: 'to'; fsName: 'Tonga'),
                       (fsCode: 'tr'; fsName: 'Turkish'),
                       (fsCode: 'ts'; fsName: 'Tsonga'),
                       (fsCode: 'tt'; fsName: 'Tatar'),
                       (fsCode: 'tw'; fsName: 'Twi'),
                       (fsCode: 'ty'; fsName: 'Tahitian'),
                       (fsCode: 'ug'; fsName: 'Uighur'),
                       (fsCode: 'uk'; fsName: 'Ukrainian'),
                       (fsCode: 'ur'; fsName: 'Urdu'),
                       (fsCode: 'uz'; fsName: 'Uzbek'),
                       (fsCode: 've'; fsName: 'Venda'),
                       (fsCode: 'vi'; fsName: 'Vietnamese'),
                       (fsCode: 'vo'; fsName: 'Volapuk'),
                       (fsCode: 'wa'; fsName: 'Walloon'),
                       (fsCode: 'wo'; fsName: 'Wolof'),
                       (fsCode: 'xh'; fsName: 'Xhosa'),
                       (fsCode: 'yi'; fsName: 'Yiddish'),
                       (fsCode: 'yo'; fsName: 'Yoruba'),
                       (fsCode: 'za'; fsName: 'Zhuang'),
                       (fsCode: 'zh'; fsName: 'Chinese'),
                       (fsCode: 'zu'; fsName: 'Zulu'));
var
  b, m, e:  Integer;

begin  { GetLanguageName }
  Result := '';
  b := Low(cCodeName);
  e := High(cCodeName);
  repeat
    m := (b + e) div 2;
    if  sLangCode = cCodeName[m].fsCode  then
      begin
        Result := cCodeName[m].fsName;
        Break
      end { found };
    if  sLangCode < cCodeName[m].fsCode  then
      e := m - 1
    else
      b := m + 1
  until  b > e
end { GetLanguageName };


end.
