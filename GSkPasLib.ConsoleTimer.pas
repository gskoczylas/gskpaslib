﻿{: @abstract Odpowiednik @code(TTimer) działający w każdej aplikacji.

   Moduł @name zawiera definicję klasy działającej podobnie do standardowego
   komponentu @code(TTimer). Jednak standardowy komponent działa pod warunkiem,
   że w aplikacji jest procedura obsługi kolejki komunikatów (np. standardowy
   program). Natomiast klasa @link(TConsoleTimer) działa również w aplikacjach
   konsolowych. }
unit  GSkPasLib.ConsoleTimer;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Windows, Classes, SyncObjs, Math;


type
  {: @abstract Odpowiednik @code(TTimer) działający w każdej aplikacji.

     Klasa @name działa podobnie do standardowego komponentu @code(TTimer).
     Jednak standardowy komponent działa pod warunkiem, że w aplikacji jest
     procedura obsługi kolejki komunikatów (np. standardowy program). Natomiast
     klasa @name działa również w aplikacjach konsolowych. }
  TConsoleTimer = class(TThread)
  strict private
    const
      cnStandardInterval = 1000;   // 1000 [ms] = 1 [s]
    var
      fTimerEvent:    TSimpleEvent;
      fTimerProc:     TNotifyEvent;  // method to call
      fbEnabled:      Boolean;
      fbConsiderTime: Boolean;
      fnInterval:     Integer;
      fnLastCallTck:  UInt64;
    procedure  SetInterval(const   nValue:  Integer);
  strict protected
    procedure  Execute();                                            override;
  public
    {: @abstract Konstruktor z opcjonalnym inicjowaniem właściwości.

       @param(fnTimerProc Procedura wywoływana co wskazany czas.)
       @param(nInterval Jak często wywoływać wskazaną procedurę.)
       @param(bConsiderProcessTime Czy uwzględniać czas działania procedury.)

       Jeżeli parametr @code(fnTimerProc) jest różny od @nil oraz parametr
       @code(Interval) jest dodatni, to właściwość @link(Enabled) zostanie
       ustawiona.

       @seeAlso(OnTimerEvent)
       @seeAlso(Interval)
       @seeAlso(ConsiderProcessTime) }
    constructor  Create(const   fnTimerProc:  TNotifyEvent = nil;
                        const   nInterval:  Integer = cnStandardInterval;
                        const   bConsiderProcessTime:  Boolean = True);

    {: @exclude }
    destructor  Destroy();                                           override;
    {-}
    property  Enabled:  Boolean
              read  fbEnabled
              write fbEnabled;
    property  Interval:  Integer
              read  fnInterval
              write SetInterval;

    {: @abstract Czy uwzględnić czas działania procedury.

       Jeżeli właściwość @name ma wartość @false, to procedura wskazana
       w @link(OnTimerEvent) jest wywoływana w jednakowych odstępach czasu
       równych liczbie milisekund wskazanych w @link(Interval).

       Jeżeli właściwość @name ma wartość @true, to kolejne wywołanie procedury
       jest opóźnione o czas poprzedniego działania tej procedury. }
    property  ConsiderProcessTime:  Boolean
              read  fbConsiderTime
              write fbConsiderTime
              default True;

    {: @abstract Procedura wywoływana co wskazany czasu.

       @bold(Uwaga!) Procedura będzie wywoływana w kontekście wątku. }
    property  OnTimerEvent:  TNotifyEvent
              read  fTimerProc
              write fTimerProc;
  end { TConsoleTimer };


implementation


uses
  GSkPasLib.Thread, GSkPasLib.TimeUtils;


{$REGION 'TConsoleTimer'}

constructor  TConsoleTimer.Create(const   fnTimerProc:  TNotifyEvent;
                                  const   nInterval:  Integer;
                                  const   bConsiderProcessTime:  Boolean);
begin
  inherited Create(False);
  fTimerEvent := TSimpleEvent.Create();
  fTimerProc := fnTimerProc;
  fbConsiderTime := bConsiderProcessTime;
  fnInterval := nInterval;
  fnLastCallTck := GetTickCount64();
  FreeOnTerminate := False; // Main thread controls for thread destruction
  Enabled := Assigned(fnTimerProc) and (nInterval > 0)
end { Create };


destructor  TConsoleTimer.Destroy();
begin
  Terminate();
  fTimerEvent.SetEvent();   // Stop timer event
  Waitfor();                // Synchronize
  fTimerEvent.Free();
  inherited
end { Destroy };


procedure  TConsoleTimer.Execute();

const
  cnMaxWait = 100;   // 100 [ms]

var
  nWait:  Cardinal;

begin  { Execute }
  while  not Terminated  do
    begin
      if  Enabled
          and (Interval > 0)  then
        nWait := Min(GetTickCount64() - fnLastCallTck, cnMaxWait)
      else
        nWait := cnMaxWait;
      if  nWait <> 0  then
        if  WaitFor(fTimerEvent, nWait) <> wrTimeout  then
          Break;
      if  GetTickCount64() - fnLastCallTck >= Interval  then
        begin
          if  not ConsiderProcessTime  then
            fnLastCallTck := GetTickCount64();
          if  Enabled
              and Assigned(fTimerProc)  then
            fTimerProc(Self);
          if  ConsiderProcessTime  then
            fnLastCallTck := GetTickCount64()
        end { if }
    end { while not Terminated }
end { Execute };


procedure  TConsoleTimer.SetInterval(const   nValue:  Integer);
begin
  fnInterval := nValue;
  fnLastCallTck := GetTickCount64();
end { SetInterval };

{$ENDREGION 'TConsoleTimer'}


end.
