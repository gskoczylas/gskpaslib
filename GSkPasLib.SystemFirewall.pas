﻿{: @abstract Podprogramy związane z Zaporą systemu Windows XP i następnych.

   Wraz z pojawieniem się zestawu poprawek Service Pack 2 dla Windows XP
   w systemie tym pojawiła się zapora systemowa blokująca swobodny dostęp
   do komputera z internetu oraz innych komputerów w sieci. Zapora ta pozwala
   definiować wyjątki. Wyjątkiem może być aplikacja, port TCP lub port UDP.

   Jeżeli wyjątkiem jest aplikacja to Zapora pozwala na dowolną komunikację z
   tą aplikacją.

   Jeżeli wyjątkiem jest port TCP lub UDP to Zapora pozwala na komunikację
   dowolnej aplikacji przez ten port.

   Zapora systemu Windows XP pozwala programowo modyfikować wyjątki oraz
   sterować pracą zapory poprzez systemowy obiekt COM. Aby można było ten obiekt
   wykorzystać w Delphi trzeba go „opakować” odpowiednio. W tym celu należy
   w trybie znakowym wykonać polecenie: @preformatted(
   tlibimp %windir%\System32\hnetcfg.dll\3 )

   Program @code(tlibimp.exe) jest w folderze @code($(DELPHI)\Bin). Po wykonaniu
   powyższego polecenia na dysku zostanie utworzony plik
   @code(NetFwTypeLib_TLB.pas). Z tego pliku korzysta moduł @name.

   Wykonałem powyższe polecenie i wygenerowany plik @code(NetFwTypeLib_TLB.pas)
   umieściłem już w tym pakiecie. Ponieważ nie jestem jego autorem więc nie jest
   on w tym pakiecie udokumentowany.

   Podprogramy dostępne w module @name nie wyczerpują wszystkich możliwości
   udostępnionych przez Zaporę. Szczegółowy opis funkcji Zapory można znaleźć
   pod adresem:
   http://msdn.microsoft.com/library/en-us/ics/ics/windows_firewall_start_page.asp.  }
unit  GSkPasLib.SystemFirewall;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  NetFwTypeLib_TLB, ComObj, Variants;


const
  { Protokół }
  NET_FW_IP_PROTOCOL_TCP    = NetFwTypeLib_TLB.NET_FW_IP_PROTOCOL_TCP;
  NET_FW_IP_PROTOCOL_UDP    = NetFwTypeLib_TLB.NET_FW_IP_PROTOCOL_UDP;
  { Zakres }
  NET_FW_SCOPE_ALL          = NetFwTypeLib_TLB.NET_FW_SCOPE_ALL;
  NET_FW_SCOPE_LOCAL_SUBNET = NetFwTypeLib_TLB.NET_FW_SCOPE_LOCAL_SUBNET;
  NET_FW_SCOPE_CUSTOM       = NetFwTypeLib_TLB.NET_FW_SCOPE_CUSTOM;
  NET_FW_SCOPE_MAX          = NetFwTypeLib_TLB.NET_FW_SCOPE_MAX;
  { Wersja IP }
  NET_FW_IP_VERSION_V4      = NetFwTypeLib_TLB.NET_FW_IP_VERSION_V4;
  NET_FW_IP_VERSION_V6      = NetFwTypeLib_TLB.NET_FW_IP_VERSION_V6;
  NET_FW_IP_VERSION_ANY     = NetFwTypeLib_TLB.NET_FW_IP_VERSION_ANY;
  NET_FW_IP_VERSION_MAX     = NetFwTypeLib_TLB.NET_FW_IP_VERSION_MAX;


type
  NET_FW_IP_PROTOCOL = NetFwTypeLib_TLB.NET_FW_IP_PROTOCOL_;
  NET_FW_SCOPE       = NetFwTypeLib_TLB.NET_FW_SCOPE_;
  NET_FW_IP_VERSION  = NetFwTypeLib_TLB.NET_FW_IP_VERSION_;


{: @abstract Funkcja sprawdza czy Zapora systemu Windows jest aktywna.

   @return Wynik funkcji @name wskazuje czy Zapora systemu Windows jest aktywna.
}
function  IsFirewallEnabled() : Boolean;


{: @abstract Procedura dodaje wskazany port do listy wyjątków Zapory systemu Windows.

   Po dodaniu portu dowolna aplikacja może korzystać z tego portu.

   @param(nPort Numer portu.)
   @param(sName Nazwa wyjątku.)
   @param(Protocol Dla jakiego protokołu (domyślnie: TCP).

          Można użyć jedną z wartości: @unorderedList(
                                         @item NET_FW_IP_PROTOCOL_TCP
                                         @item NET_FW_IP_PROTOCOL_UDP ))
   @param(Scope Pozwala ograniczyć działanie wyjątku dla grupy adresów (domyślnie: bez ograniczeń).

          Można użyć jedną z wartości: @unorderedList(
                                         @item NET_FW_SCOPE_ALL
                                         @item NET_FW_SCOPE_LOCAL_SUBNET
                                         @item NET_FW_SCOPE_CUSTOM
                                         @item NET_FW_SCOPE_MAX ))
   @param(bEnabled Pozwala zdefiniować wyjątek ale nie aktywować go (@false).)
   @param(IPVersion Wersja protokołu IP (domyślnie: dowolna).

                    Można użyć jedną z wartości: @unorderedList(
                                                   @item NET_FW_IP_VERSION_V4
                                                   @item NET_FW_IP_VERSION_V6
                                                   @item NET_FW_IP_VERSION_ANY
                                                   @item NET_FW_IP_VERSION_MAX ))
   @seeAlso(FirewallRemovePort)
   @seeAlso(FirewallAddApplication)
}
procedure  FirewallAddPort(const   nPort:     Integer;
                           const   sName:     string;
                           const   Protocol:  NET_FW_IP_PROTOCOL = NET_FW_IP_PROTOCOL_TCP;
                           const   Scope:     NET_FW_SCOPE = NET_FW_SCOPE_ALL;
                           const   bEnabled:  Boolean = True;
                           const   IPVersion: NET_FW_IP_VERSION = NET_FW_IP_VERSION_ANY);


{: @abstract Procedura usuwa wskazany port z listy wyjątków Zapory systemu Windows.

   @param(nPort Numer portu.)
   @param(sName Nazwa wyjątku.)
   @param(Protocol Dla jakiego protokołu (domyślnie: TCP).

          Można użyć jedną z wartości: @unorderedList(
                                         @item NET_FW_IP_PROTOCOL_TCP
                                         @item NET_FW_IP_PROTOCOL_UDP ))
   @seeAlso(FirewallAddPort)
   @seeAlso(FirewallRemoveApplication)
}
procedure  FirewallRemovePort(const   nPort:  Integer;
                              const   Protocol:  NET_FW_IP_PROTOCOL = NET_FW_IP_PROTOCOL_TCP);


{: @abstract Procedura dodaje wskazaną aplikację do listy wyjątków Zapory systemu Windows.

   @param(sPath Ścieżka do aplikacji dodawanej do Zapory jako wyjątek.)
   @param(sName Nazwa wyjątku.)
   @param(Scope Pozwala ograniczyć działanie wyjątku dla grupy adresów (domyślnie: bez ograniczeń).

          Można użyć jedną z wartości: @unorderedList(
                                         @item NET_FW_SCOPE_ALL
                                         @item NET_FW_SCOPE_LOCAL_SUBNET
                                         @item NET_FW_SCOPE_CUSTOM
                                         @item NET_FW_SCOPE_MAX ))
   @param(bEnabled Pozwala zdefiniować wyjątek ale nie aktywować go (@false).)
   @param(IPVersion Wersja protokołu IP (domyślnie: dowolna).

                    Można użyć jedną z wartości: @unorderedList(
                                                   @item NET_FW_IP_VERSION_V4
                                                   @item NET_FW_IP_VERSION_V6
                                                   @item NET_FW_IP_VERSION_ANY
                                                   @item NET_FW_IP_VERSION_MAX ))
   @seeAlso(FirewallRemoveApplication)
   @seeAlso(FirewallAddPort)
}
procedure  FirewallAddApplication(const   sPath:     string;
                                  const   sName:     string;
                                  const   Scope:     NET_FW_SCOPE = NET_FW_SCOPE_ALL;
                                  const   bEnabled:  Boolean = True;
                                  const   IPVersion: NET_FW_IP_VERSION = NET_FW_IP_VERSION_ANY);


{: @abstract Procedura usuwa wskazaną aplikację z listy wyjątków Zapory systemu Windows.

   @param(sPath Ścieżka do aplikacji dodawanej do Zapory jako wyjątek.)

   @seeAlso(FirewallAddApplication)
   @seeAlso(FirewallRemovePort)
}
procedure  FirewallRemoveApplication(const   sPath:  string);


implementation


const
  gcsFwMgrClassName                   = 'HNetCfg.FwMgr';
  gcsFwOpenPortClassName              = 'HNetCfg.FwOpenPort';
  gcsFwAuthorizedApplicationClassName = 'HNetCfg.FwAuthorizedApplication';


function  IsFirewallEnabled() : Boolean;

var
  iFwMgrDisp:  IDispatch;
  iFwMgr:      INetFwMgr;

begin  { IsFirewallEnabled }
  iFwMgrDisp := CreateOleObject(gcsFwMgrClassName);
  try
    iFwMgr := INetFwMgr(iFwMgrDisp);
    Result := iFwMgr.LocalPolicy.CurrentProfile.FirewallEnabled
  finally
    iFwMgrDisp := Unassigned
  end { try-finally }
end { IsFirewallEnabled };


procedure  FirewallAddPort(const   nPort:     Integer;
                           const   sName:     string;
                           const   Protocol:  NET_FW_IP_PROTOCOL;
                           const   Scope:     NET_FW_SCOPE;
                           const   bEnabled:  Boolean;
                           const   IPVersion: NET_FW_IP_VERSION);
var
  iFwMgrDisp:      IDispatch;
  iFwOpenPortDisp: IDispatch;
  iFwMgr:          INetFwMgr;
  iFwOpenPort:     INetFwOpenPort;

begin  { FirewallAddPort }
  iFwMgrDisp := CreateOleObject(gcsFwMgrClassName);
  try
    iFwMgr := INetFwMgr(iFwMgrDisp);
    iFwOpenPortDisp := CreateOleObject(gcsFwOpenPortClassName);
    try
      iFwOpenPort := INetFwOpenPort(iFwOpenPortDisp);
      iFwOpenPort.Port := nPort;
      iFwOpenPort.Name := sName;
      iFwOpenPort.Protocol := Protocol;
      iFwOpenPort.Scope := Scope;
      iFwOpenPort.Enabled := bEnabled;
      iFwOpenPort.IpVersion := IPVersion;
      iFwMgr.LocalPolicy.CurrentProfile.GloballyOpenPorts.Add(iFwOpenPort)
    finally
      iFwOpenPortDisp := Unassigned
    end { try-finally }
  finally
    iFwMgrDisp := Unassigned
  end { try-finally }
end { FirewallAddPort };


procedure  FirewallRemovePort(const   nPort:  Integer;
                              const   Protocol:  NET_FW_IP_PROTOCOL);
var
  iFwMgrDisp:  IDispatch;
  iFwMgr:  INetFwMgr;

begin  { FirewallRemovePort }
  iFwMgrDisp := CreateOleObject(gcsFwMgrClassName);
  try
    iFwMgr := INetFwMgr(iFwMgrDisp);
    iFwMgr.LocalPolicy.CurrentProfile.GloballyOpenPorts.Remove(nPort, Protocol)
  finally
    iFwMgrDisp := Unassigned
  end
end { FirewallRemovePort };


procedure  FirewallAddApplication(const   sPath:     string;
                                  const   sName:     string;
                                  const   Scope:     NET_FW_SCOPE;
                                  const   bEnabled:  Boolean;
                                  const   IPVersion: NET_FW_IP_VERSION);
var
  iFwMgrDisp:  IDispatch;
  iFwAuthorizedApplicationDisp:  IDispatch;
  iFwMgr:  INetFwMgr;
  iFwAuthorizedApplication:  INetFwAuthorizedApplication;

begin  { FirewallAddApplication }
  iFwMgrDisp := CreateOleObject(gcsFwMgrClassName);
  try
    iFwMgr := INetFwMgr(iFwMgrDisp);
    iFwAuthorizedApplicationDisp := CreateOleObject(gcsFwAuthorizedApplicationClassName);
    try
      iFwAuthorizedApplication := INetFwAuthorizedApplication(iFwAuthorizedApplicationDisp);
      iFwAuthorizedApplication.ProcessImageFileName := sPath;
      iFwAuthorizedApplication.Name := sName;
      iFwAuthorizedApplication.Scope := Scope;
      iFwAuthorizedApplication.Enabled := bEnabled;
      iFwAuthorizedApplication.IpVersion := IPVersion;
      iFwMgr.LocalPolicy.CurrentProfile.AuthorizedApplications.Add(iFwAuthorizedApplication)
    finally
      iFwAuthorizedApplicationDisp := Unassigned
    end { try-finally }
  finally
    iFwMgrDisp := Unassigned
  end { try-finally }
end { FirewallAddApplication };



procedure  FirewallRemoveApplication(const   sPath:  string);

var
  iFwMgrDisp:  IDispatch;
  iFwMgr:  INetFwMgr;

begin  { FirewallRemoveApplication }
  iFwMgrDisp := CreateOleObject(gcsFwMgrClassName);
  try
    iFwMgr := INetFwMgr(iFwMgrDisp);
    iFwMgr.LocalPolicy.CurrentProfile.AuthorizedApplications.Remove(sPath)
  finally
    iFwMgrDisp := Unassigned
  end { try-finally }
end { FirewallRemoveApplication };


end.
