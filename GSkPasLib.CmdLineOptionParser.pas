﻿{: @abstract(Analizator opcji z linii poleceń.) }
unit  GSkPasLib.CmdLineOptionParser;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    System.Classes, System.Variants, System.SysUtils, System.Math,
    System.Generics.Collections;
  {$ELSE}
    Classes, Variants, SysUtils, Math, Generics.Collections;
  {$IFEND}


const
  {: Oznaczenie krótkiej opcji }
  gcchDefShortOptionChar = '-';
  {: Oznaczenie długiej opcji }
  gcsDefLongOptionString = '--';


type
  TGSkOptionParser = class;
  TGSkCustomOption = class;

  TGSkCustomOptionClass = class of TGSkCustomOption;


  {: @abstract(Abstrakcyjna bazowa klasa dla opcji.)

     Ta klasa implementuje podstawowy zestaw metod i właściwości wykorzystywanych
     przez klasę @link(TGSkOptionParser). Definiuje również właściwość pozwalającą
     definiować opis opcji.

     @seeAlso(TGSkBoolOption)
     @seeAlso(TGSkCustomValueOption)
     @seeAlso(TGSkIntegerOption)
     @seeAlso(TGSkFloatOption)
     @seeAlso(TGSkStringOption)
     @seeAlso(TGSkStringListOption)
     @seeAlso(TGSkPathListOption)
     @seeAlso(TGSkSetOption)  }
  TGSkCustomOption = class abstract
  private
    var
      fParser:  TGSkOptionParser;
    function  ParseOption(const   Words:  TStrings)
                          : Boolean;                          virtual; abstract;
  strict protected
    fchShort:         Char;
    fsLong:           string;
    fbShortSens:      Boolean;
    fbLongSens:       Boolean;
    fsExplanation:    string;
    fbWasSpecified:   Boolean;
    fbHidden:         Boolean;
    {-}
    function  GetValueV() : Variant;                          virtual; abstract;
    procedure  SetValueV(const   vValue:  Variant);           virtual; abstract;
    {-}
    function  FormatExplanation(const   nOptWidth:  Integer;
                                const   bWrapText:  Boolean)
                                : string;
  public
    {: @abstract(Tworzy nową opcję).

       Tworzy nowy obiekt typu @className.

       Jeżeli parametr @code(chShort) ma wartość @code(#0) to dana opcja nie ma
       krótkiej wersji.

       Jeżeli parametr @code(sLong) jest pustym napisem to dana opcja nie ma
       długiej wersji. }
    constructor Create(const   chShort:  Char;
                       const   sLong:   string;
                       const   sExplanation:  string = '';
                       const   bShortCaseSensitive:  Boolean = False;
                       const   bLongCaseSensitive:  Boolean = False);   virtual;

    {: @abstract(Zwraca długość opcji.)

       Metoda @name zwraca długość napisu "-s, @--long-option", gdzie @code(s)
       jest krótką opcją. }
    function  GetOptionWidth() : Integer;

    {: @abstract(Zwraca opis opcji.);

       Opis zawiera również format opcji. }
    function  GetExplanation(const   nOptWidth:  Integer)
                             : string;

    {: @abstract(Wypisuje na ekranie (tryb znakowy) opis opcji.)

       Metoda @name może być stosowana w trybie znakowym do wyświetlenia
       opisów opcji. Opis zawiera również format opcji.

       Parametr @code(nOptWidth) wskazuje wielkość wcięcia opisu. }
    procedure  WriteExplanation(const   nOptWidth:  Integer);

    {: @abstract Wygodne definiowanie podstawowych cech opcji.

       Metoda @name przypisuje wartości następującym właściwościom opcji:
       @unorderedList(@itemSpacing(Compact)
         @item(@link(ShortForm))
         @item(@link(LongForm))
         @item(@link(Explanation))
       )  }
    procedure  Setup(const   chShortForm:  Char;
                     const   sLongForm:  string;
                     const   sExplanation:  string);

    {--- properties ---}

    {: @abstract(Krótka wersja opcji.)

       Jeżeli ma wartość @code(#0) to krótka wersja opcji nie jest używana. }
    property  ShortForm: Char
              read  fchShort
              write fchShort;

    {: @abstract(Długa wersja opcji.)

       Jeżeli jest pustym napisem to długa wersja opcji nie jest używana.
       Zazwyczaj długa wersja opcji składa się z więcej niż jednego znaku. }
    property  LongForm:  string
              read  fsLong
              write fsLong;

    {: Wskazuje czy wielkość liter ma znaczenie w krótkiej wersji opcji. }
    property  ShortCaseSensitive:  Boolean
              read  fbShortSens
              write fbShortSens;

    {: Wskazuje czy wielkość liter ma znaczenie w długiej wersji opcji. }
    property  LongCaseSensitive:  Boolean
              read  fbLongSens
              write fbLongSens;

    {: @abstract Wskazuje czy to jest „oficjalna” opcja.

       Jeżeli właściwość @name ma wartość @false, to zostanie pominięta przez
       metody @link(TGSkOptionParser.GetExplanations) oraz
       @link(TGSkOptionParser.WriteExplanations).  }
    property  Hidden:  Boolean
              read  fbHidden
              write fbHidden;

    {: Wskazuje czy opcja występuje choć raz. }
    property  WasSpecified:  Boolean
              read  fbWasSpecified;

    {: Opis opcji.
       @seeAlso(WriteExplanation) }
    property  Explanation:  string
              read  fsExplanation
              write fsExplanation;

    {: @abstract(Wartość jako @code(Variant).)

       Dla łatwiejszego dostępu przez właściwość @link(TGSkOptionParser.ByName). }
    property  AsVariant:  Variant
              read  GetValueV
              write SetValueV;
  end { TGSkCustomOption };


  {: @abstract(Prosta opcja logiczna.)

     Wyłączona, jeżeli nie występuje, włączona jeżeli występuje.
     Nie może przyjmować żadnych wartości.

     @seeAlso(TGSkOptionParser)  }
  TGSkBoolOption = class(TGSkCustomOption)
  strict protected
    function  ParseOption(const   Words:  TStrings)
                          : Boolean;                                 override;
    function  GetValueV() : Variant;                                 override;
    procedure  SetValueV(const   vValue:  Variant);                  override;
  public
    property  TurnedOn:  Boolean
              read  fbWasSpecified;
  end { TGSkBoolOption };


  {: @abstract(Klasa bazowa dla opcji z wartością.)

     @className jest bazową klasą dla wszelkich opcji mogących mieć jedną lub
     więcej wartości, zdefiniowanych jako @code(@--option=value)
     lub @code(@--option value).

     @seeAlso(TGSkIntegerOption)
     @seeAlso(TGSkFloatOption)
     @seeAlso(TGSkStringOption)
     @seeAlso(TGSkStringListOption)
     @seeAlso(TGSkSetOption)  }
  TGSkCustomValueOption<T> = class abstract(TGSkCustomOption)
  strict private
    var
      fValue:  T;

    function  ChkVal(const   Words:  TStrings;
                     const   nOptLen:  Integer)
                     : Boolean;
  strict protected
    function  GetValue() : T;                                        virtual;
    procedure  SetValue(const   Value:  T);                          virtual;

    {: @abstract Sprawdza, czy wskazany napis jest poprawną wartością opcji.

       Wynik funkcji wskazuje, czy napis wskazany w parametrze jest poprawną
       wartością opcji. Na przykład może sprawdzać, czy napis jest poprawną
       liczbą całkowitą itp.

       Jeżeli wartość opcji jest poprawna, to zostaje zapamiętana w obiekcie
       reprezentującym daną opcję. }
    function  CheckValue(const   sValue:  string)
                         : Boolean;                           virtual; abstract;
    function  ParseOption(const   Words:  TStrings)
                          : Boolean;                                 override;
  public
    property  Value:  T
              read  fValue
              write fValue;
  end { TGSkCustomValueOption };


  {: @abstract(Opcja z wartością będącą liczbą całkowitą.)

     @seeAlso(TGSkOptionParser)  }
  TGSkIntegerOption = class(TGSkCustomValueOption<Integer>)
  strict private
  strict protected
    function  CheckValue(const   sValue:  string)
                         : Boolean;                                  override;
    function  GetValueV() : Variant;                                 override;
    procedure  SetValueV(const   vValue:  Variant);                  override;
  public

    {: @abstract Wygodne definiowanie podstawowych cech opcji.

       Metoda @name przypisuje wartości następującym właściwościom opcji:
       @unorderedList(@itemSpacing(Compact)
         @item(@link(ShortForm))
         @item(@link(LongForm))
         @item(@link(Explanation))
       )  }
    procedure  Setup(const   chShortForm:  Char;
                     const   sLongForm:  string;
                     const   sExplanation:  string;
                     const   nValue:  Integer = 0);
  end { TGSkIntegerOption };


  {: @abstract(Opcja z wartością będącą liczbą rzeczywistą.)

     @seeAlso(TGSkOptionParser)  }
  TGSkFloatOption = class(TGSkCustomValueOption<Extended>)
  strict protected
    function  CheckValue(const   sValue:  string)
                         : Boolean;                                  override;
    function  GetValueV() : Variant;                                 override;
    procedure  SetValueV(const   vValue:  Variant);                  override;
  public

    {: @abstract Wygodne definiowanie podstawowych cech opcji.

       Metoda @name przypisuje wartości następującym właściwościom opcji:
       @unorderedList(@itemSpacing(Compact)
         @item(@link(ShortForm))
         @item(@link(LongForm))
         @item(@link(Explanation))
       )  }
    procedure  Setup(const   chShortForm:  Char;
                     const   sLongForm:  string;
                     const   sExplanation:  string;
                     const   nValue:  Extended = 0.0);
  end { TGSkFloatOption };


  {: @abstract(Opcja z wartością będącą napisem.)

     @seeAlso(TGSkOptionParser)  }
  TGSkStringOption = class(TGSkCustomValueOption<string>)
  strict protected
    function  CheckValue(const   sValue:  string)
                         : Boolean;                                  override;
    function  GetValueV() : Variant;                                 override;
    procedure  SetValueV(const  vValue:  Variant);                   override;
  public

    {: @abstract Wygodne definiowanie podstawowych cech opcji.

       Metoda @name przypisuje wartości następującym właściwościom opcji:
       @unorderedList(@itemSpacing(Compact)
         @item(@link(ShortForm))
         @item(@link(LongForm))
         @item(@link(Explanation))
       )  }
    procedure  Setup(const   chShortForm:  Char;
                     const   sLongForm:  string;
                     const   sExplanation:  string;
                     const   sValue:  string = '');
  end { TGSkStringOption };


  {: @abstract(Opcja z listą napisów.)

     Akceptuje wiele napisów. Nawet jeżeli ta opcja występuje wielokrotnie
     (oczywiście chodzi o nazwę opcji), to wszystkie napisy będą traktowane tak,
     jakby były zapisane w jednej opcji.

     @seeAlso(TGSkOptionParser)
     @seeAlso(TGSkPathListOption)  }
  TGSkStringListOption = class(TGSkCustomValueOption<TStringList>)
  strict private
    var
      fbDupIgnore:      Boolean;
      fbCaseSensitive:  Boolean;
    procedure  CheckDuplicates(Sender:  TObject);
  strict protected
    function  CheckValue(const   sValue:  string)
                         : Boolean;                                  override;
    function  GetValueV() : Variant;                                 override;
    procedure  SetValueV(const   vValue:  Variant);                  override;
  public
    constructor  Create(const   chShort:  Char;
                        const   sLong:  string;
                        const   sExplanation:  string = '';
                        const   bShortCaseSensitive:  Boolean = False;
                        const   bLongCaseSensitive:  Boolean = False); override;
    destructor  Destroy();                                           override;

    {: @abstract Wygodne definiowanie podstawowych cech opcji.

       Metoda @name przypisuje wartości następującym właściwościom opcji:
       @unorderedList(@itemSpacing(Compact)
         @item(@link(ShortForm))
         @item(@link(LongForm))
         @item(@link(Explanation))
       )  }
    procedure  Setup(const   chShortForm:  Char;
                     const   sLongForm:  string;
                     const   sExplanation:  string;
                     const   ValuesList:  TStrings = nil);

    {--- properties ---}

    {: @abstract Czy na liście wartości mogą się powtarzać.

       Domyślnie właściwość @name ma wartość @true. Oznacza to, że jeżeli jakiś
       napis powtarza się, zostanie zignorowany. Tylko unikalne napisy będą
       wartością tej opcji.

       @seeAlso(CaseSensitive)  }
    property  IgnoreDuplicates:  Boolean
              read  fbDupIgnore
              write fbDupIgnore
              default True;
    {: @abstract Czy wielkość liter w napisach ma znaczenie.

       Właściwość @name ma znaczenie tylko wtedy, gdy właściwość
       @link(IgnoreDuplicates) ma wartość @true.

       Domyślnie wielkość liter w napisach nie ma znaczenia.  }
    property  CaseSensitive:  Boolean
              read  fbCaseSensitive
              write fbCaseSensitive
              default False;
  end { TGSkStringListOption };


  {: @abstract(Opcja z listą ścieżek.)

     Opcja podobna do @link(TGSkStringListOption), ale akceptuje ścieżki do plików
     lub folderów. Poszczególne ścieżki powinny być rozdzielone przez
     @code(PathSep) (w systemie Windows jest to średnik).

     @seeAlso(TGSkOptionParser)  }
  TGSkPathListOption = class(TGSkStringListOption)
  strict protected
    function  CheckValue(const   sValue:  string)
                         : Boolean;                                  override;
  end { TGSkPathListOption };


  {: @abstract(Opcja pozwalająca definiować zbiory wartości.)

     Wartości nie mogą zawierać znaku „+” lub „-” ponieważ są one używane do
     dodawania lub usuwania wartości ze zbioru wartości. Użycie wartości bez „+”
     lub „-” na początki spowoduje usunięcie wartości domyślnych --- wartością
     opcji będą wyłącznie wartości jawnie wskazane w linii poleceń.

     Elementy zbioru można definiować oddzielnie lub łączyć w jednej opcji.
     Na przykład @code(--send+confirm --send+CanAbort) można zdefiniować również
     jako @code(--send+confirm+CanAbort).

     Elementy zbioru opcjonalnie mogą być rozdzielone przecinkami.

     @seeAlso(TGSkOptionParser)  }
  TGSkSetOption = class(TGSkCustomValueOption<string>)
  strict private
    var
      fPossibleValues, fValues:  TStringList;
  strict protected
    function  GetPossibleValues() : string;
    function  CheckValue(const   sValue:  string)
                         : Boolean;                                  override;
    function  GetValueV() : Variant;                                 override;
    function  GetValue() : string;                                   override;
    procedure  SetPossibleValues(const   Value:  string);
    procedure  SetValueV(const   vValue:  Variant);                  override;
    procedure  SetValue(const   Value:  string);                     override;
  public
    constructor  Create(const   chShort:  Char;
                        const   sLong:  string;
                        const   sExplanation:  string = '';
                        const   bShortCaseSensitive:  Boolean = False;
                        const   bLongCaseSensitive:  Boolean = False); override;
    destructor  Destroy();                                           override;
    {-}

    {: @abstract Wygodne definiowanie podstawowych cech opcji.

       Metoda @name przypisuje wartości następującym właściwościom opcji:
       @unorderedList(@itemSpacing(Compact)
         @item(@link(ShortForm))
         @item(@link(LongForm))
         @item(@link(Explanation))
         @item(@code(ValueList) --- jeżeli nie jest @nil)
       )  }
    procedure  Setup(const   chShortForm:  Char;
                     const   sLongForm:  string;
                     const   sExplanation:  string;
                     const   ValuesList:  TStrings = nil);

    function  HasValue(const   sValue:  string)
                       : Boolean;

    {--- properties ---}

    {: Lista dozwolonych wartości -- wyrazy rozdzielone przecinkami. }
    property  PossibleValues:  string
              read  GetPossibleValues
              write SetPossibleValues;
  end { TGSkSetOption };


  {: @abstract(Parser opcji z linii poleceń.)
     Klasa @className jest główną klasą analizującą opcje z wiersza poleceń. }
  TGSkOptionParser = class
  strict private
    var
      fParams:             TStringList;
      fOptions:            TObjectList<TGSkCustomOption>;
      fLeftList:           TStringList;
      chShortOptionChar:   Char;
      fsLongOptionString:  string;
    function  GetFreeOptionsOnDestroy() : Boolean;                   inline;
    procedure  SetFreeOptionsOnDestroy(const   bValue:  Boolean);    inline;
  strict protected
    function  GetOption(const   nIndex:  Integer)
                        : TGSkCustomOption;
    function  GetOptionsCount() : Integer;
    function  GetOptionByLongName(const   sName:  string)
                                  : TGSkCustomOption;
    function  GetOptionByShortName(const   chName:  Char)
                                   : TGSkCustomOption;
  public
    {: @abstract(Analizuje opcje z wiersza poleceń bieżącej aplikacji.) }
    constructor  Create();                                           virtual;

    {: @abstract(Analizuje opcje we wskazanych napisach.) }
    constructor  CreateParams(const   Params:  TStrings);            overload; virtual;

    {: @abstract(Analizuje opcje we wskazanych napisach.) }
    constructor  CreateParams(const   Params:  TArray<string>);      overload; virtual;

    {: @exclude }
    destructor  Destroy();                                           override;

    {: @abstract(Dodaje definicję opcji do listy analizowanych opcji.)

       @bold(Uwaga!) @br
       Klasa @className automatycznie zwalnia obiekty dodane przez metodę @name.  }
    function  AddOption(const   Option:  TGSkCustomOption)
                        : TGSkCustomOption;                          overload;

    {: @abstract(Dodaje definicję opcji do listy analizowanych opcji.)

       @bold(Uwaga!) @br
       Klasa @className automatycznie zwalnia obiekty dodane przez metodę @name.

       Przykład użycia: @longCode(#
         fOptHelp := AddOption(TGSkBoolOption, '?', 'help', 'Display program usage')
                       as TGSkBoolOption; #)  }
    function  AddOption(const   OptionClass:  TGSkCustomOptionClass;
                        const   chShortForm:  Char;
                        const   sLongForm:  string;
                        const   sExplanation:  string)
                        : TGSkCustomOption;                          overload;

    {: @abstract(Zwraca opis opcji.)

       Metoda @name zwraca tekst zawierający sformatowaną listę rozpoznawanych
       opcji, ich format oraz opis.

       @seeAlso(WriteExplanations)  }
    function  GetExplanations() : string;

    {: Właściwa analiza opcji. }
    procedure  ParseOptions();

    {: @abstract(Wyświetla opis opcji.)

       Metoda @name może być wykorzystywana w trybie znakowym do wyświetlenia
       listy rozpoznawanych opcji, ich format oraz opis.

       @seeAlso(GetExplanations)  }
    procedure  WriteExplanations();

    {--- properties ---}

    {: @abstract(Tekst nie będący opcjami.)

       Po @link(ParseOptions przeprowadzeniu analizy opcji) właściwość @name
       zawiera fragmenty tekstów nie rozpoznane jako opcje lub wartości opcji. }
    property  LeftList:  TStringList
              read  fLeftList;

    {: Liczba rozpoznawanych opcji. }
    property  OptionCount:  Integer
              read  GetOptionsCount;

    {: Tablica rozpoznawanych opcji. }
    property  Options[const   nIndex:  Integer] : TGSkCustomOption
              read  GetOption;

    {: @abstract(Zwraca opcję o wskazanej długiej nazwie.)

       Wrażliwość opcji na wielkość liter jest brana pod uwagę. }
    property  ByName[const   sName:  string] : TGSkCustomOption
              read  GetOptionByLongName;

    {: @abstract(Zwraca opcję o wskazanej krótkiej nazwie.)

       Wrażliwość opcji na wielkość liter jest brana pod uwagę. }
    property  ByShortName[const   chName:  Char] : TGSkCustomOption
              read  GetOptionByShortname;

    {: @abstract(Znak, który poprzedza krótkie opcje.)
       @seeAlso(gcchDefShortOptionChar) }
    property  ShortOptionStart:  Char
              read  chShortOptionChar
              write chShortOptionChar
              default gcchDefShortOptionChar;

    {: @abstract(Napis, który poprzedza długie opcje.)
       @seeAlso(gcsDefLongOptionString) }
    property  LongOptionStart:  string
              read  fsLongOptionString
              write fsLongOptionString;

    {: @abstract(Czy destruktor ma zwalniać również opcje)

       Domyślnie opcje zą zwalniane przez destruktor. }
    property  FreeOptionsOnDestroy:  Boolean
              read  GetFreeOptionsOnDestroy
              write SetFreeOptionsOnDestroy;
  end { TGSkOptionParser };


implementation


uses
  GSkPasLib.PasLib;


{$REGION 'TGSkOptionParser'}

function  TGSkOptionParser.AddOption(const   Option:  TGSkCustomOption)
                                     : TGSkCustomOption;
begin
  fOptions.Add(Option);
  Result := Option;
  Option.fParser := Self
end { AddOption };


function  TGSkOptionParser.AddOption(const   OptionClass:  TGSkCustomOptionClass;
                                     const   chShortForm:  Char;
                                     const   sLongForm, sExplanation:  string)
                                     : TGSkCustomOption;
begin
  Result := AddOption(OptionClass.Create(chShortForm, sLongForm, sExplanation))
end { AddOption };


constructor  TGSkOptionParser.Create();
begin
  CreateParams(nil);
end { Create };


constructor  TGSkOptionParser.CreateParams(const   Params:  TArray<string>);

var
  lParams:  TStringList;
  sParam:   string;

begin  { CreateParams }
  lParams := TStringList.Create();
  try
    for  sParam in Params  do
      lParams.Add(sParam);
    CreateParams(lParams)
  finally
    lParams.Free()
  end { try-finally }
end { CreateParams };


constructor  TGSkOptionParser.CreateParams(const   Params:  TStrings);

var
  nInd:  Integer;

begin  { CreateParams }
  inherited Create();
  fParams := TStringList.Create();
  if  Assigned(Params)  then
    fParams.Assign(Params)
  else
    for  nInd := 1  to  ParamCount  do
      fParams.Add(ParamStr(nInd));
  {-}
  fLeftList := TStringList.Create();
  fOptions := TObjectList<TGSkCustomOption>.Create();
  {-}
  fsLongOptionString := gcsDefLongOptionString;
  chShortOptionChar := gcchDefShortOptionChar
end { CreateParams };


destructor  TGSkOptionParser.Destroy();
begin
  fLeftList.Free();
  fParams.Free();
  fOptions.Free();
  inherited
end { Destroy };


procedure  TGSkOptionParser.ParseOptions();

var
  lCopyList:  TStringList;
  nInd:  Integer;
  bFoundSomething:  Boolean;

begin  { ParseOptions }
  lCopyList := TStringList.Create();
  lCopyList.Assign(fParams);
  fLeftList.Clear();
  try
    while  lCopyList.Count > 0  do
      begin
        bFoundSomething := False;
        for  nInd := 0  to  fOptions.Count-1  do
          if  fOptions[nInd].ParseOption(lCopyList)  then
            begin
              bFoundSomething := True;
              Break;
            end { if };
        if  not bFoundSomething  then
          begin
            fLeftList.Add(lCopyList[0]);
            lCopyList.Delete(0)
          end { not bFoundSomething }
      end { while lCopyList.Count > 0 }
  finally
    lCopyList.Free()
  end { try-finally }
end { ParseOptions };


procedure  TGSkOptionParser.SetFreeOptionsOnDestroy(const   bValue:  Boolean);
begin
  fOptions.OwnsObjects := bValue
end { SetFreeOptionsOnDestroy };


function  TGSkOptionParser.GetOptionsCount() : Integer;
begin
  Result := fOptions.Count
end { GetOptionsCount };


function  TGSkOptionParser.GetExplanations() : string;

var
  nInd:  Integer;
  nMaxWidth:  Integer;

begin  { GetExplanations }
  nMaxWidth := 0;
  for  nInd := OptionCount - 1  downto  0  do
    nMaxWidth := Max(nMaxWidth, Options[nInd].GetOptionWidth());
  Result := '';
  for  nInd := 0  to  OptionCount - 1  do
    with  Options[nInd]  do
      if  not Hidden  then
        Result := Result + GetExplanation(nMaxWidth)
end { GetExplanations };


function  TGSkOptionParser.GetFreeOptionsOnDestroy() : Boolean;
begin
  Result := fOptions.OwnsObjects
end { GetFreeOptionsOnDestroy };


function  TGSkOptionParser.GetOption(const   nIndex:  Integer)
                                     : TGSkCustomOption;
begin
  Result := fOptions[nIndex]
end { GetOption };


procedure  TGSkOptionParser.WriteExplanations();

var
  nInd:  Integer;
  nMaxWidth:  Integer;

begin  { WriteExplanations }
  nMaxWidth := 0;
  for  nInd := OptionCount - 1  downto  0  do
    nMaxWidth := Max(nMaxWidth, Options[nInd].GetOptionWidth());
  for  nInd := 0  to  OptionCount - 1  do
    with  Options[nInd]  do
      if  not Hidden  then
        WriteExplanation(nMaxWidth)
end { WriteExplanations };


function  TGSkOptionParser.GetOptionByLongName(const   sName:  string)
                                               : TGSkCustomOption;
var
  nInd:  Integer;

begin  { GetOptionByLongName }
  Result := nil;
  for  nInd := GetOptionsCount - 1  downto  0  do
    if  (Options[nInd].LongForm = sName)
        or (Options[nInd].LongCaseSensitive
            and (LowerCase(Options[nInd].LongForm) = LowerCase(sName)))  then
      begin
        Result := Options[nInd];
        Break
      end
end { GetOptionByLongName };


function  TGSkOptionParser.GetOptionByShortName(const   chName:  Char)
                                                : TGSkCustomOption;
var
  nInd:  Integer;

begin  { GetOptionByShortname }
  Result := nil;
  for  nInd := GetOptionsCount - 1  downto  0  do
    if  (Options[nInd].ShortForm = chName)
        or (Options[nInd].LongCaseSensitive
            and (LowerCase(Options[nInd].ShortForm) = LowerCase(chName)))  then
      begin
        Result := Options[nInd];
        Break
      end
end { GetOptionByShortname };

{$ENDREGION}


{$REGION 'TGSkCustomOption'}

constructor  TGSkCustomOption.Create(const   chShort:  Char;
                                     const   sLong:  string;
                                     const   sExplanation:  string;
                                     const   bShortCaseSensitive,
                                             bLongCaseSensitive:  Boolean);
begin
  inherited Create();
  fchShort := chShort;
  fsLong := sLong;
  fsExplanation := sExplanation;
  fbShortSens := bShortCaseSensitive;
  fbLongSens := bLongCaseSensitive
end { Create };


function  TGSkCustomOption.FormatExplanation(const   nOptWidth:  Integer;
                                             const   bWrapText:  Boolean)
                                             : string;
var
  nWritten:  Integer;
  lLines:  TStringList;
  nInd:  Integer;

begin  { FormatExplanation }
  Result := '  ';
  nWritten := 2;
  if  ShortForm <> #0  then
    begin
      Result := Result + fParser.ShortOptionStart + ShortForm;
      Inc(nWritten, 2);
      if  Length(LongForm) > 0  then
        begin
          Result := Result + ', ';
          Inc(nWritten, 2);
        end { Length(LongForm) > 0 }
    end { ShortForm <> #0 };
  if  Length(LongForm) > 0  then
    begin
      Result := Result + fParser.LongOptionStart + LongForm;
      Inc(nWritten,
          Length(fParser.LongOptionStart) + Length(LongForm))
    end { Length(LongForm) > 0 };
  Result := Result + ' --> ';
  Inc(nWritten, 5);
  if  bWrapText  then
    begin
      lLines := TStringList.Create();
      try
        lLines.Text := WrapText(Explanation, 77 - nOptWidth);
        for  nInd := 0  to  lLines.Count - 1  do
          begin
            if  Length(lLines[nInd]) > 0  then
              begin
                if  nInd = 0  then
                  Result := Result + StringOfChar(' ', nOptWidth + 4 - nWritten)
                else
                  Result := Result + StringOfChar(' ', nOptWidth + 4)
              end { Length(...) > 0 };
            Result := Result + lLines[nInd] + sLineBreak
          end { for nInd };
      finally
        lLines.Free()
      end { try-finally }
    end { if bWrapText }
  else
    Result := Result + Explanation + sLineBreak
end { FormatExplanation };


function  TGSkCustomOption.GetExplanation(const   nOptWidth:  Integer)
                                          : string;
begin
  Result := FormatExplanation(nOptWidth, False)
end { GetExplanation };


function  TGSkCustomOption.GetOptionWidth() : Integer;
begin
  Result := 0;
  if  ShortForm<>#0  then
    Inc(Result, 4); // "-x, "
  if  Length(LongForm) > 0  then
    Inc(Result,
        Length(LongForm) + Length(fParser.LongOptionStart))
  else
    Dec(Result, 2)
end { GetOptionWidth };


procedure  TGSkCustomOption.Setup(const   chShortForm:  Char;
                                  const   sLongForm, sExplanation:  string);
begin
  ShortForm := chShortForm;
  LongForm := sLongForm;
  Explanation := sExplanation
end { Setup };


procedure  TGSkCustomOption.WriteExplanation(const   nOptWidth:  Integer);
begin
  Write(FormatExplanation(nOptWidth, True))
end { WriteExplanation };

{$ENDREGION}


{$REGION 'TGSkBoolOption'}

function  TGSkBoolOption.GetValueV() : Variant;
begin
  Result := WasSpecified
end { GetValue };


function  TGSkBoolOption.ParseOption(const   Words:  TStrings)
                                     : Boolean;
var
  fnCmp:   function(const   sVal1, sVal2:  string)
                    : Boolean;
begin  { ParseOption }
  Result := False;
  if  ShortForm <> #0  then
    begin
      {$IFDEF UNICODE}
        if  ShortCaseSensitive  then
          fnCmp := SameStr
        else
          fnCmp := SameText;
      {$ELSE}
        if  ShortCaseSensitive  then
          fnCmp := AnsiSameStr
        else
          fnCmp := AnsiSameText;
      {$ENDIF -UNICODE}
      if  fnCmp(Words[0],
                fParser.ShortOptionStart + ShortForm)  then
        begin
          Result := True;
          Words.Delete(0);
          fbWasSpecified := True
        end
    end { ShortForm <> #0 };
  {-}
  if  not Result
      and (Length(LongForm) > 0)  then
    begin
      {$IFDEF UNICODE}
        if  LongCaseSensitive  then
          fnCmp := SameStr
        else
          fnCmp := SameText;
      {$ELSE}
        if  LongCaseSensitive  then
          fnCmp := AnsiSameStr
        else
          fnCmp := AnsiSameText;
      {$ENDIF -UNICODE}
      if  fnCmp(Words[0],
                fParser.LongOptionStart + LongForm)  then
        begin
          Result := True;
          Words.Delete(0);
          fbWasSpecified := True
        end
    end
end { ParseOption };


procedure  TGSkBoolOption.SetValueV(const   vValue:  Variant);
begin
  // do nothing, this option can either be specified or not
end { SetValue };

{$ENDREGION}


{$REGION 'TGSkCustomValueOption'}

function  TGSkCustomValueOption<T>.ChkVal(const   Words:  TStrings;
                                          const   nOptLen:  Integer)
                                          : Boolean;
var
  sValue:  string;

begin  { ChkVal }
  sValue := Copy(Words[0], nOptLen + 1, MaxInt);
  if  sValue = ''  then
    begin
      if  Words.Count > 1  then
        begin
          sValue := Words[1];
          if  CheckValue(sValue)  then
            begin
              Result := True;
              Words.Delete(1);
              Words.Delete(0)
            end
          else
            begin
              Result := CheckValue('');
              if   Result  then
                Words.Delete(0)
            end
        end { Words.Count > 1 }
      else
        begin
          Result := CheckValue('');
          if  Result  then
            Words.Delete(0)
        end { Words.Count = 0 }
    end { sValue = '' }
  else
    begin
      if  sValue[1] = '='  then
        Delete(sValue, 1, 1);
      Result := CheckValue(sValue);
      if  Result  then
        Words.Delete(0)
    end { Length(Words[0]) > nOptLen }
end { ChkVal };


function  TGSkCustomValueOption<T>.GetValue() : T;
begin
  Result := fValue
end { TGSkCustomValueOption };


function  TGSkCustomValueOption<T>.ParseOption(const   Words:  TStrings)
                                               : Boolean;
var
  nLen:  Integer;
  fnCmp:   function(const   sVal1, sVal2:  string)
                    : Boolean;

begin  { ParseOption }
  Result := False;
  if  ShortForm <> #0  then
    begin
      {$IFDEF UNICODE}
        if  ShortCaseSensitive  then
          fnCmp := SameStr
        else
          fnCmp := SameText;
      {$ELSE}
        if  ShortCaseSensitive  then
          fnCmp := AnsiSameStr
        else
          fnCmp := AnsiSameText;
      {$ENDIF -UNICODE}
      nLen := Length(fParser.ShortOptionStart) + Length(ShortForm);
      if  fnCmp(Copy(Words[0], 1, nLen),
                fParser.ShortOptionStart + ShortForm)  then
        Result := ChkVal(Words, nLen)
    end { ShortForm <> #0 };
  if  not Result
      and (Length(LongForm) > 0)  then
    begin
      {$IFDEF UNICODE}
        if  LongCaseSensitive  then
          fnCmp := SameStr
        else
          fnCmp := SameText;
      {$ELSE}
        if  LongCaseSensitive  then
          fnCmp := AnsiSameStr
        else
          fnCmp := AnsiSameText;
      {$ENDIF -UNICODE}
      nLen := Length(fParser.LongOptionStart) + Length(LongForm);
      if  fnCmp(Copy(Words[0], 1, nLen),
                fParser.LongOptionStart + LongForm)  then
        Result := ChkVal(Words, nLen)
    end { if };
  if  Result  then
    fbWasSpecified := True
end { ParseOption };


procedure  TGSkCustomValueOption<T>.SetValue(const   Value:  T);
begin
  fValue := Value
end { TGSkCustomValueOption };

{$ENDREGION}


{$REGION 'TGSkIntegerOption'}

function  TGSkIntegerOption.CheckValue(const   sValue:  string)
                                       : Boolean;
var
  nValue:  Integer;

begin  { CheckValue }
  Result := TryStrToInt(sValue, nValue);
  if  Result  then
    Value := nValue
end { CheckValue };


function  TGSkIntegerOption.GetValueV() : Variant;
begin
  Result := Value
end { GetValue };


procedure  TGSkIntegerOption.Setup(const   chShortForm:  Char;
                                   const   sLongForm, sExplanation:  string;
                                   const   nValue:  Integer);
begin
  inherited Setup(chShortForm, sLongForm, sExplanation);
  Value := nValue
end { Setup };


procedure  TGSkIntegerOption.SetValueV(const   vValue:  Variant);
begin
  Value := vValue
end { SetValue };

{$ENDREGION}


{$REGION 'TGSkFloatOption'}

function  TGSkFloatOption.CheckValue(const   sValue:  string)
                                     : Boolean;
var
  nValue:  Extended;

begin  { CheckValue }
  Result := TryStrToFloat(StringReplace(sValue, '.',
                                        {$IF CompilerVersion < gcnCompilerVersionDelphiXE}
                                          DecimalSeparator,
                                        {$ELSE}
                                          FormatSettings.DecimalSeparator,
                                        {$IFEND}
                                        []),
                          nValue);
  if  Result  then
    Value := nValue
end { CheckValue };


function  TGSkFloatOption.GetValueV() : Variant;
begin
  Result := Value
end { GetValueV };


procedure  TGSkFloatOption.Setup(const   chShortForm:  Char;
                                 const   sLongForm, sExplanation:  string;
                                 const   nValue:  Extended);
begin
  inherited Setup(chShortForm, sLongForm, sExplanation);
  Value := nValue
end { Setup };


procedure  TGSkFloatOption.SetValueV(const   vValue:  Variant);
begin
  Value := vValue
end { SetValueV };

{$ENDREGION}


{$REGION 'TGSkStringOption'}

function  TGSkStringOption.CheckValue(const   sValue:  string)
                                      : Boolean;
begin
  Value := sValue;
  Result := True
end { CheckValue };


function  TGSkStringOption.GetValueV() : Variant;
begin
  Result := Value
end { GetValue };


procedure  TGSkStringOption.Setup(const   chShortForm:  Char;
                                  const   sLongForm, sExplanation, sValue:  string);
begin
  inherited Setup(chShortForm, sLongForm, sExplanation);
  Value := sValue
end { Setup };


procedure  TGSkStringOption.SetValueV(const   vValue:  Variant);
begin
  Value := vValue
end { SetValue };

{$ENDREGION}


{$REGION 'TGSkStringListOption'}

function  TGSkStringListOption.CheckValue(const   sValue:  string)
                                          : Boolean;
begin
  Result := True;
  Value.Add(sValue)
end { CheckValue };


constructor  TGSkStringListOption.Create(const   chShort:  Char;
                                         const   sLong:  string;
                                         const   sExplanation:  string;
                                         const   bShortCaseSensitive,
                                                 bLongCaseSensitive:  Boolean);
begin
  inherited;
  Value := TStringList.Create();
  fbDupIgnore := True;
  fbCaseSensitive := False;
  Value.OnChange := CheckDuplicates;
end { Create };


destructor  TGSkStringListOption.Destroy();
begin
  Value.Free();
  inherited
end { Destroy };


function  TGSkStringListOption.GetValueV() : Variant;
begin
  Result := Value.Text
end { GetValue };


procedure  TGSkStringListOption.Setup(const   chShortForm:  Char;
                                      const   sLongForm, sExplanation:  string;
                                      const   ValuesList:  TStrings);
begin
  inherited Setup(chShortForm, sLongForm, sExplanation);
  if  ValuesList <> nil  then
    Value.Assign(ValuesList);
end { Setup };

procedure  TGSkStringListOption.SetValueV(const   vValue:  Variant);
begin
  Value.Text := vValue
end { SetValue };


procedure  TGSkStringListOption.CheckDuplicates(Sender:  TObject);

var
  nInd:  Integer;
  nChk:  Integer;
  sBuf:  string;
  fnCmp: function(const   sVal1, sVal2:  string)
                  : Boolean;

begin  { CheckDuplicates }
  if  fbDupIgnore  then
    try
      fbDupIgnore := False;   // zapobiega rekurencyjnemu wywołaniu, gdy usuwam duplikaty
      {$IFDEF UNICODE}
        if  fbCaseSensitive  then
          fnCmp := SameStr
        else
          fnCmp := SameText;
      {$ELSE}
        if  fbCaseSensitive  then
          fnCmp := AnsiSameStr
        else
          fnCmp := AnsiSameText;
      {$ENDIF -UNICODE}
      { Porównuję każdy napis ze wszystkimi jego poprzednikami }
      for  nInd := Value.Count - 1  downto  1  do
        begin
          sBuf := Value.Strings[nInd];
          for  nChk := nInd - 1  downto  0  do
            if  fnCmp(Value.Strings[nChk], sBuf)  then
              begin
                Value.Delete(nInd);   // usuwam późniejszy duplikat
                Break
              end { if fnCmp(...); for nChk }
        end { for nInd }
    finally
      fbDupIgnore := True
    end { try-finally; if fbDupIgnore }
end { CheckDuplicates };

{$ENDREGION}


{$REGION 'TGSkSetOption'}

function  TGSkSetOption.CheckValue(const   sValue:  string)
                                   : Boolean;
var
  lList, lResult:  TStringList;
  nInd:  Integer;
  sVal, sBuf:  string;
  chAct:  Char;
  si: Integer;

  function  NextIdx(const   chPfx:  Char;
                    const   sVal:  string)
                    : Integer;
  begin
    Result := Pos(chPfx, sVal);
    if  Result = 0  then
      Result := Length(sVal) + 1
  end { NextIdx };

begin  { CheckValue }
  Result := True;
  lList := TStringList.Create(dupIgnore, True, False);
  lResult := TStringList.Create();
  try
    lList.CommaText := sValue;
    lResult.Assign(fValues);   // default values
    lResult.Duplicates := dupIgnore;
    lResult.Sorted := True;
    nInd := lList.Count - 1;
    while  nInd >= 0  do
      begin
        sVal := lList[nInd];
        if  Length(sVal) = 0  then
          Continue;
        while  sVal <> ''  do
          begin
            chAct := sVal[1];
            if  CharInSet(chAct, ['+', '-'])  then
              Delete(sVal, 1, 1)
            else
              chAct := ' ';
            si := Min(NextIdx('+', sVal),
                      NextIdx('-', sVal));
            sBuf := Copy(sVal, 1, si - 1);
            Delete(sVal, 1, si - 1);
            case  chAct  of
              '+', ' ':
                begin
                  if  chAct <> '+'  then
                    lResult.Clear();
                  {-}
                  if  fPossibleValues.IndexOf(sBuf) >= 0  then
                    lResult.Add(sBuf)
                  else
                    begin
                      Result := False;
                      Break
                    end { fPossibleValues.IndexOf(sBuf) < 0 }
                end { '+', ' ' };
              '-':
                begin
                  if  fPossibleValues.IndexOf(sBuf) >= 0  then
                    begin
                      si := lResult.IndexOf(sBuf);
                      if  si >= 0  then
                        lResult.Delete(si)
                    end { fPossibleValues.IndexOf(sBuf) >= 0 }
                  else
                    begin
                      Result := False;
                      Break
                    end { fPossibleValues.IndexOf(sBuf) < 0 }
                end { '-' }
            end { case };
          end { while sVal <> '' };
        Dec(nInd)
      end { while nInd >= 0 }
  finally
    lList.Free();
    fValues.Assign(lResult);
    lResult.Free()
  end { try-finally }
end { CheckValue };


constructor  TGSkSetOption.Create(const   chShort:  Char;
                                  const   sLong:  string;
                                  const   sExplanation:  string;
                                  const   bShortCaseSensitive,
                                          bLongCaseSensitive:  Boolean);
begin
  inherited;
  fPossibleValues := TStringList.Create();
  fPossibleValues.Duplicates := dupIgnore;
  fPossibleValues.Sorted := True;
  fValues := TStringList.Create(dupIgnore, True, False)   // sorted, NOT case-sensitive
end { Create };


destructor  TGSkSetOption.Destroy();
begin
  fPossibleValues.Free();
  fValues.Free();
  inherited
end { Destroy };


function  TGSkSetOption.GetPossibleValues() : string;
begin
  Result := fPossibleValues.CommaText
end { GetPossibleValues };


function  TGSkSetOption.GetValueV() : Variant;
begin
  Result := FValues.CommaText
end { GetValue };


function  TGSkSetOption.GetValue() : string;
begin
  Result := fValues.CommaText
end { GetValues };


function  TGSkSetOption.HasValue(const   sValue:  string)
                                 : Boolean;
begin
  Result := fValues.IndexOf(sValue) >= 0
end { HasValue };


procedure  TGSkSetOption.SetPossibleValues(const   Value:  string);
begin
  fPossibleValues.CommaText := Value
end { SetPossibleValues };


procedure  TGSkSetOption.Setup(const   chShortForm:  Char;
                               const   sLongForm, sExplanation:  string;
                               const   ValuesList:  TStrings);
begin
  inherited Setup(chShortForm, sLongForm, sExplanation);
  if  ValuesList <> nil  then
    fValues.Assign(ValuesList);
end { Setup };


procedure  TGSkSetOption.SetValueV(const  vValue:  Variant);
begin
   fValues.CommaText := vValue
end { SetValue };


procedure  TGSkSetOption.SetValue(const   Value:  string);
begin
  fValues.CommaText := Value
end { SetValues };

{$ENDREGION}


{$REGION 'TGSkPathListOption'}

function  TGSkPathListOption.CheckValue(const   sValue:  string)
                                        : Boolean;
var
  lValues:  TStringList;

begin  { CheckValue }
  Result := True;
  lValues := TStringList.Create();
  try
    lValues.Text := StringReplace(sValue, PathSep, sLineBreak, [rfReplaceAll]);
    Value.AddStrings(lValues)
  finally
    lValues.Free()
  end { try-finally }
end { CheckValue };

{$ENDREGION}


end.

