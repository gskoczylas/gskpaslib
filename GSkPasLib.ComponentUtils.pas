﻿{: @abstract(Podprogramy związane z kontrolkami.)

   W tym module są podprogramy pomocnicze związane z kontrolkami.}
unit  GSkPasLib.ComponentUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22}  // XE
    System.Classes, System.Types, WinAPI.Windows, System.SysUtils, System.Math,
    System.UITypes,
    {$IFDEF FMX}
      FMX.Controls, FMX.StdCtrls, FMX.Graphics
    {$ELSE}
      Vcl.Controls, Vcl.ComCtrls, Vcl.Graphics, Vcl.StdCtrls
    {$ENDIF -FMX}
  {$ELSE}
    Classes, Controls, ComCtrls, Graphics, StdCtrls,
    Types, Windows, SysUtils, Math
  {$IFEND}
  {$IFNDEF FMX},
    JvLabel, JvHTControls
  {$ENDIF -FMX};


type
 {$IFDEF FMX}
  {: @exclude}
  TControlClass = class of TControl;
 {$ENDIF FMX}

 {$IFNDEF FMX}
  {: @abstract Typ proceduralny wykorzystywany w @link(ForEachComponent).

     Parametry takiego typu są wykorzystywane w procedurach
     @link(ForEachComponent).

     @param(Ctrl Kontrolka, w stosunku do której wykonywane jest działanie.)
     @param(pData Dodatkowy parametr przekazany w wywołaniu procedury
                  @link(ForEachComponent).)

     @seeAlso(TForEachControlProc)  }
  TForEachComponentProc = procedure(const   Comp:  TComponent;
                                    const   pData:  Pointer);

  {: @abstract Typ proceduralny wykorzystywany w @link(ForEachControl).

     Parametry takiego typu są wykorzystywane w procedurach
     @link(ForEachControl).

     @param(Ctrl Kontrolka, w stosunku do której wykonywane jest działanie.)
     @param(pData Dodatkowy parametr przekazany w wywołaniu procedury
                  @link(ForEachControl).)

     @seeAlso(TForEachComponentProc)  }
  TForEachControlProc = procedure(const   Ctrl:  TControl;
                                  const   pData:  Pointer);


{: @abstract  Zapewnia, że wskazana kontrolka jest widoczna.

   Procedura analizuje wszystkie kontrolki nadrzędne w stosunku do wskazanej
   kontrolki. Jeżeli któraś z nich jest komponentem typu @code(TPageControl)
   to przełącza ją na odpowiednią zakładkę  }
procedure  SetControlPage(edControl:  TControl);


{: @abstract Wykonuje wskazaną procedurę dla każdego kompontu.

   @param(Comp Procedura będzie wykonana w stosunku do tego komponentu oraz
               wszystkich komponentów podrzędnych.)
   @param(fnProc Procedura typu @link(TForEachComponentProc) wykonywana dla
                 komponentu @code(Comp) i komponaentów podrzędnych.)
   @param(Exclusions Tablica komponentów, które są pomijane podczas działania
                     procedury @code(ForEachComponent).)
   @param(BaseClass Bazowa klasa. Przetwarzane są tylko obiekty wskazanej klasy
                    lub klas potomnych.)
   @param(pData Dodatkowy parametr przekazywany do procedury @code(fnProc).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie komponenty podrzędne.)

   @seeAlso(ForEachControl)  }
procedure  ForEachComponent(const   Comp:  TComponent;
                            const   fnProc:  TForEachComponentProc;
                            const   Exclusions:  array of TComponent;
                            const   BaseClass:  TComponentClass;
                            const   pData:  Pointer = nil;
                            const   bRecurse:  Boolean = True);      overload;

{: @abstract Wykonuje wskazaną procedurę dla każdego kompontu.

   @param(Comp Procedura będzie wykonana w stosunku do tego komponentu oraz
               wszystkich komponentów podrzędnych.)
   @param(fnProc Procedura typu @link(TForEachComponentProc) wykonywana dla
                 komponentu @code(Comp) i komponaentów podrzędnych.)
   @param(BaseClass Bazowa klasa. Przetwarzane są tylko obiekty wskazanej klasy
                    lub klas potomnych.)
   @param(pData Dodatkowy parametr przekazywany do procedury @code(fnProc).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie komponenty podrzędne.)

   @seeAlso(ForEachControl)  }
procedure  ForEachComponent(const   Comp:  TComponent;
                            const   fnProc:  TForEachComponentProc;
                            const   BaseClass:  TComponentClass;
                            const   pData:  Pointer = nil;
                            const   bRecurse:  Boolean = True);      overload;

{: @abstract Wykonuje wskazaną procedurę dla każdego kompontu.

   @param(Comp Procedura będzie wykonana w stosunku do tego komponentu oraz
               wszystkich komponentów podrzędnych.)
   @param(fnProc Procedura typu @link(TForEachComponentProc) wykonywana dla
                 komponentu @code(Comp) i komponaentów podrzędnych.)
   @param(Exclusions Tablica komponentów, które są pomijane podczas działania
                     procedury @code(ForEachComponent).)
   @param(pData Dodatkowy parametr przekazywany do procedury @code(fnProc).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie komponenty podrzędne.)

   @seeAlso(ForEachControl)  }
procedure  ForEachComponent(const   Comp:  TComponent;
                            const   fnProc:  TForEachComponentProc;
                            const   Exclusions:  array of TComponent;
                            const   pData:  Pointer = nil;
                            const   bRecurse:  Boolean = True);      overload;

{: @abstract Wykonuje wskazaną procedurę dla każdego kompontu.

   @param(Comp Procedura będzie wykonana w stosunku do tego komponentu oraz
               wszystkich komponentów podrzędnych.)
   @param(fnProc Procedura typu @link(TForEachComponentProc) wykonywana dla
                 komponentu @code(Comp) i komponaentów podrzędnych.)
   @param(pData Dodatkowy parametr przekazywany do procedury @code(fnProc).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie komponenty podrzędne.)

   @seeAlso(ForEachControl)  }
procedure  ForEachComponent(const   Comp:  TComponent;
                            const   fnProc:  TForEachComponentProc;
                            const   pData:  Pointer = nil;
                            const   bRecurse:  Boolean = True);      overload;


{$IF CompilerVersion > 22}  // XE
procedure  ForEachControl(const   Ctrl:  TControl;
                          const   fnProc:  TProc<TControl, Pointer>;
                          const   Exclusions:  array of TControl;
                          const   BaseClass:  TControlClass;
                          const   pData:  Pointer = nil;
                          const   bRecurse:  Boolean = True);        overload;
{$IFEND}

{: @abstract Wykonuje wskazaną procedurę dla każdej kontrolki.

   @param(Ctrl Procedura będzie wykonana w stosunku do tej kontrolki oraz
               wszystkich kontrolek podrzędnych.)
   @param(fnProc Procedura typu @link(TForEachControlProc) wykonywana dla
                 kontrolki @code(Ctrl) i kontrolek podrzędnych.)
   @param(Exclusions Tablica kontrolek, które są pomijane podczas działania
                     procedury @code(ForEachControl).)
   @param(BaseClass Bazowa klasa. Przetwarzane są tylko obiekty wskazanej klasy
                    lub klas potomnych.)
   @param(pData Dodatkowy parametr przekazywany do procedury @code(fnProc).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie kontrolki podrzędne.)

   @seeAlso(ForEachComponent)  }
procedure  ForEachControl(const   Ctrl:  TControl;
                          const   fnProc:  TForEachControlProc;
                          const   Exclusions:  array of TControl;
                          const   BaseClass:  TControlClass;
                          const   pData:  Pointer = nil;
                          const   bRecurse:  Boolean = True);        overload;

{: @abstract Wykonuje wskazaną procedurę dla każdej kontrolki.

   @param(Ctrl Procedura będzie wykonana w stosunku do tej kontrolki oraz
               wszystkich kontrolek podrzędnych.)
   @param(fnProc Procedura typu @link(TForEachControlProc) wykonywana dla
                 kontrolki @code(Ctrl) i kontrolek podrzędnych.)
   @param(BaseClass Bazowa klasa. Przetwarzane są tylko obiekty wskazanej klasy
                    lub klas potomnych.)
   @param(pData Dodatkowy parametr przekazywany do procedury @code(fnProc).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie komponenty podrzędne.)

   @seeAlso(ForEachComponent)  }
procedure  ForEachControl(const   Ctrl:  TControl;
                          const   fnProc:  TForEachControlProc;
                          const   BaseClass:  TControlClass;
                          const   pData:  Pointer = nil;
                          const   bRecurse:  Boolean = True);        overload;

{: @abstract Wykonuje wskazaną procedurę dla każdej kontrolki.

   @param(Ctrl Procedura będzie wykonana w stosunku do tej kontrolki oraz
               wszystkich kontrolek podrzędnych.)
   @param(fnProc Procedura typu @link(TForEachControlProc) wykonywana dla
                 komponentu @code(Ctrl) i komponaentów podrzędnych.)
   @param(Exclusions Tablica komponentów, które są pomijane podczas działania
                     procedury @code(ForEachControl).)
   @param(pData Dodatkowy parametr przekazywany do procedury @code(fnProc).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie komponenty podrzędne.)

   @seeAlso(ForEachComponent)  }
procedure  ForEachControl(const   Ctrl:  TControl;
                          const   fnProc:  TForEachControlProc;
                          const   Exclusions:  array of TControl;
                          const   pData:  Pointer = nil;
                          const   bRecurse:  Boolean = True);        overload;

{: @abstract Wykonuje wskazaną procedurę dla każdej kontrolki.

   @param(Ctrl Procedura będzie wykonana w stosunku do tej kontrolki oraz
               wszystkich kontrolek podrzędnych.)
   @param(fnProc Procedura typu @link(TForEachControlProc) wykonywana dla
                 kontrolki @code(Ctrl) i komponaentów podrzędnych.)
   @param(pData Dodatkowy parametr przekazywany do procedury @code(fnProc).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie komponenty podrzędne.)  }
procedure  ForEachControl(const   Ctrl:  TControl;
                          const   fnProc:  TForEachControlProc;
                          const   pData:  Pointer = nil;
                          const   bRecurse:  Boolean = True);        overload;

{: @abstract Zmienia właściwość @code(Enabled) w każdej kontrolce

   Jest to wyspecjalizowana wersja procedury @link(ForEachComponent) zmieniająca
   wartość właściwości @code(Enaboled) wskazanej kontrolki oraz kontrolek
   podrzędnych.

   @param(Root Właściwość @code(Enabled) będzie zmieniona w tej kontrolce oraz
               we wszystkich kontrolkach podrzędnych.)
   @param(bEnable Nowa wartość właściwości @code(Enabled).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie komponenty podrzędne.)  }
procedure  EnableControls(const   Root:  TControl;
                          const   bEnable:  Boolean;
                          const   bRecurse:  Boolean = True);        overload;

{: @abstract Zmienia właściwość @code(Enabled) w każdej kontrolce

   Jest to wyspecjalizowana wersja procedury @link(ForEachComponent) zmieniająca
   wartość właściwości @code(Enaboled) wskazanej kontrolki oraz kontrolek
   podrzędnych.

   @param(Root Właściwość @code(Enabled) będzie zmieniona w tej kontrolce oraz
               we wszystkich kontrolkach podrzędnych.)
   @param(Exclusions Tablica kontrolek, które są pomijane podczas działania
                     procedury @code(EnableControls).)
   @param(bEnable Nowa wartość właściwości @code(Enabled).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie komponenty podrzędne.)
   @param(BaseClass Klasa bazowa. Właściwość @code(Enabled) będzie ustawiona
                    tylko dla obiektów takiej klasy lub klasy potomnej.) }
procedure  EnableControls(const   Root:  TControl;
                          const   Exclusions:  array of TControl;
                          const   bEnable:  Boolean;
                          const   bRecurse:  Boolean;
                          const   BaseClass:  TControlClass);        overload;

{: @abstract Zmienia właściwość @code(Enabled) w każdej kontrolce

   Jest to wyspecjalizowana wersja procedury @link(ForEachComponent) zmieniająca
   wartość właściwości @code(Enaboled) wskazanej kontrolki oraz kontrolek
   podrzędnych.

   @param(Root Właściwość @code(Enabled) będzie zmieniona w tej kontrolce oraz
               we wszystkich kontrolkach podrzędnych.)
   @param(Exclusions Tablica kontrolek, które są pomijane podczas działania
                     procedury @code(EnableControls).)
   @param(bEnable Nowa wartość właściwości @code(Enabled).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie komponenty podrzędne.)  }
procedure  EnableControls(const   Root:  TControl;
                          const   Exclusions:  array of TControl;
                          const   bEnable:  Boolean;
                          const   bRecurse:  Boolean = True);        overload;

{: @abstract Zmienia właściwość @code(Enabled) w każdej kontrolce

   Jest to wyspecjalizowana wersja procedury @link(ForEachComponent) zmieniająca
   wartość właściwości @code(Enaboled) wszystkich wskazanych kontrolek oraz
   kontrolek podrzędnych.

   @param(Controls Właściwość @code(Enabled) będzie zmieniona w każdej kontrolce
                   wskazanej w tej tablicy oraz w ich kontrolkach podrzędnych.)
   @param(bEnable Nowa wartość właściwości @code(Enabled).)
   @param(bRecurse Wskazuje czy przetwarzać rekurencyjnie komponenty podrzędne.)  }
procedure  EnableControls(const   Controls:  array of TControl;
                          const   bEnable:  Boolean);                overload;


{: @abstract Dopasowuje rozmiar czcionki do tekstu i miejsca.

   Procedura zmienia rozmiar czcionki tak aby tekst mieścił się we wskazanym
   obszarze.

   @param(AdjustFont Obszar, na krótym powinien zmieścić się tekst. Jednocześnie
                     definiuje parametry czcionki.)
   @param(sText Tekst, który powinien się zmieścić na obszarze wskazanym
                w parametrze @code(AdjustFont).)
   @param(nMaxWidth Maksymalna szerokość obszaru przeznaczonego dla tekstu
                    (w pikselach).)
   @param(nMaxHeight Maksymalna wysokość obszaru przeznaczonego dla tekstu
                     (w pikselach).)  }
procedure  AdjustFontSize(const   AdjustFont:  TCanvas;
                          const   sText:  string;
                          const   nMaxWidth, nMaxHeight:  Integer);  overload;

{: @abstract Dopasowuje rozmiar czcionki do tekstu i miejsca.

   Procedura zmienia rozmiar czcionki tak aby tekst mieścił się we wskazanym
   obszarze.

   @param(lbInfo Etykieta, w której tekst powinien się zmieścić w obszarze
                 etykiety.)
   @param(nMinFontSize Minimalny rozmiar czcionki.)
   @param(bCanWrap Wskazuje czy tekst musi się zmieścić w jednym wierszu.)  }
procedure  AdjustFontSize(const   lbInfo:  TLabel;
                          const   nMinFontSize:  Integer = 8;
                          const   bCanWrap:  Boolean = True);        overload;

{: @abstract Dopasowuje rozmiar czcionki do tekstu i miejsca.

   Procedura zmienia rozmiar czcionki tak aby tekst mieścił się we wskazanym
   obszarze. Jednocześnie w zależności od wielkości czcionki zmieniana jest
   wielkość cienia.

   @param(lbInfo Etykieta, w której tekst powinien się zmieścić w obszarze
                 etykiety.)
   @param(nMinFontSize Minimalny rozmiar czcionki.)
   @param(bCanWrap Wskazuje czy tekst musi się zmieścić w jednym wierszu.)  }
procedure  AdjustFontSize(const   lbInfo:  TJvLabel;
                          const   nMinFontSize:  Integer = 8;
                          const   bCanWrap:  Boolean = True);        overload;

procedure  AdjustFontSize(const   lbInfo:  TJvHTLabel;
                          const   nMinFontSize:  Integer = 8);       overload;

{: @abstract Przekształca czcionkę na napis.

   Funkcja @name przekształca czcionkę na napis. Może być wykorzystana do
   przechowania parametrów czcionki (np. w pliku konfiguracyjnym.)

   @param Font Czcionka podlegająca konwersji.

   @return Napis zawierający zakodowane parametry czcionki.

   @seeAlso(StringToFont)  }
function  FontToString(const   Font:  TFont)
                       : string;

{: @abstract Przekształca napis na czcionkę

   Procedura @name przekształca napis na parametry czcionki. Napis musi być
   zgodny z funkcją @link(FontToString).

   Funkcja @name może być wykorzytana do pobierania parametrów czcionki z pliku
   konfiguracyjnego.

   @param sFont Napis zawierający zakodowane parametry czcionki.
   @param Font  Wyniokowa czcionka.

   @seeAlso(FontToString)  }
procedure  StringToFont(const   sFont:  string;
                        const   Font:  TFont);

{$ENDIF -FMX}

{: @abstract Konwersja komponentu na napis.

   Funkcja @name dokonuje konwersji komponentu na napis. Jest ona napisana na
   podstawie przykładu @italic(WriteComponent, Seek, ObjectBinaryToText,
   DataString, ObjectTextToBinary, ReadComponent Example) z Pomocy dla Delphi 7.

   @seeAlso(StrToComponent)  }
function  ComponentToStr(const   Component:  TComponent)
                         : string;

{: @abstract Konwersja napisu na komponent

   Funkcja @name dokonuje konwersji napisu na komponent. Jest ona napisana na
   podstawie przykładu @italic(WriteComponent, Seek, ObjectBinaryToText,
   DataString, ObjectTextToBinary, ReadComponent Example) z Pomocy dla Delphi 7.

   @seeAlso(ComponentToStr)  }
function  StrToComponent(const   sComponent:  string)
                         : TComponent;

{$IFNDEF FMX}

{: @abstract Automatycznie dobierane szerokości kolumn.

   Funkcję @name można używać wtedy, gdy wskazany w parametrze komponent wyświetla
   dane kolumnach (właściwość @code(ViewStyle) równa @code(vsReport)).

   Aby dane szerokości kolumn były zawsze dobrze dobrane, procedurę @name należy
   wywoływać po każdym dopisaniu nowych danych, usunięciu danych lub zmodyfikowaniu
   danych wyświetlanych przez komponent.  }
procedure  ListViewColumnsAutoWidth(const   LstView:  TListView);

{$ENDIF -FMX}

procedure  SetTabOrders(const   Root:  {$IFDEF FMX}TControl{$ELSE}TWinControl{$ENDIF};
                        const   bVertically:  Boolean = False);


implementation


uses
  GSkPasLib.Tokens;


const
  cFontStyleCode:  packed array[TFontStyle] of  Char = (
                     'B',   // fsBold
                     'I',   // fsItalic
                     'U',   // fsUnderline
                     'S');  // fsStrikeOut

type
  THackedFont = class(TFont);


{$IFNDEF FMX}

procedure  SetControlPage(edControl:  TControl);

var
  ctlPage:  TControl;

begin  { SetControlPage }
  if  Assigned(edControl)  then
    begin
      ctlPage := edControl.Parent;
      while  Assigned(ctlPage)  do
        begin
          if  ctlPage is TTabSheet  then
            (ctlPage as TTabSheet).PageControl.ActivePage := (ctlPage as TTabSheet);
          ctlPage := ctlPage.Parent;
        end { while Assigned(ctlPage) }
    end { Assigned(edControl) }
end { SetControlPage };

{$ENDIF -FMX}


function  ProcessComponent(const   edComponent:  TComponent;
                           const   Exclusions:  array of TComponent;
                           const   BaseClass:  TClass)
                           : Boolean;
var
  nInd:  Integer;

begin  { ProcessComponent }
  Result := False;
  for  nInd := High(Exclusions)  downto  Low(Exclusions)  do
    if  Exclusions[nInd] = edComponent  then
      Exit;
  Result := edComponent is BaseClass
end { ProcessComponent };


{$IFNDEF FMX}

{$REGION 'ForEach'}

procedure  ForEachComponent(const   Comp:  TComponent;
                            const   fnProc:  TForEachComponentProc;
                            const   Exclusions:  array of TComponent;
                            const   BaseClass:  TComponentClass;
                            const   pData:  Pointer;
                            const   bRecurse:  Boolean);
var
  nInd:  integer;

begin  { ForEachComponent }
  if  ProcessComponent(Comp, Exclusions, BaseClass)  then
    fnProc(Comp, pData);
  if  BaseClass.InheritsFrom(TControl)
      and (Comp is TWinControl)  then
    with  Comp as TWinControl  do
      for  nInd := 0  to  ControlCount - 1  do
        begin
          if  bRecurse  then
            ForEachComponent(Controls[nInd], fnProc, Exclusions, BaseClass, pData)
          else if  ProcessComponent(Controls[nInd], Exclusions, BaseClass)  then
            fnProc(Controls[nInd], pData)
        end { for nInd }
  else
    with  Comp  do
      for  nInd := 0  to  ComponentCount - 1  do
        begin
          if  bRecurse  then
            ForEachComponent(Components[nInd], fnProc, Exclusions, BaseClass, pData)
          else if  ProcessComponent(Components[nInd], Exclusions, BaseClass)  then
            fnProc(Components[nInd], pData)
        end { for nInd }
end { ForEachComponent };


procedure  ForEachComponent(const   Comp:  TComponent;
                            const   fnProc:  TForEachComponentProc;
                            const   BaseClass:  TComponentClass;
                            const   pData:  Pointer;
                            const   bRecurse:  Boolean);
begin
  ForEachComponent(Comp, fnProc, [], BaseClass, pData, bRecurse)
end { ForEachComponent };


procedure  ForEachComponent(const   Comp:  TComponent;
                            const   fnProc:  TForEachComponentProc;
                            const   Exclusions:  array of TComponent;
                            const   pData:  Pointer;
                            const   bRecurse:  Boolean);
begin
  ForEachComponent(Comp, fnProc, Exclusions, TComponent, pData, bRecurse)
end { ForEachComponent };


procedure  ForEachComponent(const   Comp:  TComponent;
                            const   fnProc:  TForEachComponentProc;
                            const   pData:  Pointer;
                            const   bRecurse:  Boolean);
begin
  ForEachComponent(Comp, fnProc, [], TComponent, pData, bRecurse)
end { ForEachComponent };


function  ProcessControl(const   edControl:  TControl;
                         const   Exclusions:  array of TControl;
                         const   BaseClass:  TClass)
                         : Boolean;
var
  nInd:  Integer;

begin  { ProcessControl }
  Result := False;
  for  nInd := High(Exclusions)  downto  Low(Exclusions)  do
    if  Exclusions[nInd] = edControl  then
      Exit;
  Result := edControl is BaseClass
end { ProcessControl };


{$IF CompilerVersion > 22}  // XE

procedure  ForEachControl(const   Ctrl:  TControl;
                          const   fnProc:  TProc<TControl, Pointer>;
                          const   Exclusions:  array of TControl;
                          const   BaseClass:  TControlClass;
                          const   pData:  Pointer;
                          const   bRecurse:  Boolean);
var
  nInd:  integer;

begin  { ForEachControl }
  if  ProcessControl(Ctrl, Exclusions, BaseClass)  then
    fnProc(Ctrl, pData);
  if  BaseClass.InheritsFrom(TControl)
      and (Ctrl is TWinControl)  then
    with  Ctrl as TWinControl  do
      for  nInd := 0  to  ControlCount - 1  do
        begin
          if  bRecurse  then
            ForEachControl(Controls[nInd], fnProc, Exclusions, BaseClass, pData)
          else if  ProcessControl(Controls[nInd], Exclusions, BaseClass)  then
            fnProc(Controls[nInd], pData)
        end { for nInd }
end { ForEachControl };

{$IFEND}


procedure  ForEachControl(const   Ctrl:  TControl;
                          const   fnProc:  TForEachControlProc;
                          const   Exclusions:  array of TControl;
                          const   BaseClass:  TControlClass;
                          const   pData:  Pointer;
                          const   bRecurse:  Boolean);
{$IF CompilerVersion <= 22}  // 2007
var
  nInd:  Integer;
{$IFEND}

begin  { ForEachControl }
  {$IF CompilerVersion > 22}  // XE
    ForEachControl(Ctrl,
                   procedure(Ctrl:  TControl;
                             pData:  Pointer)
                     begin
                       fnProc(Ctrl, pData)
                     end,
                   Exclusions, BaseClass, pData, bRecurse)
  {$ELSE}
    if  ProcessControl(Ctrl, Exclusions, BaseClass)  then
      fnProc(Ctrl, pData);
    if  BaseClass.InheritsFrom(TControl)
        and (Ctrl is TWinControl)  then
      with  Ctrl as TWinControl  do
        for  nInd := 0  to  ControlCount - 1  do
          begin
            if  bRecurse  then
              ForEachControl(Controls[nInd], fnProc, Exclusions, BaseClass, pData)
            else if  ProcessControl(Controls[nInd], Exclusions, BaseClass)  then
              fnProc(Controls[nInd], pData)
          end { for nInd }
  {$IFEND}
end { ForEachControl };


procedure  ForEachControl(const   Ctrl:  TControl;
                          const   fnProc:  TForEachControlProc;
                          const   BaseClass:  TControlClass;
                          const   pData:  Pointer;
                          const   bRecurse:  Boolean);
begin
  ForEachControl(Ctrl, fnProc, [], BaseClass, pData, bRecurse)
end { ForEachControl };


procedure  ForEachControl(const   Ctrl:  TControl;
                          const   fnProc:  TForEachControlProc;
                          const   Exclusions:  array of TControl;
                          const   pData:  Pointer;
                          const   bRecurse:  Boolean);
begin
  ForEachControl(Ctrl, fnProc, Exclusions, TControl, pData, bRecurse)
end { ForEachControl };


procedure  ForEachControl(const   Ctrl:  TControl;
                          const   fnProc:  TForEachControlProc;
                          const   pData:  Pointer;
                          const   bRecurse:  Boolean);
begin
  ForEachControl(Ctrl, fnProc, [], TControl, pData, bRecurse)
end { ForEachControl };

{$ENDREGION 'ForEach'}


{$REGION 'EnableControls'}

procedure  SetControlEnabled(const   Ctrl:  TControl;
                             const   pData:  Pointer);
begin
  Ctrl.Enabled := PBoolean(pData)^
end { SetControlEnabled };


procedure  EnableControls(const   Root:  TControl;
                          const   bEnable:  Boolean;
                          const   bRecurse:  Boolean);
begin
  EnableControls(Root, [], bEnable, bRecurse)
end { EnableControls };


procedure  EnableControls(const   Root:  TControl;
                          const   Exclusions:  array of TControl;
                          const   bEnable:  Boolean;
                          const   bRecurse:  Boolean;
                          const   BaseClass:  TControlClass);
begin
  ForEachControl(Root, SetControlEnabled, Exclusions, BaseClass,
                 Addr(bEnable), bRecurse);
end { EnableControls };


procedure  EnableControls(const   Root:  TControl;
                          const   Exclusions:  array of TControl;
                          const   bEnable:  Boolean;
                          const   bRecurse:  Boolean);
begin
  EnableControls(Root, Exclusions, bEnable, bRecurse, TControl)
end { EnableControls };


procedure  EnableControls(const   Controls:  array of TControl;
                          const   bEnable:  Boolean);
var
  nInd:  Integer;

begin  { EnableControls }
  for  nInd := Low(Controls)  to  High(Controls)  do
    EnableControls(Controls[nInd], bEnable)
end { EnableControls };

{$ENDREGION 'EnableControls'}


{$REGION 'AdjustFontSize'}

procedure  AdjustFontSize(const   AdjustFont:  TCanvas;
                          const   sText:  string;
                          const   nMaxWidth, nMaxHeight:  Integer);
var
  Size:  TSize;

begin  { AdjustFontSize }
  with  AdjustFont  do
    begin
      Size := TextExtent(sText);
      while  ((Size.cx < nMaxWidth)
               or (Size.cy < nMaxHeight))
             and (Font.Size < 250)  do
        begin
          with  Font  do
            if  (Size = 16)
                and not (fsBold in Style)  then
              Style := Style + [fsBold]
            else
              Size := Size + 1;
          Font.Size := Font.Size + 1;
          Size := TextExtent(sText)
        end { while };
      while  ((Size.cx > nMaxWidth)
               or (Size.cy > nMaxHeight))
             and (Font.Size > 6)  do
        begin
          with  Font  do
            if  (Size = 16)
                and (fsBold in Style)  then
              Style := Style - [fsBold]
            else
              Size := Size - 1;
          Size := TextExtent(sText)
        end { while };
    end { with AdjustFont }
end { AdjustFontSize };


procedure  AdjustFontSize(const   lbInfo:  TLabel;
                          const   nMinFontSize:  Integer;
                          const   bCanWrap:  Boolean);
begin
  lbInfo.Canvas.Font := lbInfo.Font;
  AdjustFontSize(lbInfo.Canvas, lbInfo.Caption,
                 MulDiv(lbInfo.ClientWidth, 6, 7),
                 MulDiv(lbInfo.ClientHeight, 6, 7));
  if  lbInfo.Font.Size < nMinFontSize  then
    lbInfo.Font.Size := nMinFontSize;
  if  bCanWrap
      and (lbInfo.Canvas.TextWidth(lbInfo.Caption) > MulDiv(lbInfo.ClientWidth, 8, 10))  then
    lbInfo.WordWrap := True;
end { AdjustFontSize };


procedure  AdjustFontSize(const   lbInfo:  TJvLabel;
                          const   nMinFontSize:  Integer;
                          const   bCanWrap:  Boolean);
begin
  lbInfo.Canvas.Font := lbInfo.Font;
  AdjustFontSize(lbInfo.Canvas, lbInfo.Caption,
                 MulDiv(lbInfo.ClientWidth, 4, 5),
                 MulDiv(lbInfo.ClientHeight, 4, 5));
  if  lbInfo.Font.Size < nMinFontSize  then
    lbInfo.Font.Size := nMinFontSize;
  if  bCanWrap
      and (lbInfo.Canvas.TextWidth(lbInfo.Caption) > MulDiv(lbInfo.ClientWidth, 8, 10))  then
    lbInfo.WordWrap := True;
  if  lbInfo.ShadowSize > 0  then
    case  lbInfo.Font.Size  of
      1..13:  lbInfo.ShadowSize := 1;
      14..20: lbInfo.ShadowSize := 2;
      else    lbInfo.ShadowSize := 3
    end { case lbInfo.Font.Size };
end { AdjustFontSize };


procedure  AdjustFontSize(const   lbInfo:  TJvHTLabel;
                          const   nMinFontSize:  Integer);
begin
  lbInfo.Canvas.Font := lbInfo.Font;
  AdjustFontSize(lbInfo.Canvas, lbInfo.Caption,
                 MulDiv(lbInfo.ClientWidth, 6, 7),
                 MulDiv(lbInfo.ClientHeight, 6, 7));
  if  lbInfo.Font.Size < nMinFontSize  then
    lbInfo.Font.Size := nMinFontSize
end { AdjustFontSize };

{$ENDREGION 'AdjustFontSize'}


function  FontToString(const   Font:  TFont)
                       : string;

  function  FontStylesToString(const   Styles:  TFontStyles)
                               : string;
  var
    lInd:  TFontStyle;

  begin  { FontStylesToString }
    Result := '';
    for  lInd := High(TFontStyle)  downto  Low(TFontStyle)  do
      if  lInd in Styles  then
        Result := Result + cFontStyleCode[lInd]
  end { FontStylesToString };

begin  { FontToString }
  with  Font  do
    Result := Format('%s,%d,%s,%d,%s,%d',
                     [Name, Size,
                      FontStylesToString(Style),
                      Ord(Pitch),
                      ColorToString(Color),
                      Charset])
end { FontToString };


procedure  StringToFont(const   sFont:  string;
                        const   Font:  TFont);
var
  nInd:  Integer;
  sBuf, sSaveFont:  string;
  lSaveOnChange:  TNotifyEvent;

  function  StringToFontStyles(const   sStyles:  string)
                               : TFontStyles;
  var
    lInd:  TFontStyle;

  begin  { StringToFontStyles }
    Result := [];
    for  lInd := High(TFontStyle)  downto  Low(TFontStyle)  do
      if  Pos(cFontStyleCode[lInd], sStyles) > 0  then
        Include(Result, lInd)
  end { StringToFontStyles };

begin  { StringToFont }
  lSaveOnChange := Font.OnChange;
  Font.OnChange := nil;
  sSaveFont := FontToString(Font);
  try
    with  TGSkCharTokens.Create(nil)  do
      try
        SetDelimiter(',');
        Text := sFont;
        for  nInd := Count - 1  downto  0  do
          begin
            sBuf := Token[nInd];
            case  nInd  of
              0:  Font.Name := sBuf;
              1:  Font.Size := StrToIntDef(sBuf, Font.Size);
              2:  Font.Style := StringToFontStyles(sBuf);
              3:  Font.Pitch := TFontPitch(StrToIntDef(sBuf, Ord(Font.Pitch)));
              4:  Font.Color := StringToColor(sBuf);
              5:  Font.Charset := StrToIntDef(sBuf, Font.Charset)
            end { case nInd }
          end { for nInd }
      finally
        Free()
      end { try-finally }
  finally
    Font.OnChange := lSaveOnChange;
    if  sSaveFont <> FontToString(Font)  then
      THackedFont(Font).Changed()
  end { try-finally }
end { StringToFont };

{$ENDIF -FMX}


function  ComponentToStr(const   Component:  TComponent)
                         : string;
var
  lBinStream:  TMemoryStream;
  lStrStream:  TStringStream;

begin  { ComponentToStr }
  lBinStream := TMemoryStream.Create();
  try
    lStrStream := TStringStream.Create('');
    try
      lBinStream.WriteComponent(Component);
      lBinStream.Seek(0, soFromBeginning);
      ObjectBinaryToText(lBinStream, lStrStream);
      Result := lStrStream.DataString
    finally
      lStrStream.Free()
    end { try-finally }
  finally
    lBinStream.Free()
  end { try-finally }
end { ComponentToStr };


function  StrToComponent(const   sComponent:  string)
                         : TComponent;
var
  lStrStream:  TStringStream;
  lBinStream:  TMemoryStream;

begin  { StrToComponent }
  lStrStream := TStringStream.Create(sComponent);
  try
    lBinStream := TMemoryStream.Create();
    try
      ObjectTextToBinary(lStrStream, lBinStream);
      lBinStream.Seek(0, soFromBeginning);
      Result := lBinStream.ReadComponent(nil)
    finally
      lBinStream.Free()
    end { try-finally }
  finally
    lStrStream.Free()
  end { try-finally }
end { StrToComponent };


{$IFNDEF FMX}

procedure  ListViewColumnsAutoWidth(const   LstView:  TListView);

var
  lColW:  array of Integer;
  nIndT:  Integer;
  nIndC:  Integer;
  nCntC:  Integer;
  lCnvs:  TCanvas;
  nWidth: Integer;

begin  { ListViewColumnsAutoWidth }
  Assert(LstView.ViewStyle = vsReport);
  nCntC := LstView.Columns.Count;
  lCnvs := LstView.Canvas;
  SetLength(lColW, nCntC);
  for  nIndC := 0  to  High(lColW)  do
    lColW[nIndC] := 0;
  for  nIndT := LstView.Items.Count - 1  downto  0  do
    with  LstView.Items.Item[nIndT]  do
      begin
        nWidth := lCnvs.TextWidth(Caption);
        if  LstView.SmallImages <> nil  then
          Inc(nWidth, LstView.SmallImages.Width);
        if  (StateIndex >= 0)
            and (LstView.StateImages <> nil)  then
          Inc(nWidth, LstView.StateImages.Width);
        lColW[0] := Max(lColW[0], nWidth);
        for  nIndC := 1  to  Min(nCntC - 1, SubItems.Count)  do
          lColW[nIndC] := Max(lColW[nIndC],
                              lCnvs.TextWidth(SubItems.Strings[Pred(nIndC)]))
      end { with Item[nIndT]; for nIndT };
  with  LstView.Columns  do
    for  nIndC := Count - 1  downto  0  do
      with  Items[nIndC]  do
        begin
          nWidth := lCnvs.TextWidth(Caption);
          if  (ImageIndex >= 0)
              and (LstView.SmallImages <> nil)  then
            Inc(nWidth, LstView.SmallImages.Width);
          if  nWidth > lColW[nIndC]  then
            Width := ColumnHeaderWidth
          else
            Width := ColumnTextWidth
        end { with Items[]; for nIndC; with LstView.Columns }
end { ListViewColumnsAutoWidth };

{$ENDIF -FMX}


{$REGION 'SetTabOrders'}

function  SortTabOrdersHorizontally(Item1, Item2:  Pointer)
                                    : Integer;
begin
  {$IFDEF FMX}
    Result := CompareValue(TControl(Item1).Position.Y, TControl(Item2).Position.Y);
    if  Result = EqualsValue  then
      Result := CompareValue(TControl(Item1).Position.X, TControl(Item2).Position.X)
  {$ELSE}
    Result := CompareValue(TWinControl(Item1).Top, TWinControl(Item2).Top);
    if  Result = EqualsValue  then
      Result := CompareValue(TWinControl(Item1).Left, TWinControl(Item2).Left)
  {$ENDIF -FMX}
end { SortTabOrdersHorizontally };


function  SortTabOrdersVertically(Item1, Item2:  Pointer)
                                  : Integer;
begin
  {$IFDEF FMX}
    Result := CompareValue(TControl(Item1).Position.X, TControl(Item2).Position.X);
    if  Result = EqualsValue  then
      Result := CompareValue(TControl(Item1).Position.Y, TControl(Item2).Position.Y)
  {$ELSE}
    Result := CompareValue(TWinControl(Item1).Left, TWinControl(Item2).Left);
    if  Result = EqualsValue  then
      Result := CompareValue(TWinControl(Item1).Top, TWinControl(Item2).Top)
  {$ENDIF -FMX}
end { SortTabOrdersVertically };


procedure  SetTabOrders(const   Root:  {$IFDEF FMX}TControl{$ELSE}TWinControl{$ENDIF};
                        const   bVertically:  Boolean);
var
  nInd:  Integer;
  lLst:  TList;
  lCtl:  TControl;

begin  { SetTabOrders }
  lLst := TList.Create();
  try
    for  nInd := 0  to  Root.{$IFDEF FMX}ControlsCount{$ELSE}ControlCount{$ENDIF} - 1  do
      begin
        lCtl := Root.Controls[nInd];
        {$IFDEF FMX}
          lLst.Add(lCtl);
          SetTabOrders(lCtl, bVertically)
        {$ELSE}
          if  lCtl is TWinControl  then
            begin
              lLst.Add(lCtl);
              SetTabOrders(TWinControl(lCtl), bVertically)
            end
        {$ENDIF -FMX}
      end { for nInd };
    if  bVertically  then
      lLst.Sort(SortTabOrdersVertically)
    else
      lLst.Sort(SortTabOrdersHorizontally);
    for  nInd := 0  to  lLst.Count - 1  do
      {$IFDEF FMX}
        TControl(lLst.Items[nInd]).TabOrder := nInd
      {$ELSE}
        TWinControl(lLst.Items[nInd]).TabOrder := nInd
      {$ENDIF -FMX}
  finally
    lLst.Free()
  end { try-finally }
end { SetTabOrders };

{$ENDREGION 'SetTabOrders'}


end.

