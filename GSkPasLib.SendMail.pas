﻿{: @abstract Klasy do wysyłania wiadomości e-mail.

   Moduł @name zawiera klasę do wysyłania wiadomości e-mail. Ponadto zawiera
   klasy ułatwiające budowanie treści wiadomości w znormalizowany sposób. }
unit  GSkPasLib.SendMail;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  System.Classes, System.SysUtils, System.Math, System.RegularExpressions,
  System.Generics.Collections,
  IdMessage, IdMessageBuilder, IdEMailAddress, IdSMTP, IdSSLOpenSSL,
  IdExplicitTLSClientServerBase, IdUserPassProvider, IdSASL_CRAM_SHA1,
  IdSASL_CRAM_MD5, IdSASLSKey, IdSASLOTP, IdSASLAnonymous, IdSASLExternal,
  IdSASLLogin, IdSASLPlain;


const
  SMTP_PORT_EXPLICIT_TLS  = 587;
  SMTP_CONNECTION_TIMEOUT = 30000;   // [ms]
  {-}
  mpHighest = IdMessage.mpHighest;
  mpHigh    = IdMessage.mpHigh;
  mpNormal  = IdMessage.mpNormal;
  mpLow     = IdMessage.mpLow;
  mpLowest  = IdMessage.mpLowest;


type
  TIdMessagePriority = IdMessage.TIdMessagePriority;

  {: @abstract Wysyła e-mail do odbiorców, używajac serwer SMTP.

     Klasa @name pozwala wysyłać wiadomości e-mail. Można wskazać dowolnie wielu
     odbiorców. Treść wiadomości może być sformatowana jako HTML lub jako tekst
     (który można sformatować jako MarkDown). }
  TGSkSendMail = class
  strict private
    var
      fsHost:      string;
      fsLogin:     string;
      fsPassword:  string;
      fsSender:    string;
      fsSubject:   string;
      fsBodyHTML:  string;
      fsBodyMkDn:  string;
      fnConTimOut: Integer;   // ConnectTimeout
      fnPort:      Word;
      fbSSL:       Boolean;
      fPriority:   TIdMessagePriority;
      fRecipients: TStrings;
      fCC:         TStrings;
      fBCC:        TStrings;
    procedure  AssignAddrList(const   Dst:  TIdEMailAddressList;
                              const   Src:  TStrings);
    procedure  AddSSLHandler(const   SMTP:  TIdSMTP);
    procedure  InitSASL(const   SMTP:  TIdSMTP);
  public
    {: @abstract Standardowy konstruktor. }
    constructor  Create();                                           overload; virtual;
    {: @abstract Konstruktor definiujący parametry serwera SMTP oraz nadawcę.

       @param(sHost Nazwa lub adres IP serwera SMTP.)
       @param(sLogin Nazwa użytkownika serwara.)
       @param(sPassword Hasło użytkownika serwara.)
       @param(sSender Nadawca wiadomości.)
       @param(nPort Numer portu TCP/IP, używany do komunikacji z serwerem.)

       @seeAlso Host
       @seeAlso Login
       @seeAlso Password
       @seeAlso Sender
       @seeAlso Port }
    constructor  Create(const   sHost, sLogin, sPassword:  string;
                        const   sSender:  string;
                        const   nPort:  Word = SMTP_PORT_EXPLICIT_TLS); overload;
    {: @exclude }
    destructor  Destroy();                                           override;
    {-}
    {: @abstract Wysyła e-mail do zdefiniowanych odbiorców.

       Przykład: @longCode(#
        var
          lMail:  TGSkSendMail;
          lBody:  TGSkMailBodyBuilders;

        begin
          lMail := TGSkSendMail.Create('poczta.firma.pl', 'gskoczylas', 'qqryqnapatyq',
                                       '"Grześko" <gskoczylas@@firma.pl>');
          try
            lBody := TGSkMailBodyBuilders.Create();
            try
              lBody.Header(1, 'Nagłówek nr 1')
                   .Paragraph('To be or not to be...')
                   .Header(2, 'Podtytul')
                   .Paragraph('oto jest pytanie!')
                   .HorizontalLine()
                   .Paragraph('Taki test...');
              lMail.Recipients.Add('gskoczylas+Test@gmail.com');
              lMail.Subject := 'Test wysyłania poczty';
              lMail.BodyHTML := lBody.HTML;
              lMail.BodyMarkDown := lBody.MarkDown;
              lMail.SendMail();
            finally
              lBody.Free()
            end // try-finally
          finally
            lMail.Free();
          end;
          Close()
        end; #)

       @seeAlso Sender
       @seeAlso Subject
       @seeAlso BodyHTML
       @seeAlso BodyMarkDown
       @seeAlso Recipients
       @seeAlso CC
       @seeAlso BCC
       @seeAlso TGSkMailBodyBuilders }
    procedure  SendMail();
    {-}
    {: @abstract URL serwera SMTP. }
    property  Host:  string
              read  fsHost
              write fsHost;
    {: @abstract Nazwa uzytkownika serwera SMTP. }
    property  Login:  string
              read  fsLogin
              write fsLogin;
    {: @abstract Hasło użytkownika serwera SMTP. }
    property  Password:  string
              read  fsPassword
              write fsPassword;
    {: @abstract Maksymalny czas oczekiwania na połączenie z serwerem SMTP.

       Czas oczekiwania należy wskazać w milisekendach. }
    property  ConnectTimeout:  Integer
              read  fnConTimOut
              write fnConTimOut
              default SMTP_CONNECTION_TIMEOUT;
    {: @abstract Nadawca wiadomości.

       Nadawcę wiadomości można formatować w standardowy sposób, na przykład:
       @unorderedList(
         @item(@code(serwis@@firma.pl) -- tylko adres e-mail)
         @item(@code("Księgowość" <rachunki@@firma.pl>) -- nazwa i adres e-mail))

       @seeAlso Recipients }
    property  Sender:  string
              read  fsSender
              write fsSender;
    {: @abstract Temat wiadomości. }
    property  Subject:  string
              read  fsSubject
              write fsSubject;
    {: @abstract Treść wiadomości w formacie HTML.

       Treść wiadomości najlepiej przygotować przy pomocy klasy @link(TGSkMailBodyBuilderHTML)
       lub @link(TGSkMailBodyBuilders).

       Należy zdefiniować treść wiadomości albo w formacie HTML, albo jako zwykły
       @link(BodyMarkDown tekst), albo oba.

       @seeAlso BodyMarkDown }
    property  BodyHTML:  string
              read  fsBodyHTML
              write fsBodyHTML;
    {: @abstract Treść wiadomości jako zwykły tekst.

       Treść wiadomości najlepiej przygotować przy pomocy klasy
       @link(TGSkMailBodyBuilderMarkDown) lub @link(TGSkMailBodyBuilders).

       Należy zdefiniować treść wiadomości albo w formacie @link(BodyHTML HTML),
       albo jako zwykły tekst, albo oba.

       Treść wiadomości warto przygotować w formacie
       @URL(https://www.markdownguide.org MarkDown).

       @seeAlso BodyHTML }
    property  BodyMarkDown:  string
              read  fsBodyMkDn
              write fsBodyMkDn;
    {: @abstract Numer portu używanego do komunikacji z serwerem SMTP. }
    property  Port:  Word
              read  fnPort
              write fnPort
              default SMTP_PORT_EXPLICIT_TLS;
    {: @abstract Priorytet wiadomości. }
    property  Priority:  TIdMessagePriority
              read  fPriority
              write fPriority
              default mpNormal;
    {: @abstract Czy używać protokół SSL. }
    property  UseSSL:  Boolean
              read  fbSSL
              write fbSSL
              default True;
    {: @abstract Lista odbiorców wiadomości.

       Adres każdego odbiorcy należy zdefiniować tak samo, jak adres
       @link(Sender nadawcy).

       @seeAlso Sender
       @seeAlso CC
       @seeAlso BCC }
    property  Recipients:  TStrings
              read  fRecipients;
    {: @abstract Lista odbiorców @italic(do wiadomości).

       Adresy odbiorców należy formatować tak samo, jak w @link(Recipients).

       @seeAlso Sender
       @seeAlso Recipients
       @seeAlso BCC }
    property  CC:  TStrings
              read  fCC;
    {: @abstract Lista odbiorców @italic(@bold(uktyte) do wiadomości).

       Adresy odbiorców należy formatować tak samo, jak w @link(Recipients).

       @seeAlso Sender
       @seeAlso Recipients
       @seeAlso CC }
    property  BCC:  TStrings
              read  fBCC;
  end { TGSkSendMail };

  {: @abstract Poziom nagłówka.

     Ze względu na czytelność wiaadomości, najlepiej ograniczyć się do co najwyżej
     dwóch poziomów nagłówków. }
  TGSkMailBodyHeaderLevel = 1..6;

  {: @abstract Numer elementu listy w liście numerowanej. }
  TGSkMailBodyOrderedItemNo = 1..MaxInt;

  {: @abstract Poziom lewego marginesu. }
  TGSkMailBodyOffsetLevel = 0..10;

  {: @abstract Dosunięcie tekstu. }
  TGSkMailTextAlign = (taDefault, taLeft, taCenter, taRight);

  {: @abstract Rodzaj tekstu. }
  TGSkMailTextType = (ttInformation, ttWarning, ttError);

  {: @abstract Definicja kolumny tabeli. }
  TGSkMailTableColumn = record
  strict private
    var
      fsCaption:  string;
      fnWidth:    Integer;
      fAlign:     TGSkMailTextAlign;
  public
    constructor  Init(const   sCaption:  string;
                      const   nWidth:  Integer;
                      const   Align:  TGSkMailTextAlign = taLeft);
    {-}
    property  Caption:  string
              read  fsCaption;
    property  Width:  Integer
              read  fnWidth;
    property  Align:  TGSkMailTextAlign
              read  fAlign;
  end { TGSkMailTableColumn };

  {: @abstract Klasa definiuje interfejs generatorów treści wiadomości. }
  TGSkMailBodyBuilder = class abstract
    function  Header(const   Level:  TGSkMailBodyHeaderLevel;
                     const   sText:  string)
                     : TGSkMailBodyBuilder;                          overload; virtual; abstract;
    function  Header(const   Level:  TGSkMailBodyHeaderLevel;
                     const   sTextFmt:  string;
                     const   TextArgs:  array of const)
                     : TGSkMailBodyBuilder;                          overload; virtual; abstract;
    function  Paragraph(const   sText:  string;
                        const   nOffset:  TGSkMailBodyOffsetLevel = 0)
                        : TGSkMailBodyBuilder;                       overload; virtual; abstract;
    function  Paragraph(const   sTextFmt:  string;
                        const   TextArgs:  array of const;
                        const   nOffset:  TGSkMailBodyOffsetLevel = 0)
                        : TGSkMailBodyBuilder;                       overload; virtual; abstract;
    function  OrderedListStart() : TGSkMailBodyBuilder;              virtual; abstract;
    function  UnorderedListStart() : TGSkMailBodyBuilder;            virtual; abstract;
    function  ListItemBegin() : TGSkMailBodyBuilder;                 virtual; abstract;
    function  ListItemText(const   sText:  string)
                           : TGSkMailBodyBuilder;                    overload; virtual; abstract;
    function  ListItemText(const   sTextFmt:  string;
                           const   TextArgs:  array of const)
                           : TGSkMailBodyBuilder;                    overload; virtual; abstract;
    function  ListItemEnd() : TGSkMailBodyBuilder;                   virtual; abstract;
    function  ListItem(const   sText:  string)
                       : TGSkMailBodyBuilder;                        overload; virtual; abstract;
    function  ListItem(const   sTextFmt:  string;
                       const   TextArgs:  array of const)
                       : TGSkMailBodyBuilder;                        overload; virtual; abstract;
    function  OrderedListEnd() : TGSkMailBodyBuilder;                virtual; abstract;
    function  UnorderedListEnd() : TGSkMailBodyBuilder;              virtual; abstract;
    function  HorizontalLine() : TGSkMailBodyBuilder;                virtual; abstract;
    function  BlockQuoteBegin() : TGSkMailBodyBuilder;               virtual; abstract;
    function  BlockQuoteText(const   sText:  string)
                             : TGSkMailBodyBuilder;                  overload; virtual; abstract;
    function  BlockQuoteText(const   sTextFmt:  string;
                             const   TextArgs:  array of const)
                             : TGSkMailBodyBuilder;                  overload; virtual; abstract;
    function  BlockQuoteEnd() : TGSkMailBodyBuilder;                 virtual; abstract;
    function  BlockQuote(const   sText:  string)
                         : TGSkMailBodyBuilder;                      overload; virtual; abstract;
    function  BlockQuote(const   sTextFmt:  string;
                         const   TextArgs:  array of const)
                         : TGSkMailBodyBuilder;                      overload; virtual; abstract;
    function  TableHeaderStart() : TGSkMailBodyBuilder;              virtual; abstract;
    function  TableColumn(const   Column:  TGSkMailTableColumn;
                          const   TextType:  TGSkMailTextType = ttInformation;
                          const   bLastColumn:  Boolean = False)
                          : TGSkMailBodyBuilder;                     overload; virtual; abstract;
    function  TableColumn(const   sCaption:  string;
                          const   nWidth:  Integer;
                          const   Align:  TGSkMailTextAlign = taLeft;
                          const   TextType:  TGSkMailTextType = ttInformation;
                          const   bLastColumn:  Boolean = False)
                          : TGSkMailBodyBuilder;                     overload; virtual; abstract;
    function  TableHeaderEnd() : TGSkMailBodyBuilder;                virtual; abstract;
    function  TableHeader(const   Columns:  array of TGSkMailTableColumn)
                          : TGSkMailBodyBuilder;                     virtual; abstract;
    function  TableRowStart() : TGSkMailBodyBuilder;                 virtual; abstract;
    function  TableRowCell(const   sCellText:  string;
                           const   TextType:  TGSkMailTextType = ttInformation)
                           : TGSkMailBodyBuilder;                    overload; virtual; abstract;
    function  TableRowCell(const   sCellTextFmt:  string;
                           const   CellTextArgs:  array of const;
                           const   TextType:  TGSkMailTextType = ttInformation)
                           : TGSkMailBodyBuilder;                    overload; virtual; abstract;
    function  TableRowEnd() : TGSkMailBodyBuilder;                   virtual; abstract;
    function  TableRow(const   Cells:  array of string)
                       : TGSkMailBodyBuilder;                        virtual; abstract;
    function  TableEnd() : TGSkMailBodyBuilder;                      virtual; abstract;
  end { TGSkMailBodyBuilder };

  {: @abstract Bazowa implementacja generatorów treści wiadomości.

     @seeAlso TGSkMailBodyBuilderHTML
     @seeAlso TGSkMailBodyBuilderMarkDown }
  TGSkMailBodyBuilderBaseImpl = class abstract(TGSkMailBodyBuilder)
  strict private
    type
      {: @exclude }
      TElementType  = (etNone, etListOrdered, etListUnordered, etListItem,
                       etBlockQuote, etTable, etTableHeader, etTableRow);
      TListType     = etNone..etListUnordered;
      TElementTypes = set of  TElementType;
      TColumnDef = record
                   strict private
                     var
                       fnWidth:  Integer;
                       fAlign:   TGSkMailTextAlign;
                   public
                     constructor  Init(const   nWidth:  Integer;
                                       const   Align:  TGSkMailTextAlign);
                     {-}
                     property  Width:  Integer
                               read  fnWidth
                               write fnWidth;
                     property  Align:  TGSkMailTextAlign
                               read  fAlign;
                   end { TColumnDef };
    var
      fBody:          TStringWriter;
      fElementStack:  TStack<TElementType>;
      fListStack:     TStack<TListType>;
      fbIsNewLine:    Boolean;
      fColumnDefs:    TArray<TColumnDef>;
      fnColumnIndex:  Integer;
    function  GetCurrElementType() : TElementType;
    function  GetCurrListType() : TListType;
    function  GetColumnCount() : Integer;
    function  GetColumnAlign(const   nIndex:  Integer)
                             : TGSkMailTextAlign;
    function  GetColumnWidth(const   nIndex:  Integer)
                             : Integer;
    procedure  SetColumnWidth(const   nIndex, nWidth:  Integer);
    procedure  CheckIfElementIsIn(const   Elements:  TElementTypes);
    procedure  CheckIfElementIs(const   Element:  TElementType);
  strict protected
    function  FmtText(const   sText:  string)
                      : string;                                      virtual; abstract;
    function  GetListItemPrefix() : string;                          virtual;
    function  ElementStart(const   Element:  TElementType)
                           : TGSkMailBodyBuilder;
    function  ElementEnd(const   Element:  TElementType)
                         : TGSkMailBodyBuilder;
    function  Write(const   sText:  string)
                    : TGSkMailBodyBuilder;                           overload;
    function  Write(const   sTextFmt:  string;
                    const   TextArgs:  array of const)
                    : TGSkMailBodyBuilder;                           overload;
    function  WriteLine() : TGSkMailBodyBuilder;                     overload;
    function  WriteLine(const   sText:  string)
                        : TGSkMailBodyBuilder;                       overload;
    function  WriteLine(const   sTextFmt:  string;
                        const   TextArgs:  array of const)
                        : TGSkMailBodyBuilder;                       overload;
    function  ForceNewLine() : TGSkMailBodyBuilder;
    {-}
    property  CurrElementType:  TElementType
              read  GetCurrElementType;
    property  CurrListType:  TListType
              read  GetCurrListType;
    property  ColumnWidth[const   nIndex:  Integer] : Integer
              read  GetColumnWidth
              write SetColumnWidth;
    property  ColumnAlign[const   nIndex:  Integer] : TGSkMailTextAlign
              read  GetColumnAlign;
    property  ColumnIndex:  Integer
              read  fnColumnIndex;
    property  ColumnCount:  Integer
              read  GetColumnCount;
  public
    {: @exclude }
    constructor  Create();
    {: @exclude }
    destructor  Destroy();                                           override;
    {-}
    {: @abstract Wstawia nagłówek na wskazanym poziomie. }
    function  Header(const   Level:  TGSkMailBodyHeaderLevel;
                     const   sText:  string)
                     : TGSkMailBodyBuilder;                          overload; override;
    function  Header(const   Level:  TGSkMailBodyHeaderLevel;
                     const   sTextFmt:  string;
                     const   TextArgs:  array of const)
                     : TGSkMailBodyBuilder;                          overload; override;
    {: @abstract Wstawia akapit tekst. }
    function  Paragraph(const   sText:  string;
                        const   nOffset:  TGSkMailBodyOffsetLevel = 0)
                        : TGSkMailBodyBuilder;                       overload; override;
    function  Paragraph(const   sTextFmt:  string;
                        const   TextArgs:  array of const;
                        const   nOffset:  TGSkMailBodyOffsetLevel = 0)
                        : TGSkMailBodyBuilder;                       overload; override;
    {: @abstract Wstawia początek listy @bold(numerowanej). }
    function  OrderedListStart() : TGSkMailBodyBuilder;              override;
    {: @abstract Wstawia początek listy @bold(punktowanej). }
    function  UnorderedListStart() : TGSkMailBodyBuilder;            override;
    {: @abstract Wstawia początek elementu listy. }
    function  ListItemBegin() : TGSkMailBodyBuilder;                 override;
    {: @abstract Wstawia tekst elementu listy. }
    function  ListItemText(const   sText:  string)
                           : TGSkMailBodyBuilder;                    overload; override;
    function  ListItemText(const   sTextFmt:  string;
                           const   TextArgs:  array of const)
                           : TGSkMailBodyBuilder;                    overload; override;
    {: @abstract Wstawia koniec elementu listy. }
    function  ListItemEnd() : TGSkMailBodyBuilder;                   override;
    {: @abstract Wstawia element listy.

       @name jest skrótem do następującej sekwencji działań: @longCode(#
         Body.ListItemBegin();
         Body.ListItemText('Element listy');
         Body.ListItemEnd(); #)

       Powyższą sekwencję można również zapisać tak: @longCode(#
         Body.ListItemBegin()
             .ListItemText('Element listy')
             .ListItemEnd(); #) }
    function  ListItem(const   sText:  string)
                       : TGSkMailBodyBuilder;                        overload; override;
    function  ListItem(const   sTextFmt:  string;
                       const   TextArgs:  array of const)
                       : TGSkMailBodyBuilder;                        overload; override;
    {: @abstract Wstawia koniec listy @bold(numerowanej). }
    function  OrderedListEnd() : TGSkMailBodyBuilder;                override;
    {: @abstract Wstawia koniec listy @bold(punktowanej). }
    function  UnorderedListEnd() : TGSkMailBodyBuilder;              override;
    {: @abstract Wstawia poziomą linię rozdzielającą. }
    function  HorizontalLine() : TGSkMailBodyBuilder;                override;
    {: @abstract Wstawia początek bloku tekstu cytowanego. }
    function  BlockQuoteBegin() : TGSkMailBodyBuilder;               override;
    {: @abstract Wstawia treść bloku tekstu cytowanego. }
    function  BlockQuoteText(const   sText:  string)
                             : TGSkMailBodyBuilder;                  overload; override;
    function  BlockQuoteText(const   sTextFmt:  string;
                             const   TextArgs:  array of const)
                             : TGSkMailBodyBuilder;                  overload; override;
    {: @abstract Wstawia koniec bloku tekstu cytowanego. }
    function  BlockQuoteEnd() : TGSkMailBodyBuilder;                 override;
    {: @abstract Wstawia blok tekstu cytowanego.

       @name jest skrótem do następującej sekwencji działań: @longCode(#
         Body.BlockQuoteBegin();
         Body.BlockQuoteText('Cytowany blok tekstu.');
         Body.BlockQuoteEnd(); #)

       Powyższą sekwencję można również zapisać tak: @longCode(#
         Body.BlockQuoteBegin()
             .BlockQuoteText('Cytowany blok tekstu.')
             .BlockQuoteEnd(); #) }
    function  BlockQuote(const   sText:  string)
                         : TGSkMailBodyBuilder;                      override;
    function  BlockQuote(const   sTextFmt:  string;
                         const   TextArgs:  array of const)
                         : TGSkMailBodyBuilder;                      override;
    { Tables }
    function  TableHeaderStart() : TGSkMailBodyBuilder;              override;
    function  TableColumn(const   Column:  TGSkMailTableColumn;
                          const   TextType:  TGSkMailTextType = ttInformation;
                          const   bLastColumn:  Boolean = False)
                          : TGSkMailBodyBuilder;                     overload; override;
    function  TableColumn(const   sCaption:  string;
                          const   nWidth:  Integer;
                          const   Align:  TGSkMailTextAlign = taLeft;
                          const   TextType:  TGSkMailTextType = ttInformation;
                          const   bLastColumn:  Boolean = False)
                          : TGSkMailBodyBuilder;                     overload; override;
    function  TableHeaderEnd() : TGSkMailBodyBuilder;                override;
    function  TableHeader(const   Columns:  array of TGSkMailTableColumn)
                          : TGSkMailBodyBuilder;                     override;
    function  TableRowStart() : TGSkMailBodyBuilder;                 override;
    function  TableRowCell(const   sCellText:  string;
                           const   TextType:  TGSkMailTextType = ttInformation)
                           : TGSkMailBodyBuilder;                    overload; override;
    function  TableRowCell(const   sCellTextFmt:  string;
                           const   CellTextArgs:  array of const;
                           const   TextType:  TGSkMailTextType = ttInformation)
                           : TGSkMailBodyBuilder;                    overload; override;
    function  TableRowEnd() : TGSkMailBodyBuilder;                   override;
    function  TableRow(const   Cells:  array of string)
                       : TGSkMailBodyBuilder;                        override;
    function  TableEnd() : TGSkMailBodyBuilder;                      override;
    {: @abstract Zwraca wygenerowaną treść wiadomości. }
    function  ToString() : string;                                   override;
  end { TGSkMailBodyBuilderBaseImpl };

  {: @abstract Generyje treść wiadomości w formacie HTML.

     Klasa @name uzupełnia implementacje metod klasy
     @Link(TGSkMailBodyBuilderBaseImpl).

     W treści wiadomości można użyć również jeden z następujących specjalnych
     znaczników HTML:
     @unorderedList(
       @item(@code(<error>...</error>))
       @item(@code(<warning>...</warning>)))

     Powyższe znaczniki HTML zostaną zastapione odpowiednimi znacznikami MarkDown. }
  TGSkMailBodyBuilderHTML = class(TGSkMailBodyBuilderBaseImpl)
  strict private
    function  FmtStyle(const   Align:  TGSkMailTextAlign;
                       const   nWidth:  Integer;
                       const   TextType:  TGSkMailTextType )
                       : string;
  strict protected
    function  FmtText(const   sText:  string)
                      : string;                                      override;
  public
    {: @exclude }
    function  Header(const   Level:  TGSkMailBodyHeaderLevel;
                     const   sText:  string)
                     : TGSkMailBodyBuilder;                          override;
    {: @exclude }
    function  Paragraph(const   sText:  string;
                        const   nOffset:  TGSkMailBodyOffsetLevel = 0)
                        : TGSkMailBodyBuilder;                       override;
    {: @exclude }
    function  OrderedListStart() : TGSkMailBodyBuilder;              override;
    {: @exclude }
    function  UnorderedListStart() : TGSkMailBodyBuilder;            override;
    {: @exclude }
    function  ListItemBegin() : TGSkMailBodyBuilder;                 override;
    {: @exclude }
    function  ListItemEnd() : TGSkMailBodyBuilder;                   override;
    {: @exclude }
    function  OrderedListEnd() : TGSkMailBodyBuilder;                override;
    {: @exclude }
    function  UnorderedListEnd() : TGSkMailBodyBuilder;              override;
    {: @exclude }
    function  HorizontalLine() : TGSkMailBodyBuilder;                override;
    {: @exclude }
    function  BlockQuoteBegin() : TGSkMailBodyBuilder;               override;
    {: @exclude }
    function  BlockQuoteEnd() : TGSkMailBodyBuilder;                 override;
    {: @exclude }
    function  TableHeaderStart() : TGSkMailBodyBuilder;              override;
    {: @exclude }
    function  TableColumn(const   Column:  TGSkMailTableColumn;
                          const   TextType:  TGSkMailTextType = ttInformation;
                          const   bLastColumn:  Boolean = False)
                          : TGSkMailBodyBuilder;                     override;
    {: @exclude }
    function  TableHeaderEnd() : TGSkMailBodyBuilder;                override;
    {: @exclude }
    function  TableRowStart() : TGSkMailBodyBuilder;                 override;
    {: @exclude }
    function  TableRowCell(const   sCellText:  string;
                           const   TextType:  TGSkMailTextType = ttInformation)
                           : TGSkMailBodyBuilder;                    override;
    {: @exclude }
    function  TableRowEnd() : TGSkMailBodyBuilder;                   override;
    {: @exclude }
    function  TableEnd() : TGSkMailBodyBuilder;                      override;
    {: @exclude }
    function  ToString() : string;                                   override;
  end { TGSkMailBodyBuilderHTML };

  {: @abstract Generyje treść wiadomości w formacie MarkDown.

     Klasa @name uzupełnia implementacje metod klasy
     @Link(TGSkMailBodyBuilderBaseImpl).

     W treści wiadomości można użyć jeden z następujących znaczników HTML:
     @unorderedList(
       @item(@code(<b>...</b>) --- tekst @bold(pogrubiony))
       @item(@code(<i>...</i>) --- tekst @italic(kursywa))
       @item(@code(<tt>...</tt>) --- tekst @code(stałej szerokości))
       @item(@code(<a href="...">...</a>) --- odsyłacz)
       @item(@code(<error>...</error>))
       @item(@code(<warning>...</warning>)))

     Powyższe znaczniki HTML zostaną zastapione odpowiednimi znacznikami MarkDown. }
  TGSkMailBodyBuilderMarkdown = class(TGSkMailBodyBuilderBaseImpl)
    /// <remarks>
    /// Zamienia znaczniki HTML <b>, <i>, <tt> i <a> na odpowiedniki w MarkDown.
    /// </remarks>
  strict private
    const
      cnRightMargin = 80;
    var
      fnOrderNo:    Cardinal;
      fsOffset:     string;
      fEMailRegEx:  TRegEx;
      fOrderStack:  TStack<Cardinal>;
    function  FmtCell(const   sText:  string;
                      const   nWidth:  Integer;
                      const   Align:  TGSkMailTextAlign;
                      const   TextType:  TGSkMailTextType)
                      : string;
  strict protected
    function  FmtText(const   sText:  string)
                      : string;                                      override;
    function  GetListItemPrefix() : string;                          override;
  public
    {: @exclude }
    constructor  Create();
    {-}
    {: @exclude }
    function  Header(const   Level:  TGSkMailBodyHeaderLevel;
                     const   sText:  string)
                     : TGSkMailBodyBuilder;                          override;
    {: @exclude }
    function  Paragraph(const   sText:  string;
                        const   nOffset:  TGSkMailBodyOffsetLevel = 0)
                        : TGSkMailBodyBuilder;                       override;
    {: @exclude }
    function  OrderedListStart() : TGSkMailBodyBuilder;              override;
    {: @exclude }
    function  UnorderedListStart() : TGSkMailBodyBuilder;            override;
    {: @exclude }
    function  ListItemBegin() : TGSkMailBodyBuilder;                 override;
    {: @exclude }
    function  ListItemEnd() : TGSkMailBodyBuilder;                   override;
    {: @exclude }
    function  OrderedListEnd() : TGSkMailBodyBuilder;                override;
    {: @exclude }
    function  UnorderedListEnd() : TGSkMailBodyBuilder;              override;
    {: @exclude }
    function  HorizontalLine() : TGSkMailBodyBuilder;                override;
    {: @exclude }
    function  BlockQuoteBegin() : TGSkMailBodyBuilder;               override;
    {: @exclude }
    function  BlockQuoteEnd() : TGSkMailBodyBuilder;                 override;
    {: @exclude }
    function  TableHeaderStart() : TGSkMailBodyBuilder;              override;
    {: @exclude }
    function  TableColumn(const   Column:  TGSkMailTableColumn;
                          const   TextType:  TGSkMailTextType = ttInformation;
                          const   bLastColumn:  Boolean = False)
                          : TGSkMailBodyBuilder;                     override;
    {: @exclude }
    function  TableHeaderEnd() : TGSkMailBodyBuilder;                override;
    {: @exclude }
    function  TableRowStart() : TGSkMailBodyBuilder;                 override;
    {: @exclude }
    function  TableRowCell(const   sCellText:  string;
                           const   TextType:  TGSkMailTextType = ttInformation)
                           : TGSkMailBodyBuilder;                    override;
    {: @exclude }
    function  TableRowEnd() : TGSkMailBodyBuilder;                   override;
    {: @exclude }
    function  TableEnd() : TGSkMailBodyBuilder;                      override;
  end { TGSkMailBodyBuilderMarkdown };

  {: @abstract Generyje treść wiadomości jednocześnir w formacie HTML i MarkDown.

     Klasa @name uzupełnia implementacje metod klasy
     @Link(TGSkMailBodyBuilderBaseImpl). Dodatkowo dostępne są właściwości
     @link(HTML) i @link(MarkDown).

     @seeAlso TGSkMailBodyBuilderHTML
     @seeAlso TGSkMailBodyBuilderMarkdown }
  TGSkMailBodyBuilders = class(TGSkMailBodyBuilder)
  strict private
    var
      fHTML:  TGSkMailBodyBuilderHTML;
      fMkDn:  TGSkMailBodyBuilderMarkdown;
    function  GetHTML() : string;
    function  GetMarkDown() : string;
  public
    {: @exclude }
    constructor  Create();
    {: @exclude }
    destructor  Destroy();                                           override;
    {: @exclude }
    function  Header(const   Level:  TGSkMailBodyHeaderLevel;
                     const   sText:  string)
                     : TGSkMailBodyBuilder;                          override;
    function  Header(const   Level:  TGSkMailBodyHeaderLevel;
                     const   sTextFmt:  string;
                     const   TextArgs:  array of const)
                     : TGSkMailBodyBuilder;                          override;
    {: @exclude }
    function  Paragraph(const   sText:  string;
                        const   nOffset:  TGSkMailBodyOffsetLevel = 0)
                        : TGSkMailBodyBuilder;                       override;
    {: @exclude }
    function  Paragraph(const   sTextFmt:  string;
                        const   TextArgs:  array of const;
                        const   nOffset:  TGSkMailBodyOffsetLevel = 0)
                        : TGSkMailBodyBuilder;                       override;
    {: @exclude }
    function  OrderedListStart() : TGSkMailBodyBuilder;              override;
    {: @exclude }
    function  UnorderedListStart() : TGSkMailBodyBuilder;            override;
    {: @exclude }
    function  ListItemBegin() : TGSkMailBodyBuilder;                 override;
    {: @exclude }
    function  ListItemText(const   sText:  string)
                           : TGSkMailBodyBuilder;                    override;
    {: @exclude }
    function  ListItemText(const   sTextFmt:  string;
                           const   TextArgs:  array of const)
                           : TGSkMailBodyBuilder;                    override;
    {: @exclude }
    function  ListItemEnd() : TGSkMailBodyBuilder;                   override;
    {: @exclude }
    function  ListItem(const   sText:  string)
                       : TGSkMailBodyBuilder;                        override;
    {: @exclude }
    function  ListItem(const   sTextFmt:  string;
                       const   TextArgs:  array of const)
                       : TGSkMailBodyBuilder;                        override;
    {: @exclude }
    function  OrderedListEnd() : TGSkMailBodyBuilder;                override;
    {: @exclude }
    function  UnorderedListEnd() : TGSkMailBodyBuilder;              override;
    {: @exclude }
    function  HorizontalLine() : TGSkMailBodyBuilder;                override;
    {: @exclude }
    function  BlockQuoteBegin() : TGSkMailBodyBuilder;               override;
    {: @exclude }
    function  BlockQuoteText(const   sText:  string)
                             : TGSkMailBodyBuilder;                  override;
    {: @exclude }
    function  BlockQuoteText(const   sTextFmt:  string;
                             const   TextArgs:  array of const)
                             : TGSkMailBodyBuilder;                  override;
    {: @exclude }
    function  BlockQuoteEnd() : TGSkMailBodyBuilder;                 override;
    {: @exclude }
    function  BlockQuote(const   sText:  string)
                         : TGSkMailBodyBuilder;                      override;
    {: @exclude }
    function  BlockQuote(const   sTextFmt:  string;
                         const   TextArgs:  array of const)
                         : TGSkMailBodyBuilder;                      override;
    {: @exclude }
    function  TableHeaderStart() : TGSkMailBodyBuilder;              override;
    {: @exclude }
    function  TableColumn(const   Column:  TGSkMailTableColumn;
                          const   TextType:  TGSkMailTextType = ttInformation;
                          const   bLastColumn:  Boolean = False)
                          : TGSkMailBodyBuilder;                     override;
    {: @exclude }
    function  TableColumn(const   sCaption:  string;
                          const   nWidth:  Integer;
                          const   Align:  TGSkMailTextAlign = taLeft;
                          const   TextType:  TGSkMailTextType = ttInformation;
                          const   bLastColumn:  Boolean = False)
                          : TGSkMailBodyBuilder;                     override;
    {: @exclude }
    function  TableHeaderEnd() : TGSkMailBodyBuilder;                override;
    {: @exclude }
    function  TableHeader(const   Columns:  array of TGSkMailTableColumn)
                          : TGSkMailBodyBuilder;                     override;
    {: @exclude }
    function  TableRowStart() : TGSkMailBodyBuilder;                 override;
    {: @exclude }
    function  TableRowCell(const   sCellText:  string;
                           const   TextType:  TGSkMailTextType = ttInformation)
                           : TGSkMailBodyBuilder;                    override;
    {: @exclude }
    function  TableRowCell(const   sCellTextFmt:  string;
                           const   CellTextArgs:  array of const;
                           const   TextType:  TGSkMailTextType = ttInformation)
                           : TGSkMailBodyBuilder;                    override;
    {: @exclude }
    function  TableRowEnd() : TGSkMailBodyBuilder;                   override;
    {: @exclude }
    function  TableRow(const   Cells:  array of string)
                       : TGSkMailBodyBuilder;                        override;
    {: @exclude }
    function  TableEnd() : TGSkMailBodyBuilder;                      override;
    {-}
    {: @abstract Zwraca wygenerowaną wiadomość w formacie HTML. }
    property  HTML:  string
              read  GetHTML;
    {: @abstract Zwraca wygenerowaną wiadomość w formacie MarkDown. }
    property  MarkDown:  string
              read  GetMarkDown;
  end { TGSkMailBodyBuilders };


  {: @abstract Wyjątki generowane przez klasy tego modułu. }
  EGSkMailError = class(Exception);


implementation


uses
  GSkPasLib.StrUtils;


const
  STD_OFFSET_INC = 4;


{$REGION 'TGSkSendMail'}

procedure  TGSkSendMail.AddSSLHandler(const   SMTP:  TIdSMTP);

var
  lHandler:  TIdSSLIOHandlerSocketOpenSSL;

begin  { AddSSLHandler }
  lHandler := TIdSSLIOHandlerSocketOpenSSL.Create(SMTP);
  lHandler.SSLOptions.Method := sslvSSLv23;
  lHandler.SSLOptions.Mode := sslmClient;
  lHandler.SSLOptions.VerifyMode := [];
  lHandler.SSLOptions.VerifyDepth := 0;
  SMTP.IOHandler := lHandler
end { AddSSLHandler };


procedure  TGSkSendMail.AssignAddrList(const   Dst:  TIdEMailAddressList;
                                       const   Src:  TStrings);
var
  sAddr:  string;
  sTemp:  string;

begin  { AssignAddrList }
  for  sAddr in Src  do
    begin
      sTemp := Trim(sAddr);
      if  sTemp <> ''  then
        Dst.Add().Text := sTemp
    end { for sAddr }
end { AssignAddrList };


constructor  TGSkSendMail.Create(const   sHost, sLogin, sPassword, sSender:  string;
                                 const   nPort:  Word);
begin
  // inherited ...
  Create();
  {-}
  fsHost := sHost;
  fsLogin := sLogin;
  fsPassword := sPassword;
  fsSender := sSender;
  fnPort := nPort
end { Create };


constructor  TGSkSendMail.Create();
begin
  inherited;
  fnPort := SMTP_PORT_EXPLICIT_TLS;
  fnConTimOut := SMTP_CONNECTION_TIMEOUT;
  fbSSL := True;
  fPriority := mpNormal;
  {-}
  fRecipients := TStringList.Create();
  fCC := TStringList.Create();
  fBCC := TStringList.Create()
end { Create };


destructor  TGSkSendMail.Destroy();
begin
  fBCC.Free();
  fCC.Free();
  fRecipients.Free();
  inherited;
end { Destroy };


procedure  TGSkSendMail.InitSASL(const   SMTP:  TIdSMTP);

var
  lUsrPwd:  TIdUserPassProvider;
  lSHA1:    TIdSASLCRAMSHA1;
  lMD5:     TIdSASLCRAMMD5;
  lSKey:    TIdSASLSKey;
  lOTP:     TIdSASLOTP;
  lAnonms:  TIdSASLAnonymous;
  lExtrnl:  TIdSASLExternal;
  lLogin:   TIdSASLLogin;
  lPlain:   TIdSASLPlain;

begin  { InitSASL }
  lUsrPwd := TIdUserPassProvider.Create(SMTP);
  lUsrPwd.Username := fsLogin;
  lUsrPwd.Password := fsPassword;
  {-}
  lSHA1 := TIdSASLCRAMSHA1.Create(SMTP);
  lSHA1.UserPassProvider := lUsrPwd;
  {-}
  lMD5 := TIdSASLCRAMMD5.Create(SMTP);
  lMD5.UserPassProvider := lUsrPwd;
  {-}
  lSKey := TIdSASLSKey.Create(SMTP);
  lSKey.UserPassProvider := lUsrPwd;
  {-}
  lOTP := TIdSASLOTP.Create(SMTP);
  lOTP.UserPassProvider := lUsrPwd;
  {-}
  lAnonms := TIdSASLAnonymous.Create(SMTP);
  lExtrnl := TIdSASLExternal.Create(SMTP);
  {-}
  lLogin := TIdSASLLogin.Create(SMTP);
  lLogin.UserPassProvider := lUsrPwd;
  {-}
  lPlain := TIdSASLPlain.Create(SMTP);
  lPlain.UserPassProvider := lUsrPwd;
  {-}
  with  SMTP.SASLMechanisms  do
    begin
      Add().SASL := lSHA1;
      Add().SASL := lMD5;
      Add().SASL := lSKey;
      Add().SASL := lOTP;
      Add().SASL := lAnonms;
      Add().SASL := lExtrnl;
      Add().SASL := lLogin;
      Add().SASL := lPlain
    end { with SMTP.SASLMechanisms }
end { InitSASL };


procedure  TGSkSendMail.SendMail();
var
  lMsg:  TIdMessage;
  lSMTP: TIdSMTP;

begin  { SendMail }
  lMsg := TIdMessage.Create(nil);
  try
    with  TIdMessageBuilderHtml.Create()  do
      try
        PlainText.Text :=  fsBodyMkDn;
        PlainTextCharSet := 'UTF-8';
        Html.Text := fsBodyHTML;
        HtmlCharSet := 'UTF-8';
        FillMessage(lMsg)
      finally
        Free()
      end { try-finally };
    lMsg.Sender.Text := fsSender;
    lMsg.Priority := fPriority;
    lMsg.Subject := fsSubject;
    AssignAddrList(lMsg.Recipients, fRecipients);
    AssignAddrList(lMsg.CCList, fCC);
    AssignAddrList(lMsg.BccList, fBCC);
    lSMTP := TIdSMTP.Create(nil);
    try
      if  fbSSL  then
        begin
          AddSSLHandler(lSMTP);
          if  fnPort = SMTP_PORT_EXPLICIT_TLS  then
            lSMTP.UseTLS := utUseExplicitTLS
          else
            lSMTP.UseTLS := utUseImplicitTLS
        end { fbSSL };
      {-}
      if  (fsLogin <> '')
          or (fsPassword <> '')  then
        begin
          lSMTP.AuthType := satSASL;
          InitSASL(lSMTP)
        end
      else
        lSMTP.AuthType := satNone;
      {-}
      lSMTP.Host := fsHost;
      lSMTP.Port := fnPort;
      lSMTP.ConnectTimeout := fnConTimOut;
      lSMTP.UseEhlo := True;
      {-}
      lSMTP.Connect();
      try
        lSMTP.Send(lMsg)
      finally
        lSMTP.Disconnect()
      end { try-finally }
    finally
      lSMTP.Free()
    end { try-finally }
  finally
    lMsg.Free()
  end { try-finally }
end { SendMail };

{$ENDREGION 'TGSkSendMail'}


{$REGION 'TGSkMailTableColumn'}

constructor  TGSkMailTableColumn.Init(const   sCaption:  string;
                                      const   nWidth:  Integer;
                                      const   Align:  TGSkMailTextAlign);
begin
  fsCaption := sCaption;
  fnWidth := nWidth;
  fAlign := Align
end { Init };

{$ENDREGION 'TGSkMailTableColumn'}


{$REGION 'TGSkMailBodyBuilderBaseImpl'}

function  TGSkMailBodyBuilderBaseImpl.BlockQuote(const   sText:  string)
                                                 : TGSkMailBodyBuilder;
begin
  Result := BlockQuoteBegin()
            .BlockQuoteText(sText)
            .BlockQuoteEnd()
end { BlockQuote };


function  TGSkMailBodyBuilderBaseImpl.BlockQuote(const   sTextFmt:  string;
                                                 const   TextArgs:  array of const)
                                                 : TGSkMailBodyBuilder;
begin
  Result := BlockQuote(Format(sTextFmt, TextArgs))
end { BlockQuote };


function  TGSkMailBodyBuilderBaseImpl.BlockQuoteBegin() : TGSkMailBodyBuilder;
begin
  Result := ElementStart(etBlockQuote)
end { BlockQuoteBegin };


function  TGSkMailBodyBuilderBaseImpl.BlockQuoteEnd() : TGSkMailBodyBuilder;
begin
  Result := ElementEnd(etBlockQuote)
end { BlockQuoteEnd };


function  TGSkMailBodyBuilderBaseImpl.BlockQuoteText(const   sText:  string)
                                                     : TGSkMailBodyBuilder;
begin
  CheckIfElementIs(etBlockQuote);
  Result := Write(sText)
end { BlockQuoteText };


function  TGSkMailBodyBuilderBaseImpl.BlockQuoteText(const   sTextFmt:  string;
                                                     const   TextArgs:  array of const)
                                                     : TGSkMailBodyBuilder;
begin
  Result := BlockQuoteText(Format(sTextFmt, TextArgs))
end { BlockQuoteText };


procedure  TGSkMailBodyBuilderBaseImpl.CheckIfElementIsIn(const   Elements:  TElementTypes);
begin
  if  not (CurrElementType in Elements)  then
    raise EGSkMailError.Create('Incorrect e-mail body structure')
end { CheckIfElementIsIn };


procedure  TGSkMailBodyBuilderBaseImpl.CheckIfElementIs(const   Element:  TElementType);
begin
  CheckIfElementIsIn([Element])
end { CheckIfElementIs };


constructor  TGSkMailBodyBuilderBaseImpl.Create();
begin
  inherited Create();
  fBody := TStringWriter.Create();
  fElementStack := TStack<TElementType>.Create();
  fListStack := TStack<TListType>.Create();
  fbIsNewLine := True
end { Create };


destructor  TGSkMailBodyBuilderBaseImpl.Destroy();
begin
  fListStack.Free();
  fElementStack.Free();
  fBody.Free();
  inherited
end { Destroy };


function  TGSkMailBodyBuilderBaseImpl.ElementEnd(const   Element:  TElementType)
                                                 : TGSkMailBodyBuilder;
begin
  CheckIfElementIs(Element);
  fElementStack.Pop();
  if  Element in [etListOrdered, etListUnordered]  then
    fListStack.Pop();
  Result := Self
end { ElementEnd };


function  TGSkMailBodyBuilderBaseImpl.ElementStart(const   Element:  TElementType)
                                                   : TGSkMailBodyBuilder;
begin
  fElementStack.Push(Element);
  if  Element in [etListOrdered, etListUnordered]  then
    fListStack.Push(Element);
  Result := Self
end { ElementStart };


function TGSkMailBodyBuilderBaseImpl.ForceNewLine() : TGSkMailBodyBuilder;
begin
  if  not fbIsNewLine  then
    WriteLine();
  Result := Self
end { ForceNewLine };


function  TGSkMailBodyBuilderBaseImpl.GetColumnAlign(const   nIndex:  Integer)
                                                     : TGSkMailTextAlign;
begin
  if  (nIndex >= 0)
      and (nIndex <= High(fColumnDefs))  then
    Result := fColumnDefs[nIndex].Align
  else
    Result := taDefault
end { GetColumnAlign };


function  TGSkMailBodyBuilderBaseImpl.GetColumnCount() : Integer;
begin
  Result := Length(fColumnDefs)
end { GetColumnCount };


function  TGSkMailBodyBuilderBaseImpl.GetColumnWidth(const   nIndex:  Integer)
                                                     : Integer;
begin
  if  (nIndex >= 0)
      and (nIndex <= High(fColumnDefs))  then
    Result := fColumnDefs[nIndex].Width
  else
    Result := 0
end { GetColumnWidth };


function  TGSkMailBodyBuilderBaseImpl.GetCurrElementType() : TElementType;
begin
  if  fElementStack.Count > 0  then
    Result := fElementStack.Peek()
  else
    Result := etNone
end { TGSkMailBodyBuilder };


function  TGSkMailBodyBuilderBaseImpl.GetCurrListType() : TListType;
begin
  if  fListStack.Count > 0  then
    Result := fListStack.Peek()
  else
    Result := etNone
end { GetCurrListType };


function  TGSkMailBodyBuilderBaseImpl.GetListItemPrefix() : string;
begin
  Result := ''
end { GetListItemPrefix };


function  TGSkMailBodyBuilderBaseImpl.Header(const   Level:  TGSkMailBodyHeaderLevel;
                                             const   sText:  string)
                                             : TGSkMailBodyBuilder;
begin
  CheckIfElementIs(etNone);
  Result := Self
end { Header };


function  TGSkMailBodyBuilderBaseImpl.Header(const   Level:  TGSkMailBodyHeaderLevel;
                                             const   sTextFmt:  string;
                                             const   TextArgs:  array of const)
                                             : TGSkMailBodyBuilder;
begin
  Result := Header(Level, Format(sTextFmt, TextArgs))
end { Header };


function  TGSkMailBodyBuilderBaseImpl.HorizontalLine() : TGSkMailBodyBuilder;
begin
  Result := Self
end { HorizontalLine };


function  TGSkMailBodyBuilderBaseImpl.ListItem(const   sText:  string)
                                               : TGSkMailBodyBuilder;
begin
  Result := ListItemBegin()
            .ListItemText(sText)
            .ListItemEnd()
end { ListItem };


function  TGSkMailBodyBuilderBaseImpl.ListItem(const   sTextFmt:  string;
                                               const   TextArgs:  array of const)
                                               : TGSkMailBodyBuilder;
begin
  Result := ListItem(Format(sTextFmt, TextArgs))
end { ListItem };


function  TGSkMailBodyBuilderBaseImpl.ListItemBegin() : TGSkMailBodyBuilder;
begin
  CheckIfElementIsIn([etListOrdered, etListUnordered]);
  Result := ElementStart(etListItem)
end { ListItemBegin };


function  TGSkMailBodyBuilderBaseImpl.ListItemEnd() : TGSkMailBodyBuilder;
begin
  Result := ElementEnd(etListItem)
end { ListItemEnd };


function  TGSkMailBodyBuilderBaseImpl.ListItemText(const   sText:  string)
                                                   : TGSkMailBodyBuilder;
begin
  CheckIfElementIs(etListItem);
  Result := Write(GetListItemPrefix() + sText)
end { ListItemText };


function  TGSkMailBodyBuilderBaseImpl.ListItemText(const   sTextFmt:  string;
                                                   const   TextArgs:  array of const)
                                                   : TGSkMailBodyBuilder;
begin
  Result := ListItemText(Format(sTextFmt, TextArgs))
end { ListItemText };


function  TGSkMailBodyBuilderBaseImpl.OrderedListEnd() : TGSkMailBodyBuilder;
begin
  Result := ElementEnd(etListOrdered)
end { OrderedListEnd };


function  TGSkMailBodyBuilderBaseImpl.OrderedListStart() : TGSkMailBodyBuilder;
begin
  Result := ElementStart(etListOrdered)
end { OrderedListStart };


function  TGSkMailBodyBuilderBaseImpl.Paragraph(const   sText:  string;
                                                const   nOffset:  TGSkMailBodyOffsetLevel)
                                                : TGSkMailBodyBuilder;
begin
  Result := Self
end { Paragraph };


function  TGSkMailBodyBuilderBaseImpl.Paragraph(const   sTextFmt:  string;
                                                const   TextArgs:  array of const;
                                                const   nOffset:  TGSkMailBodyOffsetLevel)
                                                : TGSkMailBodyBuilder;
begin
  Result := Paragraph(Format(sTextFmt, TextArgs), nOffset)
end { Paragraph };


procedure  TGSkMailBodyBuilderBaseImpl.SetColumnWidth(const   nIndex, nWidth:  Integer);
begin
  fColumnDefs[nIndex].Width := nWidth
end { SetColumnWidth };


function  TGSkMailBodyBuilderBaseImpl.TableColumn(const   sCaption:  string;
                                                  const   nWidth:  Integer;
                                                  const   Align:  TGSkMailTextAlign;
                                                  const   TextType:  TGSkMailTextType;
                                                  const   bLastColumn:  Boolean)
                                                  : TGSkMailBodyBuilder;
begin
  Result := TableColumn(TGSkMailTableColumn.Init(sCaption, nWidth, Align),
                        TextType, bLastColumn)
end { TableColumn };


function  TGSkMailBodyBuilderBaseImpl.TableColumn(const   Column:  TGSkMailTableColumn;
                                                  const   TextType:  TGSkMailTextType;
                                                  const   bLastColumn:  Boolean)
                                                  : TGSkMailBodyBuilder;
begin
  CheckIfElementIs(etTableHeader);
  SetLength(fColumnDefs, Length(fColumnDefs) + 1);
  fColumnDefs[High(fColumnDefs)].Init(Column.Width, Column.Align);
  Inc(fnColumnIndex);
  Assert(fnColumnIndex = High(fColumnDefs));
  Result := Self
end { TableColumn };


function  TGSkMailBodyBuilderBaseImpl.TableEnd() : TGSkMailBodyBuilder;
begin
  SetLength(fColumnDefs, 0);
  fnColumnIndex := -1;
  Result := ElementEnd(etTable)
end { TableEnd };


function  TGSkMailBodyBuilderBaseImpl.TableHeader(const   Columns:  array of TGSkMailTableColumn)
                                                  : TGSkMailBodyBuilder;
var
  nInd:  Integer;

begin  { TableHeader }
  TableHeaderStart();
  for  nInd := 0  to  High(Columns)  do
    TableColumn(Columns[nInd], ttInformation,
                nInd = High(Columns));
  TableHeaderEnd();
  Result := Self
end { TableHeader };


function  TGSkMailBodyBuilderBaseImpl.TableHeaderEnd() : TGSkMailBodyBuilder;
begin
  Result := ElementEnd(etTableHeader);
  fnColumnIndex := -1
end { TableHeaderEnd };


function  TGSkMailBodyBuilderBaseImpl.TableHeaderStart() : TGSkMailBodyBuilder;
begin
  if  Length(fColumnDefs) <> 0  then
    raise  EGSkMailError.Create('Nested tables are not supported');
  fnColumnIndex := -1;
  ElementStart(etTable);
  ElementStart(etTableHeader);
  Result := Self
end { TableHeaderStart };


function  TGSkMailBodyBuilderBaseImpl.TableRow(const   Cells:  array of string)
                                               : TGSkMailBodyBuilder;
var
  nInd:  Integer;

begin  { TableRow }
  TableRowStart();
  for  nInd := 0  to  High(Cells)  do
    TableRowCell(Cells[nInd]);
  TableRowEnd();
  Result := Self
end { TableRow };


function  TGSkMailBodyBuilderBaseImpl.TableRowCell(const   sCellText:  string;
                                                   const   TextType:  TGSkMailTextType)
                                                   : TGSkMailBodyBuilder;
begin
  CheckIfElementIs(etTableRow);
  Inc(fnColumnIndex);
  Result := Self
end { TableRowCell };


function  TGSkMailBodyBuilderBaseImpl.TableRowCell(const   sCellTextFmt:  string;
                                                   const   CellTextArgs:  array of const;
                                                   const   TextType:  TGSkMailTextType)
                                                   : TGSkMailBodyBuilder;
begin
  Result := TableRowCell(Format(sCellTextFmt, CellTextArgs), TextType)
end { TableRowCell };


function  TGSkMailBodyBuilderBaseImpl.TableRowEnd() : TGSkMailBodyBuilder;
begin
  fnColumnIndex := -1;
  Result := ElementEnd(etTableRow)
end { TableRowEnd };


function  TGSkMailBodyBuilderBaseImpl.TableRowStart() : TGSkMailBodyBuilder;
begin
  CheckIfElementIs(etTable);
  fnColumnIndex := -1;
  Result := ElementStart(etTableRow)
end { TableRowStart };


function  TGSkMailBodyBuilderBaseImpl.ToString() : string;
begin
  CheckIfElementIs(etNone);
  Result := fBody.ToString()
end { ToString };


function  TGSkMailBodyBuilderBaseImpl.UnorderedListEnd() : TGSkMailBodyBuilder;
begin
  Result := ElementEnd(etListUnordered)
end { UnorderedListEnd };


function  TGSkMailBodyBuilderBaseImpl.UnorderedListStart() : TGSkMailBodyBuilder;
begin
  Result := ElementStart(etListUnordered)
end { UnorderedListStart };


function  TGSkMailBodyBuilderBaseImpl.Write(const   sText:  string)
                                            : TGSkMailBodyBuilder;
begin
  fBody.Write(FmtText(sText));
  fbIsNewLine := False;
  Result := Self
end { Write };


function  TGSkMailBodyBuilderBaseImpl.Write(const   sTextFmt:  string;
                                            const   TextArgs:  array of const)
                                            : TGSkMailBodyBuilder;
begin
  Result := Write(Format(sTextFmt, TextArgs))
end { Write };


function  TGSkMailBodyBuilderBaseImpl.WriteLine() : TGSkMailBodyBuilder;
begin
  fBody.WriteLine();
  fbIsNewLine := True;
  Result := Self
end { WriteLine };


function  TGSkMailBodyBuilderBaseImpl.WriteLine(const   sText:  string)
                                                : TGSkMailBodyBuilder;
begin
  Write(sText);
  Result := WriteLine()
end { WriteLine };


function  TGSkMailBodyBuilderBaseImpl.WriteLine(const   sTextFmt:  string;
                                                const   TextArgs:  array of const)
                                                : TGSkMailBodyBuilder;
begin
  Result := WriteLine(Format(sTextFmt, TextArgs))
end { WriteLine };

{$ENDREGION 'TGSkMailBodyBuilderBaseImpl'}


{$REGION 'TGSkMailBodyBuilderHTML'}

function  TGSkMailBodyBuilderHTML.BlockQuoteBegin() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('<blockquote>');
  Result := inherited BlockQuoteBegin()
end { BlockQuoteBegin };


function  TGSkMailBodyBuilderHTML.BlockQuoteEnd() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('</blockquote>');
  Result := inherited BlockQuoteEnd()
end { BlockQuoteEnd };


function  TGSkMailBodyBuilderHTML.FmtStyle(const   Align:  TGSkMailTextAlign;
                                           const   nWidth:  Integer;
                                           const   TextType:  TGSkMailTextType)
                                           : string;
begin
  case  Align  of
    taDefault:
      Result := '';
    taLeft:
      Result := 'text-align:left;';
    taCenter:
      Result := 'text-align:center;';
    taRight:
      Result := 'text-align:right;'
  end { case Align };
  case  TextType  of
    ttInformation:
      { OK };
    ttWarning:
      Result := Result + 'background-color:#ffffc7;';
    ttError:
      Result := Result + 'background-color:#ffccc9;'
  end { case TextType };
  if  nWidth <> 0  then
    Result := Result + 'width:' + IntToStr(nWidth) + 'ex;';
  if  Result <> ''  then
    Result := ' style="' + Result + '"'
end { FmtStyle };


function  TGSkMailBodyBuilderHTML.FmtText(const   sText:  string)
                                          : string;
begin
  // W razie potrzeby tutaj można również konwertować znaki "<", ">", "&", itp. na encje.
  // Trzeba uważać na znane znaczniki <b>, <i>, <tt>, <a ...> itp.
  Result := sText;
  Result := StringReplace(Result,
                          '<error>',
                          '<span style="background-color:#ffccc9;">',
                          [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '</error>', '</span>', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result,
                          '<warning>',
                          '<span style="background-color:#ffffc7;">',
                          [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '</warning>', '</span>', [rfReplaceAll, rfIgnoreCase])
end { FmtText };


function  TGSkMailBodyBuilderHTML.Header(const   Level:  TGSkMailBodyHeaderLevel;
                                         const   sText:  string)
                                         : TGSkMailBodyBuilder;
begin
  WriteLine('<h%0:u>%1:s</h%0:u>',
            [Level, FmtText(sText)]);
  Result := inherited Header(Level, sText)
end { Header };


function  TGSkMailBodyBuilderHTML.HorizontalLine() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('<hr />');
  Result := inherited HorizontalLine()
end { HorizontalLine };


function  TGSkMailBodyBuilderHTML.ListItemBegin() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  Write('<li>');
  Result := inherited ListItemBegin()
end { ListItemBegin };


function  TGSkMailBodyBuilderHTML.ListItemEnd() : TGSkMailBodyBuilder;
begin
  Write('</li>');
  Result := inherited ListItemEnd()
end { ListItemEnd };


function  TGSkMailBodyBuilderHTML.OrderedListEnd() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('</ol>');
  Result := inherited OrderedListEnd()
end { OrderedListEnd };


function  TGSkMailBodyBuilderHTML.OrderedListStart() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('<ol>');
  Result := inherited OrderedListStart()
end { OrderedListStart };


function  TGSkMailBodyBuilderHTML.Paragraph(const   sText:  string;
                                            const   nOffset:  TGSkMailBodyOffsetLevel)
                                            : TGSkMailBodyBuilder;
var
  sAttr:  string;

begin  { Paragraph }
  ForceNewLine();
  if  nOffset = 0  then
    sAttr := ''
  else
    sAttr := Format(' style="margin-left:%dch;"',
                    [nOffset * STD_OFFSET_INC]);
  WriteLine('<p%s>%s</p>',
            [sAttr, FmtText(sText)]);
  Result := inherited Paragraph(sText, nOffset)
end { Paragraph };


function  TGSkMailBodyBuilderHTML.TableColumn(const   Column:  TGSkMailTableColumn;
                                              const   TextType:  TGSkMailTextType;
                                              const   bLastColumn:  Boolean)
                                              : TGSkMailBodyBuilder;
begin
  Result := inherited TableColumn(Column, TextType, bLastColumn);
  if  bLastColumn
      and (Column.Align = taLeft)  then
    ColumnWidth[ColumnIndex] := 0;
  ForceNewLine();
  WriteLine('<th%s>%s</th>',
            [FmtStyle(ColumnAlign[ColumnIndex],
                      ColumnWidth[ColumnIndex],
                      TextType),
             FmtText(Column.Caption)])
end { TableColumn };


function  TGSkMailBodyBuilderHTML.TableEnd() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('</tbody></table>');
  Result := inherited TableEnd()
end { TableEnd };


function  TGSkMailBodyBuilderHTML.TableHeaderEnd() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('</tr></thead>');
  WriteLine('<tbody>');
  Result := inherited TableHeaderEnd()
end { TableHeaderEnd };


function  TGSkMailBodyBuilderHTML.TableHeaderStart() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('<table border="1" cellpadding="3" cellspacing="0"><thead><tr>');
  Result := inherited TableHeaderStart()
end { TableHeaderStart };


function  TGSkMailBodyBuilderHTML.TableRowCell(const   sCellText:  string;
                                               const   TextType:  TGSkMailTextType)
                                               : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  Result := inherited TableRowCell(sCellText, TextType);
  WriteLine('<td%s>%s</td>',
            [FmtStyle(ColumnAlign[ColumnIndex],
                      ColumnWidth[ColumnIndex],
                      TextType),
             FmtText(sCellText)])
end { TableRowCell };


function  TGSkMailBodyBuilderHTML.TableRowEnd() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('</tr>');
  Result := inherited TableRowEnd()
end { TableRowEnd };


function  TGSkMailBodyBuilderHTML.TableRowStart() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('<tr>');
  Result := inherited TableRowStart()
end { TableRowStart };


function  TGSkMailBodyBuilderHTML.ToString() : string;
begin
  Result := '<html><body>' + sLineBreak
          +   inherited ToString()
          + '</body></html>'
end { ToString };


function  TGSkMailBodyBuilderHTML.UnorderedListEnd() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('</ul>');
  Result := inherited UnorderedListEnd()
end { UnorderedListEnd };


function  TGSkMailBodyBuilderHTML.UnorderedListStart() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine('<ul>');
  Result := inherited UnorderedListStart()
end { UnorderedListStart };

{$ENDREGION 'TGSkMailBodyBuilderHTML'}


{$REGION 'TGSkMailBodyBuilderMarkdown'}

function  TGSkMailBodyBuilderMarkdown.Header(const   Level:  TGSkMailBodyHeaderLevel;
                                             const   sText:  string)
                                             : TGSkMailBodyBuilder;

  procedure  WriteHeaderType1(const   Level:  TGSkMailBodyHeaderLevel;
                              const   sText:  string);
  const
    cLevelUnderline:  array[1..2] of  Char = ('=', '-');

  var
    sBuf:  string;

  begin  { WriteHeaderType1 }
    sBuf := FmtText(sText);
    WriteLine(sBuf);
    WriteLine(StringOfChar(cLevelUnderline[Level],
                           Length(sBuf)))
  end { WriteHeaderType1 };

  procedure  WriteHeaderType2(const   Level:  TGSkMailBodyHeaderLevel;
                              const   sText:  string);
  begin
    WriteLine(StringOfChar('#', Level) + ' ' + FmtText(sText))
  end { WriteHeaderType2 };

begin  { Header }
  ForceNewLine();
  WriteLine();
  if  Level in [1, 2]  then
    WriteHeaderType1(Level, sText)
  else
    WriteHeaderType2(Level, sText);
  WriteLine();
  Result := inherited Header(Level, sText)
end { Header };


function  TGSkMailBodyBuilderMarkdown.BlockQuoteBegin() : TGSkMailBodyBuilder;
begin
  fsOffset := fsOffset + '> ';
  Result := inherited BlockQuoteBegin()
end { BlockQuoteBegin };


function  TGSkMailBodyBuilderMarkdown.BlockQuoteEnd() : TGSkMailBodyBuilder;
begin
  SetLength(fsOffset, Length(fsOffset) - 2);
  Result := inherited BlockQuoteEnd()
end { BlockQuoteEnd };


function  TGSkMailBodyBuilderMarkdown.HorizontalLine() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine();
  WriteLine(StringOfChar('-', cnRightMargin));   // WriteLine('---');
  WriteLine();
  Result := inherited HorizontalLine()
end { HorizontalLine };


function  TGSkMailBodyBuilderMarkdown.ListItemBegin() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  Result := inherited ListItemBegin()
end { ListItemBegin };


function  TGSkMailBodyBuilderMarkdown.ListItemEnd() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  Result := inherited ListItemEnd()
end { ListItemEnd };


function  TGSkMailBodyBuilderMarkdown.OrderedListEnd() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine();
  SetLength(fsOffset, Length(fsOffset) - 3);
  Result := inherited OrderedListEnd();
  fnOrderNo := fOrderStack.Pop()
end { OrderedListEnd };


function  TGSkMailBodyBuilderMarkdown.OrderedListStart() : TGSkMailBodyBuilder;
begin
  fOrderStack.Push(fnOrderNo);
  fnOrderNo := 0;
  fsOffset := fsOffset + '   ';
  Result := inherited OrderedListStart()
end { OrderedListStart };


function  TGSkMailBodyBuilderMarkdown.Paragraph(const   sText:  string;
                                                const   nOffset:  TGSkMailBodyOffsetLevel)
                                                : TGSkMailBodyBuilder;
begin
  if  nOffset <> 0  then
    fsOffset := fsOffset
                + StringOfChar(' ', nOffset * STD_OFFSET_INC);
  WriteLine(FmtText(sText));
  if  nOffset <> 0  then
    SetLength(fsOffset,
              Length(fsOffset) - nOffset * STD_OFFSET_INC);
  WriteLine();
  Result := inherited Paragraph(sText, nOffset)
end { Paragraph };


function  TGSkMailBodyBuilderMarkdown.TableColumn(const   Column:  TGSkMailTableColumn;
                                                  const   TextType:  TGSkMailTextType;
                                                  const   bLastColumn:  Boolean)
                                                  : TGSkMailBodyBuilder;
begin  { TableColumn }
  Result := inherited TableColumn(Column, TextType, bLastColumn);      // najpierw!
  Write('|' + FmtCell(Column.Caption, Column.Width, Column.Align, TextType))
end { TableColumn };


function  TGSkMailBodyBuilderMarkdown.TableEnd() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine();
  Result := inherited TableEnd()
end { TableEnd };


function  TGSkMailBodyBuilderMarkdown.TableHeaderEnd() : TGSkMailBodyBuilder;

var
  nInd:  Integer;
  sCol:  string;

begin  { TableHeaderEnd }
  ForceNewLine();
  for  nInd := 0  to  ColumnCount - 1  do
    begin
      sCol := StringOfChar('-', Max(ColumnWidth[nInd], 3));
      if  ColumnAlign[nInd] in [taLeft, taCenter]  then
        sCol[1] := ':';
      if  ColumnAlign[nInd] in [taCenter, taRight]  then
        sCol[Length(sCol)] := ':';
      Write('|' + sCol)
    end { for nInd };
  ForceNewLine();
  {-}
  Result := inherited TableHeaderEnd();
  {-}
  nInd := ColumnCount -1;
  if  ColumnAlign[nInd] = taLeft  then
    ColumnWidth[nInd] := 0
end { TableHeaderEnd };


function  TGSkMailBodyBuilderMarkdown.TableHeaderStart() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine();
  Result := inherited TableHeaderStart()
end { TableHeaderStart };


function  TGSkMailBodyBuilderMarkdown.TableRowCell(const   sCellText:  string;
                                                   const   TextType:  TGSkMailTextType)
                                                   : TGSkMailBodyBuilder;
begin
  Result := inherited TableRowCell(sCellText, TextType);   // najpierw!
  Write('|' + FmtCell(sCellText, ColumnWidth[ColumnIndex], ColumnAlign[ColumnIndex], TextType))
end { TableRowCell };


function  TGSkMailBodyBuilderMarkdown.TableRowEnd() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  Result := inherited TableRowEnd()
end { TableRowEnd };


function  TGSkMailBodyBuilderMarkdown.TableRowStart() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  Result := inherited TableRowStart()
end { TableRowStart };


function  TGSkMailBodyBuilderMarkdown.UnorderedListEnd() : TGSkMailBodyBuilder;
begin
  ForceNewLine();
  WriteLine();
  SetLength(fsOffset, Length(fsOffset) - 2);
  Result := inherited UnorderedListEnd()
end { UnorderedListEnd };


function  TGSkMailBodyBuilderMarkdown.UnorderedListStart() : TGSkMailBodyBuilder;
begin
  fsOffset := fsOffset + '  ';
  Result := inherited UnorderedListStart()
end { UnorderedListStart };


constructor  TGSkMailBodyBuilderMarkdown.Create();
begin
  inherited;
  fsOffset := '';
  //  <a href="adres">tytuł</a>
  fEMailRegEx.Create('<a\s+href="(.+)">(.+)<\/a>', [roIgnoreCase, roCompiled]);
  fOrderStack := TStack<Cardinal>.Create()
end { TGSkMailBodyBuilder };


function  TGSkMailBodyBuilderMarkdown.FmtCell(const   sText:  string;
                                              const   nWidth:  Integer;
                                              const   Align:  TGSkMailTextAlign;
                                              const   TextType:  TGSkMailTextType)
                                              : string;
begin
  case  TextType  of
    ttInformation:
      Result := sText;
    ttWarning:
      Result := '_' + sText + '_';
    ttError:
      Result := '**' + sText + '**'
  end { case TextType };
  Result := FmtText(Result);
  case  Align  of
    taLeft:
      Result := JustLeft(Result, nWidth);
    taCenter:
      Result := JustCenter(Result, nWidth);
    taRight:
      Result := JustRight(Result, nWidth)
  end { case Align }
end { FmtCell };


function  TGSkMailBodyBuilderMarkdown.FmtText(const   sText:  string)
                                              : string;
begin
  { Convert HTML markers }
  Result := fEMailRegEx.Replace(sText, '[$2]($1)');
  Result := StringReplace(Result, '<b>', '**', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '</b>', '**', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '<i>', '_', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '</i>', '_', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '<tt>', '`', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '<//tt>', '`', [rfReplaceAll, rfIgnoreCase]);
  { Special HTML markers }
  Result := StringReplace(Result, '<error>', '**', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '</error>', '**', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '<warning>', '_', [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '</warning>', '_', [rfReplaceAll, rfIgnoreCase]);
  { Insert line breaks }
  Result := fsOffset
          + WrapText(Result,
                     sLineBreak + fsOffset,
                     [' '],
                     cnRightMargin - Length(fsOffset))
end { FmtText };


function  TGSkMailBodyBuilderMarkdown.GetListItemPrefix() : string;
begin
  case  CurrListType  of
    etListOrdered:
      begin
        Inc(fnOrderNo);
        Result := Format('%u. ', [fnOrderNo])
      end { etListOrdered };
    etListUnordered:
      Result := '- ';
    else
      Result := ''
  end { case CurrElementType }
end { GetListItemPrefix };

{$ENDREGION 'TGSkMailBodyBuilderMarkdown'}


{$REGION 'TGSkMailBodyBuilders'}

function  TGSkMailBodyBuilders.BlockQuote(const   sText:  string)
                                          : TGSkMailBodyBuilder;
begin
  fHTML.BlockQuote(sText);
  fMkDn.BlockQuote(sText);
  Result := Self
end { BlockQuote };


function  TGSkMailBodyBuilders.BlockQuote(const   sTextFmt:  string;
                                          const   TextArgs:  array of const)
                                          : TGSkMailBodyBuilder;
begin
  fHTML.BlockQuote(sTextFmt, TextArgs);
  fMkDn.BlockQuote(sTextFmt, TextArgs);
  Result := Self
end { BlockQuote };


function  TGSkMailBodyBuilders.BlockQuoteBegin() : TGSkMailBodyBuilder;
begin
  fHTML.BlockQuoteBegin();
  fMkDn.BlockQuoteBegin();
  Result := Self
end { BlockQuoteBegin };


function  TGSkMailBodyBuilders.BlockQuoteEnd() : TGSkMailBodyBuilder;
begin
  fHTML.BlockQuoteEnd();
  fMkDn.BlockQuoteEnd();
  Result := Self
end { BlockQuoteEnd };


function  TGSkMailBodyBuilders.BlockQuoteText(const   sTextFmt:  string;
                                              const   TextArgs:  array of const)
                                              : TGSkMailBodyBuilder;
begin
  fHTML.BlockQuoteText(sTextFmt, TextArgs);
  fMkDn.BlockQuoteText(sTextFmt, TextArgs);
  Result := Self
end { BlockQuoteText };


function  TGSkMailBodyBuilders.BlockQuoteText(const   sText:  string)
                                              : TGSkMailBodyBuilder;
begin
  fHTML.BlockQuoteText(sText);
  fMkDn.BlockQuoteText(sText);
  Result := Self
end { BlockQuoteText };


constructor  TGSkMailBodyBuilders.Create();
begin
  inherited Create();
  fHTML := TGSkMailBodyBuilderHTML.Create();
  fMkDn := TGSkMailBodyBuilderMarkdown.Create()
end { Create };


destructor  TGSkMailBodyBuilders.Destroy();
begin
  fMkDn.Free();
  fHTML.Free();
  inherited
end { Destroy };


function  TGSkMailBodyBuilders.GetHTML() : string;
begin
  Result := fHTML.ToString()
end { GetHTML };


function  TGSkMailBodyBuilders.GetMarkDown() : string;
begin
  Result := fMkDn.ToString()
end { GetMarkDown };


function  TGSkMailBodyBuilders.Header(const   Level:  TGSkMailBodyHeaderLevel;
                                      const   sText:  string)
                                      : TGSkMailBodyBuilder;
begin
  fHTML.Header(Level, sText);
  fMkDn.Header(Level, sText);
  Result := Self
end { Header };


function  TGSkMailBodyBuilders.Header(const   Level:  TGSkMailBodyHeaderLevel;
                                      const   sTextFmt:  string;
                                      const   TextArgs:  array of const)
                                      : TGSkMailBodyBuilder;
begin
  fHTML.Header(Level, sTextFmt, TextArgs);
  fMkDn.Header(Level, sTextFmt, TextArgs);
  Result := Self
end { Header };


function  TGSkMailBodyBuilders.HorizontalLine() : TGSkMailBodyBuilder;
begin
  fHTML.HorizontalLine();
  fMkDn.HorizontalLine();
  Result := Self
end { HorizontalLine };


function  TGSkMailBodyBuilders.ListItem(const   sText:  string)
                                        : TGSkMailBodyBuilder;
begin
  fHTML.ListItem(sText);
  fMkDn.ListItem(sText);
  Result := Self
end { ListItem };


function  TGSkMailBodyBuilders.ListItem(const   sTextFmt:  string;
                                        const   TextArgs:  array of const)
                                        : TGSkMailBodyBuilder;
begin
  fHTML.ListItem(sTextFmt, TextArgs);
  fMkDn.ListItem(sTextFmt, TextArgs);
  Result := Self
end { ListItem };


function  TGSkMailBodyBuilders.ListItemBegin() : TGSkMailBodyBuilder;
begin
  fHTML.ListItemBegin();
  fMkDn.ListItemBegin();
  Result := Self
end { ListItemBegin };


function  TGSkMailBodyBuilders.ListItemEnd() : TGSkMailBodyBuilder;
begin
  fHTML.ListItemEnd();
  fMkDn.ListItemEnd();
  Result := Self
end { ListItemEnd };


function  TGSkMailBodyBuilders.ListItemText(const   sTextFmt:  string;
                                            const   TextArgs:  array of const)
                                            : TGSkMailBodyBuilder;
begin
  fHTML.ListItemText(sTextFmt, TextArgs);
  fMkDn.ListItemText(sTextFmt, TextArgs);
  Result := Self
end { ListItemText };


function  TGSkMailBodyBuilders.ListItemText(const   sText:  string)
                                            : TGSkMailBodyBuilder;
begin
  fHTML.ListItemText(sText);
  fMkDn.ListItemText(sText);
  Result := Self
end { ListItemText };


function  TGSkMailBodyBuilders.OrderedListEnd() : TGSkMailBodyBuilder;
begin
  fHTML.OrderedListEnd();
  fMkDn.OrderedListEnd();
  Result := Self
end { OrderedListEnd };


function  TGSkMailBodyBuilders.OrderedListStart() : TGSkMailBodyBuilder;
begin
  fHTML.OrderedListStart();
  fMkDn.OrderedListStart();
  Result := Self
end { OrderedListStart };


function  TGSkMailBodyBuilders.Paragraph(const   sTextFmt:  string;
                                         const   TextArgs:  array of const;
                                         const   nOffset:  TGSkMailBodyOffsetLevel)
                                         : TGSkMailBodyBuilder;
begin
  fHTML.Paragraph(sTextFmt, TextArgs, nOffset);
  fMkDn.Paragraph(sTextFmt, TextArgs, nOffset);
  Result := Self
end { Paragraph };


function  TGSkMailBodyBuilders.Paragraph(const   sText:  string;
                                         const   nOffset:  TGSkMailBodyOffsetLevel)
                                         : TGSkMailBodyBuilder;
begin
  fHTML.Paragraph(sText, nOffset);
  fMkDn.Paragraph(sText, nOffset);
  Result := Self
end { Paragraph };


function  TGSkMailBodyBuilders.TableColumn(const   Column:  TGSkMailTableColumn;
                                           const   TextType:  TGSkMailTextType;
                                           const   bLastColumn:  Boolean)
                                           : TGSkMailBodyBuilder;
begin
  fHTML.TableColumn(Column, TextType, bLastColumn);
  fMkDn.TableColumn(Column, TextType, bLastColumn);
  Result := Self
end { TableColumn };


function  TGSkMailBodyBuilders.TableColumn(const   sCaption:  string;
                                           const   nWidth:  Integer;
                                           const   Align:  TGSkMailTextAlign;
                                           const   TextType:  TGSkMailTextType;
                                           const   bLastColumn:  Boolean)
                                           : TGSkMailBodyBuilder;
begin
  fHTML.TableColumn(sCaption, nWidth, Align, TextType, bLastColumn);
  fMkDn.TableColumn(sCaption, nWidth, Align, TextType, bLastColumn);
  Result := Self
end { TableColumn };


function  TGSkMailBodyBuilders.TableEnd() : TGSkMailBodyBuilder;
begin
  fHTML.TableEnd();
  fMkDn.TableEnd();
  Result := Self
end { TableEnd };


function  TGSkMailBodyBuilders.TableHeader(const   Columns:  array of TGSkMailTableColumn)
                                           : TGSkMailBodyBuilder;
begin
  fHTML.TableHeader(Columns);
  fMkDn.TableHeader(Columns);
  Result := Self
end { TableHeader };


function  TGSkMailBodyBuilders.TableHeaderStart() : TGSkMailBodyBuilder;
begin
  fHTML.TableHeaderStart();
  fMkDn.TableHeaderStart();
  Result := Self
end { TableHeaderStart };


function  TGSkMailBodyBuilders.TableHeaderEnd() : TGSkMailBodyBuilder;
begin
  fHTML.TableHeaderEnd();
  fMkDn.TableHeaderEnd();
  Result := Self
end { TableHeaderEnd };


function  TGSkMailBodyBuilders.TableRow(const   Cells:  array of string)
                                        : TGSkMailBodyBuilder;
begin
  fHTML.TableRow(Cells);
  fMkDn.TableRow(Cells);
  Result := Self
end { TableRow };


function  TGSkMailBodyBuilders.TableRowStart() : TGSkMailBodyBuilder;
begin
  fHTML.TableRowStart();
  fMkDn.TableRowStart();
  Result := Self
end { TableRowStart };


function  TGSkMailBodyBuilders.TableRowCell(const   sCellTextFmt:  string;
                                            const   CellTextArgs:  array of const;
                                            const   TextType:  TGSkMailTextType)
                                            : TGSkMailBodyBuilder;
begin
  fHTML.TableRowCell(sCellTextFmt, CellTextArgs, TextType);
  fMkDn.TableRowCell(sCellTextFmt, CellTextArgs, TextType);
  Result := Self
end { TableRowCell };


function  TGSkMailBodyBuilders.TableRowCell(const   sCellText:  string;
                                            const   TextType:  TGSkMailTextType)
                                            : TGSkMailBodyBuilder;
begin
  fHTML.TableRowCell(sCellText, TextType);
  fMkDn.TableRowCell(sCellText, TextType);
  Result := Self
end { TableRowCell };


function  TGSkMailBodyBuilders.TableRowEnd() : TGSkMailBodyBuilder;
begin
  fHTML.TableRowEnd();
  fMkDn.TableRowEnd();
  Result := Self
end { TableRowEnd };


function  TGSkMailBodyBuilders.UnorderedListEnd() : TGSkMailBodyBuilder;
begin
  fHTML.UnorderedListEnd();
  fMkDn.UnorderedListEnd();
  Result := Self
end { UnorderedListEnd };


function  TGSkMailBodyBuilders.UnorderedListStart() : TGSkMailBodyBuilder;
begin
  fHTML.UnorderedListStart();
  fMkDn.UnorderedListStart();
  Result := Self
end { UnorderedListStart };

{$ENDREGION 'TGSkMailBodyBuilders'}


{$REGION 'TGSkMailBodyBuilderBaseImpl.TColumnDef'}

constructor  TGSkMailBodyBuilderBaseImpl.TColumnDef.Init(const   nWidth:  Integer;
                                                         const   Align:  TGSkMailTextAlign);
begin
  fnWidth := nWidth;
  fAlign := Align
end { Init };

{$ENDREGION 'TGSkMailBodyBuilderBaseImpl.TColumnDef'}


end.

