﻿{: @abstract(Moduł @name zawiera podprogramy ściśle zależne od systemu
             operacyjnego)

   W tym module są funkcje obudowujące API systemu Windows. }
unit  GSkPasLib.SystemUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$WARN SYMBOL_PLATFORM OFF}


uses
  {$IF CompilerVersion > 22} // XE
    WinAPI.Windows, WinAPI.ShellAPI, WinAPI.ShlObj, WinAPI.ActiveX,
    WinAPI.psAPI, WinAPI.TlHelp32,
    System.SysUtils, System.Classes,
    {$IFDEF FMX}
      FMX.Forms,
    {$ELSE}
      Vcl.Forms,
    {$ENDIF -FMX}
  {$ELSE}
    Windows, ShellAPI, ShlObj, ActiveX,
    psAPI, TlHelp32,
    SysUtils, Classes,
    {$IFDEF FMX}
      FMX.Forms,
    {$ELSE}
      Forms,
    {$ENDIF -FMX}
  {$IFEND}
  JclSysUtils;


const
  {:@exclude} tpIdle         = {$IF CompilerVersion > 22}System.{$IFEND}Classes.tpIdle;
  {:@exclude} tpLowest       = {$IF CompilerVersion > 22}System.{$IFEND}Classes.tpLowest;
  {:@exclude} tpLower        = {$IF CompilerVersion > 22}System.{$IFEND}Classes.tpLower;
  {:@exclude} tpNormal       = {$IF CompilerVersion > 22}System.{$IFEND}Classes.tpNormal;
  {:@exclude} tpHigher       = {$IF CompilerVersion > 22}System.{$IFEND}Classes.tpHigher;
  {:@exclude} tpHighest      = {$IF CompilerVersion > 22}System.{$IFEND}Classes.tpHighest;
  {:@exclude} tpTimeCritical = {$IF CompilerVersion > 22}System.{$IFEND}Classes.tpTimeCritical;

  {:@exclude} SW_HIDE            = WinAPI.Windows.SW_HIDE;
  {:@exclude} SW_SHOWNORMAL      = WinAPI.Windows.SW_SHOWNORMAL;
  {:@exclude} SW_NORMAL          = WinAPI.Windows.SW_NORMAL;
  {:@exclude} SW_SHOWMINIMIZED   = WinAPI.Windows.SW_SHOWMINIMIZED;
  {:@exclude} SW_SHOWMAXIMIZED   = WinAPI.Windows.SW_SHOWMAXIMIZED;
  {:@exclude} SW_MAXIMIZE        = WinAPI.Windows.SW_MAXIMIZE;
  {:@exclude} SW_SHOWNOACTIVATE  = WinAPI.Windows.SW_SHOWNOACTIVATE;
  {:@exclude} SW_SHOW            = WinAPI.Windows.SW_SHOW;
  {:@exclude} SW_MINIMIZE        = WinAPI.Windows.SW_MINIMIZE;
  {:@exclude} SW_SHOWMINNOACTIVE = WinAPI.Windows.SW_SHOWMINNOACTIVE;
  {:@exclude} SW_SHOWNA          = WinAPI.Windows.SW_SHOWNA;
  {:@exclude} SW_RESTORE         = WinAPI.Windows.SW_RESTORE;
  {:@exclude} SW_SHOWDEFAULT     = WinAPI.Windows.SW_SHOWDEFAULT;
  {:@exclude} SW_FORCEMINIMIZE   = WinAPI.Windows.SW_FORCEMINIMIZE;
  {:@exclude} SW_MAX             = WinAPI.Windows.SW_MAX;


type
  {$IF CompilerVersion <= 22}
    {: @exclude }
    SIZE_T = DWORD;
    {: @exclude }
    DWORDLONG = DWORD;
  {$IFEND}

  {: @exclude }
  TThreadPriority = {$IF CompilerVersion > 22}System.{$IFEND}Classes.TThreadPriority;

  {: @abstract(Wartości typu @name używane są przez podprogramy definiujące
               priorytet aplikacji)

     Takie wartości występują w funkcji @link(GetClassPriority) oraz w
     procedurze @link(SetPriority).

     Wartości @code(cpBelowNormal) oraz @code(cpAboveNormal) nie są wspierane
     w systemach Windows 95/98/Me.

     Pełny opis priorytetów jest na stronie:
     http://msdn.microsoft.com/library/en-us/dllproc/base/setpriorityclass.asp }
  TClassPriority = (cpIdle, cpBelowNormal, cpNormal, cpAboveNormal, cpHigh,
                    cpRealtime);

  {: @abstract(Typ @name definiuje rozdzaj zasobu dla funkcji @link(NetAddConnection))

     @value nrDisk Definiowane jest połączenie z dyskiem sieciowym.
     @value nrPrint Definiowane jest połączenie z drukarką sieciową. }
  TNetResourceType = (nrDisk  = RESOURCETYPE_DISK,
                      nrPrint = RESOURCETYPE_PRINT);

  {: @abstract(Typ @name definiuje flagi połączenia dla funkcji @link(NetAddConnection))

     @value(ncInteractive Wskazuje, że system może komunikować się z
                          użytkownikiem w celu autentykacji.)
     @value(ncPrompt Powoduje, że system nie używa domyślnej nazwy i hasła
                     użytkownika; ta flaga jest ignorowana jeżeli flaga
                     @code(ncInteractive) nie jest ustawiona.)
     @value(ncRedirect Wymusza przekierowanie lokalnego urządzania na urządzenie
                       sieciowe. Ta właściwość ma znaczenie gdy w wywołaniu
                       funkcji nie wskazano lokalnej nazwy urządzenia. Jeżeli
                       flaga @name nie jest ustawiona i nie wymaga wskazania
                       lokalnego urządzenia do przekierowania, to system wybiera
                       takie urządzenie samodzielnie.)
     @value(ncUpdateProfile Wskazuje czy że zdefiniowane połączenie ma być
                            zapamiętane. Jeżeli ta flaga jest ustawiona, to
                            system będzie automatycznie przywracać to połączenie
                            przy następnym logowaniu użytkownika w systemie.
                            System zapamiętuje i przywraca tylko połączenia
                            przekierowywane poprzez lokalne urządzenie (gdy
                            wskazano lokalną nazwę urządzenia).)

     Szczegółowy opis poszczególnych wartości można znaleźć na stronie
     http://msdn.microsoft.com/library/en-us/wnet/wnet/wnetaddconnection2.asp. }

  TNetConnectFlag = (ncInteractive, ncPrompt, ncRedirect, ncUpdateProfile);
  TNetConnectFlags = set of TNetConnectFlag;

  TShellExecWaitMode = (wmNoWait, wmWaitForInputIdle, wmWaitForTerminate);

  TGetGuidStrOpt = (goNoCurlyBrackets, goNoHyphens);
  TGetGuidStrOpts = set of  TGetGuidStrOpt;


{: @abstract(Procedura generuje wyjątek dla wskazanego kodu błędu)

   Ta procedura działa podobnie jak standardowa funkcja @code(RaiseLastOSError).
   Należy ją wywoływać z parametrem różnym od NO_ERROR (0).

   @param(nErrNo Wskazuje kod błedu systemu operacyjnego)

   @note(Procedura @name wywołuje funkcję WinAPI @code(SetLastError).

         Zamiast funkcji @name można również użyć standardową funkcję
         @code(RaiseLastOSError) z modułu @code(System.SysUtils). Ta funkcja
         generuje wyjątek @code(EOSError), ale nie zmienia stanu w WinAPI.)

   @seeAlso(CheckOSError)  }
procedure  RaiseOSError(const   nErrNo:  LongWord);


{: @abstract(Sprawdza czy wystąpił błąd systemowy)

   Procedura @name przydaje się, gdy funkcja systemowa zwraca jako swój wynik kod
   błędu. Jeżeli parametr @code(nErrNo) jest kodem błedu, czyli ma wartość inną
   niż NO_ERROR (0) to wywołuje procedurę @link(RaiseOSError).

   @seealso(RaiseOSError)  }
procedure  CheckOSError(const   nErrNo:  LongWord);                  overload;


{: @abstract(Sprawdza czy wystąpił błąd systemowy)

   Procedura @name przydaje się, gdy funkcja systemowa zwaraca wynik będący
   wartością logiczną. Jeżeli parametr @code(bReturn) ma wartość @false to
   wywołuje procedurę @link(RaiseOSError).

   @seealso(RaiseOSError)  }
procedure  CheckOSError(const   bReturn:  LongBool);                 overload;


function  ShellExec(const   sFileName:  TFileName;
                    const   sParameters:  string;
                    const   sVerb:  string;   // 'open' 'print', 'properties', 'explore', ...
                    const   sWorkingDir:  string;
                    const   nCmdShow:  Integer;
                    const   WaitMode:  TShellExecWaitMode;
                    const   pnExitCode:  PLongWord = nil)
                    : LongWord;                                      overload;


function  ShellExec(const   sFileName:  TFileName;
                    const   sParameters:  string;
                    const   WaitMode:  TShellExecWaitMode;
                    const   pnExitCode:  PLongWord = nil)
                    : LongWord;                                      overload;


{: @abstract(Wykonuje skazany program lub program skojarzony ze wskazanym plikiem)

   @param(sFileName Wskazuje program do wykonania lub plik. W drugim przypadku
                    uruchomiony będzie program skojarzony w systemie z odpowiednim
                    typem pliku.)
   @param(sParameters Wskazuje parametry uruchamianego programu.)
   @param(sVerb Wskazuje akcję do wykonania przez system. Może to być 'open',
                'print', 'properties', 'edit', 'explore'. Pusty napis oznacza
                działanie domyślnie zdefiniowane w systemie)
   @param(nCmdShow Wskazuje jakie otworzyć okno programu.)
   @param(bWaitFor Wskazuje czy procedura ma czekać na zakończenie działania
                   uruchomionego programu.)
   @param(pnExitCode Zawiera adres zmiennej, do której funkcja powinna zapisać
                     kod zakończenia uruchomionego programu. Ten kod jest
                     zapamiętywany tylko wtedy gdy parametr @code(bWaitFor)
                     ma wartość @true.)

   @return(Wynikiem funkcji jest albo NO_ERROR (0), albo systemowy kod błędu.
           Kod błędu można przekształcić w wyjątek przy pomocy procedury
           @link(RaiseOSError) lub @link(CheckOSError). ) }
function  ShellExec(const   sFileName:  TFileName;
                    const   sParameters:  string;
                    const   sVerb:  string;   // 'open' 'print', 'properties', 'explore', ...
                    const   sWorkingDir:  string = '';
                    const   nCmdShow:  Integer = SW_SHOWNORMAL;
                    const   bWaitFor:  Boolean = True;
                    const   pnExitCode:  PLongWord = nil)
                    : LongWord;                                      overload;


{: @abstract(Wykonuje skazany program lub program skojarzony ze wskazanym plikiem)

   @param(sFileName Wskazuje program do wykonania lub plik. W drugim przypadku
                    uruchomiony będzie program skojarzony w systemie z odpowiednim
                    typem pliku.)
   @param(sParameters Wskazuje parametry uruchamianego programu.)
   @param(bWaitFor Wskazuje czy procedura ma czekać na zakończenie działania
                   uruchomionego programu.)
   @param(pnExitCode Zawiera adres zmiennej, do której funkcja powinna zapisać
                     kod zakończenia uruchomionego programu. Ten kod jest
                     zapamiętywany tylko wtedy gdy parametr @code(bWaitFor)
                     ma wartość @true.)

   @return(Wynikiem funkcji jest albo NO_ERROR (0), albo systemowy kod błędu.
           Kod błędu można przekształcić w wyjątek przy pomocy procedury
           @link(RaiseOSError) lub @link(CheckOSError). ) }
function  ShellExec(const   sFileName:  TFileName;
                    const   sParameters:  string;
                    const   bWaitFor:  Boolean;
                    const   pnExitCode:  PLongWord = nil)
                    : LongWord;                                      overload;


{: @abstract(Uruchamia wskazany program działający w trybie znakowym i przechwytuje jego wyniki.)

   @note(W przypadku problemów można spróbować użyć alternatywne metody:
         @unorderedList(@itemSpacing(Compact)
           @item(Funkcja @code(Execute) z modułu @code(JclSysUtils))
           @item(Klasa @code(TJvCreateProcess) z modułu @code(JvCreateProcess))
           @item(@italic(DOSCommand) z pakietu
                 @url(https://github.com/TurboPack/DOSCommand TurboPack))
           @item(@italic(Console Application Runner Classes) z witryny
                 @url(https://delphidabbler.com/software/consoleapp delphiDabbler.com). @br
                 Zobacz również: @url(https://lib-docs.delphidabbler.com/ConsoleApp/faqs.html))))

   @param(sDosApp Program działający w trybie znakowym, wewentualnie uzupełniony
                  o parametry programu. Należy stosować takie same zasady formatowania,
                  jakie sa niezbędne do wykonania danego programu w trybie znakowym
                  (np. ścieżka zawierająca odstępy musi być ujęta w cudzysłowy))
   @param(fnGetBuffer Funkcja odbierająca tekst ze standardowego wyjścia programu.
                      Jeżeli funkcja zwróci @False, to procedura nie czeka na
                      zakończenie działania programu.)
   @param(sWorkDir Parametr pozwala wskazać folder, w którym ma działać
                   uruchamiany program)
   @param(nBufferSize Wielkość bufora (w bajtach) używanego do przechwytywania
                      danych zapisywanych przez uruchomiony proces. @br
                      Im większy bufor, tym dane będą szybciej przetważane. @br
                      Im mniejszy bufor, tym dane szybciej będą przekazywane do
                      programu nadrzędnego. @br
                      Jeżeli wartość parametru jest mniejsza lub równa zero, to
                      używany jest bufor o domyślnym rozmiarze 2048 bajtów.) :}
procedure  RunDosCmd({const}  sDosApp:  string;
                     const   fnGetBuffer:  TFunc<AnsiString, Boolean>;
                     const   sWorkDir:  string = '';
                     const   nBufferSize:  Integer = 2048);          overload;

{: @abstract(Uruchamia wskazany program działający w trybie znakowym i przechwytuje jego wyniki.)

   @note(W przypadku problemów można spróbować użyć alternatywne metody:
         @unorderedList(@itemSpacing(Compact)
           @item(Funkcja @code(Execute) z modułu @code(JclSysUtils))
           @item(Klasa @code(TJvCreateProcess) z modułu @code(JvCreateProcess))
           @item(@italic(DOSCommand) z pakietu
                 @url(https://github.com/TurboPack/DOSCommand TurboPack))
           @item(@italic(Console Application Runner Classes) z witryny
                 @url(https://delphidabbler.com/software/consoleapp delphiDabbler.com).
                 Zobacz również: @url(https://lib-docs.delphidabbler.com/ConsoleApp/faqs.html))))

   @param(sDosApp Program działający w trybie znakowym, wewentualnie uzupełniony
                  o parametry programu. Należy stosować takie same zasady formatowania,
                  jakie są niezbędne do wykonania danego programu w trybie znakowym
                  (np. ścieżka zawierająca odstępy musi być ujęta w cudzysłowy))
   @param(sWorkDir Parametr pozwala wskazać folder, w którym ma działać
                   uruchamiany program)
   @param(nBufferSize Wielkość bufora (w bajtach) używanego do przechwytywania
                      danych zapisywanych przez uruchomiony proces. @br
                      Im większy bufor, tym dane będą szybciej przetważane. @br
                      Im mniejszy bufor, tym dane szybciej będą przekazywane do
                      programu nadrzędnego. @br
                      Jeżeli wartość parametru jest mniejsza lub równa zero, to
                      używany jest bufor o domyślnym rozmiarze 2048 bajtów. )

   @return(Funkcja zwraca tekst zapisany przez program do standardowego wyjścia.) :}
function  RunDosCmd(const   sDosApp:  string;
                    const   sWorkDir:  string = '';
                    const   nBufferSize:  Integer = 2048)
                    : string;                                        overload;



{: @abstract(Funkcja @name definiuje nowe połączenie z dyskiem
            lub drukarką sieciową)

   @param(ResuourceType Wskazuje rodzaj zasobu zdefiniowany przez typ
                        @link(TNetResourceType).)
   @param(sRemoteName Wskazuje ścieżkę do podłączanego zasobu.)
   @param(sLocalName Wskazuje lokalną nazwę dla podłączanego zasobu. Dla dysku
                     powinien to być napis identyfikujący dysk (np. 'Z:'), a dla
                     drukarki --- napis identyfikujący drukarkę (np. 'LPT2:').
                     Jeżeli jest pustym napisem to urządzenie sieciowe jest
                     podłączane bez przekierowywania go na urządzenie lokalne.)
   @param(sUserName Wskazuje nazwę użytkownika. Jeżeli jest pustym napisem to
                    połączenie następuje dla domyślnego użytkownika.)
   @param(sPassword Wskazuje hasło użytkownika. Jeżeli jest pustym napisem to
                    połączenie następuje z wykorzystaniem domyślnego hasła.)
   @param(setFlags Definiuje flagi połączenia opisane w @link(TNetConnectFlags).)

   @return(Wynikiem funkcji jest albo NO_ERROR (0), albo systemowy kod błędu.
           Kod błędu można przekształcić w wyjątek przy pomocy procedury
           @link(RaiseOSError) lub @link(CheckOSError). )

   Zefiniowane połączenie z dyskiem lub drukarką sieciową można usunąć przy
   pomocy funkcji @link(NetCancelConnection).

   @seeAlso(NetCancelConnection)  }
function  NetAddConnection(const   ResourceType:  TNetResourceType;
                           const   sRemoteName:  string;
                           const   sLocalName:  string = '';
                           const   sUserName:  string = '';
                           const   sPassword:  string = '';
                           const   setFlags:  TNetConnectFlags = [])
                           : LongWord;


{: @abstract(Usuwa połączenie z zasobem sieciowym)

   @param(sName Wskazuje zasób. Jest to albo lokalna nazwa (np. 'Z:' lub 'LPT2'),
                albo nazwa zasobu sieciowego (np. '\\base\rekord').)
   @param(setFlags Uwzględniana jest tylko wartość @code(ncUpdateProfile).)
   @param(bForce Informuje czy wymuszać rozłączenie nawet gdy są otwarte pliki
                 związane z rozłączanym zasobem.)

   @return(Wynikiem funkcji jest albo NO_ERROR (0), albo systemowy kod błędu.
           Kod błędu można przekształcić w wyjątek przy pomocy procedury
           @link(RaiseOSError) lub @link(CheckOSError). )

   Połączenie z zasobem sieciowym można zdefiniować przy pomocy funkcji
   @link(NetAddConnection).

   @seeAlso(NetAddConnection) }
function  NetCancelConnection(const   sName:  string;
                              const   setFlags:  TNetConnectFlags = [];
                              const   bForce:  Boolean = False)
                              : LongWord;


{$IFNDEF FMX}
{: @abstract(Funkcja uruchamia systemowy dialog pozwalający wskazać komputer
             w otoczeniu sieciowym)

   @param(sTitle Pozwala wskazać tytuł dialogu.)

   @return(Wynikiem funkcji jest albo nazwa komputera, albo pusty napis --- jeżeli
           użytkownik anulował działanie) }
function  BrowseForComputer(const   sTitle:  string = 'Wskaż komputer')
                            : string;
{$ENDIF -FMX}


{: @abstract(Definiuje priorytet aplikacji i/lub priorytet wątku)

   @param(ClassPriority Parametr typu @link(TClassPriority) wskazuje priorytet
                        aplikacji.)
   @param(ThreadPriority Parametr standardowego typu TThreadPriority wskazuje
                         priorytet wątku.) }
procedure  SetPriority(const   ClassPriority:  TClassPriority;
                       const   ThreadPriority:  TThreadPriority);    overload;

procedure  SetPriority(const   ClassPriority:  TClassPriority);      overload;

procedure  SetPriority(const   ThreadPriority:  TThreadPriority);    overload;

{: @abstract(Pobiera priorytet aplikacji) }
function  GetClassPriority() : TClassPriority;

{: @abstract(Pobiera priorytet bieżącego wątku) }
function  GetThreadPriority() : TThreadPriority;


{: @abstract(Informuje czy użytkownik ma uprawnienia administratora)

   @seeAlso(IsElevated) }
function  IsAdminLoggedOn() : Boolean;

{: @abstract(Informuje czy bieżący proces działa z podwyższonymi uprawnieniam)

   Użytkownik może być administratorem, ale program może być uruchomiony
   z normalnym poziomem uprawnień. Funkcja @name informuje, czy program jest
   uruchomiony z podwyższonymi uprawnieniami (@italic(run as administrator)).

   @seeAlso(IsAdminLoggedOn) }
function  IsElevated() : Boolean;


{: @abstract Wynikiem funkcji jest obszar pulpitu.

   Funkcja zwraca współrzędne obszaru pulpitu. Funkcja uwzględnia miejsce
   zajęte przez systemowy pasek zadań. }
function  GetDesktopArea() : TRect;


{: @abstract Funkcja informuje, czy program działa w systemie 64-bitowym.

   Funkcja @name informuje, czy aplikacja działa pod kontrolą 64-bitowej edycji
   systemu operacyjnego.

   @note(Funkcja zwraca @true lub @false, jeżeli aplikacja jest 32-bitowe.

         Jeżeli aplikacja jest 64-bitowa, to może działać wyłącznie w 64-bitowej
         edycji systemu operacyjnego. Dlatego w tym przypaku funkcja zawsze zwraca
         @true.) }
function  Is64BitOperatingSystem() : Boolean;


{: @abstract Sprawdza czy wskazany proces działa w systemie.

   @param(sProcess Nazwa procesu.)

   @returns(Wynik funkcji informuje czy wskazany proces jest wśród procesów
            wykonujących sić w systemie.)

   Nazwa procesu musi być taka sama jak w systemowym Menadżerze zadań

   @bold(UWAGA!)@br
   Ta funkcja może być wykorzystywana tylko w systemie Windows NT i późniejszych.@br
   Funkcja @bold(nie) jest dostępna w systemach Windows 95, Windows98 ani
   Windows ME.

   @seeAlso(GetProcessList)
   @seeAlso(TerminateNamedProcess) }
function  IsProcessRunning(const   sProcess:  string)
                           : Boolean;                                overload;

{: @abstract Sprawdza, czy wskazane procesy działają w systemie.

   @param(ProcessList Nazwy procesów.)
   @param(bCheckAll Jeżeli ten parametr ma wartość @false, to funkcja sprawdza,
                    czy w systemie działa którykolwiek ze wskazanych procesów. @br
                    Jeżeli ten parametr ma wartość @true, to funkcja sprawdza,
                    czy w systemie działają wszystkie wskazane procesy.)

   @returns(Jeżeli parametr @code(bCheckAll) ma wartość @false, to wynikiem funkcji
            jest nazwa pierwszego @bold(działającego) procesu. Jeżeli żaden proces
            nie działa, to wynikiem funkcji jest pusty napis.

            Jeżeli parametr @code(bCheckAll) ma wartość @true, to wynikiem funkcji
            jest nazwa pierwszego @bold(niedziałającego) procesu. Jeżeli wszystkie
            procesy działają, to wynikiem funkcji jest pusty napis.)

   Nazwy procesów muszą być takie same jak w systemowym Menadżerze zadań

   @bold(UWAGA!)@br
   Ta funkcja może być wykorzystywana tylko w systemie Windows NT i późniejszych.@br
   Funkcja @bold(nie) jest dostępna w systemach Windows 95, Windows98 ani
   Windows ME.

   @seeAlso(GetProcessList)
   @seeAlso(TerminateNamedProcess) }
function  IsProcessRunning(const   ProcessList:  TStringList;
                           const   bCheckAll:  Boolean = False)
                           : string;                                 overload;

{: @abstract Zwraca listę procesów działających aktualnie w systemie.

   @param(@abstract List Lista procesów działających w systemie.

          Parametrem aktualnym powinien być obiekt będący potomkiem obiektu
          @code(TStrings), na przykład @code(TStringList)
          lub @code(THashedStringList). )

   Z listy @code(List) usuwane są wszystkie pozycje. Następnie lista wypełniana
   jest nazwami procesów działających w systemie w chwili wywołania procedury.

   Jeżeli kod wywołujący tę procedurę używa również moduł @link(GSkPasLib.Classes),
   to dodatkowo można użyć właściwość @code(Cardinal(List.ObjAsInteger[])), aby
   otrzymać identyfikator procesu.

   @bold(UWAGA!)@br
   Ta funkcja może być wykorzystywana tylko w systemie Windows NT i późniejszych.@br
   Funkcja @bold(nie) jest dostępna w systemach Windows 95, Windows98 ani
   Windows ME.

   @seeAlso(IsProcessRunning)
   @seeAlso(TerminateNamedProcess) }
procedure  GetProcessList(const   List:  TStrings);


{: @abstract Kończy działanie wskazanego procesu.

   @param(sProcess Nazwa procesu.)
   @param(nExitCode Kod zakończenia procesu. @br
                    Domyślnie: 0 (zero))
   @param(bTerminateAllInstances Czy kończyć działanie wszystkich procesów
                                 o wskazanej nazwie. @br
                                 Domyślnie: @true)

   @returns(@true oznacza, że proces został odnaleziony i zatrzymany. @br
            @false oznacza, że wskazanego procesu nie odnaleziono w systemie.)

   Nazwa procesu musi być taka sama jak w systemowym Menadżerze zadań.
   Jeżeli wskazanego procesu nie ma w systemie to funkcja nic nie robi.

   Funkcja @bold(nie) kończy działania bieżącej aplikaci (nie zabija swojego procesu).

   @bold(UWAGA!)@br
   Ta funkcja może być wykorzystywana tylko w systemie Windows NT i późniejszych.@br
   Funkcja @bold(nie) jest dostępna w systemach Windows 95, Windows98 ani
   Windows ME.

   @seeAlso(IsProcessRunning)
   @seeAlso(GetProcessList) }
function  TerminateNamedProcess(const   sProcess:  string;
                                const   nExitCode:  LongWord = 0;
                                const   bTerminateAllInstances:  Boolean = True)
                                : Boolean;


{: @abstract Powoduje zwolnienie nieużywanej pamięci RAM.

   Ta procedura została skopiowana z artykułu "Design Your Delphi Program
   in a Way That it Keeps its Memory Usage in Check", opublikowanego na stronie
   http://delphi.about.com/od/windowsshellapi/ss/setprocessworkingsetsize-delphi-program-memory-optimize.htm

   Procedura powinna być wywoływana po zwolnieniu większej ilości pamięci oraz
   przed okresami dłuższej bezczynności. }
procedure  TrimAppMemorySize();


{: @abstract Informuje o liczbie wątków w bieżącym procesie.

   W niektórych sytuacjach oprócz wątków tworzonych jawnie w aplikacji, system
   opreacyjny tworzy dodatkowe wątki.

   Na przykład w aplikacji przeglądającej rekurencyjnie foldery i usuwającej
   niektóre pliki do kosza (aplikacji napisanej jednowątkowo) liczba wątków
   realizujących to zadanie może wzrosnąć do kilkuset (w Windows Vista).

   Funkcja @name pozwala sprawdzać, ile wątków aktualnie realizuje bieżący proces.
   Mając taką informację można na przykład w pętli wstrzymać działanie aplikacji
   do czasu, gdy liczba wątków nie spadnie poniżej założonego limitu, na przykład: @br
   @code(while GetCurrentThreadCount() > 100 do Sleep(10);)

   @seeAlso(GetProcessorCount)  }
function  GetCurrentThreadCount() : Integer;


{: @abstract Zwraca liczbę procesorów w wystemie.

   @seeAlso(GetCurrentThreadCount)  }
function  GetProcessorCount() : Integer;


{: @abstract Zwraca wartość globalnej zmiennej środowiskowej.

   Standardowa funkcja @code(GetEnvironmentVariable) z modułu @code(SysUtils)
   zwraca wartość zmiennej środowiskowej bieżącego użytkownika. Natomiast
   funkcja @name zwraca wartości globalnych zmiennych środowiskowych.

   Na przykład w systemie Windows Vista:
   @definitionList(
     @itemLabel(@code(GetEnvironmentVariable('Temp')))
       @item(Zwróci napis @code('c:\Users\NazwaUżytkownika\AppData\Local\Temp'))
     @itemLabel(@code(@name@('Temp'@)))
       @item(Zwróci napis @code('c:\WINDOWS\Temp'))
   )
   @seeAlso(GetCurrentThreadCount)
   @seeAlso(GetSysTempPath)  }
function  GetSysEnvironmentVariable(const   sName:  string)
                                    : string;

{: @abstract Zwraca ścieżkę do systemowego folderu plików roboczych.

   Funkcja @name zwraca ścieżkę do systemowego folderu plików roboczych. Na przykład
   w systemie Windows Vista funkcja standardowo zwraca @code('c:\WINDOWS\Temp'). }
function  GetSysTempPath() : string;


{$IFDEF UNICODE} // {$IF Declared(SysUtils.TLanguages.LocaleName)}

{: @abstract Dla danego identyfikatora języka zwraca jego kod ISO 639.

   @param(LangID Identyfikator języka.)
   @returns(Kod języka, zgodny z normą ISO 649.)

   Na przykład wywołanie @code(GetLanguageCodeFromLCID($0415)) zwróci napis
   @code('pl-PL').

   @bold(Uwaga!) @br
   Ta funkcja jest dostępna tylko w nowszych wersjach Delphi (np. XE1 i nowszych)
   --- tych, które udostępniają @code(TLanguages.LocaleName). }
function  GetLanguageCodeFromLCID(const   LangID:  TLCID)
                                  : string;

{$ENDIF}


{: @abstract Zwraca ilość pamięci zajętą przez bieżący proces.

   Funkcja @name zwraca ilość pamięci (@code(WorkingSetSize)) zajętą przez
   bieżący proces. }
function  GetCurrentProcessMemorySize() : SIZE_T;

{: @abstract Zwraca ilość dostępnej pamięci fizycznej.

   Funkcja @name zwraca ilość dostępnej fizycznej pamięci systemu (w chwili
   wywołania tej funkcji). }
function  GetAvailablePhysicalMemory() : DWORDLONG;


{: @abstract Tworzy GUID i zwraca go jako napis. }
function  GetGUIDStr(const   Options:  TGetGuidStrOpts = [])
                     : string;


{$IFDEF WIN32}
{: @abstract Steruje przekierowaniem klucza Registry.

   Jeżeli aplikacja 32-bitowa działa w środowisku 64-bitowego systemu operacyjnego,
   to Windows automatycznie przekierowuje dostęp do niektórych kluczy Registry.
   Na przykład jeżeli aplikacja chce otworzyć klucz `HKLM\Software\OleForRetail`,
   to w takim środowisku Windows automatycznie przekieruje do klucza
   `SOFTWARE\WOW6432Node\OLEforRetail`. Procedura @name pozwala wyłączyć takie
   automatyczne przekierowanie.

   @note(Procedura @name jest dostępna tylko w aplikacjach 32-bitowych.
         W aplikacjach 64-bitowych takie przekierowanie nie występuje.) }
procedure  RegistryRedirection(const   RegKey:  HKEY;
                               const   bEnable:  Boolean);
{$ENDIF WIN32}


implementation


uses
  GSkPasLib.PasLibInternals, GSkPasLib.Classes;


const
  BELOW_NORMAL_PRIORITY_CLASS = $00004000;
  ABOVE_NORMAL_PRIORITY_CLASS = $00008000;
  ClassPriorityConv:  array[TClassPriority] of  DWORD = (
                         {cpIdle    }    IDLE_PRIORITY_CLASS,
                         {cpBelowNormal} BELOW_NORMAL_PRIORITY_CLASS,
                         {cpNormal  }    NORMAL_PRIORITY_CLASS,
                         {cpAboveNormal} ABOVE_NORMAL_PRIORITY_CLASS,
                         {cpHigh    }    HIGH_PRIORITY_CLASS,
                         {cpRealtime}    REALTIME_PRIORITY_CLASS );
  ThreadPriorityConv:  array[TThreadPriority] of  Integer = (
                         {tpIdle        } THREAD_PRIORITY_IDLE,
                         {tpLowest      } THREAD_PRIORITY_LOWEST,
                         {tpLower       } THREAD_PRIORITY_BELOW_NORMAL,
                         {tpNormal      } THREAD_PRIORITY_NORMAL,
                         {tpHigher      } THREAD_PRIORITY_ABOVE_NORMAL,
                         {tpHighest     } THREAD_PRIORITY_HIGHEST,
                         {tpTimeCritical} THREAD_PRIORITY_TIME_CRITICAL );


function  ExpandEnvironmentStringsForUserA(hToken:  THandle;
                                           lpSrc:   LPCSTR;
                                           lpDest:  LPSTR;
                                           dwSize:  DWORD)
                                           : BOOL;                   stdcall;
  external 'userenv.dll' name 'ExpandEnvironmentStringsForUserA';


function  ExpandEnvironmentStringsForUserW(hToken:  THandle;
                                           lpSrc:   LPCWSTR;
                                           lpDest:  LPWSTR;
                                           dwSize:  DWORD)
                                           : BOOL;                   stdcall;
  external 'userenv.dll' name 'ExpandEnvironmentStringsForUserW';


function  ExpandEnvironmentStringsForUser(hToken:  THandle;
                                          lpSrc:   LPCTSTR;
                                          lpDest:  LPTSTR;
                                          dwSize:  DWORD)
                                          : BOOL;                    stdcall;
  external 'userenv.dll' name {$IFDEF UNICODE}'ExpandEnvironmentStringsForUserW'
                                       {$ELSE}'ExpandEnvironmentStringsForUserA'{$ENDIF UNICODE};




function  ShellExec(const   sFileName:  TFileName;
                    const   sParameters:  string;
                    const   sVerb:  string;
                    const   sWorkingDir:  string;
                    const   nCmdShow:  Integer;
                    const   WaitMode:  TShellExecWaitMode;
                    const   pnExitCode:  PLongWord)
                    : LongWord;
var
  SEI:  TShellExecuteInfo;

begin  { ShellExec }
  FillChar(SEI, SizeOf(SEI), #0);
  SEI.cbSize := SizeOf(SEI);
  SEI.fMask := SEE_MASK_DOENVSUBST
               or SEE_MASK_FLAG_NO_UI
               or SEE_MASK_NOCLOSEPROCESS
               or SEE_MASK_FLAG_DDEWAIT;
  SEI.lpFile := PChar(sFileName);
  SEI.lpParameters := PCharOrNil(sParameters);
  SEI.lpVerb := PCharOrNil(sVerb);
  SEI.lpDirectory := PCharOrNil(sWorkingDir);
  SEI.nShow := nCmdShow;
  if  ShellExecuteEx(@SEI)  then
    if  WaitMode <> wmNoWait  then
      begin
        WaitForInputIdle(SEI.hProcess, INFINITE);
        if  WaitMode = wmWaitForTerminate  then
          begin
            while  WaitForSingleObject(SEI.hProcess, 25) = WAIT_TIMEOUT  do
              Application.ProcessMessages();
            if  pnExitCode <> nil  then
              GetExitCodeProcess(SEI.hProcess, pnExitCode^);
            CloseHandle(SEI.hProcess);
          end { WaitMode = wmWaitForTerminate };
        Result := NO_ERROR
      end { bWaitFor }
    else
      Result := NO_ERROR
  else
    Result := GetLastError()
end { ShellExec };


function  ShellExec(const   sFileName:  TFileName;
                    const   sParameters:  string;
                    const   WaitMode:  TShellExecWaitMode;
                    const   pnExitCode:  PLongWord)
                    : LongWord;
begin
  Result := ShellExec(sFileName, sParameters, '', '', SW_SHOWNORMAL, WaitMode, pnExitCode)
end { ShellExec };


function  ShellExec(const   sFileName:  TFileName;
                    const   sParameters, sVerb:  string;
                    const   sWorkingDir:  string;
                    const   nCmdShow:  Integer;
                    const   bWaitFor:  Boolean;
                    const   pnExitCode:  PLongWord)
                    : LongWord;
const
  cWaitModeConv:  array[Boolean] of  TShellExecWaitMode = (wmNoWait, wmWaitForTerminate);

begin  { ShellExec }
  Result := ShellExec(sFileName, sParameters, sVerb, sWorkingDir, nCmdShow,
                      cWaitModeConv[bWaitFor],
                      pnExitCode)
end { ShellExec };


function  ShellExec(const   sFileName:  TFileName;
                    const   sParameters:  string;
                    const   bWaitFor:  Boolean;
                    const   pnExitCode:  PLongWord)
                    : LongWord;
begin
  Result := ShellExec(sFileName, sParameters,
                      '', '', SW_SHOWNORMAL,
                      bWaitFor, pnExitCode)
end { ShellExec };


procedure  RunDosCmd({const}  sDosApp:  string;
                     const   fnGetBuffer:  TFunc<AnsiString, Boolean>;
                     const   sWorkDir:  string;
                     const   nBufferSize:  Integer);
const
  cnDefaultBufferSize = 2048;

var
  lSecAttr:  TSecurityAttributes;
  hReadableEndOfPipe, hWriteableEndOfPipe:  THandle;
  lStartupInfo:  TStartUpInfo;
  lProcessInfo:  TProcessInformation;
  pszBuffer:  PAnsiChar;
  nBytesRead:  DWORD;
  bWasOK:  Boolean;

begin  { RunDosCmd }
  if  nBufferSize <= 0  then
    begin
      RunDosCmd(sDosApp, fnGetBuffer, sWorkDir, cnDefaultBufferSize);
      Exit
    end { nBufferSize <= 0 };
  {-}
  lSecAttr := Default(TSecurityAttributes);
  lSecAttr.nLength := SizeOf(TSecurityAttributes);
  lSecAttr.bInheritHandle := True;
  {-}
  if  CreatePipe({var}hReadableEndOfPipe, {var}hWriteableEndOfPipe, @lSecAttr, 0)  then
    try
      pszBuffer := AllocMem(nBufferSize + 1);
      try
        lStartupInfo := Default(TStartupInfo);
        lStartupInfo.cb := SizeOf(lStartupInfo);
        {-}
        // Set up members of the STARTUPINFO structure.
        // This structure specifies the STDIN and STDOUT handles for redirection.
        // - Redirect the output and error to the writeable end of our pipe.
        // - We must still supply a valid StdInput handle (because we used STARTF_USESTDHANDLES to swear that all three handles will be valid)
        lStartupInfo.dwFlags := lStartupInfo.dwFlags or STARTF_USESTDHANDLES;
        lStartupInfo.hStdInput := GetStdHandle(STD_INPUT_HANDLE); //we're not redirecting stdInput; but we still have to give it a valid handle
        lStartupInfo.hStdOutput := hWriteableEndOfPipe; //we give the writeable end of the pipe to the child process; we read from the readable end
        lStartupInfo.hStdError := hWriteableEndOfPipe;
        {-}
        // We can also choose to say that the wShowWindow member contains a value.
        // In our case we want to force the console window to be hidden.
        lStartupInfo.dwFlags := lStartupInfo.dwFlags or STARTF_USESHOWWINDOW;
        lStartupInfo.wShowWindow := SW_HIDE;
        {-}
        // Don't forget to set up members of the PROCESS_INFORMATION structure.
        lProcessInfo := Default(TProcessInformation);
        {-}
        // WARNING: The unicode version of CreateProcess (CreateProcessW) can modify the command-line "DosApp" string.
        // Therefore "DosApp" cannot be a pointer to read-only memory, or an ACCESS_VIOLATION will occur.
        // We can ensure it's not read-only with the RTL function: UniqueString
        UniqueString({var}sDosApp);
        {-}
        if  CreateProcess(nil, PChar(sDosApp), nil, nil, True,
                          NORMAL_PRIORITY_CLASS, nil, PCharOrNil(sWorkDir),
                          lStartupInfo, {var}lProcessInfo)  then
          try
            CloseHandle(hWriteableEndOfPipe);
            hWriteableEndOfPipe := INVALID_HANDLE_VALUE;
            {-}
            repeat
              bWasOK := ReadFile(hReadableEndOfPipe, {var}pszBuffer[0], nBufferSize, {var}nBytesRead, nil);
              if  nBytesRead > 0  then
                begin
                  pszBuffer[nBytesRead] := #0;
                  if  not fnGetBuffer(pszBuffer)  then
                    begin
                      TerminateProcess(lProcessInfo.hProcess, ERROR_CANCELLED);
                      Break
                    end { not fnGetBuffer(...) }
                end { nBytesRead > 0 }
            until  not bWasOK
                   or (nBytesRead = 0);
            {-}
            WaitForSingleObject(lProcessInfo.hProcess, INFINITE)
          finally
            CloseHandle(lProcessInfo.hProcess);
            CloseHandle(lProcessInfo.hThread)
          end { try-finally }
      finally
        FreeMem(pszBuffer)
      end { try-finally }
    finally
      if  hWriteableEndOfPipe <> INVALID_HANDLE_VALUE  then
        CloseHandle(hWriteableEndOfPipe);
      CloseHandle(hReadableEndOfPipe)
    end { CreatePipe(...) }
end { RunDosCmd };


function  RunDosCmd(const   sDosApp:  string;
                    const   sWorkDir:  string;
                    const   nBufferSize:  Integer)
                    : string;
var
  sResult:  string;

begin  { RunDosCmd }
  sResult := '';
  RunDosCmd(sDosApp,
            function(sBuf:  AnsiString)
                     : Boolean
              begin
                sResult := sResult + UnicodeString(sBuf);
                Result := True   // Continue waiting for the process to finish
              end,
            sWorkDir, nBufferSize);
  Result := sResult
end { RunDosCmd };


procedure  RaiseOSError(const   nErrNo:  LongWord);
begin
  SetLastError(nErrNo);
  RaiseLastOSError()
end { RaiseOSError };


procedure  CheckOSError(const   nErrNo:  LongWord);
begin
  if  nErrNo <> NO_ERROR  then
    RaiseOSError(nErrNo)
end { CheckOSError };


procedure  CheckOSError(const   bReturn:  LongBool);
begin
  if  not bReturn  then
    RaiseLastOSError()
end { CheckOSError };


function  NetAddConnection(const   ResourceType:  TNetResourceType;
                           const   sRemoteName:  string;
                           const   sLocalName:  string;
                           const   sUserName:  string;
                           const   sPassword:  string;
                           const   setFlags:  TNetConnectFlags)
                           : LongWord;
const
  cFlags:  array[TNetConnectFlag] of  LongWord = (
               CONNECT_INTERACTIVE,     // ncInteractive
               CONNECT_PROMPT,          // ncPrompt
               CONNECT_REDIRECT,        // ncRedirect
               CONNECT_UPDATE_PROFILE); // ncUpdateProfile
var
  lNetResource:  TNetResource;
  nFlags:  LongWord;
  lInd:  TNetConnectFlag;

begin  { NetAddConnection }
  FillChar(lNetResource, SizeOf(lNetResource), 0);
  lNetResource.dwType := Ord(ResourceType);
  lNetResource.lpLocalName := PCharOrNil(sLocalName);
  lNetResource.lpRemoteName := PChar(sRemoteName);
  lNetResource.lpProvider := nil;
  nFlags := 0;
  for  lInd := High(TNetConnectFlag)  downto  Low(TNetConnectFlag)  do
    if  lInd in setFlags  then
      nFlags := nFlags or cFlags[lInd];
  Result := WNetAddConnection2(lNetResource,
                               PCharOrNil(sPassword), PCharOrNil(sUsername),
                               nFlags)
end { NetAddConnection };


function  NetCancelConnection(const   sName:  string;
                              const   setFlags:  TNetConnectFlags;
                              const   bForce:  Boolean)
                              : LongWord;
var
  nFlags:  LongWord;

begin  { NetCancelConnection }
  if  ncUpdateProfile in setFlags  then
    nFlags := CONNECT_UPDATE_PROFILE
  else
    nFlags := 0;
  Result := WNetCancelConnection2(PChar(sName), nFlags, bForce)
end { NetCancelConnection };


{$IFNDEF FMX}

function  BrowseForComputer(const   sTitle:  string = 'Wskaż komputer')
                            : string;
var
  lBrowseInfo:  TBrowseInfo;
  lItemIdList:  PItemIDList;
  szComputerName:  packed array[0..MAX_PATH]  of Char;
  pWindowList:  Pointer;
  iShellMalloc:  IMalloc;
  bResult:  Boolean;

begin  { BrowseForComputer }
  if  Failed(SHGetSpecialFolderLocation(Application.Handle,
                                        CSIDL_NETWORK, lItemIdList))  then
    raise Exception.Create('Dialog wyboru komputera nie jest dostępny');
  try
    FillChar(lBrowseInfo, SizeOf(lBrowseInfo), 0);
    lBrowseInfo.hwndOwner := Application.Handle;
    lBrowseInfo.pidlRoot := lItemIdList;
    lBrowseInfo.pszDisplayName := szComputerName;
    lBrowseInfo.lpszTitle := PChar(sTitle);
    lBrowseInfo.ulFlags := BIF_BROWSEFORCOMPUTER;
    pWindowList := DisableTaskWindows(0);
    try
      bResult := SHBrowseForFolder(lBrowseInfo) <> nil
    finally
      EnableTaskWindows(pWindowList)
    end { try-finally };
    if  bResult  then
      Result := szComputerName
    else
      Result := ''
  finally
    if  Succeeded(SHGetMalloc(iShellMalloc))  then
      iShellMalloc.Free(lItemIdList)
  end { try-finally }
end { BrowseForComputer };

{$ENDIF -FMX}


procedure  SetPriority(const   ClassPriority:  TClassPriority;
                       const   ThreadPriority:  TThreadPriority);
begin
  SetPriority(ClassPriority);
  SetPriority(ThreadPriority)
end { SetPriority };


procedure  SetPriority(const   ClassPriority:  TClassPriority);

var
  nProcessId:  DWORD;
  hProcess:  THandle;

begin  { SetPriority }
  nProcessId := GetCurrentProcessId();
  hProcess := OpenProcess(PROCESS_SET_INFORMATION, False, nProcessID);
  if  hProcess = 0 then
    RaiseLastOSError();
  try
    CheckOSError(SetPriorityClass(hProcess,
                                  ClassPriorityConv[ClassPriority]));
  finally
    CloseHandle(hProcess)
  end { try-finally }
end { SetPriority };


procedure  SetPriority(const   ThreadPriority:  TThreadPriority);

var
  hThread:  THandle;

begin  { SetPriority }
  hThread := GetCurrentThread();
  CheckOSError(SetThreadPriority(hThread,
                                 ThreadPriorityConv[ThreadPriority]))
end { SetPriority };


function  GetClassPriority() : TClassPriority;

var
  nProcessID:  DWORD;
  hProcess:  THandle;
  nPriority:  DWORD;

begin  { GetClassPriority }
  nProcessID := GetCurrentProcessID();
  hProcess := OpenProcess(PROCESS_QUERY_INFORMATION, False, nProcessID);
  if  hProcess = 0 then
    RaiseLastOSError();
  try
    nPriority := GetPriorityClass(hProcess);
    Result := Low(TClassPriority);
    while  (Result < High(TClassPriority))
           and (ClassPriorityConv[Result] <> nPriority)  do
      Inc(Result);
    if  nPriority <> ClassPriorityConv[Result]  then
      raise  EGSkPasLibError.Create('GetClassPriority', 'Unknown class priority');
  finally
    CloseHandle(hProcess)
  end { try-finally }
end { GetClassPriority };


function  GetThreadPriority() : TThreadPriority;

var
  hThread:  THandle;
  nPriority:  Integer;

begin  { GetThreadPriority }
  hThread := GetCurrentThread();
  nPriority := {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.GetThreadPriority(hThread);
  if  nPriority = THREAD_PRIORITY_ERROR_RETURN  then
    RaiseLastOSError();
  Result := Low(TThreadPriority);
  while  (Result < High(TThreadPriority))
         and (ThreadPriorityConv[Result] <> nPriority)  do
    Inc(Result);
  SetLastError(0);
  if  nPriority <> ThreadPriorityConv[Result]  then
    raise  EGSkPasLibError.Create('GetThreadPriority', 'Unknown thread priority')
end { GetThreadPriority };


function  IsAdminLoggedOn() : Boolean;

const
  SECURITY_BUILTIN_DOMAIN_RID = $00000020;
  DOMAIN_ALIAS_RID_ADMINS     = $00000220;

var
  aSIDBuffer:  array[0..199] of Byte;
  pSID:        Pointer;
  hProcess:    THandle;
  hToken:      THandle;
  dwSize:      DWORD;
  pGroupInfo:  PTokenGroups;
  sidAuth:     TSIDIdentifierAuthority;
  iCounter:    Integer;

begin { IsAdminLoggedOn }
  FillChar(SIDAuth, SizeOf(SIDAuth), 0);
  sidAuth.Value[5] := 5;
  Result := False;
  if  Win32Platform = VER_PLATFORM_WIN32_NT  then
    begin
      hProcess := GetCurrentProcess();
      hToken := 0;
      if  OpenProcessToken(hProcess, TOKEN_QUERY, hToken)
          and (hToken<>0)  then
        begin
          dwSize := 0;
          if  GetTokenInformation(hToken, TokenGroups, nil, 0, dwSize)
              or (dwSize <> 0)  then
            begin
              pGroupInfo := Pointer(GlobalAlloc(GPTR, dwSize));
              if  GetTokenInformation(hToken, TokenGroups, pGroupInfo,
                                      dwSize, dwSize)  then
                begin
                  pSID := @aSIDBuffer;
                  if  AllocateAndInitializeSid(sidAuth, 2,
                                               SECURITY_BUILTIN_DOMAIN_RID,
                                               DOMAIN_ALIAS_RID_ADMINS,
                                               0, 0, 0, 0, 0, 0, pSID)  then
                    begin
                      {$IfDef OptR }
                        {$Message warn 'Zmienna "OptR" już istnieje' }
                      {$EndIf OptR }
                      {$IfOpt R+ }
                        {$Define OptR }
                      {$Else }
                        {$Undef  OptR }
                      {$EndIf }
                      {$R-}
                      for  iCounter := 0  to  pGroupInfo^.GroupCount - 1  do
                        if  EqualSid(pSID,
                                     pGroupInfo^.Groups[iCounter].SID)  then
                          begin
                            Result:=True;
                            Break
                          end { EqualSid }
                      {$IfDef OptR }
                        {$R+}
                        {$UnDef OptR }
                      {$EndIf OptR }
                    end { AllocateAndInitializeSid }
                end { GetTokenInformation };
              if  pGroupInfo <> nil  then
                GlobalFree(DWORD(pGroupInfo))
            end { if GetTokenInformation };
          CloseHandle(hToken)
        end { if OpenProcessToken }
    end { Win32Platform = VER_PLATFORM_WIN32_NT }
  else
    Result := True
end { IsAdminLoggedOn };


function  IsElevated() : Boolean;

const
  cTokenElevation = TTokenInformationClass(20);

type
  TOKEN_ELEVATION = record
    TokenIsElevated:  DWORD;
  end { TOKEN_ELEVATION };

var
  hTokenHandle:  THandle;
  nResultLength:  Cardinal;
  lTokenElevation:  TOKEN_ELEVATION;
  HaveToken:  Boolean;

begin  { IsElevated }
  if  CheckWin32Version(6, 0)  then
    begin
      hTokenHandle := 0;
      HaveToken := OpenThreadToken(GetCurrentThread, TOKEN_QUERY, True, hTokenHandle);
      if  not HaveToken
          and (GetLastError() = ERROR_NO_TOKEN)  then
        HaveToken := OpenProcessToken(GetCurrentProcess, TOKEN_QUERY, hTokenHandle);
      if  HaveToken  then
        begin
          try
            nResultLength := 0;
            if GetTokenInformation(hTokenHandle, cTokenElevation, @lTokenElevation,
                                   SizeOf(lTokenElevation), nResultLength) then
              Result := lTokenElevation.TokenIsElevated <> 0
            else
              Result := False
          finally
            CloseHandle(hTokenHandle);
          end;
        end { if bHavetoken }
      else
        Result := False
    end { CheckWin32Version(6, 0) }
  else
    Result := IsAdminLoggedOn()
end { IsElevated };


function  GetDesktopArea() : TRect;
begin
  if  not SystemParametersInfo(SPI_GETWORKAREA, 0, @Result, 0)  then
    begin
      FillChar(Result, SizeOf(Result), 0);
      Result.Right := Screen.Width;
      Result.Bottom := Screen.Height;
    end { not SystemParametersInfo() };
end { GetDesktopArea };


function  Is64BitOperatingSystem() : Boolean;
{$IFDEF WIN32}
type
  TIsWow64Process = function(Handle: THandle; var Res: BOOL): BOOL; stdcall;

var
  fnIsWow64Process:  TIsWow64Process;
  bIsWow64Process:   BOOL;

begin  { Is64BitOperatingSystem }
  fnIsWow64Process := GetProcAddress(GetModuleHandle('kernel32.dll'), 'IsWow64Process');
  if  Assigned(fnIsWow64Process)  then
    begin
      CheckOSError(fnIsWow64Process(GetCurrentProcess(), bIsWow64Process));
      Result := bIsWow64Process
    end { Assigned(fnIsWow64Process) }
  else
    Result := False
end { Is64BitOperatingSystem };
{$ELSE}
begin
  Result := True
end { Is64BitOperatingSystem };
{$ENDIF -WIN32}


function  IsProcessRunning(const   sProcess:  string)
                           : Boolean;
var
  lProcess:  TStringList;
  nInd:  Integer;

begin  { IsProcessRunning }
  Result := False;
  lProcess := TStringList.Create();
  try
    GetProcessList(lProcess);
    for  nInd := lProcess.Count - 1  downto  0  do
      begin
        {$IFDEF UNICODE}
          Result := SameText(lProcess[nInd], sProcess);
        {$ELSE}
          Result := AnsiSameText(lProcess[nInd], sProcess);
        {$ENDIF -UNICODE}
        if  Result  then
          Break
      end { for nInd }
  finally
    lProcess.Free()
  end { try-finally }
end { IsProcessRunning };


function  IsProcessRunning(const   ProcessList:  TStringList;
                           const   bCheckAll:  Boolean)
                           : string;
var
  lProcess:  TStringList;
  nIndP:     Integer;
  nIndL:     Integer;
  lFound:    TBits;

begin  { IsProcessRunning }
  Assert(not ProcessList.CaseSensitive, 'Trzeba zakodować własne szukanie procesu');
  lFound := TBits.Create();
  try
    lFound.Size := ProcessList.Count + 1{Sentinel};
    { Acquire running processes }
    lProcess := TStringList.Create();
    try
      GetProcessList(lProcess);
      for  nIndP := lProcess.Count - 1  downto  0  do
        begin
          nIndL := ProcessList.IndexOf(lProcess[nIndP]);   // not CaseSensitive!
          if  nIndL >= 0  then
            lFound.Bits[nIndL] := True
        end { for nIndP }
    finally
      lProcess.Free()
    end { try-finally };
    { Analyse acquired data }
    if  bCheckAll  then
      begin
        nIndP := lFound.OpenBit();
        if  nIndP < ProcessList.Count  then
          Result := ProcessList[nIndP]      // return first not-running process
        else
          Result := ''                      // all processes are running
      end { bCheckAll }
    else
      begin
        Result := '';                       // assume that none process is runing
        for  nIndP := 0  to  ProcessList.Count - 1  do
          if  lFound.Bits[nIndP]  then
            begin
              Result := ProcessList[nIndP]; // return name of running process
              Break
            end
      end { NOT bCheckAll }
  finally
    lFound.Free()
  end { try-finally }
end { IsProcessRunning };


procedure  GetProcessList(const   List:  TStrings);

var
  hSnapshot:  THandle;
  lProcess:  TProcessEntry32;

begin  { GetProcessList }
  CheckWindowsNT('GetProcessList');
  try
    List.BeginUpdate();
    try
      List.Clear();
      hSnapshot := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
      if  hSnapshot = INVALID_HANDLE_VALUE  then
        RaiseLastOSError();
      try
        lProcess.dwSize := SizeOf(lProcess);
        if  Process32First(hSnapshot, lProcess)  then
          repeat
            List.AddInteger(lProcess.szExeFile, Integer(lProcess.th32ProcessID))
          until  not Process32Next(hSnapshot, lProcess)
      finally
        CloseHandle(hSnapshot)
      end { try-finally }
    finally
      List.EndUpdate()
    end { try-finally }
  except                     {$WARN SYMBOL_PLATFORM OFF}
    on  EAbstractError  do   {$WARN SYMBOL_PLATFORM ON}
      raise  EGSkPasLibError.Create('GetProcessList',
                                    'Zamiast obiektu „TStrings” użyj któregoś z jego potomków, np. „TSringList” lub „THashedStringList”');
    else
      raise
  end { try-except }
end { GetProcessList };


function  TerminateNamedProcess(const   sProcess:  string;
                                const   nExitCode:  LongWord;
                                const   bTerminateAllInstances:  Boolean)
                                : Boolean;
var
  hSnapshot:  THandle;
  lProcess:   TProcessEntry32;
  hProcess:   THandle;
  bFound:     Boolean;
  nCurrPrcID: DWORD;

begin { TerminateNamedProcess }
  CheckWindowsNT('TerminateNamedProcess');
  Result := False;
  hSnapshot := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  if  hSnapshot = INVALID_HANDLE_VALUE  then
    RaiseLastOSError();
  try
    nCurrPrcID := GetCurrentProcessId();
    lProcess.dwSize := SizeOf(lProcess);
    if  Process32First(hSnapshot, lProcess)  then
      repeat
        bFound := (lProcess.th32ProcessID <> nCurrPrcID)
                  and {$IFDEF UNICODE}
                        SameText(lProcess.szExeFile, sProcess)
                      {$ELSE}
                        AnsiSameText(lProcess.szExeFile, sProcess)
                      {$ENDIF -UNICODE};
        if  bFound  then
          begin
            hProcess := OpenProcess(PROCESS_TERMINATE, False, lProcess.th32ProcessID);
            if  hProcess = 0  then
              RaiseLastOSError();
            try
              CheckOSError(TerminateProcess(hProcess, nExitCode));
              Result := True
            finally
              CloseHandle(hProcess)
            end { try-exit };
            if  bTerminateAllInstances  then
              Continue;
            Break
          end { bFound }
      until  not Process32Next(hSnapshot, lProcess)
  finally
    CloseHandle(hSnapshot)
  end { try-finally }
end { TerminateNamedProcess };


procedure  TrimAppMemorySize();

var
  hMainHandle:  THandle;

begin  { TrimAppMemorySize }
  try
    hMainHandle := OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessID) ;
    SetProcessWorkingSetSize(hMainHandle, $FFFFFFFF, $FFFFFFFF) ;
    CloseHandle(hMainHandle);
  except
    { ignore exceptions }
  end { try-except };
  Application.ProcessMessages()
end { TrimAppMemorySize };


function  GetProcessorCount() : Integer;

{$IF not Declared(CPUCount)}
var
  lSysInfo:  TSystemInfo;
{$IFEND}

begin  { GetProcessorCount }
  {$IF Declared(CPUCount)}
    Result := CPUCount;
  {$ELSE}
    GetSystemInfo(lSysInfo);
    Result := lSysInfo.dwNumberOfProcessors;
  {$IFEND}
  Assert(Result > 0, 'Number of processors must be a positive number')
end { GetProcessorCount };


function  GetCurrentThreadCount() : Integer;

var
  nProcessID:  DWORD;
  hSnapshot:  THandle;
  lEntry:  PROCESSENTRY32;
  bRet:  BOOL;

begin  { GetCurrentThreadCount }
  nProcessID := GetCurrentProcessId();
  hSnapshot := CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
  FillChar(lEntry, SizeOf(lEntry), 0);
  lEntry.dwSize := SizeOf(lEntry);
  bRet := Process32First(hSnapshot, lEntry);
  while  bRet
         and (lEntry.th32ProcessID <> nProcessID)  do
    bRet := Process32Next(hSnapshot, lEntry);
  CloseHandle(hSnapshot);
  if  bRet  then
    Result := lEntry.cntThreads
  else
    Result := -1
end { GetCurrentThreadCount };


function  GetSysEnvironmentVariable(const   sName:  string)
                                    : string;
begin
  SetLength(Result, MAX_PATH);
  if  ExpandEnvironmentStringsForUser(0, PChar('%' + sName + '%'), PChar(Result), MAX_PATH)  then
    SetLength(Result, StrLen(PChar(Result)))
  else
    RaiseLastOSError()
end { GetSysEnvironmentVariable };


function  GetSysTempPath() : string;
begin
  Result := GetSysEnvironmentVariable('TEMP')
end { GetSysTempPath };


{$IFDEF UNICODE} // Declared(SysUtils.TLanguages.LocaleName)}

function  GetLanguageCodeFromLCID(const   LangID:  TLCID)
                                  : string;
begin
  with  Languages()  do
    Result := LocaleName[IndexOf(LangID)]
end { GetLanguageCodeFromLCID };

{$ENDIF}


function  GetCurrentProcessMemorySize() : SIZE_T;

var
  Mem:  TProcessMemoryCounters;

begin  { GetCurrentProcessMemorySize }
  Result := 0;
  Mem.cb := SizeOf(TProcessMemoryCounters);
  if  GetProcessMemoryInfo(GetCurrentProcess(), Addr(Mem), SizeOf(Mem))  then
    Result := Mem.WorkingSetSize
  else
    RaiseLastOSError()
end { GetCurrentProcessMemorySize };


function  GetAvailablePhysicalMemory() : DWORDLONG;

var
 {$IF CompilerVersion = 18.5}
  Mem:  TMemoryStatus;
 {$ELSE}
  Mem:  TMemoryStatusEx;
 {$IFEND}

begin  { GetAvailablePhysicalMemory }
  Mem.dwLength := SizeOf(Mem);
  {$IF CompilerVersion = 18.5}
    GlobalMemoryStatus(Mem);
    Result := Mem.dwAvailPhys
  {$ELSE}
    Result := 0;
    if  GlobalMemoryStatusEx(Mem)  then
      Result := Mem.ullAvailPhys
    else
      RaiseLastOSError()
  {$IFEND}
end { GetAvailablePhysicalMemory };


function  GetGUIDStr(const   Options:  TGetGuidStrOpts)
                     : string;
var
  lGUID:  TGUID;

begin  { GetGuidStr }
  if CreateGUID(lGUID) <> S_OK then
    RaiseLastOSError();
  Result := GUIDToString(lGUID);
  {-}
  if  goNoCurlyBrackets in Options  then
    if  (Result[1] = '{')
        and (Result[Length(Result)] = '}')   then
      Result := Copy(Result, 2, Length(Result) - 2);
  {-}
  if  goNoHyphens in Options   then
    Result := StringReplace(Result, '-', '', [rfReplaceAll])
end { GetGuidStr };


{$IFDEF WIN32}

procedure  RegistryRedirection(const   RegKey:  HKEY;
                               const   bEnable:  Boolean);

var
  fnReflectionKey:  function(hBase:  HKEY)
                             : LONG;                                 stdcall;

begin  { RegistryRedirection }
  if  bEnable  then
    fnReflectionKey := GetProcAddress(GetModuleHandle('advapi32.dll'), 'RegEnableReflectionKey')
  else
    fnReflectionKey := GetProcAddress(GetModuleHandle('advapi32.dll'), 'RegDisableReflectionKey');
  {-}
  if  Assigned(fnReflectionKey)  then
    CheckOSError(fnReflectionKey(RegKey))
end { RegistryRedirection };

{$ENDIF WIN32}


end.

