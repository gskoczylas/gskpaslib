﻿{: @abstract(Moduł @name zawiera podprogramy związane z aplikacją.)

   W tym module są podprogramy pomocnicze aplikacji (lub bibliotek).

   @seealso(GSkPasLib.FileUtils) }
unit  GSkPasLib.AppUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22}
    System.SysUtils,
    {$IFDEF FMX}
      FMX.Forms,
    {$ELSE}
      Vcl.Forms,
    {$ENDIF -FMX}
  {$ELSE}
    SysUtils, Forms,
  {$IFEND}
  GSkPasLib.PasLib, GSkPasLib.FileUtils;


{: @abstract Pobiera informacje o wersji aplikacji

   Funkcja udostępnia informacje dołączone jako @italic(Version Info) do aplikacji.

   Ta funkcja działa identycznie jak funkcja @link(GSkPasLib.FileUtils.GetModuleVersionInfo)
   wywołana bez parametrów lub z pustym napisem.

   Wynikiem funkcji jest @italic(obiekt). Trzeba pamiętać, aby po wykorzystaniu
   zwolnić go wywołując jego metodę @italic(Free()), na przykład: @longCode(#

   with  GetAppVersionInfo()  do
     try
       ShowMessage('Wersja pliku: ' + FileVersion)
     finally
       Free()
     end ;  #)  }
function  GetAppVersionInfo() : TGSkFileVersionInfo;


{: @abstract Zwraca @italic(InternalName) programu

   Funkcja zwraca parametr @italic(InternalName) z informacji @italic(Version
   Info) dołączonych do programu.

   Ta funkcja działa identycznie jak funkcja @link(GSkPasLib.FileUtils.GetModuleInternalName)
   wywołana bez parametrów lub z pustym napisem. }
function  GetAppInternalName() : string;


{: @abstract Zwraca ścieżkę do pliku konfiguracyjnego aplikacji.

   Plik konfiguracyjny powinien byćalbo w bieżącym folderze, albo w folderze,
   z którego został załadowany do pamięci program EXE. Nazwa pliku konfiguracyjnego
   powinna być albo taka sama jak nazwa programu EXE, ale ze wskazanym rozszerzeniem,
   albo taka, jaką wskazuje stała @code(gcsStdConfigName) z modułu @link(GSkPasLib.PasLib).

   Sposób ustalania nazwy pliku konfiguracyjnego ilustruje następujący przykład.
   Załóżmy, że program nazywa sięKASA.EXE. Program pracuje w sieci i został
   uruchomiony z folderu na serwerze. Bieżącym folderem programu jest prywatny
   folder użytkownika. Funkcja @name poszukuje pliku konfiguracyjnego sprawdzając
   czy istnieje jeden z poniższych plików: @br
   1. Plik @code(KASA.INI) w bieżącym folderze --- prywatny plik tego programu i dla
      tego użytkownika. @br
   2. Plik @code(PROGRAM.ini) w bieżącym
      folderze --- plik konfiguracyjny wspólny dla wszystkich programów tego
      użytkownika. @br
   3. Plik @code(KASA.INI) w folderze programu KASA.EXE na serwerze --- wspólny
      plik konfiguracyjny dla wszystkich użytkowników korzystających z tego
      programu. @br
   4. Plik @code(PROGRAM.ini) w folderze
      programu KASA.EXE na serwerze --- jeżeli różne programy są uruchamiane z
      tego samego foldera na serwerze to jest to wspólny plik konfiguracyjny
      wszystkich użytkowników i wszystkich programów z tego foldera.

   Nazwa @code(PROGRAM) zdefiniowana jest przez stałą @code(gcsStdConfigName)
   z modułu @link(GSkPasLib.PasLib).

   Wynikiem funkcji jest ścieżka do pierwszego ze znalezionych plików. Jeżeli
   żaden wymieniony wyżej plik nie istnieje to wynikiem funkcji jest pusty napis.

   Użycie wspólnego programu konfiguracyjnego dla wielu programów lub użytkowników
   ma sens szczególnie w przypadku programów bazodanowych, które informacji o
   parametrach połączenia z bazą danych poszukują w pliku konfiguracyjnym.
   Zazwyczaj wszystkie programy korzystają z tej samej bazy (w przypadku baz SQL).

   Zamiast rozszerzenia @code(.ini) zastosowane będzie rozszerzenie wskazane w
   parametrze funkcji.

   @bold(Uwaga), @br
   Określenie @italic(bieżący folder) oznacza bieżący folder w chwili uruchomienia
   programu. Bieżący folder może ulec zmianie w trakcie działania programu, na
   przykład w wyniku działania komponentu @code(TOpenDialog) lub @code(TSaveDialog).
   Funkcja @name korzysta z bieżącego foldru w chwili uruchomienia programu.

   @param(sStdCnf Standardowa nazwa pliku konfiguracyjnego. Domyślnie jest to
                  stała @link(GSkPasLib.PasLib.gcsStdConfigFileName).)
   @param(sStdExt Typ pliku konfiguracyjnego. Domyślnie jest to stała
                  @link(GSkPasLib.PasLib.gcsStdConfigExt).)
   @return(Wynikiem funkcji jest ścieżka pliku konfiguracyjnego.) }
function  GetAppConfigFileName(const   sStdCnf:  string = gcsStdConfigFileName;
                               const   sStdExt:  string = gcsStdConfigExt)
                               : TFileName;


{: @abstract(Zwraca tytuł aplikacji)

   Najpierw pobiera @code(Application.Title). @br
   Jeżeli nie zdefiniowano tytułu aplikacji to pobiera
   @code(Application.MainForm.Caption).

   Jeżeli apliacja nie ma zdefiniowanego ani tytułu, ani głównego okna (lub
   tytuł głównego okna jest pusty, to wynikiem funkcji jest nazwa modułu (aplikacji)
   otrzymywana w wyniku wywołania @code(ChangeFileExt(GetModuleFileName(), '')).

   @seeAlso(GSkPasLib.FileUtils.GetModuleFileName GetModuleFileName) }
function  GetAppTitle() : string;


{: @abstract(Wylicza bazowy klucz Registry dla bieżącej aplikacji.)

   Funkcja @name wyznacza bazowy klucz Registry według schematu
   @code(\Software\ShortName\ProductName\InternalName). Poszczególne segmenty
   wyznaczana są następująco:
   @table(@row(@cell(Software)
               @cell(Stały napis))
          @row(@cell(ShortName)
               @cell(wartość stałej @link(gcsCompanyName) z pliku
                     @code(Author.inc); stała ta wskazuje krótką nazwę
                     właściciela programu (np. krótka nazwa firmy)))
          @row(@cell(ProductName)
               @cell(pobierana z @italic(Version Info); ten element
                     występuje tylko wtedy gdy w @italic(VersionInfo)
                     odpowiednie pole nie jest puste))
          @row(@cell(InternalName)
               @cell(pobierana z @italic(Version Info))))

   Z powyższego sposobu wyznaczania wyniku tej funkcji wynika, że
   @italic(VersionInfo) należy obowiązkowo dołączać do każdej aplikacji
   korzystającej z tej funkcji i należy odpowiednio wypełnić przynajmniej
   wymienione wyżej pola.

   Klucz wyliczony przez tę funkcję może być uzupełniony przez inne funkcje
   biblioteki. Na przykład funkcja @link(GSkPasLib.FormUtils.GetFormRegistryKey)
   uzupełnia ten klucz o część specyficzną dla wskazanego okna aplikacji. }
function  GetAppRegistryKey() : string;


{: @abstract Bieżący folder w chwili uruchomienia aplikacji.

   Podczas działania aplikacji bieżący folder może ulegać zmianie. Funkcja @name
   zwraca zawsze ścieżkę do folderu, który był bieżącym folderem w chwili
   uruchomienia aplikacji. }
function  GetStartupPath() : TFileName;


implementation


uses
  GSkPasLib.PasLibInternals;


var
  gsInitCurrentPath:  TFileName;
  gsAppRegistryKey:  string;


function  GetAppVersionInfo() : TGSkFileVersionInfo;
begin
  Result := GetModuleVersionInfo()
end { GetAppVersionInfo };


function  GetAppInternalName() : string;
begin
  Result := GetModuleInternalName()
end { GetAppInternalName };


function  GetAppConfigFileName(const   sStdCnf, sStdExt:  string)
                               : TFileName;
var
  sCnfName:  TFileName;
  sAppDir:  TFileName;

begin  { GetAppConfigFileName }
  sCnfName := GetModuleFileName();
  sAppDir := ExtractFilePath(sCnfName);
  sCnfName := ChangeFileExt(ExtractFileName(sCnfName),
                            sStdExt);
  if  FileExists(gsInitCurrentPath + sCnfName)  then
    Result := gsInitCurrentPath + sCnfName
  else if  FileExists(gsInitCurrentPath + sStdCnf)  then
    Result := gsInitCurrentPath + sStdCnf
  else if  FileExists(sAppDir + sCnfName)  then
    Result := sAppDir + sCnfName
  else
    Result := sAppDir + sStdCnf;
  if  Result <> ''  then
    Result := ExpandUNCFileName(Result)
end { GetAppConfigFileName };


function  GetAppTitle() : string;
begin
  if  IsLibrary  then
    Result := ChangeFileExt(ExtractFileName(GetModuleFileName()), '')
  else
    begin
      Result := Application.Title;
      if  (Result = '')
          and (Application.MainForm <> nil)  then
        Result := Application.MainForm.Caption
    end { not IsLibrary }
end { GetAppTitle };


function  GetAppRegistryKey() : string;
begin
  if  gsAppRegistryKey = ''  then
    begin
      gsAppRegistryKey := '\Software\' + gcsCompanyName;
      with  GetAppVersionInfo()  do
        try
          if  (ProductName <> '')
              and (ProductName <> '$00')  then
            gsAppRegistryKey := gsAppRegistryKey + '\' + ProductName;
          if  Trim(InternalName) = ''  then
            raise  EGSkPasLibError.Create('GetAppRegistryKey',
                                          'Wypełnij VersionInfo.InternalName w opcjach projektu');
          gsAppRegistryKey := gsAppRegistryKey + '\' + InternalName
        finally
          Free()
        end { try-finally }
    end { gsAppRegistryKey <> '' };
  Result := gsAppRegistryKey
end { GetAppRegistryKey };


function  GetStartupPath() : TFileName;
begin
  Result := gsInitCurrentPath
end { GetStartupPath };


initialization
  gsInitCurrentPath := GetCurrentDir() + PathDelim;
  gsAppRegistryKey := ''


end.
