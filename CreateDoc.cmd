@ echo off
@ REM  Sposoby wywo�ania procedury:
@ REM  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@ REM    CreateDoc
@ REM      Kompiluje dokumentacje - tylko nowe lub zmienione moduly (MAKE)
@ REM
@ REM    CreateDoc  /b
@ REM      Kompiluje dokumentacje - wszystkie moduly (BUILD)
@ REM
@ REM    CreateDoc DOT
@ REM      Ogranicza sie do (re-)generowania plikow graficznych
@ REM ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
@ REM    Uzywane narzedzia:
@ REM      - PasDoc     --> https://pasdoc.github.io
@ REM      - GraphVis   --> http://www.graphviz.org
@ REM      - PngOut     --> http://advsys.net/ken/utils.htm
@ REM      - MsgDlg     --> My small tool
@ REM ====================================================================
  if  not "%1" == "MAX" (
    start "Tworzenie dokumentacji PasDoc" /max %0 MAX %*
@   exit  /b
  )
  shift
  SetLocal EnableExtensions
  REM Lokalizacja programow:
  if exist l:\PasDoc\bin\pasdoc.exe (
    SET PASDOC=l:\PasDoc\bin\pasdoc.exe
  ) else if exist s:\DelphiLib\PasDoc\bin\pasdoc.exe ( 
    SET PASDOC=s:\DelphiLib\PasDoc\bin\pasdoc.exe
  ) else (
    @echo PasDoc.exe not found.
    @pause
    @goto :EOF
  )
  SET DOT=c:\Program Files\Graphviz\bin\dot.exe
      @REM https://www.graphviz.org
  SET PNGOUT=c:\Tools\PNGOUT\pngout.exe
  SET MSGDLG=c:\Tools\MsgDlg.exe

@ REM Typ grafiki:
@ REM set GREXT=jpg
@ REM set GREXT=png
  SET GREXT=jpg
  
@ REM Parametry:
  set INCLUDE=p:\GSkPasLib;P:\GSkPasLib\Author
  set SOURCE=p:\GSkPasLib\GSkPasLib.*.pas
       set OPTIONS=--define Ver220 --define IBX --define UNICODE --define CompilerVersion=26 --write-uses-list --title GSkPasLib --use-tipue-search --marker : --language  pl --format html --introduction DocIntroduction.pdt --conclusion DocConclusion.pdt --header DocHeader.html --footer DocFooter.html --cache-dir Doc.cache --implicit-visibility implicit
@ REM  set OPTIONS=--define Ver185 --define IBX --define UNICODE --write-uses-list --title GSkPasLib --use-tipue-search --marker : --language  pl --format html --full-link --introduction DocIntroduction.pdt --conclusion DocConclusion.pdt --header DocHeader.html --footer DocFooter.html --cache-dir Doc.cache --implicit-visibility implicit
       set GRAPHVIZ=--graphviz-classes --graphviz-uses --link-gv-classes %GREXT% --link-gv-uses %GREXT%
@ REM  set GRAPHVIZ=--graphviz-classes --link-gv-classes %GREXT%
  set OUTPUT=Doc

@ REM Koniec sekcji parametrow.

  if /i "%1" equ "dot"  goto DOT

@ echo.
@ echo Kompilowanie...
@ echo.
  del/q %OUTPUT%\*.*
  if /i "%1" equ "/b"  del/q  Doc.cache\*.pduc
  %PASDOC% --include=%INCLUDE% --output %OUTPUT%\ %OPTIONS% %GRAPHVIZ% %SOURCE%

:DOT
@ echo.
@ echo Generowanie grafiki...
@ echo.

@echo on
  "%DOT%" -Grankdir=LR -T%GREXT% -o%OUTPUT%\GVUses.%GREXT% %OUTPUT%\GVUses.dot
  "%DOT%" -Grankdir=LR -T%GREXT% -o%OUTPUT%\GVClasses.%GREXT% %OUTPUT%\GVClasses.dot
@ echo.
@ if not "%GREXT%" == "png"  goto DOT_CONT
  start "Uses" "%PNGOUT%" %OUTPUT%\GVUses.%GREXT%
  timeout 1 >nul
  start "Classes" "%PNGOUT%" %OUTPUT%\GVClasses.%GREXT%
@ echo.
@REM  del  %OUTPUT%\GVUses.dot
@REM  del  %OUTPUT%\GVClasses.dot

:DOT_CONT
@ echo Gotowe.
@ echo.

@ if Exist %MSGDLG%  goto AskShowDoc
@ pause
@ goto TheEnd

:AskShowDoc
@ "%MsgDlg%" --message "Czy wy�wietli� wygenerowan� dokumentacj�?" --type Question --button "&Tak" --button "&Nie"
@ if ErrorLevel 2  goto TheEnd
@ if ErrorLevel 1  goto ShowDoc

:ShowDoc
@ echo.
@ echo Uruchamiam - zaczekaj chwil�...
  start "GSkPasLib Documentation" %OUTPUT%\index.html

:TheEnd
@ EndLocal
