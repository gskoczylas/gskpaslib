﻿unit  GSkPasLib.JSON;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


uses
  {$IF CompilerVersion > 22} // XE
    System.SysUtils, System.DateUtils, System.StrUtils, System.Classes,
    System.JSON, System.RegularExpressions, System.Generics.Collections;
  {$ELSE}
    SysUtils, DateUtils, StrUtils, Classes, Generics.Collections;
    {$LEGACYIFEND ON}
  {$IFEND}


type
  TGSkCustomJSON = class
  strict private
    const
      cnOffsetStep = 2;
    type
      TJSONContext = (jcUnknown, jcObject, jcArray);
    var
      fWriter:     TStringWriter;
      fbIndented:   Boolean;
      fbWriteSep:   Boolean;
      fbSkipOffset: Boolean;
      fnOffset:     Integer;
      fCurrCtx:     TJSONContext;
      fCtxStack:    TStack<TJSONContext>;
    function  EncodeString(const   sText:  string)
                           : string;
    procedure  DecrementOffset();                                    inline;
    procedure  IncrementOffset();                                    inline;
    procedure  WriteOffset();
    procedure  WriteSeparator();
    procedure  WriteStart(const   Context:  TJSONContext);
    procedure  WriteEnd();
  strict protected
    procedure  AssignWriter(const   Writer:  TStringWriter);
  public
    constructor  Create(const   Writer:  TStringWriter;
                        const   bIndented:  Boolean = False);
    destructor  Destroy();                                           override;
    {-}
    function  ToString() : string;                                   override;
    {-}
    function  WriteStartObject() : TGSkCustomJSON;
    function  WriteStartArray() : TGSkCustomJSON;
    function  WritePropertyName(const   sName:  string)
                                : TGSkCustomJSON;
    function  WriteValue(const   sValue:  string)
                         : TGSkCustomJSON;                                overload;
    function  WriteValue(const   nValue:  Integer)
                         : TGSkCustomJSON;                                overload;
    function  WriteValue(const   nValue:  Extended)
                         : TGSkCustomJSON;                                overload;
    function  WriteValue(const   nValue:  Currency)   // za Extended! (Delphi 11.3)
                         : TGSkCustomJSON;                                overload;
    function  WriteValue(const   tmValue:  TDateTime)
                         : TGSkCustomJSON;                                overload;
    function  WriteValue() : TGSkCustomJSON;                              overload;
    function  WriteEndObject() : TGSkCustomJSON;
    function  WriteEndArray() : TGSkCustomJSON;
    {-}
    function  WritePropertyValue(const   sName:  string;
                                 const   sValue:  string)
                                 : TGSkCustomJSON;                        overload; inline;
    function  WritePropertyValue(const   sName:  string;
                                 const   nValue:  Integer)
                                 : TGSkCustomJSON;                        overload; inline;
    function  WritePropertyValue(const   sName:  string;
                                 const   nValue:  Extended)
                                 : TGSkCustomJSON;                        overload; inline;
    function  WritePropertyValue(const   sName:  string;
                                 const   nValue:  Currency)   // za Extended! (Delphi 11.3)
                                 : TGSkCustomJSON;                        overload; inline;
    function  WritePropertyValue(const   sName:  string;
                                 const   tmValue:  TDateTime)
                                 : TGSkCustomJSON;                        overload; inline;
    function  WritePropertyValue(const   sName:  string)
                                 : TGSkCustomJSON;                        overload; inline;
    function  WritePropertyValueIf(const   bCond:  Boolean;
                                   const   sName:  string;
                                   const   sValue:  string)
                                   : TGSkCustomJSON;                      overload;
    function  WritePropertyValueIf(const   bCond:  Boolean;
                                   const   sName:  string;
                                   const   nValue:  Currency)
                                   : TGSkCustomJSON;                      overload;
    function  WritePropertyValueIfNotEmpty(const   sName:  string;
                                           const   sValue:  string)
                                           : TGSkCustomJSON;
    {-}
    property  FormattingIndented:  Boolean
              read  fbIndented
              write fbIndented;
  end { TGSkCustomJSON };

  TGSkJSON = class(TGSkCustomJSON)
  strict private
    var
      fBuilder:  TStringBuilder;
      fWriter:   TStringWriter;
    procedure  CreateBuilderAndWriter();
    procedure  DestroyBuilderAndWriter();
  public
    constructor  Create(const   bIndented:  Boolean = False);
    destructor  Destroy();                                           override;
    {-}
    function  Clear() : TGSkJSON;
  end { TGSkJSON };

  TGSkJSON2 = TGSkJSON
      deprecated 'Use TGSkJSON instead';


function  FormatJSON(const   JSON:  TJSONAncestor)
                     : string;

function  LinearizeJSON(const   sJSON:  string)
                        : string;


implementation


uses
  GSkPasLib.ASCII;


{$REGION 'Global subroutines'}

function  FormatJSON(const   JSON:  TJSONAncestor)
                     : string;
begin
  if  JSON = nil  then
    Exit('');
  {-}
  {$IF CompilerVersion >= 35}
    Result := StringReplace(JSON.Format(2), '\/', '/', [rfReplaceAll])
  {$ELSEIF CompilerVersion = 30} // 10.0 Seattle
    Result := JSON.ToString()
  {$ELSEIF CompilerVersion > 22} // XE
    Result := TJSON.Format(JSON);
  {$ELSE}
    Result := JSON.ToString();
  {$IFEND}
end { FormatJSON };


function  LinearizeJSON(const   sJSON:  string)
                        : string;
begin
  Result := TRegEx.Replace(sJSON, '[\r\n]+\s*', '')
end { StringifyJSON };

{$ENDREGION 'Global subroutines'}


{$REGION 'TTGSkCustomJSON'}

procedure  TGSkCustomJSON.AssignWriter(const   Writer:  TStringWriter);
begin
  fWriter := Writer;
  fbWriteSep := False;
  fnOffset := 0
end { AssignWriter };


constructor  TGSkCustomJSON.Create(const   Writer:  TStringWriter;
                                   const   bIndented:  Boolean);
begin
  inherited Create();
  fWriter := Writer;
  fbIndented := bIndented;
  fCurrCtx := jcUnknown;
  fCtxStack := TStack<TJSONContext>.Create()
end { Create };


procedure  TGSkCustomJSON.DecrementOffset();
begin
  Dec(fnOffset, cnOffsetStep)
end { DecrementOffset };


destructor  TGSkCustomJSON.Destroy();
begin
  fCtxStack.Free();
  inherited
end { Destroy };


function  TGSkCustomJSON.EncodeString(const   sText:  string)
                                      : string;
const
  cSpecialChars = gcsetControlCharsUnicode + ['"', '\'];

var
  nChar:  Integer;

begin  { EncodeString }
  Result := sText;
  for  nChar := Length(Result)  downto  1  do
    if  CharInSet(Result[nChar], cSpecialChars)  then
      case  Result[nChar]  of
        '"', '\':
          Insert('\', Result, nChar);
        BS:
          Result := StuffString(Result, nChar, 1, '\b');
        FF:
          Result := StuffString(Result, nChar, 1, '\f');
        LF:
          Result := StuffString(Result, nChar, 1, '\n');
        CR:
          Result := StuffString(Result, nChar, 1, '\r');
        TAB:
          Result := StuffString(Result, nChar, 1, '\t');
        else
          Result := StuffString(Result, nChar, 1,
                                Format('\u%4.4x', [Ord(Result[nChar])]))
      end { case Result[nChar] }
end { EncodeString };


procedure  TGSkCustomJSON.IncrementOffset();
begin
  Inc(fnOffset, cnOffsetStep)
end { IncrementOffset };


function TGSkCustomJSON.ToString: string;
begin
  Result := fWriter.ToString()
end { ToString };


procedure  TGSkCustomJSON.WriteEnd();
begin
  DecrementOffset();
  WriteOffset();
  case  fCurrCtx  of
    jcUnknown:
      { OK };
    jcObject:
      fWriter.Write('}');
    jcArray:
      fWriter.Write(']')
  end { case fCurrCtx };
  {-}
  fCurrCtx := fCtxStack.Pop();
  fbWriteSep := True
end { WriteEnd };


function  TGSkCustomJSON.WriteEndArray() : TGSkCustomJSON;
begin
  WriteEnd();
  Result := Self
end { WriteEndArray };


function  TGSkCustomJSON.WriteEndObject() : TGSkCustomJSON;
begin
  WriteEnd();
  Result := Self
end { WriteEndObject };


procedure  TGSkCustomJSON.WriteOffset();
begin
  if  fbSkipOffset  then
    fbSkipOffset := False
  else
    if  fbIndented  then
      begin
        fWriter.WriteLine();
        fWriter.Write(StringOfChar(' ', fnOffset))
      end { fbIndented }
end { WriteOffset };


function  TGSkCustomJSON.WritePropertyName(const   sName:  string)
                                           : TGSkCustomJSON;
begin
  WriteSeparator();
  WriteOffset();
  fWriter.Write('"');
  fWriter.Write(EncodeString(sName));
  fWriter.Write('":');
  if  fbIndented  then
    fWriter.Write(' ');
  Result := Self
end { WritePropertyName };


function  TGSkCustomJSON.WritePropertyValue(const   sName:  string;
                                            const   nValue:  Integer)
                                            : TGSkCustomJSON;
begin
  Result := WritePropertyName(sName).WriteValue(nValue)
end { WritePropertyValue };


function  TGSkCustomJSON.WritePropertyValue(const   sName, sValue:  string)
                                            : TGSkCustomJSON;
begin
  Result := WritePropertyName(sName).WriteValue(sValue)
end { WritePropertyValue };


function  TGSkCustomJSON.WritePropertyValue(const   sName:  string;
                                            const   nValue:  Currency)
                                            : TGSkCustomJSON;
begin
  Result := WritePropertyName(sName).WriteValue(nValue)
end { WritePropertyValue };


function  TGSkCustomJSON.WritePropertyValue(const   sName:  string;
                                            const   nValue:  Extended)
                                            : TGSkCustomJSON;
begin
  Result := WritePropertyName(sName).WriteValue(nValue)
end { WritePropertyValue };


function  TGSkCustomJSON.WritePropertyValue(const   sName:  string;
                                            const   tmValue:  TDateTime)
                                            : TGSkCustomJSON;
begin
  Result := WritePropertyName(sName).WriteValue(tmValue)
end { WritePropertyValue };


function  TGSkCustomJSON.WritePropertyValue(const   sName:  string)
                                            : TGSkCustomJSON;
begin
  Result := WritePropertyName(sName).WriteValue()
end { WritePropertyValue };


function  TGSkCustomJSON.WritePropertyValueIf(const   bCond:  Boolean;
                                              const   sName:  string;
                                              const   nValue:  Currency)
                                              : TGSkCustomJSON;
begin
  if  bCond  then
    WritePropertyValue(sName, nValue);
  Result := Self
end { WritePropertyValueIf };


function  TGSkCustomJSON.WritePropertyValueIf(const  bCond:  Boolean;
                                              const  sName, sValue:  string)
                                              : TGSkCustomJSON;
begin
  if  bCond  then
    WritePropertyValue(sName, sValue);
  Result := Self
end { WritePropertyValueIf };


function  TGSkCustomJSON.WritePropertyValueIfNotEmpty(const   sName, sValue:  string)
                                                      : TGSkCustomJSON;
begin
  if  sValue <> ''  then
    WritePropertyValue(sName, sValue);
  Result := Self
end { WritePropertyValueIfNotEmpty };


procedure  TGSkCustomJSON.WriteSeparator();
begin
  if  fbWriteSep  then
    begin
      fWriter.Write(',');
      if  fbIndented
          and (fCurrCtx = jcArray)  then
        fWriter.Write(' ');
      fbWriteSep := False
    end { fbWriteSep }
end { WriteSeparator };


procedure  TGSkCustomJSON.WriteStart(const   Context:  TJSONContext);
begin
  WriteSeparator();
  if  fnOffset > 0  then
    WriteOffset();
  {-}
  fCtxStack.Push(fCurrCtx);
  fCurrCtx := Context;
  {-}
  case  fCurrCtx  of
    jcUnknown:
      { OK };
    jcObject:
      fWriter.Write('{');
    jcArray:
      fWriter.Write('[');
  end { case fCurrCtx };
  IncrementOffset();
  fbWriteSep := False
end { WriteStart };


function  TGSkCustomJSON.WriteStartArray() : TGSkCustomJSON;
begin
  WriteStart(jcArray);
  WriteOffset();
  fbSkipOffset := True;
  Result := Self
end { WriteStartArray };


function  TGSkCustomJSON.WriteStartObject() : TGSkCustomJSON;
begin
  WriteStart(jcObject);
  Result := Self
end { WriteStartObject };


function  TGSkCustomJSON.WriteValue(const   nValue:  Extended)
                                    : TGSkCustomJSON;
begin
  WriteSeparator();
  fWriter.Write(StringReplace(FloatToStr(nValue), ',', '.', []));
  fbWriteSep := True;
  fbSkipOffset := False;
  Result := Self
end { WriteValue };


function  TGSkCustomJSON.WriteValue(const   nValue:  Currency)
                                    : TGSkCustomJSON;
begin
  WriteSeparator();
  fWriter.Write(StringReplace(FloatToStr(nValue), ',', '.', []));
  fbWriteSep := True;
  fbSkipOffset := False;
  Result := Self
end { WriteValue };


function  TGSkCustomJSON.WriteValue(const   tmValue:  TDateTime)
                                    : TGSkCustomJSON;

  function  ConvDateTime(const   tmValue:  TDateTime)
                         : string;
  begin
    Result := DateToISO8601(tmValue, False);
    // Result = yyyy-mm-ddThh:mm:ss.zzz+hh:mm
    //          123456789o123456789o123456789
    { Remove ".zzz" chunk }
    Assert(Result[20] = '.');
    Delete(Result, 20, 4)
  end { ConvDateTime };

begin  { WriteValue }
  WriteSeparator();
  fWriter.Write('"');
  FWriter.Write(ConvDateTime(tmValue));
  fWriter.Write('"');
  fbWriteSep := True;
  fbSkipOffset := False;
  Result := Self
end { WriteValue };


function  TGSkCustomJSON.WriteValue(const   sValue:  string)
                                    : TGSkCustomJSON;
begin  { WriteValue }
  WriteSeparator();
  fWriter.Write('"');
  fWriter.Write(EncodeString(sValue));
  fWriter.Write('"');
  fbWriteSep := True;
  fbSkipOffset := False;
  Result := Self
end { WriteValue };


function  TGSkCustomJSON.WriteValue(const   nValue:  Integer)
                                    : TGSkCustomJSON;
begin
  WriteSeparator();
  fWriter.Write(nValue);
  fbWriteSep := True;
  fbSkipOffset := False;
  Result := Self
end { WriteValue };


function  TGSkCustomJSON.WriteValue() : TGSkCustomJSON;
begin
  WriteSeparator();
  fWriter.Write('null');
  fbWriteSep := True;
  fbSkipOffset := False;
  Result := Self
end { WriteValue };

{$ENDREGION 'TGSkCustomJSON'}


{$REGION 'TGSkJSON'}

function  TGSkJSON.Clear() : TGSkJSON;
begin
  DestroyBuilderAndWriter();
  CreateBuilderAndWriter();
  AssignWriter(fWriter);
  Result := Self
end { Clear };


constructor  TGSkJSON.Create(const   bIndented:  Boolean);
begin
  CreateBuilderAndWriter();
  inherited Create(fWriter, bIndented)
end { Create };


procedure  TGSkJSON.CreateBuilderAndWriter();
begin
  fBuilder := TStringBuilder.Create();
  fWriter := TStringWriter.Create(fBuilder);
end { CreateBuilderAndWriter };


destructor  TGSkJSON.Destroy();
begin
  DestroyBuilderAndWriter();
  inherited
end { Destroy };


procedure  TGSkJSON.DestroyBuilderAndWriter();
begin
  fWriter.Free();
  fBuilder.Free();
end { DestroyBuilderAndWriter };

{$ENDREGION 'TGSkJSON'}


end.
