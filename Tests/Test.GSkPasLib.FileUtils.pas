﻿unit  Test.GSkPasLib.FileUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}


uses
  {$if Defined(D2007) }
    Windows,
  {$ifend}
  GSkPasLib.FileUtils, TestFrameWork;


type
  TFileUtils_Test = class(TTestCase)
  published
    procedure  TestFilenameFunctions();
    procedure  TestVersionFunctions();
  end { TFileUtils_Test };


implementation


uses
  SysUtils;


{ TFileUtils_Test }

procedure  TFileUtils_Test.TestFilenameFunctions();

const          {123456789.123456789.123456789.123456789.}
  csTestName = 'x:\Folder1\Folder2\Folder3\Filename.ext';
  csTest35   = 'x:\...\Folder2\Folder3\Filename.ext';
  csTest30   = 'x:\...\Folder3\Filename.ext';
  csTest20   = 'x:\...\Filename.ext';
  csTest17   = '...\Filename.ext';
  csTest10   = 'Filename.ext';

begin  { TestFilenameFunctions }
  Check(MinimizePath(csTestName, 40) = csTestName, 'nie skrócony');
  Check(MinimizePath(csTestName, 35) = csTest35, 'bez pierwszego foldera');
  Check(MinimizePath(csTestName, 30) = csTest30, 'tylko ostatni folder');
  Check(MinimizePath(csTestName, 20) = csTest20, 'bez folderów');
  Check(MinimizePath(csTestName, 17) = csTest17, 'bez dysku');
  Check(MinimizePath(csTestName, 10) = csTest10, 'tylko nazwa pliku');
  Check(DefaultFileExt('Nazwa.typ', '.ext') = 'Nazwa.typ', 'DefaultExt - bez zmian');
  Check(DefaultFileExt('Nazwa',     '.ext') = 'Nazwa.ext', 'DefaultExt - zmiana');
  Check(AnsiSameText(GetModuleFileName(), ParamStr(0)));
end { TestFilenameFunctions };


procedure  TFileUtils_Test.TestVersionFunctions();

var
  nMajor, nMinor, nRelease, nBuild:  Word;

begin  { TestVersionFunctions }
  Check(CompareVersion('1.0.0.0', '1.0.0.0', 4) = EqualsValue,
        'Porównanie wersji z dokładnością do 4 elementów');
  Check(CompareVersion('1.2.3.4', '1.2.3.x') = EqualsValue,
        'Porównanie wersji z domyślną dokładnością');
  Check(CompareVersion('1.2.3.4', '1.2.4.6') = LessThanValue,
        'Pierwsza wersja mniejsza od drugiej (domyślna precyzja)');
  Check(CompareVersion('9.8.7.6', '5.4.3.2') = GreaterThanValue,
        'Pierwsza wersja większa od drugiej');
  Check(TrimVersion('1.2.3.4', 3) = '1.2.3',
        'Wersja skrócona do 3 elementów');
  Check(TrimVersion('1.2.3.4', 2) = '1.2',
        'Wersja skrócona do 2 elementów');
  Check(TrimVersion('1.2.3.4', 1) = '1.2',
        'Wersja skrócona do 1 elementu');
  {-}
  Check(AnsiSameText(GetModuleInternalName(), 'GSkPasLibTest'),
        'Wewnętrzna nazwa tego programu');
  {-}
  GetModuleVersion(nMajor, nMinor, nRelease, nBuild);
  Check((nMajor = 4) and (nMinor = 2015) and (nRelease = 2) and (nBuild > 0),
        'Elementy wersji tego programu');
  {-}
  Check(GetModuleVersion() = '4.2015.2', 'Domyślna wersja tego programu')
end { TestVersionFunctions };


initialization
  RegisterTest(TFileUtils_Test.Suite())


end.
