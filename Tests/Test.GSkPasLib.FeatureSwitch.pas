﻿unit  Test.GSkPasLib.FeatureSwitch;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


{$TYPEINFO ON}


uses
  System.SysUtils, System.IniFiles,
  TestFramework,
  GSkPasLib.FeatureSwitch;


type
  TVariable = record
    Name:  string;
    Value: Boolean;
  end { TVariable };

  TFeatureSwitch_Test = class(TTestCase)
  strict private
    procedure  Setup(const   Variables:  array of TVariable);        reintroduce; overload;
  public
    procedure  SetUp();                                              overload; override;
    procedure  TearDown();                                           override;
  published
    procedure  GetValueNoFile();
    procedure  GetDefaultValueNoFile();
    procedure  GetValueNoDef();
    procedure  GetValueDef();
  end { TFeatureSwitch_Test };


implementation


uses
  GSkPasLib.FileUtils;


var
  gsConfigFilename:  string;


{$REGION 'TFeatureSwitch_Test'}

procedure  TFeatureSwitch_Test.GetDefaultValueNoFile();
begin
  CheckTrue(TGSkFeatureSwitch.Feature('NoFile', True))
end { GetDefaultValueNoFile };


procedure  TFeatureSwitch_Test.GetValueDef();

const
  cVars:  array[1..2] of  TVariable = (
              (Name: 'TestVarTrue'; Value: True),
              (Name: 'TestVarFalse'; Value: False));

begin
  SetUp(cVars);
  CheckTrue(TGSkFeatureSwitch.Feature('TestVarTrue', True));
  CheckFalse(TGSkFeatureSwitch.Feature('TestVarFalse', False))
end { GetValueDef };


procedure  TFeatureSwitch_Test.GetValueNoDef();
begin
  SetUp([]);
  CheckFalse(TGSkFeatureSwitch.Feature('NotDefined'))
end { GetValueNoDef };


procedure  TFeatureSwitch_Test.GetValueNoFile();
begin
  CheckFalse(TGSkFeatureSwitch.Feature('FeatureTest'))
end { GetValueNoFile };


procedure  TFeatureSwitch_Test.Setup();
begin
  TGSkFeatureSwitch.InitializeFeatures(gsConfigFilename)
end { Setup };


procedure  TFeatureSwitch_Test.TearDown();
begin
  inherited;
  DeleteFile(gsConfigFilename)
end { TearDown };


procedure  TFeatureSwitch_Test.Setup(const   Variables:  array of TVariable);

var
  lConf:  TIniFile;
  lVar:   TVariable;

begin  { Setup }
  lConf := TIniFile.Create(gsConfigFilename);
  try
    lConf.WriteBool('Create', 'Noname', False);
    for  lVar in Variables  do
      lConf.WriteBool(gcsDefaultFeaturesSectionName, lVar.Name, lVar.Value)
  finally
    lConf.Free()
  end { try-finally }
end { Setup };

{$ENDREGION 'TFeatureSwitch_Test'}


initialization
  gsConfigFilename := ExtractFilePath(GetModuleFileName()) + 'Test.ini';
  RegisterTest(TFeatureSwitch_Test.Suite())


end.
