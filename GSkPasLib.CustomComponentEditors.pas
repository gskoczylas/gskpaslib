﻿{: @exclude
   Wewnętrznych modułów biblioteki nie komentuję (na razie).  }
unit  GSkPasLib.CustomComponentEditors;

(**********************************************
 *                                            *
 *                 G S k L i b                *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  DesignIntf, DesignEditors, DesignMenus,
  SysUtils;


const
  gcnStdVerbMaxInd = 0;


type
  TGSkComponentEditor = class(TComponentEditor)
  public
    function  GetVerbCount() : Integer;                              override;
    function  GetVerb(Index:  Integer)
                      : string;                                      override;
    procedure  ExecuteVerb(Index:  Integer);                         override;
    procedure  Edit();                                               override;
  end { TCompEdDataEmbedded };

  TCompEdCustomDialog = class(TGSkComponentEditor)
  public
    function  GetVerbCount() : Integer;                              override;
    function  GetVerb(Index:  Integer)
                      : string;                                      override;
    procedure  ExecuteVerb(Index:  Integer);                         override;
  end { TCompEdCustomDialog };



implementation


uses
  GSkPasLib.Dialogs, GSkPasLib.CustomComponents, GSkPasLib.PropEdAboutForm,
  GSkPasLib.PasLibInternals, GSkPasLib.PasLibReg;


const
  csExecute = 'Execute...';


{ TGSkComponentEditor }

procedure  TGSkComponentEditor.Edit();
begin
  // inherited;
  if  GetVerbCount() <= gcnStdVerbMaxInd  then
    ExecuteVerb(0)
  else
    ExecuteVerb(gcnStdVerbMaxInd + 1)
end { Edit };


procedure  TGSkComponentEditor.ExecuteVerb(Index:  Integer);
begin
  // inherited;
  Assert(gcnStdVerbMaxInd = 0);
  case  Index  of
    0:  TPropEdAboutForm.Execute(Component.ClassName)
  end { case Index }
end { ExecuteVerb };


function  TGSkComponentEditor.GetVerb(Index:  Integer)
                                      : string;
begin
  if  Index = 0  then
    Result := gcsAboutLib + '...'
  else
    Result := Format('[%d]', [Index]);
end { GetVerb };


function  TGSkComponentEditor.GetVerbCount() : Integer;
begin
  Assert(gcnStdVerbMaxInd = 0);
  Result := 1
end { GetVerbCount };


{ TCompEdCustomDialog }

procedure  TCompEdCustomDialog.ExecuteVerb(Index:  Integer);
begin
  try
    if  Index <= gcnStdVerbMaxInd  then
      inherited ExecuteVerb(Index)
    else
      case  Index  of
        gcnStdVerbMaxInd + 1:
          if  (Component is TGSkCustomDialogP)  then
            (Component as TGSkCustomDialogP).Execute()
          else if  Component is TGSkCustomDialogFB  then
            MsgDlg('Execute() = ' +
                     BoolToStr((Component as TGSkCustomDialogFB).Execute(),
                               True),
                   mtInformation)
          else if  Component is TGSkCustomDialogFI  then
            MsgDlg('Execute() = ' +
                     IntToStr((Component as TGSkCustomDialogFI).Execute()),
                   mtInformation)
      end { case Index }
  except
    on  eErr : Exception  do
      ShowDesingError(eErr)
  end { try-except }
end { ExecuteVerb };


function  TCompEdCustomDialog.GetVerb(Index:  Integer)
                                      : string;
begin
  if  Index <= gcnStdVerbMaxInd  then
    Result := inherited GetVerb(Index)
  else
    case  Index  of
      gcnStdVerbMaxInd + 1:
        Result := csExecute;
      else
        Result := Format('[%d]', [Index])
    end { csae Index }
end { GetVerb };


function  TCompEdCustomDialog.GetVerbCount() : Integer;
begin
  Result := inherited GetVerbCount() + 1
end { GetVerbCount };


end.
