﻿{: @abstract Moduł @name definiuje kody sterujące ASCII. }
unit  GSkPasLib.ASCII;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ————————————————————————————————       *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} //XE
    System.SysUtils;
  {$ELSE}
    SysUtils;
  {$IFEND}


const
  {: @abstract Null,                 #$00     } NUL = #0;
  {: @abstract Start Of Header,      #$01, ^A } SOH = #1;
  {: @abstract Start Of Text,        #$02, ^B } STX = #2;
  {: @abstract End Of Text,          #$03, ^C } ETX = #3;
  {: @abstract End Of Transmission,  #$04, ^D } EOT = #4;
  {: @abstract Enquiry,              #$05, ^E } ENQ = #5;
  {: @abstract Acknowledgement,      #$06, ^F } ACK = #6;
  {: @abstract Bell,                 #$07, ^G } BEL = #7;
  {: @abstract Backspace,            #$08, ^H } BS  = #8;
  {: @abstract Horizontal Tab,       #$09, ^I } TAB = #9;
  {: @abstract Line Feed,            #$0A, ^J } LF  = #10;
  {: @abstract Vertical Tab,         #$0B, ^K } VT  = #11;
  {: @abstract Form Feed,            #$0C, ^L } FF  = #12;
  {: @abstract Carriage Return,      #$0D, ^M } CR  = #13;
  {: @abstract Shift Out,            #$0E, ^N } SO  = #14;
  {: @abstract Shift In,             #$0F, ^O } SI  = #15;
  {: @abstract Delete,               #$10, ^P } DLE = #16;
  {: @abstract Device Control 1,     #$11, ^Q } DC1 = #17;
  {: @abstract Device Control 2,     #$12, ^R } DC2 = #18;
  {: @abstract Device Control 3,     #$13, ^S } DC3 = #19;
  {: @abstract Device Control 4,     #$14, ^T } DC4 = #20;
  {: @abstract Negative Acknowledge, #$15, ^U } NAK = #21;
  {: @abstract Synchronize,          #$16, ^V } SYN = #22;
  {: @abstract End Block,            #$17, ^W } ETB = #23;
  {: @abstract Cancel,               #$18, ^X } CAN = #24;
  {: @abstract End Message,          #$19, ^Y } EM  = #25;
  {: @abstract Sub,                  #$1A, ^Z } SUB = #26;
  {: @abstract Escape,               #$1B     } ESC = #27;
  {: @abstract Form Seperator,       #$1C     } FS  = #28;
  {: @abstract Group Separator,      #$1D     } GS  = #29;
  {: @abstract Record Separator,     #$1E     } RS  = #30;
  {: @abstract Unit Separator,       #$1F     } US  = #31;
  {: @abstract Space,                #$20     } SP  = #32;
  {-}
  {: @abstract Delete,               #$7F     } DEL = #$7F;
  {: @abstract Device Control String #$90     } DCS = #$90;
  {: @abstract App Program Command   #$9F     } APC = #$9F;
  {: @abstract Non-Breaking Space,   #$A0     } NBSP= #$A0;

  Xon  = #17;
  Xoff = #19;

  gcsetNewLine = [CR, LF];
  gcsetControlChars = [#0..#31, #$7F];
  gcsetControlCharsEx = gcsetControlChars + [#$80, DCS, APC, #$FF];
  gcsetControlCharsExNBSP = gcsetControlCharsEx + [NBSP];
  gcsetControlCharsUnicode = gcsetControlChars + [#$80..#$9F];


{: @abstract Zwraca nazwe wskazanego znaku.

   Funkcja @name przeznaczona jest głównie do pobierania mnemoników znaków
   sterujących: #0..#31 (poniżej spacji), #160 (twarda spacja), #127 (DEL)
   oraz #255.

   W przypadku pozostałych znaków wynikiem funkcji jest ten znak ujęty w cudzysłowy. }
function  CharName(const   chVal:  AnsiChar)
                   : string;

{$IFNDEF UNICODE}

{: @abstract Zwraca wynik wyrażenia @code(chChar in setCharSet).

   Funkcja @name jest dostępna tylko w Delphi/ANSI --- dla zgodności z nowszymi
   wersjami Delphi. }
function  CharInSet(const   chChar:  AnsiChar;
                    const   setCharSet: TSysCharSet)
                    : Boolean;                                       overload; inline;

{: @abstract Zwraca wynik wyrażenia @code(chChar in setCharSet).

   Funkcja @name jest dostępna tylko w Delphi/ANSI --- dla zgodności z nowszymi
   wersjami Delphi. }
function  CharInSet(const   chChar:  WideChar;
                    const   setCharSet: TSysCharSet)
                    : Boolean;                                       overload; inline;

{$ENDIF -UNICODE}


implementation


const
  cCtrlName:  array[NUL..US] of  string = (
                  'NUL', 'SOH', 'STX', 'ETX', 'EOT', 'ENQ', 'ACK', 'BEL', 'BS',
                  'TAB', 'LF', 'VT', 'FF', 'CR', 'SO', 'SI', 'DLE', 'DC1', 'DC2',
                  'DC3', 'DC4', 'NAK', 'SYN', 'ETB', 'CAN', 'EM', 'SUB', 'ESC',
                  'FS', 'GS', 'RS', 'US');


function  CharName(const   chVal:  AnsiChar)
                   : string;
begin
  case  chVal  of
    NUL..US:
      Result := cCtrlName[chVal];
    DEL:
      Result := 'DEL';
    DCS:
      Result := 'DCS';
    APC:
      Result := 'APC';
    NBSP,   // twarda spacja
    #$FF:
      Result := Format('#$%x', [Ord(chVal)]);
    '''':
      Result := '''''''';    // '''
    else
      Result := '''' + chVal + ''''
  end { case chVal }
end { CharName };


{$IFNDEF UNICODE}

function  CharInSet(const   chChar:  AnsiChar;
                    const   setCharSet: TSysCharSet)
                    : Boolean;
begin
  Result := chChar in setCharSet
end { CharInSet };


function  CharInSet(const   chChar:  WideChar;
                    const   setCharSet: TSysCharSet)
                    : Boolean;
begin
  Result := AnsiChar(chChar) in setCharSet
end { CharInSet };

{$ENDIF -UNICODE}


end.
