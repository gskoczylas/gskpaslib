﻿{: @abstract Podprogramy ułatwiające używanie plików XML.

   Moduł @name zawiera podprogramy ułatwiające używanie plików w formacie XML.
   Duża część tych podprogramów to funkcje konwersji różnych wartości na napis
   i odwrotnie. Wynikowe napisy mają postać niezależną od @italic(Opcji
   regionalnych) w @italic(Panelu sterowania).  }
unit  GSkPasLib.XMLUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$IF CompilerVersion > 22} // XE
  {$LEGACYIFEND ON}
{$IFEND}


uses
  {$IF CompilerVersion > 22} // XE
    WinAPI.Windows,
    System.SysUtils, System.Math, System.Types, System.Win.ComObj,
    XML.XMLIntf, XML.xmldom, XML.XMLDoc,
    VCL.Graphics,
  {$ELSE}
    Windows, SysUtils, Types, ComObj, XMLIntf, xmldom, XMLDoc, Graphics,
  {$IFEND}
  {$IFDEF GetText}
    JvGnugettext,
  {$ENDIF GetText}
  GSkPasLib.PasLibInternals;


const
  {: @abstract Domyślne kodowanie dokumentów XML. }
  gcsEncodingWindowsXML = 'Windows-1250';

  gcsTrueXML:   DOMString = 'true';   gcsFalseXML:  DOMString = 'false';
  gcsTrue1XML:  DOMString = 'yes';    gcsFalse1XML: DOMString = 'no';
  gcsTrue2XML:  DOMString = 'tak';    gcsFalse2XML: DOMString = 'nie';
  gcsTrue3XML:  DOMString = 'y';      gcsFalse3XML: DOMString = 'n';
  gcsTrue4XML:  DOMString = 't';      gcsFalse4XML: DOMString = 'f';

  {: @abstract Niepoprawnie zakodowana wartość logiczna. }
  gcnErrXML_IncorrectBooleanValue = 1;

  {: @abstract Niepoprawnie zakodowana data. }
  gcnErrXML_IncorrectDateValue = 2;

  {: @abstract Niepoprawnie zakodowany czas. }
  gcnErrXML_IncorrectTimeValue = 3;

  {: @abstract Niepoprawnie zakodowana data i czas. }
  gcnErrXML_IncorretDateTimeValue = 4;

  {: @abstract Niepoprawnie zakodowana liczba rzeczywista. }
  gcnErrXML_IncorrectFloatValue = 5;

  {: @abstract Węzeł XML nie ma zdefiniowanego wymaganego atrybutu. }
  gcnErrXML_MissingAttrib = 6;

  {: @abstract Niepoprawna wartość atrybutu. }
  gcnErrXML_WrongAttribValue = 7;

  {: @abstract Niepoprawne biblioteki systemowe. }
  gcnErrXML_MissingLib = 8;

  {: @abstract Niepoprawna wartość węzła. }
  gcnErrXML_WrongNodeValue = 9;

  {: @abstract Węzeł nie istnieje. }
  gcnErrXML_MissingNode = 10;

  {: @abstract Niepoprawnie zakodowana kwota (liczba rzeczywista). }
  gcnErrXML_IncorrectCurrencyValue = 11;


const
  {$IFNDEF GetText}
    {: @abstract Niepoprawnie zakodowana wartość logiczna. }
    gcsErrXML_IncorrectBooleanValue = 'Niepoprawnie zakodowana wartość logiczna: %s';
    {: @abstract Niepoprawnie zakodowana data. }
    gcsErrXML_IncorrectDateValue = 'Niepoprawnie zakodowana data: %s';
    {: @abstract Niepoprawnie zakodowany czas. }
    gcsErrXML_IncorrectTimeValue = 'Niepoprawnie zakodowany czas: %s';
    {: @abstract Niepoprawnie zakodowana data i czas. }
    gcsErrXML_IncorretDateTimeValue = 'Niepoprawnie zakodowana data i czas: %s';
    {: @abstract Niepoprawnie zakodowana liczba rzeczywista. }
    gcsErrXML_IncorrectFloatValue = 'Niepoprawnie zakodowana liczba rzeczywista: %s';
    {: @abstract Węzeł XML nie ma zdefiniowanego wymaganego atrybutu. }
    gcsErrXML_MissingAttrib = 'Węzeł „%s” nie ma wymaganego atrybutu „%s”';
    {: @abstract Niepoprawna wartość atrybutu. }
    gcsErrXML_WrongAttribValue = 'W węźle „%s” atrybut „%s” ma niepoprawną wartość: %s'+ sLineBreak +'%s';
    {: @abstract Niepoprawna wartość węzła. }
    gcsErrXML_WrongNodeValue = 'Węzeł „%s” ma niepoprawną wartość: %s' + sLineBreak +'%s';
    {: @abstract Niepoprawne biblioteki systemowe. }
    gcsErrXML_MissingLib1 = 'Nie ma wymaganych bibliotek systemowych - błąd $%x';
    gcsErrXML_MissingLib2 = 'Zaktualizuj biblioteki systemu';
    {: @abstract Brak wskazanego węzła. }
    gcsErrXML_MissingNode = 'Węzeł „%s” nie istnieje.';
  {$ELSE}
    {: @abstract Niepoprawnie zakodowana wartość logiczna. }
    gcsErrXML_IncorrectBooleanValue = 'Incorrect logical value: %s';
    {: @abstract Niepoprawnie zakodowana data. }
    gcsErrXML_IncorrectDateValue = 'Incorrect date: %s';
    {: @abstract Niepoprawnie zakodowany czas. }
    gcsErrXML_IncorrectTimeValue = 'Incorrect time: %s';
    {: @abstract Niepoprawnie zakodowana data i czas. }
    gcsErrXML_IncorretDateTimeValue = 'Incorrect date and time: %s';
    {: @abstract Niepoprawnie zakodowana liczba rzeczywista. }
    gcsErrXML_IncorrectFloatValue = 'Incorrect float number: %s';
    {: @abstract Węzeł XML nie ma zdefiniowanego wymaganego atrybutu. }
    gcsErrXML_MissingAttrib = 'The node ''%0:s'' does not have required attribute ''%1:s''';
    {: @abstract 'Niepoprawna wartość atrybutu. }
    gcsErrXML_WrongAttribValue = 'In the node ''%0:s'' the attribute ''%1:s'' has incorrect value: %2:s'+ sLineBreak +'%3:s';
    {: @abstract Niepoprawna wartość węzła. }
    gcsErrXML_WrongNodeValue = 'The node ''%0:s'' has incorrect value: %1:s' + sLineBreak +'%2:s';
    {: @abstract Niepoprawne biblioteki systemowe. }
    gcsErrXML_MissingLib1 = 'Required system library is missing - error $%x';
    gcsErrXML_MissingLib2 = 'Update system libraries';
    {: @abstract Brak wskazanego węzła. }
    gcsErrXML_MissingNode = 'Node "%s" does not exists';
  {$ENDIF GetText}
  gcsErrXML_MissingLibFmt = '%0:s:'+ sLineBreak +'%1:s.'+ sLineBreak + sLineBreak +'%2:s';


type
  {: @abstract Wyjątki sygnalizowane przez podprogramy modułu @link(GSkPasLib.XMLUtils).

     Jeżeli podprogramy modułu @link(GSkPasLib.XMLUtils) wykryją błąd to zgłaszają
     wyjątek @name. }
  EGSkXMLError = class(EGSkPasLibErrorEx)
  public
    constructor  Create(const   sMethod:  string;
                        const   nErrCode:  Integer;
                        const   sErrMsg:  string);
  end { EGSkXMLError };


{: @abstract Konwersja wartości logicznej na napis.

   Funkcja zamienia wartość @code(bValue) na napis zdefioniowany przez stałą
   @link(gcsTrueXML) lub @link(gcsFalseXML).

   @seeAlso(StrToBoolXML)  }
function  BoolToStrXML(const   bValue:  Boolean)
                       : DOMString;

{: @abstract Konwersja napisu na wartość logiczną.

   Funkcja zamienia napis @code(sValue) na wartość logiczną. Napis musi być
   jedną ze stałych @link(gcsTrueXML), @link(gcsTrue1XML gcsTrue?XML),
   @link(gcsFalseXML) lub @link(gcsFalse1XML gcsFalse?XML). Może to być również
   liczba całkowita --- wtedy wartość @italic(zero) oznacza fałsz, a pozostałe
   wartości oznaczają prawdę.

   @seeAlso(BoolToStrXML)  }
function  StrToBoolXML(const   sValue:  DOMString)
                       : Boolean;

{: @abstract Konweresja daty na napis.

   Funkcja @name zamienia datę @code(tmValue) na napis. Wynikowy napis ma format
   niezależny od @italic(Opcji regionalnych) w @italic(Panelu sterowania).

   Konwersji podlega tylko data. Czas jest ignorowany.

   @seeAlso(StrToDateXML)  }
function  DateToStrXML(const   tmValue:  TDateTime)
                       : DOMString;

{: @abstract Konwersja napisu na datę.

   Funkcja @name zamienia napis @code(sValue) na datę. Napis musi być
   sformatowany tak jak to robi funkcja @link(DateToStrXML).

   Napis wynikowy jest sformatowany według wzorca @code(yyyy-mm-dd).

   @seeAlso(DateToStrXML)  }
function  StrToDateXML(const   sValue:  DOMString)
                       : TDateTime;

{: @abstract Konweresja czasu na napis.

   Funkcja @name zamienia czas @code(tmValue) na napis. Wynikowy napis ma format
   niezależny od @italic(Opcji regionalnych) w @italic(Panelu sterowania).

   Konwersji podlega tylko czas. Data jest ignorowana.

   @seeAlso(StrToTimeXML)  }
function  TimeToStrXML(const   tmValue:  TDateTime)
                       : DOMString;

{: @abstract Konwersja napisu na czas.

   Funkcja @name zamienia napis @code(sValue) na czas. Napis musi być
   sformatowany tak jak to robi funkcja @link(TimeToStrXML).

   Napis wynikowy jest sformatowany według wzorca @code(hh:nn:ss.zzz).

   @seeAlso(TimeToStrXML)  }
function  StrToTimeXML(const   sValue:  DOMString)
                       : TDateTime;

{: @abstract Konweresja daty i czasu na napis.

   Funkcja @name zamienia czas @code(tmValue) na napis. Wynikowy napis ma format
   niezależny od @italic(Opcji regionalnych) w @italic(Panelu sterowania).

   Napis wynikowy jest sformatowany według wzorca @code(yyyy-mm-dd, hh:nn:ss.zzz).

   @seeAlso(StrToDateTimeXML)  }
function  DateTimeToStrXML(const   tmValue:  TDateTime)
                           : DOMString;

{: @abstract Konwersja napisu na datę i czas.

   Funkcja @name zamienia napis @code(sValue) na datę i czas. Napis musi być
   sformatowany tak jak to robi funkcja @link(DateTimeToStrXML).

   @seeAlso(DateTimeToStrXML)  }
function  StrToDateTimeXML(const   sValue:  DOMString)
                           : TDateTime;

{: @abstract Konweresja liczby rzeczywistej na napis.

   Funkcja @name zamienia liczbę @code(nValue) na napis. Wynikowy napis ma
   format niezależny od @italic(Opcji regionalnych) w @italic(Panelu sterowania).

   Wynikowy napis reprezentuje liczbę @code(nValue). Do oddzielenia części
   całkowitej od części ułamkowej stosowany jest znak „.” (kropka).

   @seeAlso(StrToFloatXML)
   @seeAlso(CurrencyToStrXML)  }
function  FloatToStrXML(const   nValue:  Extended)
                        : DOMString;

{: @abstract Konwersja napisu na liczbę rzeczywistą.

   Funkcja @name zamienia napis @code(sValue) na liczbę rzeczywistą. Napis musi
   być sformatowany tak jak to robi funkcja @link(FloatToStrXML).

   @seeAlso(CurrencyToStrXML)  }
function  StrToFloatXML(const   sValue:  DOMString)
                        : Extended;

{: @abstract Konweresja waluty na napis.

   Funkcja @name zamienia liczbę @code(nValue) na napis. Wynikowy napis ma
   format niezależny od @italic(Opcji regionalnych) w @italic(Panelu sterowania).

   Wynikowy napis reprezentuje liczbę @code(nValue). Do oddzielenia części
   całkowitej od części ułamkowej stosowany jest znak „.” (kropka).

   @seeAlso(StrToFloatXML)
   @seeAlso(CurrencyToStrXML)  }
function  CurrencyToStrXML(const   nValue:  Currency;
                           const   nPrecision:  Integer = 2)
                           : DOMString;

{: @abstract Konwersja napisu na walutę.

   Funkcja @name zamienia napis @code(sValue) na walutę. Napis musi
   być sformatowany tak jak to robi funkcja @link(CurrencyToStrXML).

   @seeAlso(CurrencyToStrXML)  }
function  StrToCurrencyXML(const   sValue:  DOMString)
                           : Currency;

{: @abstract Ścieżka dostępu do węzła XML.

   Wynikiem działania funkcji jest ścieżka dostępu do wskazanego węzła.
   Wybnikowy napis składa się z głównego węzła XML i kolejnych węzłów, przez
   które trzeba przejść aby dojść do węzła @code(iNode). Kolejne węzły
   oddzielone są od siebie znakiem „\” (stała „PathDelim” ze standardowego
   modułu „SysUtils”).  }
function  GetNodePath(iNode:  IXMLNode)
                      : string;

{: @abstract Pobiera atrybut węzła.

   Funkcja @name pobiera atrybut węzła. Jeżeli atrybut jest wymagany ale nie
   występuje to generuje wyjątek @link(EGSkXMLError).

   @param iNode Węzeł XML.
   @param sAttribName Nazwa atrybutu.
   @param bRequired Czy sygnalizować wyjątek gdy atrybut nie istnieje.

   @returns(Wynikiem funkcji jest adres znalezionego atrybutu lub @nil gdy
            atrybut nie istnieje.)  }
function  GetAttribNode(const   iNode:  IXMLNode;
                        const   sAttribName:  DOMString;
                        const   bRequired:  Boolean = True)
                        : IXMLNode;

{: @abstract Pobiera wartość atrybutu --- liczbę całkowitą.

   Funkcja @name pobiera wartość atrybutu. Wartość ta musi być poprawną liczbą
   całkowitą. Liczba może być zapisana w systemie szesnastkowym zgodnie z
   zasadami języka @italic(Pascal) --- musi być poprzedzona znakiem „$”.

   @param iNode Węzeł XML.
   @param sAttribName Nazwa atrybutu.
   @param(bHex Wymusza interpretowanie atrybutu jako liczby szesnastkowej, mimo
               że liczba nie jest poprzedzona znakiem „$”.)
   @param nDefault Wartość domyślna zwracana gdy atrybut nie istnieje.

   @return(Wynikiem funkcji jest wartość atrybutu lub wartość domyślna jeżeli
           wskazany atrybut nie istnieje.)

   @seeAlso(GetAttribNode)
   @seeAlso(GetAttribCardinal)  }
function  GetAttribInt(const   iNode:  IXMLNode;
                       const   sAttribName:  DOMString;
                       const   bHex:  Boolean = False;
                       const   nDefault:  Integer = 0)
                       : Integer;

{: @abstract Pobiera wartość atrybutu --- liczbę całkowitą.

   Funkcja @name pobiera wartość atrybutu. Wartość ta musi być poprawną liczbą
   całkowitą. Liczba może być zapisana w systemie szesnastkowym zgodnie z
   zasadami języka @italic(Pascal) --- musi być poprzedzona znakiem „$”.

   @param iNode Węzeł XML.
   @param sAttribName Nazwa atrybutu.
   @param(bHex Wymusza interpretowanie atrybutu jako liczby szesnastkowej, mimo
               że liczba nie jest poprzedzona znakiem „$”.)
   @param nDefault Wartość domyślna zwracana gdy atrybut nie istnieje.

   @return(Wynikiem funkcji jest wartość atrybutu lub wartość domyślna jeżeli
           wskazany atrybut nie istnieje.)

   @seeAlso(GetAttribNode)
   @seeAlso(GetAttribCardinal)  }
function  GetAttribInt64(const   iNode:  IXMLNode;
                         const   sAttribName:  DOMString;
                         const   bHex:  Boolean = False;
                         const   nDefault:  Int64 = 0)
                         : Int64;

{: @abstract Pobiera wartość atrybutu --- liczbę całkowitą bez znaku.

   Funkcja @name pobiera wartość atrybutu. Wartość ta musi być poprawną liczbą
   całkowitą bez znaku. Liczba może być zapisana w systemie szesnastkowym zgodnie z
   zasadami języka @italic(Pascal) --- musi być poprzedzona znakiem „$”.

   @param iNode Węzeł XML.
   @param sAttribName Nazwa atrybutu.
   @param(bHex Wymusza interpretowanie atrybutu jako liczby szesnastkowej, mimo
               że liczba nie jest poprzedzona znakiem „$”.)
   @param nDefault Wartość domyślna zwracana gdy atrybut nie istnieje.

   @return(Wynikiem funkcji jest wartość atrybutu lub wartość domyślna jeżeli
           wskazany atrybut nie istnieje.)

   @seeAlso(GetAttribNode)
   @seeAlso(GetAttribInt)  }
function  GetAttribCardinal(const   iNode:  IXMLNode;
                            const   sAttribName:  DOMString;
                            const   bHex:  Boolean = False;
                            const   nDefault:  Cardinal = 0)
                            : Cardinal;

{: @abstract Pobiera wartość atrybutu --- liczbę rzeczywistą (@code(Double)).

   Funkcja @name pobiera wartość atrybutu reprezentującego liczbę (tak jak
   w funkcji @code(FloatToStrXML) lub @code(StrToFloatXML)).

   @param iNode Węzeł XML.
   @param sAttribName Nazwa atrybutu.
   @param nDefault Wartość domyślna zwracana gdy atrybut nie istnieje.

   @return(Wynikiem funkcji jest wartość atrybutu lub wartość domyślna jeżeli
           wskazany atrybut nie istnieje.)

   @seeAlso(GetAttribNode)  }
function  GetAttribFloat(const   iNode:  IXMLNode;
                         const   sAttribName:  DOMString;
                         const   nDefault:  Double = 0.0)
                         : TDateTime;

{: @abstract Pobiera wartość atrybutu --- napis.

   Funkcja @name pobiera wartość atrybutu będącego dowolnym napisem.

   @param iNode Węzeł XML.
   @param sAttribName Nazwa atrybutu.
   @param sDefault Wartość domyślna zwracana gdy atrybut nie istnieje.

   @return(Wynikiem funkcji jest wartość atrybutu lub wartość domyślna jeżeli
           wskazany atrybut nie istnieje.)

   @seeAlso(GetAttribNode)  }
function  GetAttribStr(const   iNode:  IXMLNode;
                       const   sAttribName:  DOMstring;
                       const   sDefault:  string = '')
                       : string;

{: @abstract Pobiera wartość atrybutu --- pojedynczy znak.

   Funkcja @name pobiera wartość atrybutu będącego pojedynczym znakiem.

   @param iNode Węzeł XML.
   @param sAttribName Nazwa atrybutu.
   @param chDefault Wartość domyślna zwracana gdy atrybut nie istnieje.

   @return(Wynikiem funkcji jest wartość atrybutu lub wartość domyślna jeżeli
           wskazany atrybut nie istnieje.)

   @seeAlso(GetAttribNode)  }
function  GetAttribChar(const   iNode:  IXMLNode;
                        const   sAttribName:  DOMString;
                        const   chDefault:  Char = #0)
                        : Char;

{: @abstract Pobiera wartość atrybutu --- wartość logiczną.

   Funkcja @name pobiera wartość atrybutu będącego wartością logiczną. Treść
   atrybutu musi być zgodna z funkcją @link(BoolToStrXML).

   @param iNode Węzeł XML.
   @param sAttribName Nazwa atrybutu.
   @param bDefault Wartość domyślna zwracana gdy atrybut nie istnieje.

   @return(Wynikiem funkcji jest wartość atrybutu lub wartość domyślna jeżeli
           wskazany atrybut nie istnieje.)

   @seeAlso(GetAttribNode)  }
function  GetAttribBool(const   iNode:  IXMLNode;
                        const   sAttribName:  DOMString;
                        const   bDefault:  Boolean = False)
                        : Boolean;

{: @abstract Pobiera wartość atrybutu --- kolor.

   Funkcja @name pobiera wartość atrybutu będącego zakodowanym kolorem (tak jak
   w standardowej funkcji @code(ColorToString)).

   @param iNode Węzeł XML.
   @param sAttribName Nazwa atrybutu.
   @param clDefault Wartość domyślna zwracana gdy atrybut nie istnieje.

   @return(Wynikiem funkcji jest wartość atrybutu lub wartość domyślna jeżeli
           wskazany atrybut nie istnieje.)

   @seeAlso(GetAttribNode)  }
function  GetAttribColor(const   iNode:  IXMLNode;
                         const   sAttribName:  DOMString;
                         const   clDefault:  TColor)
                         : TColor;

{: @abstract Pobiera wartość atrybutu --- datę lub czas.

   Funkcja @name pobiera wartość atrybutu reprezentującego datę lub czas (tak jak
   w funkcji @code(StrToDateTimeXML)).

   @param iNode Węzeł XML.
   @param sAttribName Nazwa atrybutu.
   @param tmDefault Wartość domyślna zwracana gdy atrybut nie istnieje.

   @return(Wynikiem funkcji jest wartość atrybutu lub wartość domyślna jeżeli
           wskazany atrybut nie istnieje.)

   @seeAlso(GetAttribNode)  }
function  GetAttribTime(const   iNode:  IXMLNode;
                        const   sAttribName:  DOMString;
                        const   tmDefault:  TDateTime)
                         : TDateTime;

{: @abstract Zwraca wskazany węzeł XML.

   Funkcja @name zwraca wskazany węzeł XML. Jeżeli wskazanego węzła nie ma,
   to generuje wyjątek @link(EGSkXMLError) zkodem błędu @link(gcnErrXML_MissingNode).

   @param(iXML Dokument XML.)
   @param(Path Lista nazw kolejnych poziomów węzłów.)

   @return(Wynikiem jest ostatni węzeł z listy wskazanej w parametrze @code(Path).) }
function  GetNode(const   iXML:  IXMLDocument;
                  const   Path:  array of string)
                  : IXMLNode;                                        overload;

{: @abstract Zwraca wskazany węzeł XML.

   Funkcja @name zwraca wskazany węzeł XML. Jeżeli wskazanego węzła nie ma,
   to generuje wyjątek @link(EGSkXMLError) zkodem błędu @link(gcnErrXML_MissingNode).

   @param(iXML Dokument XML.)
   @param(Path Lista nazw kolejnych poziomów węzłów.)

   @return(Wynikiem jest ostatni węzeł z listy wskazanej w parametrze @code(Path).
           Jeżeli parametr @code(Path) jest pusty, to funkcja zwraca główny
           węzeł dokumentu XML.) }
function  GetNode(const   iXML:  IXMLDocument;
                  const   Path:  TStringDynArray)
                  : IXMLNode;                                        overload;

{: @abstract Zwraca wskazany węzeł XML.

   Funkcja @name zwraca wskazany węzeł XML. Jeżeli wskazanego węzła nie ma,
   to generuje wyjątek @link(EGSkXMLError) zkodem błędu @link(gcnErrXML_MissingNode).

   @param(iXML Bazowy węzeł XML.)
   @param(Path Lista nazw kolejnych poziomów węzłów.)

   @return(Wynikiem jest ostatni węzeł z listy wskazanej w parametrze @code(Path).
           Jeżeli parametr @code(Path) jest pusty, to funkcja zwraca główny
           węzeł dokumentu XML.) }
function  GetNode(const   iRoot:  IXMLNode;
                  const   Path:  array of string)
                  : IXMLNode;                                        overload;

{: @abstract Zwraca wskazany węzeł XML.

   Funkcja @name zwraca wskazany węzeł XML. Jeżeli wskazanego węzła nie ma,
   to generuje wyjątek @link(EGSkXMLError) zkodem błędu @link(gcnErrXML_MissingNode).

   @param(iXML Bazowy węzeł XML.)
   @param(Path Lista nazw kolejnych poziomów węzłów.)

   @return(Wynikiem jest ostatni węzeł z listy wskazanej w parametrze @code(Path).
           Jeżeli parametr @code(Path) jest pusty, to funkcja zwraca bazowy
           węzeł XML, wskazany w parametrze @code(iRoot).) }
function  GetNode({const} iRoot:  IXMLNode;
                  const   Path:  TStringDynArray)
                  : IXMLNode;                                        overload;

{: @abstract Pobiera wartość węzła.

   Funkcja @name pobiera wartość węzła. W przypadku prostych węzłów zwracana
   jest ich właściwość @code(Text). Węzeł może być również typu CDATA.

   @param iNode Węzeł XML.
   @param sDefault Wartość domyślan gdy węzeł @code(iNode) nie istnieje.

   @return(Wynikiem jest ostatni węzeł z listy wskazanej w parametrze @code(Path).
           Jeżeli parametr @code(Path) jest pusty, to funkcja zwraca bazowy
           węzeł XML, wskazany w parametrze @code(iRoot).) }
function  GetNodeStr({const} iNode:  IXMLNode;
                     const   sDefault:  string = '')
                     : string;

{: @abstract Pobiera wartość węzła.

   Funkcja @name pobiera wartość węzła. W przypadku prostych węzłów zwracana
   jest ich właściwość @code(Text). Węzeł może być również typu CDATA.

   @param iNode Węzeł XML.
   @param nDefault Wartość domyślan gdy węzeł @code(iNode) nie istnieje.

   @return(Wynikiem funkcji jest wartość węzła lub wartość domyślna jeżeli
           wskazany węzeł nie istnieje (tzn. jeżeli ma wartość @nil).)  }
function  GetNodeInt(const   iNode:  IXMLNode;
                     const   nDefault:  Integer = 0;
                     const   bHex:  Boolean = False)
                     : Integer;

{: @abstract Tworzy węzeł typu CData.

   @param(iDoc Dokument XML.)
   @param(iName Nazwa tworzonego węzła.)
   @param(sData Tekst to umieszczenia w sekcji CDATA.)

   @returns(Wynikiem funkcji jest utworzony węzeł.)

   Funkcja @name tworzy węzeł typu CData o wskazanej nazwie i wskazanej zawartości: @br @preformatted(
   <NodeName>
     <![CDATA[ Data ]]>
   </NodeName>)

   @bold(Uwaga!) Nowy węzeł @bold(nie) jest dołączony do żadnego dokumentu lub
   węzła. }
function  CreateCData(const   iDoc:  IXMLDocument;
                      const   sName:  string;
                      const   sData:  string)
                      : IXMLNode;                                    overload;

{: @abstract Tworzy węzeł typu CData.

   @param(iDoc Dokument XML.)
   @param(iParent Węzeł nadrzędny.)
   @param(iName Nazwa tworzonego węzła.)
   @param(sData Tekst to umieszczenia w sekcji CDATA.)

   @returns(Wynikiem funkcji jest utworzony węzeł.)

   Funkcja @name tworzy węzeł typu CData o wskazanej nazwie i wskazanej zawartości: @br @preformatted(
   <NodeName>
     <![CDATA[ Data ]]>
   </NodeName>)

   @bold(Uwaga!) Nowy węzeł @bold(jest) dołączony jako węzeł potomny węzła
   @code(iParent). }
function  CreateCData(const   iDoc:  IXMLDocument;
                      const   iParent:  IXMLNode;
                      const   sName:  string;
                      const   sData:  string)
                      : IXMLNode;                                    overload;

{: @abstract Usuwa wskazany węzeł XML.

   Procedura usuwa z XML wskazany węzeł.  }
procedure  Suicide(const   iNode:  IXMLNode);

{: @abstract Aktywizuje dokument XML.

   Procedura ustawia włąściwość @code(Active) dokumentu @code(XML).
   Jeżeli XML ma być czytelny dla człowieka to warto pamiętać o ustawieniu
   parametru @code(bNodeAutoIdent).  }
procedure  ActivateXML(const   XML:  TXMLDocument;
                       const   bNodeAutoIdent:  Boolean = False);


implementation


uses
  GSkPasLib.PasLib, GSkPasLib.StrUtils;


type
  PDOMString = ^DOMString;


const
  gTrueXML:  array[0..4] of  PDOMString = (
                 Addr(gcsTrueXML), Addr(gcsTrue1XML), Addr(gcsTrue2XML),
                 Addr(gcsTrue3XML), Addr(gcsTrue4XML));
  gFalseXML:  array[0..4] of  PDOMString = (
                  Addr(gcsFalseXML), Addr(gcsFalse1XML), Addr(gcsFalse2XML),
                  Addr(gcsFalse3XML), Addr(gcsFalse4XML));


procedure  Error(const   sName:  string;
                 const   nErrCode:  Integer;
                 const   sErrMsg:  string = '');                     overload;
begin
  raise  EGSkXMLError.Create(sName, nErrCode, sErrMsg)
end { Error };


procedure  Error(const   sName:  string;
                 const   nErrCode:  Integer;
                 const   sErrFmt:  string;
                 const   ErrArgs:  array of const);                  overload;
begin
  Error(sName, nErrCode, Format(sErrFmt, ErrArgs))
end { Error };


procedure  Error(const   sName:  string;
                 const   iNode:  IXMLNode;
                 const   sValue:  DOMString;
                 const   sErrMsg:  string);                          overload;
begin
  Error(sName, gcnErrXML_WrongNodeValue,
        dgettext('GSkPasLib', gcsErrXML_WrongNodeValue),
        [GetNodePath(iNode), sValue, sErrMsg])
end { Error };


procedure  Error(const   sName:  string;
                 const   iNode:  IXMLNode;
                 const   sAttr:  DOMString;
                 const   sValue:  DOMString;
                 const   sErrMsg:  string);                          overload;
begin
  Error(sName, gcnErrXML_WrongAttribValue,
        dgettext('GSkPasLib', gcsErrXML_WrongAttribValue),
        [GetNodePath(iNode), sAttr, sValue, sErrMsg])
end { Error };


function  BoolToStrXML(const   bValue:  Boolean)
                       : DOMString;
begin
  if  bValue  then
    Result := gcsTrueXML
  else
    Result := gcsFalseXML
end { BoolToStrXML };


function  StrToBoolXML(const   sValue:  DOMString)
                       : Boolean;
var
  nInd:  Integer;

begin  { StrToBoolXML }
  Result := False;
  for  nInd := High(gTrueXML)  downto  Low(gTrueXML)  do
    if  SameText(sValue, gTrueXML[nInd]^)  then
      begin
        Result := True;
        Exit
      end { for nInd };
  for  nInd := High(gFalseXML)  downto  Low(gFalseXML)  do
    if  SameText(sValue, gFalseXML[nInd]^)  then
      begin
        Result := False;
        Exit
      end { for nInd };
  try
    Result := StrToBool(sValue)
  except
    Error('StrToBoolXML',
          gcnErrXML_IncorrectBooleanValue,
          dgettext('GSkPasLib', gcsErrXML_IncorrectBooleanValue),
          [sValue]);
  end { try-except }
end { StrToBoolXML };


function  DateToStrXML(const   tmValue:  TDateTime)
                       : DOMString;
begin
  Result := FormatDateTime('yyyy-mm-dd', tmValue)
end { DateToStrXML };


function  StrToDateXML(const   sValue:  DOMString)
                       : TDateTime;
var
  nYear, nMonth, nDay:  Word;

begin  { StrToDateXML }
  Result := 0;
  try
    if  (Length(sValue) <> 10)
        or (sValue[5] <> '-')
        or (sValue[8] <> '-')  then
      Abort();
    nYear := StrToInt(Copy(sValue, 1, 4));
    nMonth := StrToInt(Copy(sValue, 6, 2));
    nDay := StrToInt(Copy(sValue, 9, 2));
    Result := EncodeDate(nYear, nMonth, nDay)
  except
    Error('StrToDateXML',
          gcnErrXML_IncorrectDateValue,
          dgettext('GSkPasLib', gcsErrXML_IncorrectDateValue),
          [sValue]);
  end { try-except }
end { StrToDateXML };


function  TimeToStrXML(const   tmValue:  TDateTime)
                       : DOMString;
begin
  Result := FormatDateTime('hh":"nn":"ss"."zzz', tmValue)
end { TimeToStrXML };


function  StrToTimeXML(const   sValue:  DOMString)
                       : TDateTime;
var
  nHour, nMin, nSec, nMSec:  Word;

begin  { StrToTimeXML }
  Result := 0;
  try
    {hh:nn:ss.ttt}
    {123456789.12}
    if  (Length(sValue) <> 12)
        or (sValue[3] <> ':')
        or (sValue[6] <> ':')
        or (sValue[9] <> '.')  then
      Abort();
    nHour := StrToInt(Copy(sValue, 1, 2));
    nMin := StrToInt(Copy(sValue, 4, 2));
    nSec := StrToInt(Copy(sValue, 7, 2));
    nMSec := StrToInt(Copy(sValue, 10, 3));
    Result := EncodeTime(nHour, nMin, nSec, nMSec)
  except
    Error('StrToTimeXML',
          gcnErrXML_IncorrectTimeValue,
          dgettext('GSkPasLib', gcsErrXML_IncorrectTimeValue),
          [sValue])
  end { try-except }
end { StrToTimeXML };


function  DateTimeToStrXML(const   tmValue:  TDateTime)
                           : DOMString;
begin
  Result := DateToStrXML(tmValue) + ', ' + TimeToStrXML(tmValue)
end { DateTimeToStrXML };


function  StrToDateTimeXML(const   sValue:  DOMString)
                           : TDateTime;
var
  nPos:  Integer;
  sBuf:  string;

begin  { StrToDateTimeXML }
  Result := 0;
  try
    nPos := Pos(',', sValue);
    sBuf := Trim(Copy(sValue, 1, Pred(nPos)));
    if  sBuf <> ''  then
      Result := StrToDateXML(sBuf)
    else
      Result := 0;
    sBuf := Trim(Copy(sValue, Succ(nPos), MaxInt));
    if  sBuf <> ''  then
      Result := Result + StrToTimeXML(sBuf)
  except
    Error('StrToDateTimeXML',
          gcnErrXML_IncorretDateTimeValue,
          dgettext('GSkPasLib', gcsErrXML_IncorretDateTimeValue),
          [sValue])
  end { try-except }
end { StrToDateTimeXML };


function  FloatToStrXML(const   nValue:  Extended)
                        : DOMString;
begin
  Result := StringReplace(Format('%g', [nValue]),
                          {$IF CompilerVersion < gcnCompilerVersionDelphiXE}
                            DecimalSeparator,
                          {$ELSE}
                            FormatSettings.DecimalSeparator,
                          {$IFEND}
                          '.', [])
end { FloatToStrXML };


function  StrToFloatXML(const   sValue:  DOMString)
                        : Extended;
begin
  Result := 0;
  try
    Result := StrToFloat(StringReplace(sValue, '.',
                                       {$IF CompilerVersion < gcnCompilerVersionDelphiXE}
                                         DecimalSeparator,
                                       {$ELSE}
                                         FormatSettings.DecimalSeparator,
                                       {$IFEND}
                                       []))
  except
    Error('StrToFloatXML',
          gcnErrXML_IncorrectFloatValue,
          dgettext('GSkPasLib', gcsErrXML_IncorrectFloatValue),
          [sValue])
  end { try-except }
end { StrToFloatXML };


function  CurrencyToStrXML(const   nValue:  Currency;
                           const   nPrecision:  Integer)
                           : DOMString;
begin
  Result := StringReplace(Format('%.*f',
                                 [nPrecision, nValue]),
                          {$IF CompilerVersion < gcnCompilerVersionDelphiXE}
                            DecimalSeparator,
                          {$ELSE}
                            FormatSettings.DecimalSeparator,
                          {$IFEND}
                          '.', [])
end { CurrencyToStrXML };


function  StrToCurrencyXML(const   sValue:  DOMString)
                           : Currency;
begin
  Result := 0;
  try
    Result := StrToCurr(StringReplace(sValue, '.',
                                      {$IF CompilerVersion < gcnCompilerVersionDelphiXE}
                                        DecimalSeparator,
                                      {$ELSE}
                                        FormatSettings.DecimalSeparator,
                                      {$IFEND}
                                      []))
  except
    Error('StrToCurrencyXML',
          gcnErrXML_IncorrectCurrencyValue,
          dgettext('GSkPasLib', gcsErrXML_IncorrectFloatValue),
          [sValue])
  end { try-except }
end { StrToCurrencyXML };


function  GetNodePath(iNode:  IXMLNode)
                      : string;
var
  cNodeSep:  string;

begin  { GetNodePath }
  if  iNode = nil  then
    begin
      Result := '*nil*';
      Exit
    end { iNode = nil };
  if  iNode.NodeType = ntElement  then
    begin
      Result := iNode.NodeName;
      cNodeSep := PathDelim
    end
  else
    begin
      Result := '';
      cNodeSep := ''
    end ;
  iNode := iNode.ParentNode;
  while  iNode <> nil  do
    begin
      if  iNode.NodeType = ntElement  then
        begin
          Result := iNode.NodeName + cNodeSep + Result;
          cNodeSep := PathDelim
        end { iNode.NodeType = ntElement };
      iNode := iNode.ParentNode
    end { while iNode <> nil }
end { GetNodePath };


function  GetAttribNode(const   iNode:  IXMLNode;
                        const   sAttribName:  DOMString;
                        const   bRequired:  Boolean)
                        : IXMLNode;
begin
  Result := iNode.AttributeNodes.FindNode(sAttribName);
  if  Result = nil  then
    if  bRequired  then
      Error('GetAttribNode',
            gcnErrXML_MissingAttrib,
            dgettext('GSkPasLib', gcsErrXML_MissingAttrib),
            [GetNodePath(iNode), sAttribName])
end { GetAttribNode };


function  GetAttribInt(const   iNode:  IXMLNode;
                       const   sAttribName:  DOMString;
                       const   bHex:  Boolean;
                       const   nDefault:  Integer)
                       : Integer;
var
  iAttrib:  IXMLNode;

begin  { GetAttribInt }
  Result := nDefault;
  iAttrib := GetAttribNode(iNode, sAttribName, False);
  if  iAttrib <> nil  then
    try
      if  bHex
          and (iAttrib.Text[1] <> '$')  then
        Result := StrToInt('$' + iAttrib.Text)
      else
        Result := StrToInt(iAttrib.Text)
    except
      on  eErr : Exception  do
        Error('GetAttribInt', iNode, sAttribName, iAttrib.Text, eErr.Message)
    end { try-except }
end { GetAttribInt };


function  GetAttribInt64(const   iNode:  IXMLNode;
                         const   sAttribName:  DOMString;
                         const   bHex:  Boolean = False;
                         const   nDefault:  Int64 = 0)
                         : Int64;
var
  iAttrib:  IXMLNode;

begin  { GetAttribInt64 }
  Result := nDefault;
  iAttrib := GetAttribNode(iNode, sAttribName, False);
  if  iAttrib <> nil  then
    try
      if  bHex
          and (iAttrib.Text[1] <> '$')  then
        Result := StrToInt64('$' + iAttrib.Text)
      else
        Result := StrToInt64(iAttrib.Text)
    except
      on  eErr : Exception  do
        Error('GetAttribInt64', iNode, sAttribName, iAttrib.Text, eErr.Message)
    end { try-except }
end { GetAttribInt64 };


function  GetAttribCardinal(const   iNode:  IXMLNode;
                            const   sAttribName:  DOMString;
                            const   bHex:  Boolean;
                            const   nDefault:  Cardinal)
                            : Cardinal;
var
  iAttrib:  IXMLNode;

begin  { GetAttribCardinal }
  Result := nDefault;
  iAttrib := GetAttribNode(iNode, sAttribName, False);
  if  iAttrib <> nil  then
    try
      if  bHex
          and (iAttrib.Text[1] <> '$')  then
        Result := StrToInt64('$' + iAttrib.Text)
      else
        Result := StrToInt64(iAttrib.Text)
    except
      on  eErr : Exception  do
        Error('GetAttribCardinal', iNode, sAttribName, iAttrib.Text, eErr.Message)
    end { try-except }
end { GetAttribCardinal };


function  GetAttribFloat(const   iNode:  IXMLNode;
                         const   sAttribName:  DOMString;
                         const   nDefault:  Double)
                         : TDateTime;
var
  iAttrib:  IXMLNode;

begin  { GetAttribCardinal }
  Result := nDefault;
  iAttrib := GetAttribNode(iNode, sAttribName, False);
  if  iAttrib <> nil  then
    try
      Result := StrToFloatXML(iAttrib.Text)
    except
      on  eErr : Exception  do
        Error('GetAttribFloat', iNode, sAttribName, iAttrib.Text, eErr.Message)
    end { try-except }
end { GetAttribFloat };


function  GetAttribStr(const   iNode:  IXMLNode;
                       const   sAttribName:  DOMstring;
                       const   sDefault:  string)
                       : string;
var
  iAttrib:  IXMLNode;

begin  { GetAttribStr }
  iAttrib := GetAttribNode(iNode, sAttribName, False);
  if  iAttrib <> nil  then
    Result := iAttrib.Text
  else
    Result := sDefault
end { GetAttribStr };


function  GetAttribChar(const   iNode:  IXMLNode;
                        const   sAttribName:  DOMString;
                        const   chDefault:  Char)
                        : Char;
var
  sBuf:  string;

begin  { GetAttribChar }
  Result := #0;
  sBuf := GetAttribStr(iNode, sAttribName, chDefault);
  if  Length(sBuf) = 1  then
    Result := sBuf[1]
  else
    Error('GetAttribChar', iNode, sAttribName, sBuf, '');
end { GetAttribChar };


function  GetAttribBool(const   iNode:  IXMLNode;
                        const   sAttribName:  DOMString;
                        const   bDefault:  Boolean)
                        : Boolean;
var
  iAttrib:  IXMLNode;

begin  { GetAttribBool }
  Result := bDefault;
  iAttrib := GetAttribNode(iNode, sAttribName, False);
  if  iAttrib <> nil  then
    try
      Result := StrToBoolXML(iAttrib.Text)
    except
      on  eErr : Exception  do
        Error('GetAttribBool', iNode, sAttribName, iAttrib.Text, eErr.Message)
    end { try-except }
end { GetAttribBool };


function  GetAttribColor(const   iNode:  IXMLNode;
                         const   sAttribName:  DOMString;
                         const   clDefault:  TColor)
                         : TColor;
var
  iAttrib:  IXMLNode;

begin  { GetAttribColor }
  Result := clDefault;
  iAttrib := GetAttribNode(iNode, sAttribName, False);
  if  iAttrib <> nil  then
    try
      Result := StringToColor(iAttrib.Text)
    except
      on  eErr : Exception  do
        Error('GetAttribColor', iNode, sAttribName, iAttrib.Text, eErr.Message)
    end { try-except }
end { GetAttribColor };


function  GetAttribTime(const   iNode:  IXMLNode;
                        const   sAttribName:  DOMString;
                        const   tmDefault:  TDateTime)
                         : TDateTime;
var
  iAttrib:  IXMLNode;

begin  { GetAttribTime }
  Result := tmDefault;
  iAttrib := GetAttribNode(iNode, sAttribName, False);
  if  iAttrib <> nil  then
    try
      Result := StrToDateTimeXML(iAttrib.Text)
    except
      on  eErr : Exception  do
        Error('GetAttribTime', iNode, sAttribName, iAttrib.Text, eErr.Message)
    end { try-except }
end { GetAttribTime };


function  GetNode(const   iXML:  IXMLDocument;
                  const   Path:  array of string)
                  : IXMLNode;
begin  { GetNode }
  Result := GetNode(iXML.DocumentElement, MakeStringDynArray(Path))
end { GetNode };


function  GetNode(const   iXML:  IXMLDocument;
                  const   Path:  TStringDynArray)
                  : IXMLNode;
begin
  Result := GetNode(iXML.DocumentElement, Path)
end { GetNode };


function  GetNode(const   iRoot:  IXMLNode;
                  const   Path:  array of string)
                  : IXMLNode;
begin
  Result := GetNode(iRoot, MakeStringDynArray(Path))
end { GetNode };


function  GetNode({const} iRoot:  IXMLNode;
                  const   Path:  TStringDynArray)
                  : IXMLNode;
var
  nInd:  Integer;

begin  { GetNode }
  Result := iRoot;
  for  nInd := Low(Path)  to  High(Path)  do
    begin
      Result := iRoot.ChildNodes.FindNode(Path[nInd]);
      if  Result = nil  then
        Error('GetNode', gcnErrXML_MissingNode,
              dgettext('GSkPasLib', gcsErrXML_MissingNode),
              [GetNodePath(iRoot) + PathDelim + Path[nInd]]);
      iRoot := Result
    end { for nInd }
end { GetNode };


function  GetNodeStr({const} iNode:  IXMLNode;
                     const   sDefault:  string)
                     : string;
begin  { GetNodeStr }
  if  iNode <> nil  then
    if  iNode.IsTextElement  then
      Result := iNode.Text
    else
      begin
        iNode := iNode.ChildNodes.First();
        while  iNode <> nil  do
          begin
            case  iNode.NodeType  of
              ntElement,
              ntCData:
                begin
                  Result := iNode.Text;
                  Exit
                end { ntElement, ntCData };
              ntComment,
              ntText:
                { OK }
              else
                begin
                  Result := sDefault;
                  Exit
                end
            end { case iNode.NodeType };
            iNode := iNode.NextSibling()
          end { while iNode <> nil };
        Result := sDefault
      end { not iNode.IsTextElement }
  else  { iNode = nil }
    Result := sDefault
end { GetNodeStr };


function  GetNodeInt(const   iNode:  IXMLNode;
                     const   nDefault:  Integer;
                     const   bHex:  Boolean)
                     : Integer;
var
  sBuf:  string;

begin  { GetNodeInt }
  Result := nDefault;
  sBuf := GetNodeStr(iNode, IntToStr(nDefault));
  if  sBuf <> ''  then
    try
      if  bHex
          and (sBuf[1] <> '$')  then
        Result := StrToInt('$' + sBuf)
      else
        Result := StrToInt(sBuf)
    except
      on  eErr : Exception  do
        Error('GetNodeInt', iNode, sBuf, eErr.Message)
    end { try-except }
end { GetNodeInt };


function  CreateCData(const   iDoc:  IXMLDocument;
                      const   sName:  string;
                      const   sData:  string)
                      : IXMLNode;
begin
  Result := iDoc.CreateNode(sName);
  Result.ChildNodes.Add(iDoc.CreateNode(sData, ntCData))
end { CreateCData };


function  CreateCData(const   iDoc:  IXMLDocument;
                      const   iParent:  IXMLNode;
                      const   sName:  string;
                      const   sData:  string)
                      : IXMLNode;
begin
  Result := CreateCData(iDoc, sName, sData);
  iParent.ChildNodes.Add(Result)
end { CreateCData };


procedure  Suicide(const   iNode:  IXMLNode);
begin
  iNode.ParentNode.ChildNodes.Delete(iNode.ParentNode.ChildNodes.IndexOf(iNode))
end { Suicide };


procedure  ActivateXML(const   XML:  TXMLDocument;
                       const   bNodeAutoIdent:  Boolean);
begin
  try
    XML.Active := True;
    if  bNodeAutoIdent  then
      XML.Options := XML.Options + [doNodeAutoIndent];
    // if  XML.Encoding = ''  then
    //   XML.Encoding := gcsEncodingWindowsXML
    { ^^^ Jeżeli nie wskażę jawnego kodowania to domyślnie automatycznie
          użyje kodowanie UTF-8. }
  except
    on  eErr : EOleSysError  do
      if  eErr.ErrorCode = REGDB_E_CLASSNOTREG  then
        begin
          Error('ActivateXML', gcnErrXML_MissingLib,
                Format(gcsErrXML_MissingLibFmt,
                       [dgettext('GSkPasLib', gcsErrXML_MissingLib1),
                        dgettext('GSkPasLib', gcsErrXML_MissingLib2)]),
                [eErr.ErrorCode, eErr.Message]);
          Exit
        end
      else
        raise
    else
      raise
  end { try-except };
end { ActivateXML };


{ EGSkXMLError }

constructor  EGSkXMLError.Create(const   sMethod:  string;
                                 const   nErrCode:  Integer;
                                 const   sErrMsg:  string);
begin
  inherited Create('EGSkXMLError.' + sMethod, nErrCode, sErrMsg)
end { Create };


end.
