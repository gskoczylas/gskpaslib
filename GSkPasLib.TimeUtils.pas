﻿{: @abstract Podprogramy związane z datami i czasem.

   Wiele funkcji matematycznych jest zdefiniowanych w standardowym module
   @code(DateUtils).

   Również wiele przydatnych funkcji można znaleźć w module @code(JclDateTime).
   Jest to jeden z modułów pakietu JEDI Code Library
   (http://homepages.borland.com/jedi/jcl). }
unit  GSkPasLib.TimeUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22}
    WinAPI.Windows, System.SysUtils, System.DateUtils;
  {$ELSE}
    Windows, SysUtils, DateUtils;
  {$IFEND}


const
  MillisPerSecond  = 1000;
  {-}
  SecondsPerMinute = 60;
  MillisPerMinute  = SecondsPerMinute * MillisPerSecond;
  {-}
  MinutesPerHour   = 60;
  SecondsPerHour   = MinutesPerHour * SecondsPerMinute;
  MillisPerHour    = SecondsPerHour * MillisPerSecond;
  {-}
  HoursPerDay      = 24;
  MinutesPerDay    = HoursPerDay * MinutesPerHour;
  SecondsPerDay    = MinutesPerDay * SecondsPerMinute;
  MillisPerDay     = SecondsPerDay * MillisPerSecond;


type
  {: @exclude}
  {$IF not Declared(TTime)}
    TTime = type TDateTime;
  {$IFEND}
  {: @abstract Czas trwania długotrwałego procesu.

     Klasa @name zawiera metody wyliczające czas trwania długotrwałego procesu.

     W chwili rozpoczęcia działania procesu należy wywołać metodę @link(Start).
     Następnie w mniej więcej stałych odstępach czasu można wywoływać pozostałe
     metody klasy @name.

     @bold(Uwaga!) Wyniki działania metod są miarodajne tylko wtedy, gdy
     poszczególne kroki trwają mniej więcej tak samo długo. Im większe wahania
     czasu trwania poszczególnych kroków, tym mniej dokładne wyniki funkcji.

     Powyższa uwaga nie dotyczy metod @link(ElapsedTime) oraz @link(ElapsedTicks).
     Wyniki działania tych metod są zawsze dokładne.

     Dokładność wyników zwiększa się w miarę wykonywania etapów procesu. }
  TGSkProgressTime = record
  strict private
    var
      fnStartTick:   UInt64;
      ftmStartTime:  TDateTime;

  public

    {: @abstract Rozpoczęcie wykonywania procesu.

       Procedura @name zapamiętuje czas rozpoczęcia wykonywania procesu. Dane
       te są wykorzystywane przez pozostałe metody klasy @className. }
    procedure  Start();                                              inline;

    {: @abstract Liczba tików, jakie upłynęły od rozpoczęcia procesu.

       @seeAlso(ElapsedTime)
       @seeAlso(Start) }
    function  ElapsedTicks() : UInt64;                               inline;

    {: @abstract Szacowana liczba tików potrzebnych do zakończenia procesu.

       @param(nCount Dotychczasowa liczba etapów procesu. @br
                     Jeżeli ten parametr ma wartość @code(0), to wynikiem
                     funkcji jest @code(0).)
       @param(nTotal Ogólna liczba etapów procesu.)

       @seeAlso(RemainingTime) }
    function  RemainingTicks(const   nCount, nTotal:  Integer)
                             : UInt64;
    {: @abstract Szacowana liczba tików potrzebnych do wykonania procesu.

       @param(nCount Dotychczasowa liczba etapów procesu. @br
                     Jeżeli ten parametr ma wartość @code(0), to wynikiem
                     funkcji jest @code(0).)
       @param(nTotal Ogólna liczba etapów procesu.)

       @returns(Wynikiem funkcji jest szacowana liczba tików potrzebna do wykonania
                całego procesu.@br
                @code(ElapsedTicks() + RemainingTicks(nCount, nTotal)))

       @seeAlso(TotalTime) }
    function  TotalTicks(const   nCount, nTotal:  Integer)
                         : UInt64;

    {: @abstract Czas od rozpoczęcia wykonywania procesu.

       Wynikiem funkcji @name jest czas, jaki upłynął od chwili rozpoczęcia
       wykonywania procesu, czyli od momentu wywołania metody @link(Start).

       @seeAlso(RemainingTime)
       @seeAlso(TotalTime)
       @seeAlso(EndTime) }
    function  ElapsedTime() : TTime;

    {: @abstract Szacowany czas do zakończenia procesu.

       @param(nCount Dotychczasowa liczba etapów procesu. @br
                     Jeżeli ten parametr ma wartość @code(0), to wynikiem
                     funkcji jest @code(00:00:00).)
       @param(nTotal Ogólna liczba etapów procesu.)

       @returns(Wynikiem funkcji jest szacowany czas potrzebny do wykonania
                pozostałych etapów procesu.@br
                @code(ElapsedTicks() + RemainingTicks(nCount, nTotal)))

       @seeAlso(ElapsedTime)
       @seeAlso(TotalTime)
       @seeAlso(EndTime) }
    function  RemainingTime(const   nCount, nTotal:  Integer)
                            : TTime;                                 inline;

    {: @abstract Szacowany czas do wykonania całego procesu.

       @param(nCount Dotychczasowa liczba etapów procesu. @br
                     Jeżeli ten parametr ma wartość @code(0), to wynikiem
                     funkcji jest @code(00:00:00).)
       @param(nTotal Ogólna liczba etapów procesu.)

       @returns(Wynikiem funkcji jest szacowany czas potrzebny do wykonania
                całego procesu (wszystkich etapów).@br
                @code(ElapsedTicks() + RemainingTicks(nCount, nTotal)))

       @seeAlso(ElapsedTime)
       @seeAlso(RemainingTime)
       @seeAlso(EndTime) }
    function  TotalTime(const   nCount, nTotal:  Integer)
                        : TTime;                                     inline;

    {: @abstract Szacowany czas zakończenia procesu.

       @param(nCount Dotychczasowa liczba etapów procesu. @br
                     Jeżeli ten parametr ma wartość @code(0), to wynikiem
                     funkcji jest @code(00:00:00).)
       @param(nTotal Ogólna liczba etapów procesu.)

       @returns(Wynikiem funkcji jest szacowany czas zakończenia wykonania procesu
                (data i godzina).)

       @seeAlso(ElapsedTime)
       @seeAlso(RemainingTime)
       @seeAlso(TotalTime) }
    function  EndTime(const   nCount, nTotal:  Integer)
                      : TDateTime;                                   inline;

    {: @abstract Tiki w chwili rozpoczęcia wykonywania procesu. }
    property  StartTick:  UInt64
              read  fnStartTick;

    {: @abstract Czas w chwili rozpoczęcia wykonywania procesu. }
    property  StartTime:  TDateTime
              read  ftmStartTime;
  end { TGSkProgressTime };


{: @abstract Oblicza liczbę tików (1/1000 sekundy) pomiędzy dwoma wartościami.

   Funkcja @name wylicza liczbę tików (1/1000 sekundy) pomiędzy dwoma wskazanymi
   wartościami. W obliczeniach zakłada, że wartość @code(aSooner) jest wcześniejsza
   niż wartość @code(aLater). Funkcja uwzględnia również fakt, że co około 49,7 dni
   licznik tików przepełnia się i zeruje.

   Funkcja @name działa „zawsze wprzód” (uwzględnia „przekręcenie” zegara).
   Natomiast funkcja @link(TickSpan) działa „w obie strony”.

   Na przykład: @longCode(#
     Delay := TickBetween(200, 300);  // 100
     Delay := TickSpan(200, 300);     // 100

     Delay := TickBetween(300, 200);  // 4294967195
     Delay := TickSpan(300, 200);     // 100
   #)

   @seeAlso(TickSpan)
   @seeAlso(TicksSince) }
function  TickBetween(const   aSooner, aLater:  LongWord)
                      : UInt64;

{: @abstract Oblicza różnicę tików (1/1000 sekundy) pomiędzy dwoma wartościami.

   Funkcja @name zwraca liczbę tików pomiędzy wskazanymi wartościami. Wynikiem
   funkcji jest zawsze różnica pomiędzy wartością większą i mniejszą.

   Funkcja @name działa „w obie strony”. Natomiast funkcja @link(TickBetween)
   działa „zawsze wprzód” (uwzględnia „przekręcienie” zegara).

   Na przykład: @longCode(#
     Delay := TickBetween(200, 300);  // 100
     Delay := TickSpan(200, 300);     // 100

     Delay := TickBetween(300, 200);  // 4294967195
     Delay := TickSpan(300, 200);     // 100
   #)

   @seeAlso(TickBetween)
   @seeAlso(TicksSince) }
function  TickSpan(const   aTicksNow, aTicksThen:  LongWord)
                   : LongWord;

{: @abstract Liczba tików od wskazanego czasu.

   Funkcja @name zwraca liczbę tików od wskazanego momentu w przeszłości. Funkcja
   zwraca taki sam wynik jak wyrażenie „@code(GetTickCount64() - nStart)”.

   @seeAlso(TickBetween)
   @seeAlso(TickSpan)
   @seeAlso(GetTickCount64) }
function  TicksSince64(const   nStart:  UInt64)
                       : UInt64;

{: @abstract Liczba tików od wskazanego czasu.

   Funkcja @name zwraca liczbę tików od wskazanego momentu w przeszłości. Funkcja
   zwraca taki sam wynik jak wywołanie funkcji @code(TickBetween(nStart, GetTickCount())).

   @seeAlso(TickBetween)
   @seeAlso(TickSpan) }
function  TicksSince(const   nStart:  LongWord)
                     : UInt64;

{: @abstract Informuje, czy upłynął wskazany czas.

   Funkcja @name informuje, czy upłynęła wskazana liczba tików od wskazanego
   momenu w przeszłości.

   @seeAlso(IsTimeout)
   @seeAlso(TicksSince64) }
function  IsTimeout64(const   nStart, nTimeout:  UInt64)
                      : Boolean;

{: @abstract Informuje, czy upłynął wskazany czas.

   Funkcja @name informuje, czy upłynęła wskazana liczba tików od wskazanego
   momenu w przeszłości.

   @seeAlso(IsTimeout64)
   @seeAlso(TicksSince) }
function  IsTimeout(const   nStart, nTimeout:  LongWord)
                    : Boolean;

{: @abstract Odlicza czas od uruchomienia komputera.

   Funkcja @name wyznacza czas, jaki upłynął od uruchomienia systemu.
   Czas jest wyznaczany z dokładnością do 1. milisekundy.

   @seeAlso(TicksToStr)  }
function  GetTickCount64() : UInt64;

{: @abstract Przekształca liczbę tików w odpowiednio sformatowany napis.

   W zależności od liczby tików, wynikiem funkcji jest napis w formacie:
    @unorderedList(@itemSpacing(Compact)
      @item(@code('H:MM:SS.TTT'), np. „@code(2:53:34.324)”)
      @item(@code('M m S.TTT s'), np. „@code(23 m 12.324 s)”)
      @item(@code('S.TTT s'), np. „@code(3.184 s)”))

   @seeAlso(GetTickCount64) }
function  TicksToStr(const   nTicks:  UInt64;
                     const   bIncludeTicks:  Boolean = True)
                     : string;

{$IF CompilerVersion = 22} // XE

{: @abstract Converts a TDateTime value to ISO8601 format.
   @param(tmDate A TDateTime value.)
   @param (bInputIsUTC If bInputIsUTC is @true, then the resulting ISO8601 string
                       will show the exact same time as tmDate has.)
   @returns(@name representation of tmDate. The resulting ISO string will be
            in UTC, i.e. will have a Z (Zulu) post fix. If bInputIsUTC = @true,
            then the time portion of tmDate will not be modified.) }
function  DateToISO8601(const   tmDate:  TDateTime;
                        const   bInputIsUTC:  Boolean = True)
                        : string;
{$IFEND}

{: @abstract Sprawdza, czy wskazany napis jest poprawną datą.
   @param (sDate Tekst reprezentujący datę.)
   Format daty musi być zgodny z ustawieniami regionalnymi. }
function  IsValidDateStr(var   sDate:  string)
                         : Boolean;


implementation


uses
  GSkPasLib.StrUtils;


var
  fnGetTickCount64:  function() : UInt64;                            stdcall;


{$REGION 'Local subroutines'}

function  OlderGetTickCount64() : UInt64;                            stdcall;

var
  nFreq, nCount:  Int64;

begin  { OlderGetTickCount64 }
  if  QueryPerformanceFrequency(nFreq)  then
    begin
      QueryPerformanceCounter(nCount);
      if  nFreq <> 0  then
        Result := (nCount div nFreq) * 1000
      else
        Result := GetTickCount()
    end { if }
  else
    Result := GetTickCount()
end { OlderGetTickCount64 };


procedure  InitializeGetTickCount64();

{$IF not Declared(WinAPI.Windows.GetTickCount64)}
var
  hKernel32:  HMODULE;
{$IFEND}

begin  { InitializeGetTickCount64 }
  Assert(not Assigned(fnGetTickCount64));
  {$IF Declared(WinAPI.Windows.GetTickCount64)}
    fnGetTickCount64 := WinAPI.Windows.GetTickCount64
  {$ELSE}
    hKernel32 := GetModuleHandle('kernel32');
    if  hKernel32 <> 0  then
      fnGetTickCount64 := GetProcAddress(hKernel32, 'GetTickCount64');
    if  not Assigned(fnGetTickCount64)  then
      fnGetTickCount64 := OlderGetTickCount64
  {$IFEND}
end { InitializeGetTickCount64 };


procedure  ProcessTicks({const} nTicks:  UInt64;
                        out   nHr, nMn, nSc, nMs:  Word);
begin
  nMs := nTicks mod 1000;
  nTicks := nTicks div 1000;   // s
  nSc := nTicks mod 60;
  nTicks := nTicks div 60;     // m
  nMn := nTicks mod 60;
  nHr := nTicks div 60         // h
end { ProcessTicks };

{$ENDREGION 'Local subroutines'}


{$REGION 'Global subroutines'}

function  TickBetween(const   aSooner, aLater:  LongWord)
                      : UInt64;
begin
  if  aSooner <= aLater  then
    Result := aLater - aSooner
  else
    Result := MAXDWORD - aSooner + aLater
end { TickBetween };


function  TickSpan(const   aTicksNow, aTicksThen:  LongWord)
                   : LongWord;
begin
  if  aTicksNow <= aTicksThen  then
    Result := aTicksThen - aTicksNow
  else
    Result := aTicksNow - aTicksThen
end { TicksSpan };


function  TicksSince64(const   nStart:  UInt64)
                     : UInt64;
begin
  Result := GetTickCount64() - nStart
end { TicksSince64 };


function  TicksSince(const   nStart:  LongWord)
                     : UInt64;
begin
  Result := TickBetween(nStart, GetTickCount())
end { TicksSince };


function  IsTimeout64(const   nStart, nTimeout:  UInt64)
                      : Boolean;
begin
  Result := TicksSince64(nStart) >= nTimeout
end { IsTimeout64 };


function  IsTimeout(const   nStart, nTimeout:  LongWord)
                    : Boolean;
begin
  Result := TicksSince(nStart) >= nTimeout
end { IsTimeout };


function  GetTickCount64() : UInt64;
begin
  Result := fnGetTickCount64()
end { GetTickCount64 };


function  TicksToStr(const   nTicks:  UInt64;
                     const   bIncludeTicks:  Boolean)
                     : string;
var
  nHr, nMn, nSc, nMs:  Word;
  sFmt:  string;

begin  { TicksToStr }
  ProcessTicks(nTicks, nHr, nMn, nSc, nMs);
  if  not bIncludeTicks
      and (nMs >= 500)  then
    begin
      Inc(nSc);
      if  (nSc = 60)  then
        begin
          Inc(nMn);
          nSc := 0;
          if  nMn = 60  then
            begin
              Inc(nHr);
              nMn := 0
            end { nMn = 60 }
        end { nSc = 60 };
    end { not bIncludeTicks };
  {-}
  if  nHr <> 0  then
    sFmt := '%0:u:%1:2.2u:%2:2.2u'
  else if  nMn <> 0  then
    sFmt := '%1:u m %2:u'
  else
    sFmt := '%2:u';
  if  bIncludeTicks  then
    sFmt := sFmt + FormatSettings.DecimalSeparator + '%3:3.3u';
  {-}
  Result := Format(sFmt, [nHr, nMn, nSc, nMs]);
  {-}
  if  bIncludeTicks  then
    begin
      Result := TrimRightChar(Result, '0');
      if  Result[Length(Result)] = FormatSettings.DecimalSeparator  then
        SetLength(Result, Length(Result) - 1);
    end { bIncludeTicks };
  if  nHr = 0  then
    Result := Result + ' s'
end { TicksToStr };


{$IF CompilerVersion = 22} // XE

{: @abstract Converts a TDateTime value to ISO8601 format.
   @param(tmDate A TDateTime value.)
   @param (bInputIsUTC If bInputIsUTC is @true, then the resulting ISO8601 string
                       will show the exact same time as tmDate has.)
   @returns @name representation of tmDate. The resulting ISO string will be
            in UTC, i.e. will have a Z (Zulu) post fix. If bInputIsUTC = @true,
            then the time portion of tmDate will not be modified.) }
function  DateToISO8601(const   tmDate:  TDateTime;
                        const   bInputIsUTC:  Boolean = True)
                        : string;
const
  csDateFormat:  string = 'yyyy''-''mm''-''dd''T''hh'':''nn'':''ss''.''zzz''Z'''; { Do not localize }
  csOffsetFormat: string = '%s%s%.02d:%.02d'; { Do not localize }
  csNeg:  array[Boolean] of string = ('+', '-');  { Do not localize }

var
  nBias:  Integer;
  tzTimeZone:  TTimeZone;

begin  { DateToISO8601 }
  Result := FormatDateTime(csDateFormat, tmDate);
  if  not bInputIsUTC then
    begin
      tzTimeZone := TTimeZone.Local;
      nBias := Trunc(tzTimeZone.GetUTCOffset(tmDate).Negate.TotalMinutes);
      if  nBias <> 0  then
        begin
          // Remove the Z, in order to add the UTC_Offset to the string.
          SetLength(Result, Length(Result) - 1);
          Result := Format(csOffsetFormat,
                           [Result,
                            csNeg[nBias > 0],
                            Abs(nBias) div MinsPerHour,
                            Abs(nBias) mod MinsPerHour]);
        end { nBias <> 0 }
    end { not bInputIsUTC }
end { DateToISO8601 };

{$IFEND}


function  IsValidDateStr(var   sDate:  string)
                         : Boolean;
var
  tmDummy:  TDateTime;

begin  { IsValidDateStr }
  Result := TryStrToDate(sDate, tmDummy)
end { IsValidDateStr };

{$ENDREGION 'Global subroutines'}


{$REGION 'TGSkProgressTime'}

function  TGSkProgressTime.ElapsedTime: TTime;

var
  nTmp:  UInt64;
  nHr, nMn, nSc, nMs:  Word;

begin  { ElapsedTime }
  nTmp := ElapsedTicks();
  nMs := nTmp mod 1000;   nTmp := nTmp div 1000;
  nSc := nTmp mod 60;     nTmp := nTmp div 60;
  nMn := nTmp mod 60;     nTmp := nTmp div 60;
  nHr := nTmp;
  Result := EncodeTime(nHr, nMn, nSc, nMs)
end { ElapsedTime };


function  TGSkProgressTime.EndTime(const   nCount, nTotal:  Integer)
                            : TDateTime;
begin
  Result := StartTime + TotalTime(nCount, nTotal)
end { EndTime };


function  TGSkProgressTime.ElapsedTicks() : UInt64;
begin
  Result := GetTickCount64() - StartTick
end { ElapsedTicks };


function  TGSkProgressTime.RemainingTicks(const   nCount, nTotal:  Integer)
                                          : UInt64;
begin
  if  nCount > 0  then
    Result := (GetTickCount64() - StartTick) * Cardinal(nTotal - nCount) div Cardinal(nCount)
  else
    Result := 0
end { RemainingTicks };


function  TGSkProgressTime.TotalTicks(const   nCount, nTotal:  Integer)
                                      : UInt64;
begin
  if  nCount > 0  then
    Result := (GetTickCount64() - StartTick) * Cardinal(nTotal) div Cardinal(nCount)
  else
    Result := 0
end { TotalTicks };


function  TGSkProgressTime.RemainingTime(const   nCount, nTotal:  Integer)
                                  : TTime;
begin
  Assert(nTotal > 0);
  Assert(nCount >= 0);
  Assert(nCount <= nTotal);
  Result := IncMilliSecond(0{EncodeTime(0, 0, 0, 0)},
                           RemainingTicks(nCount, nTotal))
end { RemainingTime };


procedure  TGSkProgressTime.Start();
begin
  fnStartTick := GetTickCount64();
  ftmStartTime := Now()
end { Start };


function  TGSkProgressTime.TotalTime(const   nCount, nTotal:  Integer)
                              : TTime;
begin
  // Result := ElapsedTime() + RemainingTime()
  Result := IncMilliSecond(0{EncodeTime(0, 0, 0, 0)},
                           TotalTicks(nCount, nTotal))
end { TotalTime };

{$ENDREGION 'TGSkProgressTime'}


initialization
  InitializeGetTickCount64();


end.

