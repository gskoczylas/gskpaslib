﻿{: @abstract(Moduł @name zawiera podprogramy związane z plikami.)

   W tym module są podprogramy obudowujące systemowe API, manipulujące nazwą lub
   atrybutami plików itp.  }
unit  GSkPasLib.FileUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$WARN SYMBOL_PLATFORM OFF}
{$IF CompilerVersion > 22} // XE
  {$LEGACYIFEND ON}
{$IFEND}


uses
  {$IF CompilerVersion > 22}
    WinAPI.Windows,
    System.Types, System.SysUtils, System.Classes, System.StrUtils, System.Math,
    System.DateUtils,
    {$IFNDEF FMX}
      Vcl.Graphics,
    {$ENDIF -FMX}
  {$else}
    Windows, Types, SysUtils, Classes, StrUtils, Math, DateUtils,
    {$IFNDEF FMX}
      Graphics,
    {$ENDIF -FMX}
  {$IFEND}
  JclFileUtils;


const
  {: @abstract(Standardowa liczba elementów numeru wersji) }
  gcnStdVersionElements = 3;
  {: @abstract(Minimalna liczba elementów numeru wersji) }
  gcnMinVersionElements = 2;

  LessThanValue    = {$IF CompilerVersion > 22}System.{$IFEND}Types.LessThanValue;
  EqualsValue      = {$IF CompilerVersion > 22}System.{$IFEND}Types.EqualsValue;
  GreaterThanValue = {$IF CompilerVersion > 22}System.{$IFEND}Types.GreaterThanValue;


type
  {: @abstract Typ definiuje relację starszeństwa pomiędzy wartościami.

     Typ @name jest wykorzystany do wskazania relacji starszeństwa między
     sprawdzaną wartością i wartością wzorcową.

     @definitionList(
       @itemSpacing(Compact)
       @itemLabel(LessThanValue)
       @item(Sprawdzana wartość jest mniejsza (starsza) niż wartość wzorcowa.)
       @itemLabel(EqualsValue)
       @item(Wartości sprawdzana i wzorcowa są jednakowe.)
       @itemLabel(GreaterThanValue)
       @item(Sprawdzana wartość jest większa (nowsza) niż wartość wzorcowa.))  }
  TValueRelationship = {$IF CompilerVersion > 22}System.{$IFEND}Types.TValueRelationship;

  {: @abstract(Ułatwia pobieranie informacji o wersji programu lub biblioteki)

  Obiekt klasy @name oprócz standardowych właściwości udostępnia również kilka
  dodatkowych informacji o wskazanym w konstruktorze pliku. }
  TGSkFileVersionInfo = class(TJclFileVersionInfo)
  private
    fsFileName:  TFileName;
    function  GetMajorFileVersion() : Word;
    function  GetMinorFileVersion() : Word;
    function  GetReleaseFileVersion() : Word;
    function  GetBuildFileVersion() : Word;
    function  GetFileDate() : TDateTime;
  public
    constructor  Create(const   sFileName:  TFileName);
    property  FileName:  TFileName
              read  fsFileName;
    property  FileDate:  TDateTime
              read  GetFileDate;
    property  FileMajorVersion:  Word
              read  GetMajorFileVersion;
    property  FileMinorVersion:  Word
              read  GetMinorFileVersion;
    property  FileRelease:  Word
              read  GetReleaseFileVersion;
    property  FileBuild:  Word
              read  GetBuildFileVersion;
  end { TGSkFileVersionInfo };


{$IFNDEF FMX}
{: @abstract(Funkcja @name skraca wskazaną ścieżkę do pliku lub foldera)

   Funkcja w pierwszej kolejności eliminuje foldery występujące w ścieżce
   zamieniając je na '...'. Ponieważ szerokość napisu zależy od użytej czcionki
   więc różne wersje tej funkcji pozwalają w różny sposób wskazać używaną
   czcionkę.

   Funkcja wymaga, aby wskazać albo maksymalną długość (parametr @code(nMaxLength)),
   albo obszar (parametr @code(Canvas)).

   @param(sFileName Jest ścieżką podlegającą ewentualnemu skróceniu.)

   @param(nMaxLength Wskazuje maksymalną długość wynikowego napisu (w pikselach).)

   @param(Canvas Wskazuje obszar, na którym będzie umieszczona skracana ścieżka.)

   @param(Font Może być użyty zamiast @code(Cavas).)

   @return(Wynikiem jest wskazana ścieżka skrócona tak, że mieści się we wskazanym
           obszarze.) }
function  MinimizePath(const   sFileName:  TFileName;
                       const   nMaxLength:  Integer;
                       const   Canvas:  TCanvas = nil)
                       : TFileName;                                  overload;

function  MinimizePath(const   sFileName:  TFileName;
                       const   Canvas:  TCanvas)
                       : TFileName;                                  overload;

function  MinimizePath(const   sFileName:  TFileName;
                       const   nMaxLength:  Integer;
                       const   Font:  TFont)
                       : TFileName;                                  overload;
{$ENDIF -FMX}


{: @abstract(Zwraca ścieżkę do aplikacji lub biblioteki)

   @return(Wynikiem funkcji jest ścieżka do pliku bieżącej aplikacji lub biblioteki.
           Dla aplikacji wynik funkcji jest taki sam jak @code(Application.ExeName)
           lub @code(ParamStr(0)).)
   @seeAlso(GetModuleDir)
   @seeAlso(GetModuleVersion) }
function  GetModuleFileName() : TFileName;

{: @abstract(Zwraca ścieżkę do aplikacji lub biblioteki)

   @return(Wynikiem funkcji jest ścieżka do pliku bieżącej aplikacji lub biblioteki.)

   @seeAlso(GetModuleFileName) }
function  GetModuleDir() : TFileName;                                inline;


{: @abstract(Zwraca numer wersji wskazanego pliku)

   Domyślnie funkcja (wywołana bez parametrów) zwraca nie więcej niż
   @link(gcnStdVersionElements) elementów numeru wersji, ale nie mniej niż
   @link(gcnMinVersionElements) elementów.

   @param(sFileName Wskazuje plik. Jeżeli jest pusty to funkcja zwraca numer
                    wersji bieżącej aplikacji lub biblioteki.)
   @param(nMaxElements Maksymalna zwracana liczba elementów wersji. Jest to
                       @italic(maksymalna) liczba elementów. Jeżeli końcowe
                       zwracane elementy są równe zero to są redukowane.)

   @return(Wynikiem funkcji jest napis składający się z nie więcej niż
           @code(nMaxElements) elementów ale nie mniej niż
           @link(gcnMinVersionElements) elementów.) }
function  GetModuleVersion(sFileName:  TFileName = '';
                           const   nMaxElements:  Integer = gcnStdVersionElements)
                           : string;                                 overload;

{: @abstract(Zwraca numer wersji wskazanego pliku)

   @param(sFileName Wskazuje plik. Jeżeli jest pusty to funkcja zwraca numer
                    wersji bieżącej aplikacji lub biblioteki.)
   @param(n… Numer wersji wskazanego pliku.) }
procedure  GetModuleVersion(sFileName:  TFileName;
                            out  nMajor, nMinor, nRelease, nBuild:  Word);  overload;

{: @abstract(Zwraca numer wersji bieżącej aplikacji lub biblioteki) }
procedure  GetModuleVersion(out  nMajor, nMinor, nRelease, nBuild:  Word);  overload; inline;

{: @abstract(Porównuje dwa napisy reprezentujące numer wersji)

   Domyślnie porównywane jest @link(gcnStdVersionElements) elementów obu wersji.

   Wynik funkcji jest typu @code(TValueRelationship). Ten typ jest zdefiniowany
   (w Delphi 7) w module @code(Types).

   @return(Wynikiem jest jedna z wartości @code(LessThanValue) gdy pierwsza wersja
           jest niższa niż druga, @code(EqualsValue) gdy obie wersje są równe lub
           @code(GreaterThanValue) gdy pierwsza wersja jest wyższa niż druga.) }
function  CompareVersion(const   sVersion1, sVersion2:  string;
                         const   nElements:  Integer = gcnStdVersionElements)
                         : TValueRelationship;

{: @abstract(Skraca numer wersji do wskazanej liczby elementów)

   Standardowo skraca wskazany numer wersji do @link(gcnStdVersionElements)
   elementów. Zerowe elementy na końcu wersji są również eliminowane.

   @return(Wynikiem funkcji jest co najmniej @link(gcnMinVersionElements)
           elementów numeru wersji.) }
function  TrimVersion(const   sVersion:  string;
                      const   nMaxElements:  Integer = gcnStdVersionElements)
                      : string;

{: @abstract(Dzieli numer wersji w postaci napisu na poszczególne elementy) }
procedure  ProcessVersion(const   sVersion:  string;
                          out  nMajor, nMinor, nRelease, nBuild:  Word);

{: @abstract(Definiuje domyślny typ pliku)

   Funkcja działa podobnie do standardowej funkcji @code(ChangeFileExt).
   Różni się od niej tym, że zmienia typ pliku tylko wtedy gdy nie został
   określony. }
function  DefaultFileExt(const   sFileName:  TFileName;
                         const   sFileExt:  string)
                         : TFileName;

{: @abstract(Zwraca pełne informacje o wersji wskazanego modułu)

   Jeżeli parametr @code(sFileName) jest pusty to zwraca informacje o wersji
   bieżącej aplikacji lub biblioteki.

   @return(Wynikiem funkcji jest obiekt @link(TGSkFileVersionInfo). Obiekt ten
           musi zostać po wykorzystaniu zwolniony poprzez wywołanie jego metody
           @code(Free) --- podobnie jak każdy inny obiekt.) }
function  GetModuleVersionInfo(sFileName:  TFileName = '')
                               : TGSkFileVersionInfo;

{: @abstract(Zwraca @italic(InternalName) wskazanego pliku)

   Jeżeli parametr @code(sFileName) jest pustym napisem, to zwraca
   @italic(InternalName) bieżącej aplikacji lub biblioteki. }
function  GetModuleInternalName(sFileName:  TFileName = '')
                                : string;

{: @abstract(Sprawdza czy plik jest w użyciu)

   Wynikiem funkcji jest @true gdy plik jest otwarty przez jakąkolwiek aplikację
   (lub bibliotekę). }
function  FileInUse(const   sFileName:  TFileName)
                    : Boolean;


{: @abstract(Zwraca rozmiar wskazanego pliku.)

   Funkcja zwraca rozmiar wskazanego pliku (w bajtach). Jeżeli wskazany plik nie
   istnieje, to wynikiem funkcji jest @code(-1). }
function  GetFileSize(const   sFilePath:  TFileName)
                      : Int64;


{: @abstract(Przekształca napis na poprawną nazwę pliku.)

   Funkcja @name przekształca napis na poprawną nazwę pliku.

   @param(sFileName Kandydat na nazwę pliku.

          @bold(Uwaga!) To @bold(nie) jest pełana ścieżka do pliku, tylko
          potencjalna nazwa pliku.)

   @param(bReplaceSpaces Jeżęli @true, to znaki spacji zamienia na podkreślenia.)

   @return(Wynikiem funkcji jest napis przekształcony następująco:
           @unorderedList(
             @item(Znaki „<”, „>”, „/”, „\”, „:”, „|”, „?” lub „*”
                   są zamieniane na „-”)
             @item(Znaki sterujące są zamieniane na „.”)
             @item(Cudzysłów jest zamieniany na apostrof)
             @item(Spacje są zamieniane na podkreślenia, jeżeli parametr
                   @code(bReplaceSpaces) jest @true.))) }
function  GetFixedFilename(const   sFilename:  string;
                           const   bReplaceSpaces:  Boolean = True)
                           : TFileName;


implementation


uses
  GSkPasLib.SystemUtils;


{$REGION 'Internal functions'}

procedure  SplitVersion(sVersion:  string;
                        const  Elements:  TStringList);
var
  nInd:  Integer;

begin  { SplitVersion }
  Elements.BeginUpdate();
  try
    Elements.Clear();
    while  sVersion <> ''  do
      begin
        nInd := Pos('.', sVersion);
        if  nInd = 0  then
          nInd := Succ(Length(sVersion));
        Elements.Add(LeftStr(sVersion, Pred(nInd)));
        Delete(sVersion, 1, nInd)
      end { while sVersion <> '' }
  finally
    Elements.EndUpdate()
  end { try-finally }
end { SplitVersion };


function  InternalFileTimeToDateTime(FileTime:  TFileTime)
                                     : TDateTime;
var
  lFileTime:  TFileTime;
  lSysTime:   TSystemTime;

begin  { InternalFileTimeToDateTime }
  GSkPasLib.SystemUtils.CheckOSError(FileTimeToLocalFileTime(FileTime, lFileTime));
  GSkPasLib.SystemUtils.CheckOSError(FileTimeToSystemTime(lFileTime, lSysTime));
  with  lSysTime  do
    Result := EncodeDateTime(wYear, wMonth, wDay,
                             wHour, wMinute, wSecond, wMilliseconds)
end { InternalFileTimeToDateTime };

{$ENDREGION 'Internal functions'}


{$REGION 'Public functions'}


{$IFNDEF FMX}

function   MinimizePath(const   sFileName:  TFileName;
                        const   nMaxLength:  Integer;
                        const   Canvas:  TCanvas)
                        : TFileName;
var
  sDrive:  TFileName;
  sDir:  TFileName;
  sName:  TFileName;
  bRoot:  Boolean;
  nInd, nMaxLen:  Integer;

  function  Len(const   sText:  string)
                : Integer;
  begin
    if  Canvas <> nil  then
      Result := Canvas.TextWidth(sText)
    else
      Result := Length(sText)
  end { Len };

begin  { MinimizePath }
  if  (nMaxLength <= 0)
      and (Canvas = nil)  then
    raise  Exception.Create('MinimizePath: parametr „MaxLength” powinien być dodatni lub parametr „Canvas” nie może być pusty');
  Result := sFileName;
  sDir := ExtractFilePath(Result);
  sName := ExtractFileName(Result);
  if  nMaxLength > 0  then
    nMaxLen := nMaxLength
  else
    with  Canvas.ClipRect  do
      nMaxLen := Right - Left;
  if  (Length(sDir) >= 2)
      and (sDir[2] = ':')  then
    begin
      sDrive := Copy(sDir, 1, 2);
      Delete(sDir, 1, 2);
    end
  else
    sDrive := '';
  while  ((sDir <> '')
           or (sDrive <> ''))
         and (Len(Result) > nMaxLen)  do
    begin
      if  sDir = '\...\'  then
        begin
          sDrive := '';
          sDir := '...\'
        end
      else
        if  sDir = ''  then
          sDrive := ''
        else
          begin
            if  sDir = '\' then
              sDir := ''
            else
              begin
                if  sDir[1] = '\'  then
                  begin
                    bRoot := True;
                    Delete(sDir, 1, 1)
                  end
                else
                  bRoot := False;
                if  sDir[1] = '.'  then
                  Delete(sDir, 1, 4);
                {$IFDEF UNICODE}
                  nInd := Pos('\', sDir);
                {$ELSE}
                  nInd := AnsiPos('\', sDir);
                {$ENDIF -UNICODE}
                if  nInd <> 0  then
                  begin
                    Delete(sDir, 1, nInd);
                    sDir := '...\' + sDir
                  end
                else
                  sDir := '';
                if  bRoot  then
                  sDir := '\' + sDir
              end { sDir <> '\' }
          end { sDir <> '' };
      Result := sDrive + sDir + sName;
    end { while }
end { MinimizePath };


function  MinimizePath(const   sFileName:  TFileName;
                       const   Canvas:  TCanvas)
                       : TFileName;
begin
  Result := MinimizePath(sFileName, 0, Canvas)
end { MinimizePath };


function  MinimizePath(const   sFileName:  TFileName;
                       const   nMaxLength:  Integer;
                       const   Font:  TFont)
                       : TFileName;
var
  lBitmap:  TBitmap;

begin  { MinimizePath }
  if  nMaxLength <= 0  then
    raise  Exception.Create('MinimizePath: parametr „nMaxLength” musi być większy od zera');
  lBitmap := TBitmap.Create();
  try
    lBitmap.Canvas.Font.Assign(Font);
    Result := MinimizePath(sFileName, nMaxLength, lBitmap.Canvas)
  finally
    lBitmap.Free()
  end { try-finally }
end { MinimizePath };

{$ENDIF -FMX}


function  GetModuleFileName() : TFileName;

var
  pszPath:  PChar;

begin  { GetModuleFileName }
  if  IsLibrary  then
    begin
      pszPath := StrAlloc(MAX_PATH * SizeOf(Char));
      try
        ZeroMemory(pszPath, MAX_PATH * SizeOf(Char));
        if  {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.GetModuleFileName(hInstance, pszPath, MAX_PATH) = 0  then
          RaiseLastOSError();
        Result := StrPas(pszPath)
      finally
        StrDispose(pszPath)
      end { try-finally }
    end { IsLibrary }
  else
    Result := ParamStr(0);   // Application.ExeName
  Result := PathGetLongName(Result)
end { GetModuleFileName };


function  GetModuleDir() : TFileName;
begin
  Result := ExtractFileDir(GetModuleFileName())
end { GetModuleDir };


function  GetModuleVersion(sFileName:  TFileName;
                           const   nMaxElements:  Integer)
                           : string;
begin
  Result := '';
  if  sFileName = ''  then
    sFileName := GetModuleFileName();
  with  TJclFileVersionInfo.Create(sFileName)  do
    try
      Result := TrimVersion(FileVersion, nMaxElements)
    finally
      Free()
    end { try-finally }
end { GetModuleVersion };


procedure  GetModuleVersion(sFileName:  TFileName;
                            out  nMajor, nMinor, nRelease, nBuild:  Word);  overload;
begin
  if  sFileName = ''  then
    sFileName := GetModuleFileName();
  with  TGSkFileVersionInfo.Create(sFileName)  do
    try
      nMajor := FileMajorVersion;
      nMinor := FileMinorVersion;
      nRelease := FileRelease;
      nBuild := FileBuild
    finally
      Free()
    end { try-finally }
end { GetModuleVersion };


procedure  GetModuleVersion(out  nMajor, nMinor, nRelease, nBuild:  Word);
begin
  GetModuleVersion('', nMajor, nMinor, nRelease, nBuild)
end { GetModuleVersion };


function  CompareVersion(const   sVersion1, sVersion2:  string;
                         const   nElements:  Integer)
                         : TValueRelationship;
var
  lVer1, lVer2:  TStringList;
  nVer1, nVer2:  Integer;
  nInd:  Integer;

begin  { CompareVersion }
  Result := EqualsValue;
  if  sVersion1 = sVersion2  then
    Exit;
  lVer1 := TStringList.Create();
  try
    lVer2 := TStringList.Create();
    try
      SplitVersion(TrimVersion(sVersion1, nElements),
                   lVer1);
      SplitVersion(TrimVersion(sVersion2, nElements),
                   lVer2);
      for  nInd := 0  to  Pred(MinIntValue([lVer1.Count,
                                            lVer2.Count,
                                            nElements]))  do
        begin
          try
            nVer1 := StrToInt(lVer1[nInd]);
            nVer2 := StrToInt(lVer2[nInd]);
            Result := CompareValue(nVer1, nVer2)
          except
            Result := EnsureRange(CompareStr(lVer1[nInd],
                                             lVer2[nInd]),
                                  LessThanValue, GreaterThanValue)
          end { try-except };
          if  Result <> EqualsValue  then
            Exit
        end { for nInd };
      Assert(Result = EqualsValue);
      Result := CompareValue(lVer1.Count, lVer2.Count)
    finally
      lVer2.Free()
    end { try-finally }
  finally
    lVer1.Free()
  end { try-finally }
end { CompareVersion };


function  TrimVersion(const   sVersion:  string;
                      const   nMaxElements:  Integer)
                      : string;
const
  cnMinElements = 2;

var
  lVer:  TStringList;
  nInd:  Integer;

begin  { TrimVersion }
  Result := '';
  lVer := TStringList.Create();
  try
    SplitVersion(sVersion, lVer);
    while  (lVer.Count > nMaxElements)
           and (lVer.Count > cnMinElements)  do
      lVer.Delete(Pred(lVer.Count));
    nInd := lVer.Count - 1;
    while  (lVer.Count > cnMinElements)
           and ((lVer[nInd] = '')
                or (lVer[nInd] = '0'))  do
      begin
        lVer.Delete(nInd);
        Dec(nInd)
      end { while };
    for  nInd := 0  to  lVer.Count - 1  do
      Result := Result + lVer[nInd] + '.';
    SetLength(Result, Length(Result) - 1);
  finally
    lVer.Free()
  end { try-finally }
end { TrimVersion };


procedure  ProcessVersion(const   sVersion:  string;
                          out  nMajor, nMinor, nRelease, nBuild:  Word);
var
  lElements:  TStringList;

begin  { ProcessVersion }
  lElements := TStringList.Create();
  try
    SplitVersion(sVersion, lElements);
    while  lElements.Count < 4  do
      lElements.Add('0');
    nMajor := StrToIntDef(lElements.Strings[0], 0);
    nMinor := StrToIntDef(lElements.Strings[1], 0);
    nRelease := StrToIntDef(lElements.Strings[2], 0);
    nBuild := StrToIntDef(lElements.Strings[3], 0)
  finally
    lElements.Free()
  end { try-finally }
end { ProcessVersion };


function  DefaultFileExt(const   sFileName:  TFileName;
                         const   sFileExt:  string)
                         : TFileName;
begin
  if  ExtractFileExt(sFileName) = ''  then
    Result := ChangeFileExt(sFileName, sFileExt)
  else
    Result := sFileName
end { DefaultFileExt };


function  GetModuleVersionInfo(sFileName:  TFileName)
                               : TGSkFileVersionInfo;
begin
  if  sFileName = ''  then
    sFileName := GetModuleFileName();
  Result := TGSkFileVersionInfo.Create(sFileName)
end { GetModuleVersionInfo };


function  GetModuleInternalName(sFileName:  TFileName)
                                : string;
begin
  if  sFileName = ''  then
    sFileName := GetModuleFileName();
  with  TGSkFileVersionInfo.Create(sFileName)  do
    try
      Result := InternalName
    finally
      Free()
    end { try-finally }
end { GetModuleInternalName };


function  FileInUse(const   sFileName:  TFileName)
                    : Boolean;
var
  hTemp:  HFILE;

begin  { FileInUse }
  Result := False;
  if  not FileExists(sFileName)  then
    Exit;
  hTemp := CreateFile(PChar(sFileName),
                      GENERIC_READ or GENERIC_WRITE,
                      0, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  Result := hTemp = INVALID_HANDLE_VALUE;
  if  not Result  then
    CloseHandle(hTemp)
end { FileInUse };


function  GetFileSize(const   sFilePath:  TFileName)
                      : Int64;
var
  lData:  TSearchRec;

begin  { GetFileSize }
  Result := -1;
  if  FindFirst(sFilePath, MaxInt, lData) = 0  then
    try
      Result := lData.Size
    finally
      FindClose(lData)
    end { try-finally }
end { GetFileSize };


function  GetFixedFilename(const   sFilename:  string;
                           const   bReplaceSpaces:  Boolean)
                           : TFileName;
var
  nInd:  Integer;

begin  { GetFixedFilename }
  Result := sFilename;
  for  nInd :=  1  to  Length(Result)  do
    case  Result[nInd]  of
      ' ':
        if  bReplaceSpaces  then
          Result[nInd] := '_';
      '<', '>', '/', '\', ':', '|', '?', '*':
        Result[nInd] := '-';
      #0..Pred(' '), #127:
        Result[nInd] := '.';
      '"':
        Result[nInd] := '''';
    end { case Result[nInd] }
end { GetFixedFilename };

{$ENDREGION 'Public functions'}


{$REGION 'TGSkFileVersionInfo'}

constructor  TGSkFileVersionInfo.Create(const   sFileName:  TFileName);
begin
  inherited Create(sFileName);
  fsFileName := sFileName
end { Create };


function  TGSkFileVersionInfo.GetBuildFileVersion() : Word;
begin
  Result := LoWord(FixedInfo.dwFileVersionLS)
end { GetBuildFileVersion };


function  TGSkFileVersionInfo.GetFileDate() : TDateTime;
begin
  {$IFDEF D7 }
    if  FileExists(fsFileName)  then
      Result := FileDateToDateTime(FileAge(fsFileName))
    else
      Result := 0.0
  {$else}
    if  not FileAge(fsFileName, Result)  then
      Result := 0.0
  {$ENDIF -D7 }
end { GetFileDate };


function  TGSkFileVersionInfo.GetMajorFileVersion() : Word;
begin
  Result := HiWord(FixedInfo.dwFileVersionMS)
end { GetMajorFileVersion };


function  TGSkFileVersionInfo.GetMinorFileVersion() : Word;
begin
  Result := LoWord(FixedInfo.dwFileVersionMS)
end { GetMinorFileVersion };


function  TGSkFileVersionInfo.GetReleaseFileVersion() : Word;
begin
  Result := HiWord(FixedInfo.dwFileVersionLS)
end { GetReleaseFileVersion };

{$ENDREGION 'TGSkFileVersionInfo'}


end.

