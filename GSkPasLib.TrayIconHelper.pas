﻿unit  GSkPasLib.TrayIconHelper;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


uses
  System.SysUtils,
  VCL.ExtCtrls;


type
  TBalloonFlag = TBalloonFlags;

  TTrayIconHlp = class helper for  TCustomTrayIcon
  public
    procedure  ShowBalloon(const   sMessage:  string;
                           const   sTitle:  string = '';
                           const   Flag:  TBalloonFlag = bfNone;
                           const   nTimeout:  Integer = 10000);      overload;
    procedure  ShowBalloon(const   sMessageFmt:  string;
                           const   MessageArgs:  array of const;
                           const   sTitle:  string = '';
                           const   Flag:  TBalloonFlag = bfNone;
                           const   nTimeout:  Integer = 10000);      overload;
    procedure  ShowBalloon(const   sMessage:  string;
                           const   Flag:  TBalloonFlag;
                           const   nTimeout:  Integer = 10000);      overload; inline;
    procedure  ShowBalloon(const   sMessageFmt:  string;
                           const   MessageArgs:  array of const;
                           const   Flag:  TBalloonFlag;
                           const   nTimeout:  Integer = 10000);      overload;

  end { TTrayIconHlp };


implementation


{$REGION 'TTrayIconHlp'}

procedure  TTrayIconHlp.ShowBalloon(const   sMessage:  string;
                                    const   Flag:  TBalloonFlag;
                                    const   nTimeout:  Integer);
begin
  ShowBalloon(sMessage, '', Flag, nTimeout)
end { ShowBalloon };


procedure  TTrayIconHlp.ShowBalloon(const   sMessage, sTitle:  string;
                                    const   Flag:  TBalloonFlag;
                                    const   nTimeout:  Integer);
begin
  BalloonHint := sMessage;
  BalloonTitle := sTitle;
  BalloonFlags := Flag;
  BalloonTimeout := nTimeout;
  ShowBalloonHint()
end { ShowBalloon };


procedure  TTrayIconHlp.ShowBalloon(const   sMessageFmt:  string;
                                    const   MessageArgs:  array of const;
                                    const   Flag:  TBalloonFlag;
                                    const   nTimeout:  Integer);
begin
  ShowBalloon(Format(sMessageFmt, MessageArgs), '', Flag, nTimeout)
end { ShowBalloon };


procedure  TTrayIconHlp.ShowBalloon(const   sMessageFmt:  string;
                                    const   MessageArgs:  array of const;
                                    const   sTitle:  string;
                                    const   Flag:  TBalloonFlag;
                                    const   nTimeout:  Integer);
begin
  ShowBalloon(Format(sMessageFmt, MessageArgs), sTitle, Flag, nTimeout)
end { ShowBalloon };

{$ENDREGION 'TTrayIconHlp'}


end.
