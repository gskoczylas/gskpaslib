﻿{: @abstract Definiuje komponent do przechowywania dowolnych danych.

   Standardowo można do dowolnego programu dołączyć dowolne dane poprzez zasoby.
   Wymaga to użycia kompilatora zasobów (oraz znajomości składni pliku zasobów.

   Zdefiniowany w tym module komponent @link(TGSkDataEmbedded) ułatwia dołączanie
   do programu zawartości dowolnego pliku.  }
unit  GSkPasLib.DataEmbedded;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Classes, SysUtils, GSkPasLib.CustomComponents;


type
{: @abstract Komponent do przechowywania dowolnych danych.

   Standardowo można do dowolnego programu dołączyć dowolne dane poprzez zasoby.
   Wymaga to użycia kompilatora zasobów (oraz znajomości składni pliku zasobów.

   Komponent @name ułatwia dołączanie do programu zawartości dowolnego pliku.
}
  TGSkDataEmbedded = class(TGSkCustomComponent)
  private
    FData:  TMemoryStream;
    function  GetSize() : Int64;
    function  GetStream() : TStream;
    procedure  WriteEmbeddedData(Stream:  TStream);
    procedure  ReadEmbeddedData(Stream:  TStream);
    procedure  SetStream(const   Value:  TStream);
    procedure  SetSize(const   nValue:  Int64);
  protected
    {: @exclude
       Standardowe metody nie opisuję. }
    procedure  DefineProperties(Filer:  TFiler);                     override;
  public
    {: @exclude }
    constructor  Create(AOwner:  TComponent);                        override;
    {: @exclude }
    destructor  Destroy();                                           override;

    {: @abstract Zapisuje przechowywane dane do wskazanego pliku.

       @seeAlso SaveToStream
    }
    procedure  SaveToFile(const   sFileName:  TFileName);

    {: @abstract Zapisuje przechowywane dane do wskazanego strumienia.

       @seeAlso SaveToFile
    }
    procedure  SaveToStream(const   Stream:  TStream);

  published
    {: @abstract Rozmiar przechowywanych danych (w bajtach). }
    property  Size:  Int64
              read  GetSize
              write SetSize
              default  0;

    {: @abstract Przechowywane dane. }
    property  Data:  TStream
              read  GetStream
              write SetStream;
  end { TGSkDataEmbedded };


implementation


{ TGSkDataEmbedded }

constructor  TGSkDataEmbedded.Create(AOwner:  TComponent);
begin
  inherited;
  FData := TMemoryStream.Create()
end { Create };



procedure  TGSkDataEmbedded.DefineProperties(Filer:  TFiler);
begin
  inherited;
  Filer.DefineBinaryProperty('EmbeddedData',
                             ReadEmbeddedData, WriteEmbeddedData,
                             FData.Size > 0);
end { DefineProperties };


destructor  TGSkDataEmbedded.Destroy();
begin
  FreeAndNil(FData);
  inherited
end { Destroy };


function  TGSkDataEmbedded.GetSize() : Int64;
begin
  Result := FData.Size
end { GetSize };


function  TGSkDataEmbedded.GetStream() : TStream;
begin
  Result := FData
end { GetStream };


procedure  TGSkDataEmbedded.ReadEmbeddedData(Stream:  TStream);

var
  nSize:  Int64;

begin  { ReadEmbeddedData }
  Stream.Read(nSize, SizeOf(nSize));
  FData.Clear();
  FData.CopyFrom(Stream, nSize);
  FData.Seek(0, soBeginning)
end { ReadEmbeddedData };


procedure  TGSkDataEmbedded.SaveToFile(const   sFileName:  TFileName);

var
  nPos:  Int64;

begin  { SaveToFile }
  nPos := FData.Position;             // save position
  FData.Seek(0, soBeginning);
  FData.SaveToFile(sFileName);
  FData.Seek(nPos, soBeginning)   // restore position
end { SaveToFile };


procedure  TGSkDataEmbedded.SaveToStream(const   Stream:  TStream);

var
  nPos:  Int64;

begin  { SaveToStream }
  nPos := FData.Position;
  FData.Seek(0, soBeginning);
  FData.SaveToStream(Stream);
  FData.Seek(nPos, soBeginning)
end { SaveToStream };


procedure  TGSkDataEmbedded.SetSize(const   nValue:  Int64);
begin
  FData.SetSize(nValue)
end { SetSize };


procedure  TGSkDataEmbedded.SetStream(const   Value:  TStream);
begin
  FData.Clear();
  if  Value <> nil  then
    FData.CopyFrom(Value, Value.Size - Value.Position)
end { SetStream };


procedure  TGSkDataEmbedded.WriteEmbeddedData(Stream:  TStream);

var
  nSize:  Int64;

begin  { WriteEmbeddedData }
  nSize := FData.Size;
  Stream.Write(nSize, SizeOf(nSize));
  FData.Seek(0, soFromBeginning);
  Stream.CopyFrom(FData, nSize)
end { WriteEmbeddedData };


end.
