﻿{: @abstract(Moduł zawiera bazowe definicje komponentów i dialogów tej biblioteki)

   Wszystkie komponenty i dialogi tej biblioteki są potomkami bazowych komponentów
   zdefiniowanych w module @name.  }
unit  GSkPasLib.CustomComponents;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$Warn SYMBOL_PLATFORM off }


uses
  {$IF CompilerVersion > 22}
    System.Classes, System.SysUtils, System.StrUtils,
    {$IFDEF FMX}
      FMX.Controls, FMX.Forms;
    {$ELSE}
      Vcl.Controls, Vcl.Forms;
    {$ENDIF -FMX}
  {$ELSE}
    Classes, Controls, Forms, SysUtils, StrUtils;
  {$IFEND}


type
  {: @abstract(Właściwość wykorzystywana tylko w trybie projektowania do
               wywołania opisu komponentów biblioteki) }
  TGSkPasLibAboutProperty = type Integer;

  {: @abstract(Typ reprezentujący bezparametrową proceduralną metodę obiektów) }
  TMethod = procedure()  of object;

  {: @abstract(Bazowy komponent tej biblioteki)

     Wszystkie komponenty w bibliotece są potomkami tego komponentu. Komponent
     zawiera definicję właściwości @link(About) oraz kilka metod. }
  TGSkCustomComponent = class(TComponent)
  private
    FAbout:  TGSkPasLibAboutProperty;
  protected

    {: @abstract Znajduje okno, w którym jest wskazany komponent.

       Funkcja @name szuka okna, w którym znajduje się wskazany komponent.
       Szukanie rozpoczyna się od wskazanego komponentu. Analizowane są
       właściwości @code(Parent) (jeżeli nie ma to @code(Owner)) kolejnych
       nadrzędnych elementów aż zostanie znalezione odpowiednie okno.

       @param(AComponent Szukane jest okno zawierający ten komponent.)
       @param(bFloating Ten parametr ma znaczenie wtedy gdy komponent
                        @code(AComponent) jest w oknie zadokowanym w innym oknie.
                        Jeżeli parametr ma wartość @false (wartość domyślna) to
                        szukanie trwa tak długo aż zostanie znalezione okno
                        niezadokowane.)

       @returns(Znalezione okno lub @nil.)  }
    class function  FindComponentForm(AComponent:  TComponent {$IFNDEF FMX};
                                      const   bFloating:  Boolean = False{$ENDIF})
                                      : TForm;

    {: @abstract Sprawdza czy właściwość jest zdefiniowana.

       Procedura @name sprawdza czy wskazana właściwość jest zdefiniowana.

       @param(Prop Sprawdzana właściwość.)
       @param(sPropName Nazwa sprawdzanej właściwości.) }
    procedure  CheckObjProp(Prop:  TPersistent;
                            const   sPropName:  string);             overload;

  public
    constructor  Create(Owner:  TComponent);                         override;
    destructor  Destroy();                                           override;

    {: @abstract(Zwraca nazwę klasy)

       Funkcja @name dla wskazanej instancji obiektu zwraca jego nazwę. Zazwyczaj
       jest wykorzystywana do raportowania błędów.

       @seeAlso(ComponentDisplayName)  }
    class function  ClassDisplayName(const   Instance:  TObject)
                                     : string;                       overload;

    {: @abstract(Zwraca nazwę klasy)

       Funkcje @name dla wskazanej klasy obiektu zwraca jego nazwę. Zazwyczaj
       jest wykorzystywana do raportowania błędów.

       @seeAlso(ComponentDisplayName)  }
    class function  ClassDisplayName(const   AClass:  TClass)
                                     : string;                       overload;

    {: @abstract(Zwraca nazwę klasy)

       Funkcje @name dla wskazanego komponentu zwracają jego nazwę. Zazwyczaj
       jest wykorzystywana do raportowania błędów.

       @param(Component Analizowany komponent.)
       @param(bWithPath Jeżeli ten parametr ma wartość @true to wynikowy napis
                        jest poprzedzany nazwami wszystkich komponentów nadrzędnych.)
       @param(bWithRoot Jeżeli parametr @code(bWithPath) ma wartość @true, a
                        parametr @code(bWithRoot) ma wartość @false to wynikowy
                        napis jest poprzedzany nazwami wszystkich komponentów
                        nadrzędnych z wyjątkiem komponentu--korzenia.

                        Jeżeli parametr @code(bWithPath) ma wartość @false to
                        parametr @code(bWithRoot) jest ignorowany.)

       @seeAlso(ClassDisplayName)  }
    class function  ComponentDisplayName(Component:  TComponent;
                                         const   bWithPath:  Boolean = False;
                                         const   bWithRoot:  Boolean = True)
                                         : string;

    {: @abstract Sprawdza czy właściwość jest zdefiniowana.

       Procedura @name sprawdza czy wskazana właściwość jest zdefiniowana.

       @param(Owner Obiekt którego dotyczy sprawdzana właściwość.)
       @param(Prop Sprawdzana właściwość.)
       @param(sPropName Nazwa sprawdzanej właściwości.) }
    class procedure  CheckObjProp(const   Owner:  TComponent;
                                  const   Prop:  TPersistent;
                                  const   sPropName:  string);       overload;

    {: @abstract(Wywołuje metodę abstrakcyjną.)

       Metoda służy do wywoływania metody zdefiniowanej jako @bold(abstract).
       Jeżeli taka metoda nie jest zdefiniowana w potomku obiektu lub w jej
       metodzie występuje @bold(inherited) (nieprawidłowo!) to zamiast niewiele
       mówiącego komunikatu błędu @italic(Abstract error) wyświetlany jest
       raport precyzujący rodzaj błędu. }
    class procedure  CallAbstractMethod(const   fnMethod:  TMethod;
                                        const   Instance:  TObject;
                                        const   sMethodName:  string);
  published
    {: @abstract(Informuje o bibliotece)

       Jedynym przeznaczeniem tej właściwości jest wyświetlenie @italic(w trybie
       projektowania) informacji o bibliotece. }
    property  About:  TGSkPasLibAboutProperty
              read  FAbout
              write FAbout
                            stored  False;
  end { TGSkCustomComponent };

  {: @abstract(Klasa bazowa dialogów zdefiniowanych w bibliotece)

     W praktyce zazwyczaj wykorzystywany jest jedna z klas @link(TGSkCustomDialogFB),
     @link(TGSkCustomDialogFI) lub @link(TGSkCustomDialogP). }
  TGSkCustomDialog = class(TGSkCustomComponent);

  {: @abstract(Klasa bazowa części dialogów zdefiniowanych w bibliotece)

     W dialogach tego typu metoda @italic(Execute) jest funkcją zwracającą
     w wyniku wywołania wartość typu @code(Boolean). }

  TGSkCustomDialogFB = class(TGSkCustomDialog)
  public
    constructor  Create(Owner:  TComponent);                         override;
    destructor  Destroy();                                           override;
    {: Metoda @name zwracająca w wyniku wartość logiczną. }
    function  Execute() : Boolean;                         virtual;  abstract;
  end { TGSkCustomDialogFB };

  {: @abstract(Klasa bazowa części dialogów zdefiniowanych w bibliotece)

     W dialogach tego typu metoda @italic(Execute) jest funkcją zwracającą
     w wyniku wywołania wartość typu @code(Integer). }
  TGSkCustomDialogFI = class(TGSkCustomDialog)
  public
    constructor  Create(Owner:  TComponent);                         override;
    destructor  Destroy();                                           override;
    {: Metoda @name zwracająca w wyniku liczbę całkowitą. }
    function  Execute() : Integer;                         virtual;  abstract;
  end { TGSkCustomDialogFI};

  {: @abstract(Klasa bazowa części dialogów zdefiniowanych w bibliotece)

     W dialogach tego typu metoda @italic(Execute) jest procedurą. }
  TGSkCustomDialogP = class(TGSkCustomDialog)
  public
    constructor  Create(Owner:  TComponent);                         override;
    destructor  Destroy();                                           override;
    {: Metoda @name będąca procedurą bez parametrów. }
    procedure  Execute();                                  virtual;  abstract;
  end { TGSkCustomDialogP};


{: @abstract(Generuje wyjątek związany z metodą typu @code(abstract))

   @param(Instance wskazuje na błędny obiekt)
   @param(sMethodName wskazuje nazwę metody --- w komunikacie błędu)

   Procedura generuje wyjątek typu @code(EAbstractException) ale z opisem
   informującym jaka metoda nie jest zakodowana lub zawiera @bold(inherited)
   w swojej treści.

   Ta procedura jest wywoływana między innymi przez metodę
   @link(TGSkCustomComponent.CallAbstractMethod). Jest ona dostępna publicznie
   bo metoda @code(CallAbstractMethod) przewidziana jest dla metod będących
   procedurami bez parametrów. W innych komponentach może istnieć potrzeba
   zakodowania procedury podobnej do @code(CallAbstractMetod) ale dla metod
   innego typu. }
procedure  RaiseAbstractError(const   Instance:  TObject;
                              const   sMethodName:  string);


implementation


uses
  GSkPasLib.PasLibInternals;


const
  gcsAbstractError = 'Obiekt klasy "%0:s": brak definicji metody "%1:s" lub w tej metodzie występuje *inherited*';


procedure  RaiseAbstractError(const   Instance:  TObject;
                              const   sMethodName:  string);
begin
  raise  EAbstractError.CreateFmt(gcsAbstractError,
                                  [TGSkCustomComponent.ClassDisplayName(Instance),
                                   sMethodName])
end { RaiseAbstractError };


{ TGSkCustomComponent }

class procedure  TGSkCustomComponent.CallAbstractMethod(const   fnMethod:  TMethod;
                                                        const   Instance:  TObject;
                                                        const   sMethodName:  string);
begin
  try
    fnMethod()
  except
    on  EAbstractError  do
      RaiseAbstractError(Instance, sMethodName)
    else
      raise
  end { try-except }
end { CallAbstractMethod };


class function  TGSkCustomComponent.ClassDisplayName(const   Instance:  TObject)
                                                     : string;
begin
  try
    if  Instance is TComponent  then
      Result := ComponentDisplayName(Instance as TComponent, True)
    else if  Instance is TCollectionItem  then
      Result := Format('%s.%s',
                       [ClassDisplayName((Instance as TCollectionItem).Collection),
                        (Instance as TCollectionItem).DisplayName])
    else
      Result := Instance.ClassName();
  except
    on  lErr : Exception  do
      Result := lErr.Message
  end { try-except };
end { ClassDisplayName };


class procedure  TGSkCustomComponent.CheckObjProp(const   Owner:  TComponent;
                                                  const   Prop:  TPersistent;
                                                  const   sPropName:  string);
begin
  if  Prop = nil  then
    raise  EGSkPasLibError.Create(Format('%s.%s',
                                         [ComponentDisplayName(Owner, True),
                                          sPropName]),
                                  'Właściwość nie jest określona')
end { CheckObjProp };


procedure  TGSkCustomComponent.CheckObjProp(Prop:  TPersistent;
                                            const   sPropName:  string);
begin
  CheckObjProp(Self, Prop, sPropName);
end { CheckObjProp };


class function  TGSkCustomComponent.ClassDisplayName(const   AClass:  TClass)
                                                     : string;
begin
  Result := AClass.ClassName()
end { ClassDisplayName };


class function  TGSkCustomComponent.ComponentDisplayName(Component:  TComponent;
                                                         const   bWithPath, bWithRoot:  Boolean)
                                                         : string;
var
  sComp:  string;

  function  ComponentName(const   Component:  TComponent)
                          : string;
  begin
    Assert(Component <> nil);
    if  Component.Name  <> ''  then
      Result := Component.Name
    else
      Result := Component.ClassName
  end { ComponentName };

begin  { ComponentDisplayName }
  if  Assigned(Component)  then
    begin
      Result := '';
      repeat
        sComp := ComponentName(Component);
        if  Result = ''  then
          Result := sComp
        else
          Result := Format('%s.%s', [sComp, Result]);
        if  Component is TControl  then
          Component := (Component as TControl).Parent
        else
          Component := Component.Owner
      until  not bWithPath
             or (Component = nil)
             or (Component is TCustomForm)
             or (Component is TApplication);
      if  bWithPath  then
        if  Assigned(Component)  then
          if  Component is TCustomForm  then
            Result := Format('%s.%s', [ComponentName(Component), Result]);
      if  bWithPath  then
        if  not bWithRoot  then
          if  {$IFDEF UNICODE}
                StartsText(sComp + '.', Result)
              {$ELSE}
                AnsiStartsText(sComp + '.', Result)
              {$ENDIF -UNICODE}  then
            Delete(Result, 1, Length(sComp) + 1)
    end { Component <> nil }
  else
    Result := '*nil*'
end { ComponentDisplayName };


constructor  TGSkCustomComponent.Create(Owner:  TComponent);
begin
  inherited
end { Create };


destructor  TGSkCustomComponent.Destroy();
begin
  inherited
end { Destroy };


{ TGSkCustomDialogFB }

constructor  TGSkCustomDialogFB.Create(Owner:  TComponent);
begin
  inherited
end { Create };


destructor  TGSkCustomDialogFB.Destroy();
begin
  inherited
end { Destroy };


{ TGSkCustomDialogFI }

constructor  TGSkCustomDialogFI.Create(Owner:  TComponent);
begin
  inherited
end { Create };


destructor  TGSkCustomDialogFI.Destroy();
begin
  inherited
end { Destroy };


{ TGSkCustomDialogP }

constructor  TGSkCustomDialogP.Create(Owner:  TComponent);
begin
  inherited
end { Create };


destructor  TGSkCustomDialogP.Destroy();
begin
  inherited
end { Destroy };


class  function TGSkCustomComponent.FindComponentForm(AComponent:  TComponent {$IFNDEF FMX};
                                                      const   bFloating:  Boolean {$ENDIF})
                                                      : TForm;
begin
  Result := nil;
  if  not Assigned(AComponent)  then
    Exit;
  repeat
    {$IFNDEF FMX}
      if  AComponent is TForm  then
        if  not bFloating
            or (AComponent as TForm).Floating  then
          Break;
    {$ENDIF -FMX}
    AComponent := AComponent.Owner
  until  AComponent = nil;
  Result := (AComponent as TForm)
end { FindComponentForm };


end.
