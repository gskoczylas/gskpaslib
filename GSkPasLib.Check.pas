﻿{: @abstract Podprogramy do sprawdzania poprawności danych.

   Moduł zawiera podprogramy ułatwiające sprawdzanie poprawności danych.
   Te podprogramy wykorzystywane są najczęściej w zdarzeniu @code(BeforePost)
   lub w akcji związanej z kliknięciem w przycisk @code([OK]).  }
unit  GSkPasLib.Check;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22}
    System.UITypes,
    Vcl.Controls, Vcl.Dialogs, Vcl.Forms, System.SysUtils, Vcl.Mask, Data.DB,
    Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.DBCtrls,
  {$ELSE}
    Controls, Dialogs, Forms, SysUtils, Mask, DB,
    StdCtrls, ExtCtrls, DBCtrls,
  {$IFEND}
  {$IFDEF GetText}
    JvGnugettext,
  {$ENDIF GetText}
  JvToolEdit, JvDBCombobox, JvDBLookup;


const
  {: @abstract(Pusta data) }
  EmptyDate:  TDateTime = 0.0;


{: @abstract(Sprawdza warunek "bCond". Jeżeli warunek jest prawdziwy to na tym kończy
             działanie.)

   Jeżeli warunek nie jest prawdziwy to sygnalizuje błąd o treści @code(sInfo).
   Komunikat o błędzie wyświetlany jest pod obiektem @code(edControl) (o ile
   jest dość miejsca na ekranie).  Następnie przekazuje sterowanie do tego
   obiektu i przerywa dalszą pracę (@code(Abort)).  }
procedure  ChkCond(const   bCond:  Boolean;
                   const   edControl:  TWinControl;
                   const   sInfo:  string = '');

{: @abstract Sprawdza, czy do wskazanego pola wpisano cokolwiek.

   Jeżeli pole jest puste to sygnalizuje błąd, przekazuje sterowanie do tego pola
   i przerywa działanie (@code(Abort)).  }
procedure  ChkDef(const   edEdit:  TCustomEdit;
                  const   sWypelnijCo:  string);                     overload;

{: @abstract Sprawdza czy do pola została wpisana data.

   Jeżeli nie to sygnalizuje błąd, przekazuje sterowanie do tego pola
   i przerywa działanie (@code(Abort)).  }
procedure  ChkDef(const   edEdit:  TJvCustomDateEdit;
                  const   sWypelnijDateCzego:  string);              overload;

{: @abstract Sprawdza, czy w @code(edCombo) został wybrany któryś wariant (ItemIndex >= 0).

   Jeżeli nie to sygnalizuje błąd, przekazuje sterowanie do tego obiektu
   i przerywa pracę (@code(Abort)).  }
procedure  ChkDef(const   edCombo:  TCustomComboBox;
                  const   sWskazCo:  string);                        overload;

{: @abstract Sprawdza, czy w @code(edCombo) został wybrany któryś wariant (ItemIndex >= 0).

   Jeżeli nie to sygnalizuje błąd, przekazuje sterowanie do tego obiektu
   i przerywa pracę (@code(Abort)).  }
procedure  ChkDef(const   edCombo:  TJvLookupControl;
                  const   sWskazCo:  string);                        overload;

{: @abstract Sprawdza, czy w @code(edRadio) został wybrany któryś wariant (ItemIndex >= 0).

   Jeżeli nie to sygnalizuje błąd, przekazuje sterowanie do tego obiektu
   i przerywa pracę (@code(Abort)).  }
procedure  ChkDef(const   edRadio:  TRadioGroup;
                  const   sWskazCo:  string);                        overload;

{: @abstract Sprawdza, czy w @code(edRadio) został wybrany któryś wariant (ItemIndex >= 0).

   Jeżeli nie to sygnalizuje błąd, przekazuje sterowanie do tego obiektu
   i przerywa pracę (@code(Abort)).  }
procedure  ChkDef(const   edPole:  TDBRadioGroup;
                  const   sWskazCo:  string);                        overload;

{: @abstract Sprawdza, czy pole @code(Field) jest niepuste.  

   Jeżeli pole jest puste to sygnalizuje błąd, przekazuje sterowanie do
   pierwszego obiektu na ekranie związanego z polem @code(Field) i przerywa 
   pracę (@code(Abort)).  }
procedure  ChkDef(Field:  TField;
                  const   sWpiszCo:  string);                        overload;

{: @abstract(Sprawdza, czy data @code(edFromDate) nie jest większa od daty 
             @code(edToDate).)

   Jeżeli kolejność dat nie jest poprawna to sygnalizuje błąd, przekazuje
   sterowanie do daty początkowej i przerywa pracę (@code(Abort)).  }
procedure  ChkSeqOfDates(const  edFromDate, edToDate:  TJvCustomDateEdit;
                         const  sKolejnoscCzego:  string); overload;

{: @abstract(Sprawdza, czy data wpisana w polu @code(edDate) mieści się
             w przedziale wyznaczonym przez daty @code(dFromDate) 
             i @code(dToDate).)
             
   Jeżeli tak nie jest to sygnalizuje błąd, przekazuje sterowanie do 
   pola @code(edDate) i przerywa pracę (@code(Abort)).  }
procedure  ChkSeqOfDates(const   edFromDate:  TJvCustomDateEdit;
                         const   sDataCzegoOd:  string;
                         const   edToDate:  TJvCustomDateEdit;
                         const   sDataCzegoDo:  string);   overload;

{: @abstract(Sprawdza, czy data wpisana w polu @code(edDate) mieści się
             w przedziale wyznaczonym przez daty @code(dFromDate) i @code(dToDate).)
   
   Jeżeli tak nie jest to sygnalizuje błąd, przekazuje sterowanie do pola 
   @code(edDate) i przerywa pracę (@code(Abort)).  }
procedure  ChkDateRange(const   edDate:  TJvCustomDateEdit;
                        const   dFromDate, dToDate:  TDateTime;
                        const   sDataCzego:  string);

{: @abstract Zadaje użytkownikowi pytanie @code(sQuestion).  

   Użytkownik ma do wyboru dwie możliwości: @code(OK) lub @code(Anuluj).  
   Jeżeli wybierze @code(Anuluj) to procedura przekaże sterowanie do obiektu 
   @code(edControl) i przerwie pracę (@code(Abort)).

   Przykład:
   @longCode(#
     try
       Param := IntToStr(Trim(Edit1.Text));
     except
       if  AskChk(Edit1,
                  'Parametr nie jest prawidłowy.'
                    + #13'Jeżeli zaakceptujesz to zostanie on pominięty.')
     end ; #)  }
procedure  AskChk(const   edControl:  TWinControl;
                  const   sQuestion:  string;
                  const   mtDialogType:  TMsgDlgType = mtConfirmation);   overload;


{: @abstract Zadaje użytkownikowi pytanie @code(sQuestion).

   Użytkownik ma do wyboru dwie możliwości: @code(OK) lub @code(Anuluj).
   Jeżeli wybierze @code(Anuluj) to procedura przekaże sterowanie do obiektu
   @code(edControl) i przerwie pracę (@code(Abort)).

   @param(edControl Kontrolka, której dotyczy pytanie lub wątpliwość.)
   @param(sQuestion Treść pytania lub wątpliwości.)
   @param(mtDialogType Typ dialogu z pytaniem lub wątpliwością.)
   @param(sAcceptBtn Tekst wyświelany na przycisku akceptacji.)
   @param(sCorrectBtn Tekst wyświetlany na przycisku odrzucenia.)

   Przykład:
   @longCode(#
     try
       Param := IntToStr(Trim(Edit1.Text));
     except
       if  AskChk(Edit1,
                  'Parametr nie jest prawidłowy.'
                    + #13'Jeżeli zaakceptujesz to zostanie on pominięty.',
                  mtWarning, ')
     end ; #)  }
procedure  AskChk(const   edControl:  TWinControl;
                  const   sQuestion:  string;
                  const   mtDialogType:  TMsgDlgType;
                  const   sAcceptBtn, sCorrectBtn:  string);         overload


{: @abstract Pyta użytkownika czy akceptuje puste dane.

   Jeżeli do pola @code(edEdit) użytkownik nic nie wpisał (jest puste) to
   wyświetla komunikat @italic(Brak »sBrakCzego«).  Użytkownik ma do wyboru dwie
   możliwości: @code(OK) lub @code(Anuluj).  Jeżeli wybierze @code(Anuluj) to
   procedura przekaże sterowanie do obiektu @code(edControl) i przerwie pracę
   (@code(Abort)).  }
procedure  AskChkDef(const   edEdit:  TCustomEdit;
                     const   sBrakCzego:  string;
                     const   mtDialogType:  TMsgDlgType = mtConfirmation);


implementation


uses
  GSkPasLib.PasLib, GSkPasLib.Dialogs, GSkPasLib.ComponentUtils;


type
  TCustomComboBoxCrack = class(TCustomComboBox);


procedure  ChkCond(const   bCond:  Boolean;
                   const   edControl:  TWinControl;
                   const   sInfo:  string);
begin  { ChkCond }
  if  bCond  then
    Exit;
  { Error }
  if  sInfo <> ''  then
    ErrDlg(sInfo, edControl)
  else
    Abort()
end { ChkCond };


procedure  ChkDef(const   edEdit:  TCustomEdit;
                  const   sWypelnijCo:  string);
var
  sBuf:  string;

begin  { ChkDef }
  if  edEdit is TCustomMaskEdit  then
    sBuf := (edEdit as TCustomMaskEdit).Text
  else
    sBuf := edEdit.Text;
  ChkCond(Trim(sBuf) <> '',
          edEdit,
          Format({$IFDEF GetText}
                   dgettext('GSkPasLib', 'Fill in the %s.'),
                 {$ELSE}
                   'Wypełnij %s.',
                 {$ENDIF -GetText}
                 [sWypelnijCo]))
end { ChkDef };


procedure  ChkDef(const   edEdit:  TJvCustomDateEdit;
                  const   sWypelnijDateCzego:  string);
begin
  ChkCond(edEdit.Date <> EmptyDate, edEdit,
          Format({$IFDEF GetText}
                   dgettext('GSkPasLib', 'Fill in the date %s.'),
                 {$ELSE}
                   'Wypełnij datę%s.',
                 {$ENDIF -GetText}
                 [sWypelnijDateCzego]))
end { ChkDef };


procedure  ChkDef(const   edCombo:  TCustomComboBox;
                  const   sWskazCo:  string);

var
  bCond:  Boolean;

begin  { ChkDef }
  Assert(Assigned(edCombo));
  {$IfOpt C+}
    if  edCombo is TJvCustomDBComboBox   then
      Assert(Assigned((edCombo as TJvCustomDBComboBox ).Field))
    else if  edCombo is TDBComboBox  then
      Assert(Assigned((edCombo as TDBComboBox).Field));
  {$EndIf C+}
  if  edCombo is TJvCustomDBComboBox  then
    bCond := (not (edCombo as TJvCustomDBComboBox).Field.IsNull)
  else if  edCombo is TDBComboBox  then
    bCond := (not (edCombo as TDBComboBox).Field.IsNull)
  else if  TCustomComboBoxCrack(edCombo).Style in [csSimple, csDropDown]  then
    bCond := Trim(TCustomComboBoxCrack(edCombo).Text) <> ''
  else
    bCond := (edCombo.ItemIndex >= 0);
  ChkCond(bCond,
          edCombo,
          Format({$IFDEF GetText}
                   dgettext('GSkPasLib', 'Select the %s.'),
                 {$ELSE}
                   'Wskaż %s.',
                 {$ENDIF -GetText}
                 [sWskazCo]))
end { ChkDef };


procedure  ChkDef(const   edCombo:  TJvLookupControl;
                  const   sWskazCo:  string);
begin
  Assert(Assigned(edCombo));
  Assert(Assigned(edCombo.Field));
  ChkCond(not edCombo.Field.IsNull,
          edCombo,
          Format({$IFDEF GetText}
                   dgettext('GSkPasLib', 'Select the %s.'),
                 {$ELSE}
                   'Wskaż %s.',
                 {$ENDIF -GetText}
                 [sWskazCo]))
end { ChkDef };


procedure  ChkDef(const   edRadio:  TRadioGroup;
                  const   sWskazCo:  string);
begin
  Assert(Assigned(edRadio));
  ChkCond(edRadio.ItemIndex >= 0,
          edRadio,
          Format({$IFDEF GetText}
                   dgettext('GSkPasLib', 'Select the %s.'),
                 {$ELSE}
                   'Wskaż %s.',
                 {$ENDIF -GetText}
                 [sWskazCo]));
end { ChkDef };


procedure  ChkDef(const   edPole:  TDbRadioGroup;
                  const   sWskazCo:  string);
begin
  Assert(Assigned(edPole));
  ChkCond(edPole.ItemIndex >= 0,
          edPole,
          Format({$IFDEF GetText}
                   dgettext('GSkPasLib', 'Select the %s.'),
                 {$ELSE}
                   'Wskaż %s.',
                 {$ENDIF -GetText}
                 [sWskazCo]))
end { ChkDef };


procedure  ChkDef(        Field:  TField;
                  const   sWpiszCo:  string);
begin
  Assert(Assigned(Field));
{
  Assert(Assigned(Field.Owner));
  Assert(Field.Owner is TForm);
}
  if  not Field.IsNull  then
    case  Field.DataType  of
      ftString:
        begin
          with  Field as TStringField  do
            begin
              AsString := Trim(AsString);
              if  AsString <> ''  then
                Exit;
            end { with TField as TStringField }
        end { ftString };
      ftSmallint, ftInteger, ftLargeint,
      ftFloat, ftCurrency,
      ftDate, ftTime, ftDateTime:
        if  Field.Value <> 0  then
          Exit
      else
        Exit
    end { case Field.DataType };
  MessageDlg(Format({$IFDEF GetText}
                      dgettext('GSkPasLib', 'Fill in the %s.'),
                    {$ELSE}
                      'Wypełnij %s.',
                    {$ENDIF -GetText}
                    [sWpiszCo]),
             mtError, [mbOk],
             0{(Field.Owner as TForm).HelpContext});
  Field.FocusControl;
  Abort
end { ChkDef };


procedure  ChkSeqOfDates(const  edFromDate, edToDate:  TJvCustomDateEdit;
                         const  sKolejnoscCzego:  string);
begin
  Assert(Assigned(edFromDate));
  Assert(Assigned(edToDate));
  ChkCond(edFromDate.Date <= edToDate.Date,
          edFromDate,
          Format({$IFDEF GetText}
                   dgettext('GSkPasLib', 'Incorrect order of the %s.'),
                 {$ELSE}
                   'Niepoprawna kolejność %s.',
                 {$ENDIF -GetText}
                 [sKolejnoscCzego]))
end { ChkSeqOfDates };


procedure  ChkSeqOfDates(const   edFromDate:  TJvCustomDateEdit;
                         const   sDataCzegoOd:  string;
                         const   edToDate:  TJvCustomDateEdit;
                         const   sDataCzegoDo:  string);
begin
  Assert(Assigned(edFromDate));
  Assert(Assigned(edToDate));
  ChkCond(edFromDate.Date <= edToDate.Date,
          edFromDate,
          Format({$IFDEF GetText}
                   dgettext('GSkPasLib', 'The date %0:s can not be later than %1:s.')
                   + sLineBreak + dgettext('GSkPasLib', 'Incorrect order of dates.'),
                 {$ELSE}
                   'Data %s nie może być późniejsza od daty %s.'
                   + sLineBreak + 'Niepoprawna kolejność dat.',
                 {$ENDIF -GetText}
                 [sDataCzegoOd, sDataCzegoDo]));
end { ChkSeqOfDates };


procedure  ChkDateRange(const   edDate:  TJvCustomDateEdit;
                        const   dFromDate, dToDate:  TDateTime;
                        const   sDataCzego:  string);
var
  sInfo:  string;

  function  FmtDate(dDate:  TDateTime)
                    : string;
  begin
    Result := DateToStr(dDate);
    if  dDate = Date  then
      Result := Result + ' '
                + {$IFDEF GetText}
                    dgettext('GSkPasLib', '(today)');
                  {$ELSE}
                    '(dzisiaj)';
                  {$ENDIF -GetText}
  end { FmtDate };

begin  { ChkDateRange }
  Assert((dFromDate <> EmptyDate) or (dToDate <> EmptyDate));
  Assert(Assigned(edDate));
  if  (dFromDate <> EmptyDate) and (dToDate <> EmptyDate)  then
    sInfo := Format({$IFDEF GetText}
                      dgettext('GSkPasLib', 'This date must be the period from %0:s to %1:s.'),
                    {$ELSE}
                      'Ta data musi być z okresu od %s do %s.',
                    {$ENDIF -GetText}
                    [DateToStr(dFromDate), DateToStr(dToDate)])
  else if  dFromDate <> EmptyDate  then
    sInfo := Format({$IFDEF GetText}
                      dgettext('GSkPasLib', 'This date may not be ealier than %s.'),
                    {$ELSE}
                      'Ta data nie może być wcześniejsza niż %s.',
                    {$ENDIF -GetText}
                    [FmtDate(dFromDate)])
  else
    sInfo := Format({$IFDEF GetText}
                      dgettext('GSkPasLib', 'This date may not be later than %s.'),
                    {$ELSE}
                      'Ta data nie może być późniejsza niż %s.',
                    {$ENDIF -GetText}
                    [FmtDate(dToDate)]);
  sInfo := {$IFDEF GetText}
             dgettext('GSkPasLib', 'Incorrect date.')
           {$ELSE}
             'Niepoprawna data.' 
           {$ENDIF -GetText}
           + sLineBreak + sInfo;
  ChkCond((dFromDate <= edDate.Date) and
              ((edDate.Date <= dToDate) or (dToDate = EmptyDate)),
          edDate, sInfo)
end { ChkDateRange };


procedure  AskChk(const   edControl:  TWinControl;
                  const   sQuestion:  string;
                  const   mtDialogType:  TMsgDlgType);
begin
  AskChk(edControl, sQuestion, mtDialogType,
         {$IFDEF GetText}
           dgettext('GSkPasLib', 'Accept'),
           dgettext('GSkPasLib', 'Correct')
         {$ELSE}
           'Akceptuję', 'Poprawiam'
         {$ENDIF -GetText})
end { AskChk };


procedure  AskChk(const   edControl:  TWinControl;
                  const   sQuestion:  string;
                  const   mtDialogType:  TMsgDlgType;
                  const   sAcceptBtn, sCorrectBtn:  string);
begin
  Assert(Assigned(edControl));
  Assert(edControl.Owner is TForm, 'Właścicielem pola powinna być forma');
  SetControlPage(edControl);
  ChkCond(MsgDlgEx(sQuestion, mtDialogType,
                   [sAcceptBtn, sCorrectBtn],
                   [1,          2],
                   edControl) = 1,
          edControl, '')
end { AskChk };


procedure  AskChkDef(const   edEdit:  TCustomEdit;
                     const   sBrakCzego:  string;
                     const   mtDialogType:  TMsgDlgType);
begin
  Assert(Assigned(edEdit));
  Assert(edEdit.Owner is TForm, 'Właścicielem pola powinna być forma');
  if  Trim(edEdit.Text) = ''  then
    begin
      SetControlPage(edEdit);
      ChkCond(MsgDlgEx(Format({$IFDEF GetText}
                                dgettext('GSkPasLib', 'Missing %s.')
                              {$ELSE}
                                'Brak %s.'
                              {$ENDIF -GetText},
                              [sBrakCzego]),
                       mtDialogType,
                       {$IFDEF GetText}
                         [dgettext('GSkPasLib', 'Accept'),
                          dgettext('GSkPasLib', 'Correct')]
                       {$ELSE}
                         ['Akceptuję', 'Poprawiam']
                       {$ENDIF -GetText},
                       [1, 2],
                       edEdit) = 1,
              edEdit, '')
    end { edEdit.Text = '' }
end { AskChkDef };


end.
