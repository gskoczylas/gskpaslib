﻿{: @abstract Definicja bazowa głównego okna aplikacji.

   Moduł @name zawiera definicję okna bazowego dla głównego okna aplikacji. 
   Jest ono potomkiem bazowego okna @link(TzzGSkForm).

   W typowej aplikacji główne okno powinno być potomkami (bezpośrednimi lub
   pośrednimi) klasy @link(TzzGSkCustomMainForm).  }
unit  GSkPasLib.CustomMainForm;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GSkPasLib.Form, ComCtrls, ActnList, Menus, ExtCtrls, Registry,
  JvExExtCtrls, JvSpeedbar,
  GSkPasLib.PrevInst, GSkPasLib.ParamSaver, GSkPasLib.FileUtils,
  GSkPasLib.SpeedBarCtl, JvExtComponent;


type
  {: @abstract Okno bazowe głównego okna aplikacji.

     W typowej aplikacji główne okno powinno być bezpośrednim lub pośrednim
     potomkiem okna @name.

     Okno @name jest potomkiem klasy @link(TzzGSkForm).  }
  TzzGSkCustomMainForm = class(TzzGSkForm)
    sbSpeedBar:  TJvSpeedBar;
    mnMainMenu:  TMainMenu;
    mnPlik:      TMenuItem;
    alActions:   TActionList;
    aTerminate:  TAction;
    mnTerminate: TMenuItem;
    procedure  aTerminateExecute(Sender:  TObject);
    procedure  PrevInstLimitExceded(Sender:  TObject;
                                    nInstanceCount:  Byte;
                                    var   Actions:  TPrevInstActions);
  private
    fnVersionRelationship:  TValueRelationship;
    fSpeedBarCtl:  TGSkSpeedBarCtl;
    ffnOnClose:  TCloseEvent;
    procedure  ResizePanels();
    procedure  OnCloseEvent(Sender:  TObject;
                            var   CloseAction:  TCloseAction);
    procedure  WMEndSession(var   Msg:  TWMEndSession);              message WM_ENDSESSION;
  protected
    {: @exclude }
    procedure  DoCreate();                                           override;
    {: @exclude }
    procedure  Resize();                                             override;
    {: @exclude }
    procedure  Minimize(Sender:  TObject);
    {: @exclude }
    procedure  Restore(Sender:  TObject);
    {: @exclude }
    procedure  ReadParams(const   Saver:  TGSkCustomParamSaver);     override;
  public
    {: @abstract Wyświetla parametry bazy danych.
    
       Metoda @name wyświetla na pasku statusu okna nazwę („alias”) oraz
       użytkownika bazy danych.  }
    procedure  ShowDatabaseParams(const   sAlias, sUserName:  string);
    
    {: @abstract Informuje czy wersja programu uległa zmianie.
    
      Właściwość @name informuje jak się ma bieżąca wersja programu do wersji
      programu uruchomionego poprzednio.
      
      Dzięki tej właściwości można wykryć pierwsze uruchomienie nowej wersji
      programu na danym komputerze (np. po aktualizacji programu).  }
    property  VersionRelationship:  TValueRelationship
              read  fnVersionRelationship;
  end { TGSkCustomMainForm };


// var
//   GSkCustomMainForm:  TGSkCustomMainForm;


implementation


uses
  GSkPasLib.CustomMainDataModule, GSkPasLib.AppUtils, GSkPasLib.PasLib;


{$R *.dfm}


procedure  TzzGSkCustomMainForm.aTerminateExecute(Sender:  TObject);
begin
  inherited;
  Close()
end { aTerminateExecute };


procedure  TzzGSkCustomMainForm.DoCreate();

var
  nInd:  Integer;
  lPrevInst:  TGSkPrevInst;

begin  { DoCreate }
  CreateMutex(nil, False,
              PChar(LowerCase(ChangeFileExt(ExtractFileName(Application.ExeName),
                                            ''))));
  inherited;
  with  Screen  do
    for  nInd := DataModuleCount - 1  downto  0  do
      if  DataModules[nInd] is TzzGSkCustomMainDataModule  then
        with  DataModules[nInd] as TzzGSkCustomMainDataModule  do
          begin
            ShowDatabaseParams(DatabaseAlias, UserName);
            Break
          end ;
  { - }
  ffnOnClose := OnClose;
  OnClose := OnCloseEvent;
  { - }
  Application.OnMinimize := Minimize;
  Application.OnRestore := Restore;
  { - }
  lPrevInst := TGSkPrevInst.Create(Self);
  lPrevInst.OnLimitExceded := PrevInstLimitExceded;
  { - }
  fSpeedBarCtl := TGSkSpeedBarCtl.Create(Self);
  fSpeedBarCtl.SpeedBar := sbSpeedBar;
  fSpeedBarCtl.RestoreLayout();
end { DoCreate };


procedure  TzzGSkCustomMainForm.Minimize(Sender:  TObject);
begin
  Perform(WM_SYSCOMMAND, SC_MINIMIZE, 0)
end { Minimize };


procedure  TzzGSkCustomMainForm.Resize();
begin
  inherited;
  ResizePanels()
end { Resize };


procedure  TzzGSkCustomMainForm.ResizePanels();

var
  nInd:  Integer;
  nWidth:  Integer;

begin  { ResizePanels }
  with  sbStatus.Panels  do
    begin
      nWidth := sbStatus.ClientWidth;
      for  nInd := 1  to  Count - 1  do
        Dec(nWidth, Items[nInd].Width);
      Items[0].Width := nWidth
    end { with sbStatus.Panels }
end { ResizePanels };


procedure  TzzGSkCustomMainForm.Restore(Sender:  TObject);
begin
  Perform(WM_SYSCOMMAND, SC_RESTORE, 0)
end { Restore };


procedure  TzzGSkCustomMainForm.ShowDatabaseParams(const   sAlias, sUserName:  string);
begin
  with  sbStatus.Panels  do
    begin
      with  Items[1]  do
        begin
          Text := sAlias;
          Width := Canvas.TextWidth(sAlias) + 10
        end { with Items[1] };
      with  Items[2]  do
        begin
          Text := sUserName;
          Width := Canvas.TextWidth(sUserName) + 10
        end { with Items[2] }
    end { with sbStatus.Panels };
  ResizePanels()
end { ShowDatabaseParams };


procedure  TzzGSkCustomMainForm.WMEndSession(var   Msg:  TWMEndSession);
begin
  inherited;
  if  Msg.EndSession  then
    with  TRegistry.Create()  do
      try
        Assert(RootKey = HKEY_CURRENT_USER);
        if  OpenKey('\Software\Microsoft\Windows\CurrentVersion\RunOnce',
                    True)  then
          try
            WriteString(GetAppInternalName(), GetCommandLine())
          finally
            CloseKey()
          end { try-finally }
      finally
        Free()
      end { try-finally }
end { WMEndSession };


procedure  TzzGSkCustomMainForm.PrevInstLimitExceded(Sender:  TObject;
                                                     nInstanceCount:  Byte;
                                                     var   Actions:  TPrevInstActions);
var
  sWarn:  string;

begin  { PrevInstLimitExceded }
  inherited;
  sWarn := 'Ten program już jest uruchomiony ';
  if  nInstanceCount = 2  then
    sWarn := sWarn + 'jeden raz'
  else
    sWarn := Format('%s%d razy',
                    [sWarn, Pred(nInstanceCount)]);
  sWarn := sWarn
           + #13'Czy uruchomić następną kopię programu?';
  if  Application.MessageBox(PChar(sWarn),
                             PChar(Application.Title),
                             MB_ICONQUESTION or MB_YESNO or MB_DEFBUTTON2) = IDYES  then
    Actions := []
  else
    Actions := [piRestPrev, piQuitCurr]
end { PrevInstLimitExceded };


procedure  TzzGSkCustomMainForm.ReadParams(const   Saver:  TGSkCustomParamSaver);
begin
  if  Saver.OpenKey(GetAppRegistryKey(), True)  then
    try
      with  GetAppVersionInfo()  do
        try
          fnVersionRelationship := CompareVersion(FileVersion,
                                                  Saver.ReadString(gcsParAppVersion),
                                                  4);
          Saver.WriteString(gcsParAppVersion, FileVersion)
        finally
          Free()
        end { with GetAppVersionInfo() }
    finally
      Saver.CloseKey()
    end { try-finally }
  else
    fnVersionRelationship := EqualsValue
end { ReadParams };


procedure  TzzGSkCustomMainForm.OnCloseEvent(Sender:  TObject;
                                             var   CloseAction:  TCloseAction);
begin
  if  Assigned(ffnOnClose)  then
    ffnOnClose(Sender, CloseAction);   // <-- inherited;
  if  CloseAction = caNone  then
    Exit;
  fSpeedBarCtl.SaveLayout()
end { OnCloseEvent };


end.
