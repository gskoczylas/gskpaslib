﻿{: @abstract Rejestrowanie bloków pamięci.

   Moduł @name zawiera definicję klasy pomocniczej dla klasy @link(TGSkLog).
   Po umieszczeniu go w sekcji @code(uses) razem z modułem @link(GSkPasLib.Log)
   możliwe jest wygodne rejetrowanie bloków pamięci. }
unit  GSkPasLib.LogDump;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    System.SysUtils, System.Classes,
  {$ELSE}
    SysUtils, Classes,
  {$IFEND}
  GSkPasLib.Log;


type
  {: @abstract Rozszerza klasę @link(TGSkLog) o monitorowanie bloków pamięci. }
  TGSkLogHlpDump = class helper for TGSkLog
  public

    {: @abstract Rejestruje wskazany blok bajtów.

       Ta wersja metody przeznaczona jest do rejestrowania dowolnych bloków bajtów.

       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogDump(const   sName:  string;
                      const   Value:  TBytes;
                      const   lmMsgType:  TGSkLogMessageType = lmInformation)
                      : TGSkLog;                                    overload; inline;

    {: @abstract Rejestruje wskazany blok napis jako blok bajtów.

       Ta wersja metody przeznaczona jest do rejestrowania bloków bajtów
       przekazywanych jako napis.

       @param sName     Nazwa wartości (zmiennej).
       @param sValue    Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogDump(const   sName:  string;
                      const   sValue:  string;
                      const   lmMsgType:  TGSkLogMessageType = lmInformation)
                      : TGSkLog;                                    overload; inline;
  end { TGSkLogHlpDump };


function  LogDump(const   sName:  string;
                  const   Value:  TBytes;
                  const   LogObj:  TGSkLog;
                  const   lmMsgType:  TGSkLogMessageType = lmInformation)
                  : TGSkLog;                                         overload;

function  LogDump(const   sName:  string;
                  const   sValue:  string;
                  const   LogObj:  TGSkLog;
                  const   lmMsgType:  TGSkLogMessageType = lmInformation)
                  : TGSkLog;                                         overload; inline;


implementation


uses
  GSkPasLib.Dump;


{$REGION 'Global subroutines'}

function  LogDump(const   sName:  string;
                  const   Value:  TBytes;
                  const   LogObj:  TGSkLog;
                  const   lmMsgType:  TGSkLogMessageType)
                  : TGSkLog;
var
  lDump:  TStringStream;
  sDump:  string;

begin  { LogDump }
  lDump := TStringStream.Create('', TEncoding.Unicode);
  try
    TGSkDump.Dump(Value, lDump);
    lDump.Seek(0, soFromBeginning);
    sDump := lDump.ReadString(lDump.Size);
    LogObj.LogMemo(sName, sDump, lmMsgType)
  finally
    lDump.Free()
  end { try-finally };
  Result := LogObj
end { LogDump };


function  LogDump(const   sName:  string;
                  const   sValue:  string;
                  const   LogObj:  TGSkLog;
                  const   lmMsgType:  TGSkLogMessageType)
                  : TGSkLog;
begin
  Result := LogDump(sName, BytesOf(sValue), LogObj, lmMsgType)
end { LogDump };

{$ENDREGION 'Global subroutines'}


{$REGION 'TGSkLogHlpDump'}

function  TGSkLogHlpDump.LogDump(const   sName:  string;
                                 const   Value:  TBytes;
                                 const   lmMsgType:  TGSkLogMessageType)
                                 : TGSkLog;
begin
  Result := GSkPasLib.LogDump.LogDump(sName, Value, Self, lmMsgType)
end { LogDump };


function  TGSkLogHlpDump.LogDump(const   sName, sValue:  string;
                                 const   lmMsgType:  TGSkLogMessageType)
                                 : TGSkLog;
begin
  Result := LogDump(sName, BytesOf(sValue), lmMsgType)
end { LogDump };

{$ENDREGION 'TGSkLogHlpDump'}


end.
