﻿unit  Test.GSkPasLib.Log;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}

{#Pomysł: wykorzystać zmienne ExceptProc, ErrorProc, AssertErrorProc, AbstractErrorProc
          z modułu System do monitorowania błędów. }

uses
  TestFramework, SysUtils,
  GSkPasLib.Log;


type
  TLog_Test = class(TTestCase)
  published
    procedure  TestAssign();
    procedure  TestMemory1();
    procedure  TestMemory2();
    procedure  TestMemo();
    procedure  TestAbandon();
    procedure  TestLogOffset();
//     procedure  TestLogService();
  end { TFileUtils_Test };


implementation


uses
  GSkPasLib.LogFile{, GSkPasLib.LogTraceTool};


{ TLog_Test }

procedure TLog_Test.TestAbandon;

var
  lLog:  TGSkLog;

begin  { TestAbandon }
  lLog := TGSkLog.Create(nil);
  try
    TGSkLogFile.CreateLog(lLog, flAppDir);
    lLog.LogMessage('*** Jeden poziom. Wycofanie informacji powoduje, że nic nie jest rejestrowane');
    lLog.BeginLog();
    try
      lLog.LogMessage('Jakiś tekst', lmInformation);
      lLog.LogMessage('Jakieś ostrzeżenie', lmWarning);
      lLog.LogMessage('Jakiś błąd', lmError);
    finally
      lLog.AbandonLog()
    end { try-finally };
    lLog.LogLines();
    lLog.LogMessage('*** Dwa poziomy. Wycofuję informacje z drugiego poziomu');
    lLog.BeginLog();
    try
      lLog.LogMessage('Poziom #1: Cokolwiek');
      lLog.BeginLog();
      try
        lLog.LogMessage('Poziom #2: Inna informacja');
        lLog.LogMessage('Poziom #2: Jeszcze coś innego');
      finally
        lLog.AbandonLog()
      end { try-finally };
      lLog.LogMessage('Poziom #1: Poziom #2 nie powien występować w dzienniku')
    finally
      lLog.EndLog()
    end { try-finally }
  finally
    lLog.Free()
  end { try-finally }
end { TestAbandon };


procedure  TLog_Test.TestAssign();

var
  lLog, lLog1, lLog2:  TGSkLog;

begin  { TestAssign }
  lLog := TGSkLog.Create(nil);
  try
    lLog1 := TGSkLog.Create(nil);
    try
      lLog1.AddLogAdapter(TGSkLogFile.Create(lLog1));
      lLog.Assign(lLog1);
      lLog.Active := True;
      lLog.Offset := 12;
      lLog2 := TGSkLog.Create(nil);
      try
        lLog2.AddLogAdapter(TGSkLogFile.Create(lLog2));
        lLog.Assign(lLog2);
        lLog.Active := True;
        lLog.Offset := 12
      finally
        lLog2.Free()
      end { try-finally }
    finally
      lLog1.Free()
    end { try-finally }
  finally
    lLog.Free()
  end { try-finally }
end { TestAssign };


procedure  TLog_Test.TestLogOffset();

const
  cnLogOffsetStep = gcnLogOffsetStep * 2;

var
  lLog:  TGSkLog;
  nOfs:  Integer;

begin  { TestLogOffset }
  lLog := TGSkLog.CreateLog(nil,
                            [{TGSkLogTraceTool{, TGSkLogGExperts, TGSkLogDialog}]);
  try
    lLog.AddLogAdapter(TGSkLogFile.CreateLog(nil, flAppDir));  // niestandardowy konstruktor!
    nOfs := 0;
    CheckEquals(nOfs, lLog.Offset, 'Na początku powinno być ' + IntToStr(nOfs));
    lLog.BeginLog();
    CheckEquals(nOfs, lLog.Offset, 'Po „BeginLog” nadal powinno być ' + IntToStr(nOfs));
    lLog.LogEnter('Przetwarzanie');   Inc(nOfs, cnLogOffsetStep);
    CheckEquals(nOfs, lLog.Offset, 'Po „LogEnter” powinno być ' + IntToStr(nOfs));
    lLog.EndLog();
    CheckEquals(nOfs, lLog.Offset, 'Po „EndLog” nadal powinno być ' + IntToStr(nOfs));

    lLog.BeginLog();
    CheckEquals(nOfs, lLog.Offset, 'Po drugim „BeginLog” powinno być ' + IntToStr(nOfs));
    lLog.LogEnter('Drugi poziom');   Inc(nOfs, cnLogOffsetStep);
    CheckEquals(nOfs, lLog.Offset, 'Po drugim „LogEnter” powinno być ' + IntToStr(nOfs));
    lLog.LogExit('Drugi poziom'); Dec(nOfs, cnLogOffsetStep);
    CheckEquals(nOfs, lLog.Offset, 'Po drugim „LogExit” powinno być ' + IntToStr(nOfs));
    lLog.AbandonLog();
    CheckEquals(nOfs, lLog.Offset, 'Po „AbandonLog” powinno być ' + IntToStr(nOfs));

{}{
    lLog.BeginLog();
    CheckEquals(nOfs, lLog.Offset, 'Po trzecim „BeginLog” powinno być ' + IntToStr(nOfs));
    lLog.LogEnter('Drugi poziom [2]');   Inc(nOfs, cnLogOffsetStep);
    CheckEquals(nOfs, lLog.Offset, 'Po trzecim „LogEnter” powinno być ' + IntToStr(nOfs));
    lLog.AbandonLog();   Dec(nOfs, cnLogOffsetStep);
    CheckEquals(nOfs, lLog.Offset, 'Po drugim „AbandonLog” powinno być ' + IntToStr(nOfs));
{}//^^^ To nie działa. Offset pozostaje wyższy

    lLog.BeginLog();
    CheckEquals(nOfs, lLog.Offset, 'Po czwartym „BeginLog” nadal powinno być ' + IntToStr(nOfs));
    lLog.LogExit('Przetwarzanie');   Dec(nOfs, cnLogOffsetStep);
    CheckEquals(0, lLog.Offset, 'Po czwartym „LogExit” powinno być ' + IntToStr(nOfs));
    lLog.EndLog();
    CheckEquals(0, lLog.Offset, 'Po czwartym „EndLog” nadal powinno być ' + IntToStr(nOfs));
  finally
    lLog.Free()
  end { try-finally }
end { TestLogOffset };


// procedure  TLog_Test.TestLogService();
//
// var
//   lLog:  TGSkLog;
//   lSvc:  TGSkLogService;
//
// begin  { TestLogService }
//   lLog := TGSkLog.Create(nil);
//   try
//     lSvc := TGSkLogService.Create(lLog);
//     lSvc.ModuleName := 'Moduł testowy';
//     lSvc.LogFilePath := 'p:\GSkPasLib\Tests\bin\LogService.log';
//     {-}
//     lLog.LogMessage('Stać do walki', lmError);
//     lLog.BeginLog();
//     lLog.LogMessage('Hero or Murder?', lmWarning);
//     lLog.LogMessage('He''s fantastic!', lmInformation);
//     lLog.EndLog();
//   finally
//     lLog.Free()
//   end { try-finally }
// end { TestLogService };


procedure  TLog_Test.TestMemo();

var
  lLog:  TGSkLog;

begin
  lLog := TGSkLog.CreateLog(nil,
                            [{TGSkLogTraceTool{, TGSkLogGExperts, TGSkLogDialog}]);
  try
    lLog.AddLogAdapter(TGSkLogFile.CreateLog(nil, flAppDir));  // niestandardowy konstruktor!
    lLog.LogMemo('Pirwszy wiersz'#13#10
                   + 'Drugi wiersz'#13#10
                   + 'Trzeci wiersz'#13#10,
                 lmWarning)
  finally
    lLog.Free()
  end { try-finally }
end { TestMemo };


procedure  TLog_Test.TestMemory1();

var
  lLog:  TGSkLog;

begin  { TestMemory1 }
  lLog := TGSkLog.Create(nil);
  lLog.Free()
end { TestMemory1 };


procedure  TLog_Test.TestMemory2();

var
  lLog:  TGSkLog;

begin  { TestMemory2 }
  lLog := TGSkLog.Create(nil);
  lLog.AddLogAdapter(TGSkLogFile.Create(lLog));
  lLog.Free()
end { TestMemory2 };


initialization
  RegisterTest(TLog_Test.Suite())


end.
