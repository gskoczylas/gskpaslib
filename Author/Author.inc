﻿{ Author identification }


  {: @abstract Standardowa nazwa pliku konfiguracyjnego

     Stała @name jest wykorzystywana przez funkcję
     @link(GSkPasLib.AppUtils.GetAppConfigFileName) do ustalenia nazwy pliku
     konfiguracyjnego aplikacji.

     Stała @name zazwyczaj powinna wskazywać krótką nazwę firmy, np. „Rekord”,
     „Fiat”, „GSkSoft”, „Jantar” itp., będącej autorem programu korzystającego
     z pakietu @bold(@italic(GSkPasLib)).

     Zmodyfikuj zawartość pliku „Author.inc” --- wstaw do definicji
     w tym pliku swoje dane.  }
  gcsStdConfigName = 'Program';

  {: @abstract Krótka nazwa firmy lub właściciela programów.

     Stała @name wskazuje krótką nazwę firmy lub właściciela (autora) programów
     i bibliotek tworzonych z wykorzystaniem pakietu @bold(@italic(GSkPasLib)).

     Zmodyfikuj zawartość pliku „Author.inc” --- wstaw do definicji
     w tym pliku swoje dane.  }
  gcsCompanyName = 'Program';

  {: @abstract Nazwa firmy ---  np. w nagłówkach dokumentów, adresach firmy itp. }
  gcsCompanyFullName = 'ZUI PROGRAM Grzegorz Skoczylas';

  {: @abstract Adres strony internetowej właściciela }
  gcsCompanyWebsite = 'https://plus.google.com/+GrzegorzSkoczylas';

  {: @abstract Wizytówka właściciela }
  gcsCompanyCard = gcsCompanyFullName
                   + #13#10
                   + #13#10 + gcsCompanyWebsite;


{ The End }
