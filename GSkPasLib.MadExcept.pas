﻿unit  GSkPasLib.MadExcept;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$IF CompilerVersion > 22} // XE
  {$LEGACYIFEND ON}
{$IFEND}


uses
  madExcept,
  {$IF CompilerVersion > 22} // XE
    System.SysUtils, Winapi.Windows
  {$ELSE}
    SysUtils, Windows
  {$IFEND}     {$IFDEF GetText},
  JvGnugettext {$ENDIF GetText};


{: @abstract Tłumaczy teksty na bieżący język.

   Procedura @name tłumaczy teksty podsystemu @italic(madExcept) na bieżący
   język. Przy okazji ustawia standardowe nazwy plików.

   Procedura @name jest wywoływana @bold(automatycznie) podczas uruchamiania
   programu. Można ją również wywołać w kodzie, na przykład jeżeli zostanie
   zmieniony język interfejsu programu. }
procedure  InitializeMadExcept();


implementation


uses
  {$IFDEF DEBUG}
    GSkPasLib.MiscUtils,
  {$ENDIF DEBUG}
  GSkPasLib.FileUtils;


{$IF CompilerVersion >= 20}   // Delphi 2009

function  GetExceptionStackInfoProc(P:  System.PExceptionRecord)
                                    : Pointer;
var
  sReport:  string;

begin  { GetExceptionStackInfoProc }
  try
    sReport := CreateBugReport(etHidden, nil, nil, GetCurrentThreadId());
    Result := StrNew(PChar(sReport))
  except
    Result := nil
  end { try-except }
end { GetExceptionStackInfoProc };


function  GetStackInfoStringProc(Info:  Pointer)
                                 : string;
begin
  Result := PChar(Info)
end { GetStackInfoStringProc };


procedure  CleanUpStackInfoProc(Info:  Pointer);
begin
  StrDispose(PChar(Info))
end { CleanUpStackInfoProc };

{$IFEND}


procedure  InitializeMadExcept();

var
  sModule:  string;

begin  { InitializeMadExcept }
  with  MESettings()  do
    begin
      { Translations }
      {$IFDEF GetText}
        TitleBar              := dgettext('madExcept', '%appname%');
        ExceptMsg             := dgettext('madExcept', 'An error occurred in the application.');
        FrozenMsg             := dgettext('madExcept', 'The application seems to be frozen.');
        BitFaultMsg           := dgettext('madExcept', 'The file "%modname%" seems to be corrupt!');
        SendBtnCaption        := dgettext('madExcept', 'send bug report');
        SaveBtnCaption        := dgettext('madExcept', 'save bug report');
        PrintBtnCaption       := dgettext('madExcept', 'print bug report');
        ShowBtnCaption        := dgettext('madExcept', 'show bug report');
        ContinueBtnCaption    := dgettext('madExcept', 'continue application');
        RestartBtnCaption     := dgettext('madExcept', 'restart Application');
        CloseBtnCaption       := dgettext('madExcept', 'close application');
        OkBtnCaption          := dgettext('madExcept', '&OK');
        DetailsBtnCaption     := dgettext('madExcept', '&Details');
        PleaseWaitTitle       := dgettext('madExcept', 'Information');
        PleaseWaitText        := dgettext('madExcept', 'Please wait a moment...');
        BugTrackerTitle       := dgettext('madExcept', '%appname% ver. %appversion%, %exceptMsg%');
        BugTrackerDescription := dgettext('madExcept', 'error details:' + sLineBreak + '%errorDetails%');
        MailSubject           := dgettext('madExcept', '%appname% %appversion% bug report');
        MailBody              := dgettext('madExcept', 'please find the bug report attached - module %modname%');
        SendBoxTitle          := dgettext('madExcept', 'Sending bug report...');
        PrepareAttachMsg      := dgettext('madExcept', 'Preparing attachments...');
        MxLookupMsg           := dgettext('madExcept', 'Searching for mail server...');
        ConnectMsg            := dgettext('madExcept', 'Connecting to server...');
        SendMailMsg           := dgettext('madExcept', 'Sending mail...');
        FieldsMsg             := dgettext('madExcept', 'Setting fields...');
        SendAttachMsg         := dgettext('madExcept', 'Sending attachments...');
        SendFinalizeMsg       := dgettext('madExcept', 'Finalizing...');
        SendFailureMsg        := dgettext('madExcept', 'Sorry, sending the bug report didn''t work.');
      {$ENDIF GetText}
      { Files }
      sModule := GetModuleFileName();
      BugReportFile := ChangeFileExt(sModule, '_bugreport.txt');
      sModule := ChangeFileExt(ExtractFileName(sModule), '');
      BugReportSendAs := sModule + '_bugreport.txt';
      ScreenShotSendAs := sModule + '_screenshot.png';
//    BugReportZip := sModule + '_bugreport.zip';
      {-}
      {$IFDEF DEBUG}
        AutoShowBugReport := True;
        if  IsInsideDelphi()  then
          PauseFreezeCheck();
      {$ENDIF DEBUG}
    end { with MESettings() };
  {-}
  {$IF CompilerVersion >= 20}   // Delphi 2009
    Exception.GetExceptionStackInfoProc := GetExceptionStackInfoProc;
    Exception.GetStackInfoStringProc := GetStackInfoStringProc;
    Exception.CleanUpStackInfoProc := CleanUpStackInfoProc
  {$IFEND}
end { InitializeMadExcept };


procedure  FinalizeMadExcept();
begin
  {$IF CompilerVersion >= 20}   // Delphi 2009
    Exception.GetExceptionStackInfoProc := nil;
    Exception.GetStackInfoStringProc := nil;
    Exception.CleanUpStackInfoProc := nil
  {$IFEND}
end { FinalizeMadExcept };


initialization
  InitializeMadExcept();


finalization
  FinalizeMadExcept();


end.
