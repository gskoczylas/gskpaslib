﻿{: @exclude
   Wewnętrznych modułów biblioteki nie komentuję (na razie).

   @abstract Definiuje komponenty biblioteki w palecie komponentów Delphi.

   Oprócz definiowania komponentów definiuje również edytory niektórych
   właściwości oraz edytory niektórych komponentów.

   Przypisuje właściwości komponentów zdefiniowane w bibliotece do specjalnej
   kategorii.  }
unit  GSkPasLib.PasLibReg;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$R GSkPasLib.CustomComponents.dcr }
{$R GSkPasLib.Log.dcr }
{$R GSkPasLib.Tokens.dcr }
{$R GSkPasLib.PrevInst.dcr }
{$R GSkPasLib.DataEmbedded.dcr }
{$R GSkPasLib.SpeedBarCtl.dcr }
{$R GSkPasLib.ClientDataSet.dcr }


uses
  Windows, Classes, SysUtils, Forms, DesignIntf;


procedure  Register();

procedure  ShowDesingError(const   eErr:  Exception);


implementation


uses
  GSkPasLib.PasLibInternals, GSkPasLib.CustomComponents, GSkPasLib.PropEdAbout,
  GSkPasLib.CompEditCustomDialog, GSkPasLib.Log, GSkPasLib.LogFile, GSkPasLib.Tokens,
  GSkPasLib.PrevInst, GSkPasLib.DataEmbedded, GSkPasLib.CompEdDataEmbedded,
  GSkPasLib.PropEdDataEmbedded, GSkPasLib.Dialogs, GSkPasLib.SpeedBarCtl, GSkPasLib.ClientDataSet;


procedure  Register();
begin
  { Components }
{-
  RegisterNoIcon([TGSkCustomComponent,
                  TGSkCustomDialog, TGSkCustomDialogFB, TGSkCustomDialogFI, TGSkCustomDialogP,
                  TGSkCustomLogAdapter, TGSkCustomTokens]);
-}
  RegisterComponents(gcsLibName,
                     [TGSkLog, TGSkLogFile,
                      TGSkCharTokens, TGSkStringTokens,
                      TGSkPrevInst, TGSkDataEmbedded,
                      TGSkSpeedBarCtl, TGSkClientDataSet]);
  { Properties }
  RegisterPropertyInCategory(gcsLibName,
                             TypeInfo(TGSkPasLibAboutProperty));
  RegisterPropertiesInCategory(gcsLibName, TGSkLog,
                               ['Active', 'Prefix', 'Offset', 'UpdateLevel',
                                'LogMessageTime', 'ShowMessageTypes',
                                'BeforeMessage', 'AfterMessage']);
  RegisterPropertiesInCategory(gcsLibName, TGSkCustomLogAdapter,
                               ['LogName']);
  RegisterPropertiesInCategory(gcsLibName, TGSkLogFile,
                               ['FileName', 'FileSizeUnits', 'FileSizeLimit',
                                'NewFileCond', 'Flags']);
  RegisterPropertiesInCategory(gcsLibName, TGSkCustomTokens,
                               ['Text', 'AllowEmptyTokens']);
  RegisterPropertiesInCategory(gcsLibName, TGSkStringTokens,
                               ['CaseSensitive']);
  RegisterPropertiesInCategory(gcsLibName, TGSkPrevInst,
                               ['InstancesLimit', 'OnLimitExceded',
                                'LimitExceededAction']);
  RegisterPropertiesInCategory(gcsLibName, TGSkDataEmbedded,
                               ['Size', 'Data']);
  RegisterPropertyInCategory(gcsLibName, TGSkSpeedBarCtl, 'Speedbar');
  RegisterPropertyInCategory(gcsLibName, TGSkClientDataSet, 'ContextLevel');
  { Property editors }
  RegisterPropertyEditor(TypeInfo(TGSkPasLibAboutProperty), nil,
                         'About', TPropEdAboutGSkPasLib);
  RegisterPropertyEditor(TypeInfo(TPropEdDataEmbedded), TGSkDataEmbedded,
                         'Data', TPropEdDataEmbedded);
  { Component editors }
  RegisterComponentEditor(TGSkCustomDialogFB, TCompEdCustomDialog);
  RegisterComponentEditor(TGSkCustomDialogFI, TCompEdCustomDialog);
  RegisterComponentEditor(TGSkCustomDialogP, TCompEdCustomDialog);
  RegisterComponentEditor(TGSkDataEmbedded, TCompEdDataEmbedded)
end { Register };


procedure  ShowDesingError(const   eErr:  Exception);
begin
  if  not (eErr is EAbort)  then
    begin
      SetForegroundWindow(Application.Handle);
      BringWindowToTop(Application.Handle);
      MsgDlg(eErr.ClassName + ': '#13 + eErr.Message,
             mtError)
    end { eErr <> EAbort }
end { ShowDesingError };


end.
