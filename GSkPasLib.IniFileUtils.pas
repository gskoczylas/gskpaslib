﻿{: @abstract(Moduł @name zawiera podprogramy obsługi plików INI.)

   Podprogramy z tego modułu są zaprojektowane do odczytywania i zapisywania
   pojedynczych wartości z plików INI.

   We wszystkich podprogramach modułu @name ostatni parametr jest opcjonalny.
   Wskazuje on nazwę pliku INI. Jeżeli zostanie pominięty lub jest pustym napisem,
   to używany jest plik o nazwie i ścieżce takiej samej, jak plik aplikacji (EXE,
   DLL lub OCX), ale o rozszerzeniu „.ini”.  }
unit  GSkPasLib.IniFileUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    System.Classes, System.SysUtils, System.IniFiles;
  {$ELSE}
    Classes, SysUtils, IniFiles;
  {$IFEND}


function  ReadString(const   sSection, sName:  string;
                     const   sDefault:  string = '';
                     const   sFileName:  string = '')
                     : string;

function  ReadInteger(const   sSection, sName:  string;
                      const   nDefault:  LongInt = 0;
                      const   sFileName:  string = '')
                      : LongInt;

function  ReadFloat(const   sSection, sName:  string;
                    const   nDefault:  Double = 0.0;
                    const   sFileName:  string = '')
                    : Double;

function  ReadCurrency(const   sSection, sName:  string;
                       const   nDefault:  Currency = 0.0;
                       const   sFileName:  string = '')
                       : Currency;

function  ReadBool(const   sSection, sName:  string;
                   const   bDefault:  Boolean = False;
                   const   sFileName:  string = '')
                   : Boolean;

function  ReadDate(const   sSection, sName:  string;
                   const   tmDefault:  TDateTime = 0;
                   const   sFileName:  string = '')
                   : TDateTime;

function  ReadDateTime(const   sSection, sName:  string;
                       const   tmDefault:  TDateTime = 0;
                       const   sFileName:  string = '')
                       : TDateTime;

function  ReadTime(const   sSection, sName:  string;
                   const   tmDefault:  TDateTime = 0;
                   const   sFileName:  string = '')
                   : TDateTime;

function  SectionExists(const   sSection:  string;
                        const   sFileName:  string = '')
                        : Boolean;

procedure  ReadSection(const   sSection:  string;
                       const   Strings:  TStrings;
                       const   sFileName:  string = '');

procedure  ReadSections(const   Strings:  TStrings;
                        const   sFileName:  string = '')             overload;

procedure  ReadSections(const   sSection:  string;
                        const   Strings:  TStrings;
                        const   sFileName:  string = '');            overload;

procedure  ReadSectionValues(const   sSection:  string;
                             const   Strings:  TStrings;
                             const   sFileName:  string = '');

procedure  WriteString(const   sSection, sName, sValue:  string;
                       const   sFileName:  string = '');

procedure  WriteInteger(const   sSection, sName:  string;
                        const   nValue:  LongInt;
                        const   sFileName:  string = '');

procedure  WriteFloat(const   sSection, sName:  string;
                      const   nValue:  Double;
                      const   sFileName:  string = '');

procedure  WriteCurrency(const   sSection, sName:  string;
                         const   nValue:  Currency;
                         const   sFileName:  string = '');

procedure  WriteBool(const   sSection, sName:  string;
                     const   bValue:  Boolean;
                     const   sFileName:  string = '');

procedure  WriteDate(const   sSection, sName:  string;
                     const   tmValue:  TDateTime;
                     const   sFileName:  string = '');

procedure  WriteDateTime(const   sSection, sName:  string;
                         const   tmValue:  TDateTime;
                         const   sFileName:  string = '');

procedure  WriteTime(const   sSection, sName:  string;
                     const   tmValue:  TDateTime;
                     const   sFileName:  string = '');

procedure  EraseSection(const   sSection:  string;
                        const   sFileName:  string = '');

procedure  DeleteKey(const   sSection, sName:  string;
                     const   sFileName:  string = '');


implementation


uses
  GSkPasLib.FileUtils, GSkPasLib.Classes;


type
  IIni = interface ['{12AF2CFD-1415-4B44-A5F4-9F84F1FB1015}']
    function  GetIni() : TIniFile;
    property  ini:  TIniFile
              read  GetIni;
  end { IIni };

  TIni = class(TInterfacedObject, IIni)
  private
    var
      fIni:  TIniFile;
    function  GetIni() : TIniFile;
  public
    constructor  Create({const}   sFileName:  string);
    destructor  Destroy();                                           override;
  end { TIni };


function  Ini(const   sFileName:  string)
              : IIni;
begin
  Result := TIni.Create(sFileName) as IIni
end { Ini };


{$REGION 'TIni'}

constructor  TIni.Create({const}   sFileName:  string);
begin
  inherited Create();
  if  sFileName = ''  then
    sFileName := ChangeFileExt(GetModuleFileName(), '.ini');
  fIni := TIniFile.Create(sFileName)
end { Create };


destructor  TIni.Destroy();
begin
  fIni.Free();
  inherited
end { Destroy };


function  TIni.GetIni() : TIniFile;
begin
  Result := fIni
end { GetIni };

{$ENDREGION 'TIni'}


{$REGION 'Global subroutines'}

function  ReadString(const   sSection, sName, sDefault, sFileName:  string)
                     : string;
begin
  Result := Ini(sFileName).ini.ReadString(sSection, sName, sDefault)
end { ReadString };


function  ReadInteger(const   sSection, sName:  string;
                      const   nDefault:  LongInt;
                      const   sFileName:  string)
                      : LongInt;
begin
  Result := Ini(sFileName).ini.ReadInteger(sSection, sName, nDefault)
end { ReadInteger };


function  ReadFloat(const   sSection, sName:  string;
                    const   nDefault:  Double;
                    const   sFileName:  string)
                    : Double;
begin
  Result := Ini(sFileName).ini.ReadFloat(sSection, sName, nDefault)
end { ReadFloat };


function  ReadCurrency(const   sSection, sName:  string;
                       const   nDefault:  Currency;
                       const   sFileName:  string)
                       : Currency;
begin
  Result := Ini(sFileName).ini.ReadCurrency(sSection, sName, nDefault)
end { ReadCurrency };


function  ReadBool(const   sSection, sName:  string;
                   const   bDefault:  Boolean;
                   const   sFileName:  string)
                   : Boolean;
begin
  Result := Ini(sFileName).ini.ReadBool(sSection, sName, bDefault)
end { ReadBool };


function  ReadDate(const   sSection, sName:  string;
                   const   tmDefault:  TDateTime;
                   const   sFileName:  string)
                   : TDateTime;
begin
  Result := Ini(sFileName).ini.ReadDate(sSection, sName, tmDefault)
end { ReadDate };


function  ReadDateTime(const   sSection, sName:  string;
                       const   tmDefault:  TDateTime;
                       const   sFileName:  string)
                       : TDateTime;
begin
  Result := Ini(sFileName).ini.ReadDateTime(sSection, sName, tmDefault)
end { ReadDateTime };


function  ReadTime(const   sSection, sName:  string;
                   const   tmDefault:  TDateTime;
                   const   sFileName:  string)
                   : TDateTime;
begin
  Result := Ini(sFileName).ini.ReadTime(sSection, sName, tmDefault)
end { ReadTime };


function  SectionExists(const   sSection, sFileName:  string)
                        : Boolean;
begin
  Result := Ini(sFileName).ini.SectionExists(sSection)
end { SectionExists };


procedure  ReadSection(const   sSection:  string;
                       const   Strings:  TStrings;
                       const   sFileName:  string);
begin
  Ini(sFileName).ini.ReadSection(sSection, Strings)
end { ReadSection };


procedure  ReadSections(const   Strings:  TStrings;
                        const   sFileName:  string);
begin
  Ini(sFileName).ini.ReadSections(Strings)
end { ReadSections };


procedure  ReadSections(const   sSection:  string;
                        const   Strings:  TStrings;
                        const   sFileName:  string);
begin
  Ini(sFileName).ini.ReadSections(sSection, Strings)
end { ReadSections };


procedure  ReadSectionValues(const   sSection:  string;
                             const   Strings:  TStrings;
                             const   sFileName:  string);
begin
  Ini(sFileName).ini.ReadSectionValues(sSection, Strings)
end { ReadSectionValues };


procedure  WriteString(const   sSection, sName, sValue, sFileName:  string);
begin
  Ini(sFileName).ini.WriteString(sSection, sName, sValue)
end { WriteString };


procedure  WriteInteger(const   sSection, sName:  string;
                        const   nValue:  LongInt;
                        const   sFileName:  string);
begin
  Ini(sFileName).ini.WriteInteger(sSection, sName, nValue)
end { WriteInteger };


procedure  WriteFloat(const   sSection, sName:  string;
                      const   nValue:  Double;
                      const   sFileName:  string);
begin
  Ini(sFileName).ini.WriteFloat(sSection, sName, nValue)
end { WriteFloat };


procedure  WriteCurrency(const   sSection, sName:  string;
                         const   nValue:  Currency;
                         const   sFileName:  string);
begin
  Ini(sFileName).ini.WriteCurrency(sSection, sName, nValue)
end { WriteCurrency };


procedure  WriteBool(const   sSection, sName:  string;
                     const   bValue:  Boolean;
                     const   sFileName:  string);
begin
  Ini(sFileName).ini.WriteBool(sSection, sName, bValue)
end { WriteBool };


procedure  WriteDate(const   sSection, sName:  string;
                     const   tmValue:  TDateTime;
                     const   sFileName:  string);
begin
  Ini(sFileName).ini.WriteDate(sSection, sName, tmValue)
end { WriteDate };


procedure  WriteDateTime(const   sSection, sName:  string;
                         const   tmValue:  TDateTime;
                         const   sFileName:  string);
begin
  Ini(sFileName).ini.WriteDateTime(sSection, sName, tmValue)
end { WriteDateTime };


procedure  WriteTime(const   sSection, sName:  string;
                     const   tmValue:  TDateTime;
                     const   sFileName:  string);
begin
  Ini(sFileName).ini.WriteTime(sSection, sName, tmValue)
end { WriteTime };


procedure  EraseSection(const   sSection:  string;
                        const   sFileName:  string);
begin
  Ini(sFileName).ini.EraseSection(sSection)
end { EraseSection };


procedure  DeleteKey(const   sSection, sName:  string;
                     const   sFileName:  string);
begin
  Ini(sFileName).ini.DeleteKey(sSection, sName)
end { DeleteKey };

{$ENDREGION 'Global subroutines'}


end.

