﻿{: @abstract Funckje uniwersalne, niezależne od typu. }
unit  GSkPasLib.Generics;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    System.SysUtils, System.StrUtils, System.TypInfo, System.RTTI,
    System.Generics.Collections, System.Generics.Defaults;
  {$ELSE}
    SysUtils,
    Generics.Defaults;
  {$IFEND}


type
  {: @abstract Definicje uniwersalnych funkcji, niezależnych od typu.

     Klasa @name zawiera definicje funkcji, które można znaleźć w takich
     standardowych modułach jak @code(SysUtils), @code(DateUtils), @code(Math)
     i innych. Wersje funckji zdefiniowane w klasie @name są uniwersalne,
     niezależne od typu danych. }
  TGSkGenerics = class sealed
  public
    {: @abstract: W zależności od warunku zwraca jedną z wartości.

       Jeżeli @code(bCond) jest równe @true, to wynikiem funkcji jest wartość
       @code(ValTrue). W przeciwnym razie wynikiem funkcji jest wartość @code(ValFalse). }
    class function  IfThen<T>(const   bCond:  Boolean;
                              const   ValTrue, ValFalse:  T)
                              : T;                                   overload; static;

    {: @abstract Gwarantuje, że wartość jest we wskazanym przedziale.

       @unorderedList(
         @item(Jeżeli @code(Value) jest mniejsze niż @code(ValMin), to wynikiem
               funkcji jest @code(ValMin).)
         @item(Jeżeli @code(Value) jest większe niż @code(ValMax), to wynikiem
               funkcji jest @code(ValMax).)
         @item(W przeciwnym razie wynikiem funkcji jest @code(Value).)) }
    class function  EnsureRange<T>(const   Value, ValMin, ValMax:  T)
                                   : T;                              overload; static;
    class function  EnsureRange<T>(const   Value, ValMin, ValMax:  T;
                                   const   iComparer:  IComparer<T>)
                                   : T;                              overload; static;

    {: @abstract Sprawdza, czy wartość jest we wskazanym przedziale wartości. }
    class function  InRange<T>(const   Value, ValMin, ValMax:  T)
                               : Boolean;                            overload; static;
    class function  InRange<T>(const   Value, ValMin, ValMax:  T;
                               const   iComparer:  IComparer<T>)
                               : Boolean;                            overload; static;

    {: @abstract Zwraca mniejszą z dwóch wartości. }
    class function  Min<T>(const   Val1, Val2:  T)
                           : T;                                      overload; static;
    class function  Min<T>(const   Val1, Val2:  T;
                           const   iComparer:  IComparer<T>)
                           : T;                                      overload; static;

    {: @abstract Zwraca większą z dwóch wartości. }
    class function  Max<T>(const   Val1, Val2:  T)
                           : T;                                      overload; static;
    class function  Max<T>(const   Val1, Val2:  T;
                           const   iComparer:  IComparer<T>)
                           : T;                                      overload; static;

    {: @abstract Zwraca pierwszą niestandardową wartość.

       @returns(Funkcja @name zwraca pierwszą niestandardową wartość. W zależności
                od typu, jest to pierwsza wartość różna od zera, pierwszy niepusty
                napis, pierwszy wskaźnik różny od @nil, itp.

              Jeżeli wszystkie wartości są wartościami standardowymi, to wynikiem
              funkcji jest wartość standardowa, czyli odpowiednio zero lub pusty
              napis, lub @nil, itp. }
    class function  Coalesce<T>(const   Values:  array of T)
                                : T;                                 overload; static;
    class function  Coalesce<T>(const   Values:  array of T;
                                const   iComparer:  IEqualityComparer<T>)
                                : T;                                 overload; static;

    class function  IndexOf<T>(const   Values:  array of T;
                               const   Item:  T)
                               : Integer;                            overload; static;
    class function  IndexOf<T>(const   Values:  array of T;
                               const   Item:  T;
                               const   iComparer:  IEqualityComparer<T>)
                               : Integer;                            overload; static;

    class function  Contains<T>(const   Values:  array of T;
                                const   Item:  T)
                                : Boolean;                           overload; static;
    class function  Contains<T>(const   Values:  array of T;
                                const   Item:  T;
                                const   iComparer:  IEqualityComparer<T>)
                                : Boolean;                           overload; static;

    {: @abstract Zwraca nazwę elementu typu wyliczeniowego.

       Funkcja @name jest trochę prostszą wersją standardowej funkcji
       @code(GetEnumName).

       @param(Item Element typu wyliczeniowego @code(T).)
       @param(sRemovePrefix Jeżeli to nie jest pusty napis, to wskazany przedrostek
                            jest usuwany z nazw elementów zbioru.)

       @return(Wynikiem funkcji jest nazwa wskazanego elementu typu wyliczeniowego.)

       @seeAlso(SetToStr) }
    class function  GetEnumItemName<T>(const   Item:  T;
                                       const   sRemovePrefix:  string = '')
                                       : string;                     static;

    {: @abstract Konwersja zbioru na napis.

       @param(aSet Dowolna wartość zbiorowa.)
       @param(sRemovePrefix Jeżeli to nie jest pusty napis, to wskazany przedrostek
                            jest usuwany z nazw elementów zbioru.)

       Przykład:  @longCode(#
         type
           TDeferredOption = (doUserIntf, doClosingDay);
           TDeferredOptions = set of  TDeferredOption;

         begin
           writeln(G.SetToStr<TDeferredOptions>([])); // 1
           writeln(G.SetToStr<TDeferredOptions>([doUserIntf, doClosingDay])); // 2
           writeln(G.SetToStr<TDeferredOptions>([doUserIntf, doClosingDay], 'do')); // 3
       #)

       W powyższym przykładzie pierwsze wywołanie zwróci napis @code('[]'),
       drugie wywołanie — napis @code('[doUserIntf, doClosingDay]'),
       a trzecie wywołanie — napis @code('[UserIntf, ClosingDay]').

       @seeAlso(GetEnumItemName) }
    class function  SetToStr<T>(const   aSet:  T;
                                const   sRemovePrefix:  string = '')
                                : string;

    {: @abstract Zamienia wartości wskazanych zmiennych. }
    class procedure  Swap<T>(var   Data1, Data2:  T);                static;

  end { TGSkGenerics };

  {: @abstract Skrócona nazwa (alias) klasy @link(TGSkGenerics).

     Zamiast pisać na przykład @longCode(#
       lLev := TGSkGenerics.Min<TGSkLogMessageType>(lmInformation, LogObj().LogFilter); #)
     można to samo napisać nieco krócej @longCode(#
       lLev := G.Min<TGSkLogMessageType>(lmInformation, LogObj().LogFilter); #) }
  G = TGSkGenerics;   // shortcut for TGSkGenerics

  TGSkList<T> = class(TList<T>)
  public
    function  Contains(const   IsFound:  TFunc<T, Boolean>)
                       : Boolean;                                    overload;
    function  IndexOf(const   IsFound:  TFunc<T, Boolean>)
                      : Integer;                                     overload;
    function  Find(const   IsFound:  TFunc<T, Boolean>)
                   : T;                                              overload;
    function  Find(const  IsFound:  TFunc<T, Boolean>;
                   const   Default:  T)
                   : T;                                              overload;
    function  TryFind(const   IsFound:  TFunc<T, Boolean>;
                      out   Found:  T)
                      : Boolean;
  end { TGSkList };

  {: @abstract Intefejs listy elementów.

     @name definiuje intefejs listy dowolnych elementów.

     @seeAlso(TGSkListImpl) :}
  IGSkListIntf<T> = interface
               ['{E8515FE9-CB02-4105-BAC6-E79294FFE97D}']
   {$REGION 'private'}
    function  GetCount() : Integer;
    function  GetItem(const   nIndex:  Integer)
                      : T;
   {$ENDREGION 'private'}
   function  Add(const   Value:  T)
                 : Integer;
   function  IndexOf(const   Value:  T)
                     : Integer;
   function  Contains(const   Value:  T)
                      : Boolean;
   {-}
   property  Count:  Integer
             read  GetCount;
   property  Items[const   nIndex:  Integer] : T
             read  GetItem;
  end { IGSkListIntf };

  {: @abstract Implementacja intefejsu listy elementów.

     Klasa @name implementuje interfejs @link(IGSkListIntf<T>).

     @seeAlso(IGSkListIntf<T>) :}
  TGSkListImpl<T> = class(TInterfacedObject, IGSkListIntf<T>)
  strict private
    var
      fList:  TList<T>;
    { IGSkListIntf }
    function  GetCount() : Integer;
    function  GetItem(const   nIndex:  Integer)
                      : T;
  public
    constructor  Create();
    destructor  Destroy();                                           override;
    { IGSkListIntf }
   function  Add(const   Value:  T)
                 : Integer;
   function  IndexOf(const   Value:  T)
                     : Integer;
   function  Contains(const   Value:  T)
                      : Boolean;
  end { TGSkListImpl };

implementation


{$REGION 'TGSkGenerics'}

class function  TGSkGenerics.Coalesce<T>(const   Values:  array of T)
                                         : T;
begin
  Result := Coalesce<T>(Values, TEqualityComparer<T>.Default);
end { Coalesce };


class function  TGSkGenerics.Coalesce<T>(const   Values:  array of T;
                                         const   iComparer:  IEqualityComparer<T>)
                                         : T;
var
  nInd:  Integer;

begin  { Coalesce<T> }
  for  nInd := Low(Values)  to  High(Values)  do
    if  not iComparer.Equals(Values[nInd], Default(T))  then
      Exit(Values[nInd]);
  Result := Default(T)
end { Coalesce<T> };


class function  TGSkGenerics.Contains<T>(const   Values:  array of T;
                                         const   Item:  T;
                                         const   iComparer:  IEqualityComparer<T>)
                                         : Boolean;
begin
  Result := IndexOf<T>(Values, Item, iComparer) >= 0
end { Contains };


class function  TGSkGenerics.Contains<T>(const   Values:  array of T;
                                         const   Item:  T)
                                         : Boolean;
begin
  Result := IndexOf<T>(Values, Item) >= 0
end { Contains };


class function  TGSkGenerics.EnsureRange<T>(const   Value, ValMin, ValMax:  T;
                                            const   iComparer:  IComparer<T>): T;
begin
  Assert(iComparer.Compare(ValMin, ValMax) < 0);
  Result := Max<T>(Min<T>(Value, ValMax, iComparer), ValMin, iComparer)
end { EnsureRange };


class function  TGSkGenerics.GetEnumItemName<T>(const   Item:  T;
                                                const   sRemovePrefix:  string)
                                                : string;
var
  nItem:  Integer;

begin  { GetEnumItemName }
  if  PTypeInfo(TypeInfo(T))^.Kind = tkEnumeration  then
    begin
      nItem := Ord(TValue.From<T>(Item).AsOrdinal);
      Result := GetEnumName(TypeInfo(T), nItem);
      {-}
      if  (sRemovePrefix <> '')
          and (sRemovePrefix <> Result)
          and StartsStr(sRemovePrefix, Result)  then
        Delete(Result, 1, Length(sRemovePrefix))
    end { tkEnumeration }
  else
    raise  EArgumentException.Create('T must be an enumerated type')
end { GetEnumItemName<T> };


class function  TGSkGenerics.EnsureRange<T>(const   Value, ValMin, ValMax:  T)
                                            : T;
begin
  Result := EnsureRange<T>(Value, ValMin, ValMax, TComparer<T>.Default)
end { EnsureRange };


class function  TGSkGenerics.IfThen<T>(const   bCond:  Boolean;
                                       const   ValTrue, ValFalse:  T)
                                       : T;
begin
  if  bCond  then
    Result := ValTrue
  else
    Result := ValFalse
end { IfThen };


class function  TGSkGenerics.IndexOf<T>(const   Values:  array of T;
                                        const   Item:  T)
                                        : Integer;
begin
  Result := IndexOf<T>(Values, Item, TEqualityComparer<T>.default)
end { IndexOf };


class function  TGSkGenerics.IndexOf<T>(const   Values:  array of T;
                                        const   Item:  T;
                                        const   iComparer:  IEqualityComparer<T>)
                                        : Integer;
begin
  Result := Length(Values);
  repeat
    Dec(Result)
  until  (Result < Low(Values))
         or iComparer.Equals(Values[Result], Item)
end { IndexOf<T> };


class function  TGSkGenerics.InRange<T>(const   Value, ValMin, ValMax:  T;
                                        const   iComparer:  IComparer<T>)
                                        : Boolean;
begin
  Result := (iComparer.Compare(ValMin, Value) <= 0)
            and (iComparer.Compare(Value, ValMax) <= 0)
end { InRange };


class function  TGSkGenerics.InRange<T>(const   Value, ValMin, ValMax:  T)
                                        : Boolean;
begin
  Result := InRange<T>(Value, ValMin, ValMax, TComparer<T>.Default)
end { InRange };


class function  TGSkGenerics.Max<T>(const   Val1, Val2:  T;
                                    const   iComparer:  IComparer<T>)
                                    : T;
begin
  if  iComparer.Compare(Val1, Val2) > 0  then
    Result := Val1
  else
    Result := Val2
end { Max };


class function  TGSkGenerics.Max<T>(const   Val1, Val2:  T)
                                    : T;
begin
  Result := Max<T>(Val1, Val2, TComparer<T>.Default)
end { Max };


class function  TGSkGenerics.Min<T>(const   Val1, Val2:  T;
                                    const   iComparer:  IComparer<T>)
                                    : T;
begin
  if  iComparer.Compare(Val1, Val2) < 0  then
    Result := Val1
  else
    Result := Val2
end { Min };


class function  TGSkGenerics.Min<T>(const   Val1, Val2:  T)
                                    : T;
begin
  Result := Min<T>(Val1, Val2, TComparer<T>.Default)
end { Min };


class function  TGSkGenerics.SetToStr<T>(const   aSet:  T;
                                         const   sRemovePrefix:  string)
                                         : string;
var
  pInfo:  PTypeInfo;
  pData:  PTypeData;
  nInd:   Integer;
  pBytes: PByte;
  sItem:  string;

begin  { LogSet<T> }
  pInfo := TypeInfo(T);
  if  pInfo^.Kind = tkSet  then
    begin
      Result := '';
      pData := GetTypeData(pInfo);
      if  pData^.CompType = nil  then   // unable to continue
        Exit('[set-of-' + GetTypeName(pInfo) + ']');
      pInfo := pData^.CompType^;    // tkSet --> tkEnumeration
      pData := GetTypeData(pInfo);
      pBytes := PByte(@aSet);
      for  nInd := pData^.MinValue  to  pData^.MaxValue  do
        if  (pBytes[nInd div 8] and (1 shl (nInd mod 8))) <> 0  then
          begin
            if  Result = ''  then
              Result := '['
            else
              Result := Result + ', ';
            sItem := GetEnumName(pInfo, nInd);
            if  (sRemovePrefix <> '')
                and (sRemovePrefix <> sItem)
                and StartsStr(sRemovePrefix, sItem)  then
              Delete(sItem, 1, Length(sRemovePrefix));
            Result := Result + sItem
          end ;
      if  Result = ''  then
        Result := '[]'
      else
        Result := Result + ']'
    end { pInfo^.Kind = tkSet }
  else
    raise  EArgumentException.Create('T must be a set type');
end { SetToStr<T> };


class procedure  TGSkGenerics.Swap<T>(var   Data1, Data2:  T);

var
  Tmp:  T;

begin  { Swap }
  Tmp := Data1;
  Data1 := Data2;
  Data2 := Tmp
end { Swap };

{$ENDREGION 'TGSkGenerics'}


{$REGION 'TGSkList<T>'}

function  TGSkList<T>.Contains(const   IsFound:  TFunc<T, Boolean>)
                               : Boolean;
var
  lItem:  T;

begin  { TGSkList<T> }
  for  lItem in Self  do
    if  IsFound(lItem)  then
      Exit(True);
  Result := False
end { TGSkList<T> };


function  TGSkList<T>.Find(const   IsFound:  TFunc<T, Boolean>)
                           : T;
begin
  if  not TryFind(IsFound, Result)  then
    raise EListError.Create('Item not found');
end { Find<T> };


function  TGSkList<T>.Find(const   IsFound:  TFunc<T, Boolean>;
                           const   Default:  T)
                           : T;
begin
  if  not TryFind(IsFound, Result)  then
    Result := Default
end { Find };


function  TGSkList<T>.IndexOf(const   IsFound:  TFunc<T, Boolean>)
                              : Integer;
begin
  Result := Count - 1;
  while  Result >= 0  do
    begin
      if  IsFound(Items[Result])  then
        Break;
      Dec(Result)
    end { while Result >= 0 }
end { IndexOf };


function  TGSkList<T>.TryFind(const   IsFound:  TFunc<T, Boolean>;
                              out     Found:  T)
                              : Boolean;
var
  lFnd:  T;

begin  { TryFind }
  for  lFnd in Self  do
    if  IsFound(lFnd)  then
      begin
        Found := lFnd;
        Result := True;
        Exit
      end { IsFound(...) };
  Result := False
end { TryFind };

{$ENDREGION 'TGSkList<T>'}


{$REGION 'TGSkListImpl<T>'}

function  TGSkListImpl<T>.Add(const   Value:  T)
                              : Integer;
begin
  Result := fList.Add(Value)
end { Add };


function  TGSkListImpl<T>.Contains(const   Value:  T)
                                   : Boolean;
begin
  Result := fList.IndexOf(Value) >= 0
end { Contains };


constructor  TGSkListImpl<T>.Create();
begin
  inherited Create();
  fList := TList<T>.Create()
end { Create };


destructor  TGSkListImpl<T>.Destroy();
begin
  fList.Free();
  inherited
end { Destroy };


function  TGSkListImpl<T>.GetCount() : Integer;
begin
  Result := fList.Count
end { GetCount };


function  TGSkListImpl<T>.GetItem(const   nIndex:  Integer)
                                  : T;
begin
  Result := fList.Items[nIndex]
end { GetItem };


function  TGSkListImpl<T>.IndexOf(const   Value:  T)
                                  : Integer;
begin
  Result := fList.IndexOf(Value)
end { IndexOf };

{$ENDREGION 'TGSkListImpl<T>'}


end.
