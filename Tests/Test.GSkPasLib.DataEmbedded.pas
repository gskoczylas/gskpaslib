﻿unit  Test.GSkPasLib.DataEmbedded;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}


uses
  Classes, Forms, SysUtils,
  GSkPasLib.DataEmbedded, TestFrameWork;


type
  TDataEmbedded_Test = class(TTestCase)
  published
    procedure  VerifyDataEmbedded();
  end { TDataEmbedded_Test };


implementation


{ TDataEmbedded_Test }

procedure  TDataEmbedded_Test.VerifyDataEmbedded();

var
  fData:  TFileStream;

begin  { VerifyDataEmbedded }
  with  TGSkDataEmbedded.Create(nil)  do
    try
      Check(Size = 0,
            'Po utworzniu „size” ma być wyzerowane');
      fData := TFileStream.Create(Application.ExeName,
                                  fmOpenRead or fmShareDenyWrite);
      try
        Data := fData;
        Check(Size = fData.Size,
              'Wielkość zagnieżdżonych danych inna niż wielkość ładowanego pliku');
        Data := nil;
        Check(Size = 0,
              'Po przypisaniu „Data := nil” powinien wyzerować „size”');
      finally
        fData.Free()   // fData
      end { try-finally }
    finally
      Free()     // GSkDataEmbedded
    end { try-finally }
end { VerifyDataEmbedded };


initialization
  RegisterTest(TDataEmbedded_Test.Suite())


end.
