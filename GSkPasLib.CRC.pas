﻿{: @abstract Obliczanie sumy kontrolnej CRC16 lub CRC32)

   Moduł zawiera definicję klas oraz wygodne samodzielne funkcje do obliczania
   sum kontrolnych @link(TGSkCRC16 CRC16) lub @link(TGSkCRC16 CRC32).

   W bardziej złożonych przypadkach wydajniejsze jest korzystanie z klas.
   Natomiast w prostych przypadkach wygodniejsze jest korzystanie z dostępnych
   w tym module podprogramów.  }
unit  GSkPasLib.CRC;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


(*   Usage schema:
 +   -------------
 +   var   CRC:  TGSkCRC;
 +         Buffer:  array[1..4096] of byte;
 +   begin { example }
 +     CRC := TGSkCRC.Create;
 +     try
 +       { First file }
 +       while  not EOF(InpFile1)  do
 +         begin
 +          nBufSize := SizeOf(Buffer);
 +            nBufSize := BlockRead(InpFile1, Buffer, nBufSize);
 +           CRC.CalculateCRC(nCRC, @Buffer, nBufSize);
 +         end { while };
 +       WriteLn('1st file CRC=', CRC.CRC);
 +       { Second file }
 +       CRC.Reset;
 +       while  not EOF(InpFile2)  do
 +         begin
 +           nBufSize := SizeOf(Buffer);
 +           nBufSize := BlockRead(InpFile2, Buffer, nBufSize);
 +           CRC.CalculateCRC(nCRC, @Buffer, nBufSize);
 +         end { while };
 +       WriteLn('2nd file CRC=', CRC.CRC);
 +     finally
 +       CRC.Free;
 +     end { try-finally }
 +   end { excample };
*)


interface


uses
  {$IF CompilerVersion >= 23}
    System.SysUtils, System.SyncObjs, System.Classes;
  {$ELSE}
    SysUtils, SyncObjs, Classes;
  {$IFEND}


type
  {: @abstract(Klasa do obliczeń CRC16)

     W bardziej złożonych przypadkach wydajniejsze jest korzystanie z klas.
     Natomiast w prostych przypadkach wygodniejsze jest korzystanie z dostępnych
     w tym module podprogramów @link(CalcCRC16).

     @bold(Uwaga!) Ta klasa zwraca wyniki różne od typowych sum kontrolnych CRC16.
     Jednak została już wykorzystana w kilku aplikacjach. Dlatego musi zostać
     taka, jaka jest.

     @seeAlso(TGSkCRC32)  }
  TGSkCRC16 = class(TObject)
    private
      nCRC:  Word;
      procedure  BuildCRCTable();
      function  GetCRC() : Word;
    public
      {: @exclude }
      constructor  Create();
      {: @exclude }
      destructor  Destroy();                                         override;

      {: @abstract(Resetuje obliczenia sumy kontrolnej)

         Każde wywołanie metody @link(CalculateCRC) uzupełnia obliczenia sumy
         kontrolnej. Po wywołaniu metody @name suma kontrolna będzie obliczana
         od nowa.  }
      procedure  Reset();

      {: @abstract(Kontynuuje obliczanie sumy kontrolnej)

         Procedura @code(CalculateCRC) kontynuuje obliczanie sumy kontrolnej
         dla wskazanych danych.

         Obliczanie sumy kontrolnej można rozpocząć od nowa wywołując metodę
         @link(Reset).

         @param(pBuffer Adres danych.)
         @param(nBufSize Wielkość danych (w bajtach).)

         @seeAlso(CRC)  }
      procedure  CalculateCRC(const   pBuffer:  Pointer;
                                      nBufSize:  Cardinal);          overload;

      {: @abstract(Kontynuuje obliczanie sumy kontrolnej)

         Procedura @code(CalculateCRC) kontynuuje obliczanie sumy kontrolnej
         dla wskazanego napisu.

         Obliczanie sumy kontrolnej można rozpocząć od nowa wywołując metodę
         @link(Reset).

         @seeAlso(CRC)  }
      procedure  CalculateCRC(const   sText:  AnsiString);           overload;

{$IFDEF UNICODE}
      {: @abstract(Kontynuuje obliczanie sumy kontrolnej)

         Procedura @code(CalculateCRC) kontynuuje obliczanie sumy kontrolnej
         dla wskazanego napisu.

         Obliczanie sumy kontrolnej można rozpocząć od nowa wywołując metodę
         @link(Reset).

         @seeAlso(CRC)  }
      procedure  CalculateCRC(const   sText:  UnicodeString);        overload;
{$ENDIF UNICODE}

      {: @abstract(Kontynuuje obliczanie sumy kontrolnej)

         Procedura @code(CalculateCRC) kontynuuje obliczanie sumy kontrolnej
         dla wskazanej liczby.

         Obliczanie sumy kontrolnej można rozpocząć od nowa wywołując metodę
         @link(Reset).

         @seeAlso(CRC)  }
      procedure  CalculateCRC(const   nNumber:  Integer);            overload;

      {: @abstract(Kontynuuje obliczanie sumy kontrolnej)

         Procedura @code(CalculateCRC) kontynuuje obliczanie sumy kontrolnej
         dla wskazanego czasu.

         Obliczanie sumy kontrolnej można rozpocząć od nowa wywołując metodę
         @link(Reset).

         @seeAlso(CRC)  }
      procedure  CalculateCRC(const   dTime:  TDateTime);            overload;

      {: @abstract(Kontynuuje obliczanie sumy kontrolnej)

         Procedura @code(CalculateCRC) kontynuuje obliczanie sumy kontrolnej
         dla danych ze wskazanego strumienia. Pobierane są zawsze wszystkie
         dane ze strumienia. Po zakończeniu działania procedury przywracana jest
         aktualna pozycja w strumieniu.

         Obliczanie sumy kontrolnej można rozpocząć od nowa wywołując metodę
         @link(Reset).

         @seeAlso(CRC)  }
      procedure  CalculateCRC(const   Stream:  TStream);             overload;

      {: @abstract(Suma kontrolna)

         Aktualny stan sumy kontrolnej CRC, wyliczonej poprzez wywoływanie metod
         @link(CalculateCRC) po utworzeniu obiektu lub ostatnim wywołaniu metody
         @link(Reset).  }
      property  CRC:  Word
                read  GetCRC;
  end { TGSkCRC16 };


{*******************************************************************************
* Uwaga!
* Poniższa implementacja CRC32 jest błędna. Zwraca różne wyniki dla identycznych
* parametrów.
*******************************************************************************}
  {: @abstract(Klasa do obliczeń CRC32)

     W bardziej złożonych przypadkach wydajniejsze jest korzystanie z klas.
     Natomiast w prostych przypadkach wygodniejsze jest korzystanie z dostępnych
     w tym module podprogramów @link(CalcCRC32).

     @seeAlso(TGSkCRC16)  }
  TGSkCRC32 = class(TObject)
//     private
//       nCRC:  LongWord;
//       procedure  BuildCRCTable();
//       function  GetCRC() : LongWord;
//     public
//       {: @exclude }
//       constructor  Create();
//       {: @exclude }
//       destructor  Destroy();                                         override;
//
//       {: @abstract(Resetuje obliczenia sumy kontrolnej)
//
//          Każde wywołanie metody @link(CalculateCRC) uzupełnia obliczenia sumy
//          kontrolnej. Po wywołaniu metody @name suma kontrolna będzie obliczana
//          od nowa.  }
//       procedure  Reset();
//
//       {: @abstract(Kontynuuje obliczanie sumy kontrolnej)
//
//          Procedura @code(CalculateCRC) kontynuuje obliczanie sumy kontrolnej
//          dla wskazanych danych.
//
//          Obliczanie sumy kontrolnej można rozpocząć od nowa wywołując metodę
//          @link(Reset).
//
//          @param(pBuffer Adres danych.)
//          @param(nBufSize Wielkość danych (w bajtach).)
//
//          @seeAlso(CRC)  }
//       procedure  CalculateCRC(const   pBuffer:  Pointer;
//                                       nBufSize:  Cardinal);          overload;
//
//       {: @abstract(Kontynuuje obliczanie sumy kontrolnej)
//
//          Procedura @code(CalculateCRC) kontynuuje obliczanie sumy kontrolnej
//          dla wskazanego napisu.
//
//          Obliczanie sumy kontrolnej można rozpocząć od nowa wywołując metodę
//          @link(Reset).
//
//          @seeAlso(CRC)  }
//       procedure  CalculateCRC(const   sText:  string);               overload;
//
//       {: @abstract(Kontynuuje obliczanie sumy kontrolnej)
//
//          Procedura @code(CalculateCRC) kontynuuje obliczanie sumy kontrolnej
//          dla wskazanej liczby.
//
//          Obliczanie sumy kontrolnej można rozpocząć od nowa wywołując metodę
//          @link(Reset).
//
//          @seeAlso(CRC)  }
//       procedure  CalculateCRC(const   nNumber:  Integer);            overload;
//
//       {: @abstract(Kontynuuje obliczanie sumy kontrolnej)
//
//          Procedura @code(CalculateCRC) kontynuuje obliczanie sumy kontrolnej
//          dla wskazanego czasu.
//
//          Obliczanie sumy kontrolnej można rozpocząć od nowa wywołując metodę
//          @link(Reset).
//
//          @seeAlso(CRC)  }
//       procedure  CalculateCRC(const   dTime:  TDateTime);            overload;
//
//       {: @abstract(Kontynuuje obliczanie sumy kontrolnej)
//
//          Procedura @code(CalculateCRC) kontynuuje obliczanie sumy kontrolnej
//          dla danych ze wskazanego strumienia. Pobierane są zawsze wszystkie
//          dane ze strumienia. Po zakończeniu działania procedury przywracana jest
//          aktualna pozycja w strumieniu.
//
//          Obliczanie sumy kontrolnej można rozpocząć od nowa wywołując metodę
//          @link(Reset).
//
//          @seeAlso(CRC)  }
//       procedure  CalculateCRC(const   Stream:  TStream);             overload;
//
//       {: @abstract(Suma kontrolna)
//
//          Aktualny stan sumy kontrolnej CRC, wyliczonej poprzez wywoływanie metod
//          @link(CalculateCRC) po utworzeniu obiektu lub ostatnim wywołaniu metody
//          @link(Reset).  }
//       property  CRC:  LongWord
//                       read  GetCRC;
  end { TGSkCRC32 };


//  type
//    TEnumCRC16TableProc = procedure(const   nInd:   Cardinal;
//                                    const   nElem:  Word;
//                                    const   pData:  pointer);
//    TEnumCRC32TableProc = procedure(const   nInd:   Cardinal;
//                                    const   nElem:  LongWord;
//                                    const   pData:  pointer);
//
//
//  procedure  EnumCRC16Table(const   EnumProc:  TEnumCRC16TableProc;
//                            const   pData:     pointer);
//
//  procedure  EnumCRC32Table(const   EnumProc:  TEnumCRC32TableProc;
//                            const   pData:     pointer);
//


{: @abstract(Oblicza sumę kontrolną CRC16)

   Funkcja @name oblicza sumę kontrolną CRC16 dla danych wskazanych w
   parametrach.

   Jeżeli suma kontrolna ma być policzona dla wielu danych to należy użyć
   komponent @link(TGSkCRC16).

   @param pBuffer Adres danych.
   @param nBufSize Wielkość danych (w bajtach).

   @seeAlso(CalcCRC32)  }
function  CalcCRC16(const   pBuffer:  Pointer;
                    const   nBufSize:  Cardinal)
                    : Word;                                          overload;

{: @abstract(Oblicza sumę kontrolną CRC16)

   Funkcja @name oblicza sumę kontrolną CRC16 dla wskazanego napisu.

   Jeżeli suma kontrolna ma być policzona dla wielu danych to należy użyć
   komponent @link(TGSkCRC16).

   @seeAlso(CalcCRC32)  }
function  CalcCRC16(const   sText:  AnsiString)
                    : Word;                                          overload;

{$IFDEF UNICODE}

{: @abstract(Oblicza sumę kontrolną CRC16)

   Funkcja @name oblicza sumę kontrolną CRC16 dla wskazanego napisu.

   Jeżeli suma kontrolna ma być policzona dla wielu danych to należy użyć
   komponent @link(TGSkCRC16).

   @seeAlso(CalcCRC32)  }
function  CalcCRC16(const   sText:  UnicodeString)
                    : Word;                                          overload;

{$ENDIF UNICODE}

{: @abstract(Oblicza sumę kontrolną CRC16)

   Funkcja @name oblicza sumę kontrolną CRC16 dla wskazanej liczby.

   Jeżeli suma kontrolna ma być policzona dla wielu danych to należy użyć
   komponent @link(TGSkCRC16).

   @seeAlso(CalcCRC32)  }
function  CalcCRC16(const   nNumber:  Integer)
                    : Word;                                          overload;

{: @abstract(Oblicza sumę kontrolną CRC16)

   Funkcja @name oblicza sumę kontrolną CRC16 dla wskazanego czasu.

   Jeżeli suma kontrolna ma być policzona dla wielu danych to należy użyć
   komponent @link(TGSkCRC16).

   @seeAlso(CalcCRC32)  }
function  CalcCRC16(const   dTime:  TDateTime)
                    : Word;                                          overload;

{: @abstract(Oblicza sumę kontrolną CRC16)

   Funkcja @name oblicza sumę kontrolną CRC16 dla danych ze
   wskazanego strumienia. Zawsze pobierane są wszystkie dane ze strumienia.
   Po zakończeniu działania funkcji przywracana jest początkowa pozycja w
   strumieniu.

   Jeżeli suma kontrolna ma być policzona dla wielu danych to należy użyć
   komponent @link(TGSkCRC16).

   @seeAlso(CalcCRC32)  }
function  CalcCRC16(const   Stream:  TStream)
                    : Word;                                          overload;

{: @abstract(Oblicza sumę kontrolną CRC32)

   Funkcja @name oblicza sumę kontrolną CRC32 dla danych wskazanych
   w parametrach.

   Jeżeli suma kontrolna ma być policzona dla wielu danych to należy użyć
   komponent @link(TGSkCRC32).

   @param pBuffer Adres danych.
   @param nBufSize Wielkość danych (w bajtach).

   @seeAlso(CalcCRC16)  }
function  CalcCRC32(const   pBuffer:  Pointer;
                    const   nBufSize:  Cardinal)
                    : LongWord;                                      overload;

{: @abstract(Oblicza sumę kontrolną CRC32)

   Funkcja @name oblicza sumę kontrolną CRC16 dla wskazanego napisu.

   Jeżeli suma kontrolna ma być policzona dla wielu danych to należy użyć
   komponent @link(TGSkCRC16).

   @seeAlso(CalcCRC32)  }
function  CalcCRC32(const   sText:  string)
                    : LongWord;                                      overload;

{: @abstract(Oblicza sumę kontrolną CRC32)

   Funkcja @name oblicza sumę kontrolną CRC32 dla wskazanej liczby.

   Jeżeli suma kontrolna ma być policzona dla wielu danych to należy użyć
   komponent @link(TGSkCRC32).

   @seeAlso(CalcCRC16)  }
function  CalcCRC32(const   nNumber:  Integer)
                    : LongWord;                                      overload;

{: @abstract(Oblicza sumę kontrolną CRC32)

   Funkcja @name oblicza sumę kontrolną CRC32 dla wskazanego czasu.

   Jeżeli suma kontrolna ma być policzona dla wielu danych to należy użyć
   komponent @link(TGSkCRC32).

   @seeAlso(CalcCRC16)  }
function  CalcCRC32(const   dTime:  TDateTime)
                    : LongWord;                                      overload;

{: @abstract(Oblicza sumę kontrolną CRC32)

   Funkcja @name oblicza sumę kontrolną CRC32 dla danych ze
   wskazanego strumienia. Zawsze pobierane są wszystkie dane ze strumienia.
   Po zakończeniu działania funkcji przywracana jest początkowa pozycja w
   strumieniu.

   Jeżeli suma kontrolna ma być policzona dla wielu danych to należy użyć
   komponent @link(TGSkCRC32).

   @seeAlso(CalcCRC16)  }
function  CalcCRC32(const   Stream:  TStream)
                    : LongWord;                                      overload;


implementation


const
  cnCRCTableSize = 256;
  cnCRCTableMaxInd = cnCRCTableSize - 1;


var
  Synchro:  TCriticalSection;


{$REGION 'CRC 16'}

const
  cnMaskCRC16 = $FFFF;
  cnInitCRC16 = cnMaskCRC16;
  cnPolynomialCRC16 = $8408;


type
  TCRC16Table = packed array[0..cnCRCTableMaxInd] of  Word;


var
  pCRC16Table:  ^TCRC16Table;
  nCRC16UseCount:  Cardinal;


constructor  TGSkCRC16.Create();
begin
  inherited  Create;
  Synchro.Enter();
  try
    if  nCRC16UseCount = 0  then
      begin
        Assert(pCRC16Table = nil);
        New(pCRC16Table);
        BuildCRCTable
      end { nCRC16UseCount = 0 };
    Inc(nCRC16UseCount)
  finally
    Synchro.Leave()
  end { try-finally };
  Reset()
end { Create };


destructor  TGSkCRC16.Destroy();
begin
  Synchro.Enter();
  try
    Dec(nCRC16UseCount);
    if  nCRC16UseCount = 0  then
      begin
        Assert(pCRC16Table <> nil);
        Dispose(pCRC16Table);
        pCRC16Table := nil
      end { nCRC16UseCount = 0 }
  finally
    Synchro.Leave()
  end { try-finally };
  inherited  Destroy;
end { Destroy };


procedure  TGSkCRC16.BuildCRCTable();

var
  i, j, r:  Word;

begin  { BuildCRCTable }
 { This routine simply builds the coefficient table used to calculate
   16 bit CRC values throughout this program.  The 256 word table
   has to be set up once when the program starts.  Alternatively, the
   values could be hard coded in, which would offer a miniscule improvement
   in overall performance of the program. }
  Assert(pCRC16Table <> nil);
  for  i := 0  to  cnCRCTableMaxInd  do
    begin
      r := i;
      for  j := 0  to  7  do
        if  Odd(r)  then
          r := (r shr 1) xor cnPolynomialCRC16
        else
          r := r shr 1;
      pCRC16Table^[i] := r
    end { for i }
end { BuildCRCTable };


function  TGSkCRC16.GetCRC() : Word;
begin
  Result := nCRC and cnMaskCRC16
end { GetCRC };


procedure  TGSkCRC16.Reset();
begin
  nCRC := cnInitCRC16
end { Reset };


procedure  TGSkCRC16.CalculateCRC(const   pBuffer:  pointer;
                                          nBufSize:  Cardinal);
var
  pByte:  ^byte;

begin  { CalculateCRC }
 { This is the routine used to calculate the 16 bit CRC of a block of data.
   This is done by processing the input buffer using the coefficient table
   that was created when the program was initialized.  This routine takes
   an input value as a seed, so that a running calculation of the CRC can
   be used as blocks are read and written. }
  Assert(pCRC16Table <> nil);
  pByte := pBuffer;
  while  nBufSize > 0  do
    begin
      nCRC := pCRC16Table^[(nCRC xor pByte^) and $FF]
              xor (nCRC shr 8);
      Inc(pByte);
      Dec(nBufSize)
    end { while }
end { CalculateCRC };


procedure  TGSkCRC16.CalculateCRC(const   sText:  AnsiString);
begin
  CalculateCRC(PAnsiChar(sText), Length(sText) * SizeOf(AnsiChar))
end { CalculateCRC };


{$IFDEF UNICODE}

procedure  TGSkCRC16.CalculateCRC(const   sText:  UnicodeString);
begin
  CalculateCRC(PWideChar(sText), Length(sText) * SizeOf(WideChar))
end { CalculateCRC };

{$ENDIF UNICODE}


procedure  TGSkCRC16.CalculateCRC(const   nNumber:  integer);
begin
  CalculateCRC(@nNumber, SizeOf(nNumber))
end { CalculateCRC };


procedure  TGSkCRC16.CalculateCRC(const   dTime:  TDateTime);
begin
  CalculateCRC(@dTime, SizeOf(dTime))
end { CalculateCRC };


procedure  TGSkCRC16.CalculateCRC(const   Stream:  TStream);

const
  cnBufSize = 8 * 1024;

var
  Buf:  packed array[1..cnBufSize] of  Byte;
  nSavePos, nBufSize:  Integer;

begin  { CalculateCR }
  nSavePos := Stream.Position;
  try
    Stream.Position := 0;
    repeat
      nBufSize := Stream.Read(Buf, cnBufSize);
      CalculateCRC(Addr(Buf), nBufSize)
    until  Stream.Position >= Stream.Size
  finally
    Stream.Position := nSavePos
  end { try-finally }
end { CalculateCR };

{$ENDREGION 'CRC 16'}


{$REGION 'CRC 32'}

//const
//  cnMaskCRC32 = $00FFFFFF;
//  cnInitCRC32 = $FFFFFFFF;
//  cnPolynimialCRC32 = $EDB88320;


//type
//  TCRC32Table = packed array[0..cnCRCTableMaxInd] of  LongWord;


//var
//  pCRC32Table:  ^TCRC32Table;
//  nCRC32UseCount:  Cardinal;


// constructor  TGSkCRC32.Create();
// begin
//   inherited  Create;
//   Synchro.Enter();
//   try
//     if  nCRC32UseCount = 0  then
//       begin
//         Assert(pCRC32Table = nil);
//         New(pCRC32Table);
//         BuildCRCTable
//       end { nCRC32UseCount = 0 };
//     Inc(nCRC32UseCount)
//   finally
//     Synchro.Leave()
//   end { try-finally };
//   Reset()
// end { Create };


// destructor  TGSkCRC32.Destroy();
// begin
//   Synchro.Enter();
//   try
//     Dec(nCRC32UseCount);
//     if  nCRC32UseCount = 0  then
//       begin
//         Assert(pCRC32Table <> nil);
//         Dispose(pCRC32Table);
//         pCRC32Table := nil
//       end { nCRC32UseCount = 0 }
//   finally
//     Synchro.Leave()
//   end { try-finally };
//   inherited  Destroy;
// end { Destroy };


// procedure  TGSkCRC32.BuildCRCTable();
//
// var
//   i, j:  integer;
//   nVal:  LongWord;
//
// begin  { BuildCRCTable }
//   Assert(pCRC32Table <> nil);
//   for  i := 0  to  cnCRCTableMaxInd  do
//     begin
//       nVal := i;
//       for  j := 8  downto  1  do
//         if  Odd(nVal)  then
//           nVal := (nVal shr 1) xor cnPolynimialCRC32
//         else
//           nVal := nVal shr 1;
//       pCRC32Table^[i] := nVal
//     end { for i }
// end { BuildCRCTable };


// function  TGSkCRC32.GetCRC() : LongWord;
// begin
//   Result := nCRC and cnMaskCRC32
// end { GetCRC };


// procedure  TGSkCRC32.Reset();
// begin
//   nCRC := cnInitCRC32
// end { Reset };


// procedure  TGSkCRC32.CalculateCRC(const   pBuffer:  pointer;
//                                           nBufSize:  Cardinal);
// var
//   pByte:  ^byte;
//
// begin  { CalculateCRC }
//  { This is the routine used to calculate the 32 bit CRC of a block of data.
//    This is done by processing the input buffer using the coefficient table
//    that was created when the program was initialized.  This routine takes
//    an input value as a seed, so that a running calculation of the CRC can
//    be used as blocks are read and written.  }
//   Assert(pCRC32Table <> nil);
//   pByte := @pBuffer;
//   while  nBufSize > 0  do
//     begin
//       nCRC := (nCRC shr 8) xor pCRC32Table^[Byte(nCRC xor LongInt(pByte^))];
//       Inc(pByte);
//       Dec(nBufSize)
//     end { while nBufSize > 0 }
// end { CalculateCRC };


// procedure  TGSkCRC32.CalculateCRC(const   sText:  string);
// begin
//   CalculateCRC(PChar(sText), Length(sText) * SizeOf(Char))
// end { CalculateCRC };


// procedure  TGSkCRC32.CalculateCRC(const   nNumber:  integer);
// begin
//   CalculateCRC(@nNumber, SizeOf(nNumber))
// end { CalculateCRC };


// procedure  TGSkCRC32.CalculateCRC(const   dTime:  TDateTime);
// begin
//   CalculateCRC(@dTime, SizeOf(dTime))
// end { CalculateCRC };


// procedure  TGSkCRC32.CalculateCRC(const   Stream:  TStream);
//
// const
//   cnBufSize = 16 * 1024;
//
// var
//   Buf:  packed array[1..cnBufSize] of  Byte;
//   nSavePos, nBufSize:  Integer;
//
// begin  { CalculateCR }
//   nSavePos := Stream.Position;
//   try
//     Stream.Position := 0;
//     repeat
//       nBufSize := Stream.Read(Buf, cnBufSize);
//       CalculateCRC(Addr(Buf), nBufSize)
//     until  Stream.Position >= Stream.Size
//   finally
//     Stream.Position := nSavePos
//   end { try-finally }
// end { CalculateCR };


//  procedure  EnumCRC16Table(const   EnumProc:  TEnumCRC16TableProc;
//                            const   pData:     pointer);
//  var
//    nInd:  integer;
//
//  begin  { EnumCRC16Table }
//    for  nInd := 0  to  cnCRCTableMaxInd  do
//      EnumProc(nInd, pCRC16Table^[nInd], pData)
//  end { EnumCRC16Table };
//
//
//  procedure  EnumCRC32Table(const   EnumProc:  TEnumCRC32TableProc;
//                            const   pData:     pointer);
//  var
//    nInd:  integer;
//
//  begin  { EnumCRC32Table }
//    for  nInd := 0  to  cnCRCTableMaxInd  do
//      EnumProc(nInd, pCRC32Table^[nInd], pData)
//  end { EnumCRC32Table };

{$ENDREGION 'CRC 32'}


function  CalcCRC16(const   pBuffer:  Pointer;
                    const   nBufSize:  Cardinal)
                    : Word;
begin
  with  TGSkCRC16.Create()  do
    try
      CalculateCRC(pBuffer, nBufSize);
      Result := CRC
    finally
      Free()
    end { try-finally; with TGSkCRC16.Create() }
end { CalcCRC16 };


function  CalcCRC16(const   sText:  AnsiString)
                    : Word;
begin
  with  TGSkCRC16.Create()  do
    try
      CalculateCRC(sText);
      Result := CRC
    finally
      Free()
    end { try-finally }
end { CalcCRC16 };


{$IFDEF UNICODE}

function  CalcCRC16(const   sText:  UnicodeString)
                    : Word;
begin
  with  TGSkCRC16.Create()  do
    try
      CalculateCRC(sText);
      Result := CRC
    finally
      Free()
    end { try-finally }
end { CalcCRC16 };

{$ENDIF UNICODE}


function  CalcCRC16(const   nNumber:  integer)
                    : Word;
begin
  with  TGSkCRC16.Create()  do
    try
      CalculateCRC(nNumber);
      Result := CRC
    finally
      Free()
    end { try-finally }
end { CalcCRC16 };


function  CalcCRC16(const   dTime:  TDateTime)
                    : Word;
begin
  with  TGSkCRC16.Create()  do
    try
      CalculateCRC(dTime);
      Result := CRC
    finally
      Free()
    end { try-finally }
end { CalcCRC16 };


function  CalcCRC16(const   Stream:  TStream)
                    : Word;
begin
  with  TGSkCRC16.Create()  do
    try
      CalculateCRC(Stream);
      Result := CRC
    finally
      Free()
    end { try-finally }
end { CalcCRC16 };


function  CalcCRC32(const   pBuffer:  Pointer;
                    const   nBufSize:  Cardinal)
                    : LongWord;
begin
//   with  TGSkCRC32.Create()  do
//     try
//       CalculateCRC(pBuffer, nBufSize);
//       Result := CRC
//     finally
//       Free()
//     end { try-finally }
  raise  Exception.Create('Funkcja „CalcCRC32” nie działa prawidłowo!')
end { CalcCRC32 };


function  CalcCRC32(const   sText:  string)
                    : LongWord;
begin
//   with  TGSkCRC32.Create()  do
//     try
//       CalculateCRC(sText);
//       Result := CRC
//     finally
//       Free()
//     end { try-finally }
  raise  Exception.Create('Funkcja „CalcCRC32” nie działa prawidłowo!')
end { CalcCRC32 };


function  CalcCRC32(const   nNumber:  integer)
                    : LongWord;
begin
//   with  TGSkCRC32.Create()  do
//     try
//       CalculateCRC(nNumber);
//       Result := CRC
//     finally
//       Free()
//     end { try-finally }
  raise  Exception.Create('Funkcja „CalcCRC32” nie działa prawidłowo!')
end { CalcCRC32 };


function  CalcCRC32(const   dTime:  TDateTime)
                    : LongWord;
begin
//   with  TGSkCRC32.Create()  do
//     try
//       CalculateCRC(dTime);
//       Result := CRC
//     finally
//       Free()
//     end { try-finally }
  raise  Exception.Create('Funkcja „CalcCRC32” nie działa prawidłowo!')
end { CalcCRC32 };


function  CalcCRC32(const   Stream:  TStream)
                    : LongWord;
begin
//   with  TGSkCRC32.Create()  do
//     try
//       CalculateCRC(Stream);
//       Result := CRC
//     finally
//       Free()
//     end { try-finally }
  raise  Exception.Create('Funkcja „CalcCRC32” nie działa prawidłowo!')
end { CalcCRC32 };


initialization

  nCRC16UseCount := 0;
//  nCRC32UseCount := 0;
  Synchro := TCriticalSection.Create();


finalization
  FreeAndNil(Synchro);


end.

