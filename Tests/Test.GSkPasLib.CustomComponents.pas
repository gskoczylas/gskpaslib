﻿unit  Test.GSkPasLib.CustomComponents;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$WARN SYMBOL_PLATFORM OFF}
{$TYPEINFO ON}


uses
  Controls, ComCtrls, StdCtrls, ExtCtrls, SysUtils, Dialogs, Forms,
  JvListBox, JvDriveCtrls,
  GSkPasLib.CustomComponents, TestFrameWork;


type
  TBaseComp = class(TGSkCustomComponent)
  protected
    procedure  Proc();   virtual; abstract;
  end { TBaseComp };

  TTestComp = class(TBaseComp)
  public
    procedure  Test();
  end { TTestComp };

  TCustomComponents_Test = class(TTestCase)
  private
    cmpTest:  TTestComp;
    frmTest:  TForm;
    dlbTest:  TJvDirectoryListBox;
    pnlTest:  TPanel;
  public
    procedure  SetUp();                                              override;
    procedure  TearDown();                                           override;
  published
    procedure  VerifyComponentName();
    procedure  VerifyAbstractMethod();
  end { TSystemUtils_Test };


implementation


uses
  GSkPasLib.Dialogs;


{ TCustomComponents_Test }

procedure TCustomComponents_Test.SetUp;

var
  pcPageControl: TPageControl;
  TabSheet1: TTabSheet;
  tsTabSheetSecond: TTabSheet;
  gbGroupBox: TGroupBox;
  TabSheet3: TTabSheet;

begin
  inherited;
  {$Warnings OFF }
  cmpTest := TTestComp.Create(nil);
  {$Warnings ON  }
  { - }
  frmTest := TForm.Create(nil);
  pcPageControl := TPageControl.Create(frmTest);
  TabSheet1 := TTabSheet.Create(frmTest);
  tsTabSheetSecond := TTabSheet.Create(frmTest);
  TabSheet3 := TTabSheet.Create(frmTest);
  pnlTest := TPanel.Create(frmTest);
  gbGroupBox := TGroupBox.Create(frmTest);
  dlbTest := TJvDirectoryListBox.Create(frmTest);
  with  pcPageControl  do
    begin
      Name := 'pcPageControl';
      Parent := frmTest;
      Left := 0;
      Top := 0;
      Width := 345;
      Height := 297;
      ActivePage := tsTabSheetSecond;
      TabOrder := 0;
    end { with pcPageControl };
  with  TabSheet1  do
    begin
      Name := 'TabSheet1';
      Parent := pcPageControl;
      PageControl := pcPageControl;
      Caption := 'TabSheet1';
    end { with TabSheet1 };
  with  tsTabSheetSecond  do
    begin
      Name := 'tsTabSheetSecond';
      Parent := pcPageControl;
      PageControl := pcPageControl;
      Caption := 'tsTabSheetSecond';
      ImageIndex := 1;
    end { with tsTabSheetSecond };
  with  TabSheet3  do
    begin
      Name := 'TabSheet3';
      Parent := pcPageControl;
      PageControl := pcPageControl;
      Caption := 'TabSheet3';
      ImageIndex := 2;
    end { with TabSheet3 };
  with  pnlTest  do
    begin
      Name := 'pnlTest';
      Parent := tsTabSheetSecond;
      Left := 16;
      Top := 16;
      Width := 305;
      Height := 233;
      BevelInner := bvRaised;
      BevelWidth := 5;
      BorderWidth := 5;
      TabOrder := 0;
    end { with pnPanel };
  with  gbGroupBox  do
    begin
      Name := 'gbGroupBox';
      Parent := pnlTest;
      Left := 24;
      Top := 24;
      Width := 257;
      Height := 185;
      Caption := 'gbGroupBox';
      TabOrder := 0;
    end { gbGroupBox };
  with  dlbTest  do
    begin
      Name := 'dlbTest';
      Parent := gbGroupBox;
      Left := 8;
      Top := 24;
      Width := 241;
      Height := 153;
//    Style := lbOwnerDrawFixed;
      TabOrder := 0;
      Directory := '.';
    end { with dlbTest };
  frmTest.Caption := 'Test';
  frmTest.AutoSize := True;
  frmTest.Position := poDefault;
  frmTest.Show();
end { SetUp };


procedure TCustomComponents_Test.TearDown;
begin
  inherited;
  frmTest.Free();
  cmpTest.Free()
end { TearDown };


procedure  TCustomComponents_Test.VerifyAbstractMethod();
begin
  cmpTest.Test()
end { VerifyAbstractMethod };


procedure  TCustomComponents_Test.VerifyComponentName();
begin
  { Wg ".Name" }
  Check(cmpTest.ComponentDisplayName(dlbTest, False) = dlbTest.Name,
        'Komponent bez ścieżki - wg ".Name"');
  Check(cmpTest.ComponentDisplayName(dlbTest, True) = 'TForm.pcPageControl.tsTabSheetSecond.pnlTest.gbGroupBox.dlbTest',
        'Komponent ze ścieżką - wg ".Name"');
  { Zamiast ".Name" użyje ".ClassName" }
  frmTest.Name := '';
  dlbTest.Name := '';
  pnlTest.Name := '';
  Check(cmpTest.ComponentDisplayName(dlbTest, False) = dlbTest.ClassName,
        'Komponent bez ścieżki - wg. ".ClassName"');
  Check(cmpTest.ComponentDisplayName(dlbTest, True) = 'TForm.pcPageControl.tsTabSheetSecond.TPanel.gbGroupBox.TJvDirectoryListBox',
        'Komponent ze ścieżką - wg ".ClassName"');
end { VerifyComponentName };


{ TTestComp }

procedure TTestComp.Test();
begin
  try
    CallAbstractMethod(Proc, Self, 'Proc1');
    raise  Exception.Create('Powinien był sygnalizować "EAbstractError"');
  except
    on  eErr : EAbstractError  do
      ErrDlg(eErr.Message, nil, False, 3000)
    else
      raise
  end { try-except }
end { Test };


initialization
  RegisterTest(TCustomComponents_Test.Suite())


end.
