﻿{: @abstract Podprogramy matematyczne (i okolice).

   Wiele funkcji matematycznych jest zdefiniowanych w standardowym module
   @code(Math).

   Również wiele przydatnych funkcji można znaleźć w module @code(jclMath). Jest
   to jeden z modułów pakietu JEDI Code Library (http://homepages.borland.com/jedi/jcl).  }
unit  GSkPasLib.Math;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    System.Math, System.Types;
  {$ELSE}
    Math;
  {$IFEND}


{: @abstract Zaokrągla liczbę do wskazanej dokładności.

   Jeżeli parametr @code(Precision) jest dodatni to @code(Value) zaokrąglana
   jest do wskazanej liczby cyfr po przecinku (ułamkowych).

   Jeżeli parametr @code(Precision) jest równy zero to @code(Value) zaokrąglana
   jest do wartości całkowitych.

   Jeżeli parametr @code(Precision) jest ujemny to @code(Value) zaokrąglana
   jest do wskazanej liczby cyfr przed przecinkiem (dziesiątki, setki, tysiące
   itd.).

   Przykłady:
   @longCode(#
     x := RoundFloat(1234.567,  2);   // 1234.57 -- do setnych
     x := RoundFloat(1234.567,  1);   // 1234.6  -- do dziesiętnych
     x := RoundFloat(1234.567,  0);   // 1235    -- do jednostek
     x := RoundFloat(1234.567, -1);   // 1230    -- do dziesiątek
     x := RoundFloat(1234.567, -2);   // 1200    -- do setek
     x := RoundFloat(1234.567, -3);   // 1000    -- do tysięcy
   #)

   @param(Value Zaokrąglana wartość.)
   @param(Precision Parametr określa precyzję zaokrąglania. Może to być liczba
                    dodatnia, ujemna lub zero.)

   @returns(Wynikiem funkcji jest wartość po zaokrągleniu.

           @bold(UWAGA!)@br
           Ze względu na specyfikę działania modułu matematycznego procesora
           wynik działania funkcji zazwyczaj nie jest równy dokładnie
           matematycznej wartości zaokrąglanej liczby --- zazwyczaj jest to
           wartość różniąca się od dokładnej matematycznej wartości o stosunkowo
           niewielką wartość.)  }
function  RoundFloat(const   Value:  Extended;
                     const   Precision:  Integer)
                     : Extended;

{: @abstract Zwraca indeks liczby w tablicy liczb.

   @returns(Jeżeli liczba zostanie znaleziona w tablicy, to wynikiem funkcji
            jest indeks odpowiedniego elementu. Indeks pierwszego elementu tablicy
            jest równy zero.

            Jeżeli liczba nie zostanie znaleziona w tablicy, to wynikiem funkcji
            jest @code(-1).)

   @seeAlso(MatchValue)  }
function  IndexValue(const   nValue:  Integer;
                     const   aValues:  array of Integer)
                     : Integer;

{: @abstract Sprawdza, czy liczba występuje w tablicy.

   @seeAlso(IndexValue)  }
function  MatchValue(const   nValue:  Integer;
                     const   aValues:  array of Integer)
                     : Boolean;

{: @abstract Zwraca jedną z wartości, w zależności od warunku.

   W standardowym module @code(Math) jest kilka przeładowanych funkcji
   o analogicznym działaniu. Wśród nich brak funkcji dla wartości typu
   @code(Currency). Dzięki funkcji @name (z tego modułu) można uniknąć
   zbędnych konwersji wartości typu @code(Currency) na inne numeryczne typy. }
function  IfThen(const   bCondition:  Boolean;
                 const   nTrue:  Currency;
                 const   nFalse:  Currency = 0.0)
                 : Currency;                               overload; inline;

{: @abstract Porównuje dwie wartości typu @code(Currency).

   W standardowym module @code(System.Math) jest zdefiniowanych kilka funkcji
   @code(CompareValue) dla różnych typów numerucznych. Brakuje wśród nich funkcji
   dla typu (Currency). }
function  CompareValue(const   A, B:  Currency)
                       : TValueRelationship;                         overload;



implementation


function  RoundFloat(const   Value:  Extended;
                     const   Precision:  Integer)
                     : Extended;
var
  nVal, nTmp, nRound:  Extended;

begin  { RoundFloat }
  nVal := Value;
  if  nVal < 0.0  then
    nRound := -0.5000001
  else
    nRound := +0.5000001;
  if  Precision <> 0  then
    begin
      nTmp := Power(10, Precision);
      Result := Int(nVal * nTmp + nRound) / nTmp
    end { Precision > 0 }
  else
    Result := Int(nVal + nRound);
end { RoundFloat };


function  IndexValue(const   nValue:  Integer;
                     const   aValues:  array of Integer)
                     : Integer;
begin
  Result := High(aValues);
  while  Result >= Low(aValues)  do
    begin
      if  aValues[Result] = nValue  then
        Break;
      Dec(Result)
    end { while }
end { IndexValue };


function  MatchValue(const   nValue:  Integer;
                     const   aValues:  array of Integer)
                     : Boolean;
begin
  Result := IndexValue(nValue, aValues) >= 0
end { MatchValue };


function  IfThen(const   bCondition:  Boolean;
                 const   nTrue:  Currency;
                 const   nFalse:  Currency)
                 : Currency;
begin
  if  bCondition  then
    Result := nTrue
  else
    Result := nFalse
end { IfThen };


function  CompareValue(const   A, B:  Currency)
                       : TValueRelationship;
begin
  if  A = B  then
    Result := EqualsValue
  else if  A < B  then
    Result := LessThanValue
  else
    Result := GreaterThanValue;
end { CompareValue };


end.
