﻿{: @exclude }
unit  GSkPasLib.DatabaseLoginDlg;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, SyncObjs, IBDatabase, IniFiles, IB,
  IBErrorCodes, DB, ImgList,
  JvAnimatedImage, JvLabel, JvExControls;


type
  TSetAliasMethod = procedure(const   sAlias:  string)
                              of object;

  TzzFormLoginDlg = class(TForm)
    lbAppTitle:    TJvLabel;
    lbVersion:     TLabel;
    pnLogin:       TPanel;
    lbDatabase:    TLabel;
    lbUserName:    TLabel;
    lbPassword:    TLabel;
    edUserName:    TEdit;
    edPassword:    TEdit;
    btOK:          TBitBtn;
    btAbort:       TBitBtn;
    edDatabase:    TComboBox;
    lbAuthor:      TLabel;
    imLogo:        TImage;
    imLogoAuthor:  TImage;
    aiLogoAuthor:  TJvAnimatedImage ;
    imWarning:     TImage;
    ilWarnings:    TImageList;
    lbKeys:        TLabel;
    pnLogging:     TPanel;
    procedure  edDatabaseChange(Sender:  TObject);
    procedure  btOKClick(Sender:  TObject);
    procedure  edDataChange(Sender:  TObject);
    procedure  FormCreate(Sender:  TObject);
    procedure  FormDestroy(Sender:  TObject);
    procedure  edPasswordEnter(Sender:  TObject);
    procedure  edPasswordExit(Sender:  TObject);
    procedure  edPasswordKeyDown(Sender:  TObject;
                                 var   Key:  Word;
                                 Shift:  TShiftState);
    procedure  edPasswordKeyUp(Sender:  TObject;
                               var   Key:  Word;
                               Shift:  TShiftState);
    procedure  FormKeyDown(Sender:  TObject;
                           var   Key:  Word;
                           Shift:  TShiftState);
    procedure  FormShow(Sender:  TObject);
    procedure  FormClose(Sender:  TObject;
                         var   Action:  TCloseAction);
  private
    fDatabase:      TIBDatabase;
    ffnSetAlias:    TSetAliasMethod;
    fcsSynchro:     TCriticalSection;
    fnLastWarning:  Integer;
    fDBConf:        TIniFile;
    fDBParam:       TStrings;
    procedure  LoginError(const   sMsg:  string);
    procedure  CheckStatusKeys();
  protected
    procedure  DoShow();                                             override;
  public
    class procedure  Login(const   Database:  TIBDatabase;
                           const   fnSetAlias:  TSetAliasMethod);
  end { TzzLoginDialog };


//  var
//    zzLoginDialog:  TzzLoginDialog;


implementation


uses
  GSkPasLib.Dialogs, GSkPasLib.FileUtils, GSkPasLib.AppUtils, GSkPasLib.ComponentUtils,
  GSkPasLib.ParamSaver, GSkPasLib.PasLib, GSkPasLib.DbUtils;


{$R *.DFM}


const
  gcsDatabases = 'Bazy';
  gcsParDatabase = 'baza.danych';


procedure  TzzFormLoginDlg.DoShow();

var
  sBuf:  string;
  nInd:  Integer;
  sLogo:  TFileName;

  procedure  LoginErrorNoConfigFile();

  var
    sErr:  string;
    lCfg:  TStringList;
    sCnfName:  TFileName;
    sAppDir:  TFileName;
    nInd:  integer;

  begin  { LoginErrorNoConfigFile }
    sErr := 'Nie istnieje plik konfiguracyjny. Nie odnaleziono żadnego z plików:';
    lCfg := TStringList.Create();
    try
      lCfg.Duplicates := dupIgnore;
      lCfg.Sorted := True;
      sCnfName := GetModuleFileName();
      sAppDir := ExtractFilePath(sCnfName);
      sCnfName := ChangeFileExt(ExtractFileName(sCnfName),
                                gcsStdConfigExt);
      lCfg.Add(ExpandUNCFileName(GetStartupPath()
                                   + ChangeFileExt(ExtractFileName(sCnfName),
                                                   gcsStdConfigExt)));
      lCfg.Add(ExpandUNCFileName(GetStartupPath()
                                   + gcsStdConfigFileName));
      lCfg.Add(ExpandUNCFileName(sAppDir + sCnfName));
      lCfg.Add(ExpandUNCFileName(sAppDir + gcsStdConfigFileName));
      for  nInd := 0  to  lCfg.Count - 1  do
        sErr := sErr + #13#10'— ' + lCfg.Strings[nInd];
      LoginError(sErr)
    finally
      lCfg.Free()
    end { try-finally }
  end { LoginErrorNoConfigFile };

begin  { DoShow }
  inherited;
  sLogo := ChangeFileExt(Application.ExeName, '.wmf');
  if  not FileExists(sLogo)  then
    begin
      sLogo := ChangeFileExt(sLogo, '.emf');
      if  not FileExists(sLogo)  then
        begin
          sLogo := ChangeFileExt(sLogo, '.bmp');
          if  not FileExists(sLogo)  then
            sLogo := ''
        end { not FileExists() }
    end { not FileExists() };
  if  sLogo <> ''  then
    imLogo.Picture.LoadFromFile(sLogo);
  lbVersion.Caption := 'Wersja ' + GetModuleVersion();
  sBuf := GetAppTitle();
  lbAppTitle.Caption := sBuf;
  AdjustFontSize(lbAppTitle, 10);
  edDatabase.Clear();
  edUserName.Clear();
  edPassword.Clear();
  with  fDatabase  do
    begin
      sBuf := GetAppConfigFileName();
      if  sBuf = ''  then
        LoginErrorNoConfigFile();
      fDBConf := TIniFile.Create(sBuf);
      fDBConf.ReadSection(gcsDatabases, edDatabase.Items);
      if  edDatabase.Items.Count = 0  then
        LoginError(Format('W pliku „%s” brak definicji bazy danych',
                          [fDBConf.FileName]));
    end { with };
  nInd := 0;
  with  CreateStdParamSaver()  do
    if  OpenKey(GetAppRegistryKey(), False)  then
      try
        try
          sBuf := ReadString(gcsParDatabase);
          if  sBuf <> ''  then
            begin
              nInd := edDatabase.Items.Count;
              repeat
                Dec(nInd)
              until  (nInd < 0)
                     or {$IFDEF UNICODE}
                          SameText(sBuf, edDatabase.Items[nInd])
                        {$ELSE}
                          AnsiSameText(sBuf, edDatabase.Items[nInd])
                        {$ENDIF -UNICODE}
            end { sBuf <>  '' };
          if  nInd < 0  then
            nInd := 0
        except
          { OK }
        end { try-except }
      finally
        CloseKey()
      end { try-finally };
  Assert(nInd >= 0);
  edDatabase.ItemIndex := nInd;
  edDatabase.OnChange(Self);
  edDatabase.Enabled := (edDatabase.Items.Count > 1);
  if  edDatabase.Enabled  then
    edDatabase.Style := csDropDownList
  else
    edDatabase.Style := csSimple;
end { DoShow };


procedure  TzzFormLoginDlg.edDatabaseChange(Sender:  TObject);
begin
  fDBConf.ReadSectionValues(edDatabase.Text, fDBParam);
  edUserName.Text := fDBParam.Values['user_name'];
  edPassword.Text := fDBParam.Values['password'];
  edDataChange(Sender)
end { edDatabaseChange };


procedure  TzzFormLoginDlg.btOKClick(Sender:  TObject);

var
  sName:  string;

  function  CheckHost(const   sPath:  string)
                      : string;
  var
    sHost:  string;
    lProt:  TProtocol;
    sFile:  string;

  begin  { CheckHost }
    ProcessDbPath(sPath, sHost, lProt, sFile);
    if  (sHost = '')
        and (lProt = Local)  then
      Result := MakeDbPath('127.0.0.1', TCP, sFile)
    else
      Result := sPath
  end { CheckHost };

begin  { btOKClick }
  pnLogging.Show();
  pnLogging.BringToFront();
  Update();
  aiLogoAuthor.Active := False;
  aiLogoAuthor.GlyphNum := 0;
  aiLogoAuthor.Update();
  with  fDatabase  do
    try
      DatabaseName := CheckHost(fDBConf.ReadString(gcsDatabases,
                                                   edDatabase.Text, ''));
      Params.Values['user_name'] := Trim(edUserName.Text);
      Params.Values['password'] := Trim(edPassword.Text);
      try
        ffnSetAlias(edDatabase.Text);
        Open();
        with  CreateStdParamSaver()  do
          try
            if  OpenKey(GetAppRegistryKey(), True)  then
              try
                WriteString(gcsParDatabase, edDatabase.Text)
              finally
                CloseKey()
              end { try-finally };
          finally
            Free()
          end { try-finally };
      except
        on  eErr : EIBError  do
          with  eErr  do
            begin
              case  IBErrorCode  of
                isc_bad_db_format:
                  sName := 'Wskazany plik ma niepoprawny format';
                isc_db_corrupt:
                  sName := 'Baza danych jest uszkodzona';
                isc_login:
                  sName := 'Niepoprawna nazwa lub hasło użytkownika';
                isc_shutdown:
                  sName := 'Baza danych nie jest w tej chwili dostępna';
                else
                  sName := ''
              end { case IBErrorCode };
              if  sName <> ''  then
                sName := sName + #13#13;
              ErrDlg(sName + eErr.Message, edPassword)
            end { with eErr; EIBError }
        else
          raise
      end { try-except };
      ModalResult := mrOk
    finally
      pnLogging.Hide()
    end { try-finally }
end { btOKClick };


procedure  TzzFormLoginDlg.LoginError(const   sMsg:  string);
begin
  ErrDlg(sMsg, False);
  Application.Terminate();
  Abort()
end { LoginError };


procedure  TzzFormLoginDlg.edDataChange(Sender:  TObject);
begin
  btOK.Enabled := (edDatabase.Text <> '')
                  and (Trim(edUserName.Text) <> '')
                  and (Trim(edPassword.Text) <> '')
end { edDataChange };


procedure  TzzFormLoginDlg.FormCreate(Sender:  TObject);
begin
  aiLogoAuthor.Hint := gcsCompanyCard;
  imLogo.Hint := gcsCompanyCard;
  imLogoAuthor.Hint := gcsCompanyCard;
  lbAuthor.Caption := gcsCompanyCard;
  { - }
  fcsSynchro := TCriticalSection.Create();
  fDBParam := TStringList.Create()
end { FormCreate };


procedure  TzzFormLoginDlg.FormDestroy(Sender:  TObject);
begin
  fDBParam.Free();
  fDBConf.Free();
  fcsSynchro.Free()
end { FormDestroy };


procedure  TzzFormLoginDlg.edPasswordEnter(Sender:  TObject);
begin
  fnLastWarning := -1;
  CheckStatusKeys()
end { edPasswordEnter };


procedure  TzzFormLoginDlg.edPasswordExit(Sender:  TObject);
begin
  imWarning.Hide();
  lbKeys.Caption := '';
  aiLogoAuthor.Show()
end { edPasswordExit };


procedure  TzzFormLoginDlg.edPasswordKeyDown(Sender:  TObject;
                                             var   Key:  Word;
                                             Shift:  TShiftState);
begin
  CheckStatusKeys()
end { edPasswordKeyDown };


procedure  TzzFormLoginDlg.edPasswordKeyUp(Sender:  TObject;
                                           var   Key:  Word;
                                           Shift:  TShiftState);
begin
  CheckStatusKeys()
end { edPasswordKeyUp };


procedure  TzzFormLoginDlg.CheckStatusKeys();

var
  sInfo:  string;
  nState:  SmallInt;
  nWarning:  Integer;

  procedure  FmtInfo(var   sOldInfo:  string;
                     const   sAddKey, sAddState:  string;
                     var   nWarning:  Integer;
                     const   nCurrWarning:  Integer);
  const
    ccMark = '—';

  var
    sBuf:  string;

  begin  { FmtInfo }
    sBuf := Format('Klawisz %s jest %s', [sAddKey, sAddState]);
    if  sOldInfo = ''  then
      sOldInfo := sBuf
    else
      begin
        if  sOldInfo[1] <> ccMark  then
          Insert(ccMark + ' ', sOldInfo, 1);
        sOldInfo := sOldInfo + #13 + ccMark + ' ' + sBuf
      end { sOldInfo <> '' };
    if  nWarning < 0  then
      nWarning := nCurrWarning;
    if  Pos(sAddKey, lbKeys.Caption) = 0  then
      lbKeys.Caption := TrimLeft(lbKeys.Caption + '   ' + sAddKey)
  end { FmtInfo };

begin  { CheckStatusKeys }
  fcsSynchro.Enter();
  try
    sInfo := '';
    nWarning := -1;
    lbKeys.Caption := '';
    { Caps Lock }
    nState := GetKeyState(VK_CAPITAL);
    if  nState and $0F <> 0  then   // [CAPS LOCK] aktywny
      if  nState and $F0 <> 0  then
        FmtInfo(sInfo, '[CAPS LOCK]', 'wciśnięty!', nWarning, 0)
      else
        FmtInfo(sInfo, '[CAPS LOCK]', 'aktywny', nWarning, 1);
    { Shift }
    nState := GetKeyState(VK_SHIFT);
     if  nState and $F0 <> 0  then
       FmtInfo(sInfo, '[Shift]', 'wciśnięty', nWarning, 2);
    { Show status }
    if  nWarning >= 0  then
      begin
        imWarning.Hint := sInfo;
        if  nWarning <> fnLastWarning  then
          begin
            ilWarnings.GetIcon(nWarning, imWarning.Picture.Icon);
            fnLastWarning := nWarning
          end { nWarning <> nLastWarning };
        imWarning.Show();
      end { nWarning >= 0 }
    else
      imWarning.Hide();
  finally
    aiLogoAuthor.Visible := not imWarning.Visible;
    fcsSynchro.Leave()
  end { try-finally }
end { CheckStatusKeys };


procedure  TzzFormLoginDlg.FormKeyDown(Sender:  TObject;
                                       var   Key:  Word;
                                       Shift:  TShiftState);
begin
  if  (Key = VK_ESCAPE)
      and (Shift = [])  then
    begin
      btAbort.Click();
      Key := 0
    end { [Esc] }
end { FormKeyDown };


procedure  TzzFormLoginDlg.FormShow(Sender:  TObject);
begin
  aiLogoAuthor.Active := True
end { FormShow };


class procedure  TzzFormLoginDlg.Login(const   Database:  TIBDatabase;
                                       const   fnSetAlias:  TSetAliasMethod);
var
  fLoginDlg:  TzzFormLoginDlg;

begin  { Login }
  fLoginDlg := TzzFormLoginDlg.Create(nil);
  fLoginDlg.fDatabase := Database;
  fLoginDlg.ffnSetAlias := fnSetAlias;
  if  fLoginDlg.ShowModal() <> mrOk  then
    begin
      Application.Terminate();
      Abort()
    end { fLoginDlg.ShowModal() <> mrOk }
end { Login };


procedure  TzzFormLoginDlg.FormClose(Sender:  TObject;
                                     var   Action:  TCloseAction);
begin
  Action := caFree
end { FormClose };


end.

