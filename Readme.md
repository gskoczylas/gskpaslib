﻿# GSkPasLib

Package of general purpose classes, procedures and functions.

- Language: Pascal/Delphi

This package has been tested with the following Delphi versions.

- Delphi 2007
- Delphi XE1
- Delphi 10 Seattle

Initial version of this package was created with Delphi 7.

## Source code documentation

All documentation comments are written in Polish. If your are interested in
documentation in English, please contact me (mailto:gskoczylas+GSkPasLib@gmail.com).
