$remotes = git remote
foreach ($remote in $remotes) {
	Write-Host ""
	Write-Host "${remote}: " -NoNewline
	git push $remote
}
