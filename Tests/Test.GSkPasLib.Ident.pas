﻿unit  Test.GSkPasLib.Ident;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}


uses
  TestFrameWork, GSkPasLib.Ident,
  SysUtils;


type
  TIdent_Test = class(TTestCase)
  published
    procedure  VerifyNIP();
    procedure  VerifyREGON();
    procedure  VerifyNRB();
    procedure  VerifyPESEL();
  end { TIdent_Test };


implementation

{ TIdent_Test }

procedure  TIdent_Test.VerifyNIP();
begin
  CheckEquals('9', NIPChecksum('937-21-88-329'), 'Cyfra kontrolna bez "PL"');
  CheckEquals('9', NIPChecksum('PL 937-21-88-329'), 'Cyfra kontrolna z "PL"');
  CheckTrue(NIPIsCorrect('PL 937-21-88-329'), 'NIP z "PL"');
  CheckTrue(NIPIsCorrect('937-21-88-329'), 'NIP bez "PL"')
end { VerifyNIP };


procedure  TIdent_Test.VerifyNRB();
begin
  CheckTrue(NRBIsCorrect('13 1140 2004 0000 3202 4187 3799'), 'NRB bez "PL"');
  CheckTrue(NRBIsCorrect('PL 13 1140 2004 0000 3202 4187 3799'), 'NRB z "PL"');
end { VerifyNRB };


procedure  TIdent_Test.VerifyPESEL();

const
  sPESEL = '12321209099';   // wygenerowany przy pomocy http://www.bogus.ovh.org/generatory/pesel.html

begin  { VerifyPESEL }
  CheckEquals(EncodeDate(2012, 12, 12), PESELToDate(sPESEL));
  Check(PESELToSex(sPESEL) = sexMale);
  CheckEquals('9', PESELChecksum(sPESEL), 'Suma kontrolna PESEL');
  CheckTrue(PESELIsCorrect(sPESEL));
  CheckTrue(PESELIsCorrect(sPESEL, EncodeDate(2012, 12, 12), sexMale));
end { VerifyPESEL };


procedure  TIdent_Test.VerifyREGON();
begin
  CheckEquals('9', REGONChecksum('072189079'));
  CheckTrue(REGONIsCorrect('072189079'))
end { VerifyREGON };


initialization
  RegisterTest(TIdent_Test.Suite())


end.
