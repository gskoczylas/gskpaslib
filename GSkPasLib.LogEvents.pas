﻿{: @abstract Moduł @name rejestruje monitorowanie zdarzenia poprzez zdarzenia.

   Moduł zawiera definicję obiektu @link(TGSkLogEvents). Jest to
   @link(GSkPasLib.Log.TGSkCustomLogAdapter adapter logowania) czyli obiekt pomocniczy
   dla komponentu @link(GSkPasLib.Log.TGSkLog).

   Ten adapter rejestruje informacje poprzez zdarzenia. Aplikacja może na przykład
   zapisywać zdarzenia w komponencie @code(TMemoEdit) lub @code(TRichEdit) itp.

   @seeAlso(GSkPasLib.Log)  }
unit  GSkPasLib.LogEvents;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22}  // XE
    System.Classes,
  {$ELSE}
    Classes,
  {$IFEND}
  GSkPasLib.Log;


type
  TGSkLogMessageEvent = procedure(Sender:  TObject;
                                  const   Msg:  TGSkLogMessage)      of object;

  {: @abstract(@link(GSkPasLib.Log.TGSkCustomLogAdapter Adapter) rejestrujący
               informacje przy pomocy okien zdarzeń.) }
  TGSkLogEvents = class(TGSkCustomLogAdapter)
  strict private
    var
      fOnBeginLog:  TNotifyEvent;
      fOnMessage:   TGSkLogMessageEvent;
      fOnEndLog:    TNotifyEvent;
      fOnClear:     TNotifyEvent;
  protected
    {** TGSkCustomLogAdapter **}

    {: @abstract Rozpoczyna buforowanie informacji.

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.BeginLog). Powoduje wygenerowania zdarzenia
       @link(OnBeginLog).  }
    procedure  BeginLog();                                           override;

    {: @abstract Rejestruje informację.

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.LogMessage).

       W przypadku obiektu @className metoda @name generuje zdarzenie
       @link(OnMessage).  }
    procedure  LogMessage(const   Msg:  TGSkLogMessage);             override;

    {: @abstract Kończy buforowanie informacji.

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.EndLog). W przypadku obiektu @className
       metoda @name generuje zdarzenie @link(OnEndLog). }
    procedure  EndLog();                                             override;

    {: @abstract Czyści (usuwa) zarejestrowane informacje

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.Clear). W przypadku obiektu @className
       metoda @name generuje zdarzenie @link(OnClear).  }
    procedure  Clear();                                              override;
  public
    property  OnBeginLog:  TNotifyEvent
              read fOnBeginLog  write fOnBeginLog;
    property  OnMessage:  TGSkLogMessageEvent
              read fOnMessage  write fOnMessage;
    property  OnEndLog:  TNotifyEvent
              read fOnEndLog  write fOnEndLog;
    property  OnClear:  TNotifyEvent
              read fOnClear   write fOnClear;
  end { TGSkLogEvents };


implementation


{$REGION 'TGSkLogEvents'}

procedure  TGSkLogEvents.BeginLog();
begin
  // inherited;  -- abstract
  if  Assigned(fOnBeginLog)  then
    fOnBeginLog(Self)
end { BeginLog };


procedure  TGSkLogEvents.Clear();
begin
  // inherited;  -- abstract
  if  Assigned(fOnClear)  then
    fOnClear(Self)
end { Clear };


procedure  TGSkLogEvents.EndLog();
begin
  // inherited;  -- abstract
  if  Assigned(fOnEndLog)  then
    fOnEndLog(Self)
end { EndLog };


procedure  TGSkLogEvents.LogMessage(const   Msg:  TGSkLogMessage);
begin
  // inherited;  -- abstract
  if  Assigned(fOnMessage)  then
    fOnMessage(Self, Msg)
end { LogMessage };

{$ENDREGION 'TGSkLogEvents'}


end.
