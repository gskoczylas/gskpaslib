﻿{: @abstract Zapisuje do pliku wskazane wartości. }
unit  GSkPasLib.Dump;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    System.SysUtils, System.Math, System.Classes, System.SyncObjs;
  {$ELSE}
    SysUtils, Math, Classes, SyncObjs;
  {$IFEND}


const
  {: Domyślne rozszerzenie nazwy plików. }
  gcsDefaultDumpFileExt = '.dmp';


type
  {$IF not Declared(RawByteString)}
    {: Zgodność z nowszymi wersjami Delphi }
    RawByteString = AnsiString;
  {$IFEND}

  {: @abstract Zapisuje do plików wskazane wartości.

     Każde wywołanie metody @link(Dump) lub metody @link(DumpIt) powoduje zapisanie
     wskazanych wartości do kolejnego pliku. Nazwy plików są poprzedzone numerem
     kolejnym pliku. Dla każdego uruchomienia aplikacji pliki numerowane są od zera.
     Jeżeli pliki o danej nazwie już istnieją, to zostaną automatycznie nadpisane
     nową treścią.

     Metody klasy @name mogą być bezpiecznie wywoływane w aplikacjach wielowątkowych.

     Przykład 1 @longCode(#
       var
         lBuf:  TBytes;
       begin
         while not EndOfData do
           begin
             LoadBucketOfData(lBuf);
             TGSkDump.Dump(lBuf, '.\Dump\Buffer');
             ProcessBuffer(lBuf)
           end ;
     #)

     Przykład 2 @longCode(#
       var
         lBuf:  TBytes;
         lDmp:  TGSKDump;
       begin
         lDmp := TGSkDump.Create();
         try
           lDmp.DefaultPath := '.\Dump';
           while not EndOfData  do
             begin
               LoadBucketOfData(lBuf);
               lDmp.DumpIt(lBuf, 'Buffer');
               ProcessBuffer(lBuf)
             end
         finally
           lDmp.Free()
         end ;
     #)

     Przykład 3 @longCode(#
       var
         lBuf:  TBytes;
         lDmp:  TGSkDump;
       begin
         lDmp := TGSkDump.Create();
         try
           lDmp.DefaultFileName := '.\Dump\Buffer';
           while not EndOfData  do
             begin
               LoadBucketOfData(lBuf);
               lDmp.DumpIt(lBuf);
               ProcessBuffer(lBuf)
             end
         finally
           lDmp.Free()
         end ;
     #) }
  TGSkDump = class
  strict private
    class var
      fnCount:  Cardinal;
      fSync:    TCriticalSection;
      fsFile:   string;
    var
      fsDefFilePath:  string;
      fsDefFileName:  string;
      fsDefFileExt:   string;
   {$IFDEF UNICODE}
    class constructor  Create();
    class destructor  Destroy();
   {$ELSE }
    class procedure  CreateId();
    class procedure  DestroyIt();
   {$ENDIF}
    class function  Coalesce(const   sText1, sText2:  string)
                             : string;                               inline;
    class function  FileName(const   sFilePath, sFileName, sFileExt:  string)
                             : string;                               overload;
    function  FileName(const   sFileName:  string)
                       : string;                                     overload; inline;
  public
    {: @exclude}
    constructor  Create();

    {: @abstract Zapisuje do pliku wskazany napis.

       @param(sText Tekst do zapisania do pliku.)
       @param(sFileName Nazwa pliku, do którego dane zostaną zapisane.
                        Jeżeli jest pusta, to nazwa pliku składa się
                        tylko z numeru kolejnego pliku.)
       @seeAlso(DefaultFileName)
       @seeAlso(DefaultFilePath)
       @seeAlso(DefaultFileExt)

       W aplikacjach wielowątkowych wywołania metody @name są kolejkowane. }
    class procedure  Dump(const   sText:  RawByteString;
                          const   sFileName:  string = '');          overload; inline;

    class procedure  Dump(const   sText:  RawByteString;
                          const   Output:  TStream);                 overload; inline;

   {$IF Declared(UnicodeString)}
    {: @abstract Zapisuje do pliku wskazany napis.

       @param(sText Tekst do zapisania do pliku.)
       @param(sFileName Nazwa pliku, do którego dane zostaną zapisane.
                        Jeżeli jest pusta, to nazwa pliku składa się
                        tylko z numeru kolejnego pliku.)

       W aplikacjach wielowątkowych wywołania metody @name są kolejkowane. }
    class procedure  Dump(const   sText:  UnicodeString;
                          const   sFileName:  string = '');          overload; inline;

    class procedure  Dump(const   sText:  UnicodeString;
                          const   Output:  TStream);                 overload; inline;
   {$IFEND}

    {: @abstract Zapisuje do pliku wskazany blok pamięci.

       @param(Bytes Blok pamięci do zapisania do pliku.)
       @param(sFileName Nazwa pliku, do którego dane zostaną zapisane.
                        Jeżeli jest pusta, to nazwa pliku składa się
                        tylko z numeru kolejnego pliku.)

       W aplikacjach wielowątkowych wywołania metody @name są kolejkowane. }
    class procedure  Dump(const   Bytes:  TBytes;
                          const   sFileName:  string = '');          overload;

    class procedure  Dump(const   Bytes:  TBytes;
                          const   Output:  TStream);                 overload;

    {: @abstract Zapisuje do pliku wskazany blok pamięci.

       @param(pData Adres bloku pamięci do zapisania do pliku.)
       @param(nLength Liczba @bold(bajtów) w bloku pamięci.)
       @param(sFileName Nazwa pliku, do którego dane zostaną zapisane.
                        Jeżeli jest pusta, to nazwa pliku składa się
                        tylko z numeru kolejnego pliku.)

       W aplikacjach wielowątkowych wywołania metody @name są kolejkowane. }
    class procedure  Dump(const   pData:  Pointer;
                          const   nLength:  Cardinal;
                          const   sFileName:  string = '');          overload;

    {: @abstract Zapisuje do pliku wskazany napis.

       @param(sText Tekst do zapisania do pliku.)
       @param(sFileName Nazwa pliku, do którego dane zostaną zapisane.
                        Jeżeli jest pusta, to nazwa pliku składa się
                        tylko z numeru kolejnego pliku.)

       W aplikacjach wielowątkowych wywołania metody @name są kolejkowane. }
    procedure  DumpIt(const   sText:  RawByteString;
                      const   sFileName:  string = '');              overload; inline;

   {$IF Declared(UnicodeString)}
    {: @abstract Zapisuje do pliku wskazany napis.

       @param(sText Tekst do zapisania do pliku.)
       @param(sFileName Nazwa pliku, do którego dane zostaną zapisane.
                        Jeżeli jest pusta, to nazwa pliku składa się
                        tylko z numeru kolejnego pliku.)
       @seeAlso(DefaultFilePath)
       @seeAlso(DefaultFileName)
       @seeAlso(DefaultFileExt)

       W aplikacjach wielowątkowych wywołania metody @name są kolejkowane. }
    procedure  DumpIt(const   sText:  UnicodeString;
                      const   sFileName:  string = '');              overload; inline;
   {$IFEND}

    {: @abstract Zapisuje do pliku wskazany blok pamięci.

       @param(Bytes Blok pamięci do zapisania do pliku.)
       @param(sFileName Nazwa pliku, do którego dane zostaną zapisane.
                        Jeżeli jest pusta, to nazwa pliku składa się
                        tylko z numeru kolejnego pliku.)
       @seeAlso(DefaultFilePath)
       @seeAlso(DefaultFileName)
       @seeAlso(DefaultFileExt)

       W aplikacjach wielowątkowych wywołania metody @name są kolejkowane. }
    procedure  DumpIt(const   Bytes:  TBytes;
                      const   sFileName:  string = '');              overload; inline;

    {: @abstract Zapisuje do pliku wskazany blok pamięci.

       @param(pBytes Adres bloku pamięci do zapisania do pliku.)
       @param(nLength Liczba @bold(bajtów) w bloku pamięci.)
       @param(sFileName Nazwa pliku, do którego dane zostaną zapisane.
                        Jeżeli jest pusta, to nazwa pliku składa się
                        tylko z numeru kolejnego pliku.)
       @seeAlso(DefaultFilePath)
       @seeAlso(DefaultFileName)
       @seeAlso(DefaultFileExt)

       W aplikacjach wielowątkowych wywołania metody @name są kolejkowane. }
    procedure  DumpIt(const   pBytes:  Pointer;
                      const   nLength:  Cardinal;
                      const   sFileName:  string = '');              overload; inline;

    {-}

    {: @abstract Nazwa ostatnio zapisanego pliku. }
    class property  LastFilename:  string
                    read  fsFile;

    {: @abstract Domyślna nazwa pliku.

       Właściwość @name pozwala zdefiniować domyślną nazwę pliku. Będzie użyta
       wtedy, gdy w metodzie @link(Dump) nazwa pliku nie zostanie wskazana.

       @longCode(#
         var
           lDmp:  TGSkDump;

         begin
           lDmp := TGSkDump.Create();
           try
             lDmp.DefaultFileName = 'MemoryDump';
             // ...
             while not EndOfData  do
               begin
                 LoadBucket(lBuf);
                 lDmp.DumpIt(lBuf);
                 // Processing of data bucket
               end ;
           finally
             lDmp.Free()
           end ; #)

       @bold(Uwaga!) Właściwość @name ma zastosowanie tylko w przypadku używania
       @italic(zwykłych) metod obiektu (a nie w przypadku metod klasowych).

       @seeAlso(DumpIt)
       @seeAlso(DefaultFilePath)
       @seeAlso(DefaultFileExt) }
    property  DefaultFileName:  string
              read  fsDefFilename
              write fsDefFilename;

    {: @abstract Domyślny folder zapisywania plików.

       Jeżeli w metodzie @link(Dump) nie wskazano pliku lub wskazany plik nie
       zawiera folderu, to zostanie zapisany do folderu wskazanego przez
       właściwość @name.

       Jeżeli @name jest pustym napisem, to pliki będą zapisywane do bieżącego
       folderu.

       @bold(Uwaga!) Właściwość @name ma zastosowanie tylko w przypadku używania
       @italic(zwykłych) metod obiektu (a nie w przypadku metod klasowych).

       @seeAlso(DumpIt)
       @seeAlso(DefaultFileName)
       @seeAlso(DefaultFileExt) }
    property  DefaultFilePath:  string
              read  fsDefFilePath
              write fsDefFilePath;

    {: @abstract Domyślne rozszerzenie nazwy pliku.

       Jeżeli nie zostanie wskazane rozszerzenie nazwy pliku, to zostanie
       użyte domyślne rozszerzenie wskazane przez właściwość @name.

       @bold(Uwaga!) Właściwość @name ma zastosowanie tylko w przypadku używania
       @italic(zwykłych) metod obiektu (a nie w przypadku metod klasowych).

       @seeAlso(DumpIt)
       @seeAlso(DefaultFilePath)
       @seeAlso(DefaultFileName) }
    property  DefaultFileExt:  string
              read  fsDefFileExt
              write fsDefFileExt;

  end { TGSkDump };


implementation


uses
  GSkPasLib.ASCII, GSkPasLib.Classes;


{$REGION 'TGSkDump'}

class function  TGSkDump.Coalesce(const   sText1, sText2:  string)
                                  : string;
begin
  if  sText1 <> ''  then
    Result := sText1
  else
    Result := sText2
end { Coalesce };


{$IFDEF UNICODE}
class constructor  TGSkDump.Create();
{$ELSE}
class procedure  TGSkDump.CreateIt();
{$ENDIF}
begin
  fSync := TCriticalSection.Create()
end { Create[It] };


constructor  TGSkDump.Create();
begin
  inherited;
  fsDefFileExt := gcsDefaultDumpFileExt
end { Create };


{$IFDEF UNICODE}
class destructor  TGSkDump.Destroy();
{$ELSE}
class procedure  TGSkDump.DestroyIt();
{$ENDIF}
begin
  FreeAndNil(fSync)
end { Destroy[It] };


class procedure  TGSkDump.Dump(const   Bytes:  TBytes;
                               const   sFileName:  string);
var
  lOut:  TFileStream;

begin  { Dump }
  lOut := TFileStream.Create(FileName('.', sFileName, gcsDefaultDumpFileExt),
                             fmCreate or fmShareDenyWrite);
  try
    Dump(Bytes, lOut)
  finally
    lOut.Free()
  end { try-finally }
end { Dump };


class procedure  TGSkDump.Dump(const   Bytes:  TBytes;
                               const   Output:  TStream);
const
  cnWidth = 16;

var
  nInd:  Integer;

  function  FmtLine(const   Bytes:  TBytes;
                    const   nFirstInt:  Integer)
                    : string;
  var
    nInd:  Integer;
    nMax:  Integer;

  begin  { FmtLine }
    Result := '';
    nMax := Min(nFirstInt + cnWidth - 1, High(Bytes));
    for  nInd := nFirstInt  to  nMax  do
      Result := Result + Format('%2.2x ', [Bytes[nInd]]);
    Result := Result + StringOfChar(' ', cnWidth * 3 + 1 - Length(Result)) + '| ';
    for  nInd := nFirstInt  to  nMax  do
      if  CharInSet(AnsiChar(Bytes[nInd]), gcsetControlChars)  then
        Result := Result + '.'
      else
        Result := Result + WideChar(AnsiChar(Bytes[nInd]))
  end { FmtLine };

begin  { Dump }
  fSync.Enter();
  try
    nInd := Low(Bytes);
    while  nInd <= High(Bytes)  do
      begin
        Output.WriteDataString(FmtLine(Bytes, nInd) + sLineBreak);
        Inc(nInd, cnWidth)
      end { while }
  finally
    fSync.Leave()
  end { try-finally }
end { Dump };


class procedure  TGSkDump.Dump(const   pData:  Pointer;
                               const   nLength:  Cardinal;
                               const   sFileName:  string);
var
  lBytes:  TBytes;

begin  { Dump }
  SetLength(lBytes, nLength);
  Move(PByte(pData)^, lBytes[0], nLength);
  Dump(lBytes, sFileName)
end { Dump };


class procedure  TGSkDump.Dump(const   sText:  RawByteString;
                               const   Output:  TStream);
begin
  Dump(BytesOf(sText), Output)
end { Dump };


class procedure  TGSkDump.Dump(const   sText:  RawByteString;
                               const   sFileName:  string);
begin
  Dump(BytesOf(sText), sFileName)
end { Dump };


{$IF Declared(UnicodeString)}

class procedure TGSkDump.Dump(const   sText:  UnicodeString;
                              const   sFileName:  string);
begin
  Dump(BytesOf(sText), sFileName)
end { Dump };


class procedure TGSkDump.Dump(const   sText:  UnicodeString;
                              const   Output:  TStream);
begin
  Dump(BytesOf(sText), Output)
end { Dump };


{$IFEND}


procedure  TGSkDump.DumpIt(const   Bytes:  TBytes;
                           const   sFileName:  string);
begin
  Dump(Bytes, FileName(sFileName))
end { DumpIt };


procedure  TGSkDump.DumpIt(const   pBytes:  Pointer;
                           const   nLength:  Cardinal;
                           const   sFileName:  string);
begin
  Dump(pBytes, nLength, sFileName)
end { DumpIt };


{$IF Declared(UnicodeString)}

procedure  TGSkDump.DumpIt(const   sText:  UnicodeString;
                           const   sFileName:  string);
begin
  Dump(BytesOf(sText), sFileName)
end { DumpIt };

{$IFEND}

procedure  TGSkDump.DumpIt(const   sText:  RawByteString;
                           const   sFileName:  string);
begin
  Dump(BytesOf(sText), sFileName)
end { DumpIt };


function  TGSkDump.FileName(const   sFileName:  string)
                            : string;
begin
  Result := FileName(DefaultFilePath,
                     Coalesce(sFileName, DefaultFileName),
                     DefaultFileExt)
end { FileName };


class function  TGSkDump.FileName(const   sFilePath, sFileName, sFileExt:  string)
                                  : string;
var
  sPath, sName:  string;

begin  { FileName }
  sPath := ExtractFilePath(sFileName);
  if  sPath = ''  then
    sPath := IncludeTrailingPathDelimiter(Coalesce(sFilePath, '.'));
  sName := ExtractFileName(sFileName);
  if  sName <> ''  then
    sName := ' ' + sName;
  Result := Format('%s[%6u]%s', [sPath, fnCount, sName]);
  Inc(fnCount);
  if  ExtractFileExt(Result) = ''  then
    Result := ChangeFileExt(Result, Coalesce(sFileExt, gcsDefaultDumpFileExt));
  fsFile := Result
end { FileName };

{$ENDREGION 'TGSkDump'}


initialization
  {$IFNDEF UNICODE}
    TGSkDump.CreateIt();
  {$ENDIF UNICODE}


finalization
  {$IFNDEF UNICODE}
    TGSkDump.DestroyIt();
  {$ENDIF UNICODE}

end.
