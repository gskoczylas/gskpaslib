﻿{: @abstract Zawiera wygodne mechanizmy do przechowywania parametrów.

   Moduł @name ujednolica obsługę systemowej bazy @italic(Registry) oraz plików
   XML. Zasady zapamiętywania i przywracania parametrów opisane są w dokumentacji
   komponentu @link(TGSkCustomParamSaver). }
unit  GSkPasLib.ParamSaver;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{.$DEFINE ParamReg}
{.$DEFINE ParamXML}


{$IF Defined(ParamReg) and Defined(ParamXML)}
  {$MESSAGE ERROR 'Zdefiniuj albo „ParamReg”, albo „ParamXML”, albo żadną z nich.'}
{$IFEND}


uses
  {$IF CompilerVersion > 22}
    System.Classes, VCL.Graphics, System.SysUtils, WinAPI.Windows, System.Win.Registry,
    XML.XMLDoc, XML.XMLIntf,
  {$ELSE}
    Classes, Graphics, SysUtils, Windows, Registry,
    XMLDoc, XMLIntf,
  {$IFEND}
  JclSysInfo,
  GSkPasLib.CustomComponents, GSkPasLib.PasLibInternals;


const
  {: @abstract Standardowe rozszerzenie pliku XML z parametrami.

     Jeżeli nie wskazano inaczej to plik z parametrami XML standardowo ma takie
     rozszerzenie.

     @seeAlso(TGSkParamSaverXML.FileName) }
  gcsParamFileExtXML = '.param.xml';


type
  {: @abstract Miejsce przechowywania parametrów.

     Typ @name określa miejsce przechowywania parametrów.

     @value(ktCurrentUser Parametry są przechowywane dla użytkownika aktualnie
                          zalogowanego do komputera.)
     @value(ktLocalMachine Parametry są przechowywane dla dowolnego użytkownika
                           komputera (wspólne dla wszystkich użytkowników
                           komputera).) }
  TGSkParamSaverKeyType = (ktCurrentUser, ktLocalMachine);

  {: @abstract Aktualny stan klucza.

     Parametry przypisane są do klucza. Klucz musi zostać otwarty przed jego
     użyciem i zamknięty po użyciu. Typ @name określa bieżący stan klucza.

     @value ksClosed Klucz jest zamknięty.
     @value ksReadOnly Klucz jest otwarty w trybie @italic(tylko do odczytu).
     @value ksReadWrite Klucz jest otwarty z pełnym dostępem do parametrów. }
  TGSkParamSaverKeyState = (ksClosed, ksReadOnly, ksReadWrite);

  {: @abstract Typ wartości.

     Jednym z atrybutów wartości pamiętanych w poszczególnych węzłach jest typ
     wartości.

     @value(sdUnknown Typ wartości jest nieznany.)
     @value(sdString  Wartość jest napisem.)
     @value(sdInteger Wartość jest liczbą całkowitą.)
     @value(sdBinary  Wartość jest obiektem innym niż napis lub liczba całkowita.)

     @seeAlso(TGSkCustomParamSaver.GetDataType)  }
  TGSkParamSaverDataType = (sdUnknown, sdString, sdInteger, sdBinary);

  {: @abstract Klasa bazowa dla komponentów przechowywania parametrów.

     Na bazie komponentu @name można definiować inne specjalizowane komponenty
     zapamiętujące parametry w systemowej bazie @link(TGSkParamSaverRegistry Registry),
     w plikach @link(TGSkParamSaverXML XML) lub w innych lokalizacjach. W programach
     należy korzystać z tych specjalizowanych komponentów.

     Komponent @name jest zaprojektowany tak, aby działać na strukturach
     hierarchicznych podobnych do systemowej bazy Registry.

     Najpierw trzeba otworzyć (lub utworzyć) jeden z kluczy. W tym celu wywołuje
     się metodę @link(OpenKey) lub @link(OpenKeyReadOnly) wskazując otwierany
     klucz. Na koniec trzeba klucz zamknąć wywołując metodę @link(CloseKey).

     W każdym kluczu można zapamiętać dowolnie wiele różnych wartości. Jedna z
     tych wartości to tak zwana wartość domyślna. Jest to wartość dowolnego typu,
     ale z pustą nazwą wartości.

     Każdy klucz może mieć również dowolną liczbę kluczy podrzędnych.

     Jeżeli zajdzie potrzeba pracy z kilkoma kluczami jednocześnie to bez
     względu na ich wzajemną zależność w hierarchii każdy z tych kluczy trzeba
     otworzyć niezależnie od pozostałych i również każdy klucz musi być
     niezależnie zamknięty.

     @seeAlso(TGSkParamSaverRegistry)
     @seeAlso(TGSkParamSaverXML)  }
  TGSkCustomParamSaver = class abstract (TGSkCustomComponent)
  strict private
    FKeyType:  TGSkParamSaverKeyType;
    FLastKey:  string;
    FKeyState:  TGSkParamSaverKeyState;
    FUpdateLevel:  Integer;
    function  GetKey() : string;
  strict protected

    {: @abstract Otwiera wskazany klucz.

       Metoda @name otwiera wskazany klucz. Wszystkie komponenty potomne muszą
       implementować tę metodę.  }
    function  InternalOpenKey(const   sKey:  string;
                              const   bCanCreate:  Boolean)
                              : Boolean;                   virtual; abstract;

    {: @abstract Otwiera wskazany klucz w trybie tylko-do-odczytu.

       Metoda @name otwiera wskazany klucz w trybie @italic(tylko do odczytu).
       Wszystkie komponenty potomne muszą implementować tę metodę.  }
    function  InternalOpenKeyReadOnly(const   sKey:  string)
                                      : Boolean;           virtual; abstract;

    {: @abstract Zamyka bieżący klucz.

       Metoda @name zamyka bieżący klucz (otwarty przez @link(InternalOpenKey)
       lub @link(InternalOpenKeyReadOnly). Wszystkie komponenty potomne muszą
       implementować tę metodę.  }
    procedure  InternalCloseKey();                         virtual; abstract;

    {: @abstract Sprawdza czy dane mogą być pobrane.

       Metoda @name sprawdza czy dane mogą zostać pobrane. Sprawdzany jest typ
       i wielkość danych oraz czy klucz jest otwarty.  }
    procedure  CheckRead(const   sName:  string = '';
                         const   DataType:  TGSkParamSaverDataType = sdUnknown;
                                 nSize:  Integer = 0);

    {: @abstract Sprawdza czy dane mogą być zapamiętane.

       Metoda @name sprawdza czy dane mogą zostać zapamiętane -- sprawdza czy
       klucz jest otwarty i czy nie jest otwarty w trybie @italic(tylko do
       odczytu).  }
    procedure  CheckWrite();

    {: @abstract Sprawdza czy klucz jest zamknięty.

       Metoda @name sprawdza czy klucz jest aktualnie zamknięty.  }
    procedure  CheckClosed();

    {: @abstract Definiuje korzeń drzewa danych.

       Metoda @name definiuje korzeń drzewa danych.  }
    procedure  SetKeyType(const   Value:  TGSkParamSaverKeyType);    virtual;

  public
    {: @exclude }
    constructor  Create(AOwner:  TComponent);                        override;
    {: @exclude }
    destructor  Destroy();                                           override;

    {: @abstract Rozpoczyna modyfikowanie danych.

       Metoda @name powinna powinna być wywoływana zawsze gdy aplikacja
       potrzebuje zapamiętać bądź zmienić wiele danych. Jej użycie powoduje, że
       dane będą fizycznie zapisane dopiero po wywołaniu metody @link(EndUpdate).

       Bieżący poziom wywołań metod @name i @link(EndUpdate) można spradzić dzięki
       właściwości @link(UpdateLevel).

       Szczegóły działania metod @name i @link(EndUpdate) zależą od ich
       implementacji w klasach potomnych.

       @seeAlso(EndUpdate)
       @seeAlso(UpdateLevel)  }
    procedure  BeginUpdate();                                        virtual;

    {: @abstract Kończy modyfikowanie danych.

       Metoda @link(BeginUpdate) powinna powinna być wywoływana zawsze gdy
       aplikacja potrzebuje zapamiętać bądź zmienić wiele danych. Jej użycie
       powoduje, że dane będą fizycznie zapisane dopiero po wywołaniu metody
       @name.

       Bieżący poziom wywołań metod @link(BeginUpdate) i @name można spradzić
       dzięki właściwości @link(UpdateLevel).

       Szczegóły działania metod @link(BeginUpdate) i @name zależą od ich
       implementacji w klasach potomnych.

       @seeAlso(BeginUpdate)
       @seeAlso(UpdateLevel)  }
    procedure  EndUpdate();                                          virtual;

    {: @abstract Otwiera wskazany klucz.

       Metoda @name otwiera wskazany klucz. Należy wskazać pełną ścieżkę klucza.
       Kolejne poziomy klucza należy rozdzielać znakiem „\” lub „/”.

       @param(sKey Nazwa klucza.)
       @param(bCanCreate Parametr @name decyduje o zachowaniu się funkcji wtedy
                         gdy dany klucz lub jego część nie istnieje.)

       @seeAlso(OpenKeyReadOnly)
       @seeAlso(CloseKey)  }
    function  OpenKey(const   sKey:  string;
                      const   bCanCreate:  Boolean = True)
                      : Boolean;

    {: @abstract Otwiera wskazany klucz w trybie @italic(tylko-do-odczytu).

       Metoda @name otwiera wskazany klucz w trybie @italic(tylko-do-odczytu).

       @seeAlso(OpenKey)
       @seeAlso(CloseKey)  }
    function  OpenKeyReadOnly(const   sKey:  string)  
                              : Boolean;

    {: @abstract Zamyka klucz.

       Każdy klucz otwarty metodą @link(OpenKey) lub @link(OpenKeyReadOnly)
       należy zamknąć wywołując metodę @name.

       @seeAlso(OpenKey)
       @seeAlso(OpenKeyReadOnly)  }
    procedure  CloseKey();

    {: @abstract Funkcja usuwa wskazany klucz.

       Funkcję @name tę należy używać ostrożnie. Powoduje ona usunięcie
       wszystkich wartości w usuwanym kluczu oraz wszystkich kluczy podrzędnych.

       @seeAlso(DeleteValue)  }
    function  DeleteKey(const   sKey:  string)
                        : Boolean;                                   virtual;

    {: @abstract Funkcja usuwa z bieżącego klucza wskazaną wartość.

       Funkcja usuwa z bieżącego klucza wskazaną wartość.

       @seeAlso(DeleteKey)  }
    function  DeleteValue(const   sName:  string)
                          : Boolean;                                 virtual;

    {: @abstract Sprawdza czy wskazany klucz już istnieje.

       Funkcja @name sprawdza czy wskazany klucz już istnieje.

       @seeAlso(HasSubKeys)
       @seeAlso(ValueExists)  }
    function  KeyExists(const   sKey:  string)
                        : Boolean;                                   virtual;

    {: @abstract Informuje czy w bieżącym kluczu istnieje wskazana wartość.

       Funkcja @name Funkcja informuje czy w bieżącym kluczu istnieje wskazana
       wartość.

       @seeAlso(KeyExists)  }
    function  ValueExists(const   sName:  string)
                          : Boolean;                                 virtual;

    {: @abstract Informuje czy klucz ma klucze podrzędne.

       Funkcja @name informuje, czy aktualnie otwarty klucz ma klucze podrzędne.

       @seeAlso(KeyExists)  }
    function  HasSubKeys() : Boolean;                                virtual;

    {: @abstract Rozmiar danych wskazanej wartości.

       Jeżeli dane są napisem to wynikiem funkcji @name jest długość napisu plus
       jeden dla znaku @code(NUL) kończącego napis.

       @returns(Wynikiem funkcji jest liczba bajtów wartości o wskazanej nazwie.
                W przypadku błędu wynikiem funkcji jest @code(-1).)

       @seeAlso(ReadBinaryData)
       @seeAlso(GetDataType)  }
    function  GetDataSize(const   sName:  string)
                          : Integer;                       virtual; abstract;

    {: @abstract Typ wartości.

       Funkcja informuje o typie wartości.

       @seeAlso(GetDataSize)  }
    function  GetDataType(const   sName:  string)
                          : TGSkParamSaverDataType;        virtual; abstract;

    {: @abstract Funkcja odczytuje dane binarne.

       Funkcja name odczytuje dane binarne. Dane takie muszą być zapamiętane
       przez metodę @link(WriteBinaryData).

       Nie jest wskazane aby w parametrach zapisywać bardzo obszerne dane.
       Jeżeli zajdzie taka potrzeba to optymalniejszym rozwiązaniem jest
       zapisanie takich danych do pliku na dysku, a w parametrach zapamiętanie
       tylko ścieżki do tego pliku.

       @seeAlso(WriteBinaryData)
       @seeAlso(GetDataSize)  }
    function  ReadBinaryData(const   sName:  string;
                             var   Buffer;
                             const   nBufferSize:  Integer)
                             : Integer;                              virtual;

    {: @abstract Funkcja odczytuje wartość logiczną.

       Funkcja @name odczytuje wartość logiczną. Jeżeli wskazanej wartości nie
       ma to zwracana jest wartość domyślna.

       @seeAlso(WriteBool)  }
    function  ReadBool(const   sName:  string;
                       const   bDefault:  Boolean = False)
                       : Boolean;                                    virtual;

    {: @abstract Odczytuje wartość typu @code(Currency).

       Funkcja @name odczytuje wartość typu @code(Currency). Jeżeli wskazana
       zmienna nie istnieje to zwracana jest wartość domyślna.

       @seeAlso(WriteCurrency)  }
    function  ReadCurrency(const   sName:  string;
                           const   nDefault:  Currency = 0.0)
                           : Currency;                               virtual;

    {: @abstract Odczytuje datę.

       Funkcja odczytuje datę zapamiętaną przez metodę @link(WriteDate) lub
       @link(WriteDateTime). Jeżeli wskazana zmienna nie istnieje to zwracana
       jest wartość domyślna.

       @seeAlso(WriteDate)
       @seeAlso(WriteDateTime)  }
    function  ReadDate(const   sName:  string;
                       const   dDefault:  TDateTime = 0.0)
                       : TDateTime;                                  virtual;

    {: @abstract Zwraca zapamiętany czas.

       Funkcja @name zwraca czas zapamiętany przez metodę @link(WriteTime) lub
       @link(WriteDateTime). Jeżeli wskazana zmienna nie istnieje to zwracana
       jest wartość domyślna.

       @seeAlso(WriteTime)
       @seeAlso(WriteDateTime)  }
    function  ReadTime(const   sName:  string;
                       const   dDefault:  TDateTime = 0.0)
                       : TDateTime;                                  virtual;

    {: @abstract Odczytuje datę i czas.

       Funkcja @name odczytuje datę i czas zapamiętane przez metodę
       @link(WriteDateTime) lub @link(WriteDate), lub @link(WriteTime). Jeżeli
       wskazana zmienna nie istnieje to zwracana jest wartość domyślna.

       @seeAlso(WriteDateTime)
       @seeAlso(WriteDate)
       @seeAlso(WriteTime)  }
    function  ReadDateTime(const   sName:  string;
                           const   dDefault:  TDateTime = 0.0)
                           : TDateTime;                              virtual;

    {: @abstract Zwraca wartość wskazanej zmiennej.

       Funkcja @name zwraca wartość wskazanej zmiennej rzeczywistej. Jeżeli
       wskazana zmienna nie istnieje to zwracana jest wartość domyślna.

       @seeAlso(WriteFloat)  }
    function  ReadFloat(const   sName:  string;
                        const   nDefault:  Double = 0.0)
                        : Double;                                    virtual;

    {: @abstract Zwraca zapamiętaną wartość całkowitą.

       Funkcja @name zwraca zapamiętaną wartość całkowitą. Jeżeli wskazana
       zmienna nie istnieje to zwracana jest wartość domyślna.

       @seeAlso(WriteInteger)  }
    function  ReadInteger(const   sName:  string;
                          const   nDefault:  Integer = 0)
                          : Integer;                                 virtual;

    {: @abstract Zwraca wartość wskazanej zmiennej.

       Funkcja @name zwraca wartość wskazanej zmiennej. Jeżeli wskazana zmienna
       nie istnieje to zwracana jest wartość domyślna.

       @seeAlso(WriteString)
       @seeAlso(ReadChar)  }
    function  ReadString(const   sName:  string;
                         const   sDefault:  string = '')
                         : string;                                   virtual;

    {: @abstract Zwraca wartość wskazanej zmiennej.

       Funkcja @name zwraca wartość wskazanej zmiennej. Jeżeli wskazana zmienna
       nie istnieje to zwracana jest wartość domyślna.

       @seeAlso(WriteChar)
       @seeAlso(ReadString)  }
    function  ReadChar(const   sName:  string;
                       const   chDefault:  Char = #0)
                       : Char;                                       virtual;

    {: @abstract Funkcja odczytuje kolor.

       Funkcja @name odczytuje kolor zapamiętany wcześniej przez metodę
       @link(WriteColor).

       Jeżeli wskazana zmienna nie istnieje to zwracana jest wartość domyślna.

       @seeAlso(WriteColor)  }
    function  ReadColor(const   sName:  string;
                        const   clDefault:  TColor = clNone)
                        : TColor;                                    virtual;

    {: @abstract Zwraca parametry czcionki.

       Funkcja @name zwraca czionkę zapamiętaną przez metodę @link(WriteFont).
       Jeżeli wskazana zmienna nie istnieje to zwracana jest wartość domyślna.

       Funkcja zwraca standardowy obiekt typu @code(TFont) opisujący wszystkie
       parametry zapamiętanej wcześniej czcionki. Obiekt ten po wykorzystaniu
       należy zwolnić wywołując jego metodę @code(Free()).

       Przykład:
       @longCode(#
         var
           lPar:  TGSkParamSaverRegistry;
           lFont:  TFont;
           sKey:  string;

         begin
           lPar := TGSkParamSaverRegistry.Create(nil);
           try
             sKey := ChangeFileExt(ExtractFileName(Application.ExeName),
                                   '');
             if  lPar.OpenKeyReadOnly(sKey)  then
               try
                 lFont := lPar.ReadFont('Czcionka', Self.Font);
                 try
                   Self.Font.Assign(lFont)
                 finally
                   lFont.Free()
                 end // try-finally
               finally
                 lPar.CloseKey()
               end // try-finally
           finally
             lPar.Free()
           end // try-finally
         end ;
       #)

       @seeAlso(WriteFont)  }
    function  ReadFont(const   sName:  string;
                       const   Default:  TFont)
                       : TFont;                                      virtual;

    {: @abstract Pobiera listę nazw kluczy.

       Po wywołaniu metody @name w zmiennej @code(Names) umieszczona będzie
       lista nazw kluczy bezpośrednio podrzędnych w stosunku do bieżącego klucza.

       @seeAlso(GetValueNames)  }
    procedure  GetKeyNames(const   Names:  TStrings);                virtual;

    {: @abstract Pobiera listę nazw parametrów.

       Po wywołaniu tej funkcji @name w zmiennej @code(Names) umieszczana jest
       lista nazw wszystkich wartości zapamiętanych w bieżącym kluczu.

       @seeAlso(GetKeyNames)  }
    procedure  GetValueNames(const   Names:  TStrings);              virtual;

    {: @abstract Funkcja zapisuje dane binarne.

       Nie zaleca się zapisywania w parametrach wielkich ilości danych binarnych.
       Jeżeli zajdzie taka potrzeba to wydajniesze jest zapisanie takich danych
       do pliku na dysku i zapamiętanie w parametrach tylko ścieżki do tego pliku.

       @seeAlso(ReadBinaryData)  }
    procedure  WriteBinaryData(const   sName:  string;
                               var     Buffer;
                               const   nBufferSize:  Integer);       virtual;

    {: @abstract Zapamiętuje parametr logiczny.

       Metoda zapamiętuje parametr logiczny.

       @seeAlso(ReadBool)  }
    procedure  WriteBool(const   sName:  string;
                         const   bValue:  Boolean);                  virtual;

    {: @abstract Zapamiętuje wartość typu @italic(Currency).

       Metoda @name zapamiętuje wartość typu @italic(Currency).

       @seeAlso(ReadCurrency)  }
    procedure  WriteCurrency(const   sName:  string;
                             const   nValue:  Currency);             virtual;

    {: @abstract Zapamiętuje datę.

       Metoda @name zapamiętuje datę. Można ją później odczytać metodą
       @link(ReadDate) (lub @link(ReadDateTime)).

       @seeAlso(WriteTime)
       @seeAlso(WriteDateTime)
       @seeAlso(ReadDate)
       @seeAlso(ReadDateTime)  }
    procedure  WriteDate(const   sName:  string;
                         const   dValue:  TDateTime);                virtual;

    {: @abstract Zapamiętuje czas.

       Funkcja @name zapamiętuje czas. Można go później odczytać metodą
       @link(ReadTime) (lub @link(ReadDateTime)).

       @seeAlso(WriteDate)
       @seeAlso(WriteDateTime)
       @seeAlso(ReadTime)
       @seeAlso(ReadDateTime)  }
    procedure  WriteTime(const   sName:  string;
                         const   dValue:  TDateTime);                virtual;

    {: @abstract Zapamiętuje datę i czas.

       Funkcja @name zapamiętuje datę i czas. Wartość tę można później odczytać
       za pomocą metod @link(ReadDateTime), @link(ReadDate) lub @link(ReadTime).

       @seeAlso(WriteDate)
       @seeAlso(WriteDateTime)
       @seeAlso(ReadDate)
       @seeAlso(ReadDateTime)  }
    procedure  WriteDateTime(const   sName:  string;
                             const   dValue:  TDateTime);            virtual;

    {: @abstract Zapamiętuje wskazaną liczbę rzeczywistą.

       Funkcja @name zapamiętuje wskazaną liczbę.

       @seeAlso(ReadFloat)  }
    procedure  WriteFloat(const   sName:  string;
                          const   nValue:  Double);                  virtual;

    {: @abstract Zapamiętuje wskazaną liczbę całkowitą.

       Funkcja @name zapamiętuje wskazaną liczbę.

       @seeAlso(ReadInteger)  }
    procedure  WriteInteger(const   sName:  string;
                            const   nValue:  Integer);               virtual;

    {: @abstract Zapamiętuje napis.

       Funkcja @name zapamiętuje wskazany napis.

       @seeAlso(ReadString)  }
    procedure  WriteString(const   sName:  string;
                           const   sValue:  string);                 virtual;

    {: @abstract Zapamiętuje znak.

       Funkcja @name zapamiętuje wskazany znak.

       @seeAlso(ReadString)
       @seeAlso(WriteChar)  }
    procedure  WriteChar(const   sName:  string;
                         const   chValue:  Char);                    virtual;

    {: @abstract Zapamiętuje kolor.

       Metoda @name zapamiętuje kolor. Można go później odczytać za pomocą
       metody @link(ReadColor).

       @seeAlso(ReadColor)  }
    procedure  WriteColor(const   sName:  string;
                          const   clValue:  TColor);

    {: @abstract Zapamiętuje wszystkie parametry wskazanej czcionki.

       Metoda @name zapamiętuje wszystkie parametry wskazanej czcionki. Można
       je później odczytać za pomocą metody @link(ReadFont).

       @seeAlso(ReadFont)  }
    procedure  WriteFont(const   sName:  string;
                         const   Font:  TFont);

    {: @abstract Zapamiętuje wskazaną liczbę całkowitą.

       Metoda @name zapamiętuje wskazaną liczbę całkowitą. O ile jest to możliwe
       to liczba jest zapisywana w formacie szesnastkowym.

       W komponencie @link(TGSkParamSaverRegistry) metoda @name działa tak samo
       jak metoda @link(WriteInteger). Natomiast w komponencie
       @link(TGSkParamSaverXML) metoda @name zapisuje wartość liczby jako ciąg
       cyfr szesnastkowych poprzedzonych znakiem @code('$') (zgodnie z konwencją
       języka Pascal).

       @seeAlso(WriteInteger)  }
    procedure  WriteHex(const   sName:  string;
                        const   nValue:  Integer);                   virtual;
  published
    {: @abstract Typ klucza.

       Właściwość @name określa czy klucz (i dane) pamiętane będą tylko dla
       użytkownika zalogowanego aktualnie do komputera (@code(ktCurrentUser))
       albo dla dowolnego użytkownika komputera (@code(ktLocalMachine)).

       @seeAlso(KeyState)
       @seeAlso(Key)  }
    property  KeyType:  TGSkParamSaverKeyType
              read  FKeyType
              write SetKeyType
              default  ktCurrentUser;

    {: @abstract Informuje o statusie klucza.

       Właściwość @name informuje o aktualnym statusie klucza. Klucz może być
       otwarty w pełnym trybie umożliwoającym swobodne odczytywania wartości
       oraz modyfikowanie wartości lub dopisywanie nowych.

       Istnieje również stan, w którym można tylko pobierać wartości, ale nie
       wolno ich modyfikować w żaden sposób.

       @seeAlso(KeyType)
       @seeAlso(Key)  }
    property  KeyState:  TGSkParamSaverKeyState
              read  FKeyState;

    {: @abstract Ta właściwość przedstawia aktualnie otwarty klucz.

       Jeżeli klucz jest otwarty to wartość @name zwraca jest jego wartość.
       Jeżeli natomiast klucz jest zamknięty to zwracany jest pusty napis.

       @seeAlso(KeyType)
       @seeAlso(KeyState)  }
    property  Key:  string
              read  GetKey;

    {: @abstract Poziom aktualizacji danych.

       Właściwość @name wskazuje bieżący poziom aktualizowania danych. Po każdym
       wywołaniu metody @link(BeginUpdate) poziom jest zwiększany o jeden.
       Natomiast po każdym wywołaniu metody @link(EndUpdate) poziom ten jest
       zmniejszany.

       Jeżeli bieżący poziom jest różny od zara to modyfikowane dane nie są
       fizycznie zapamiętywane. Zostaną zapamiętane dopiero po wywołaniu metody
       @link(EndUpdate) zmniejszającej ten poziom do zera. Dzięki temu
       modyfikowanie serii danych odbywa się szybciej.

       Działanie metod @link(BeginUpdate) i @link(EndUpdate) zależy od sposobu
       ich implementacji w klasach pochodnych.

       @seeAlso(BeginUpdate)
       @seeAlso(EndUpdate)  }
    property  UpdateLevel:  Integer
              read  FUpdateLevel;
  end { TGSkCustomParamSaver };


  {: @abstract Klasa obiektów do przechowywania parametrów.

     Typ @name oznacza klasę obiektów będących komponentami potomnymi komponentu
     @link(TGSkCustomParamSaver).

     @seeAlso(TGSkCustomParamSaver)
     @seeAlso(TGSkParamSaverRegistry)
     @seeAlso(TGSkParamSaverXML)  }
  TGSkParamSaverClass = class of TGSkCustomParamSaver;


  {: @abstract Ułatwia obsługę Registry.

     Komponent @name ułatwia aplikacjom i biblitotekom zapamiętywanie parametrów
     w systemowej bazie @italic(Registry).

     Komponent @name definiuje działanie wszystkich właściwości i metod bazowego
     komponentu @inherited. Dodatkowo wprowadza kilka specyficznych właściwości
     i metod.

     @seeAlso(TGSkCustomParamSaver)
     @seeAlso(TGSkParamSaverXML)  }
  TGSkParamSaverRegistry = class(TGSkCustomParamSaver)
  strict private
    FReg:  TRegistry;
    FBaseKey:  string;
    function  GetKey(const   sKey:  string)
                     : string;
  strict protected
    {: @inherited }
    function  InternalOpenKey(const   sKey:  string;
                              const   bCanCreate:  Boolean)
                              : Boolean;                             override;
    {: @inherited }
    function  InternalOpenKeyReadOnly(const   sKey:  string)
                                      : Boolean;                     override;
    {: @inherited }
    procedure  InternalCloseKey();                                   override;
    {: @inherited }
    procedure  SetKeyType(const   Value:  TGSkParamSaverKeyType);    override;
  public
    {: @exclude }
    constructor  Create(AOwner:  TComponent);                        override;
    {: @exclude }
    destructor  Destroy();                                           override;
    {: @inherited }
    function  DeleteKey(const   sKey:  string)
                        : Boolean;                                   override;
    {: @inherited }
    function  DeleteValue(const   sName:  string)
                          : Boolean;                                 override;
    {: @inherited }
    function  KeyExists(const   sKey:  string)
                        : Boolean;                                   override;
    {: @inherited }
    function  ValueExists(const   sName:  string)
                          : Boolean;                                 override;
    {: @inherited }
    function  HasSubKeys() : Boolean;                                override;
    {: @inherited }
    function  GetDataSize(const  sName:  string)
                          : Integer;                                 override;
    {: @inherited }
    function  GetDataType(const   sName:  string)
                          : TGSkParamSaverDataType;                  override;
    {: @inherited }
    function  ReadBinaryData(const   sName:  string;
                             var   Buffer;
                             const   nBufferSize:  Integer)
                             : Integer;                              override;
    {: @inherited }
    function  ReadBool(const   sName:  string;
                       const   bDefault:  Boolean = False)
                       : Boolean;                                    override;
    {: @inherited }
    function  ReadCurrency(const   sName:  string;
                           const   nDefault:  Currency = 0.0)
                           : Currency;                               override;
    {: @inherited }
    function  ReadDate(const   sName:  string;
                       const   dDefault:  TDateTime = 0.0)
                       : TDateTime;                                  override;
    {: @inherited }
    function  ReadTime(const   sName:  string;
                       const   dDefault:  TDateTime = 0.0)
                       : TDateTime;                                  override;
    {: @inherited }
    function  ReadDateTime(const   sName:  string;
                           const   dDefault:  TDateTime = 0.0)
                           : TDateTime;                              override;
    {: @inherited }
    function  ReadFloat(const   sName:  string;
                        const   nDefault:  Double = 0.0)
                        : Double;                                    override;
    {: @inherited }
    function  ReadInteger(const   sName:  string;
                          const   nDefault:  Integer = 0)
                          : Integer;                                 override;
    {: @inherited }
    function  ReadString(const   sName:  string;
                         const   sDefault:  string = '')
                         : string;                                   override;
    {: @inherited }
    procedure  GetKeyNames(const   Names:  TStrings);                override;
    {: @inherited }
    procedure  GetValueNames(const   Names:  TStrings);              override;
    {: @inherited }
    procedure  WriteBinaryData(const   sName:  string;
                               var     Buffer;
                               const   nBufferSize:  Integer);       override;
    {: @inherited }
    procedure  WriteBool(const   sName:  string;
                         const   bValue:  Boolean);                  override;
    {: @inherited }
    procedure  WriteCurrency(const   sName:  string;
                             const   nValue:  Currency);             override;
    {: @inherited }
    procedure  WriteDate(const   sName:  string;
                         const   dValue:  TDateTime);                override;
    {: @inherited }
    procedure  WriteTime(const   sName:  string;
                         const   dValue:  TDateTime);                override;
    {: @inherited }
    procedure  WriteDateTime(const   sName:  string;
                             const   dValue:  TDateTime);            override;
    {: @inherited }
    procedure  WriteFloat(const   sName:  string;
                          const   nValue:  Double);                  override;
    {: @inherited }
    procedure  WriteInteger(const   sName:  string;
                            const   nValue:  Integer);               override;
    {: @inherited }
    procedure  WriteString(const   sName:  string;
                           const   sValue:  string);                 override;
    {: @abstract Zapisuje liczbę całkowitą.

       W komponencie @className metoda @name działa tak samo jak metoda
       @link(WriteInteger).

       @seeAlso(WriteInteger)
       @seeAlso(TGSkCustomParamSaver.WriteHex)  }
    procedure  WriteHex(const   sName:  string;
                        const   nValue:  Integer);                   override;
  published

    {: @abstract Klucz bazowy.

       W metodach @link(OpenKey) i @link(OpenKeyReadOnly) wskazujemy klucz,
       który chcemy otworzyć. W przypadku @italic(Registry) klucz ten jest
       automatycznie uzupełniany o stały początek wskazany we właściwości @name.

       Jeżeli podczas projektowania nie zostanie zdefiniowana wartość tej
       właściwości to podczas działania parogramu właściwość przyjmuje wartość
       @code(\Software\ShortName\ProductName\InternalName). Poszczególne
       segmenty wyznaczana są następująco:
       @table(@row(@cell(Software)
                   @cell(Stały napis))
              @row(@cell(ShortName)
                   @cell(wartość stałej @link(gcsCompanyName) z pliku
                         @code(Author.inc); stała ta wskazuje krótką nazwę
                         właściciela programu (np. krótka nazwa firmy)))
              @row(@cell(ProductName)
                   @cell(pobierana z @italic(Version Info); ten element
                         występuje tylko wtedy gdy w @italic(VersionInfo)
                         odpowiednie pole nie jest puste))
              @row(@cell(InternalName)
                   @cell(pobierana z @italic(Version Info))))

       W tym pakiecie zakładam, że do każdego projektu dołączone są informacje
       z @italic(Version Info). Jedną z najczęściej wykorzystywanych informacji
       jest @italic(InternalName).

       @seeAlso(TGSkCustomParamSaver.KeyType)  }
    property  BaseKey:  string
              read  FBaseKey
              write FBaseKey;
  end { TGSkParamSaverRegistry };

  {: @abstract Możlowe lokalizacje pliku parametrów.

     @value(flAppData Plik parametrów jest w folderze %AppData%, w folderze
                      podrzędnym zależnym od nazwy firmy -- nazywa się tak samo
                      jak aplikacja.)
     @value(flAppDir Plik parametrów jest w folderze aplikacji --  nazywa się
                     tak samo jak aplikacja.)
     @value(flCustom Plik parametrów jest we wskazanym pliku.)

     @seeAlso(TGSkLogFile.CreateLog)  }
  TGSkParamSaverFileLocation = (flAppData, flAppDir, flCustom);

  {: @abstract Ułatwia zapamiętywanie parametrów w XML.

     Komponent @name ułatwia aplikacjom i biblitotekom zapamiętywanie parametrów
     w pliku XML.

     Komponent @name definiuje działanie wszystkich właściwości i metod bazowego
     komponentu @inherited. Dodatkowo wprowadza kilka specyficznych właściwości
     i metod.

     @seeAlso(TGSkCustomParamSaver)
     @seeAlso(TGSkParamSaverRegistry)  }
  TGSkParamSaverXML = class(TGSkCustomParamSaver)
  strict private
    fFileLocation: TGSkParamSaverFileLocation;
    fXMLFileName:  TFileName;
    fsCustomDir:   TFileName;
    fXML:          TXMLDocument;
    fiCurrNode:    IXMLNode;
    function  IndexOfNode(const   iNode:  IXMLNode;
                          const   sNodeKind:  WideString;
                          const   sNodeName:  string)
                          : Integer;
    function  GetStdFileName() : string;
    function  InternalRead(const   sName:  string;
                           const   sDefault:  string)
                           : string;
    procedure  InternalWrite(const   sName:  string;
                             const   sValue:  string;
                             const   DataType:  TGSkParamSaverDataType;
                                     nSize:  Integer = 0;
                             const   bFreeString:  Boolean = False);
    procedure  SetXMLFileName(const   Value:  TFileName);
    procedure  GetNodeNames(const   sNodeKind:  WideString;
                            const   Nodes:  TStrings);
    procedure  SaveToFile(const   bUnconditional:  Boolean);
    procedure  Init();
  strict protected
    function  LocateKey(const   sKey:  string;
                        const   bCanCreate:  Boolean)
                        : IXMLNode;
    {: @inherited }
    function  InternalOpenKey(const   sKey:  string;
                              const   bCanCreate:  Boolean)
                              : Boolean;                             override;
    {: @inherited }
    function  InternalOpenKeyReadOnly(const   sKey:  string)
                                      : Boolean;                     override;
    {: @inherited }
    procedure  InternalCloseKey();                                   override;
    {: @inherited }
    procedure  SetKeyType(const   Value:  TGSkParamSaverKeyType);    override;
  public

    {: @abstract Standardowy konstruktor.

       Plik parametrów zostanie utworzony tak, jakby został wywołany konstruktor
       z następującymi parametrami: @code(CreateFile(AOwner, flAppData);)

       @seeAlso(CreateFile)  }
    constructor  Create(AOwner:  TComponent);                        override;

    {: @abstract Kreator ze wskazaniem lokalizacji pliku parametrów.

       @param(AOwner Właściciel komponentu.

                     Dla komponentów tymczasowych najlepiej wywołać konstruktor
                     z tym parametrem równym @nil.)

       @param(FileLocation Lokalizacja pliku z parametrami.

                           @link(Create Standardowo) plik z parametrami jest
                           w folderze @code(%AppData%\NazwaFirmy).)

       @param(sFilePath Ścieżka do pliku parametrów.

                        Ten parametr ma znaczenie tylko wtedy, gdy parametr
                        @code(LogFileLocation) ma wartość @code(flCustom).)

       @seeAlso(Create)  }
    constructor  CreateFile(const   AOwner:  TComponent;
                            const   FileLocation:  TGSkParamSaverFileLocation;
                            const   sFilePath:  TFileName = '');

    {: @exclude }
    destructor  Destroy();                                           override;
    {: @inherited }
    procedure  EndUpdate();                                          override;
    {: @inherited }
    function  DeleteKey(const   sKey:  string)
                        : Boolean;                                   override;
    {: @inherited }
    function  DeleteValue(const   sName:  string)
                          : Boolean;                                 override;
    {: @inherited }
    function  KeyExists(const   sKey:  string)
                        : Boolean;                                   override;
    {: @inherited }
    function  ValueExists(const   sName:  string)
                          : Boolean;                                 override;
    {: @inherited }
    function  HasSubKeys() : Boolean;                                override;
    {: @inherited }
    function  GetDataSize(const  sName:  string)
                          : Integer;                                 override;
    {: @inherited }
    function  GetDataType(const   sName:  string)
                          : TGSkParamSaverDataType;                  override;
    {: @inherited }
    function  ReadBinaryData(const   sName:  string;
                             var   Buffer;
                             const   nBufferSize:  Integer)
                             : Integer;                              override;
    {: @inherited }
    function  ReadBool(const   sName:  string;
                       const   bDefault:  Boolean = False)
                       : Boolean;                                    override;
    {: @inherited }
    function  ReadCurrency(const   sName:  string;
                           const   nDefault:  Currency = 0.0)
                           : Currency;                               override;
    {: @inherited }
    function  ReadDate(const   sName:  string;
                       const   dDefault:  TDateTime = 0.0)
                       : TDateTime;                                  override;
    {: @inherited }
    function  ReadTime(const   sName:  string;
                       const   dDefault:  TDateTime = 0.0)
                       : TDateTime;                                  override;
    {: @inherited }
    function  ReadDateTime(const   sName:  string;
                           const   dDefault:  TDateTime = 0.0)
                           : TDateTime;                              override;
    {: @inherited }
    function  ReadFloat(const   sName:  string;
                        const   nDefault:  Double = 0.0)
                        : Double;                                    override;
    {: @inherited }
    function  ReadInteger(const   sName:  string;
                          const   nDefault:  Integer = 0)
                          : Integer;                                 override;
    {: @inherited }
    function  ReadString(const   sName:  string;
                         const   sDefault:  string = '')
                         : string;                                   override;
    {: @inherited }
    procedure  GetKeyNames(const   Names:  TStrings);                override;
    {: @inherited }
    procedure  GetValueNames(const   Names:  TStrings);              override;
    {: @inherited }
    procedure  WriteBinaryData(const   sName:  string;
                               var     Buffer;
                               const   nBufferSize:  Integer);       override;
    {: @inherited }
    procedure  WriteBool(const   sName:  string;
                         const   bValue:  Boolean);                  override;
    {: @inherited }
    procedure  WriteCurrency(const   sName:  string;
                             const   nValue:  Currency);             override;
    {: @inherited }
    procedure  WriteDate(const   sName:  string;
                         const   dValue:  TDateTime);                override;
    {: @inherited }
    procedure  WriteTime(const   sName:  string;
                         const   dValue:  TDateTime);                override;
    {: @inherited }
    procedure  WriteDateTime(const   sName:  string;
                             const   dValue:  TDateTime);            override;
    {: @inherited }
    procedure  WriteFloat(const   sName:  string;
                          const   nValue:  Double);                  override;
    {: @inherited }
    procedure  WriteInteger(const   sName:  string;
                            const   nValue:  Integer);               override;
    {: @inherited }
    procedure  WriteString(const   sName:  string;
                           const   sValue:  string);                 override;
    {: @inherited }
    procedure  WriteHex(const   sName:  string;
                        const   nValue:  Integer);                   override;
  published

    {: @abstract Właściwość @name wskazuje plik z parametrami.

       Jeżeli podczas projektowania programu właściwość nie zostanie
       zdefiniowana to podczas działania programu stosowany jest plik o nazwie
       takiej samej jak aplikacja lub biblioteka ale rozszerzenie zostanie
       zamienione na rozszerzenie standardowe.

       W zależności od wartości parametru @link(KeyType) używane jest różne
       rozszerzenie pliku wskazanego przez właściwość @name.

       Jeżeli @link(KeyType) ma wartość @code(ktLocalMachine) to stosowane jest
       rozszerzenie zdefiniowane przez stałą @link(gcsParamFileExtXML).

       Jeżeli @link(KeyType) ma wartość @code(ktCurrentUser) to powyższe
       rozszerzenie poprzedzane jest dodatkowo nazwą użytkownika aktualnie
       zalogowanego w systemie @italic(Windows).

       Ilustruje to następująca tabela:
       @table(
         @rowHead(@cell(@code(FileName))
                  @cell(@code(KeyType))
                  @cell(Użytkownik komputera)
                  @cell(Używana nazwa pliku XML))
         @row(@cell(@code(Dane))
              @cell(@code(ktLocalMachine))
              @cell(Grzegorz)
              @cell(@code(Dane.param.xml)))
         @row(@cell(@code(Dane))
              @cell(@code(ktCurrentUser))
              @cell(Grzegorz)
              @cell(@code(Dane.Grzegorz.param.xml))))

       Jeżeli wskazano tylko nazwę pliku, bez ścieżki, to plik znajduje się
       w folderze zależnym od wartości @link(CreateFile parametru @code(FileLocation)):
       @definitionList(@itemSpacing(Compact)
         @itemLabel(flAppData)
           @item(Plik parametrów jest w folderze %AppData%, w folderze podrzędnym
                 zależnym od nazwy firmy; plik nazywa się tak samo jak aplikacja.)
         @itemLabel(flAppDir)
           @item(Plik parametrów jest w folderze aplikacji.)
         @itemLabel(flCustom)
           @item(Plik parametrów jest  w bieżącym folderze.))

       @seeAlso(CreateFile)  }
    property  FileName:  TFileName
              read  fXMLFileName
              write SetXMLFileName;
  end { TGSkParamSaverXML };

  {: @abstract Błędy w obsłudze parametrów.

     Błędy w obsłudze parametrów sygnalizowane są jako zdarzenia takiego typu.

     @seeAlso(TGSkCustomParamSaver)
     @seeAlso(TGSkParamSaverRegistry)
     @seeAlso(TGSkParamSaverXML)  }
  EGSkParamSaver = class(EGSkPasLibError);


{: @abstract Udostępnia standardową klasę komponentów do przechowywania parametrów.

   Funkcja @name zwraca w wyniku standardową klasę komponentów do przechowywania
   parametrów. Jest to jedna z klas zdefiniowanych na bazie klasy
   @link(TGSkCustomParamSaver).

   @returns(Wynik funkcji zależy od zdefiniowanych zmiennych kompilatora:
            @table(@rowHead(@cell(Zmienna)
                            @cell(Znaczenie))
              @row(@cell(@code(ParamReg))
                   @cell(@link(GetStdParamSaverClass Standardowym) komponentem
                         przechowywania parametrów programu będzie komponent
                         @link(TGSkParamSaverRegistry).))
              @row(@cell(@code(ParamXML))
                   @cell(@link(GetStdParamSaverClass Standardowym) komponentem
                         przechowywania parametrów programu będzie komponent
                         @link(TGSkParamSaverXML).))
              @row(@cell(Nie jest zdefiniowana żadna z powyższych zmiennych
                         kompilatora)
                   @cell(Tak jakby była zdefiniowana zmienna @code(ParamReg)
                         czyli wynikiem funkcji jest klasa
                         @link(TGSkParamSaverRegistry).))))

   @seeAlso(CreateStdParamSaver)  }
function  GetStdParamSaverClass() : TGSkParamSaverClass;

{: @abstract Udostępnia standardowy komponent do przechowywania parametrów.

   Funkcja @name w wyniku udostępnia obiekt takiego typu jaki jest wynikiem
   wywołania funkcji @link(GetStdParamSaverClass).

   @param(AOwner Obiekt będący właścicielem utworzonego obiektu. Dla obiektów
                 tymczasowych najlepiej pozostawić ten parametr pusty (@nil).)

   @return(Wynikiem funkcji jest obiekt klasy takiej jaką daje funkcja
           @link(GetStdParamSaverClass).)

   @bold(Uwaga!) @br
   Aplikacja musi pamiętać o zwolnieniu obiektu zwróconego przez funkcję @name
   (przez wywołanie jego metody @code(Free).

   Przykład: @longCode(#
     with  CreateStdParamSaver()  do
       try
         WriteString('Baza', dbBaza.DatabaseName)
       finally
         Free()
       end ;
   #)

   @seeAlso(GetStdParamSaverClass)  }
function  CreateStdParamSaver(const   AOwner:  TComponent = nil)
                              : TGSkCustomParamSaver;

{: @deprecated

   Funkcja @name została zastąpiona przez identyczną funkcję
   @link(CreateStdParamSaver). Nowa funkcja ma bardziej jednoznaczną nazwę.

   Funkcja @name została zachowana wyłącznie dla zachowania zgodności wstecz.
   W nowych aplikacjach należy zawsze używać funkcję @link(CreateStdParamSaver).

  @seeAlso(CreateStdParamSaver)  }
function  GetStdParamSaver(const   AOwner:  TComponent = nil)
                           : TGSkCustomParamSaver;                   deprecated {$IFDEF UNICODE}'Używaj CreateStdParamSaver'{$ENDIF UNICODE};


implementation


uses
  {$IFDEF TraceParamSaver }
    GSkPasLib.Log, GSkPasLib.LogGExperts,
  {$ENDIF TraceParamSaver }
  GSkPasLib.AppUtils, GSkPasLib.ComponentUtils, GSkPasLib.XMLUtils,
  GSkPasLib.FileUtils, GSkPasLib.Tokens;


const
  gcsParamFileExtFmt = '.%s' + gcsParamFileExtXML;
  { XML Nodes }
  csRootNodeXML:  WideString = 'PARAMETERS';
  csKeyNodeXML:   WideString = 'KEY';
  csValueNodeXML: WideString = 'VALUE';
  { XML Attributes }
  csNameAttrXML:  WideString = 'name';
  csSizeAttrXML:  WideString = 'size';
  csTypeAttrXML:  WideString = 'type';
  { XML type codes }
  csValueDataType:  array[TGSkParamSaverDataType] of  Char = (
    'U',   // sdUnknown
    'S',   // sdString
    'I',   // sdInteger
    'B');  // sdBinary
  {$IFDEF TraceParamSaver }
    csParamSaverKeyType:  array[TGSkParamSaverKeyType] of  string = (
      'ktCurrentUser', 'ktLocalMachine');
    csParamSaverDataType:  array[TGSkParamSaverDataType] of  string = (
      'sdUnknown', 'sdString', 'sdInteger', 'sdBinary');
  {$ENDIF TraceParamSaver }
  {$INCLUDE Author.inc }


type
  THackXMLDocument = class(TXMLDocument);


const
  gcsParamSaverErrNotOpen  = 'Klucz nie jest otwarty';
  gcsParamSaverErrReadOnly = 'Klucz jest otwarty w trybie TYLKO-DO-CZYTANIA';
  gcsParamSaverErrIsOpen   = 'Klucz jest otwarty';
  gcsParamSaverErrConvErr  = 'Wartość zmiennej „%s” jest nieodpowiedna: %s';
  gcsParamSaverErrDataType = 'Typ wartości zmiennej „%s” jest nieodpowedni';
  gcsParamSaverErrSize     = 'Niepoprawny rozmiar watości „%s”';


{$REGION 'Local subroutines'}

procedure  ConvError(const   sClass, sMethod, sVariable, sValue:  string);
begin
  {$IFDEF TraceParamSaver }
    LogMessage('ConvError("%s", "%s", "%s", "%s")',
               [sClass, sMethod, sVariable, sValue]);
  {$ENDIF TraceParamSaver }
  raise  EGSkParamSaver.Create(sClass + '.' + sMethod,
                               Format(gcsParamSaverErrConvErr,
                                      [sVariable, sValue]))
end { ConvError };

{$ENDREGION 'Local subroutines'}


{$REGION 'Global subroutines'}

function  GetStdParamSaverClass() : TGSkParamSaverClass;
begin
  {$IFDEF ParamXML}
    Result := TGSkParamSaverXML
  {$ELSE}
    Result := TGSkParamSaverRegistry
  {$ENDIF}
end { GetStdParamSaverClass };


function  CreateStdParamSaver(const   AOwner:  TComponent)
                              : TGSkCustomParamSaver;
begin
  Result := GetStdParamSaverClass().Create(AOwner)
end { CreateStdParamSaver };


function  GetStdParamSaver(const   AOwner:  TComponent)
                           : TGSkCustomParamSaver;
begin
  Result := CreateStdParamSaver(AOwner)
end { GetStdParamSaver };

{$ENDREGION 'Global subroutines'}


{$REGION 'TGSkCustomParamSaver'}

procedure  TGSkCustomParamSaver.BeginUpdate();
begin
  InterlockedIncrement(FUpdateLevel)
end { BeginUpdate };


procedure  TGSkCustomParamSaver.CheckClosed();
begin
  {$IFDEF TraceParamSaver }
    LogMessage('TGSkCustomParamSaver.CheckClosed');
  {$ENDIF TraceParamSaver }
  if  FKeyState <> ksClosed  then
    raise  EGSkParamSaver.Create('TGSkCustomParamSaver.CheckClosed',
                                 gcsParamSaverErrIsOpen)
end { CheckClosed };


procedure  TGSkCustomParamSaver.CheckRead(const   sName:  string;
                                          const   DataType:  TGSkParamSaverDataType;
                                                  nSize:  Integer);
begin
  {$IFDEF TraceParamSaver }
    LogMessage('TGSkCustomParamSaver.CheckRead("%s", %s, %d)',
               [sName, csParamSaverDataType[DataType], nSize]);
  {$ENDIF TraceParamSaver }
  if  FKeyState = ksClosed  then
    raise EGSkParamSaver.Create('TGSkCustomParamSaver.CheckRead',
                                gcsParamSaverErrNotOpen);
  if  sName <> ''  then
    begin
      if  DataType <> sdUnknown  then
        begin
          if  not(GetDataType(sName) in [DataType, sdUnknown])  then
            raise  EGSkParamSaver.Create('TGSkCustomParamSaver.CheckRead',
                                         Format(gcsParamSaverErrDataType,
                                                [sName]));
          if  (nSize = 0)
              and (DataType = sdInteger)  then
            nSize := SizeOf(Integer)
        end { DataType <> sdUnknown };
      if  nSize > 0  then
        if  GetDataSize(sName) <> nSize  then
          if  GetDataSize(sName) > 0  then
            raise  EGSkParamSaver.Create('TGSkCustomParamSaver.CheckRead',
                                         Format(gcsParamSaverErrSize,
                                                [sName]))
    end { sName <> '' }
end { CheckRead };


procedure  TGSkCustomParamSaver.CheckWrite();
begin
  {$IFDEF TraceParamSaver }
    LogMessage('TGSkCustomParamSaver.CheckWrite');
  {$ENDIF TraceParamSaver }
  case  FKeyState  of
    ksClosed:
      raise  EGSkParamSaver.Create('TGSkCustomParamSaver.CheckWrite',
                                   gcsParamSaverErrNotOpen);
    ksReadOnly:
      raise  EGSkParamSaver.Create('TGSkCustomParamSaver.CheckWrite',
                                   gcsParamSaverErrReadOnly)
  end { case FKeyState }
end { CheckWrite };


procedure  TGSkCustomParamSaver.CloseKey();
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.CloseKey');
  {$ENDIF TraceParamSaver }
  InternalCloseKey();
  FKeyState := ksClosed;
  {$IFDEF TraceParamSaver }
    LogExit('TGSkCustomParamSaver.CloseKey')
  {$ENDIF TraceParamSaver }
end { CloseKey };


constructor  TGSkCustomParamSaver.Create(AOwner:  TComponent);
begin
  inherited
end { Create };


function  TGSkCustomParamSaver.DeleteKey(const   sKey:  string)
                                         : Boolean;
begin
  // CheckClosed();
  Result := False
end { DeleteKey };


function  TGSkCustomParamSaver.DeleteValue(const   sName:  string)
                                           : Boolean;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.DeleteValue("%s")', [sName]);
  {$ENDIF TraceParamSaver }
  CheckWrite();
  Result := False;
  {$IFDEF TraceParamSaver }
    LogExit('TGSkCustomParamSaver.DeleteValue')
  {$ENDIF TraceParamSaver }
end { DeleteValue };


destructor  TGSkCustomParamSaver.Destroy();
begin
  inherited
end { Destroy };


procedure  TGSkCustomParamSaver.EndUpdate();
begin
  InterlockedDecrement(FUpdateLevel)
end { EndUpdate };


function  TGSkCustomParamSaver.GetKey() : string;
begin
  if  FKeyState <> ksClosed  then
    Result := FLastKey
  else
    Result := ''
end { GetKey };


procedure  TGSkCustomParamSaver.GetKeyNames(const   Names:  TStrings);
begin
  CheckRead();
  Names.Clear()
end { GetKeyNames };


procedure  TGSkCustomParamSaver.GetValueNames(const   Names:  TStrings);
begin
  CheckRead();
  Names.Clear()
end { GetKeyValues };


function  TGSkCustomParamSaver.HasSubKeys() : Boolean;
begin
  CheckRead();
  Result := False
end { HasSubKeys };


function  TGSkCustomParamSaver.KeyExists(const   sKey:  string)
                                         : Boolean;
begin
  // CheckRead();
  Result := False
end { KeyExists };


function  TGSkCustomParamSaver.OpenKey(const   sKey:  string;
                                       const   bCanCreate:  Boolean)
                                       : Boolean;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.OpenKey("%s", %s)',
             [sKey, BoolToStr(bCanCreate, True)]);
  {$ENDIF TraceParamSaver }
  FLastKey := sKey;
  Result := InternalOpenKey(sKey, bCanCreate);
  if  Result  then
    FKeyState := ksReadWrite
  else
    FKeyState := ksClosed;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.OpenKey')
  {$ENDIF TraceParamSaver }
end { OpenKey };


function  TGSkCustomParamSaver.OpenKeyReadOnly(const   sKey:  string)
                                               : Boolean;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.OpenKeyReadOnly("%s")', [sKey]);
  {$ENDIF TraceParamSaver }
  FLastKey := sKey;
  Result := InternalOpenKeyReadOnly(sKey);
  if  Result  then
    FKeyState := ksReadOnly
  else
    FKeyState := ksClosed;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.OpenKeyReadOnly');
  {$ENDIF TraceParamSaver }
end { OpenKeyReadOnly };


function  TGSkCustomParamSaver.ReadBinaryData(const   sName:  string;
                                              var     Buffer;
                                              const   nBufferSize:  Integer)
                                              : Integer;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadBinaryData("%s", ..., %d)',
             [sName, nBufferSize]);
  {$ENDIF TraceParamSaver }
  CheckRead(sName, sdBinary);
  if  GetDataSize(sName) > nBufferSize   then
    raise  EGSkParamSaver.Create('TGSkCustomParamSaver.ReadBinaryData',
                                 Format(gcsParamSaverErrSize,
                                        [sName]));
  Result := 0;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.ReadBinaryData')
  {$ENDIF TraceParamSaver }
end { ReadBinaryData };


function  TGSkCustomParamSaver.ReadBool(const   sName:  string;
                                        const   bDefault:  Boolean)
                                        : Boolean;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadBool("%s", %s)',
             [sName, BoolToStr(bDefault, True)]);
  {$ENDIF TraceParamSaver }
  CheckRead(sName, sdInteger);
  Result := bDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.ReadBool')
  {$ENDIF TraceParamSaver }
end { ReadBool };


function  TGSkCustomParamSaver.ReadChar(const   sName:  string;
                                        const   chDefault:  Char)
                                        : Char;
var
  sBuf:  string;

begin  { ReadChar }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadChar("%s", ''%s'')',
             [sName, chDefault]);
  {$ENDIF TraceParamSaver }
  sBuf := ReadString(sName, chDefault);
  if  Length(sBuf) = 1  then
    Result := sBuf[1]
  else
    raise  EGSkParamSaver.Create('TGSkCustomParamSaver.ReadChar',
                                 Format('Incorrect length of data (%d, „%s”)',
                                        [Length(sBuf), sBuf]));
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.ReadChar')
  {$ENDIF TraceParamSaver }
end { ReadChar };


function  TGSkCustomParamSaver.ReadColor(const   sName:  string;
                                         const   clDefault:  TColor)
                                         : TColor;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadColor("%s", %s)',
             [sName, ColorToString(clDefault)]);
  {$ENDIF TraceParamSaver }
  Result := StringToColor(ReadString(sName, ColorToString(clDefault)));
  {$IFDEF TraceParamSaver }
    LogValue('Result', ColorToString(Result));
    LogExit('TGSkCustomParamSaver.ReadColor')
  {$ENDIF TraceParamSaver }
end { ReadColor };


function  TGSkCustomParamSaver.ReadCurrency(const   sName:  string;
                                            const   nDefault:  Currency)
                                            : Currency;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadCurrency("%s", %m)',
             [sName, nDefault]);
  {$ENDIF TraceParamSaver }
  CheckRead(sName, sdBinary, SizeOf(Currency));
  Result := nDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.ReadCurrency')
  {$ENDIF TraceParamSaver }
end { ReadCurrency };


function  TGSkCustomParamSaver.ReadDate(const   sName:  string;
                                        const   dDefault:  TDateTime)
                                        : TDateTime;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadDate("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dDefault)]);
  {$ENDIF TraceParamSaver }
  CheckRead(sName, sdBinary, SizeOf(TDateTime));
  Result := dDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.ReadDate')
  {$ENDIF TraceParamSaver }
end { ReadDate };


function  TGSkCustomParamSaver.ReadDateTime(const   sName:  string;
                                            const   dDefault:  TDateTime)
                                            : TDateTime;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadDateTime("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dDefault)]);
  {$ENDIF TraceParamSaver }
  CheckRead(sName, sdBinary, SizeOf(TDateTime));
  Result := dDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.ReadDateTime')
  {$ENDIF TraceParamSaver }
end { ReadDateTime };


function  TGSkCustomParamSaver.ReadFloat(const   sName:  string;
                                         const   nDefault:  Double)
                                         : Double;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadFloat("%s", %f)',
             [sName, nDefault]);
  {$ENDIF TraceParamSaver }
  CheckRead(sName, sdBinary, SizeOf(Double));
  Result := nDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.ReadFloat')
  {$ENDIF TraceParamSaver }
end { ReadFloat };


function  TGSkCustomParamSaver.ReadFont(const   sName:  string;
                                        const   Default:  TFont)
                                        : TFont;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadFont("%s", [%s])',
             [sName, FontToString(Default)]);
  {$ENDIF TraceParamSaver }
  Result := TFont.Create();
  StringToFont(ReadString(sName,
                          FontToString(Default)),
               Result);
  {$IFDEF TraceParamSaver }
    LogValue('Result', FontToString(Result));
    LogExit('TGSkCustomParamSaver.ReadFont')
  {$ENDIF TraceParamSaver }
end { ReadFont };


function  TGSkCustomParamSaver.ReadInteger(const   sName:  string;
                                           const   nDefault:  Integer)
                                           : Integer;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadInteger("%s", %d)',
             [sName, nDefault]);
  {$ENDIF TraceParamSaver }
  CheckRead(sName, sdInteger);
  Result := nDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.ReadInteger')
  {$ENDIF TraceParamSaver }
end { ReadInteger };


function  TGSkCustomParamSaver.ReadString(const   sName, sDefault:  string)
                                          : string;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadString("%s", "%s")',
             [sName, sDefault]);
  {$ENDIF TraceParamSaver }
  CheckRead(sName, sdString);
  Result := sDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.ReadString')
  {$ENDIF TraceParamSaver }
end { ReadString };


function  TGSkCustomParamSaver.ReadTime(const   sName:  string;
                                        const   dDefault:  TDateTime)
                                        : TDateTime;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkCustomParamSaver.ReadTime("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dDefault)]);
  {$ENDIF TraceParamSaver }
  CheckRead(sName, sdBinary, SizeOf(TDateTime));
  Result := dDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkCustomParamSaver.ReadTime')
  {$ENDIF TraceParamSaver }
end { ReadTime };


procedure  TGSkCustomParamSaver.SetKeyType(const   Value:  TGSkParamSaverKeyType);
begin
  {$IFDEF TraceParamSaver }
    LogMessage('TGSkCustomParamSaver.SetKeyType(%s)',
               [csParamSaverKeyType[Value]]);
  {$ENDIF TraceParamSaver }
  FKeyType := Value
end { SetKeyType };


function  TGSkCustomParamSaver.ValueExists(const   sName:  string)
                                           : Boolean;
begin
  CheckRead();
  Result := False
end { ValueExists };


procedure  TGSkCustomParamSaver.WriteBinaryData(const   sName:  string;
                                                var   Buffer;
                                                const   nBufferSize:  Integer);
begin
  CheckWrite()
end { WriteBinaryData };


procedure  TGSkCustomParamSaver.WriteBool(const   sName:  string;
                                          const   bValue:  Boolean);
begin
  CheckWrite()
end { WriteBool };


procedure  TGSkCustomParamSaver.WriteChar(const   sName:  string;
                                          const   chValue:  Char);
begin
  WriteString(sName, chValue)
end { WriteChar };


procedure  TGSkCustomParamSaver.WriteColor(const   sName:  string;
                                           const   clValue:  TColor);
begin
  WriteString(sName, ColorToString(clValue))
end { WriteColor };


procedure  TGSkCustomParamSaver.WriteCurrency(const   sName:  string;
                                              const   nValue:  Currency);
begin
  CheckWrite()
end { WriteCurrency };


procedure  TGSkCustomParamSaver.WriteDate(const   sName:  string;
                                          const   dValue:  TDateTime);
begin
  CheckWrite()
end { WriteDate };


procedure  TGSkCustomParamSaver.WriteDateTime(const   sName:  string;
                                              const   dValue:  TDateTime);
begin
  CheckWrite()
end { WriteDateTime };


procedure  TGSkCustomParamSaver.WriteFloat(const   sName:  string;
                                           const   nValue:  Double);
begin
  CheckWrite()
end { WriteFloat };


procedure  TGSkCustomParamSaver.WriteFont(const   sName:  string;
                                          const   Font:  TFont);
begin
  WriteString(sName, FontToString(Font))
end { WriteFont };


procedure  TGSkCustomParamSaver.WriteHex(const   sName:  string;
                                         const   nValue:  Integer);
begin
  CheckWrite()
end { WriteHex };


procedure  TGSkCustomParamSaver.WriteInteger(const   sName:  string;
                                             const   nValue:  Integer);
begin
  CheckWrite()
end { WriteInteger };


procedure  TGSkCustomParamSaver.WriteString(const   sName, sValue:  string);
begin
  CheckWrite()
end { WriteString };


procedure  TGSkCustomParamSaver.WriteTime(const   sName:  string;
                                          const   dValue:  TDateTime);
begin
  CheckWrite()
end { WriteTime };

{$ENDREGION 'TGSkCustomParamSaver'}


{$REGION 'TGSkParamSaverRegistry'}

constructor  TGSkParamSaverRegistry.Create(AOwner:  TComponent);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.Create');
  {$ENDIF TraceParamSaver }
  inherited;
  FReg := TRegistry.Create();
  SetKeyType(KeyType);
  if  not(csDesigning in ComponentState)  then
    FBaseKey := GetAppRegistryKey();
  {$IFDEF TraceParamSaver }
    LogValue('BaseKey', FBaseKey);
    LogExit('TGSkParamSaverRegistry.Create')
  {$ENDIF TraceParamSaver }
end { Create };


function  TGSkParamSaverRegistry.DeleteKey(const   sKey:  string)
                                           : Boolean;
var
  lsKey:  string;

begin  { DeleteKey }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.DeleteKey("%s")',
             [sKey]);
  {$ENDIF TraceParamSaver }
  lsKey := GetKey(sKey);
  inherited DeleteKey(lsKey);
  Result := FReg.DeleteKey(lsKey);
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.DeleteKey')
  {$ENDIF TraceParamSaver }
end { DeleteKey };


function  TGSkParamSaverRegistry.DeleteValue(const   sName:  string)
                                             : Boolean;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.DeleteValue("%s")',
             [sName]);
  {$ENDIF TraceParamSaver }
  inherited DeleteValue(sName);
  Result := FReg.DeleteValue(sName);
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.DeleteValue')
  {$ENDIF TraceParamSaver }
end { DeleteValue };


destructor  TGSkParamSaverRegistry.Destroy();
begin
  {$IFDEF TraceParamSaver }
    LogMessage('TGSkParamSaverRegistry.Destroy');
  {$ENDIF TraceParamSaver }
  FReg.Free();
  inherited
end { Destroy };


function  TGSkParamSaverRegistry.GetDataSize(const   sName:  string)
                                             : Integer;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.GetDataSize("%s")', [sName]);
  {$ENDIF TraceParamSaver }
  Result := FReg.GetDataSize(sName);
  if  FReg.GetDataType(sName) in [rdString, rdExpandString]  then
    Dec(Result);   // NUL
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.GetDataSize("%s")')
  {$ENDIF TraceParamSaver }
end { GetDataSize };


function  TGSkParamSaverRegistry.GetDataType(const   sName:  string)
                                             : TGSkParamSaverDataType;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.GetDataType("%s")', [sName]);
  {$ENDIF TraceParamSaver }
  case  FReg.GetDataType(sName)  of
    rdInteger:
      Result := sdInteger;
    rdBinary:
      Result := sdBinary;
    rdUnknown:
      Result := sdUnknown;
    else
      Result := sdString
  end { case };
  {$IFDEF TraceParamSaver }
    LogValue('Result', csParamSaverDataType[Result]);
    LogExit('TGSkParamSaverRegistry.GetDataType')
  {$ENDIF TraceParamSaver }
end { GetDataType };


function  TGSkParamSaverRegistry.GetKey(const   sKey:  string)
                                        : string;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.GetKey("%s")',
             [sKey]);
  {$ENDIF TraceParamSaver }
  if  sKey = ''  then
    Result := ExcludeTrailingPathDelimiter(FBaseKey)
  else if  sKey[1] <> PathDelim  then
    Result := IncludeTrailingPathDelimiter(FBaseKey) + sKey
  else
    Result := sKey;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.GetKey')
  {$ENDIF TraceParamSaver }
end { GetKey };


procedure  TGSkParamSaverRegistry.GetKeyNames(const   Names:  TStrings);
begin
  Names.BeginUpdate();
  try
    inherited;
    FReg.GetKeyNames(Names)
  finally
    Names.EndUpdate()
  end { try-finally }
end { GetKeyNames };


procedure  TGSkParamSaverRegistry.GetValueNames(const   Names:  TStrings);
begin
  Names.BeginUpdate();
  try
    inherited;
    FReg.GetValueNames(Names)
  finally
    Names.EndUpdate()
  end { try-finally }
end { GetValueNames };


function  TGSkParamSaverRegistry.HasSubKeys() : Boolean;
begin
  inherited HasSubKeys();
  Result := FReg.HasSubKeys()
end { HasSubKeys };


procedure  TGSkParamSaverRegistry.InternalCloseKey();
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.InternalCloseKey');
  {$ENDIF TraceParamSaver }
  inherited;
  FReg.CloseKey();
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverRegistry.InternalCloseKey')
  {$ENDIF TraceParamSaver }
end { InternalCloseKey };


function  TGSkParamSaverRegistry.InternalOpenKey(const   sKey:  string;
                                                 const   bCanCreate:  Boolean)
                                                 : Boolean;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.InternalOpenKey("%s", %s)',
             [sKey, BoolToStr(bCanCreate, True)]);
  {$ENDIF TraceParamSaver }
  Result := FReg.OpenKey(GetKey(sKey), bCanCreate);
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.InternalOpenKey')
  {$ENDIF TraceParamSaver }
end { InternalOpenKey };


function  TGSkParamSaverRegistry.InternalOpenKeyReadOnly(const   sKey:  string)
                                                         : Boolean;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.InternalOpenKeyReadOnly("%s")',
             [sKey]);
  {$ENDIF TraceParamSaver }
  Result := FReg.OpenKeyReadOnly(GetKey(sKey));
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.InternalOpenKeyReadOnly')
  {$ENDIF TraceParamSaver }
end { InternalOpenKeyReadOnly };


function  TGSkParamSaverRegistry.KeyExists(const   sKey:  string)
                                           : Boolean;
var
  lsKey:  string;

begin  { KeyExists }
  lsKey := GetKey(sKey);
  inherited KeyExists(lsKey);
  Result := FReg.KeyExists(lsKey)
end { KeyExists };


function  TGSkParamSaverRegistry.ReadBinaryData(const   sName:  string;
                                                var     Buffer;
                                                const   nBufferSize:  Integer)
                                                : Integer;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.ReadBinaryData("%s", ..., %d)',
             [sName, nBufferSize]);
  {$ENDIF TraceParamSaver }
  inherited ReadBinaryData(sName, Buffer, nBufferSize);
  Result := FReg.ReadBinaryData(sName, Buffer, nBufferSize);
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.ReadBinaryData')
  {$ENDIF TraceParamSaver }
end { ReadBinaryData };


function  TGSkParamSaverRegistry.ReadBool(const   sName:  string;
                                          const   bDefault:  Boolean)
                                          : Boolean;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.ReadBool("%s", %s)',
             [sName, BoolToStr(bDefault, True)]);
  {$ENDIF TraceParamSaver }
  inherited ReadBool(sName, bDefault);
  if  FReg.ValueExists(sName)  then
    Result := FReg.ReadBool(sName)
  else
    Result := bDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.ReadBool')
  {$ENDIF TraceParamSaver }
end { ReadBool };


function  TGSkParamSaverRegistry.ReadCurrency(const   sName:  string;
                                              const   nDefault:  Currency)
                                              : Currency;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.ReadCurrency("%s", %m)',
             [sName, nDefault]);
  {$ENDIF TraceParamSaver }
  inherited ReadCurrency(sName, nDefault);
  if  FReg.ValueExists(sName)  then
    Result := FReg.ReadCurrency(sName)
  else
    Result := nDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.ReadCurrency')
  {$ENDIF TraceParamSaver }
end { ReadCurrency };


function  TGSkParamSaverRegistry.ReadDate(const   sName:  string;
                                          const   dDefault:  TDateTime)
                                          : TDateTime;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.ReadDate("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dDefault)]);
  {$ENDIF TraceParamSaver }
  inherited ReadDate(sName, dDefault);
  if  FReg.ValueExists(sName)  then
    Result := FReg.ReadDate(sName)
  else
    Result := dDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.ReadDate')
  {$ENDIF TraceParamSaver }
end { ReadDate };


function  TGSkParamSaverRegistry.ReadDateTime(const   sName:  string;
                                              const   dDefault:  TDateTime)
                                              : TDateTime;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.ReadDateTime("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dDefault)]);
  {$ENDIF TraceParamSaver }
  inherited ReadDateTime(sName, dDefault);
  if  FReg.ValueExists(sName)  then
    Result := FReg.ReadDateTime(sName)
  else
    Result := dDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.ReadDateTime')
  {$ENDIF TraceParamSaver }
end { ReadDateTime };


function  TGSkParamSaverRegistry.ReadFloat(const   sName:  string;
                                           const   nDefault:  Double)
                                           : Double;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.ReadFloat("%s", %f)',
             [sName, nDefault]);
  {$ENDIF TraceParamSaver }
  inherited ReadFloat(sName, nDefault);
  if  FReg.ValueExists(sName)  then
    Result := FReg.ReadFloat(sName)
  else
    Result := nDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.ReadFloat')
  {$ENDIF TraceParamSaver }
end { ReadFloat };


function  TGSkParamSaverRegistry.ReadInteger(const   sName:  string;
                                             const   nDefault:  Integer)
                                             : Integer;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.ReadInteger("%s", %d)',
             [sName, nDefault]);
  {$ENDIF TraceParamSaver }
  inherited ReadInteger(sName, nDefault);
  if  FReg.ValueExists(sName)  then
    Result := FReg.ReadInteger(sName)
  else
    Result := nDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.ReadInteger')
  {$ENDIF TraceParamSaver }
end { ReadInteger };


function  TGSkParamSaverRegistry.ReadString(const   sName, sDefault:  string)
                                            : string;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.ReadString("%s", "%s")',
             [sName, sDefault]);
  {$ENDIF TraceParamSaver }
  inherited ReadString(sName, sDefault);
  if  FReg.ValueExists(sName)  then
    Result := FReg.ReadString(sName)
  else
    Result := sDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.ReadString')
  {$ENDIF TraceParamSaver }
end { ReadString };


function  TGSkParamSaverRegistry.ReadTime(const   sName:  string;
                                          const   dDefault:  TDateTime)
                                          : TDateTime;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.ReadTime("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dDefault)]);
  {$ENDIF TraceParamSaver }
  inherited ReadTime(sName, dDefault);
  if  FReg.ValueExists(sName)  then
    Result := FReg.ReadTime(sName)
  else
    Result := dDefault;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverRegistry.ReadTime')
  {$ENDIF TraceParamSaver }
end { ReadTime };


procedure  TGSkParamSaverRegistry.SetKeyType(const   Value:  TGSkParamSaverKeyType);
begin
  inherited;
  case  Value  of
    ktCurrentUser:
      FReg.RootKey := HKEY_CURRENT_USER;
    ktLocalMachine:
      FReg.RootKey := HKEY_LOCAL_MACHINE
  end { case }
end { SetKeyType };


function  TGSkParamSaverRegistry.ValueExists(const   sName:  string)
                                             : Boolean;
begin
  inherited ValueExists(sName);
  Result := FReg.ValueExists(sName)
end { ValueExists };


procedure  TGSkParamSaverRegistry.WriteBinaryData(const   sName:  string;
                                                  var     Buffer;
                                                  const   nBufferSize:  Integer);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.WriteBinaryData("%s", ..., %d)',
             [sName, nBufferSize]);
  {$ENDIF TraceParamSaver }
  inherited;
  FReg.WriteBinaryData(sName, Buffer, nBufferSize);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverRegistry.WriteBinaryData')
  {$ENDIF TraceParamSaver }
end { WriteBinaryData };


procedure  TGSkParamSaverRegistry.WriteBool(const   sName:  string;
                                            const   bValue:  Boolean);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.WriteBool("%s", %s',
             [sName, BoolToStr(bValue, True)]);
  {$ENDIF TraceParamSaver }
  inherited;
  FReg.WriteBool(sName, bValue);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverRegistry.WriteBool')
  {$ENDIF TraceParamSaver }
end { WriteBool };


procedure  TGSkParamSaverRegistry.WriteCurrency(const   sName:  string;
                                                const   nValue:  Currency);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.WriteCurrency("%s", %m)',
             [sName, nValue]);
  {$ENDIF TraceParamSaver }
  inherited;
  FReg.WriteCurrency(sName, nValue);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverRegistry.WriteCurrency')
  {$ENDIF TraceParamSaver }
end { WriteCurrency };



procedure  TGSkParamSaverRegistry.WriteDate(const   sName:  string;
                                            const   dValue:  TDateTime);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.WriteDate("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dValue)]);
  {$ENDIF TraceParamSaver }
  inherited;
  FReg.WriteDate(sName, dValue);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverRegistry.WriteDate')
  {$ENDIF TraceParamSaver }
end { WriteDate };


procedure  TGSkParamSaverRegistry.WriteDateTime(const   sName:  string;
                                                const   dValue:  TDateTime);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.WriteDateTime("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dValue)]);
  {$ENDIF TraceParamSaver }
  inherited;
  FReg.WriteDateTime(sName, dValue);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverRegistry.WriteDateTime')
  {$ENDIF TraceParamSaver }
end { WriteDateTime };


procedure  TGSkParamSaverRegistry.WriteFloat(const   sName:  string;
                                             const   nValue:  Double);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.WriteFloat("%s", %f)',
             [sName, nValue]);
  {$ENDIF TraceParamSaver }
  inherited;
  FReg.WriteFloat(sName, nValue);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverRegistry.WriteFloat')
  {$ENDIF TraceParamSaver }
end { WriteFloat };


procedure  TGSkParamSaverRegistry.WriteHex(const   sName:  string;
                                           const   nValue:  Integer);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.WriteHex("%s", $%x)',
             [sName, nValue]);
  {$ENDIF TraceParamSaver }
  inherited;
  FReg.WriteInteger(sName, nValue);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverRegistry.WriteHex')
  {$ENDIF TraceParamSaver }
end { WriteHex };


procedure  TGSkParamSaverRegistry.WriteInteger(const   sName:  string;
                                               const   nValue:  Integer);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.WriteInteger("%s", %d)',
             [sName, nValue]);
  {$ENDIF TraceParamSaver }
  inherited;
  FReg.WriteInteger(sName, nValue);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverRegistry.WriteInteger')
  {$ENDIF TraceParamSaver }
end { WriteInteger };


procedure  TGSkParamSaverRegistry.WriteString(const   sName, sValue:  string);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.WriteString("%s", "%s")',
             [sName, sValue]);
  {$ENDIF TraceParamSaver }
  inherited;
  FReg.WriteString(sName, sValue);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverRegistry.WriteString')
  {$ENDIF TraceParamSaver }
end { WriteString };


procedure  TGSkParamSaverRegistry.WriteTime(const   sName:  string;
                                            const   dValue:  TDateTime);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverRegistry.WriteTime("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dValue)]);
  {$ENDIF TraceParamSaver }
  inherited;
  FReg.WriteTime(sName, dValue);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverRegistry.WriteTime')
  {$ENDIF TraceParamSaver }
end { WriteTime };

{$ENDREGION 'TGSkParamSaverRegistry'}


{$REGION 'TGSkParamSaverXML'}

constructor  TGSkParamSaverXML.Create(AOwner:  TComponent);
begin
  inherited;
  Init()
end { Create };


constructor  TGSkParamSaverXML.CreateFile(const   AOwner:  TComponent;
                                          const   FileLocation:  TGSkParamSaverFileLocation;
                                          const   sFilePath:  TFileName);
begin
  inherited Create(AOwner);
  fFileLocation := FileLocation;
  fsCustomDir := sFilePath;
  Init()
end { CreateFile };


function  TGSkParamSaverXML.DeleteKey(const   sKey:  string)
                                      : Boolean;
var
  nInd:  Integer;
  iNode:  IXMLNode;

begin  { DeleteKey }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.DeleteKey(%s)',
             [sKey]);
  {$ENDIF TraceParamSaver }
  inherited DeleteKey(sKey);
  iNode := LocateKey(sKey, False);
  Result := False;
  if  iNode <> nil  then
    begin
      iNode := iNode.ParentNode;
      nInd := IndexOfNode(iNode, csKeyNodeXML, sKey);
      if  nInd >= 0  then
        begin
          Result := iNode.ChildNodes.Delete(nInd) >= 0;
          if  Result  then
            if  fXML.FileName <> ''  then
              SaveToFile(False);
        end { nInd >= 0 }
    end { iNode <> nil };
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.DeleteKey')
  {$ENDIF TraceParamSaver }
end { DeleteKey };


function  TGSkParamSaverXML.DeleteValue(const   sName:  string)
                                        : Boolean;
var
  nInd:  Integer;

begin  { DeleteValue }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.DeleteValue("%s")',
             [sName]);
  {$ENDIF TraceParamSaver }
  inherited DeleteValue(sName);
  nInd := IndexOfNode(fiCurrNode, csValueNodeXML, sName);
  if  nInd >= 0  then
    Result := fiCurrNode.ChildNodes.Delete(nInd) >= 0
  else
    Result := False;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.DeleteValue')
  {$ENDIF TraceParamSaver }
end { DeleteValue };


destructor  TGSkParamSaverXML.Destroy();
begin
  SaveToFile(True);
  try
    fiCurrNode := nil
  except
    { OK }
  end { try-ignore };
  fXML.Free();
  inherited
end { Destroy };


function  TGSkParamSaverXML.GetStdFileName() : string;

var
  sExt:  string;
  sDir:  string;

begin  { GetStdFileName }
  case  KeyType  of
    ktCurrentUser:
      sExt := Format(gcsParamFileExtFmt, [GetLocalUserName()]);
    ktLocalMachine:
      sExt := gcsParamFileExtXML
  end { case };
  case  fFileLocation  of
    flAppData:
      sDir := IncludeTrailingPathDelimiter(GetAppdataFolder())
                + gcsCompanyName + PathDelim
                + ChangeFileExt(ExtractFileName(GetModuleFileName()), '');
    flAppDir:
      sDir := ExtractFilePath(GetModuleFileName());
    flCustom:
      sDir := fsCustomDir
  end { fFileLocation };
  Result := IncludeTrailingPathDelimiter(sDir)
            + ChangeFileExt(ExtractFileName(GetModuleFileName()),
                            sExt);
  {$IFDEF TraceParamSaver }
    LogMessage('TGSkParamSaverXML.GetStdFileName() -> ' + Result)
  {$ENDIF TraceParamSaver }
end { GetStdFileName };


procedure  TGSkParamSaverXML.GetKeyNames(const   Names:  TStrings);
begin
  inherited;
  Names.BeginUpdate();
  try
    inherited;
    GetNodeNames(csKeyNodeXML, Names)
  finally
    Names.EndUpdate()
  end { try-finally }
end { GetKeyNames };


procedure  TGSkParamSaverXML.GetNodeNames(const   sNodeKind:  WideString;
                                          const   Nodes:  TStrings);
var
  iNode:  IXMLNode;

  procedure  AddNode(const   iNode:  IXMLNode;
                     const   Nodes:  TStrings);
  var
    iName:  IXMLNode;

  begin  { AddNode }
    iName := iNode.AttributeNodes.FindNode(csNameAttrXML);
    if  iName <> nil  then
      Nodes.AddObject(iName.Text, TObject(iNode))
    else
      Nodes.AddObject('', TObject(iNode))
  end { AddNode };

begin  { GetNodeNames }
  Nodes.BeginUpdate();
  try
    Nodes.Clear();
    iNode := fiCurrNode.ChildNodes.FindNode(sNodeKind);
     if  iNode <> nil  then
//       if  iNode.Collection <> nil  then
//         for  nInd := 0  to  iNode.Collection.Count - 1  do
//           AddNode(iNode.Collection.Nodes[nInd], Nodes)
//       else
//         AddNode(iNode, Nodes)
//     ^^^ Powyższy algorytm nie działa (przestał działać?)
       repeat
         if  iNode.NodeName = sNodeKind  then
           AddNode(iNode, Nodes);
         iNode := iNode.NextSibling()
       until  iNode = nil
  finally
    Nodes.EndUpdate()
  end { try-finally }
end { GetNodeNames };


procedure  TGSkParamSaverXML.GetValueNames(const   Names:  TStrings);
begin
  Names.BeginUpdate();
  try
    inherited;
    GetNodeNames(csValueNodeXML, Names)
  finally
    Names.EndUpdate()
  end { try-finally }
end { GetValueNames };


function  TGSkParamSaverXML.HasSubKeys() : Boolean;
begin
  inherited HasSubkeys();
  Result := fiCurrNode.ChildNodes.FindNode(csKeyNodeXML) <> nil
end { HasSubKeys };


function  TGSkParamSaverXML.IndexOfNode(const   iNode:  IXMLNode;
                                        const   sNodeKind:  WideString;
                                        const   sNodeName:  string)
                                        : Integer;
var
  iTemp:  IXMLNode;
  iAttr:  IXMLNode;

begin  { IndexOfNode }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.IndexOfNode(%s, "%s", "%s")',
             [GetNodePath(iNode), sNodeKind, sNodeName]);
  {$ENDIF TraceParamSaver }
  Result := -1;
  iTemp := iNode.ChildNodes.First();
  while  iTemp <> nil  do
    begin
      Inc(Result);
      if  iTemp.NodeName = sNodeKind  then
        begin
          iAttr := iTemp.AttributeNodes.FindNode(csNameAttrXML);
          if  iAttr <> nil  then
            begin
              if  iAttr.Text = sNodeName  then
                begin
                  {$IFDEF TraceParamSaver }
                    LogResult(Result);
                    LogExit('TGSkParamSaverXML.IndexOfNode');
                  {$ENDIF TraceParamSaver }
                  Exit
                end { iAttr.Text = sNodeName }
            end { iAttr <> nil }
          else
            begin
              if  sNodeName = ''  then
                begin
                  {$IFDEF TraceParamSaver }
                    LogResult(Result);
                    LogExit('TGSkParamSaverXML.IndexOfNode');
                  {$ENDIF TraceParamSaver }
                  Exit
                end { sNodeName = '' }
            end { iAttr = nil }
        end { iTemp.NodeName = sNodeKind };
      iTemp := iTemp.NextSibling()
    end { while iTemp <> nil };
  Result := -1;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.IndexOfNode');
  {$ENDIF TraceParamSaver }
end { IndexOfNode };


procedure  TGSkParamSaverXML.Init();
begin
  fXML := TXMLDocument.Create(Self);
  // FXML.NodeIndentStr := ' ';
  fXML.Options := [doAutoSave];
  if  not(csDesigning in ComponentState)  then
    begin
      ActivateXML(fXML, True);
      fXMLFileName := GetStdFileName();
      if  FileExists(fXMLFileName)  then
        fXML.LoadFromFile(fXMLFileName)
    end { csDesigning not in ComponentState };
  THackXMLDocument(fXML).SetModified(False)
end { Init };


procedure  TGSkParamSaverXML.InternalCloseKey();
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.InternalCloseKey');
  {$ENDIF TraceParamSaver }
  inherited;
  try
    fiCurrNode := nil
  except
    { OK }
  end { try-except };
  SaveToFile(False);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.InternalCloseKey')
  {$ENDIF TraceParamSaver }
end { InternalCloseKey };


function  TGSkParamSaverXML.InternalOpenKey(const   sKey:  string;
                                            const   bCanCreate:  Boolean)
                                            : Boolean;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.InternalOpenKey("%s", %s)',
             [sKey, BoolToStr(bCanCreate, True)]);
  {$ENDIF TraceParamSaver }
  fiCurrNode := nil;
  fiCurrNode := LocateKey(sKey, bCanCreate);
  Result := fiCurrNode <> nil;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.InternalOpenKey')
  {$ENDIF TraceParamSaver }
end { InternalOpenKey };


function  TGSkParamSaverXML.InternalOpenKeyReadOnly(const   sKey:  string)
                                                    : Boolean;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.InternalOpenKeyReadOnly("%s")',
             [sKey]);
  {$ENDIF TraceParamSaver }
  Result := InternalOpenKey(sKey, False);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.InternalOpenKeyReadOnly')
  {$ENDIF TraceParamSaver }
end { InternalOpenKeyReadOnly };


procedure  TGSkParamSaverXML.InternalWrite(const   sName, sValue:  string;
                                           const   DataType:  TGSkParamSaverDataType;
                                                   nSize:  Integer;
                                           const   bFreeString:  Boolean);
var
  nInd:  Integer;

begin  { InternalWrite }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.InternalWrite("%s", "%s", %s, %d, %s)',
             [sName, sValue,
              csParamSaverDataType[DataType],
              nSize, BoolToStr(bFreeString)]);
  {$ENDIF TraceParamSaver }
  nInd := IndexOfNode(fiCurrNode, csValueNodeXML, sName);
  if  nInd >= 0  then
    fiCurrNode.ChildNodes[nInd].Text := sValue
  else
    begin
      with  fiCurrNode.AddChild(csValueNodeXML)  do
        begin
          if  sName <> ''  then
            Attributes[csNameAttrXML] := sName;
          if  bFreeString  then
            ChildNodes.Add(OwnerDocument.CreateNode(sValue, ntCData))
          else
            Text := sValue
        end { with FCurrNode };
      nInd := IndexOfNode(fiCurrNode, csValueNodeXML, sName)
    end { nInd < 0 };
  Assert(nInd >= 0);
  with  fiCurrNode.ChildNodes[nInd]  do
    begin
      Attributes[csTypeAttrXML] := csValueDataType[DataType];
      if  DataType <> sdInteger  then
        begin
          if  (nSize = 0)
              and (DataType = sdInteger)  then
            nSize := SizeOf(Integer);
          Attributes[csSizeAttrXML] := IntToStr(nSize);
        end { DataType <> sdInteger }
    end { with };
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.InternalWrite')
  {$ENDIF TraceParamSaver }
end { InternalWrite };


function  TGSkParamSaverXML.KeyExists(const   sKey:  string)
                                      : Boolean;
var
  iKey:  IXMLNode;

begin  { KeyExists }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.KeyExists("%s")', [sKey]);
  {$ENDIF TraceParamSaver }
  inherited KeyExists(sKey);
  iKey := LocateKey(sKey, False);
  if  iKey <> nil  then
    Result := IndexOfNode(iKey, csKeyNodeXML, sKey) >= 0
  else
    Result := False;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.KeyExists')
  {$ENDIF TraceParamSaver }
end { KeyExists };


function  TGSkParamSaverXML.LocateKey(const   sKey:  string;
                                      const   bCanCreate:  Boolean)
                                      : IXMLNode;
var
  nIndS, nIndN:  Integer;

begin  { LocateKey }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.LocateKey("%s", %s)',
             [sKey, BoolToStr(bCanCreate)]);
  {$ENDIF TraceParamSaver }
  Result := fXML.DocumentElement;
  if  (Result = nil)
      and bCanCreate  then
    Result := fXML.AddChild(csRootNodeXML);
  if  Result <> nil  then
    with  TGSkCharTokens.Create(Self)  do
      try
        SetDelimiters(['\', '/']);
        Text := sKey;
        for  nIndS := 0  to  Count - 1  do
          begin
            nIndN := IndexOfNode(Result, csKeyNodeXML, Token[nIndS]);
            if  nIndN < 0  then
              begin
                if  bCanCreate  then
                  begin
                    for  nIndN := nIndS  to  Count - 1  do
                      begin
                        Result := Result.AddChild(csKeyNodeXML);
                        Result.Attributes[csNameAttrXML] := Token[nIndN]
                      end { for nIndN }
                  end { bCanCreate }
                else
                  Result := nil;
                Break
              end { nIndN < 0 };
            Result := Result.ChildNodes[nIndN]
          end { for nInd }
      finally
        Free()
      end { try-finally };
  {$IFDEF TraceParamSaver }
    LogValue('Result', GetNodePath(Result));
    LogExit('TGSkParamSaverXML.LocateKey')
  {$ENDIF TraceParamSaver }
end { LocateKey };


function  TGSkParamSaverXML.ReadBinaryData(const   sName:  string;
                                           var     Buffer;
                                           const   nBufferSize:  Integer)
                                           : Integer;
var
  sBuf:  AnsiString;
  pszInd:  PAnsiChar;
  xData:  array[0..$3fffffff] of Byte  absolute Buffer;
  nInd:  Integer;

  function  HexToDec(const   cHex:  AnsiChar)
                     : Integer;
  begin
    case  cHex  of
      '0'..'9':
        Result := Ord(cHex) - Ord('0');
      'A'..'F':
        Result := Ord(cHex) - Ord('A') + 10;
      'a'..'f':
        Result := Ord(cHex) - Ord('a') + 10
      else
        Result := 0
    end { case cHex }
  end { HexToDec };                               

begin  { ReadBinaryData }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.ReadBinaryData("%s", ..., %d)',
             [sName, nBufferSize]);
  {$ENDIF TraceParamSaver }
  {Result :=} inherited ReadBinaryData(sName, Buffer, nBufferSize);
  sBuf := AnsiString(InternalRead(sName, ''));
  if  Odd(Length(sBuf))  then
    raise  EGSkParamSaver.Create('ReadBinaryData',
                                 Format(gcsParamSaverErrSize, [sName]));
  pszInd := PAnsiChar(sBuf);
  nInd := 0;
  Result := 0;
  while  (pszInd^ <> #0)
         and (Result < nBufferSize)  do
    begin
      xData[nInd] := (HexToDec(pszInd^) shl 4)
                     or HexToDec((pszInd + 1)^);
      Inc(Result);
      Inc(nInd);
      Inc(pszInd, 2)
    end { while };
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.ReadBinaryData')
  {$ENDIF TraceParamSaver }
end { ReadBinaryData };


function  TGSkParamSaverXML.ReadBool(const   sName:  string;
                                     const   bDefault:  Boolean)
                                     : Boolean;
var
  sValue:  string;

begin  { ReadBool }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.ReadBool("%s", %s)',
             [sName, BoolToStr(bDefault, True)]);
  {$ENDIF TraceParamSaver }
  Result := inherited ReadBool(sName, bDefault);
  sValue := InternalRead(sName, BoolToStrXML(bDefault));
  try
    Result := StrToBoolXML(sValue)
  except
    ConvError('TGSkParamSaverXML', 'ReadBool', sName, sValue)
  end { try-except };
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.ReadBool')
  {$ENDIF TraceParamSaver }
end { ReadBool };


function  TGSkParamSaverXML.ReadCurrency(const   sName:  string;
                                         const   nDefault:  Currency)
                                         : Currency;
var
  sValue:  string;

begin  { ReadCurrency }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.ReadCurrency("%s", %m)',
             [sName, nDefault]);
  {$ENDIF TraceParamSaver }
  Result := inherited ReadCurrency(sName, nDefault);
  sValue := InternalRead(sName, CurrToStr(nDefault));
  try
    Result := StrToCurr(sValue)
  except
    ConvError('TGSkParamSaverXML', 'ReadCurrency', sName, sValue)
  end { try-except };
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.ReadCurrency')
  {$ENDIF TraceParamSaver }
end { ReadCurrency };


function  TGSkParamSaverXML.ReadDate(const   sName:  string;
                                     const   dDefault:  TDateTime)
                                     : TDateTime;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.ReadDate("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dDefault)]);
  {$ENDIF TraceParamSaver }
  { Result := } inherited ReadDate(sName, dDefault);
  Result := ReadDateTime(sName, dDefault);
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.ReadDate')
  {$ENDIF TraceParamSaver }
end { ReadDate };


function  TGSkParamSaverXML.ReadDateTime(const   sName:  string;
                                         const   dDefault:  TDateTime)
                                         : TDateTime;
var
  sDateTime:  string;

begin  { ReadDateTime }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.ReadDateTime("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dDefault)]);
  {$ENDIF TraceParamSaver }
  Result := inherited ReadDateTime(sName, dDefault);
  sDateTime := InternalRead(sName,
                            DateTimeToStrXML(dDefault));
  try
    Result := StrToDateTimeXML(sDateTime)
  except
    ConvError('TGSkParamSaverXML', 'ReadDateTime', sName, sDateTime)
  end { try-except };
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.ReadDateTime')
  {$ENDIF TraceParamSaver }
end { ReadDateTime};


function  TGSkParamSaverXML.ReadFloat(const   sName:  string;
                                      const   nDefault:  Double)
                                      : Double;
var
  sValue:  string;

begin  { ReadFloat }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.ReadFloat("%s", %f)',
             [sName, nDefault]);
  {$ENDIF TraceParamSaver }
  Result := inherited ReadFloat(sName, nDefault);
  sValue := InternalRead(sName, FloatToStrXML(nDefault));
  try
    Result := StrToFloatXML(sValue)
  except
    ConvError('TGSkParamSaverXML', 'ReadFloat', sName, sValue)
  end { try-except };
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.ReadFloat')
  {$ENDIF TraceParamSaver }
end { ReadFloat };


function  TGSkParamSaverXML.ReadInteger(const   sName:  string;
                                        const   nDefault:  Integer)
                                        : Integer;
var
  sValue:  string;

begin  { ReadInteger }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.ReadInteger("%s", %d)',
             [sName, nDefault]);
  {$ENDIF TraceParamSaver }
  Result := inherited ReadInteger(sName, nDefault);
  sValue := InternalRead(sName, IntToStr(nDefault));
  try
    Result := StrToInt(sValue)
  except
    ConvError('TGSkParamSaverXML', 'ReadInteger', sName, sValue)
  end { try-except };
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.ReadInteger')
  {$ENDIF TraceParamSaver }
end { ReadInteger };


function  TGSkParamSaverXML.ReadString(const   sName, sDefault:  string)
                                       : string;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.ReadString("%s", "%s")',
             [sName, sDefault]);
  {$ENDIF TraceParamSaver }
  Result := inherited ReadString(sName, sDefault);
  Result := InternalRead(sName, sDefault);
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.ReadString')
  {$ENDIF TraceParamSaver }
end { ReadString };


function  TGSkParamSaverXML.ReadTime(const   sName:  string;
                                     const   dDefault:  TDateTime)
                                     : TDateTime;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.ReadTime("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dDefault)]);
  {$ENDIF TraceParamSaver }
  {Result :=} inherited ReadTime(sName, dDefault);
  Result := ReadDateTime(sName, dDefault);
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.ReadTime')
  {$ENDIF TraceParamSaver }
end { ReadTime };


procedure  TGSkParamSaverXML.SaveToFile(const   bUnconditional:  Boolean);

var
  sDir:  string;

begin  { SaveToFile }
  if  (bUnconditional
         or (UpdateLevel = 0))
      and fXML.Modified  then
    if  fXMLFileName <> ''  then
      begin
        sDir := ExtractFileDir(fXMLFileName);
        if  sDir <> ''  then
          ForceDirectories(sDir);
        fXML.SaveToFile(fXMLFileName)
      end { fXMLFileName <> '' }
    else if  fXML.FileName <> ''  then
      fXML.SaveToFile()
end { SaveToFile };


procedure  TGSkParamSaverXML.SetKeyType(const   Value:  TGSkParamSaverKeyType);
begin
  inherited;
  FileName := GetStdFileName()
end { SetKeyType };


procedure  TGSkParamSaverXML.SetXMLFileName(const   Value:  TFileName);
begin
  if  not SameFileName(fXMLFileName, Value)  then
    begin
      if  not(csDesigning in ComponentState)  then
        SaveToFile(True);
      if  ExtractFileDir(Value) <> ''  then
        fXMLFileName := Value
      else
        case  fFileLocation  of
          flAppData:
            fXMLFileName := IncludeTrailingPathDelimiter(GetAppdataFolder())
                            + gcsCompanyName + PathDelim
                            + Value;
          flAppDir:
            fXMLFileName := ExtractFilePath(GetModuleFileName())
                            + Value;
          flCustom:
            fXMLFileName := Value
        end { fFileLocation };
      if  not(csDesigning in ComponentState)  then
        begin
          ActivateXML(fXML, True);
          if  FileExists(Value)  then
            fXML.LoadFromFile(Value)
          else
            fXML.FileName := Value;
          THackXMLDocument(fXML).SetModified(False)
        end { csDesigning not in ComponentState }
    end { not SameFileName(fXMLFileName, Value) }
end { SetXMLFileName };


function  TGSkParamSaverXML.ValueExists(const   sName:  string)
                                        : Boolean;
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.ValueExists("%s")',
             [sName]);
  {$ENDIF TraceParamSaver }
  inherited ValueExists(sName);
  Result := IndexOfNode(fiCurrNode, csValueNodeXML, sName) >= 0;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.ValueExists')
  {$ENDIF TraceParamSaver }
end { ValueExists };


procedure  TGSkParamSaverXML.WriteBinaryData(const   sName:  string;
                                             var     Buffer;
                                             const   nBufferSize:  Integer);
type
  TStr2 = string[2];

var
  sBuf:  AnsiString;
  xData:  array[0..$3fffffff] of Byte  absolute Buffer;
  nInd:  Integer;

  function  ByteToHex(const   nByte:  Byte)
                      : TStr2;

    function  NibbleToChar(const   nNibble:  Byte)
                           : AnsiChar;
    begin
      Assert(nNibble in [0..15]);
      case  nNibble  of
        0..9:
          Result := AnsiChar(nNibble + Ord('0'));
        10..15:
          Result := AnsiChar (nNibble - 10 + Ord('A'))
        else
          Result := #0
      end { case nNibble }
    end { NibbleToChar };

  begin  { ByteToHex }
    SetLength(Result, 2);
    Result[1] := NibbleToChar(nByte shr 4);
    Result[2] := NibbleToChar(nByte and $0F)
  end { ByteToHex };

begin  { WriteBinaryData }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.WriteBinaryData("%s", ..., %d)',
             [sName, nBufferSize]);
  {$ENDIF TraceParamSaver }
  inherited;
  sBuf := '';
  for  nInd := 0  to  Pred(nBufferSize)  do
    sBuf := sBuf + ByteToHex(xData[nInd]);
  InternalWrite(sName, string(sBuf), sdBinary, nBufferSize);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.WriteBinaryData')
  {$ENDIF TraceParamSaver }
end { WriteBinaryData };


procedure  TGSkParamSaverXML.WriteBool(const   sName:  string;
                                       const   bValue:  Boolean);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.WriteBool("%s", %s)',
             [sName, BoolToStr(bValue, True)]);
  {$ENDIF TraceParamSaver }
  inherited;
  InternalWrite(sName,
                BoolToStr(bValue){BoolToStrXML(bValue)},   // Registry compatibility
                sdInteger);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.WriteBool')
  {$ENDIF TraceParamSaver }
end { WriteBool };


procedure  TGSkParamSaverXML.WriteCurrency(const   sName:  string;
                                           const   nValue:  Currency);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.WriteCurrency("%s", %m)',
             [sName, nValue]);
  {$ENDIF TraceParamSaver }
  inherited;
  InternalWrite(sName, CurrToStr(nValue), sdBinary, SizeOf(Currency));
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.WriteCurrency')
  {$ENDIF TraceParamSaver }
end { WriteCurrency };


procedure  TGSkParamSaverXML.WriteDate(const   sName:  string;
                                       const   dValue:  TDateTime);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.WriteDate("%s", [%s]',
             [sName, TGSkLog.DateTimeToString(dValue)]);
  {$ENDIF TraceParamSaver }
  inherited;
  // InternalWrite(sName, DateToStrXML(dValue), sdBinary, SizeOf(TDateTime));
  WriteDateTime(sName, dValue);   // Registry compatibility
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.WriteDate')
  {$ENDIF TraceParamSaver }
end { WriteDate };


procedure  TGSkParamSaverXML.WriteDateTime(const   sName:  string;
                                           const   dValue:  TDateTime);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.WriteDateTime("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dValue)]);
  {$ENDIF TraceParamSaver }
  inherited;
  InternalWrite(sName, DateTimeToStrXML(dValue), sdBinary, SizeOf(TDateTime));
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.WriteDateTime')
  {$ENDIF TraceParamSaver }
end { WriteDateTime };


procedure  TGSkParamSaverXML.WriteFloat(const   sName:  string;
                                        const   nValue:  Double);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.WriteFloat("%s", %f)',
             [sName, nValue]);
  {$ENDIF TraceParamSaver }
  inherited;
  InternalWrite(sName, FloatToStrXML(nValue), sdBinary, SizeOf(Double));
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.WriteFloat')
  {$ENDIF TraceParamSaver }
end { WriteFloat };


procedure  TGSkParamSaverXML.WriteHex(const   sName:  string;
                                      const   nValue:  Integer);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.WriteHex("%s", $%x)',
             [sName, nValue]);
  {$ENDIF TraceParamSaver }
  inherited;
  InternalWrite(sName, Format('$%x', [nValue]), sdInteger);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.WriteHex')
  {$ENDIF TraceParamSaver }
end { WriteHex };


procedure  TGSkParamSaverXML.WriteInteger(const   sName:  string;
                                          const   nValue:  Integer);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.WriteInteger("%s", %d)',
             [sName, nValue]);
  {$ENDIF TraceParamSaver }
  inherited;
  InternalWrite(sName, IntToStr(nValue), sdInteger);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.WriteInteger')
  {$ENDIF TraceParamSaver }
end { WriteInteger };


procedure  TGSkParamSaverXML.WriteString(const   sName, sValue:  string);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.WriteString("%s", "%s")',
             [sName, sValue]);
  {$ENDIF TraceParamSaver }
  inherited;
  InternalWrite(sName, sValue, sdString, Length(sValue), True);
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.WriteString')
  {$ENDIF TraceParamSaver }
end { WriteString };


procedure  TGSkParamSaverXML.WriteTime(const   sName:  string;
                                       const   dValue:  TDateTime);
begin
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.WriteTime("%s", [%s])',
             [sName, TGSkLog.DateTimeToString(dValue)]);
  {$ENDIF TraceParamSaver }
  inherited;
  // InternalWrite(sName, TimeToStrXML(dValue), sdBinary, SizeOf(TDateTime));
  WriteDateTime(sName, dValue);   // Registry compatibility
  {$IFDEF TraceParamSaver }
    LogExit('TGSkParamSaverXML.WriteTime')
  {$ENDIF TraceParamSaver }
end { WriteTime };


function  TGSkParamSaverXML.GetDataSize(const   sName:  string)
                                        : Integer;
var
  nInd:  Integer;
  chAttr:  Char;

begin  { GetDataSize }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.GetDataSize("%s")', [sName]);
  {$ENDIF TraceParamSaver }
  nInd := IndexOfNode(fiCurrNode, csValueNodeXML, sName);
  if  nInd >= 0  then
    begin
      Result := GetAttribInt(fiCurrNode.ChildNodes[nInd], csSizeAttrXML);
      if  Result = 0  then
        begin
          chAttr := GetAttribChar(fiCurrNode.ChildNodes[nInd], csTypeAttrXML);
          if  chAttr = csValueDataType[sdInteger]  then
            Result := SizeOf(Integer)
          else if  chAttr = csValueDataType[sdString]  then
            Result := Length(InternalRead(sName, ''))
        end { Result = 0 }
    end { nInd >= 0 }
  else
    Result := -1;
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.GetDataSize');
  {$ENDIF TraceParamSaver }
end { GetDataSize };


function  TGSkParamSaverXML.GetDataType(const   sName:  string)
                                        : TGSkParamSaverDataType;
var
  nInd:  Integer;
  chBuf:  Char;

begin  { GetDataType }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.GetDataType("%s")', [sName]);
  {$ENDIF TraceParamSaver }
  nInd := IndexOfNode(fiCurrNode, csValueNodeXML, sName);
  if  nInd >= 0  then
    begin
      chBuf := GetAttribChar(fiCurrNode.ChildNodes[nInd], csTypeAttrXML);
      Assert(Low(TGSkParamSaverDataType) = sdUnknown);
      Result := High(csValueDataType);
      repeat
        if  csValueDataType[Result] = chBuf  then
          begin
            {$IFDEF TraceParamSaver }
              LogValue('Result', csParamSaverDataType[Result]);
              LogExit('TGSkParamSaverXML.GetDataType');
            {$ENDIF TraceParamSaver }
            Exit
          end { csValueDataType[Result] = chBuf };
        Result := Pred(Result)
      until  Result = sdUnknown
    end { nInd >= 0 }
  else
    Result := sdUnknown;
  {$IFDEF TraceParamSaver }
    LogValue('Result', csParamSaverDataType[Result]);
    LogExit('TGSkParamSaverXML.GetDataType')
  {$ENDIF TraceParamSaver }
end { GetDataType };


function  TGSkParamSaverXML.InternalRead(const   sName, sDefault:  string)
                                         : string;
var
  nInd:  Integer;
  iNode:  IXMLNode;

begin  { InternalRead }
  {$IFDEF TraceParamSaver }
    LogEnter('TGSkParamSaverXML.InternalRead("%s", "%s")',
             [sName, sDefault]);
  {$ENDIF TraceParamSaver }
  Result := sDefault;
  nInd := IndexOfNode(fiCurrNode, csValueNodeXML, sName);
  if  nInd >= 0  then
    begin
      iNode := fiCurrNode.ChildNodes[nInd].ChildNodes.First();
      if  (iNode.NodeType = ntText)
          and (iNode.NextSibling() = nil)  then
        Result := iNode.Text
      else
        repeat
          if  iNode.NodeType = ntCData  then
            begin
              Result := iNode.Text;
              Break
            end { ntCData };
          iNode := iNode.NextSibling()
        until  iNode = nil
    end { nInd >= 0 };
  {$IFDEF TraceParamSaver }
    LogResult(Result);
    LogExit('TGSkParamSaverXML.InternalRead')
  {$ENDIF TraceParamSaver }
end { InternalRead };


procedure  TGSkParamSaverXML.EndUpdate();
begin
  inherited;
  if  UpdateLevel = 0  then
    SaveToFile(True)
end { EndUpdate };

{$ENDREGION 'TGSkParamSaverXML'}


end.
