object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'frmMain'
  ClientHeight = 149
  ClientWidth = 319
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnSend: TButton
    Left = 48
    Top = 48
    Width = 75
    Height = 25
    Caption = '&Send'
    Default = True
    TabOrder = 0
    OnClick = btnSendClick
  end
  object btnClose: TButton
    Left = 176
    Top = 48
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Close'
    TabOrder = 1
    OnClick = btnCloseClick
  end
end
