@echo off
@SetLocal
@ REM set GREXT=jpg
@ REM set GREXT=gif
  SET GREXT=png
@ REM Eksperymentalnie sprawdzi�em, �e grafika w formcie PNG lub GIF
@ REM jest najmniejsza w tym przypadku.

@ echo.
@ echo Generowanie grafiki...
@ echo.
  SET DOT="c:\Program Files\Graphviz\bin\dot.exe"
  %DOT% -Grankdir=LR -T%GREXT% -oGVClasses.%GREXT% GVClasses.dot
  %DOT% -Grankdir=LR -T%GREXT% -oGVUses.%GREXT% GVUses.dot

@ echo.
@ echo Gotowe.
@ echo.

@EndLocal
