﻿unit  Test.GSkPasLib.PingHost;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


{$TYPEINFO ON}


uses
  JclSysInfo,
  TestFramework,
  GSkPasLib.PingHost, GSkPasLib.PingHostImpl;


type
  TPingHost_Test = class(TTestCase)
  strict private
    var
      fiPing:  IGSkPingInterface;
    procedure  CheckPingSuccess(const   iPing:  IGSkPingInterface;
                                const   sHost:  string);
    procedure  CheckPingFailure(const   iPing:  IGSkPingInterface;
                                const   sHost:  string);
  public
    procedure  SetUp();                                              override;
  published
    procedure  TestPingLocalhost();
    procedure  TestPingHostByName();
    procedure  TestPingGoogle();
    procedure  TestPingNonExistentHost();
    procedure  TestPingNonExistentWebsite();
  end { TPingHost_Test };


implementation


{$REGION 'TPingHost_Test'}

procedure  TPingHost_Test.CheckPingFailure(const   iPing:  IGSkPingInterface;
                                           const   sHost:  string);
var
  sAction:  string;

begin  { CheckPingFailure }
  sAction := 'ping ' + sHost;
  CheckFalse(iPing.PingHost(sHost), sAction);
  CheckNotEqualsString('', iPing.ErrorMessage, sAction + ' error message');
  CheckNotEquals(0, iPing.ErrorCode, sAction + ' error code')
end { CheckPingFailure };


procedure  TPingHost_Test.CheckPingSuccess(const   iPing:  IGSkPingInterface;
                                           const   sHost:  string);
var
  sAction:  string;

begin  { CheckPingSuccess }
  sAction := 'ping ' + sHost;
  CheckTrue(iPing.PingHost(sHost), sAction);
  CheckEqualsString('', iPing.ErrorMessage, sAction + ' error message');
  CheckEquals(0, iPing.ErrorCode, sAction + ' error code')
end { CheckPingSuccess };


procedure  TPingHost_Test.SetUp();
begin
  inherited;
  fiPing := TGSkPingHostImpl.Create()
end { SetUp };


procedure  TPingHost_Test.TestPingGoogle();
begin
  CheckPingSuccess(fiPing, 'google.com');
end { TestPingGoogle };


procedure  TPingHost_Test.TestPingHostByName();

var
  sHostName:  string;

begin  { TestPingHostByName }
  sHostName := GetLocalComputerName();
  CheckPingSuccess(fiPing, sHostName);
end { TestPingHostByName };


procedure  TPingHost_Test.TestPingLocalhost();
begin
  CheckPingSuccess(fiPing, '127.0.0.1');
  CheckPingSuccess(fiPing, 'localhost');
end { TestPingLocalhost };


procedure  TPingHost_Test.TestPingNonExistentHost();
begin
  CheckPingFailure(fiPing, 'LocalHostNotExist');
end { TestPingNonExistentHost };


procedure  TPingHost_Test.TestPingNonExistentWebsite();
begin
  CheckPingFailure(fiPing, 'WebSite.NotExist.com');
end { TestPingNonExistentWebsite };

{$ENDREGION 'TPingHost_Test'}


initialization
  RegisterTest(TPingHost_Test.Suite())


end.

