﻿unit  Test.GSkPasLib.CmdLineOptionParser;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


{$TYPEINFO ON}


uses
  SysUtils, DateUtils,
  TestFramework,
  GSkPasLib.CmdLineOptionParser;


type
  TCmdLineParser_Test = class(TTestCase)
  private
    procedure  SetUp(var     Parser:  TGSkOptionParser;
                     const   SetOpt:  TGSkSetOption;
                     const   Params:  TArray<string>;
                     const   sDefVal: string = '');                  reintroduce;
  public
//     procedure  SetUp();                                              override;
//     procedure  TearDown();                                           override;
  published
    procedure  TestSetOption();
  end { TCmdLineParser_Test };


implementation


{$REGION 'TCmdLineParser_Test'}

procedure  TCmdLineParser_Test.SetUp(var     Parser:  TGSkOptionParser;
                                     const   SetOpt:  TGSkSetOption;
                                     const   Params:  TArray<string>;
                                     const   sDefVal: string);
begin
  SetOpt.Value := sDefVal;
  if  Parser <> nil  then
    Parser.Free();
  Parser := TGSkOptionParser.CreateParams(Params);
  Parser.FreeOptionsOnDestroy := False;
  Parser.AddOption(SetOpt);
  Parser.ParseOptions();
  CheckEqualsString('', Parser.LeftList.Text, 'Unrecognised params')
end { SetUp };


procedure  TCmdLineParser_Test.TestSetOption();

var
  lParser:  TGSkOptionParser;
  lSetOpt:  TGSkSetOption;

begin  { TestSetOption }
  lSetOpt := TGSkSetOption.Create('S', 'send', 'Send something');
  lSetOpt.PossibleValues := 'Confirm,CanAbort';
  lParser := nil;
  {-}
  SetUp(lParser, lSetOpt, ['--send=Confirm+CanAbort']);
  Check(lSetOpt.HasValue('confirm'), '1. Confirm');
  Check(lSetOpt.HasValue('CanAbort'), '1. CanAbort');
  {-}
  SetUp(lParser, lSetOpt, ['--send=Confirm, +CanAbort']);
  Check(lSetOpt.HasValue('confirm'), '2. confirm');
  Check(lSetOpt.HasValue('CanAbort'), '2. CanAbort');
  {-}
  SetUp(lParser, lSetOpt, ['--send', '+Confirm+CanAbort']);
  Check(lSetOpt.HasValue('confirm'), '3. comfirm');
  Check(lSetOpt.HasValue('CanAbort'), '3. CanAbort');
  {-}
  SetUp(lParser, lSetOpt, ['--send', 'CanAbort']);
  CheckFalse(lSetOpt.HasValue('confirm'), '4. confirm');
  CheckTrue(lSetOpt.HasValue('canAbort'), '4. canAbort');
  {-}
  SetUp(lParser, lSetOpt, ['--send-Confirm+CanAbort'], 'Confirm');
  CheckFalse(lSetOpt.HasValue('Confirm'), '5. Confirm');
  CheckTrue(lSetOpt.HasValue('CanAbort'), '5. CanAbort');
  {-}
  SetUp(lParser, lSetOpt, ['--send-Confirm+CanAbort'], 'Confirm');
  CheckFalse(lSetOpt.HasValue('Confirm'), '6. Confirm');
  CheckTrue(lSetOpt.HasValue('CanAbort'), '6. CanAbort')
end { TestSetOption };

{$ENDREGION 'TCmdLineParser_Test'}


initialization
  RegisterTest(TCmdLineParser_Test.Suite())


end.
