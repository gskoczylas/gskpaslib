﻿{: @abstract(Moduł @name rejestruje monitorowanie zdarzenia w pliku LOG.)

   Moduł zawiera definicję obiektu @link(TGSkLogFile). Jest to
   @link(GSkPasLib.Log.TGSkCustomLogAdapter adapter logowania) czyli obiekt
   pomocniczy dla komponentu @link(GSkPasLib.Log.TGSkLog).

   Ten adapter rejestruje informacje w pliku LOG. Aby zapobiec zajmowaniu zbyt
   dużo miejsca na dysku przez plik LOG moduł ma wbudowane ograniczenia dotyczące
   jego wielkości. Po ich przekroczeniu dotychczasowy plik LOG jest przenoszony
   do pliku z rozszerzeniem @code(.old.log) i tworzony jest nowy plik @code(.log).

   @seeAlso(GSkPasLib.Log)  }
unit  GSkPasLib.LogFile;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{.$DEFINE TraceLogFile}

{$WARN SYMBOL_PLATFORM OFF}   // W1002 Symbol 'faSysFile' is specific to a platform


uses
  {$IF CompilerVersion > 22} // XE
    WinAPI.Windows,
    System.SysUtils, System.SyncObjs, System.Classes, System.Math,
    System.DateUtils, System.StrUtils,
    {$LEGACYIFEND ON}
  {$ELSE}
    Windows, SysUtils, SyncObjs, Classes, Math,
    DateUtils, StrUtils,
  {$IFEND}
  JclSysInfo,
  {$IFDEF GetText}
    JvGnugettext,
  {$ENDIF GetText}
  GSkPasLib.Log;


const
  {: @abstract Domyślny maksymalny rozmiar pliku LOG (w kilobajtach). }
  gcnStdFileSizeLimit = 1024;   // 1024 kB = 1 MB
  {$INCLUDE Author.inc }


type
  {: @abstract Jednoski miary definiujące wielkości pliku.

     @value fsKB Rozmiar pliku przedstawiony jest w kilobajtach (1024 bajtów)
     @value fsMB Rozmiar pliku przedstawiony jest w megabajtach (1024 kilobajtów)
     @value fsGB Rozmiar pliku przedstawiony jest w gigabajtach (1024 megabajtów) }
  TGSkFileSizeUnits = (fsKB, fsMB, fsGB);

  {: @abstract Kryterium tworzenia nowego pliku LOG.

     @value(nfSizeLimitOnly Tylko wielkość pliku.)
     @value(nfDay Codziennie nowy plik LOG. Do nazwy pliku dodawana jest bieżąca
                  data w formacie @code(YYYY-MM-DD).)
     @value(nfWeekDay Oddzielny plik LOG dla każdego dnia tygodnia. Do nazwy pliku
                      dodawana jest nazwa dnia tygodnia.)
     @value(nfMonthDay Oddzielny plik LOG dla każdego dnia miesiąca. Do nazwy pliku
                       dodawana jest dwycyfrowa liczba oznaczająca numer dnia
                       w miesiącu.)
     @value(nfYearDay Oddzielny plik LOG dla każdego dnia roku. Do nazwy pliku
                      dodawana jest trzycyfrowa liczba ozbnaczająca numer dnia
                      w roku.)
     @value(nfMonthWeek Oddzielny plik LOG dla każdego tygodnia w miesiącu. Do
                        nazwy pliku dodawany jest numer tygodnia w miesiącu.)
     @value(nfYearWeek Oddzielny plik dla każdego tygodnia w roku. Do nazwy pliku
                       dodawany jest dwucyfrowy numer tygodnia w roku.)
     @value(nfMonth Oddzielny plik dla każego miesiąca. Do nazwy pliku dodawana
                    jest nazwa miesiąca.)
     @value(nfYearMonth Oddzielny plik dla każdego miesiąca. Do nazwy pliku
                        dodawany jest numer miesiąca w roku.)
     @value(nfYear Oddzielny plik dla każdego roku. Do nazwy pliku dodawany jest
                   czterocyfrowy numer roku.)
     @value(nfMonthAndDay Oddzielny plik na każdy dzień roku. Do nazwy pliku
                          dodawany jest numer miesiąca i numer dnia w miesiącu
                          w formacie @code(MM-DD).)
     @value(nfMonthAndWeek Oddzielny plik na każdy tydzień roku. Do nazwy pliku
                           dodawany jest numer miesiąca i numer tygodnia w miesiącu
                           w formacie @code(MM-W).)
     @value(nfYearAndDay Oddzielny plik na każdy dzień w każdym roku. Do nazwy
                         pliku dodawany jest rok i numer dnia w roku w formacie
                         @code(YYYY-DDD).)
     @value(nfYearAndWeek Oddzielny plik na każdy tydzień w każdym roku. Do nazwy
                          pliku dodawany jest rok i numer tygodnia w roku w
                          formacie @code(YYYY-WW).)
     @value(nfYearAndMonth Oddzielny plik na każdy miesiąc w każdym roku. Do nazwy
                           pliku dodawany jest rok i numer miesiąca w formacie
                           @code(YYYY-MM).) }
  TGSkNewFileCond = (nfSizeLimitOnly,
                     nfDay, nfWeekDay, nfMonthDay, nfYearDay,
                     nfMonthWeek, nfYearWeek,
                     nfMonth, nfYearMonth,
                     nfYear,
                     nfMonthAndDay, nfMonthAndWeek,
                     nfYearAndDay, nfYearAndWeek, nfYearAndMonth);

  {: @abstract Flagi sterujące monitorowaniem informacji.

     @value(lfKeepFileOpen Plik LOG jest otwierany gdy po raz pierwszy trzeba do
                           niego zapisać informację. Po zapisaniu może być zaraz
                           zamykany lub program może trzymać ten plik cały czas
                           otwarty. Jeżeli plik jest cały czas otwarty to
                           rejestrowanie informacji odbywa się znacznie szybciej,
                           ale w przypadku awarii programu ostatnie informacje
                           mogą nie być zapisane do pliku.)
     @value(lfKeepOlderFiles Czy mają być tworzone pliki @code(.old.log).)
     @value(lfWriteMsgType Czy w pliku ma być rejestrowany
                           @link(GSkPasLib.Log.TGSkLogMessageType typ informacji).)
     @value(lfWritePrefix Czy w pliku mają być zapisywane
                          @link(GSkPasLib.Log.TGSkLog.Prefix prefiksy) informacji.) }
  TGSkLogFileFlag = (lfKeepFileOpen, lfKeepOlderFiles,
                     lfWriteMsgType, lfWritePrefix);

  {: @abstract Flagi sterujące monitorowaniem informacji. }
  TGSkLogFileFlags = set of TGSkLogFileFlag;

  {: @abstract Możlowe lokalizacje pliku LOG.

     @value(flAppData Plik LOG jest w folderze %AppData%, w folderze podrzędnym
                      zależnym od nazwy firmy i aplikacji -- nazywa się tak samo
                      jak aplikacja.)
     @value(flAppDir Plik LOG jest w folderze aplikacji --  nazywa się tak samo
                     jak aplikacja.)
     @value(flCustom Plik LOG jest we wskazanym pliku.)

     @seeAlso(TGSkLogFile.CreateLog)  }
  TGSkLogFileLocation = (flAppData, flAppDir, flCustom);


const
  {: @abstract Standardowo rejestrowany jest typ i prefiks informacji. }
  gcsetStdLogFileFlags  = [lfWriteMsgType, lfWritePrefix];
  {: @exclude}
  gcsStdLogFileExt     = '.log';
  {: @exclude}
  gcsLogFileExtFmt     = '.%s' + gcsStdLogFileExt;
  {: @exclude}
  gcsStdOldLogFileExt  = '.old' + gcsStdLogFileExt;
  {: @seeAlso(TGSkLogFileLocation) }
  gcStdLogFileLocation = flAppData;


type
  TGSkLogFile = class;

  {$IF not Declared(TDate)}
    TDate = TDateTime;
  {$IFEND}

  {: @abstract Rozdzaj operacji na pliku dziennika.

     Wartość typu @name jest jednym z parametrów @link(TLogFileEvent zdarzenia)
     generowanego przez obiekty typu @link(TGSkLogFile).

     @value(lfCreate Utworzono instancję klasy @link(TGSkLogFile).)
     @value(lfCreating Zostanie utworzony nowy plik dziennika.)
     @value(lfCreated Został utworzny nowy plik dziennika.)
     @value(lfOpening Plik dziennika zostanie otwarty.)
     @value(lfOpened Plik dziennika został owarty.)
     @value(lfClosing Plik dziennika zostanie zamknięty.)
     @value(lfClosed Plik dziennika został zamknięty.)
     @value(lfRenaming Nazwa pliku dziennika zostanie zmieniona.)
     @value(lfRenamed Nazwa pliku dziennika zostanie zmieniona.)
     @value(lfDeleting Plik dziennika zostanie usunięty z dysku.)
     @value(lfDeleted Plik dziennika został usunięty z dysku.)
     @value(lfDestroy Usunięto instancję klasy @link(TGSkLogFile).) }
  TLogFileOperation = (lfCreate,
                       lfCreating, lfCreated,
                       lfOpening, lfOpened,
                       lfClosing, lfClosed,
                       lfRenaming, lfRenamed,
                       lfDeleting, lfDeleted,
                       lfDestroy);

  {: @abstract Informuje o operacjach na pliku dziennika.

     @param(Sender Obiekt generujący zdarzenie.)
     @param(FileOperation Rodzaj działania.)
     @param(sFileName Nazwa pliku. W przypadku działań @code(lfCreate) lub
                      @code(lfDestroy) parametr jest pusty.)
     @param(sFileName2 Nazwa docelowego pliku. Jest niepusty tylko dla działań
                       @code(lfRenaming) i @code(lfRenamed).)

     @seeAlso(TGSkLogFile.OnFileOperation OnFileOperation) }
  TLogFileEvent = procedure(Sender:  TGSkLogFile;
                            const   FileOperation:  TLogFileOperation;
                            const   sFileName:  TFileName;
                            const   sFileName2:  TFileName)          of object;

  {: @abstract(@link(GSkPasLib.Log.TGSkCustomLogAdapter Adapter) zapisujący
               rejestrowane informacje w pliku LOG.) }
  TGSkLogFile = class(TGSkCustomLogAdapter)
  strict private
    var
      fFileSizeUnits:   TGSkFileSizeUnits;
      fnFileSizeLimit:  Int64;   // bytes
      fsetFlags:        TGSkLogFileFlags;
      fMessages:        TGSkLogMessages;
      fHeader:          TGSkLogMessages;
      fsBaseFileName:   TFileName;
      fsLogFileName:    TFileName;
      fhFile:           THandle;   // Cardinal?
      fSynchro:         TCriticalSection;
      fNewFileCond:     TGSkNewFileCond;
      fbForceNewFile:   Boolean;
      fbSilentMode:     Boolean;
      fLogFileLocation: TGSkLogFileLocation;
      ftmHistoryEdge:   TDate;
      ffnFileOperation: TLogFileEvent;
     {$IFDEF TraceLogFile}
      fLogMe:           TStringList;
     {$ENDIF TraceLogFile}
    function  LogFileIsOpen() : Boolean;                             inline;
    function  LogFileSize() : Int64;
    function  LogFileTime() : TDateTime;
    function  LogFileDir(const   Location:  TGSkLogFileLocation)
                         : string;
    function  GetFileSizeLimit() : Int64;
    function  GetFileName() : TFileName;                             inline;
    procedure  SetFileName(const   sValue:  TFileName);              inline;
    procedure  SetLogFileLocation(const   Value:  TGSkLogFileLocation);
    procedure  SetFileSizeLimit(const   nValue:  Int64);
    procedure  SetFlags(const   setValue:  TGSkLogFileFlags);
    procedure  SetNewFileCond(const   nfValue:  TGSkNewFileCond);
    procedure  SetHistoryEdge(const   tmValue:  TDate);
    procedure  LogFileClose();
    procedure  DeleteLogFile(const   sFileName:  TFileName);
    procedure  RaiseOSError();
   {$IFDEF TraceLogFile}
    procedure  LogFileNames(const   sAction:  string);
   {$ENDIF TraceLogFile}
    procedure  DoFileOperation(const   FileOperation:  TLogFileOperation;
                               const   sFileName:  TFileName = '';
                               const   sFileName2:  TFileName = '');
    procedure  RemoveOldLogFiles();

  strict protected
    {: @abstract Rozszerzenie nazwy pliku dziennika.

       Metoda @name wyznacza rozszerzenie nazwy dziennika. Wynik zwracany przez
       tę funkcję zależy od wartości właściwości @link(NewFileCond).  }
    function  LogFileExt(tmDate:  TDateTime = 0.0)
                         : string;


    {: @abstract Definiuje nazwę dziennika.

       Metoda @name definiuje nazwę dziennika. Jest to część implementacji
       właściwości @link(LogName).

       W adapterze @className wartość @link(LogName) oznacza nazwę pliku.  }
    procedure  SetLogName(const   sName:  string);                   override;

    {: @abstract Definiuje pełną nazwę dziennika.

       Metoda @name definiuje pełną nazwę dziennika. Jest to część implementacji
       właściwości @link(LogFullName).

       W adapterze @className wartość @link(LogFullName) oznacza pełną ścieżkę
       do pliku.  }
    procedure  SetLogFullName(const   sName:  string);               override;

    {: @abstract Aktualizuje pełną nazwę dziennika.

       W zależności od wartości @link(NewFileCond) do wskazanej nazwy pliku
       dziennika może zostać dodana itp. Metoda @name aktualizuje pełną nazwę
       pliku dziennika tak, aby uwzględniała odpowiedni dodatek do nazwy pliku.

       Wywołanie metody @name (w komponencie @className) pozwala tworzyć pliki
       dziennika o odpowiedniej nazwie, gdy aplikacja działa o północy. }
    procedure  ResetLogFullName();

    {: @abstract Zmienia nazwę wskazanego pliku.

       Jeżeli plik jest używany przez inną aplikację, procedura ponawia próby zmiany
       nazwy przez jedną sekundkę. Jeżeli plik nadal jest zajęty, to procedura
       wyświetal użytkownikowi pytanie, co robić w tej sytuacji (możliwe działania
       to @italic(Przerwać) (@code(Abort)), @italic(Czekać) (próbować przez kolejną
       sekundę) lub @italic(Ignorować) (jeżeli się da, to skasować stary plik).}
    procedure  RenameLogFile(const   sOldName, sNewName:  TFileName);

  public

    {: @abstract Standardowy konstruktor.

       @param(AOwner Właściciel obiektu.

                     Jeżeli właścicielem obiektu @name jest ten sam obiekt, który
                     jest właścicielem obiektu @link(TGSkLog), obiekt @name zostanie
                     automatycznie @link(TGSkLog.AddLogAdapter dodany do listy
                     adapterów) obiektu @link(TGSkLog).

                     Jeżeli właścicielem obiektu @name jest obiekt @link(TGSkLog),
                     obiekt @name zostanie automatycznie @link(TGSkLog.AddLogAdapter
                     dodany do listy adapterów) obiektu @link(TGSkLog). Ponadto
                     z chwilą zwolnienia obiektu @link(TGSkLog) automatycznie
                     zwolniony zostanie również obiekt @name. )

       @seeAlso(CreateLog)  }
    constructor  Create(AOwner:  TComponent);                        override;

    {: @abstract Construktor ze wskazaniem lokalizacji pliku LOG.

       @param(AOwner Właściciel obiektu.

                     Jeżeli właścicielem obiektu @name jest ten sam obiekt, który
                     jest właścicielem obiektu @link(TGSkLog), obiekt @name zostanie
                     automatycznie @link(TGSkLog.AddLogAdapter dodany do listy
                     adapterów) obiektu @link(TGSkLog).

                     Jeżeli właścicielem obiektu @name jest obiekt @link(TGSkLog),
                     obiekt @name zostanie automatycznie @link(TGSkLog.AddLogAdapter
                     dodany do listy adapterów) obiektu @link(TGSkLog). Ponadto
                     z chwilą zwolnienia obiektu @link(TGSkLog) automatycznie
                     zwolniony zostanie również obiekt @name. )

       @param(LogFileLocation Lokalizacja pliku LOG.

                              @link(Create Standardowo) plik LOG jest w folderze
                              @code(%AppData%\NazwaFirmy\NazwaAplikacji))

       @param(sLogFilePath Ścieżka do pliku LOG.

                           Ten parametr ma znaczenie tylko wtedy, gdy parametr
                           @code(LogFileLocation) ma wartość @code(flCustom).)

       @seeAlso(Create)  }
    constructor  CreateLog(const   AOwner:  TComponent;
                           const   LogFileLocation:  TGSkLogFileLocation;
                           const   sLogFilePath:  string = '';
                           const   fnOnLogFileOp:  TLogFileEvent = nil);

    {: @exclude }
    destructor  Destroy();                                           override;

    {: @exclude
       Nie ma czego dokumentować bo to tylko implementacja standardowej metody. }
    procedure  Assign(Source:  TPersistent);                         override;

    {* TGSkCustomLogAdapter *}

    {: @abstract Rozpoczyna buforowanie informacji.

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.BeginLog). }
    procedure  BeginLog();                                           override;

    {: @abstract Rejestruje informację.

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.LogMessage). }
    procedure  LogMessage(const   Msg:  TGSkLogMessage);             override;

    {: @abstract Kończy buforowanie informacji.

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.EndLog). }
    procedure  EndLog();                                             override;

    {: @abstract Czyści (usuwa) zarejestrowane infromacje.

       Metoda @name implementuje w tym adapterze ogólną metodę
       @link(GSkPasLib.Log.TGSkLog.Clear).

       Wywołanie tej metody powoduje usunięcie z dysku dotychczasowego pliku LOG.
       Przy pierwszym rejestrowaniu kolejnych informacji utworzony zostanie nowy
       wplik LOG. }
    procedure  Clear();                                              override;

  published
    {: @abstract Bazowa nazwa pliku LOG.

       Właściwość @name jest aliasem właściwości @link(LogFullName).

       W niektórych @link(TGSkNewFileCond okolicznościach) do tej nazwy
       dołączane są dodatkowe znaki. }
    property  FileName:  TFileName
              read  GetFileName
              write SetFileName;

    {: @abstract(Jednostki miary, w jakich przedstawiany jest maksymalny rozmiar
                 pliku.)
       Ta włąściwość wskazuje jednostki miary, w jakich przedstawiany jest
       maksymalny rozmiar pliku LOG.

       @seealso FileSizeLimit }
    property  FileSizeUnits:  TGSkFileSizeUnits
              read  fFileSizeUnits
              write fFileSizeUnits
              default  fsKB;

    {: @abstract Limit wielkości pliku LOG.

       Jeżeli właściwość @name ma wartość @code(0) (zero), to rozmiar pliku LOG
       nie jest sprawdzany (pliki LOG mogą mieć dowolną wielkość).

       Sposób interpretacji tej wielkości zależy również od wartości właściwości
       @link(FileSizeUnits). }
    property  FileSizeLimit:  Int64
              read  GetFileSizeLimit
              write SetFileSizeLimit
              default  gcnStdFileSizeLimit;

    {: @abstract Warunki tworzenia nowego pliku LOG. }
    property  NewFileCond:  TGSkNewFileCond
              read  fNewFileCond
              write SetNewFileCond
              default  nfSizeLimitOnly;

    {: @abstract Flagi definiujące sposób pracy z plikiem LOG. }
    property  Flags:  TGSkLogFileFlags
              read  fsetFlags
              write SetFlags
              default  gcsetStdLogFileFlags;

    {: @abstract Lokalizacja pliku LOG. }
    property  LogFileLocation:  TGSkLogFileLocation
              read  fLogFileLocation
              write SetLogFileLocation
              default  gcStdLogFileLocation;

    {: @abstract Automatycznie usuwa pliki LOG starsze niż wskazana data.

       Jeżeli właściwość @link(NewFileCond) ma wartość inną niż @code(nfSizeLimitOnly)
       oraz właściwość @name jest niepusta, to z dysku zostaną automatycznie
       usunięte pliki LOG starsze niż wskazana data. }
    property  HistoryEdge:  TDate
              read  ftmHistoryEdge
              write SetHistoryEdge;

    {: @abstract Czy wyświetlać komunikaty.

       Jeżeli właściwość @name jest ustawiona (ma wartość @true), to nie jest
       wyświetlany żaden komunikat użytkownikowi.

       Standardowo komunikaty mogą być wyświetlane w takich przypadkach, jak
       brak dostępu do pliku, brak folderu dla pliku itp. }
    property  SilentMode:  Boolean
              read  fbSilentMode
              write fbSilentMode;

    {: @abstract Informuje o operacjach na pliku dziennika.

       Zdarzenie @name jest generowane przed i po takich działaniach jak utworzenie
       pliku dziennika, otwarcie pliku dziennika czy zamknięcie pliku dziennika. }
    property  OnFileOperation:  TLogFileEvent
              read  ffnFileOperation
              write ffnFileOperation
              default  nil;
  end { TGSkLogFile };


function  MakeLogToFile(const   LogFileLocation:      TGSkLogFileLocation = flAppData;
                        const   LogFileSizeLimitKiB:  Int64 = gcnStdFileSizeLimit;
                        const   LogFileCycle:         TGSkNewFileCond = nfSizeLimitOnly)
                        : TGSkLog;                                   overload;

function  MakeLogToFile(const   LogFilePath:          TFileName;
                        const   LogFileSizeLimitKiB:  Int64 = gcnStdFileSizeLimit;
                        const   LogFileCycle:         TGSkNewFileCond = nfSizeLimitOnly)
                        : TGSkLog;                                   overload;


implementation


uses
  {$IFDEF TraceLogFile}
    GSkPasLib.Classes,
  {$ENDIF TraceLogFile}
  GSkPasLib.PasLib, GSkPasLib.FileUtils, GSkPasLib.SystemUtils, GSkPasLib.Dialogs,
  GSkPasLib.TimeUtils;


const
  gcn1kB = 1024;
  gcn1MB = gcn1kB * 1024;
  gcn1GB = gcn1MB * 1024;
  gcsNewLine = sLineBreak;   // + sLineBreak +#10;

  gcnSharingViolationWait = 1000;   // [ms]
  gcnSharingVilationSleep = 25;     // [ms]


const
  {$IFNDEF GetText}
    gcsDeleteFileError = 'Podczas usuwania pliku „%s” wystąpił błąd %u:'+ sLineBreak +'%s';
    gcsRenameFileError = 'Podczas zmiany nazwy pliku „%s”'+ sLineBreak +'na „%s”'+ sLineBreak +'wystąpił błąd %u:'+ sLineBreak +'%s';
    gcsCreateFileError = 'Podczas tworzenia pliku „%s” wystąpisł błąd %u:'+ sLineBreak +'%s';
    gcsOpenFileError   = 'Podczas otwierania pliku „%s” wystąpił błąd %u:'+ sLineBreak +'%s';
    gcsLockFileError   = 'Podczas próby zablokowania pliku „%s” wystąpił błąd %u:'+ sLineBreak +'%s';
    gcsLogFileHeader   = 'Historia zdarzeń w %s';
  {$ELSE}
    gcsDeleteFileError = 'Deleting the file ''%0:s'' raised error %1:u:'+ sLineBreak +'%2:s';
    gcsRenameFileError = 'Renaming the file ''%0:s'''+ sLineBreak +'to ''%1:s'''+ sLineBreak +'raised error %2:u:'+ sLineBreak +'%3:s';
    gcsCreateFileError = 'Creating the file ''%0:s'' raised error %1:u:'+ sLineBreak +'%2:s';
    gcsOpenFileError   = 'Opening the file ''%0:s'' raised error %1:u:'+ sLineBreak +'%2:s';
    gcsLockFileError   = 'Locking the file ''%0:s'' raised error %1:u:'+ sLineBreak +'%2:s';
    gcsLogFileHeader   = 'Events of %s';
  {$ENDIF GetText}


type
  TGSkLogHack = class(TGSkLog)
  public
    property  StartUp;
  end { TGSkLogHack };


{$REGION 'Global subroutines'}

function  MakeLogToFile(const   LogFileLocation:      TGSkLogFileLocation;
                        const   LogFileSizeLimitKiB:  Int64;
                        const   LogFileCycle:         TGSkNewFileCond)
                        : TGSkLog;
var
  lLogFile:  TGSkLogFile;

begin  { MakeLogToFile }
  Result := TGSkLog.Create(nil);
  lLogFile := TGSkLogFile.Create(Result);
  {-}
  lLogFile.LogFileLocation := LogFileLocation;
  lLogFile.FileSizeUnits := fsKB;
  lLogFile.FileSizeLimit := LogFileSizeLimitKiB;
  lLogFile.NewFileCond := LogFileCycle;
  Result.AddLogAdapter(lLogFile)
end { MakeLogToFile };


function  MakeLogToFile(const   LogFilePath:          TFileName;
                        const   LogFileSizeLimitKiB:  Int64;
                        const   LogFileCycle:         TGSkNewFileCond)
                        : TGSkLog;
var
  lLogFile:  TGSkLogFile;

begin  { MakeLogToFile }
  Result := TGSkLog.Create(nil);
  lLogFile := TGSkLogFile.Create(Result);
  {-}
  lLogFile.LogFileLocation := flAppDir;
  lLogFile.FileName := LogFilePath;
  lLogFile.FileSizeUnits := fsKB;
  lLogFile.FileSizeLimit := LogFileSizeLimitKiB;
  lLogFile.NewFileCond := LogFileCycle;
  Result.AddLogAdapter(lLogFile)
end { MakeLogToFile };

{$ENDREGION 'Global subroutines'}


{$REGION 'TGSkLogFile'}

procedure  TGSkLogFile.Assign(Source:  TPersistent);
begin
  fSynchro.Enter();
  try
    inherited;
    if  Source is TGSkLogFile  then
      begin
        if  LogFileIsOpen()  then
          LogFileClose();
        with  Source as TGSkLogFile  do
          begin
            Self.FileSizeUnits := FileSizeUnits;
            Self.FileSizeLimit := FileSizeLimit;
            Self.Flags := Flags;
            Self.LogFileLocation := LogFileLocation;
            Self.NewFileCond := NewFileCond;
            Self.LogFullName := LogFullName;
            Self.HistoryEdge := HistoryEdge;
            Self.SilentMode := SilentMode
          end { with Source }
      end { Source is TGSkLogFile }
  finally
    fSynchro.Leave()
  end { try-finally }
end { Assign };


procedure  TGSkLogFile.BeginLog();
begin
  fSynchro.Enter();
  try
    // inherited; -- abstract
    fMessages.Clear()
  finally
    fSynchro.Leave()
  end { try-finally }
end { BeginLog };


procedure  TGSkLogFile.Clear();
begin
  fSynchro.Enter();
  try
    // inherited; -- abstract
    if  LogFileIsOpen()  then
      LogFileClose();
    fbForceNewFile := True
  finally
    fSynchro.Leave()
  end { try-finally }
end { Clear };


constructor  TGSkLogFile.Create(AOwner:  TComponent);
begin
  inherited;
  {$IFDEF TraceLogFile}
    fLogMe := TStringList.Create();
  {$ENDIF TraceLogFile}
  fhFile := INVALID_HANDLE_VALUE;
  FileSizeUnits := fsKB;
  FileSizeLimit := gcnStdFileSizeLimit;
  fMessages := TGSkLogMessages.Create();
  fHeader := TGSkLogMessages.Create();
  fsetFlags := gcsetStdLogFileFlags;
  fSynchro := TCriticalSection.Create();
  fLogFileLocation := flAppData;
  FileName := ''
end { Create };


constructor  TGSkLogFile.CreateLog(const   AOwner:  TComponent;
                                   const   LogFileLocation:  TGSkLogFileLocation;
                                   const   sLogFilePath:  string;
                                   const   fnOnLogFileOp:  TLogFileEvent);
begin
  Create(AOwner);
  fLogFileLocation := LogFileLocation;
  case  LogFileLocation  of
    flAppData,
    flAppDir:
      FileName := '';
    flCustom:
      FileName := sLogFilePath
  end { case LogFileLocation };
  ffnFileOperation := fnOnLogFileOp;
  DoFileOperation(lfCreate)
end { CreateLog };


procedure  TGSkLogFile.DeleteLogFile(const   sFileName:  TFileName);

const
  cnMaxSilentWait = 7000;    // [ms]

var
  nErrCode:  LongWord;
  nStart:    LongWord;

begin  { DeleteLogFile }
  DoFileOperation(lfDeleting, sFileName);
  nStart := GetTickCount();
  while  not DeleteFile(sFileName)  do
    begin
      nErrCode := GetLastError();
      if  not(nErrCode in [ERROR_FILE_NOT_FOUND, ERROR_PATH_NOT_FOUND])  then
        if  SilentMode  then
          if  TicksSince(nStart) < cnMaxSilentWait  then
            begin
              Sleep(50);
              Continue
            end { TicksSince(...) < time-out }
          else
            Exit
        else
          case  MsgBox(dgettext('GSkPasLib', gcsDeleteFileError),
                       [sFileName, nErrCode, SysErrorMessage(nErrCode)],
                       MB_ICONERROR or MB_ABORTRETRYIGNORE or MB_DEFBUTTON2)  of
            IDABORT:
              Abort();
            IDRETRY:
              Continue;
            IDIGNORE:
              Break
          end { case MsgDlg; while not DeleteFile() }
    end { while };
  DoFileOperation(lfDeleted, sFileName)
end { DeleteLogFile };


destructor  TGSkLogFile.Destroy();
begin
  DoFileOperation(lfDestroy);
  if  fMessages.HasMassages()  then
    EndLog();
  fMessages.Free();
  fHeader.Free();
  if  LogFileIsOpen()  then
    LogFileClose();
  fSynchro.Free();
  inherited;
  {$IFDEF TraceLogFile}
    fLogMe.Free()
  {$ENDIF TraceLogFile}
end { Destroy };


procedure  TGSkLogFile.DoFileOperation(const   FileOperation:  TLogFileOperation;
                                       const   sFileName:  TFileName;
                                       const   sFileName2:  TFileName);
begin
  if  Assigned(ffnFileOperation)  then
    ffnFileOperation(Self, FileOperation, sFileName, sFileName2)
end { DoFileOperation };


procedure  TGSkLogFile.EndLog();

var
  sDir, sFile:  string;
  lFile:  TSearchRec;
  bNewFile:  Boolean;

  procedure  PrintMessages(const   Messages:  TGSkLogMessages;
                           const   bCanCloseLogFile:  Boolean;
                           const   bSkipHeader:  Boolean);           forward;

  procedure  WriteBuf(const   pBuffer:  PByte;
                      const   nBufLen:  Cardinal);
  var
    nStart:  Cardinal;
    nError:  Cardinal;

  begin  { WriteBuf }
    nStart := GetTickCount();
    while  FileSeek(fhFile, 0, FILE_END) = Integer(INVALID_FILE_SIZE)  do
      begin
        nError := GetLastError();
        if  (nError = ERROR_LOCK_VIOLATION)
            and (GetTickCount() <= nStart + gcnSharingViolationWait)  then
          begin
            Sleep(gcnSharingVilationSleep);
            Continue
          end { if };
        GSkPasLib.SystemUtils.RaiseOSError(nError)
      end { while };
    {-}
    nStart := GetTickCount();
    while  FileWrite(fhFile, pBuffer^, nBufLen) < 0  do
      begin
        nError := GetLastError();
        if  (nError = ERROR_LOCK_VIOLATION)
            and (GetTickCount() - nStart <= gcnSharingViolationWait)  then
          begin
            Sleep(gcnSharingVilationSleep);
            Continue
          end { if };
        GSkPasLib.SystemUtils.RaiseOSError(nError)
      end { while }
  end { WriteBuf };

  procedure  WriteBOM();

  var
    lBOM:  {$IFDEF UNICODE}TBytes{$ELSE}packed array[0..2] of  Byte{$ENDIF};

  begin  { WriteBOM }
    {$IFDEF UNICODE}
      lBOM := TEncoding.UTF8.GetPreamble();
    {$ELSE}
      lBOM[0] := $EF;
      lBOM[1] := $BB;
      lBOM[2] := $BF;
    {$ENDIF -UNICODE}
    WriteBuf(Addr(lBOM[0]), Length(lBOM))
  end { WriteBOM };

  procedure  WriteLine(const   sText:  string);

  var
    sBuf:  {$IFDEF UNICODE}UTF8String{$ELSE}string{$ENDIF};

  begin  { WriteLine }
    sBuf := UTF8Encode(sText + gcsNewLine);
    WriteBuf(PByte(PAnsiChar(sBuf)), Length(sBuf))
  end { WriteLine };

  // Returns True if creates new file (header has been printed)
  function  LogFileOpen() : Boolean;

  label
    Retry;

  const
    cnWaitingTimeout = 10000;   // [ms]

  var
    nErrCode:  DWORD;
    nStart:    DWORD;

  begin  { LogFileOpen }
    Result := False;
    RemoveOldLogFiles();
    {-}
    Assert(fhFile = INVALID_HANDLE_VALUE);
    nStart := GetTickCount();
   Retry:
    if  not FileExists(fsLogFileName)  then
      begin
        DoFileOperation(lfCreating, fsLogFileName);
        ForceDirectories(ExtractFileDir(fsLogFileName));
        fhFile := FileCreate(fsLogFileName);
        if  fhFile = INVALID_HANDLE_VALUE  then
          begin
            nErrCode := GetLastError();
            if  SilentMode  then
              if  TicksSince(nStart) <= cnWaitingTimeout  then
                begin
                  Sleep(50);
                  goto  Retry
                end { TicksSince(...) <= time-out }
              else
                Abort()
            else
              case  MsgBox(dgettext('GSkPasLib', gcsCreateFileError),
                           [fsLogFileName,
                            nErrCode, SysErrorMessage(nErrCode)],
                           MB_ICONERROR or MB_RETRYCANCEL)  of
                IDRETRY:
                  goto  Retry;
                IDCANCEL:
                  Abort();
                else
                  Abort()
              end { MsgDlg() }
          end { fhFile <> INVALID_HANDLE_VALUE };
        Assert(fhFile <> INVALID_HANDLE_VALUE);
        WriteBOM();
        WriteLine(Format(dgettext('GSkPasLib', gcsLogFileHeader),
                         [ModuleName]));
        WriteLine('');
        if  fHeader.HasMassages()  then
          begin
            PrintMessages(fHeader, False, False);
            WriteLine('');
            Result := True
          end { fHeader.HasMassages() };
        if  lfKeepFileOpen in fsetFlags  then
          begin
            FileClose(fhFile);
            fhFile := INVALID_HANDLE_VALUE
          end { lfKeepFileOpen in FFlags };
        DoFileOperation(lfCreated, fsLogFileName)
      end { not FileExists() };
    if  fhFile = INVALID_HANDLE_VALUE  then
      begin
        DoFileOperation(lfOpening, fsLogFileName);
        nErrCode := NO_ERROR;
        nStart := GetTickCount();
        fhFile := FileOpen(fsLogFileName,
                           fmOpenWrite or fmShareDenyNone);
        repeat
          if  fhFile <> INVALID_HANDLE_VALUE  then
            Break;
          nErrCode := GetLastError();
          if  nErrCode <> ERROR_SHARING_VIOLATION  then
            Break;
          Sleep(gcnSharingVilationSleep);
          fhFile := FileOpen(fsLogFileName,
                             fmOpenWrite or fmShareDenyNone);
        until  (fhFile <> INVALID_HANDLE_VALUE)
               or (GetTickCount() > nStart + gcnSharingViolationWait);
        if  fhFile = INVALID_HANDLE_VALUE  then
          begin
            Assert(nErrCode <> NO_ERROR);   // nErrCode := GetLastError();
            if  fbSilentMode  then
              if  TicksSince(nStart) <= cnWaitingTimeout  then
                begin
                  Sleep(50);
                  goto  Retry
                end { TicksSince(...) <= time-out }
              else
                Abort()
            else
              case  MsgBox(dgettext('GSkPasLib', gcsOpenFileError),
                           [fsLogFileName,
                            nErrCode, SysErrorMessage(nErrCode)],
                           MB_ICONERROR or MB_RETRYCANCEL)  of
                IDRETRY:
                  goto  Retry;
                IDCANCEL:
                  Abort()
              end { case MsgDlg() }
          end { fhFile = INVALID_HANDLE_VALUE };
        DoFileOperation(lfOpened, fsLogFileName)
      end { fhFile = INVALID_HANDLE_VALUE };
    Assert(fhFile <> INVALID_HANDLE_VALUE)
  end { LogFileOpen };

  function  LogFileLock(const   nLockSize:  Int64Rec)
                        : Int64Rec;
  const
    cnTimeout = 3000;          // [ms]
    cnSilentTimeout = 10000;   // [ms]

  var
    nErrCode:  LongWord;
    nStart:    DWORD;
    nTimeout:  LongWord;

  begin  { LogFileLock }
    Int64(Result) := LogFileSize();
    nStart := GetTickCount();
    if  SilentMode  then
      nTimeout := cnSilentTimeout
    else
      nTimeout := cnTimeout;
    while  not LockFile(fhFile,
                        Result.Lo, Result.Hi,
                        nLockSize.Lo, nLockSize.Hi)  do
      begin
        nErrCode := GetLastError();
        if  nErrCode = ERROR_LOCK_VIOLATION  then
          if  TicksSince(nStart) <= nTimeout  then
            begin
              Sleep(10);
              Continue
            end { Lock violation & not timeout };
        if  SilentMode  then
          Abort();
        case  MsgBox(dgettext('GSkPasLib', gcsLockFileError),
                     [fsLogFileName,
                      nErrCode, SysErrorMessage(nErrCode)],
                     MB_ICONERROR or MB_RETRYCANCEL)  of
          IDRETRY:
            Continue;
          IDCANCEL:
            Abort()
        end { case }
      end { while }
  end { LogFileLock };

  function  StartNewLogFile() : Boolean;
  begin
    if  FileExists(fsLogFileName)  then
      begin
        if  NewFileCond <> nfSizeLimitOnly  then
          Result := DateOf(LogFileTime()) <> Today()
        else
          Result := False;
        if  not Result  then
          Result := (fnFileSizeLimit > 0)
                    and (LogFileSize() >= {$IF CompilerVersion > 22}System.{$IFEND}Math.IfThen(fbForceNewFile, 1, fnFileSizeLimit))
      end { FileExists(...) }
    else
      Result := True
  end { StartNewLogFile };

  procedure  LogFileUnlock(const   LockPos, LockSize:  Int64Rec);
  begin
    CheckOSError(UnlockFile(fhFile,
                            LockPos.Lo, LockPos.Hi,
                            LockSize.Lo, LockSize.Hi))
  end { LogFileUnlock };

  procedure  PrintMessages(const   Messages:  TGSkLogMessages;
                           const   bCanCloseLogFile:  Boolean;
                           const   bSkipHeader:  Boolean);

  var
    lLines:  TStringList;
    lLockPos, lLockSize:  Int64Rec;
    lMsg:  TGSkLogMessage;
    sBuf:  string;
    lBuf:  TStringList;
    nInd, nIndL:  Integer;

  begin  { PrintMessages }
    lLines := TStringList.Create();
    try
      Int64(lLockSize) := 0;
      {$IFDEF TraceLogFile}
        Inc(Int64(lLockSize), Length(UTF8Encode(fLogMe.Text)) + Length(sLineBreak) + fLogMe.Count * 2);
      {$ENDIF TraceLogFile}
      lMsg := Messages.FirstMessage();
      while  lMsg <> nil  do
        begin
          if  not(bSkipHeader
                  and (mfInit in lMsg.Flags))  then
            begin
              if  mfMemo in lMsg.Flags  then
                begin
                  lBuf := TStringList.Create();
                  try
                    with  lBuf  do
                      begin
                        Text := lMsg.MessageText;
                        { - }
                        if  lfWriteMsgType in fsetFlags  then
                          sBuf := lMsg.MessageTypeToChar()
                                   + ' '
                        else
                          sBuf := '';
                        if  lMsg.MessageTime <> 0  then
                          sBuf := sBuf + lMsg.FormatLogTime() + ' ';
                        if  lMsg.ThreadID <> 0  then
                          sBuf := sBuf + lMsg.FormatThreadID() + ' ';
                        if  (lfWritePrefix in fsetFlags)
                            and (lMsg.Prefix <> '')  then
                          sBuf := sBuf + lMsg.Prefix + ' ';
                        Insert(0, sBuf + StringOfChar('-', 25));
                        Append(sBuf + StringOfChar('=', 25));
                        { - }
                        for  nIndL := 0  to  Count - 1  do
                          lLines.Add(Strings[nIndL]);
                        sBuf := Text
                      end { with lBuf }
                  finally
                    lBuf.Free()
                  end { try-finally }
                end { mfMemo in lMsg.Flags }
              else
                begin
                  sBuf := lMsg.MessageToStr(lfWriteMsgType in fsetFlags,
                                            True, True,
                                            lfWritePrefix in fsetFlags);
                  lLines.Add(sBuf)
                end { not(mfMemo in lMsg.Flags) };
              Inc(Int64(lLockSize),
                  (Length(Utf8Encode(sBuf)) + Length(gcsNewLine)))
            end { skip header };
          lMsg := Messages.NextMessage()
        end { while lMsg <> nil };
      lLockPos := LogFileLock(lLockSize);
      try
        {$IFDEF TraceLogFile}
          for  nInd := 0  to  fLogMe.Count - 1  do
            WriteLine(TGSkLogMessage.MessageTypeToChar(TGSkLogMessageType(fLogMe.ObjAsInteger[nInd]))
                        + ' --> LOGFILE --> ' + fLogMe.Strings[nInd]);
          fLogMe.Clear();
        {$ENDIF TraceLogFile}
        for  nInd := 0  to  lLines.Count - 1  do
          WriteLine(lLines[nInd])
      finally
        LogFileUnlock(lLockPos, lLockSize);
        if  bCanCloseLogFile  then
          if  not(lfKeepFileOpen in fsetFlags)  then
            LogFileClose()
      end { try-finally };
      Messages.Clear()
    finally
      lLines.Free()
    end { try-finally }
  end { PrintMessages };

begin  { EndLog }
  fSynchro.Enter();
  try
    if  fMessages.IsEmpty()  then
      Exit;
    bNewFile := False;
    if  not LogFileIsOpen()  then
      begin
        {$IFDEF TraceLogFile}
          fLogMe.Add('Log file is NOT open');
        {$ENDIF TraceLogFile}
        if  StartNewLogFile()  then
          begin
            {$IFDEF TraceLogFile}
              fLogMe.Add('Start new log file');
            {$ENDIF TraceLogFile}
            ResetLogFullName();
            {$IFDEF TraceLogFile}
              fLogMe.Add('LogFullName = "' + LogFullName + '"');
            {$ENDIF TraceLogFile}
            if  not(lfKeepOlderFiles in fsetFlags)  then
              begin
                sDir := ExtractFilePath(fsLogFileName);
                if  FindFirst(sDir + fsBaseFileName + '*' + gcsStdLogFileExt,
                              faAnyFile, lFile) = 0  then
                  try
                    repeat
                      sFile := sDir + lFile.Name;
                      if  not SameFileName(sFile, fsLogFileName)  then
                        begin
                          {$IFDEF TraceLogFile}
                            fLogMe.Add('DELETE ' + sFile);
                          {$ENDIF TraceLogFile}
                          DeleteLogFile(sFile)
                        end { if };
                    until  FindNext(lFile) <> 0
                  finally
                    FindClose(lFile)
                  end { try-finally }
              end { if lfKeepOlderFiles not in FFlags };
            if  FileExists(fsLogFileName)  then
              begin
  //               Assert(AnsiStartsText(fsBaseFileName, fsLogFileName),
  //                      'BaseFileName=' + fsBaseFileName + ', FileName=' + fsLogFileName);
                {$IFDEF TraceLogFile}
                  fLogMe.Add('File ' + fsLogFileName + ' exists');
                  LogFileNames('EndLog');
                {$ENDIF TraceLogFile}
                sFile := ChangeFileExt(fsLogFileName, gcsStdOldLogFileExt);
                {$IFDEF TraceLogFile}
                  fLogMe.Add('RENAME ' + fsLogFileName + ' TO ' + sFile);
                {$ENDIF TraceLogFile}
                RenameLogFile(fsLogFileName, sFile)
              end { FileExists(FLogFileName) }
          end { new file };
        bNewFile := LogFileOpen();
        {$IFDEF TraceLogFile}
          fLogMe.Add('Log file opened: ' + LogFullName);
        {$ENDIF TraceLogFile}
        fbForceNewFile := False
      end { not LogFileIsOpen() }    {$IFDEF TraceLogFile}
    else
      fLogMe.Add('Log file is open') {$ENDIF TraceLogFile};
    Assert(LogFileIsOpen());
    PrintMessages(fMessages, True, bNewFile)
  finally
    fSynchro.Leave()
  end { try-finally }
end { EndLog };


function  TGSkLogFile.GetFileName() : TFileName;
begin
  Result := LogFullName
end { GetFileName };


function  TGSkLogFile.GetFileSizeLimit() : Int64;
begin
  Result := 0;
  case  fFileSizeUnits  of
    fsKB:
      Result := fnFileSizeLimit div gcn1kB;
    fsMB:
      Result := fnFileSizeLimit div gcn1MB;
    fsGB:
      Result := fnFileSizeLimit div gcn1GB
  end { case fFileSizeUnits }
end { GetFileSizeLimit };


procedure  TGSkLogFile.LogFileClose();
begin
  Assert(fhFile <> INVALID_HANDLE_VALUE);
  DoFileOperation(lfClosing, fsLogFileName);
  FileClose(fhFile);
  fhFile := INVALID_HANDLE_VALUE;
  DoFileOperation(lfClosed, fsLogFileName);
  {$IFDEF TraceLogFile}
    fLogMe.Add('Log file closed')
  {$ENDIF TraceLogFile}
end { LogFileClose };


function  TGSkLogFile.LogFileDir(const   Location:  TGSkLogFileLocation)
                                 : string;
begin
  case  Location  of
    flAppData:
      Result := GetAppdataFolder()
                + PathDelim + gcsCompanyName
                + PathDelim + ChangeFileExt(ExtractFileName(GetModuleFileName()), '');
    flAppDir:
      Result := ExtractFileDir(GetModuleFileName(){ParamStr(0)});
    flCustom:
      Result := ExtractFileDir(LogFullName)
  end { case Location }
end { LogFileDir };


function  TGSkLogFile.LogFileExt(tmDate:  TDateTime)
                                 : string;
begin
  if  tmDate = 0  then
    tmDate := Date();
  case  fNewFileCond  of
    nfSizeLimitOnly:
      Result := '';
    nfDay:
      Result := FormatDateTime('yyyy-mm-dd', tmDate);
    nfWeekDay:
      Result := IntToStr(DayOfTheWeek(tmDate));   // FormatDateTime('dddd', tmToday);
    nfMonthDay:
      Result := FormatDateTime('dd', tmDate);
    nfYearDay:
      Result := Format('%3.3d', [DayOfTheYear(tmDate)]);
    nfMonthWeek:
      Result := Format('%d', [WeekOfTheMonth(tmDate)]);
    nfYearWeek:
      Result := Format('%2.2d', [WeekOfTheYear(tmDate)]);
    nfMonth:
      Result := FormatDateTime('mmmmm', tmDate);
    nfYearMonth:
      Result := FormatDateTime('mm', tmDate);
    nfYear:
      Result := FormatDateTime('yyyy', tmDate);
    nfMonthAndDay:
      Result := FormatDateTime('mm-dd', tmDate);
    nfMonthAndWeek:
      Result := Format('%2.2d-%d', [MonthOf(tmDate), WeekOfTheMonth(tmDate)]);
    nfYearAndDay:
      Result := Format('%4.4d-%3.3d', [YearOf(tmDate), DayOfTheYear(tmDate)]);
    nfYearAndWeek:
      Result := Format('%4.4d-%2.2d', [YearOf(tmDate), WeekOfTheYear(tmDate)]);
    nfYearAndMonth:
      Result := FormatDateTime('yyyy-mm', tmDate)
  end { case fNewFileCond };
  if  Result = ''  then
    Result := gcsStdLogFileExt
  else
    Result := Format(gcsLogFileExtFmt, [Result])
end { LogFileExt };


function  TGSkLogFile.LogFileIsOpen() : Boolean;
begin
  Result := fhFile <> INVALID_HANDLE_VALUE
end { LogFileIsOpen };


{$IFDEF TraceLogFile}

procedure  TGSkLogFile.LogFileNames(const   sAction:  string);

var
  lLev:  TGSkLogMessageType;

begin  { LogFileNames }
  if  (Parent <> nil)
      and (Parent.AdapterCount <> 0)  then
    begin
      if  SameFileName(fsBaseFileName, ChangeFileExt(fsLogFileName, ''))  then
        lLev := lmAppLevel3
      else
        lLev := lmError;
      if  TGSkLogHack(Parent).StartUp
          or (lLev = lmError)  then
        begin
          fLogMe.AddInteger('BaseFileName = "' + fsBaseFileName + '"', Ord(lLev));
          fLogMe.AddInteger('LogFileName = "' + fsLogFileName + '"', Ord(lLev));
          if  lLev = lmError  then
            fLogMe.AddInteger('Expected = "' + ChangeFileExt(fsLogFileName, '') + '"', Ord(lLev))
        end { if }
    end { Parent <> nil and AdapterCount <> 0 }
end { LogFileNames };

{$ENDIF TraceLogFile}


function  TGSkLogFile.LogFileSize() : Int64;

var
  lTmp:  Int64Rec;
  lFile:  TSearchRec;

begin  { LogFileSize }
  if  LogFileIsOpen()  then
    begin
      lTmp.Lo := {$IF CompilerVersion > 22}WinAPI.{$IFEND}Windows.GetFileSize(fhFile, Addr(lTmp.Hi));
      if  lTmp.Lo = $FFFFFFFF  then
        RaiseOSError()
    end { LogFileIsOpen() }
  else
    if  FindFirst(fsLogFileName, MaxInt{faAnyFile}, lFile) = 0  then
      begin
        {$IF SizeOf(lFile.Size) = SizeOf(Int64)}
          Int64(lTmp) := lFile.Size;
        {$ELSE}
          lTmp.Lo := lFile.FindData.nFileSizeLow;
          lTmp.Hi := lFile.FindData.nFileSizeHigh;
        {$IFEND}
        FindClose(lFile)
      end
    else
      begin
        Result := -1;
        Exit
      end { not FindFirst(...) };
  Result := Int64(lTmp)
end { LogFileSize };


function  TGSkLogFile.LogFileTime() : TDateTime;

var
  lFile:  TSearchRec;


begin  { LogFileTime }
  Result := 0;
  if  LogFileIsOpen()  then
    Result := Now()
  else
    if  FindFirst(fsLogFileName, MaxInt{faAnyFile}, lFile) = 0  then
      begin
        with  lFile  do
          {$IFDEF UNICODE}
            Result := lFile.TimeStamp;
          {$else}
            Result := FileDateToDateTime(Time);
          {$ENDIF -UNICODE}
        FindClose(lFile)
      end { FindFirst(...) = 0 }
end { LogFileTime };


procedure  TGSkLogFile.LogMessage(const   Msg:  TGSkLogMessage);
begin
  fSynchro.Enter();
  try
    inherited;
    fMessages.AddMessage(Msg.Clone());
    if  mfInit in Msg.Flags  then
      fHeader.AddMessage(Msg.Clone())
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogMessage };


procedure  TGSkLogFile.RaiseOSError();
begin
  if  csDesigning in ComponentState  then
    ErrBox(SysErrorMessage(GetLastError()), True)
  else
    RaiseLastOSError()
end { RaiseOSError };


procedure  TGSkLogFile.RemoveOldLogFiles();

var
  lFiles:  TStringList;
  lSrch:   TSearchRec;
  sPath:   string;
  sBordr:  string;
  sExt:    string;

begin  { RemoveOldLogFiles }
  if  (NewFileCond <> nfSizeLimitOnly)
      and (HistoryEdge <> 0.0)  then
    begin
      fSynchro.Enter();
      try
        lFiles := TStringList.Create();
        try
          sPath := ExtractFilePath(fsBaseFileName);
          if  FindFirst(fsBaseFileName + '*.log', MaxInt, lSrch) = 0  then
            try
              repeat
                if  lSrch.Attr and (faDirectory or faSysFile or faReadOnly) <> 0  then
                  Continue;
                if  EndsText(gcsStdOldLogFileExt, lSrch.Name)  then
                  sExt := gcsStdOldLogFileExt
                else
                  sExt := gcsStdLogFileExt;
                {$IFDEF UNICODE}
                  lFiles.Values[FormatDateTime('yyyy-mm-dd', lSrch.TimeStamp) + sExt] := sPath + lSrch.Name
                {$ELSE}
                  lFiles.Values[FormatDateTime('yyyy-mm-dd', FileDateToDateTime(lSrch.Time)) + sExt] := sPath + lSrch.Name
                {$ENDIF -UNICODE}
              until  FindNext(lSrch) <> 0
            finally
              FindClose(lSrch)
            end { try-finally };
          lFiles.Sort();
          sBordr := FormatDateTime('yyyy-mm-dd', HistoryEdge);
          while  (lFiles.Count <> 0)
                 and (lFiles.Names[0] < sBordr)  do
            begin
              DeleteLogFile(lFiles.ValueFromIndex[0]);
              lFiles.Delete(0)
            end { while }
        finally
          lFiles.Free()
        end { try-finally }
      finally
        fSynchro.Leave()
      end { try-finally }
    end { if <RemoveOldLogFiles> }
end { RemoveOldLogFiles };


procedure  TGSkLogFile.RenameLogFile(const   sOldName, sNewName:  TFileName);

const
  cnSilentRetry = 5000;   // [ms]

var
  nErrCode:  DWORD;
  nStart:  DWORD;

begin  { RenameLogFile }
  DoFileOperation(lfRenaming, sOldName, sNewName);
  nStart := GetTickCount();
  while  not MoveFileEx(PChar(sOldName),
                        PChar(sNewName),
                        MOVEFILE_COPY_ALLOWED
                          or MOVEFILE_REPLACE_EXISTING)  do
    begin
      nErrCode := GetLastError();
      if  nErrCode = ERROR_SHARING_VIOLATION  then
        if  GetTickCount() <= nStart + gcnSharingViolationWait  then
          begin
            Sleep(gcnSharingVilationSleep);
            Continue
          end { if };
      if  nErrCode = ERROR_SHARING_VIOLATION  then
        Break
      else
        if  SilentMode  then
          if  TicksSince(nStart) <= cnSilentRetry  then
            begin
              Sleep(10);
              Continue
            end { TicksSince(...) <= time-out }
          else
            Abort()
        else
          case  MsgBox(dgettext('GSkPasLib', gcsRenameFileError),
                       [sOldName, sNewName,
                        nErrCode, SysErrorMessage(nErrCode)],
                       MB_ICONERROR or MB_ABORTRETRYIGNORE)  of
            IDABORT:
              Abort();
            IDRETRY:
              Continue;
            IDIGNORE:
              begin
                DeleteLogFile(sOldName);
                Break
              end { mrIgnore }
          end { case MsgDlg() }
    end { while };
  DoFileOperation(lfRenamed, sOldName, sNewName)
end { RenameLogFile };


procedure  TGSkLogFile.ResetLogFullName();
begin
  fsLogFileName := fsBaseFileName + LogFileExt();
  {$IFDEF TraceLogFile}
    LogFileNames('ResetLogFullName')
  {$ENDIF TraceLogFile}
end { ResetLogFullName };


procedure  TGSkLogFile.SetFileName(const   sValue:  TFileName);
begin
  LogFullName := sValue
end { SetFileName };


procedure  TGSkLogFile.SetFileSizeLimit(const   nValue:  Int64);
begin
  case  fFileSizeUnits  of
    fsKB:
      fnFileSizeLimit := nValue * gcn1kB;
    fsMB:
      fnFileSizeLimit := nValue * gcn1MB;
    fsGB:
      fnFileSizeLimit := nValue * gcn1GB
  end { case fFileSizeUnits };
  if  LogFileIsOpen()
      and (LogFileSize() > fnFileSizeLimit)  then
    LogFileClose()
end { SetFileSizeLimit };


procedure  TGSkLogFile.SetFlags(const   setValue:  TGSkLogFileFlags);
begin
  fsetFlags := setValue;
  if  not(lfKeepFileOpen in fsetFlags)
      and LogFileIsOpen()  then
    LogFileClose()
end { SetFlags };


procedure  TGSkLogFile.SetHistoryEdge(const   tmValue:  TDate);
begin
  if  tmValue <> ftmHistoryEdge  then
    begin
      ftmHistoryEdge := tmValue;
      RemoveOldLogFiles()
    end { if tmValue <> ftmHistoryEdge }
end { SetHistoryEdge };


procedure  TGSkLogFile.SetLogFileLocation(const   Value:  TGSkLogFileLocation);
begin
  fLogFileLocation := Value;
  LogFullName := LogFileDir(Value) + PathDelim + Self.LogName
end { SetLogFileLocation };


procedure  TGSkLogFile.SetLogFullName(const   sName:  string);

var
  sLog:  string;

begin  { SetLogFullName }
  if  ExtractFilePath(sName) = ''  then
    begin
      SetLogName(sName);
      Exit
    end { ExtractFilePath(sName) = '' };
  if  SameText(ExtractFileExt(sName), gcsStdLogFileExt)  then
    sLog := ChangeFileExt(sName, '')
  else
    sLog := sName;
  inherited  SetLogFullName(sLog);
  if  sLog = ''  then
    sLog := inherited LogFullName;
  fsBaseFileName := sLog;
  fsLogFileName := fsBaseFileName + LogFileExt();
  {$IFDEF TraceLogFile}
    LogFileNames('SetLogFullName')
  {$ENDIF TraceLogFile}
end { SetLogFullName };


procedure  TGSkLogFile.SetLogName(const   sName:  string);

var
  sLog:  string;

begin  { SetLogName }
  if  ExtractFilePath(sName) <> ''  then
    begin
      SetLogFullName(sName);
      Exit
    end { ExtractFilePath(sName) <> '' };
  sLog := ChangeFileExt(sName, '');
  inherited SetLogName(sLog);
  if  sLog = ''  then
    sLog := inherited LogName;
  fsBaseFileName := LogFileDir(fLogFileLocation) + PathDelim + sLog;
  fsLogFileName := fsBaseFileName + LogFileExt();
  {$IFDEF TraceLogFile}
    LogFileNames('SetLogName')
  {$ENDIF TraceLogFile}
end { SetLogName };


procedure  TGSkLogFile.SetNewFileCond(const   nfValue:  TGSkNewFileCond);
begin
  if  fNewFileCond <> nfValue  then
    begin
      if  LogFileIsOpen()  then
        LogFileClose();
      fNewFileCond := nfValue;
      SetLogFullName(LogFullName)   // spowoduje wygenerowanie nowego rozszerzenia pliku
    end { FNewFileCond <> Value }
end { SetNewFileCond };

{$ENDREGION 'TGSkLogFile'}


end.
