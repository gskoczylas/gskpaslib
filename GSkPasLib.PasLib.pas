﻿{: @abstract Globalne definicje biblioteki @bold(@italic(GSkPasLib))

   Przed rozpoczęciem korzystania z biblioteki @bold(@italic(GSkPasLib)) należy
   przejrzeć zdefiniowane w tym module stałe i w razie potrzeby zmodyfikować
   je zgodnie z własnymi potrzebami.  }
unit  GSkPasLib.PasLib;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    Winapi.Messages;
  {$ELSE}
    Messages;
  {$IFEND}


const
  {$INCLUDE Author.inc}

  {$if  not Declared(gcsStdConfigName) }
    {$message  error 'W pliku „Author.inc” umieść definicję stałej „gcsStdConfigName” definiującej standardową nazwę pliku konfiguracyjnego.' }
  {$ifend}
  {$if  not Declared(gcsCompanyName) }
    {$message  error 'W pliku „Author.inc” umieść definicję stałej „gcsCompanyName” definiującej krótką nazwę firmy będącej autorem programu.' }
  {$ifend}
  {$if  not Declared(gcsCompanyCard) }
    {$message  error 'W pliku „Author.inc” umieść definicję stałej „gcsCompanyCard” definiującej Twój adres' }
  {$ifend}

  {: @abstract Standardowy typ pliku konfiguracyjnego

     Standardową nazwę pliku konfiguracyjnego definiuje stała
     @link(gcsStdConfigName).  }
  gcsStdConfigExt  = '.ini';   // start with '.'

  {: @abstract Standardowy plik konfiguracyjny }
  gcsStdConfigFileName = gcsStdConfigName + gcsStdConfigExt;

  { Wewnętrzne komunikaty pakietu }
  {: @exclude } gcnMsgSetProp = WM_APP + 0;

  {: @abstract Numer ostatniego komunikatu (SendMessage, PostMessage)

     Biblioteka @bold(@italic(GSkPasLib)) do niektórych działań wykorzystuje
     własne komunikaty systemu Windows, wysyłane przez systemowe funkcje
     @code(SendMessage), @code(PostMessage) lub przez standardową metodę
     @code(Perform).

     Jeżeli w aplikacji będzie potrzeba przesyłania własnych komunikatów, to aby
     uniknąć ewentualnych konfliktów z biblioteką, aplikacja powinna wykorzystywać
     komunikaty o numerach wyższych niż stała @name.

     Przykład: @longCode(#

       const
         gcnSpecjalnaAkcja = gcnMsgLast + 1;

       type
         TOknoPodstawowe = class
         private
           procedure  SpecjalnaAkcja(var Msg: TMessage); message gcnSpecjalnaAkcja;
           // inne definicje
         end ;

         // gdzieś w kodzie aplikacji
         for  nInd := Screen.FormCount - 1  downto  0  do
           PostMessage(Screen.Forms[nInd].Handle, gcnSpecjalnaAkcja, 0, 0);
     #) }
  gcnMsgLast = gcnMsgSetProp;

  { Parametry wewnętrznych komunikatów }
  {: @exclude } gcnMsgSetPropModalResult = 1;
  {: @exclude } gcnMsgSetPropDataModule  = 2;
  {: @exclude } gcnMsgSetPropAction      = 3;
  {: @exclude } gcnMsgSetPropFocus       = 4;

  { Parametry zapamiętywane w Registry lub XML }
  {: @exclude } gcsParFormLeft   = 'FormLeft';
  {: @exclude } gcsParFormTop    = 'FormTop';
  {: @exclude } gcsParFormWidth  = 'FormWidth';
  {: @exclude } gcsParFormHeight = 'FormHeight';
  {: @exclude } gcsParFormState  = 'FormState';
  {: @exclude } gcsParAppVersion = 'AppVersion';

  {: @exclude } gcsDataModuleSuffix     = '_Dane';

  {: @abstract Pierwszy wolny numer komunikatu.

     Jednym z filarów systemu @italic(Windows) są komunikaty przesyłane między
     różnymi komponentami, bibliotekami i aplikacjami. Część komunikatów jest
     zarezerwowana dla Windows. Pozostałe są dostępne dla twórców komponentów,
     bibliotek i aplikacji.

     Kłopot w tym, że nie ma żadnego standardu według którego te „pozostałe”
     komponenty i biblioteki mogą wykorzystywać komunikaty do własnych celów.

     Część numerów komunikatów wykorzystuje standardowa biblioteka VCL. Również
     pakiet @bold(@italic(GSkPasLib)) wykorzystuje komunikaty na wewnętrzne
     potrzeby.

     Stała @name zawiera numer pierwszego wolnego numeru komunikatu, który w
     razie potrzeby może być użyty w aplikacji.

     Przykład wykorzystania stałej @name:
     @longCode(#
       uses ..., GSkPasLib.PasLib, GSkPasLib.CustomForm, ...

       const
         gcnAkcja     = gcnMsgApplication;
         gcnInnaAkcja = gcnMsgApplication + 1;

       TOkno = class(TzzGSkCustomForm)
       private
         procedure  Akcja(var   Msg: TMessage);       message gcnAkcja;
       protected
         procedure  InnaAkcja(var   Msg:  TMessage);  message gcnInnaAkcja;
         ...
         procedure  Wykonaj();
       end ;

       procedure  TOkno.Wykonaj();
       begin
         ...
         PostMessage(Handle, gcnAkcja, 1, 2);
         ...
         PostMessage(Handle, gcnInnaAkcja, Integer(Self), 0);
         ...
       end ;
     #)  }
  gcnMsgApplication = gcnMsgLast + 1;

  // Do porównywania ze standardową stałą „CompilerVersion”
  gcnCompilerVersionDelphi6    = 14.0;
  gcnCompilerVersionDelphi7    = 15.0;
  gcnCompilerVersionDelphi8    = 16.0;
  gcnCompilerVersionDelphi2005 = 17.0;
  gcnCompilerVersionDelphi2006 = 18.0;
  gcnCompilerVersionDelphi2007 = 18.5;
  gcnCompilerVersionDelphi2009 = 20.0;
  gcnCompilerVersionDelphi2010 = 21.0;
  gcnCompilerVersionDelphiXE   = 22.0;
  gcnCompilerVersionDelphiXE2  = 23.0;
  gcnCompilerVersionDelphiXE3  = 24.0;
  gcnCompilerVersionDelphiXE4  = 25.0;
  gcnCompilerVersionDelphiXE5  = 26.0;
  gcnCompilerVersionDelphiXE6  = 27.0;
  gcnCompilerVersionDelphiXE7  = 28.0;
  gcnCompilerVersionDelphiXE8  = 29.0;
//gcnCompilerVersionDelphiXE9  =
  gcnCompilerVersionDelphiSeattle    = 30.0;
  gcnCompilerVersionDelphiAlexandria = 35.0;

  gcnCompilerVersionDelphiXE10 = gcnCompilerVersionDelphiSeattle
                                    deprecated 'Use gcnCompilerVersionDelphiSeattle instead';



function  dgettext(const   sDomain, sText:  string)
                   : string;                                         inline;

{$IFDEF GetText}

function  dngettext(const   sDomain, sSingular, sPlural:  string;
                    const   nCount:  Integer)
                    : string;                                        inline;

{$ENDIF GetText}


implementation


{$IFDEF GetText}

uses
  JvGnugettext;

{$ENDIF GetText}


function  dgettext(const   sDomain, sText:  string)
                   : string;
begin
  {$IFDEF GetText}
    Result := JvGnugettext.dgettext(sDomain, sText)
  {$ELSE}
    Result := sText
  {$ENDIF -GetText}
end { dgettext };


{$IFDEF GetText}

function  dngettext(const   sDomain, sSingular, sPlural:  string;
                    const   nCount:  Integer)
                    : string;
begin
  Result := JvGnugettext.dngettext(sDomain, sSingular, sPlural, nCount)
end { dngettext };

{$ENDIF GetText}


end.

