﻿{: @abstract Praca z dokumentami w formacie CSV. }
unit  GSkPasLib.CSV;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  SysUtils, Contnrs, Classes, DB, Math{$IFDEF GetText}, JvGnugettext{$ENDIF};


const
  TAB = #9;


type
  {$IF declared(TBufferedFileStream)}
    TFileStreamType = TBufferedFileStream;
  {$ELSE}
    TFileStreamType = TFileStream;
  {$IFEND}

  {: @abstract Czy pola CSV umieszczać w cudzysłowach.

     @value(tsAsNeeded Wartość będzie zapisana w cudzysłowach jeżeli zawiera znaki
                       sterujące lub seperator pól.)
     @value(tsNever Wartość nigdy nie będzie zapisana w cudzysłowach. Aplikacja
                    już się tym zajęła dodając pola do rekordów.)
     @value(tsAlways Wszystkie wartości zawsze będą ujęte w cudzysłowy.)

     @seeAlso(TGSkCSV.UseTextSep)  }
  TCSVTextSep = (tsAsNeeded, tsNever, tsAlways);

  TCSVImportProgressEvent = procedure(Sender:  TObject;
                                      const  nPosition, nMax:  Integer;
                                      var   bCanceled:  Boolean)     of object;

  {: @abstract Klasa do pracy z plikami w formacie CSV.

     Klasa @name jest napisana przy założeniu, że wszystkie dane z pliku CSV
     mieszczą się w pamięci RAM. }
  TGSkCSV = class
  strict private
    const
      fchStdFldSep = ',';
    var
      fchFldSep:    Char;
      fUseTextSep:  TCSVTextSep;
      fRecords:     TObjectList;   // list of TStringList
      fOnProgress:  TCSVImportProgressEvent;
    function  GetRecordCount() : Integer;                            inline;
    function  GetFieldCount(const   nRecordNo:  Integer)
                            : Integer;
    function  GetField(const   nRecordNo, nFieldNo:  Integer)
                       : string;
    procedure  SetField(const   nRecordNo, nFieldNo:  Integer;
                        const   sValue:  string);
    procedure  SetFieldSeparator(const   chValue:  Char);
  strict private
    {: @abstract Definiuje wartości pól we wskazanym wierszu. }
    procedure  SetFields(const   nRecordNo:  Integer;
                         const   Fields:  array of string);
  strict protected
    procedure  DoOnProgress(const   nPosition, nMax:  Integer;
                            var   bCanceled:  Boolean);
  public
    {: @exclude - standardowy konstruktor }
    constructor  Create();
    {: @exclude - standardowy destruktor }
    destructor  Destroy();                                           override;
    {-}
    {: @abstract Dodaje nowy rekord z polami o wskazanej treści.

       Metoda @name najpierw dodaje do dokumentu nowy rekord, a następnie wypełnia
       kolejne pola wskazanymi wartościami.

       @param(Fields Lista wartości. Dodany rekord ma tyle pól, ile jest elementów
                     tej tablicy.)

       @returns(Wynikiem jest numer rekordu. Pierwszy rekord ma numer @code(zero)).

       @seeAlso(InsertRecord)
       @seeAlso(DeleteRecord)  }
    function  AddRecord(const   Fields:  array of string)
                        : Integer;                                   overload;
    {: @abstract Dodaje nowy rekord do dokumentu.

       Dodany rekord ma zero pól.

       @returns(Wynikiem jest numer rekordu. Pierwszy rekord ma numer @code(zero)).

       @seeAlso(InsertRecord)
       @seeAlso(DeleteRecord)  }
    function  AddRecord() : Integer;                                 overload;
    {: @abstract Wstawia nowy rekord przed wskazanym rekordem dokumentu.

       Metoda @name wstawia nowy rekord przed wskazanym rekordem dokumentu,
       a następnie wypełnia pola rekordu wskazanymi wartościami.

       @param(nBeforeRecNo Numer rekordu, przed którym należy wstawić nowy rekord.)
       @param(Fields Wartości pól wstawianego rekordu. Rekord będzie mieć tyle pól,
                     ile jest elementów tej tablicy.)

       @seeAlso(AddRecord)
       @seeAlso(DeleteRecord)  }
    procedure  InsertRecord(const   nBeforeRecNo:  Integer;
                            const   Fields:  array of string);       overload;
    {: @abstract Wstawia nowy rekord przed wskazanym rekordem dokumentu.

       Metoda @name wstawia nowy rekord przed wskazanym rekordem dokumentu.
       Wstawiony rekord ma zero pól.

       @param(nBeforeRecNo Numer rekordu, przed którym należy wstawić dodatkowy
                           rekord.)

       @seeAlso(AddRecord)
       @seeAlso(DeleteRecord)  }
    procedure  InsertRecord(const   nBeforeRecNo:  Integer);         overload;
    {: @abstract Usuwa rekord o wskazanym numerze.

       @seeAlso(AddRecord)
       @seeAlso(InsertRecord)
       @seeAlso(Clear Usuwa wszystkie rekordy z dokumentu.)  }
    procedure  DeleteRecord(const   nRecordNo:  Integer);
    {: @abstract Dodaje pole do wskazanego rekordu.

       Metoda @name dodaje nowe pole do wskazanego rekordu.

       @param(nRecordNo Numer rekordu, do którego będzie dodane nowe pole.)
       @param(sValue Wartość nowego pola.)

       @returns(Wynikiem funkcji jest numer dodanego pola.)

       @seeAlso(FieldCount)  }
    function  AddField(const   nRecordNo:  Integer;
                       const   sValue:  string)
                       : Integer;
    {: @abstract Wstawia pole do wskazanego rekordu.

       Metoda @name wstawia pole do wskazanego rekordu.

       @param(nRecordNo Numer rekordu, do którego będzie dodane nowe pole.)
       @param(nBeforeFieldNo Numer pola, przed którym będzie dodane nowe pole.)
       @param(sValue Wartość nowego pola.)

       @seeAlso(FieldCount)  }
    procedure  InsertField(const   nRecordNo:  Integer;
                           const   nBeforeFieldNo:  Integer;
                           const   sValue:  string);
    {: @abstract Usuwa wszystkie rekordy z dokumentu.

       Po wykonaniu metody @name dokument nie zawiera żadnych rekordów.

       @seeAlso(DeleteRecord Usuwa pojedynczy rekord.)  }
    procedure  Clear();                                              inline;
    {: @abstract Odczytuje dokument CSV ze wskazanego pliku.

       Metoda @name odczytuje dokument w formacie CSV ze wskazanego pliku. Jeżeli
       nie wskazano jawnie rozszerzenia pliku, to użyte zostanie rozszerzenie
       „csv”.

       Podczas wczytywania danych uwzględniana jest wartość właściwości
       @link(FieldSeparator).

       @seeAlso(Import)
       @seeAlso(Save)  }
    procedure  Load({const} sInputFile:  string{$IFDEF UNICODE};
                    const   Encoding:  TEncoding = nil{$ENDIF});     overload;
    {: @abstract Odczytuje dokument CSV ze wskazanego strumienia.

       Metoda @name odczytuje dokument w formacie CSV ze wskazanego strumienia.

       @seeAlso(Import)
       @seeAlso(Save)  }
    procedure  Load(const   InputStream:  TFileStreamType{$IFDEF UNICODE};
                   {const}  Encoding:  TEncoding = nil{$ENDIF});     overload;

    {: @abstract Pobiera wartości pół ze wskazanego źródła danych.

       Metoda @name pobiera dane ze wskazanego źródła danych. Pobierane są
       wszystkie pola wszystkich rekordów.

       @param(dsDataSet Źródło danych.)
       @param(Fields Lista nazw pół, które mają być zaimportowane. Jeżeli ta
                     tablica jest pusta, to importowane są wszystkie pola.)
       @param(bHeader Pierwszy rekord CSV to nagłówki pól.)
       @param(bDisplayLabel Czy w nagłówku używać nazwy pól (@false), czy
                            opisy pól (@true).)

       @seeAlso(Load)
       @seeAlso(Save)  }
    procedure  Import(const   dsDataSet:  TDataSet;
                      const   Fields:  array of string;
                      const   bHeader:  Boolean = True;
                      const   bDisplayLabel:  Boolean = False);      overload;

    {: @abstract Pobiera wartości pół ze wskazanego źródła danych.

       Metoda @name pobiera dane ze wskazanego źródła danych. Pobierane są
       wszystkie pola wszystkich rekordów.

       @param(dsDataSet Źródło danych.)
       @param(bHeader Pierwszy rekord CSV to nagłówki pól.)
       @param(bDisplayLabel Czy w nagłówku używać nazwy pól (@false), czy
                            opisy pól (@true).)

       @seeAlso(Load)
       @seeAlso(Save)  }
    procedure  Import(const   dsDataSet:  TDataSet;
                      const   bHeader:  Boolean = True;
                      const   bDisplayLabel:  Boolean = False);      overload;

    {: @abstract Zapisuje dokument CSV do wskazanego pliku.

       Metoda @name zapisuje dokument w formacie CSV do wskazanego pliku. Jeżeli
       nie wskazano jawnie rozszerzenia pliku, to użyte zostanie rozszerzenie
       „csv”. Zapisywane pola rekordów rozdzielane są znakiem wskazywanym przez
       właściwość @link(FieldSeparator).

       Jeżeli wartość jakiegoś pola rekordu zawiera znaki ze zbioru
       @code([#0..' ', #127, #160, #255]) (czyli znaki sterujące lub odstęp, lub
       twardy odstęp) to pole ujmowane jest w cudzysłowy. Jeżeli w treści pola
       występuje cudzysłów, to jest on zapisywany jako podwójny cudzysłów.

       Parametr @code(Encoding) pozwala wskazać kodowanie pliku wynikowego.
       Jeżeli parametr ma wartość @nil, to zastosowane zostanie kodowanie
       domyślne (w polskiej wersji Windows będzie to @code(Windows-1250)).

       @seeAlso(Load)
       @seeAlso(Import)  }
    procedure  Save({const} sOutputFile:  string{$IFDEF UNICODE};
                    const   Encoding:  TEncoding = nil{$ENDIF});     overload;
    {: @abstract Zapisuje dokument CSV do wskazanego strumienia.

       Metoda @name zapisuje dokument w formacie CSV do wskazanego strumienia.
       Zapisywane pola rekordów rozdzielane są znakiem wskazywanym przez
       właściwość @link(FieldSeparator).

       Jeżeli wartość jakiegoś pola rekordu zawiera znaki ze zbioru
       @code([#0..' ', #127, #160, #255]) (czyli znaki sterujące lub odstęp, lub
       twardy odstęp) to pole ujmowane jest w cudzysłowy. Jeżeli w treści pola
       występuje cudzysłów, to jest on zapisywany jako podwójny cudzysłów.

       Parametr @code(Encoding) pozwala wskazać kodowanie pliku wynikowego.
       Jeżeli parametr ma wartość @nil, to zastosowane zostanie kodowanie
       domyślne (w polskiej wersji Windows będzie to @code(Windows-1250)).

       @seeAlso(Load)
       @seeAlso(Import)  }
    procedure  Save(const   OutputStream:  TStream{$IFDEF UNICODE};
                    const   Encoding:  TEncoding = nil{$ENDIF});     overload;

    {: @abstract Znak rozdzielający pola rekordu.

       Właściwość @name definiuje znak używany do rozdzielania pól rekordów.
       Standardowo znakiem tym jest przecinek. Niekiedy zamiast przecinka używany
       jest średnik lub znak @link(TAB tabulacji).

       Znak zdefiniowany przez właściwość @name używany jest w metodach
       @link(Load) oraz @link(Save).  }
    property  FieldSeparator:  Char
              read  fchFldSep
              write SetFieldSeparator
              default fchStdFldSep;
    {: @abstract Liczba rekordów w dokumencie.

       @seeAlso(FieldCount)  }
    property  RecordCount:  Integer
              read  GetRecordCount;
    {: @abstract Liczba pól w rekordzie.

       W dokumentach CSV w każdym rekordzie liczba pól może być inna.

       @seeAlso(RecordCount)
       @seeAlso(Field)  }
    property  FieldCount[const   nRecordNo:  Integer] :  Integer
              read  GetFieldCount;
    {: @abstract Pola dokumentu.

       @seeAlso(RecordCount)
       @seeAlso(FieldCount)  }
    property  Field[const   nRecordNo, nFieldNo:  Integer] : string
              read  GetField
              write SetField;
    {: @abstract Czy umieszczać pola w cudzysłowach.

       Właściwość @name wpływa na działanie metody @link(Save).  }
    property  UseTextSep:  TCSVTextSep
              read  fUseTextSep
              write fUseTextSep;

    {: @abstract Informuje o postępie importu danych. }
    property  OnImportProgress:  TCSVImportProgressEvent
              read  fOnProgress
              write fOnProgress
              default  nil;
  end { TGSkCSV };

  ECSV = class(Exception);


implementation


uses
  GSkPasLib.PasLib, GSkPasLib.Classes, GSkPasLib.ASCII, GSkPasLib.StrUtils, GSkPasLib.Dialogs;


const
  CR = #13;
  LF = #10;


{$REGION 'TGSkCSV'}

function  TGSkCSV.AddField(const   nRecordNo:  Integer;
                           const   sValue:  string)
                           : Integer;
begin
  Result := (fRecords.Items[nRecordNo] as TStringList).Add(sValue)
end { AddField };


function  TGSkCSV.AddRecord() : Integer;
begin
  Result := fRecords.Add(TStringList.Create());
end { AddRecord };


function  TGSkCSV.AddRecord(const   Fields:  array of string)
                            : Integer;
begin
  Result := AddRecord();
  SetFields(Result, Fields)
end { AddRecord };


procedure  TGSkCSV.Clear;
begin
  fRecords.Clear()
end { Clear };


constructor  TGSkCSV.Create();
begin
  fRecords := TObjectList.Create();
  fchFldSep := fchStdFldSep;
end { Create };


procedure  TGSkCSV.DeleteRecord(const   nRecordNo:  Integer);
begin
  fRecords.Delete(nRecordNo)
end { DeleteRecord };


destructor TGSkCSV.Destroy;
begin
  fRecords.Free();
  inherited
end { Destroy };


procedure  TGSkCSV.DoOnProgress(const   nPosition, nMax:  Integer;
                                var   bCanceled:  Boolean);
begin
  if  Assigned(fOnProgress)  then
    fOnProgress(Self, nPosition, nMax, bCanceled)
end { DoOnProgress };


function  TGSkCSV.GetField(const   nRecordNo, nFieldNo:  Integer)
                           : string;
begin
  Result := (fRecords.Items[nRecordNo] as TStringList).Strings[nFieldNo]
end { GetField };


function  TGSkCSV.GetFieldCount(const   nRecordNo:  Integer)
                                : Integer;
begin
  if  nRecordNo < fRecords.Count  then
    Result := (fRecords.Items[nRecordNo] as TStringList).Count
  else
    Result := 0
end { GetFieldCount };


function  TGSkCSV.GetRecordCount() : Integer;
begin
  Result := fRecords.Count
end { GetRecordCount };


procedure  TGSkCSV.Import(const   dsDataSet:  TDataSet;
                          const   Fields:  array of string;
                          const   bHeader, bDisplayLabel:  Boolean);
var
  lBuf:  array of  string;
  lIdx:  array of  TField;
  lFld:  TField;
  nInd:  Integer;
  nRec:  Integer;
  nMax:  Integer;
  lPos:  TBookmark;
  bEsc:  Boolean;

begin  { Import }
  Clear();
  if  Length(Fields) = 0  then
    raise  ECSV.Create('Empty table of fields');
  SetLength(lIdx, 0);
  for  nInd := Low(Fields)  to  High(Fields)  do
    begin
      lFld := dsDataSet.FindField(Fields[nInd]);
      if  lFld = nil  then
        raise  ECSV.CreateFmt('Field ''%s'' does not exists', [Fields[nInd]]);
      SetLength(lIdx, Succ(Length(lIdx)));
      lIdx[High(lIdx)] := lFld
    end { for nInd };
  SetLength(lBuf, Length(lIdx));
  if  bHeader  then
    begin
      for  nInd := High(lIdx)  downto  Low(lIdx)  do
        with  lIdx[nInd]  do
          if  bDisplayLabel  then
            lBuf[nInd] := DisplayLabel
          else
            lBuf[nInd] := FieldName;
      Self.AddRecord(lBuf)
    end { bHeader };
  dsDataSet.DisableControls();
  try
    if  Assigned(fOnProgress)  then
      dsDataSet.Last();   // update RecordCount
    nMax := dsDataSet.RecordCount - 1;
    lPos := dsDataSet.GetBookmark();
    try
      bEsc := False;
      DoOnProgress(0, nMax, bEsc);
      if  not bEsc  then
        begin
          nRec := 0;
          dsDataSet.First();
          while  not dsDataSet.Eof  do
            begin
              Inc(nRec);
              DoOnProgress(nRec, nMax, bEsc);
              if  bEsc  then
                Break;
              for  nInd := High(lIdx)  downto  Low(lIdx)  do
                with  lIdx[nInd]  do
                  if  IsNull  then
                    lBuf[nInd] := ''
                  else
                    lBuf[nInd] := AsString;
              AddRecord(lBuf);
              dsDataSet.Next()
            end { while }
        end { not bEsc }
    finally
      dsDataSet.GotoBookmark(lPos);
      dsDataSet.FreeBookmark(lPos)
    end { try-except }
  finally
    dsDataSet.EnableControls()
  end { try-except }
end { Import };


procedure  TGSkCSV.Import(const   dsDataSet:  TDataSet;
                          const   bHeader, bDisplayLabel:  Boolean);
var
  lFields:  array of  string;
  nInd:  Integer;

begin  { Import }
  SetLength(lFields, dsDataSet.FieldCount);
  for  nInd := dsDataSet.Fields.Count - 1  downto  0  do
    lFields[nInd] := dsDataSet.Fields[nInd].FieldName;
  Import(dsDataSet, lFields, bHeader, bDisplayLabel)
end { Import };


procedure  TGSkCSV.InsertField(const   nRecordNo, nBeforeFieldNo:  Integer;
                               const   sValue:  string);
begin
  (fRecords.Items[nRecordNo] as TStringList).Insert(nBeforeFieldNo, sValue)
end { InsertField };


procedure  TGSkCSV.InsertRecord(const   nBeforeRecNo:  Integer);
begin
  fRecords.Insert(nBeforeRecNo, TStringList.Create())
end { InsertRecord };


procedure  TGSkCSV.InsertRecord(const   nBeforeRecNo:  Integer;
                                const   Fields:  array of string);
begin
  InsertRecord(nBeforeRecNo);
  SetFields(nBeforeRecNo, Fields)
end { InsertRecord };


procedure  TGSkCSV.Load({const} sInputFile:  string{$IFDEF UNICODE};
                        const   Encoding:  TEncoding = nil{$ENDIF});
var
  lInput:   TFileStreamType;

begin  { Load }
  if  ExtractFileExt(sInputFile) = ''  then
    sInputFile := sInputFile + '.csv';
  lInput := TFileStreamType.Create(sInputFile, fmOpenRead or fmShareDenyWrite);
  try
    Load(lInput{$IFDEF UNICODE}, Encoding{$ENDIF})
  finally
    lInput.Free()
  end { try-finally }
end { Load };


procedure  TGSkCSV.Load(const   InputStream:  TFileStreamType{$IFDEF UNICODE};
                       {const}  Encoding:  TEncoding{$ENDIF});
const
  csetBlanks = [#0..Pred(CR), Succ(CR)..' '];

var
  chBuf:    Char;
  lRecord:  TStringList;
  sField:   string;
  bNewRec:  Boolean;
  bNewFld:  Boolean;
  nLineNo:  Integer;
 {$IFDEF UNICODE}
  nSize:    Integer;
  lBuffer:  TBytes;
{$ENDIF UNICODE}

  function  SkipBlanks(var   chBuf:  Char)
                       : Char;
  begin
    while  not InputStream.Eof()  do
      begin
        chBuf := InputStream.ReadDataChar();
        if  not CharInSet(chBuf, csetBlanks)  then
          Break
      end { while not InputStream.Eof() };
    Result := chBuf
  end { SkipBlanks };

begin  { Load }
  { TODO 4 -cSprawdzić: Warto by dokładnie przetestować tę metodę (Load) }
  {                     Trzeba sprawdzić czytanie plików ANSI/UTF-8/Unicode }
  fRecords.Clear();
  InputStream.Seek(0, soBeginning);
  {$IFDEF UNICODE}
    nSize := Min(InputStream.Size, 50);
    SetLength(lBuffer, nSize);
    InputStream.ReadBuffer(lBuffer[0], nSize);
    nSize := TEncoding.GetBufferEncoding(lBuffer, Encoding, nil);
    InputStream.Seek(nSize, soFromBeginning);
  {$ENDIF UNICODE}
  chBuf := #0;
  while  not InputStream.Eof()
         and (chBuf <= ' ')  do
    chBuf := InputStream.ReadDataChar();
  if  InputStream.Eof()  then
    Exit;   // input file is empty
  bNewRec := True;
  bNewFld := False;
  nLineNo := 1;
  lRecord := nil;
  repeat
    if  bNewRec  then
      begin
        if  bNewFld  then
          lRecord.Add('');
        lRecord := TStringList.Create();
        fRecords.Add(lRecord);
        bNewRec := False
      end { bNewRec };
    sField := '';
    if  chBuf = '"'  then
      begin
        repeat
          if  InputStream.Eof()  then
            raise  ECSV.Create({$IFDEF GetText}
                                 dgettext('GSkPasLib', 'Unexpected end of file')
                               {$else}
                                 'Niespodziewany koniec pliku'
                               {$ENDIF -GetText});
          chBuf := InputStream.ReadDataChar();
          if  chBuf = '"'  then  // either end of field or '""' inside field
            begin
              if  InputStream.Eof()  then
                Break;
              chBuf := InputStream.ReadDataChar();
              if  chBuf = '"'  then   // '""' inside field
                begin
                  sField := sField + '"';
                  Continue
                end { chBuf = '"' };
              { After field have to be blank characters or field separator }
              if  chBuf <> CR  then
                if  chBuf <= ' '  then
                  if  SkipBlanks(chBuf) = CR  then
                    bNewRec := True;
              if  not CharInSet(chBuf, [CR, fchFldSep])  then
                raise  ECSV.CreateFmt({$IFDEF GetText}
                                        dgettext('GSkPasLib', 'Incorrect file structure in line %d'),
                                      {$ELSE}
                                        'Niepoprawna struktura pliku w wierszu nr %d',
                                      {$ENDIF -GetText}
                                      [nLineNo]);
              Break
            end { chBuf = '"' }
          else
            if  chBuf = LF  then
              Inc(nLineNo);
          sField := sField + chBuf
        until  False
      end { chBuf = '"' }
    else
      begin
        while  not CharInSet(chBuf, [fchFldSep, CR])  do
          begin
            if  chBuf < ' '  then
              raise  ECSV.CreateFmt({$IFDEF GetText}
                                      dgettext('GSkPasLib', 'Unexpected character #$%0:x in line %1:d'),
                                    {$ELSE}
                                      'Nieoczekiwany znak #$%x w wierszu nr %d',
                                    {$ENDIF -GetText}
                                    [Ord(chBuf), nLineNo]);
            sField := sField + chBuf;
            if  InputStream.Eof()  then
              Break;
            chBuf := InputStream.ReadDataChar();
          end { while };
        sField := Trim(sField)
      end { chBuf <> '"' };
    lRecord.Add(sField);
    bNewFld := chBuf = fchFldSep;
    if  bNewFld  then
      if  SkipBlanks(chBuf) = CR  then
        bNewRec := True;
    if  (chBuf = CR)
        and not InputStream.Eof()  then
      begin
        bNewRec := True;
        chBuf := InputStream.ReadDataChar();
        if  (chBuf = LF)
            and not InputStream.Eof()  then
          chBuf := InputStream.ReadDataChar();
        Inc(nLineNo)
      end { CR }
  until  InputStream.Eof();
  if  bNewFld  then
    lRecord.Add('')
end { Load };


procedure  TGSkCSV.Save({const} sOutputFile:  string{$IFDEF UNICODE};
                        const   Encoding:  TEncoding{$ENDIF});

var
  lOutput:  TFileStreamType;
 {$IFDEF GetText}
  asOK:  array[0..0] of  string;
  anOK:  array[0..0] of  Integer;
 {$ENDIF GetText}

begin  { Save }
  if  ExtractFileExt(sOutputFile) = ''  then
    sOutputFile := sOutputFile + '.csv';
  try
    lOutput := TFileStreamType.Create(sOutputFile, fmCreate or fmShareDenyWrite);
  except
    on  eErr : Exception  do
      begin
        {$IFDEF GetText}
          asOK[0] := dgettext('GSkPasLib', 'OK');
          anOK[0] := 1;
          MsgDlgEx(dgettext('GSkPasLib', 'Error'),
                   Format(dgettext('GSkPasLib',
                                   'Creating file %0:s raised exception %1:s: %2:s'),
                          [sOutputFile, eErr.ClassName(), eErr.Message]),
                   mtError, asOK, anOK);
        {$else}
          MsgDlg('Tworzenie pliku %s wygenerowało wyjątek %s: %s',
                 [sOutputFile, eErr.ClassName(), eErr.Message],
                 mtError);
        {$ENDIF}
        raise
      end { on Exception }
  end { try-except };
  try
    Save(lOutput{$IFDEF UNICODE}, Encoding{$ENDIF})
  finally
    lOutput.Free()
  end { try-finally }
end { Save };


procedure  TGSkCSV.Save(const   OutputStream:  TStream{$IFDEF UNICODE};
                        const   Encoding:  TEncoding{$ENDIF});
var
  nRec:  Integer;
  nFld:  Integer;
  nChr:  Integer;
  sFld:  string;
 {$IFDEF UNICODE}
  lBuf:  TBytes;
 {$ENDIF UNICODE}
  lRec:  TStringList;
  bDel:  Boolean;   // use delimiter

begin  { Save }
  {$IFDEF UNICODE}
    if  Encoding = nil  then
      begin
        Save(OutputStream, TEncoding.Default);
        Exit
      end { Encoding = nil };
  {$ENDIF UNICODE}
  OutputStream.Size := 0;
  {$IFDEF UNICODE}
    lBuf := Encoding.GetPreamble();
    if  Length(lBuf) <> 0  then
      OutputStream.WriteBuffer(lBuf[0], Length(lBuf));
  {$ENDIF UNICODE}
  for  nRec := 0  to  RecordCount - 1  do
    begin
      lRec := fRecords.Items[nRec] as TStringList;
      for  nFld := 0  to  lRec.Count - 1  do
        begin
          if  nFld > 0  then
            begin
              {$IFDEF UNICODE}
                lBuf := Encoding.GetBytes(fchFldSep);
                OutputStream.WriteBuffer(lBuf[0], Length(lBuf))
              {$ELSE}
                OutputStream.WriteDataChar(fchFldSep)
              {$ENDIF -UNICODE}
            end { nFld > 0 };
          sFld := lRec.Strings[nFld];
          bDel := False;
          case  fUseTextSep  of
            tsAsNeeded:
              for  nChr := 1  to  Length(sFld)  do
                if  CharInSet(sFld[nChr], [#0..' ', #127, #160, #255, '"', fchFldSep])  then
                  begin
                    bDel := True;
                    Break
                  end { if; for };
            tsNever:
              bDel := False;
            tsAlways:
              bDel := True
          end { case fUseTextSep };
          if  bDel  then
            sFld := QuoteStr(sFld, '"');
          {$IFDEF UNICODE}
            lBuf := Encoding.GetBytes(sFld);
            if  Length(lBuf) <> 0  then
              OutputStream.WriteBuffer(lBuf[0], Length(lBuf))
          {$ELSE}
            OutputStream.WriteDataString(sFld)
          {$ENDIF -UNICODE}
        end { for nFld };
      {$IFDEF UNICODE}
        lBuf := Encoding.GetBytes(sLineBreak);
        OutputStream.WriteBuffer(lBuf[0], Length(lBuf))
      {$ELSE}
        OutputStream.WriteDataString(sLineBreak)
      {$ENDIF -UNICODE}
    end { for nRec }
end { Save };


procedure  TGSkCSV.SetField(const   nRecordNo, nFieldNo:  Integer;
                            const   sValue:  string);
var
  lFields:  TStringList;

begin  { SetField }
  lFields := fRecords.Items[nRecordNo] as TStringList;
  while  lFields.Count <= nFieldNo  do
    lFields.Add('');
  lFields.Strings[nFieldNo] := sValue
end { SetField };


procedure  TGSkCSV.SetFields(const   nRecordNo:  Integer;
                             const   Fields:  array of string);
var
  nFld:  Integer;

begin  { SetFields }
  with  TStringList(fRecords.Items[nRecordNo])  do
    for  nFld := Low(Fields)  to  High(Fields)  do
      Add(Fields[nFld])
end { SetFields };


procedure  TGSkCSV.SetFieldSeparator(const   chValue:  Char);
begin
  if  CharInSet(chValue, [#0, CR, LF, '"'])  then
    raise  ERangeError.Create({$IFDEF GetText}
                                dgettext('GSkPasLib', 'Incorrect seperator of the CSV fields')
                              {$ELSE}
                                'Niepoprawny znak separatora pól CSV'
                              {$ENDIF -GetText});
  fchFldSep := chValue;
end { SetFieldSeparator };

{$ENDREGION 'TGSkCSV'}


end.
