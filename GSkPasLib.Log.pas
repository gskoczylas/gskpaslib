﻿{: @abstract(Moduł @name definiuje obiekty i podprogramy ułatwiające śledzenie
             pracy programu lub biblioteki.)

   Obiekt @link(TGSkLog) jest modułem wirtualnym. Aby działał trzeba
   zdefiniować obiekt (lub obiekty) rejestrujące zwane adapterami. Takie obiekty
   muszą być potomkami zdefiniowanego w tym module obiektu
   @link(TGSkCustomLogAdapter).

   W bibliotece jest zdefiniowane kilka adapterów. Są ona dostępna w modułach
   @code(GSkPasLib.Log…).

   Wszystkie adaptery muszą być zarejestrowane w głównym module @link(TGSkLog).

   @seeAlso(GSkPasLib.LogFile) }
unit  GSkPasLib.Log;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$WARN SYMBOL_PLATFORM OFF}
{$IF CompilerVersion > 22} // XE
  {$LEGACYIFEND ON}
{$IFEND}

{.$DEFINE ShowLogLevel}


{ TODO 5 -cOptymalizacja : Rejestrowanie informacji poprzez zewnętrzny proces }
{ Rejestrowanie informacji zauważalnie spowalnia działanie monitorowanej
  aplikacji, zwłaszcza gdy rejestrowanych jest wiele szczegółowych informacji.
  Najprostszym pomysłem wyeliminowania tego spowolnienia jest rejestrowanie
  informacji w oddzielnym wątku. Po gruntownej analizie doszedłem do wniosku,
  że to nie jest takie proste. Ponieważ VCL i tak nie jest thread-safe, więc
  krytyczne działania i tak musiałyby być wykonywane w głównym wątku przez
  metodę Synchronize. W efekcie przyspieszenie byłoby całkowicie pomijalne,
  a kod znacząco by się skomplikował.

  Gdyby kiedykolwiek istniała konieczność przyspieszenia monitorowania, to
  jedynym skutecznym sposobem jest napisanie oddzielnego programu do zapisywania
  monitorowanych informacji. Wtedy ta metoda przekazywałaby jedynie te informacje
  do tego zewnętrznego procesu, a on by je zapisywał do pliku itp. }


uses
  {$IF CompilerVersion > 22} // XE
    WinAPI.Windows,
    System.Classes, System.SysUtils, System.DateUtils, System.SyncObjs, System.Math,
    System.StrUtils, System.Contnrs, System.Types, System.Variants,
    System.TypInfo,
  {$ELSE}
    Windows,
    Classes, SysUtils, DateUtils, SyncObjs, Math,
    StrUtils, Contnrs, ConvUtils, Types, Variants,
    TypInfo,
  {$IFEND}
  JclSysInfo,
  {$IFDEF GetText}
    JvGnugettext,
  {$ENDIF GetText}
  GSkPasLib.CustomComponents, GSkPasLib.PasLibInternals;


const
  {: @abstract Znak używany w procedurze @link(TGSkLog.LogSeparator).  }
  gccSeparatorChar = '-';

  {: @abstract(Liczba powtórzeń znaku @code(gccSeparatorChar) w procedurze
               @link(TGSkLog.LogSeparator).)  }
  gcnSeparatorLen  = 50;

  {: @abstract Rozpoczęcie sesji monitorowania aplikacji.
     Napis @name identyfikuje rozpoczęcie sesji monitorowania aplikacji. }
  gcsSessionStart = '*** START ***';
  {: @abstract Zakończenie sesji monitorowania aplikacji.
     Napis @name identyfikuje zakończenie sesji monitorowania aplikacji. }
  gcsSessionStop = '* STOP *   // %s';
  {: @abstract O ile jest zwiększany lub zmniejszany poziom monitorowania.
     @seeAlso(TGSkLog.Offset)  }
  gcnLogOffsetStep = 2;


type
  {: @abstract Rodzaj monitorowanych informacji.

     Moduł pozwala śledzić informacje, ostrzeżenia i błędy. Sposób rejestrowania
     poszczególnych rodzajów informacji zależy od konkretnego adaptera
     (będącego potomkiem obiektu @link(TGSkCustomLogAdapter)).

     Podczas monitorowania aplikacji można automatycznie filtrować mniej ważne
     informacje (pomijać je podczas monitorowania).

     Standardowo wykorzystywane są tylko poziomy @code(lmInformation..lmError).
     Poziomy @code(lmDebugLowLevel..lmDebug) są przeznaczone do wykorzystania
     w aplikacjach, które potrzebują więcej poziomów monitorowania. Są one
     rejestrowane tak samo, jak informacje typu @code(lmInformation).  }
  TGSkLogMessageType = (lmDebugLowLevel = -3, lmDebugDetails = -2, lmDebug = -1,
                        lmInformation = 0, lmWarning = 1, lmError = 2);

  TGSkLogStdMessageType = lmInformation..lmError;

  {: @abstract Rodzaj monitorowanych informacji.

     Moduł pozwala śledzić informacje, ostrzeżenia i błędy. Sposób rejestrowania
     poszczególnych rodzajów informacji zależy od konkretnego adaptera
     (będącego potomkiem obiektu @link(TGSkCustomLogAdapter)).  }
  TGSkLogMessageTypes = set of TGSkLogStdMessageType;

  {: @abstract Format monitorowanych liczb całkowitych.

     Jeżeli monitorowana jest liczba całkowita to można wskazać format ich zapisu.

     @value(vtDec Liczby mają być zapisywane w systemie dziesiętnym.)
     @value(vtHex Liczby mają być zapisane w systemie szesnastkowym.)
     @value(vtBin Liczby mają być zapisane w systemie dwójkowym.)
     @value(vtDecHex Liczby mają być zapisane w systemie dziesiętnym oraz
                     dodatkowo w systemie szesnastkowym.)
     @value(vtHexDec Liczby mają być zapisane w systemie szesnastkowym oraz
                     dodatkowo w systemie dziesiętnym.)
     @value(vtHexBin Liczby mają być zapisane w systemie szesnastkowym oraz
                     dodatkowo w systemie dwójkowym.)  }
  TGSkLogValueNumType = (vtDec, vtHex, vtBin, vtDecHex, vtHexDec, vtHexBin);

  {: @abstract Sposób interpretowania liczb typu @code(Currency).

     Jeżeli monitorowana jest liczba typu @code(Currency), to można wskazać
     sposób interpretowania takiej wartości.

     @value(ctMoney Liczba ma być interpretowana jako waluta.)
     @value(ctPercent Liczba ma być interpretowana jako procenty. )
     @value(ctNumber Liczba ma być interpretowana jako liczba rzeczywista. ) }
  TGSkLogValueCurrType = (ctMoney, ctPercent, ctNumber);


const
  {: @abstract Rodzaje informacji wyróżnianych podczas śledzenia.

     Różne rodzaje informacji mogą być specjalnie wyróżniane przez moduły
     raportujące. Standardowo wyróżnianie są ostrzeżenia i błędy.

     Sposób wyróżniania zależy od konkretnego modułu raportującego. Niektóre
     moduły wcale nie wyróżniają informacji.  }
  gcsetShowMsgTypes = [lmWarning, lmError];

  lmAppLevel1 = lmDebug           deprecated;
  lmAppLevel2 = lmDebugDetails    deprecated;
  lmAppLevel3 = lmDebugLowLevel   deprecated;


type
  TGSkLog = class;
  TGSkLogMessage = class;

  {: @abstract Wyjątki typu @name występują w modułach monitorowania.  }
  EGSkLogException = class(EGSkPasLibError);

  {: @abstract Flagi informujące o rodzaju informacji w TGSkLogMessage.

     Flagi @name informują o rodzaju informacji przechowywanych w rekordach typu
     @link(TGSkLogMessage).

     @value(mfMemo        Rejestrowana jest notatka (wielowierszowa).)
     @value(mfLogTime     Rejestrować czas informacji.)
     @value(mfLogThreadID Rejestrować identyfikator wątku.)
     @value(mfSession     Rejestrować rozpoczęcie i zakończenie sesji.)
     @value(mfEnter       To jest wejście do bloku rejestracji (wejście do podprogramu).)
     @value(mfExit        To jest wyjście z bloku rejestracji (wyjście z podprogramu).)
     @value(mfInit        To są wiadomości inicjujące rejestrowanie).
     @value(mfCustom      Flaga może być wykorzystana przez klasy potomne.) }
  TGSkLogMessageFlag = (mfMemo, mfLogTime, mfLogThreadID, mfSession, mfEnter, mfExit,
                        mfInit, mfCustom);
  TGSkLogMessageFlags = set of TGSkLogMessageFlag;

  {: @abstract(Obiekty typu @name (i typów potomnych) służą do przekazywania
               informacji między komponentem @link(TGSkLog) oraz adapterami.)

     @seeAlso(TGSkLogMessage)  }
  TGSkCustomLogMessage = class abstract(TObject);

  {: @abstract Znacznik początku bloku rejestrowanych informacji.

     Każde wywołanie metody @link(TGSkLog.BeginLog) powoduje zarejestrowanie
     komunikatu typu @name. Dzięki temu możliwa jest implementacja metody
     @link(TGSkLog.AbandonLog).

     @seeAlso(TGSkLogMessage)  }
  TGSkLogMessageMarker = class(TGSkCustomLogMessage)
  strict private
    fnLogLevel:   Cardinal;
    fnLogOffset:  Cardinal;
  public
    constructor  Create(const   nLogLevel:  Cardinal;
                        const   nLogOffset:  Cardinal);
    {-}
    property  LogLevel:  Cardinal
              read  fnLogLevel;
    property  LogOffset:  Cardinal
              read  fnLogOffset;
  end { TGSkLogMessageMarker };

  {: @abstract Informacje przekazywane z obiektu @link(TGSkLog) do adapterów.

     Informacje zarejestrowane przez komponent @link(TGSkLog) są przekazywane
     do @link(TGSkCustomLogAdapter adapterów logowania) jako obiekty typu @name. }
  TGSkLogMessage = class(TGSkCustomLogMessage)
  strict private
    class var
      fnThreadIDLen:   Integer;
    var
      fsMessage:       string;
      fsPrefix:        string;
      fmtMessageType:  TGSkLogMessageType;
      fnMessageOffset: Cardinal;
      ftmMessageTime:  TDateTime;
      fnThreadID:      DWORD;
      fsetShowTypes:   TGSkLogMessageTypes;
      fsetFlags:       TGSkLogMessageFlags;
{$IF CompilerVersion >= 22} // XE
    class constructor  Create();
{$ELSE}
  private
    class procedure  ClassCreate();
{$IFEND}
  public
    {: @abstract Konstruktor inicjujący wszystkie pola.

       Konstruktor tworzy obiekt typu @className i inicjuje jego pola
       wartościami przekazanymi w parametrach.

       @seeAlso(Clone)  }
    constructor  Create(const   sMessage:  string;
                        const   sPrefix:  string;
                        const   mtMessageType:  TGSkLogMessageType;
                        const   nOffset:  Cardinal;
                        const   setShowTypes:  TGSkLogMessageTypes;
                        const   setFlags:  TGSkLogMessageFlags;
                        const   tmMessageTime:  TDateTime = 0.0;
                        const   nThreadID:  DWORD = High(DWORD));

    {: @abstract Klonuje bieżący obiekt.

       Metoda @name tworzy nowy obiekt, którego wszystkie właściwości są takie
       same, jak bieżący obiekt.  }
    function  Clone() : TGSkLogMessage;                              virtual;

    {: @abstract Przekształca typ informacji w znak.

       Niektóre adaptery wykorzystują tę funkcję do wyróżniania rodzajów informacji
       wskazanych poprzez parametr @code(setShowMsgTypes).

       @return(Wynikiem jest znak odpowiadający wskazanemu rodzajowi
               informacji lub odstęp.)  }
    function  MessageTypeToChar() : Char;                            overload;

    {: @abstract Przekształca typ informacji w znak.

       @param(MessageType Typ informacji.)

       @return(Wynikiem jest znak odpowiadający wskazanemu rodzajowi
               informacji.)  }
    class function  MessageTypeToChar(const   MessageType:  TGSkLogMessageType)
                                      : Char;                        overload;

    {: @abstract(Przekształca datę w format wykorzystywany podczas rejestrowania
                 informacji.)  }
    function  FormatLogTime() : string;

    {: @abstract(Formatuje identyfikator wątku jako liczbę szesnastkową.) }
    function  FormatThreadID() : string;

    {: @abstract Formatuje treść wiadomość.

       @param(Msg Formatowana informacja. )
       @param(bType Wskazuje czy ma być uwzględniony typ informacji. )
       @param(bTime Wskazuje czy ma być uwzględniony czas utworzenia informacji.
                    @false oznacza czy w miejscu przewidzianym na czas utworzenia
                    informacji będą odstępy. )
       @param(bPrefix Wskazuje czy uwzględnić @link(Prefix prefiks) informacji. )

       @return(Wynikiem funkcji jest odpowiednio sformatowana informacja)  }
    function  MessageToStr(const   bType, bTime, bThreadID, bPrefix:  Boolean)
                           : string;

    {: @abstract Treść monitorowanej informacji.  }
    property  MessageText:  string
              read  fsMessage
              write fsMessage;
    {: @abstract Treść @link(TGSkLog.Prefix prefiksu) monitorowanej informacji. }
    property  Prefix:  string
              read  fsPrefix
              write fsPrefix;
    {: @abstract Typ informacji.  }
    property  MessageType:  TGSkLogMessageType
              read  fmtMessageType
              write fmtMessageType;
    {: @abstract Poziom informacji.  }
    property  MessageOffset:  Cardinal
              read  fnMessageOffset
              write fnMessageOffset;
    {: @abstract Czas wygenerowania informacji.  }
    property  MessageTime:  TDateTime
              read  ftmMessageTime
              write ftmMessageTime;
    {: @abstract Identyfikator wątku, który wygenerował tę informację. }
    property  ThreadID:  DWORD
              read  fnThreadID
              write fnThreadID;
    {: @abstract Które rodzaje informacji powinny być wyróżnione.
       Informacje @code(lmAppLevel3..lmAppLevel1) są traktowane jak informacje
       @code(lmInformation). }
    property  ShowMessageTypes:  TGSkLogMessageTypes
              read  fsetShowTypes
              write fsetShowTypes;
    {: @abstract Atrybuty rejestrowanej informacji.  }
    property  Flags:  TGSkLogMessageFlags
              read  fsetFlags
              write fsetFlags;
  end { TGSkLogMessage };

  {: @abstract Lista zarejestrowanych informacji.

     Wszystkie informacje rejestrowane po wywołaniu @link(TGSkLog.BeginLog) muszą
     zostać zapamiętane do czasu wywołania albo @link(TGSkLog.EndLog), albo
     @link(TGSkLog.AbandonLog). Są one pamiętane w obiektach typu @name.

     Również niektóre adaptery mogą buforować rejestrowane informacje w obiektach
     typu @name.  }
  TGSkLogMessages = class
  strict private
    var
      fMessages:  TObjectList;
      fnCurrMsg:  Integer;
  public
    {: @exclude }
    constructor  Create();
    {: @exclude }
    destructor  Destroy();                                           override;

    {: @abstract Informuje, czy są zapamiętane jakieś informacje.
       @seeAlso IsEmpty  }
    function  HasMassages() : Boolean;

    {: @abstract Informuje, czy lista zapamiętanych informacji jest pusta.
       @seeAlso HasMassages  }
    function  IsEmpty() : Boolean;

    {: @abstract Informuje o poziomie monitorowania ostatniej zarejestrowanej informacji.
       Jeżeli ostatnia informacja nie jest typu @link(TGSkLogMessageMarker), to
       wynikiem funkcji jest @code(0).

       Jeżeli ostatnia informacja jest obiektem typu @link(TGSkLogMessageMarker)
       to parametrowi @code(nOffset) jest przypisywana wartość przesunięcia
       zapamiętana w tym obiekcie.  }
    function  LastMessageLogLevel(var   nOffset:  Cardinal)
                                  : Cardinal;

    {: @abstract Zwraca pierwszy obiekt typu @link(TGSkLogMessage).

       Metoda @name zaprojektowana jest do wykorzystywania w adapterach, do których
       są przekazywane tylko informacje typu @link(TGSkLogMessage) (obiekty typu
       @link(TGSkLogMessageMarker) są pomijane).

       @returns(Wynikiem funkcji jest albo obiekt typu @link(TGSkLogMessage),
                albo @code(nil) -- gdy lista zapamiętanych informacji jest pusta.)

       @seeAlso NextMessage
       @seeAlso LastMessage  }
    function  FirstMessage() : TGSkLogMessage;

    {: @abstract Zwraca kolejny obiekt typu  @link(TGSkLogMessage).

       Metoda @name zaprojektowana jest do wykorzystywania w adapterach, do których
       są przekazywane tylko informacje typu @link(TGSkLogMessage) (obiekty typu
       @link(TGSkLogMessageMarker) są pomijane).

       @returns(Wynikiem funkcji jest albo obiekt typu @link(TGSkLogMessage),
                albo @code(nil) -- gdy w liście zapamiętanych informacji nie ma
                więcej informacji.)

       @seeAlso FirstMessage
       @seeAlso LastMessage  }
    function  NextMessage() : TGSkLogMessage;

    {: @abstract Zwraca ostatni obiekt typu @link(TGSkLogMessage).

       Metoda @name zaprojektowana jest do wykorzystywania w adapterach, do których
       są przekazywane tylko informacje typu @link(TGSkLogMessage) (obiekty typu
       @link(TGSkLogMessageMarker) są pomijane).

       @returns(Wynikiem funkcji jest albo obiekt typu @link(TGSkLogMessage),
                albo @code(nil) -- gdy lista zapamiętanych informacji jest pusta.)

       @seeAlso FirstMessage
       @seeAlso NextMessage  }
    function  LastMessage() : TGSkLogMessage;

    {: @abstract Liczba zapamiętanych informacji.  }
    function  CustomMessageCount() : Integer;

    {: @abstract Czy wskazana informacja jest typu @link(TGSkLogMessageMarker). }
    function  IsLogMessageMarker(const   nIndex:  Integer)
                                 : Boolean;

    {: @abstract Zwraca wskazaną informację typu @link(TGSkLogMessage). }
    function  LogMessage(const   nIndex:  Integer)
                         : TGSkLogMessage;

    {: @abstract Dodaje kolejną informację do listy zapamiętanych informacji. }
    procedure  AddMessage(const   LogMessage:  TGSkCustomLogMessage);

    {: @abstract Usuwa ostatnią zapamiętaną wiadomość.  }
    procedure  RemoveLastMessage();

    {: @abstract Usuwa wszystkie zapamiętane wiadomości.  }
    procedure  Clear();
  end { TGSkLogMessages };


  {: @abstract(Zdarzenia typu @name są generowane przed i po zarejestrowaniu
               każdej informacji.)  }
  TGSkLogMessageEvent = procedure(const   Sender:  TObject;
                                  const   Msg:     TGSkLogMessage)  of object;

  {: @abstract Bazowy adapter.

   Obiekt @link(TGSkLog) jest modułem wirtualnym. Aby działał, trzeba zdefiniować
   obiekt (lub obiekty) rejestrujące zwane adapterami. Takie adaptery muszą być
   potomkami zdefiniowanego w tym module obiektu @link(TGSkCustomLogAdapter).

   W tej bibliotece jest kilka modułów rejestrujących informacje. Są one dostępna
   w modułach @code(GSkPasLib.Log…).

   @seeAlso(TGSkLogFile) }
  TGSkCustomLogAdapter = class abstract(TGSkCustomComponent)
  strict private
    var
      fsLogName:       string;
      fsLogFullName:   string;
      fsModuleName:    string;
      fEndSessionMsg:  TGSkLogMessage;
      fParent:         TGSkLog;
    procedure  SetParent(const   Value:  TGSkLog);
  private
    class function  EvalLogName(const   sName:  string;
                                const   bFull:  Boolean)
                                : string;

  strict protected

    {: @exclude}
    procedure  Notification(AComponent:  TComponent;
                            Operation:  TOperation);                 override;

    {: @abstract Wewnętrzna nazwa dziennika.

       Jeżeli nazwa dziennika nie została zdefiniowana, to właściwość
       @link(LogName) zwraca domyślną nazwę dziennika. Natomiast właściwość
       @name zawsze zwraca zdefiniowaną lub pustą nazwę dziennika.

       @seeAlso(LogName)  }
    property  InternalLogName:  string
              read  fsLogName;

    {: @abstract Wewnętrzna pełna nazwa dziennika.

       Jeżeli pełna nazwa dziennika nie została zdefiniowana, to właściwość
       @link(LogFullName) zwraca domyślną pełną nazwę dziennika. Natomiast
       właściwość @name zawsze zwraca zdefiniowaną lub pustą pełną nazwę dziennika.

       @seeAlso(LogFullName)  }
    property  InternalLogFullName:  string
              read  fsLogFullName;

  protected

    {: @abstract Obiekt, do którego adapter jest podłączony.

       Dzięki właściwości @name adapter może w razie potrzeby monitorować
       własne działania (zwłaszcza wyjątki). }
    property  Parent:  TGSkLog
              read  fParent
              write SetParent;

    {: @abstract Zwraca bieżącą nazwę dziennika.

       Funkcja @name zwraca bieżącą nazwę dziennika. Jest to część implementacji
       właściwości @link(LogName).  }
    function  GetLogName() : string;                                 virtual;

    {: @abstract Zwraca bieżącą pełną nazwę dziennika.

       Funkcja @name zwraca bieżącą pełną nazwę dziennika. Jest to część
       implementacji właściwości @link(LogFullName).  }
    function  GetLogFullName() : string;                             virtual;

    {: @abstract Definiuje nową nazwę dziennika.

       Metoda @name definiuje nową nazwę dziennika. Jest to część implementacji
       właściwości @link(LogName).  }
    procedure  SetLogName(const   sName:  string);                   virtual;

    {: @abstract Definiuje nową pełną nazwę dziennika.

       Metoda @name definiuje nową pełną nazwę dziennika. Jest to część
       implementacji właściwości @link(LogFullName).  }
    procedure  SetLogFullName(const   sName:  string);               virtual;

    {: @abstract Czyści (usuwa) zarejestrowane informacje.

       Nie wszystkie adaptery realizują tę metodę. Opis jej implementacji przez
       poszczególne adaptery należy szukać w opisie metody @name danego
       adaptera.  }
    procedure  Clear();                                       virtual; abstract;

    {: @abstract Rozpoczyna buforowanie rejestrowanych informacji.

       Każde wywołanie metody @name zwiększa @link(TGSkLog.UpdateLevel poziom
       buforowania), a każde wywołanie metody @link(EndLog) -- zmniejsza go.
       Jeżeli po zmniejszeniu poziom jest równy zero to buforowane informacje
       przekazywane są do poszczególnych adapterów.

       @bold(Uwaga!) @br
       Po wywołaniu metody @name wszystkie monitorowane informacje przechowywane
       są w pamięci operacyjnej tak długo, aż zostanie wywołana metoda @link(EndLog).
       Jeżeli w międzyczasie nastąpi awaria aplikacji, informacje te przepadną.

       @seeAlso(EndLog)  }
    procedure  BeginLog();                                    virtual; abstract;

    {: @abstract Rejestruje informację.

       Jeżeli metoda @name potrzebuje przechować obiekt na później, powinna
       przechowywać kopie obiektu. Kopię obiektu można najprościej uzyskać
       wywołując metodę @link(TGSkLogMessage.Clone Clone).  }
    procedure  LogMessage(const   Msg:  TGSkLogMessage);             virtual;

    {: @abstract Kończy buforowanie informacji.

       Każde wywołanie metody @link(BeginLog) zwiększa
       @link(TGSkLog.UpdateLevel poziom buforowania), a każde wywołanie metody
       @name -- zmniejsza go. Jeżeli po zmniejszeniu poziom jest równy zero to
       buforowane informacje przekazywane są do poszczególnych adapterów.

       @seeAlso(BeginLog)  }
    procedure  EndLog();                                      virtual; abstract;

  public
    {: @exclude }
    constructor  Create(Owner:  TComponent);                         override;

    {: @exclude
       Nie ma czego dokumentować - to tylko implementacja standardowej metody. }
    procedure  Assign(Source:  TPersistent);                         override;

    {: @exclude
       Nie ma czego dokumentować - to tylko implementacja standardowej metody. }
    procedure  BeforeDestruction();                                  override;

  published
    {: @abstract Nazwa dziennika.

       Właściwość @name pozwala wskazać nazwę dziennika. Poszczególne adaptery
       różnie implementują tę właściwość. Opis implementacji dla danego adaptera
       można sprawdzić w opisie metod @link(GetLogName) oraz @link(SetLogName)
       danego adaptera lub w opisie właściwości @name danego adaptera.

       Różne adaptery korzystają albo z właściwości @name, albo z właściwości
       @link(LogFullName). Na przykład właściwość @name może być
       właściwą nazwą pliku (np. @code('Monitor.log')), a właściwość @link(LogFullName)
       zawierać pełną ścieżkę do pliku (np. @code('c:\Test\Monitor.log')).

       @seeAlso(LogFullName)
       @seeAlso(ModuleName)  }
    property  LogName:  string
              read  GetLogName
              write SetLogName;

    {: @abstract Pełna nazwa dziennika.

       Właściwość @name pozwala wskazać pełną nazwę dziennika. Poszczególne
       adaptery różnie implementują tę właściwość. Opis implementacji dla danego
       adaptera można sprawdzić w opisie metod @link(GetLogFullName) oraz
       @link(SetLogFullName) danego adaptera lub w opisie właściwości @name
       danego adaptera.

       Różne adaptery korzystają albo z właściwości @name, albo z właściwości
       @link(LogName). Na przykład właściwość @name może zawierać pełną ścieżkę
       do pliku (np. @code('c:\Test\Monitor.log')), a właściwość @link(LogName)
       zawierać właściwą nazwę pliku (np. @code('Monitor.log')).

       @seeAlso(LogName)
       @seeAlso(ModuleName)  }
    property  LogFullName:  string
              read  GetLogFullName
              write SetLogFullName;

    {: Nazwa modułu.

       Sposób wykorzystania tej właściwości zależy od implementacji danego
       adaptera. Na przykład adapter może tę nazwę umieścić w tytule pliku dziennika.

       @seeAlso(LogName)
       @seeAlso(LogFullName)  }
    property  ModuleName:  string
              read  fsModuleName
              write fsModuleName;

  end { TGSkCustomLogAdapter };

  TGSkCustomLogAdapterClass = class of TGSkCustomLogAdapter;

  {: @exclude Klasa używana wewnętrznie }
  TGSkAdapterList = class(TList)
  strict private
    var
      fParent:  TGSkLog;
  strict protected
    function  GetAdapter(const   nIndex:  Integer)
                         : TGSkCustomLogAdapter;                     inline;
    procedure  SetAdapter(const   nIndex:  Integer;
                          const   Adapter:  TGSkCustomLogAdapter);   inline;
    procedure  Notify(Ptr:  Pointer;
                      Action:  TListNotification);                   override;
  public
    constructor  Create(const   Parent:  TGSkLog);
    {-}
    property  Items[const   nIndex:  Integer] : TGSkCustomLogAdapter
              read  GetAdapter
              write SetAdapter;                                      default;
  end { TGSkAdapterList };

  {: @abstract Główny komponent rejestrowania informacji.

   Obiekt @link(TGSkLog) jest modułem wirtualnym. Aby działał trzeba
   zdefiniować obiekt (lub obiekty) rejestrujące zwane adapterami. Takie adaptery
   muszą być potomkami zdefiniowanego w tym module obiektu
   @link(TGSkCustomLogAdapter).

   W tej bibliotece jest kilka adapterów. Są one dostępna w modułach
   @code(GSkPasLib.Log…).  }
  TGSkLog = class(TGSkCustomComponent)
  strict private
    var
      ffnBeforeMessage: TGSkLogMessageEvent;
      ffnAfterMessage:  TGSkLogMessageEvent;
      fAdapters:        TGSkAdapterList;
      fSynchro:         TCriticalSection;
      fMessages:        TGSkLogMessages;
      fnUpdateLevel:    Cardinal;
      fnOffset:         Cardinal;
      fsPrefix:         string;
      fbLogMessageTime: Boolean;
      fbLogThreadID:    Boolean;
      fbActive:         Boolean;
      fbAutoSession:    Boolean;
      fbStartup:        Boolean;
      fbChkCtrlChars:   Boolean;
      fbLogAssertions:  Boolean;
      fsetShowMsgTypes: TGSkLogMessageTypes;
      fsLogName:        string;
      fsLogFullName:    string;
      fsModuleName:     string;
      fLogFilter:       TGSkLogMessageType;
      fHeader:          TStringList;
      {*** UWAGA! Zmieniając listę pól zweryfikuj metodę Assign() ***}
    function  CanLog(const  lmLogLevel:  TGSkLogMessageType)
                     : Boolean;                                      inline;
    function  GetAdapterCount() : Integer;
    function  GetAdapter(const   nIndex:  Integer)
                         : TGSkCustomLogAdapter;
    function  GetLogName() : string;
    function  GetLogFullName() : string;
    function  GetModuleName() : string;
    function  GetIsLog(const   LogLevel:  TGSkLogMessageType)
                       : Boolean;
    function  GetIsLogWarning() :  Boolean;                          inline;
    function  GetIsLogInformation() :  Boolean;                      inline;
    function  GetIsLogDebug() :  Boolean;                            inline;
    function  GetIsLogDebugDetails() :  Boolean;                     inline;
    function  GetIsLogDebugLowLevel() :  Boolean;                    inline;
    procedure  SetModuleName({const} sName:  string);
    procedure  SetLogName(const   sName:  string);
    procedure  SetLogFullName(const   sName:  string);
   {$IFDEF DEBUG}
    procedure  SetLogFilter(const   Value:  TGSkLogMessageType);
   {$ENDIF DEBUG}
    procedure  DoLog();
    procedure  Init(const   AOwner:  TComponent);
    procedure  CheckAdapters();
    procedure  CheckAdapter(const   LogAdapter:  TGSkCustomLogAdapter);
    procedure  Error(const   sFunction, sErrMsg:  string);
    procedure  FreeInternalAdapter(const   LogAdapter:  TGSkCustomLogAdapter);
    procedure  InternalLogMessage(const   sMsg:         string;
                                  const   lmMsgType:    TGSkLogMessageType;
                                 {const}  setMsgFlags:  TGSkLogMessageFlags = []);  overload;
    procedure  InternalLogMessage(const   sMsgFmt:    string;
                                  const   MsgArgs:    array of const;
                                  const   lmMsgType:  TGSkLogMessageType;
                                  const   setMsgFlags:  TGSkLogMessageFlags = []);  overload;
  protected
    {: @abstract Implementuje zwracanie prefiksu --- właściwość @link(Prefix).

       Metoda @name zwraca bieżący prefiks. Klasy potomne mogą ją przedefiniować
       tak, aby zwracać nietypowy prefiks.

       Przykład: @longCode(#
       type
         TGSkLogEx = class(TGSkLog)
         strict protected
            function GetPrefix() : string; override;
         end ;

       var
         sPrefiksModulu:  string;

       function  TGSkLogEx.GetPrefix() : string;
       begin
         Result := sPrefixModulu
       end; #)

       @seeAlso(Prefix)  }
    function  GetPrefix() : string;                                  virtual;

    {: @abstract Implementuje pobieranie wartości właściwości @link(Offset).

       To jest metoda wirtualna. Klasy potomne mogą ją redefiniować.

       seeAlso(SetOffset)
       seeAlso(Offset)  }
    function  GetOffset() : Cardinal;                                virtual;

    {: @abstract Implementuje pobieranie wartości właściwości @link(Startup).

       To jest metoda wirtualna. Klasy potomne mogą ją redefiniować. }
    function  GetStartup() : Boolean;                                virtual;

    {: @abstract Implementuje nadawanie wartości właściwości @link(Offset).

       To jest metoda wirtualna. Klasy potomne mogą ją redefiniować.

       seeAlso(GetOffset)
       seeAlso(Offset)  }
    procedure  SetOffset(const   nOffset:  Cardinal);                virtual;

    {: @abstract Implementuje nadawanie wartości właściwości @link(Startup).

       To jest metoda wirtualna. Klasy potomne mogą ją redefiniować. }
    procedure  SetStartup(const   bValue:  Boolean);                 virtual;

    {: @abstract Metoda wywoływana przed zarejestrowaniem informacji.

       Metoda @name jest wywoływana przed zarejestrowaniem każdej informacji.
       Standardowo służy do wygenerowania zdarzenia @link(BeforeMessage).

       @seeAlso(DoAfterMessage)  }
    procedure  DoBeforeMessage(const   Msg:  TGSkLogMessage);        virtual;

    {: @abstract Metoda wywoływana po zarejestrowaniu informacji.

       Metoda @name jest wywoływana po zarejestrowaniu każdej informacji.
       Standardowo służy do wygenerowania zdarzenia @link(AfterMessage).

       @seeAlso(DoBeforeMessage)  }
    procedure  DoAfterMessage(const   Msg:  TGSkLogMessage);         virtual;

    {: @abstract Metoda pozwala dodać obiekt wiadomości do listy wiadomości.

       Metodę @name mogą wykorzystać obiekty potomne w celu dodania obiektu
       reprezentującego monitorowaną wiadomość do wewnętrznej kolejki wiadomości.  }
    procedure  AddMessage(const   Msg:  TGSkLogMessage);             virtual;

    {: @exclude
       Nie ma czego dokumentować - to tylko implementacja standardowej metody. }
    procedure  Notification(AComponent:  TComponent;
                            Operation:  TOperation);                 override;

    {: @abstract Informuje, czy monitorowanie już się rozpoczęło.

       Przed zarejestrowaniem pierwszej informacji automatycznie rejestrowany
       jest nagłówek (zawierający nazwę i wersję monitorowanej aplikacji, a także
       @link(AddHeaderLine nagłówek) (jeżeli jest zdefiniowany).

       Właściwość @name informuje, czy opisany wyżej nagłówek został już
       zarejestrowany. }
    property  StartUp:  Boolean
              read  GetStartup
              write SetStartup;

  public
    {: @abstract Standardowy konstruktor.

       Po utworzeniu obiektu należy w nim zarejestrować, przy pomocy
       @link(AddLogAdapter), przynajmniej jeden adapter.  }
    constructor  Create(AOwner:  TComponent);                        override;

    {: @abstract Rozszerzony konstruktor.

       Po utworzeniu obiektu automatycznie tworzy i rejestruje w nim listę
       adapterów o klasach wskazanych w tablicy @code(Adapters).

       Jeżeli dany adapter ma kilka konstruktorów, do jego utworzenia zostanie
       użyty konstruktor standardowy @code(Create(AOwner)).

       @seeAlso(AddLogAdapter)  }
    constructor  CreateLog(const   AOwner:  TComponent;
                           const   Adapters:  array of TGSkCustomLogAdapterClass);

    {: @exclude }
    destructor  Destroy();                                           override;

    {: @exclude
       Nie ma czego dokumentować - to tylko implementacja standardowej metody. }
    procedure  Assign(Source:  TPersistent);                         override;

    {: @abstract Przekształca datę i/lub czas na napis.

       Funkcja pomija pustą datę lub pusty czas.  }
    class function  DateTimeToString(const   dtValue:  TDateTime)
                                     : string;

    {: @abstract Przekształca dowolny obiekt na napis.

       Funkcja zwraca klasę obiektu. Jeżeli obiekt ma właściwość @code(Name),
       to również jest ona częścią wynikowego napisu. }
    class function  ObjectToString(const   Value:  TObject)
                                   : string;

    {: @abstract Przekształca dowolny interfejs na napis.

       Funkcja zwraca albo napis @code('*nil'), albo @code('[.interface.]). }
    class function  InterfaceToString(const   iValue:  IInterface)
                                      : string;

    {: @abstract Przekształca wartość logiczną na napis.

       Funkcja zwraca wynik używając standardową funkcję @code(BoolToStr). }
    class function  BooleanToString(const   bValue:  Boolean)
                                    : string;                        inline;

    {: @abstract Przekształca wartość typu @code(Variant) na napis.

       @param(vValue Przekształcana wartość.)
       @param(bChkCtrlChars Ten parametr ma znaczenie tylko wtedy, gdy przekształcana
                            wartość jest napisem. Jeżeli parametr ma wartość @true,
                            to napis ujmowany jest w cudzysłowy oraz znaki kontrolne
                            są zamieniane na odpowiednie kody.) }
    class function  VariantToString(const   vValue:  Variant;
                                    const   bChkCtrlChars:  Boolean = True)
                                    : string;

    {: @abstract Rejestruje adapter.

       Jeżeli właścicielem adaptera jest bieżący obiekt, to adapter zostanie do
       niego automatycznie dodany. Tak samo się dzieje, gdy właścicielem bieżącego
       obiektu oraz adaptera jest ten sam obiekt nadrzędny (np. okno lub moduł
       danych).

       Przykład: @longCode(#
         var
           gLog:  TGSkLog;
         begin
           gLog := TGSkLog.Create(nil);
           TGSkLogFile.Create(gLog); #)

       Powyższe instrukcje mają taki sam efekt, jak następujące instrukcje
       @longCode(#
         var
           gLog:  TGSkLog;
           gLogFile:  TGSkLogFile;
         begin
           gLog := TGSkLog.Create(nil);
           gLogFile := TGSkLogFile.Create(nil);
           gLog.AddLogAdapter(gLogFile); #)

       W pierwszym powyższym przykładzie wystarczy zwolnić obiekt @code(gLog),
       a dodany do niego adapter zostanie również automatycznie zwolniony, ponieważ
       obiekt @code(gLog) jest jego właścicielem. Natomiast w drugim przypadku
       każdy z obiektów trzeba oddzielnie zwalniać.

       Ten sam adapter można wskazać wielokrotnie. Będzie zarejestrowany tylko raz.

       @return(Indeks zarejestrowanego adaptera)

       @seeAlso(AdapterCount)
       @seeAlso(Adapters)  }
    function  AddLogAdapter(const   LogAdapter:  TGSkCustomLogAdapter)
                            : Integer;

    {: @abstract Odrejestrowuje adapter.

       @param(LogAdapter Odrejestrowywany adapter.)

       @seeAlso(AdapterCount)
       @seeAlso(Adapters)  }
    procedure  RemoveLogAdapter(const   LogAdapter:  TGSkCustomLogAdapter);  overload;

    {: @abstract Odrejestrowuje adapter.

       @param(LogAdapter Numer odrejestrowywanego adaptera.)

       @seeAlso(AdapterCount)
       @seeAlso(Adapters)  }
    procedure  RemoveLogAdapter(const   nAdapter:  Integer);                 overload;

    {: @abstract Rozpoczyna monitorowanie bloku informacji.

       Po wykonaniu tej metody wszystkie monitorowane informacje są buforowane.
       Aby je zarejestrować należy wywołać metodę @link(EndLog).

       Metody @name oraz @link(EndLog) muszą być zawsze wywoływane do pary.

       Zamiast metody @link(EndLog) można wywołać metodę @link(AbandonLog).
       Spowoduje ona wycofanie wszystkich informacji przekazanych do zarejestrowania
       po ostatnim wywołaniu metody @name.

       Analogia do baz danych: metoda @name jest odpowiednikiem polecenia
       START TRANSACTION.

       @bold(Uwaga!) @br
       Po wywołaniu metody @name wszystkie monitorowane informacje przechowywane
       są w pamięci operacyjnej tak długo, aż zostanie wywołana metoda @link(EndLog).
       Jeżeli w międzyczasie nastąpi awaria aplikacji, informacje te przepadną.

       @seeAlso(EndLog)
       @seeAlso(AbandonLog)
       @seeAlso(Adapters)  }
    function  BeginLog() : TGSkLog;

    {: @abstract Kończy monitorowanie bloku informacji.

       Po wykonaniu metody @link(BeginLog) wszystkie monitorowane informacje są
       buforowane. Aby je zarejestrować należy wywołać metodę @name.

       Metody @link(BeginLog) oraz @name muszą być zawsze wywoływane do pary.

       Zamiast metody @link(EndLog) można wywołać metodę @link(AbandonLog).
       Spowoduje ona wycofanie wszystkich informacji przekazanych do zarejestrowania
       po ostatnim wywołaniu metody @link(BeginLog).

       Analogia do baz danych: metoda @name jest odpowiednikiem polecenia
       COMMIT TRANSACTION.

       @seeAlso(AbandonLog)  }
    function  EndLog() : TGSkLog;

    {: @abstract Wycofuje informacje przesłane do zarejestrowania.

       Metoda @name wycofuje wszystkie informacje przesłane do zarejestrowania
       po ostatnim wywołaniu metody @link(BeginLog).

       Dla każdego wywołanie metody @link(BeginLog) musi być do pary wywołanie
       albo metody @link(EndLog), albo @name.

       Analogia do baz danych: metoda @name jest odpowiednikiem polecenia
       ROLLBACK TRANSACTION.

       @seeAlso(BeginLog)
       @seeAlso(EndLog)  }
    function  AbandonLog() : TGSkLog;

    {: @abstract Zwiększa poziom zagnieżdżenia monitorowanych informacji.
       @seeAlso(Offset)  }
    function  BeginLevel() : TGSkLog;

    {: @abstract Zmniejsza poziom zagnieżdżenia monitorowanych informacji.
       @seeAlso(Offset)  }
    function  EndLevel() : TGSkLog;

    {: @abstract Czyści (usuwa) monitorowane informacje.

       @link(TGSkCustomLogAdapter.Clear Implementacja) tej metody zależy od
       poszczególnych adapterów. Niektóre adaptery ignorują tę metodę. Opisu jej
       implementacji w poszczególnych adapterach należy szukać w opisie metody
       @link(TGSkCustomLogAdapter.Clear Clear) poszczególnych adapterów.  }
    function  Clear() : TGSkLog;

    {: @abstract Rozpoczyna monitorowanie bloku kodu.

       @param sMsg      Informacja wstawiana na początku monitorowanego kodu.
       @param lmMsgType Rodzaj monitorowanej informacji.

       Metoda rejestruje napis jednocześnie zwiększając poziom zagnieżdżenia
       rejestrowanych informacji. Na końcu bloku monitorowanego kodu powinna być
       wywołana dowolna metoda @link(LogExit).

       Metody @name oraz @link(LogExit) zazwyczaj używa się odpowiednio na
       początku i końcu monitorowanego podprogramu.

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogExit('Obliczenia')
         end ;
       #)

       @seeAlso(Offset)  }
    function  LogEnter(const   sMsg:  string;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rozpoczyna monitorowanie bloku kodu.

       @param(sMsgFmt Format monitorowanych informacji. Jest to taki sam napis
                      jak w standardowej funkcji @code(Format). )
       @param(MsgArgs Argumenty podlegające formatowaniu --- podobnie jak w
                      standardowej funkcji @code(Format). )
       @param(lmMsgType Rodzaj monitorowanej informacji. )

       Metoda rejestruje napis jednocześnie zwiększając poziom zagnieżdżenia
       rejestrowanych informacji. Na końcu bloku monitorowanego kodu powinna być
       wywołana dowolna metoda @link(LogExit).

       Metody @name oraz @link(LogExit) zazwyczaj używa się odpowiednio na
       początku i końcu monitorowanego podprogramu.

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogExit('Obliczenia')
         end ;
       #)

       @seeAlso(Offset)  }
    function  LogEnter(const   sMsgFmt:  string;
                       const   MsgArgs:  array of const;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Kończy monitorowanie bloku kodu.

       @param sMsg Informacja wstawiana na końcu monitorowanego kodu.
       @param lmMsgType Rodzaj monitorowanej informacji.

       Metoda rejestruje napis jednocześnie zmniejszając poziom zagnieżdżenia
       rejestrowanych informacji. Na początku bloku monitorowanego kodu powinna
       być wywołana dowolna metoda @link(LogEnter).

       Metody @link(LogEnter) oraz @name zazwyczaj używa się odpowiednio na
       początku i końcu monitorowanego podprogramu.

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogExit('Obliczenia')
         end ;
       #)

       @seeAlso(Offset)  }
    function  LogExit(const   sMsg:  string;
                      const   lmMsgType:  TGSkLogMessageType = lmInformation)
                      : TGSkLog;                                     overload;

    {: @abstract Kończy monitorowanie bloku kodu.

       @param(sMsgFmt Format monitorowanych informacji. Jest to taki sam napis
                      jak w standardowej funkcji @code(Format). )
       @param(MsgArgs Argumenty podlegające formatowaniu --- podobnie jak w
                      standardowej funkcji @code(Format). )
       @param(lmMsgType Typ monitorowanej informacji. )

       Metoda rejestruje napis jednocześnie zmniejszając poziom zagnieżdżenia
       rejestrowanych informacji. Na początku bloku monitorowanego kodu powinna
       być wywołana dowolna metoda @link(LogEnter).

       Metody @link(LogEnter) oraz @name zazwyczaj używa się odpowiednio na
       początku i końcu monitorowanego podprogramu.

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogExit('Obliczenia')
         end ;
       #)

       @seeAlso(Offset)  }
    function  LogExit(const   sMsgFmt:  string;
                      const   MsgArgs:  array of const;
                      const   lmMsgType:  TGSkLogMessageType = lmInformation)
                      : TGSkLog;                                     overload;

    {: @abstract Rejestruje monitorowaną informację.

       @param sMsg      Monitorowana informacja.
       @param lmMsgType Typ informacji.  }
    function  LogMessage(const   sMsg:  string;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Rejestruje monitorowaną informację.

       @param(sMsgFmt Format informacji. Jest to napis taki sam jak w standardowej
                      funkcji @code(Format). )
       @param(MsgArgs Formatowane informacje --- analogicznie jak w standardowej
                      funkcji @code(Format). )
       @param(lmMsgType Typ informacji. )  }
    function  LogMessage(const   sMsgFmt:  string;
                         const   MsgArgs:  array of const;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Rejestruje informację wielowierszową.

       @param(sMemo Treść rejestrowanej informacji; może się składać z dowolnie
                    wielu wierszy tekstu. )
       @param(lmMsgType Typ informacji. )  }
    function  LogMemo(const   sMemo:  string;
                      const   lmMsgType:  TGSkLogMessageType = lmInformation)
                      : TGSkLog;                                     overload;

    {: @abstract Rejestruje informację wielowierszową.

       @param(sName Nazwa (opis) rejestrowanej informacji.)
       @param(sMemo Treść rejestrowanej informacji; może się składać z dowolnie
                    wielu wierszy tekstu. )
       @param(lmMsgType Typ informacji. )  }
    function  LogMemo(const   sName, sMemo:  string;
                      const   lmMsgType:  TGSkLogMessageType = lmInformation)
                      : TGSkLog;                                     overload;

    {: @abstract Rejestruje wyjątek.

       @param(eError Wyjątek wygenerowany przez aplikację.)
       @param(sInfo Opcjonalnie --- okoliczności wystąpienia wyjątki.)
       @param(lmMsgType Klasa błędu. Zazwyczaj @code(lmError) lub @code(lmWarning).) }
    function  LogException(const   eError:  Exception;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;                                overload;

    {: @abstract Rejestruje wyjątek.

       @param(eError Wyjątek wygenerowany przez aplikację.)
       @param(sInfo Opcjonalnie --- okoliczności wystąpienia wyjątki.)
       @param(lmMsgType Opcjonalnie --- klasa błędu. Zazwyczaj @code(lmError)
                        lub @code(lmWarning).) }
    function  LogException(const   eError:  Exception;
                           const   sInfo:  string = '';
                           const   lmMsgType:  TGSkLogMessageType = lmError)
                           : TGSkLog;                                overload;

    {: @abstract Rejestruje wyjątek.

       Parametry @code(sInfoFmt) i @code(InfoArgs) pozwalają sformatować dodatkową
       informację o okolicznościach wystąpienia wyjątku.  }
    function  LogException(const   eError:  Exception;
                           const   sInfoFmt:  string;
                           const   InfoArgs:  array of const;
                           const   lmMsgType:  TGSkLogMessageType = lmError)
                           : TGSkLog;                                overload;

    {: @abstract Rejestruje wskazaną liczbę (domyślnie: 1) pustych wierszy.  }
    function  LogLines(const   nLines:  Integer = 1;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestruje linię oddzielającą.

       @param cSepChar  Z jakich znaków składa się linia oddzielająca.
       @param nSepLen   Z ilu znaków składa się linia oddzielająca.
       @param lmMsgType Typ informacji.  }
    function  LogSeparator(const   cSepChar:  Char = gccSeparatorChar;
                           const   nSepLen:  Integer  = gcnSeparatorLen;
                           const   lmMsgType:  TGSkLogMessageType = lmInformation)
                           : TGSkLog;                                overload;

    {: @abstract Rejestruje wskazana wartość.

       Zazwyczaj metoda jest wykorzystywana do rejestrowania wartości zmiennej.
       Monitorowane informacje mają postać @code(Nazwa = Wartość).

       @param sName Nazwa wartości (zmiennej).
       @param vValue Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   vValue:  Variant;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestruje wskazaną wartość całkowitoliczbową.

       Ta wersja metody przeznaczona jest do rejestrowania liczb całkowitych.
       Można je wyświetlać w w formacie dziesiętnym, szesnastkowym lub obu.

       @param sName     Nazwa wartości (zmiennej).
       @param nValue    Monitorowana wartość.
       @param ValType   Sposób formatowania liczby.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   nValue:  Int64;
                       const   ValType:  TGSkLogValueNumType = vtDec;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestruje wskazaną wartość typu @code(Currency).

       Ta wersja metody przeznaczona jest do rejestrowania wartości typu
       @code(Currency). Domyślnie wartości tego typu są interpretowane jako kwoty.

       @param sName     Nazwa wartości (zmiennej).
       @param nValue    Monitorowana wartość.
       @param ValType   Sposób formatowania liczby.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   nValue:  Currency;
                       const   ValType:  TGSkLogValueCurrType = ctMoney;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestrowanie wskazany obszar pamięci.

       Dla kolejnych bajtów zapisywany jest ich kod oraz odpowiadający mu znak.

       @param sName     Nazwa identyfikująca obszar.
       @param Memory    Adres początku obszaru.
       @param nMemSize  Wielkość obszaru w bajtach.
       @param lmMsgType Typ informacji.

       Przykład użycia: @longcode(#
         BlockRead(Plik, Bufor, SizeOf(Bufor), Odczytane);
         LogValue('Przeczytany bufor', Bufor, Odczytane); #)  }
    function  LogValue(const   sName:  string;
                       const   Memory;
                       const   nMemSize:  Cardinal;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestruje wskazaną wartość typu @code(TStrings) lub pochodnego.

       Ta wersja metody przeznaczona jest do rejestrowania obiektów typu @code(TStrings)
       lub pochodnego (np. @code(TStringList)).

       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   Value:  TStrings;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload; inline;

    {: @abstract Rejestruje wskazaną wartość typu @code(TStrings) lub pochodnego.

       Ta wersja metody przeznaczona jest do rejestrowania obiektów typu @code(TStrings)
       lub pochodnego (np. @code(TStringList)).

       @param sName         Nazwa wartości (zmiennej).
       @param Value         Monitorowana wartość.
       @param lmMsgType     Typ informacji.
       @param bLineNumbers  Print line numbers.  }
    function  LogValue(const   sName:  string;
                       const   Value:  TStrings;
                       const   bLineNumbers:  Boolean;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestruje wskazaną wartość typu @code(TStrings) lub pochodnego.

       Ta wersja metody przeznaczona jest do rejestrowania obiektów typu @code(TStrings)
       lub pochodnego (np. @code(TStringList)).

       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param nFromIdx  Indeks pierwszej monitorowanej wartości
       @param nToIdx    Indeks ostatniej monitorowanej wartości
       @param bLineNumbers  Print line numbers.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   Value:  TStrings;
                       const   nFromIdx:  Integer;
                       const   nToIdx:  Integer = MaxInt;
                       const   bLineNumbers:  Boolean = True;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestruje wskazany obiekt.

       Ta wersja metody przeznaczona jest do rejestrowania obiektów dowolnego
       typu. Rejestrowana jest klasa i nazwa obiektu (jeżeli obiekt ma właściwość
       @code(Name)).

       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowany obiekt.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   Value:  TObject;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload; inline;


    {: @abstract Rejestruje wskazany komponent.

       Ta wersja metody przeznaczona jest do rejestrowania komponentów.

       @param(sName     Nazwa wartości (zmiennej).)
       @param(Value     Monitorowany komponent.)
       @param(bDetailed Jeżeli ten parametr ma wartość @false, to rejestrowana
                        jest ścieżka i klasa komponentu, wyznaczona przy pomocy
                        @link(ComponentDisplayName). Natomiast jeżeli ten parametr
                        ma wartość @true, to rejestrowane są szczegółowe informacje
                        o komponencie, wyznaczone przy pomocy @link(ComponentToStr).)
       @param(lmMsgType Typ informacji.)  }
    function  LogValue(const   sName:  string;
                       const   Value:  TComponent;
                       const   bDetailed:  Boolean = False;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestruje wskazany interfejs.

       Ta wersja metody przeznaczona jest do rejestrowania dowolnych interfejsów.

       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   iValue:  IInterface;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Rejestruje wskazany identyfikator GUID.

       Ta wersja metody przeznaczona jest do rejestrowania dowolnych
       identyfikatorów GUID.

       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValue(const   sName:  string;
                       const   guidValue:  TGUID;
                       const   lmMsgType:  TGSkLogMessageType = lmInformation)
                       : TGSkLog;                                    overload;

    {: @abstract Warunkowo rejestruje wskazana wartość.

       Zazwyczaj metoda jest wykorzystywana do rejestrowania wartości zmiennej.
       Monitorowane informacje mają postać @code(Nazwa = Wartość).

       @param(bCond Wartość zostanie zarejestrowana pod warunkiem, że tem parametr
                    ma wartość @true.)
       @param sName Nazwa wartości (zmiennej).
       @param vValue Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValueIf(const   bCond:  Boolean;
                         const   sName:  string;
                         const   vValue:  Variant;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Warunkowo rejestruje wskazaną wartość całkowitoliczbową.

       Ta wersja metody przeznaczona jest do rejestrowania liczb całkowitych.
       Można je wyświetlać w w formacie dziesiętnym, szesnastkowym lub obu.

       @param(bCond Wartość zostanie zarejestrowana pod warunkiem, że tem parametr
                    ma wartość @true.)
       @param sName     Nazwa wartości (zmiennej).
       @param nValue    Monitorowana wartość.
       @param ValType   Sposób formatowania liczby.
       @param lmMsgType Typ informacji.  }
    function  LogValueIf(const   bCond:  Boolean;
                         const   sName:  string;
                         const   nValue:  Int64;
                         const   ValType:  TGSkLogValueNumType = vtDec;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Warunkowo rejestruje wskazaną wartość typu @code(Currency).

       Ta wersja metody przeznaczona jest do rejestrowania wartości typu
       @code(Currency). Domyślnie wartości tego typu są interpretowane jako kwoty.

       @param(bCond Wartość zostanie zarejestrowana pod warunkiem, że tem parametr
                    ma wartość @true.)
       @param sName     Nazwa wartości (zmiennej).
       @param nValue    Monitorowana wartość.
       @param ValType   Sposób formatowania liczby.
       @param lmMsgType Typ informacji.  }
    function  LogValueIf(const   bCond:  Boolean;
                         const   sName:  string;
                         const   nValue:  Currency;
                         const   ValType:  TGSkLogValueCurrType = ctMoney;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Warunkowo rejestrowanie wskazany obszar pamięci.

       Dla kolejnych bajtów zapisywany jest ich kod oraz odpowiadający mu znak.

       @param(bCond Wartość zostanie zarejestrowana pod warunkiem, że tem parametr
                    ma wartość @true.)
       @param sName     Nazwa identyfikująca obszar.
       @param Memory    Adres początku obszaru.
       @param nMemSize  Wielkość obszaru w bajtach.
       @param lmMsgType Typ informacji.

       Przykład użycia: @longcode(#
         BlockRead(Plik, Bufor, SizeOf(Bufor), Odczytane);
         LogValue('Przeczytany bufor', Bufor, Odczytane); #)  }
    function  LogValueIf(const   bCond:  Boolean;
                         const   sName:  string;
                         const   Memory;
                         const   nMemSize:  Cardinal;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Warunkowo rejestruje wskazaną wartość typu @code(TStrings) lub pochodnego.

       Ta wersja metody przeznaczona jest do rejestrowania obiektów typu @code(TStrings)
       lub pochodnego (np. @code(TStringList)).

       @param(bCond Wartość zostanie zarejestrowana pod warunkiem, że tem parametr
                    ma wartość @true.)
       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValueIf(const   bCond:  Boolean;
                         const   sName:  string;
                         const   Value:  TStrings;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Warunkowo rejestruje wskazaną wartość typu @code(TStrings) lub pochodnego.

       Ta wersja metody przeznaczona jest do rejestrowania obiektów typu @code(TStrings)
       lub pochodnego (np. @code(TStringList)).

       @param(bCond Wartość zostanie zarejestrowana pod warunkiem, że tem parametr
                    ma wartość @true.)
       @param sName         Nazwa wartości (zmiennej).
       @param Value         Monitorowana wartość.
       @param lmMsgType     Typ informacji.
       @param bLineNumbers  Print line numbers.  }
    function  LogValueIf(const   bCond:  Boolean;
                         const   sName:  string;
                         const   Value:  TStrings;
                         const   bLineNumbers:  Boolean;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Warunkowo rejestruje wskazaną wartość typu @code(TStrings) lub pochodnego.

       Ta wersja metody przeznaczona jest do rejestrowania obiektów typu @code(TStrings)
       lub pochodnego (np. @code(TStringList)).

       @param(bCond Wartość zostanie zarejestrowana pod warunkiem, że tem parametr
                    ma wartość @true.)
       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param nFromIdx  Indeks pierwszej monitorowanej wartości
       @param nToIdx    Indeks ostatniej monitorowanej wartości
       @param bLineNumbers  Print line numbers.
       @param lmMsgType Typ informacji.  }
    function  LogValueIf(const   bCond:  Boolean;
                         const   sName:  string;
                         const   Value:  TStrings;
                         const   nFromIdx:  Integer;
                         const   nToIdx:  Integer = MaxInt;
                         const   bLineNumbers:  Boolean = True;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Warunkowo rejestruje wskazany obiekt.

       Ta wersja metody przeznaczona jest do rejestrowania obiektów dowolnego
       typu. Rejestrowana jest klasa i nazwa obiektu (jeżeli obiekt ma właściwość
       @code(Name)).

       @param(bCond Wartość zostanie zarejestrowana pod warunkiem, że tem parametr
                    ma wartość @true.)
       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowany obiekt.
       @param lmMsgType Typ informacji.  }
    function  LogValueIf(const   bCond:  Boolean;
                         const   sName:  string;
                         const   Value:  TObject;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Warunkowo rejestruje wskazany komponent.

       Ta wersja metody przeznaczona jest do rejestrowania komponentów.

       @param(bCond Wartość zostanie zarejestrowana pod warunkiem, że tem parametr
                    ma wartość @true.)
       @param(sName     Nazwa wartości (zmiennej).)
       @param(Value     Monitorowany komponent.)
       @param(bDetailed Jeżeli ten parametr ma wartość @false, to rejestrowana
                        jest ścieżka i klasa komponentu, wyznaczona przy pomocy
                        @link(ComponentDisplayName). Natomiast jeżeli ten parametr
                        ma wartość @true, to rejestrowane są szczegółowe informacje
                        o komponencie, wyznaczone przy pomocy @link(ComponentToStr).)
       @param(lmMsgType Typ informacji.)  }
    function  LogValueIf(const   bCond:  Boolean;
                         const   sName:  string;
                         const   Value:  TComponent;
                         const   bDetailed:  Boolean = False;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Warunkowo rejestruje wskazany interfejs.

       Ta wersja metody przeznaczona jest do rejestrowania dowolnych interfejsów.

       @param(bCond Wartość zostanie zarejestrowana pod warunkiem, że tem parametr
                    ma wartość @true.)
       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValueIf(const   bCond:  Boolean;
                         const   sName:  string;
                         const   iValue:  IInterface;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Warunkowo rejestruje wskazany identyfikator GUID.

       Ta wersja metody przeznaczona jest do rejestrowania dowolnych
       identyfikatorów GUID.

       @param(bCond Wartość zostanie zarejestrowana pod warunkiem, że tem parametr
                    ma wartość @true.)
       @param sName     Nazwa wartości (zmiennej).
       @param Value     Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogValueIf(const   bCond:  Boolean;
                         const   sName:  string;
                         const   guidValue:  TGUID;
                         const   lmMsgType:  TGSkLogMessageType = lmInformation)
                         : TGSkLog;                                  overload;

    {: @abstract Rejestruje wynik funkcji.

       Metoda jest wykorzystywana do rejestrowania wyniku funkcji. Jest ona
       równoważna wywołaniu metody @link(LogValue), na przykład @longCode(#
         function  Oblicz(const nPar:  Integer)
                          : Integer;
         begin
           LogEnter('Oblicz(%d)', [nPar]);
           ...
           LogResult(Result);
           LogExit('Oblicz')
         end;
       #)

       @param vValue Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogResult(const   vValue:  Variant;
                        const   lmMsgType:  TGSkLogMessageType = lmInformation)
                        : TGSkLog;                                   overload; inline;

    {: @abstract Rejestruje wynik funkcji.

       Metoda jest wykorzystywana do rejestrowania wyniku funkcji. Jest ona
       równoważna wywołaniu metody @link(LogValue), na przykład @longCode(#
         function  Oblicz(const nPar:  Int64)
                          : Int64;
         begin
           LogEnter('Oblicz(%d)', [nPar]);
           ...
           LogResult(Result);
           LogExit('Oblicz')
         end;
       #)

       @param nValue    Monitorowana wartość.
       @param ValType   Sposób formatowania liczby.
       @param lmMsgType Typ informacji.  }
    function  LogResult(const   nValue:  Int64;
                        const   ValType:  TGSkLogValueNumType = vtDec;
                        const   lmMsgType:  TGSkLogMessageType = lmInformation)
                        : TGSkLog;                                   overload; inline;


    {: @abstract Rejestruje wynik funkcji.

       Metoda jest wykorzystywana do rejestrowania wyniku funkcji. Jest ona
       równoważna wywołaniu metody @link(LogValue), na przykład @longCode(#
         function  Oblicz(const nPar:  Int64)
                          : Int64;
         begin
           LogEnter('Oblicz(%d)', [nPar]);
           ...
           LogResult(Result);
           LogExit('Oblicz')
         end;
       #)

       @param nValue    Monitorowana wartość.
       @param ValType   Sposób formatowania liczby.
       @param lmMsgType Typ informacji.  }
    function  LogResult(const   nValue:  Currency;
                        const   ValType:  TGSkLogValueCurrType = ctMoney;
                        const   lmMsgType:  TGSkLogMessageType = lmInformation)
                        : TGSkLog;                                   overload; inline;

    {: @abstract Rejestruje wynik funkcji.

       Metoda jest wykorzystywana do rejestrowania wyniku funkcji. Jest ona
       równoważna wywołaniu metody @link(LogValue), na przykład @longCode(#
         function  Parameters(const nPar:  Integer)
                             : TStringList;
         begin
           LogEnter('Parameters(%d)', [nPar]);
           ...
           LogResult(Result);
           LogExit('Parameters')
         end;
       #)

       @param Value Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogResult(const   Value:  TStrings;
                        const   bLineNumbers:  Boolean = True;
                        const   lmMsgType:  TGSkLogMessageType = lmInformation)
                        : TGSkLog;                                   overload; inline;

    {: @abstract Rejestruje wynik funkcji.

       Metoda jest wykorzystywana do rejestrowania wyniku funkcji. Jest ona
       równoważna wywołaniu metody @link(LogValue), na przykład @longCode(#
         function  Parameters(const nPar:  Integer)
                             : TStringList;
         begin
           LogEnter('Parameters(%d)', [nPar]);
           ...
           LogResult(Result);
           LogExit('Parameters')
         end;
       #)

       @param Value Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogResult(const   Value:  TStrings;
                        const   lmMsgType:  TGSkLogMessageType)
                        : TGSkLog;                                   overload; inline;

    {: @abstract Rejestruje wynik funkcji.

       Metoda jest wykorzystywana do rejestrowania wyniku funkcji. Jest ona
       równoważna wywołaniu metody @link(LogValue), na przykład @longCode(#
         function  CreateIt(const nPar:  Integer)
                            : TAnyObject;
         begin
           LogEnter('CreateIt(%d)', [nPar]);
           ...
           LogResult(Result);
           LogExit('CreateIt')
         end;
       #)

       @param Value Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogResult(const   Value:  TObject;
                        const   lmMsgType:  TGSkLogMessageType = lmInformation)
                        : TGSkLog;                                   overload; inline;

    {: @abstract Rejestruje wynik funkcji.

       Metoda jest wykorzystywana do rejestrowania wyniku funkcji. Jest ona
       równoważna wywołaniu metody @link(LogValue), na przykład @longCode(#
         function  CreateIt(const nPar:  Integer)
                            : TAnyComponent;
         begin
           LogEnter('CreateIt(%d)', [nPar]);
           ...
           LogResult(Result);
           LogExit('CreateIt')
         end;
       #)

       @param Value Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogResult(const   Value:  TComponent;
                        const   bDetailed:  Boolean = False;
                        const   lmMsgType:  TGSkLogMessageType = lmInformation)
                        : TGSkLog;                                   overload; inline;

    {: @abstract Rejestruje wynik funkcji.

       Metoda jest wykorzystywana do rejestrowania wyniku funkcji. Jest ona
       równoważna wywołaniu metody @link(LogValue), na przykład @longCode(#
         function  Select(const nPar:  Integer)
                          : IAnyInterface;
         begin
           LogEnter('Select(%d)', [nPar]);
           ...
           LogResult(Result);
           LogExit('Select')
         end;
       #)

       @param Value Monitorowana wartość.
       @param lmMsgType Typ informacji.  }
    function  LogResult(const   iValue:  IInterface;
                        const   lmMsgType:  TGSkLogMessageType = lmInformation)
                        : TGSkLog;                                   overload; inline;

    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(vValue, lmMsgType));)
           @item(@code(LogExit(sMsg, lmMsgType));))

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   vValue:  Variant;
                            const   sMsg:  string;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;

    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(nValue, lmMsgType));)
           @item(@code(LogExit(sMsg, lmMsgType));))

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   nValue:  Int64;
                            const   sMsg:  string;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;


    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(nValue, lmMsgType));)
           @item(@code(LogExit(sMsg, lmMsgType));))

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   nValue:  Currency;
                            const   sMsg:  string;
                            const   ValType:  TGSkLogValueCurrType = ctMoney;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;


    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(Value, lmMsgType));)
           @item(@code(LogExit(sMsg, lmMsgType));))

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   Value:  TStrings;
                            const   sMsg:  string;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;


    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(Value, lmMsgType));)
           @item(@code(LogExit(sMsg, lmMsgType));))

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   Value:  TObject;
                            const   sMsg:  string;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;


    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(Value, bDetailed, lmMsgType));)
           @item(@code(LogExit(sMsg, lmMsgType));))

       Parametr @code(bDetailed) informuje, czy mają być zarejestrowane szczegóły
       komponentu.

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   Value:  TComponent;
                            const   sMsg:  string;
                            const   bDetailed:  Boolean = False;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;


    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(iValue, lmMsgType));)
           @item(@code(LogExit(sMsg, lmMsgType));))

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   iValue:  IInterface;
                            const   sMsg:  string;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;

    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(vValue, lmMsgType));)
           @item(@code(LogExit(sMsg, MsgArgs, lmMsgType));))

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   vValue:  Variant;
                            const   sMsgFmt:  string;
                            const   MsgArgs:  array of const;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;

    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(nValue, lmMsgType));)
           @item(@code(LogExit(sMsg, MsgArgs, lmMsgType));))

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   nValue:  Int64;
                            const   sMsgFmt:  string;
                            const   MsgArgs:  array of const;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;

    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(Value, lmMsgType));)
           @item(@code(LogExit(sMsg, MsgArgs, lmMsgType));))

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   Value:  TStrings;
                            const   sMsgFmt:  string;
                            const   MsgArgs:  array of const;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;

    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(Value, lmMsgType));)
           @item(@code(LogExit(sMsg, MsgArgs, lmMsgType));))

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   Value:  TObject;
                            const   sMsgFmt:  string;
                            const   MsgArgs:  array of const;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;

    {: @abstract Rejestruje opuszczenie funkcji.

       Metoda @name wykonuje kolejno następujące metody:
         @orderedList(@itemSpacing(Compact)
           @item(@code(LogResult(iValue, lmMsgType));)
           @item(@code(LogExit(sMsg, MsgArgs, lmMsgType));))

       Przykład: @longCode(#
         LogEnter('Obliczenia');
         try
           ...
         finally
           LogResultExit(Result, 'Obliczenia')
         end ;
       #)

       @seeAlso(LogResult)
       @seeAlso(LogExit)  }
    function  LogResultExit(const   iValue:  IInterface;
                            const   sMsgFmt:  string;
                            const   MsgArgs:  array of const;
                            const   lmMsgType:  TGSkLogMessageType = lmInformation)
                            : TGSkLog;                               overload;

    {: @abstract Dodaje informacje do nagłówka dziennika.

       Przed zarejestrowaniem pierwszej informacji do dziennika automatycznie
       dodawane są ścieżka do monitorowanej aplikacji oraz jej numer wersji.
       Metoda @name pozwala uzupełnić ten nagłówek o dodatkowy tekst.

       @bold(Uwaga!) Jeżeli metoda @name zostanie wywołana po zarejestrowaniu
       pierwszej informacji, to nie ma to żadnego efektu.  }
    procedure  AddHeaderLine(const   sLine:  string);                overload;

    {: @abstract Dodaje informacje do nagłówka dziennika.

       Przed zarejestrowaniem pierwszej informacji do dziennika automatycznie
       dodawane są ścieżka do monitorowanej aplikacji oraz jej numer wersji.
       Metoda @name pozwala uzupełnić ten nagłówek o dodatkowy tekst.

       @bold(Uwaga!) Jeżeli metoda @name zostanie wywołana po zarejestrowaniu
       pierwszej informacji, to nie ma to żadnego efektu.  }
    procedure  AddHeaderLine(const   sLineFmt:  string;
                             const   LineArgs:  array of const);     overload;

    { public properties }

    {: @abstract Liczba aktualnie zarejestrowanych modułów adapterów.

       @seeAlso(Adapters)
       @seeAlso(AddLogAdapter)  }
    property  AdapterCount:  Integer
              read  GetAdapterCount;

    {: @abstract Zarejestrowane adaptery.

       @seeAlso(AdapterCount)
       @seeAlso(AddLogAdapter)
       @seeAlso(RemoveLogAdapter)  }
    property  Adapters[const   nIndex:  Integer] : TGSkCustomLogAdapter
              read  GetAdapter;   //#Usprawnić: Napisać edytor tej właściwości i przenieść do *published*

    property  IsLog[const   lmLogLevel:  TGSkLogMessageType] : Boolean
              read  GetIsLog;
    property  IsLogWarning:  Boolean
              read  GetIsLogWarning;
    property  IsLogInformation:  Boolean
              read  GetIsLogInformation;
    property  IsLogDebug:  Boolean
              read  GetIsLogDebug;
    property  IsLogDebugDetails:  Boolean
              read  GetIsLogDebugDetails;
    property  IsLogDebugLowLevel:  Boolean
              read  GetIsLogDebugLowLevel;

  published

    {: @abstract Umożliwia aktywowanie i dezaktywowanie monitorowania.  }
    property  Active:  Boolean
              read  fbActive
              write fbActive
              default  True;

    {: @abstract Dodatkowy prefiks dodawany na początku rejestrowanych informacji.

       Dzięki prefiksowi można łatwo rozróżnić informacje rejestrowane przez różne
       wątki lub różne instancje tego samego programu.

       @name zazwyczaj jest pustym napisem. Nie powinien to być długi napis.
       Najczęściej będzie to krótkie określenie, na przykład nazwa wątku, nazwa
       użytkownika itp.  }
    property  Prefix:  string
              read  GetPrefix
              write fsPrefix;

    {: @abstract Poziom zagnieżdżenia monitorowanych informacji.

       Poziom zagnieżdżenia jest zwiększany przez metody @link(LogEnter) oraz
       @link(BeginLevel) i zmniejszany przez metody @link(LogExit) oraz
       @link(EndLevel).  }
    property  Offset:  Cardinal
              read  GetOffset
              write SetOffset
              default  0;

    {: @abstract Poziom buforowania rejestrowanych informacji.

       Każde wywołanie metody @link(BeginLog) zwiększa tę wartość, a każde
       wywołanie metody @link(EndLog) -- zmniejsza. Jeżeli po wywołaniu metody
       @link(EndLog) poziom zostanie zmniejszony do zera to buforowane informacje
       przekazywane są do poszczególnych adapterów.  }
    property  UpdateLevel:  Cardinal
              read  fnUpdateLevel;

    {: @abstract Steruje rejestrowaniem czasu monitorowanej informacji.

       Jeżeli ma wartość @false, to w miejscu przeznaczonym dla czasu zarejestrowania
       informacji są odstępy.  }
    property  LogMessageTime:  Boolean
              read  fbLogMessageTime
              write fbLogMessageTime
              default  True;

    {: @abstract Steruje rejestrowaniem identyfikatora wątku. }
    property  LogThreadID:  Boolean
              read  fbLogThreadID
              write fbLogThreadID
              default  True;

    {: @abstract Steruje rodzajem wyróżnianych informacji.

       Sposób wyróżniania zależy od poszczególnych adapterów. Niektóre adaptery
       mogą ignorować tę właściwość.  }
    property  ShowMessageTypes:  TGSkLogMessageTypes
              read  fsetShowMsgTypes
              write fsetShowMsgTypes
              default  gcsetShowMsgTypes;

    {: @abstract Zdarzenie generowane jest przed zarejestrowaniem informacji.

       Zdarzenie @name pozwala na zmodyfikowanie treści rejestrowanej informacji.
       Na przykład w informacji zawierającej kody sterujące można zamienić te
       kody na ich mnemoniki.

       @seeAlso(AfterMessage)  }
    property  BeforeMessage:  TGSkLogMessageEvent
              read  ffnBeforeMessage
              write ffnBeforeMessage
              default  nil;

    {: @abstract Zdarzenie generowane jest po zarejestrowaniu informacji.

       Zdarzenie @name jest generowane po zarejestrowaniu każdej informacji.

       @seeAlso(BeforeMessage)  }
    property  AfterMessage:  TGSkLogMessageEvent
              read  ffnAfterMessage
              write ffnAfterMessage
              default  nil;

    {: @abstract Nazwa dziennika.

       Jeżeli właściwość @name jest definiowana, to jej wartość jest
       przekazywana do wszystkich adapterów związanych z bieżącym obiektem
       @className -- poprzez właściwość @link(TGSkCustomLogAdapter.LogName).

       @seeAlso(TGSkCustomLogAdapter.LogName)
       @seeAlso(LogFullName)
       @seeAlso(Prefix)  }
    property  LogName:  string
              read  GetLogName
              write SetLogName;

    {: @abstract Pełna nazwa dziennika.

       Jeżeli właściwość @name jest definiowana, to jej wartość jest
       przekazywana do wszystkich adapterów związanych z bieżącym obiektem
       @className -- poprzez właściwość @link(TGSkCustomLogAdapter.LogFullName).

       @seeAlso(TGSkCustomLogAdapter.LogFullName)
       @seeAlso(LogName)
       @seeAlso(Prefix)  }
    property  LogFullName:  string
              read  GetLogFullName
              write SetLogFullName;

    {: @abstract Nazwa modułu.

       Nazwa modułu jest przekazywana do wszystkich
       @link(TGSkCustomLogAdapter adapterów logowania). Sposób jej wykorzystania
       zależy od danego adaptera. Na przykład nazwa może być drukowana w tytule
       pliku dziennika. }
    property  ModuleName:  string
              read  GetModuleName
              write SetModuleName;

    {: @abstract Filtruje monitorowane informacje.

       W zależności od wartości właściwości @name w dzienniku rejestrowane będą:
       @definitionList(@itemSpacing(Compact)
         @itemLabel(@code(lmInformation))
           @item(Monitorowane są wszystkie informacje.)
         @itemLabel(@code(lmWarning))
           @item(Monitorowane są tylko ostrzeżenia i błędy.)
         @itemLabel(@code(lmError))
           @item(Monitorowane są tylko błędy.)
       )

       @seeAlso(ShowMessageTypes)  }
    property  LogFilter:  TGSkLogMessageType
              read  fLogFilter
              write {$IFDEF DEBUG}SetLogFilter{$ELSE}fLogFilter{$ENDIF}
              default  lmInformation;

    {: @abstract Automatyczne rejestrowanie początku i końca sesji monitorowania.

       Jeżeli właściwość @name ma wartość @true, to automatycznie rejestrowana
       jest dodatkowa informacja o rozpoczęciu i zakończeniu sesji monitorowania
       aplikacji.

       @seeAlso gcsSessionStart
       @seeAlso gcsSessionStop  }
    property  AutoSession:  Boolean
              read  fbAutoSession
              write fbAutoSession
              default  True;

    {: @abstract Czy sprawdzać znaki sterujące w monitorowanych danych.

       Jeżeli właściwość @name ma wartość @true (wartość domyślna) to monitorowane
       teksty są sprawdzane pod kątem występowania w nich znaków sterujących.
       Znaki sterujące zostają zastąpione ich kodem dziesiętnym, poprzedzonym
       znakiem „#” (notacja języka Pascal). }
    property  CheckControlCharacters:  Boolean
              read  fbChkCtrlChars
              write fbChkCtrlChars
              default  True;

    {: @abstract Czy rejestrować błędy niezmienników.

       Jeżeli nie jest spełniony warunek wskazany w standardowej procedurze
       @code(Assert), to jest on rejestrowany przez każdy adapter, który ma
       ustawioną właściwość @name. }
    property  LogAssertions:  Boolean
              read  fbLogAssertions
              write fbLogAssertions
              default  True;
  end { TGSkLog };

  TGSkLogClass = class of TGSkLog;


{: @abstract Warunkowo zwraca poziom monitorowania.

   Jeżeli parametr @code(bCond) ma wartość @true, to wynikiem funkcji @name
   jest wartość parametru @code(lmIfTrue). W przeciwnym razie wynikiem funkcji
   jest wartość parametru @code(lmIfFalse). }
function  IfThen(const   bCond:  Boolean;
                 const   lmIfTrue:  TGSkLogMessageType;
                 const   lmIfFalse:  TGSkLogMessageType = lmInformation)
                 : TGSkLogMessageType;                               overload;

implementation


uses
  GSkPasLib.PasLib, GSkPasLib.ASCII, GSkPasLib.Dialogs, GSkPasLib.StrUtils,
  GSkPasLib.FileUtils, GSkPasLib.ComponentUtils, GSkPasLib.AppUtils;


const
  cnMaxMessageOffset   = 120 div gcnLogOffsetStep * gcnLogOffsetStep;
  cnLimitMessageOffset = cnMaxMessageOffset * 10;
  cnResetMessageOffset = 50 div gcnLogOffsetStep * gcnLogOffsetStep;
  {-}
  csLogEnterSuffix = ' - START';
  csLogExitSuffix  = ' - STOP';
  {$IFNDEF GetText}
    csLogNoAdapters  = 'Nie można śledzić - nie ma podłączonego żadnego adaptera';
    csLogNilAdapter  = 'Nie można dodać adaptera o adresie *nil*';
    csLogException   = 'Wyjątek %s: %s';
  {$ELSE}
    csLogNoAdapters  = 'Can not trace - none adapter is connected';
    csLogNilAdapter  = 'Can not add *nil* as adapter';
    csLogException   = 'Exception %0:s: %1:s';
  {$ENDIF GetText}


var
  gStdAssertProc:  TAssertErrorProc;
  gLogObjects:     TObjectList;


{$REGION 'Local subroutines'}

procedure  LogAssertError(const    sMessage, sFilename:  string;
                          nLineNumber:  Integer;
                          pErrorAddr:  Pointer);
var
  nInd:  Integer;
  fLog:  TGSkLog;

begin  { LogAssertError }
  for  nInd := 0  to  gLogObjects.Count - 1  do
    begin
      fLog := TGSkLog(gLogObjects.Items[nInd]);
      if  fLog.LogAssertions  then
        fLog.LogMessage('Assertion failed in %s, line %d: %s',
                        [sFilename, nLineNumber, sMessage],
                        lmError)
    end { for nInd };
  gStdAssertProc(sMessage, sFilename, nLineNumber, pErrorAddr)
end { LogAssertError };

{$ENDREGION 'Local subroutines'}


{$REGION 'Global subroutines'}

function  IfThen(const   bCond:  Boolean;
                 const   lmIfTrue, lmIfFalse:  TGSkLogMessageType)
                 : TGSkLogMessageType;
begin
  if  bCond  then
    Result := lmIfTrue
  else
    Result := lmIfFalse
end { IfThen };


{$ENDREGION 'Global subroutines'}


{$REGION 'TGSkAdapterList'}

constructor  TGSkAdapterList.Create(const   Parent:  TGSkLog);
begin
  inherited Create();
  fParent := Parent
end { Create };


function  TGSkAdapterList.GetAdapter(const   nIndex:  Integer)
                                     : TGSkCustomLogAdapter;
begin
  Result := inherited Get(nIndex)   // TGSkCustomLogAdapter(inherited Get(nIndex))
end { GetAdapter };


procedure  TGSkAdapterList.Notify(Ptr:  Pointer;
                                  Action:  TListNotification);
begin
  inherited;
  case  Action  of
    lnAdded:
      TGSkCustomLogAdapter(Ptr).Parent := fParent;
    lnExtracted,
    lnDeleted:
      TGSkCustomLogAdapter(Ptr).Parent := nil
  end { case Action }
end { Notify };


procedure  TGSkAdapterList.SetAdapter(const   nIndex:  Integer;
                                      const   Adapter:  TGSkCustomLogAdapter);
begin
  inherited Put(nIndex, Adapter)   // inherited Put(nIndex, Pointer(Adapter))
end { SetAdapter };

{$ENDREGION 'TGSkAdapterList'}


{$REGION 'TGSkLog'}

function  TGSkLog.AbandonLog() : TGSkLog;

var
  nLogLev:  Cardinal;
  nOffset:  Cardinal;

begin  { AbandonLog }
  Result := Self;
  fSynchro.Enter();
  try
    Assert((fnUpdateLevel > 0) = fMessages.HasMassages);
    with  fMessages  do
      if  HasMassages()  then
        repeat
          nOffset := Offset;
          nLogLev := LastMessageLogLevel(nOffset);
          // Offset := nOffset;
          { To nie zawsze dobrze działa. Prawdopodobnie złe działanie jest
            w następującej sytuacji:
              - jest wiele poziomów monitorowania (wszystkie!)
              - kod wygląda mniej więcej tak:
                BeginLog();
                  LogEnter(...);
                  ...
                EndLog();
                ...
                BeginLog();
                  ...
                  LogExit(...);
                EndLog();
            Jeżeli się nie mylę, to powyższe przypisanie miało „ratować”,
            gdy wywołania „LogEnter” i „LogExit” są nie do pary (co w zasadzie
            należy traktować jako błąd logiczny monitorowania).
            Nie mam teraz czasu, aby to dokładnie prześledzić.

            W razie zmian analogicznie zmienić również DoLog(). }
          RemoveLastMessage()
        until  (nLogLev = fnUpdateLevel)
               or IsEmpty();
    if  fnUpdateLevel > 0  then
      Dec(fnUpdateLevel);
  finally
    fSynchro.Leave()
  end { try-finally }
end { AbandonLog };


procedure  TGSkLog.AddHeaderLine(const   sLine:  string);
begin
  if  fHeader = nil  then
    fHeader := TStringList.Create();
  fHeader.Add(sLine)
end { AddHeaderLine };


procedure  TGSkLog.AddHeaderLine(const   sLineFmt:  string;
                                 const   LineArgs:  array of const);
begin
  AddHeaderLine(Format(sLineFmt, LineArgs))
end { AddHeaderLine };


function  TGSkLog.AddLogAdapter(const   LogAdapter:  TGSkCustomLogAdapter)
                                : Integer;
begin
  CheckAdapter(LogAdapter);
  Result := fAdapters.IndexOf(LogAdapter);
  if  Result < 0  then
    begin
      LogAdapter.ModuleName := Self.ModuleName;
      LogAdapter.FreeNotification(Self);
      Result := fAdapters.Add(LogAdapter);
    end { Result < 0 };
end { AddLogAdapter };


procedure  TGSkLog.AddMessage(const   Msg:  TGSkLogMessage);
begin
  fSynchro.Enter();
  try
    fMessages.AddMessage(Msg)
  finally
    fSynchro.Leave()
  end { try-finally }
end { AddMessage };


procedure  TGSkLog.Assign(Source:  TPersistent);

var
  nInd:  Integer;
  lAdapterClass:  TGSkCustomLogAdapterClass;
  lAdapter:  TGSkCustomLogAdapter;

begin  { Assign }
  if  Source is TGSkLog  then
    with  Source as TGSkLog  do
      begin
        Self.BeforeMessage := BeforeMessage;
        Self.AfterMessage := AfterMessage;
        // Self.fnUpdateLevel := fnUpdateLevel;
        Self.Offset := Offset;
        Self.Prefix := Prefix;
        Self.LogMessageTime := LogMessageTime;
        Self.LogThreadID := LogThreadID;
        Self.Active := Active;
        Self.ShowMessageTypes := ShowMessageTypes;
        Self.LogFilter := LogFilter;
        Self.AutoSession := AutoSession;
        Self.StartUp := StartUp;
        Self.CheckControlCharacters := CheckControlCharacters;
        Self.LogAssertions := LogAssertions;
        Self.LogFullName := LogFullName;
        Self.ModuleName := ModuleName;
        if  fHeader <> nil  then
          begin
            Self.fHeader := TStringList.Create();
            Self.fHeader.Assign(fHeader);
          end { fHeader <> nil };
        while  Self.fAdapters.Count <> 0  do
          Self.RemoveLogAdapter(0);
        for  nInd := 0  to  fAdapters.Count - 1  do
          begin
            lAdapterClass := TGSkCustomLogAdapterClass(fAdapters.Items[nInd].ClassType);
            lAdapter := lAdapterClass.Create(Owner);
            {#Poprawić: menadżer pamięci sygnalizuje powyżej wyciek pamięci! }
            lAdapter.Assign(fAdapters.Items[nInd]);
            Self.AddLogAdapter(lAdapter)
          end { for nInd }
      end { with Source }
  else
    inherited
end { Assign };


function  TGSkLog.BeginLevel() : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    Offset := Offset + gcnLogOffsetStep;
    if  Offset > cnLimitMessageOffset  then
      Offset := cnResetMessageOffset
  finally
    fSynchro.Leave()
  end { try-finally }
end { BeginLevel };


function  TGSkLog.BeginLog() : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    if  fnUpdateLevel = 0  then
      fMessages.Clear();
    Inc(fnUpdateLevel);
    fMessages.AddMessage(TGSkLogMessageMarker.Create(fnUpdateLevel, Offset))
  finally
    fSynchro.Leave()
  end { try-finally }
end { BeginLog };


class function  TGSkLog.BooleanToString(const   bValue:  Boolean)
                                        : string;
begin
  Result := BoolToStr(bValue, True)
end { BooleanToString };


function  TGSkLog.CanLog(const   lmLogLevel:  TGSkLogMessageType)
                         : Boolean;
begin
  Result := lmLogLevel >= fLogFilter
end { CanLog };


procedure  TGSkLog.CheckAdapter(const   LogAdapter:  TGSkCustomLogAdapter);
begin
  if  LogAdapter = nil  then
    Error('TGSkLog.CheckAdapter', dgettext('GSkPasLib', csLogNilAdapter));
end { CheckAdapter };


procedure  TGSkLog.CheckAdapters();
begin
  if  fAdapters.Count = 0  then
    Error('TGSkLog.CheckAdapters', dgettext('GSkPasLib', csLogNoAdapters));
end { CheckAdapters };


function  TGSkLog.Clear() : TGSkLog;

var
  nInd:  Integer;
  lLogAdapter:  TGSkCustomLogAdapter;

begin  { Clear }
  Result := Self;
  fSynchro.Enter();
  try
    CheckAdapters();
    fMessages.Clear();
    for  nInd := fAdapters.Count - 1  downto  0  do
      begin
        lLogAdapter := fAdapters.Items[nInd];
        try
          CallAbstractMethod(lLogAdapter.Clear, lLogAdapter, 'Clear')
        except
          on  EAbort  do
            { OK }
          else
            raise
        end { try-except }
      end { nInd }
  finally
    fSynchro.Leave()
  end { try-finally }
end { Clear };


constructor  TGSkLog.Create(AOwner:  TComponent);
begin
  inherited;
  Init(AOwner)
end { Create };


constructor  TGSkLog.CreateLog(const   AOwner:  TComponent;
                               const   Adapters:  array of TGSkCustomLogAdapterClass);
var
  nInd:  Integer;

begin  { CreateLog }
  inherited Create(AOwner);
  Init(AOwner);
  for  nInd := Low(Adapters)  to  High(Adapters)  do
    AddLogAdapter(Adapters[nInd].Create(Self))
    { Lepiej w tej kolejności, bo logowanie do pliku jest zazwyczaj pierwszym
      zarejestrowanym adapterem. }
end { CreateLog };


class function  TGSkLog.DateTimeToString(const   dtValue:  TDateTime)
                                         : string;
begin
  if  dtValue = 0  then
    Result := DateTimeToStr(dtValue)
  else if  TimeOf(dtValue) = 0  then
    Result := DateToStr(dtValue)
  else if  DateOf(dtValue) = 0  then
    Result := TimeToStr(dtValue)
  else
    Result := DateTimeToStr(dtValue)
end { DateTimeToString };


destructor  TGSkLog.Destroy();
begin
  fSynchro.Enter();
  try
    while  fAdapters.Count <> 0  do
      RemoveLogAdapter(fAdapters[0]);
    fAdapters.Free();
    fMessages.Free();
    fHeader.Free();
    gLogObjects.Remove(Self);
  finally
    fSynchro.Leave();
    FreeAndNil(fSynchro)
  end { try-finally };
  inherited
end { Destroy };


procedure  TGSkLog.DoAfterMessage(const   Msg:  TGSkLogMessage);
begin
  if  Assigned(ffnAfterMessage)  then
    ffnAfterMessage(Self, Msg)
end { DoAfterMessage };


procedure  TGSkLog.DoBeforeMessage(const   Msg:  TGSkLogMessage);
begin
  if  Assigned(ffnBeforeMessage)  then
    ffnBeforeMessage(Self, Msg)
end { DoBeforeMessage };


procedure  TGSkLog.DoLog();

var
  nIndM, nIndA:  Integer;
  lAdapter:  TGSkCustomLogAdapter;
  lAccepted:  TBits;
  lMsg:  TGSkLogMessage;
  bGetOffset:  Boolean;
  nOffset:  Cardinal;

begin  { DoLog }
  fSynchro.Enter();
  try
    CheckAdapters();
    lAccepted := TBits.Create();
    try
      lAccepted.Size := fAdapters.Count;
      for  nIndA := fAdapters.Count - 1  downto  0  do
        begin
          lAdapter := fAdapters.Items[nIndA];
          try
            TGSkCustomComponent.CallAbstractMethod(lAdapter.BeginLog, lAdapter, 'BeginLog');
            lAccepted.Bits[nIndA] := True
          except
            on  EAbort  do
              lAccepted.Bits[nIndA] := False
            else
              raise
          end { try-except }
        end { for nIndA };
      {-}
      bGetOffset := True;
      for  nIndM := 0  to  fMessages.CustomMessageCount - 1  do
        begin
          if  fMessages.IsLogMessageMarker(nIndM)  then
            begin
              if  bGetOffset  then
                begin
                  nOffset := Offset;
                  fMessages.LastMessageLogLevel(nOffset);  // pobierze Offset z najstarszego markera
                  // Offset := nOffset;
                  { To nie zawsze dobrze działa. Prawdopodobnie złe działanie jest
                    w następującej sytuacji:
                      - jest wiele poziomów monitorowania (wszystkie!)
                      - kod wygląda mniej więcej tak:
                        BeginLog();
                          LogEnter(...);
                          ...
                        EndLog();
                        ...
                        BeginLog();
                          ...
                          LogExit(...);
                        EndLog();
                    Jeżeli się nie mylę, to powyższe przypisanie miało „ratować”,
                    gdy wywołania „LogEnter” i „LogExit” są nie do pary (co w zasadzie
                    należy traktować jako błąd logiczny monitorowania).
                    Nie mam teraz czasu, aby to dokładnie prześledzić.

                    W razie zmian analogicznie zmienić również AbandonLog(). }
                  bGetOffset := False
                end { if bGetOffset }
            end { fMessages.IsLogMessageMarker(nIndM) }
          else
            begin
              lMsg := fMessages.LogMessage(nIndM);
              DoBeforeMessage(lMsg);
              for  nIndA := fAdapters.Count - 1  downto  0  do
                if  lAccepted.Bits[nIndA]  then
                  begin
                    lAdapter := fAdapters.Items[nIndA];
                    try
                      lAdapter.LogMessage(lMsg);
                    except
                      on  EAbstractError  do
                        RaiseAbstractError(lAdapter, 'LogMessage')
                      else
                        raise
                    end { try-except }
                  end { if lAccepted.Bits[nIndA]; for nIndA };
              DoAfterMessage(lMsg)
            end { if not fMessages.IsLogMessageMarker(nIndM) }
        end { for nIndM };
      {-}
      for  nIndA := fAdapters.Count - 1  downto  0  do
        if  lAccepted.Bits[nIndA]  then
          begin
            lAdapter := fAdapters.Items[nIndA];
            TGSkCustomComponent.CallAbstractMethod(lAdapter.EndLog, lAdapter, 'EndLog')
          end ;
      fMessages.Clear()
    finally
      lAccepted.Free()
    end { try-finally }
  finally
    fSynchro.Leave()
  end { try-finally }
end { DoLog };


function  TGSkLog.EndLevel() : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    Offset := EnsureRange(Int64(Offset) - gcnLogOffsetStep, 0, MaxInt)
  finally
    fSynchro.Leave()
  end { try-finally }
end { EndLevel };


function  TGSkLog.EndLog() : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    if  fnUpdateLevel > 0  then
      Dec(fnUpdateLevel);
    if  fnUpdateLevel = 0  then
      if  fMessages.HasMassages  then
        DoLog();
  finally
    fSynchro.Leave()
  end { try-finally }
end { EndLog };


procedure  TGSkLog.Error(const   sFunction, sErrMsg:  string);
begin
  if  csDesigning in ComponentState  then
    ErrBox(sErrMsg, True)
  else
    raise  EGSkLogException.Create('TGSkLog.' + sFunction,
                                   GetModuleFileName() + ': ' + sErrMsg)
end { Error };


procedure  TGSkLog.FreeInternalAdapter(const   LogAdapter:  TGSkCustomLogAdapter);
begin
  if  LogAdapter.Owner = Self  then
    LogAdapter.Free()
end { FreeInternalAdapter };


function  TGSkLog.GetAdapter(const   nIndex:  Integer)
                             : TGSkCustomLogAdapter;
begin
  Result := fAdapters.Items[nIndex]
end { GetAdapter };


function  TGSkLog.GetAdapterCount() : Integer;
begin
  Result := fAdapters.Count
end { GetAdapterCount };


function  TGSkLog.GetIsLog(const   LogLevel:  TGSkLogMessageType)
                           : Boolean;
begin
  Result := LogFilter <= LogLevel
end { IsLog };


function  TGSkLog.GetIsLogDebug() : Boolean;
begin
  Result := GetIsLog(lmDebug)
end { GetIsLogDebug };


function  TGSkLog.GetIsLogDebugDetails() : Boolean;
begin
  Result := GetIsLog(lmDebugDetails)
end { GetIsLogDebugDetails };


function  TGSkLog.GetIsLogDebugLowLevel() : Boolean;
begin
  Result := GetIsLog(lmDebugLowLevel)
end { GetIsLogDebugLowLevel };


function  TGSkLog.GetIsLogInformation() : Boolean;
begin
  Result := GetIsLog(lmInformation)
end { GetIsLogInformation };


function  TGSkLog.GetIsLogWarning() : Boolean;
begin
  Result := GetIsLog(lmWarning)
end { GetIsLogWarning };


function  TGSkLog.GetLogFullName() : string;

var
  nInd:  Integer;

begin  { GetLogFullName }
  if  fsLogFullName = ''  then
    for  nInd := 0  to  fAdapters.Count - 1  do
      begin
        fsLogFullName := fAdapters.Items[nInd].LogFullName;
        if  fsLogFullName <> ''  then
          Break
      end { for nInd };
  Result := fsLogFullName
end { GetLogFullName };


function  TGSkLog.GetLogName() : string;

var
  nInd:  Integer;

begin  { GetLogName }
  if  fsLogName = ''  then
    for  nInd := 0  to  fAdapters.Count - 1  do
      begin
        fsLogName := fAdapters.Items[nInd].LogName;
        if  fsLogName <> ''  then
          Break
      end { for nInd };
  Result := fsLogName
end { GetLogName };


function  TGSkLog.GetModuleName() : string;
begin
  Result := fsModuleName;
  if  Result = ''  then
    Result := GetAppTitle()
end { GetModuleName };


function  TGSkLog.GetOffset() : Cardinal;
begin
  Result := fnOffset
end { GetOffset };


function  TGSkLog.GetPrefix() : string;
begin
  Result := fsPrefix
end { GetPrefix };


function  TGSkLog.GetStartup() : Boolean;
begin
  Result := fbStartup
end { GetStartup };


procedure  TGSkLog.Init(const   AOwner:  TComponent);

var
  nInd:  Integer;

begin  { Init }
  fbActive := True;
  fbLogMessageTime := True;
  fbLogThreadID := True;
  fbAutoSession := True;
  fbStartup := True;
  fbChkCtrlChars := True;
  fAdapters := TGSkAdapterList.Create(Self);
  fSynchro := TCriticalSection.Create();
  fsetShowMsgTypes := gcsetShowMsgTypes;
  fLogFilter := lmInformation;
  fMessages := TGSkLogMessages.Create();
  if  AOwner <> nil  then
    with  AOwner  do
      for  nInd := ComponentCount - 1  downto  0  do
        if  Components[nInd] is TGSkCustomLogAdapter  then
          AddLogAdapter(Components[nInd] as TGSkCustomLogAdapter);
  fbLogAssertions := True;
  gLogObjects.Add(Self)
end { Init };


class function  TGSkLog.InterfaceToString(const   iValue:  IInterface)
                                          : string;
begin
  if  iValue <> nil  then
    Result := Format('{interface %p}', [Pointer(iValue)])
  else
    Result := '*nil*'
end { InterfaceToString };


procedure  TGSkLog.InternalLogMessage(const   sMsg:         string;
                                      const   lmMsgType:    TGSkLogMessageType;
                                     {const}  setMsgFlags:  TGSkLogMessageFlags);
var
  setFlags:  TGSkLogMessageFlags;
  nInd:  Integer;
  lBuf:  TStringList;

begin  { InternalLogMessage }
  if  fbActive
      and CanLog(lmMsgType)  then
    begin
      fSynchro.Enter();
      try
        if  Startup  then
          begin
            Startup := False;
            if  AutoSession
                and (lmInformation >= fLogFilter)  then
              begin
                BeginLog();
                try
                  InternalLogMessage('', lmInformation, [mfSession]);
                  setFlags := setMsgFlags - [mfMemo] + [mfInit];
                  InternalLogMessage(gcsSessionStart, lmInformation, setFlags);
                  BeginLevel();
                  try
                    InternalLogMessage('* Module: ' + GetModuleFileName(),
                                       lmInformation, setFlags);
                    InternalLogMessage('* Version: ' + GetModuleVersion('', 4),
                                       lmInformation, setFlags);
                    InternalLogMessage('* Computer: ' + GetLocalComputerName(),
                                       lmInformation, setFlags);
                    InternalLogMessage('* UserName: ' + GetLocalUserName(),
                                       lmInformation, setFlags);
                    lBuf := TStringList.Create();
                    try
                      GetIpAddresses(lBuf);
                      InternalLogMessage('* Address IP: '
                                           + StringReplace(lBuf.CommaText,
                                                           ',', ', ', [rfReplaceAll]),
                                         lmInformation, setFlags)
                    finally
                      lBuf.Free()
                    end { try-finally };
                    if  fHeader <> nil  then
                      begin
                        for  nInd := 0  to  fHeader.Count - 1  do
                          InternalLogMessage('* ' + fHeader.Strings[nInd],
                                             lmInformation, setFlags);
                        FreeAndNil(fHeader)
                      end { fHeader <> nil }
                  finally
                    EndLevel()
                  end { try-finally }
                finally
                  EndLog()
                end { try-finally }
              end { if AutoSession }
          end { if Startup };
        if  fbLogMessageTime  then
          Include(setMsgFlags, mfLogTime);
        if  fbLogThreadID  then
          Include(setMsgFlags, mfLogThreadID);
        AddMessage(TGSkLogMessage.Create(sMsg, Prefix, lmMsgType,
                                         Offset, fsetShowMsgTypes,
                                         setMsgFlags))
      finally
        fSynchro.Leave()
      end { try-finally }
    end { fbActive and CanLog(lmMsgType) }
end { InternalLogMessage };


procedure  TGSkLog.InternalLogMessage(const   sMsgFmt:    string;
                                      const   MsgArgs:    array of const;
                                      const   lmMsgType:  TGSkLogMessageType;
                                      const   setMsgFlags:  TGSkLogMessageFlags);
begin
  InternalLogMessage(Format(sMsgFmt, MsgArgs), lmMsgType, setMsgFlags)
end { InternalLogMessage };


function  TGSkLog.LogEnter(const   sMsgFmt:  string;
                           const   MsgArgs:  array of const;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
begin
  Result := LogEnter(Format(sMsgFmt, MsgArgs), lmMsgType)
end { LogEnter };


function  TGSkLog.LogEnter(const   sMsg:  string;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
begin
  Result := Self;
  if  CanLog(lmMsgType)  then
    begin
      BeginLog();
      try
        BeginLevel();
        InternalLogMessage(sMsg + csLogEnterSuffix, lmMsgType, [mfEnter]);
        BeginLevel()
      finally
        EndLog()
      end { try-finally }
    end { CanLog(lmMsgType) }
end { LogEnter };


function  TGSkLog.LogExit(const   sMsg:  string;
                          const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
begin
  Result := Self;
  if  CanLog(lmMsgType)  then
    begin
      BeginLog();
      try
        EndLevel();
        InternalLogMessage(sMsg + csLogExitSuffix, lmMsgType, [mfExit]);
        EndLevel()
      finally
        EndLog()
      end { try-finally }
    end { CanLog(lmMsgType) }
end { LogExit };


function  TGSkLog.LogException(const   eError:  Exception;
                               const   sInfo:  string;
                               const   lmMsgType:  TGSkLogMessageType)
                               : TGSkLog;
const
  cnErrorSeparatorLen = 25;

{$IF CompilerVersion >= 22}
var
  sStackTrace:  string;
{$IFEND}

  procedure  LogError(const   eError:  Exception);
  begin
    if  eError <> nil  then
      begin
        LogMessage(dgettext('GSkPasLib', csLogException),
                   [eError.ClassName(), eError.Message],
                   lmMsgType);
        {$IF CompilerVersion >= 20}
          if  eError.StackTrace <> ''  then
            LogMemo(eError.StackTrace, lmError);
        {$IFEND}
        {$IF CompilerVersion >= 22}
          BeginLevel();
          try
            LogError(eError.InnerException);
          finally
            EndLevel()
          end { try-finally }
        {$IFEND}
      end { eError <> nil }
  end { LogError };

begin  { LogException }
  Result := Self;
  BeginLog();
  try
    if  sInfo <> ''  then
      LogMessage(sInfo + ' - ' + eError.ClassName(), lmMsgType);
    LogSeparator('-', cnErrorSeparatorLen);
    LogError(eError);
    {$IF CompilerVersion >= 22}
      sStackTrace := eError.StackTrace;
      if  sStackTrace <> ''  then
        LogMemo(sStackTrace, lmMsgType);
    {$IFEND}
    LogSeparator('-', cnErrorSeparatorLen)
  finally
    EndLog()
  end { try-finally }
end { LogException };


function  TGSkLog.LogException(const   eError:  Exception;
                               const   sInfoFmt:  string;
                               const   InfoArgs:  array of const;
                               const   lmMsgType:  TGSkLogMessageType)
                               : TGSkLog;
begin
  Result := LogException(eError, Format(sInfoFmt, InfoArgs), lmMsgType)
end { LogException };


function  TGSkLog.LogException(const   eError:  Exception;
                               const   lmMsgType:  TGSkLogMessageType)
                               : TGSkLog;
begin
  Result := LogException(eError, '', lmMsgType)
end { LogException };


function  TGSkLog.LogExit(const   sMsgFmt:  string;
                          const   MsgArgs:  array of const;
                          const   lmMsgType:  TGSkLogMessageType)
                          : TGSkLog;
begin
  Result := LogExit(Format(sMsgFmt, MsgArgs), lmMsgType)
end { LogExit };


function  TGSkLog.LogLines(const   nLines:  Integer;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
var
  nInd:  Integer;
  bLogTime:  Boolean;

begin  { LogLines }
  Result := Self;
  BeginLog();
  try
    bLogTime := fbLogMessageTime;
    fbLogMessageTime := False;
    try
      for  nInd := nLines - 1  downto  0  do
        InternalLogMessage('', lmMsgType)
    finally
      fbLogMessageTime := bLogTime
    end { try-finally }
  finally
    EndLog()
  end { try-finally }
end { LogLines };


function  TGSkLog.LogMemo(const   sMemo:  string;
                          const   lmMsgType:  TGSkLogMessageType)
                          : TGSkLog;
begin
  Result := Self;
  BeginLog();
  try
    InternalLogMessage(sMemo, lmMsgType, [mfMemo])
  finally
    EndLog()
  end { try-finally }
end { LogMemo };


function  TGSkLog.LogMemo(const   sName, sMemo:  string;
                          const   lmMsgType:  TGSkLogMessageType)
                          : TGSkLog;
begin
  Result := Self;
  BeginLog();
  try
    LogMessage(sName + ':', lmMsgType);
    LogMemo(sMemo, lmMsgType)
  finally
    EndLog()
  end { try-finally }
end { LogMemo };


function  TGSkLog.LogMessage(const   sMsg:  string;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  Result := Self;
  BeginLog();
  try
    InternalLogMessage(sMsg, lmMsgType)
  finally
    EndLog()
  end { try-finally }
end { LogMessage };


function  TGSkLog.LogMessage(const   sMsgFmt:  string;
                             const   MsgArgs:  array of const;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  Result := Self;
  BeginLog();
  try
    InternalLogMessage(sMsgFmt, MsgArgs, lmMsgType)
  finally
    EndLog()
  end { try-finally }
end { LogMessage };


function  TGSkLog.LogResult(const   vValue:  Variant;
                            const   lmMsgType:  TGSkLogMessageType)
                            : TGSkLog;
begin
  Result := LogValue('Result', vValue, lmMsgType)
end { LogResult };


function  TGSkLog.LogResult(const   nValue:  Int64;
                            const   ValType:  TGSkLogValueNumType;
                            const   lmMsgType:  TGSkLogMessageType)
                            : TGSkLog;
begin
  Result := LogValue('Result', nValue, ValType, lmMsgType)
end { LogResult };


function  TGSkLog.LogResult(const   Value:  TStrings;
                            const   bLineNumbers:  Boolean;
                            const   lmMsgType:  TGSkLogMessageType)
                            : TGSkLog;
begin
  Result := LogValue('Result', Value, bLineNumbers, lmMsgType)
end { LogResult };


function  TGSkLog.LogResult(const   Value:  TStrings;
                            const   lmMsgType:  TGSkLogMessageType)
                            : TGSkLog;
begin
  Result := LogValue('Result', Value, lmMsgType)
end { LogResult };


function  TGSkLog.LogResult(const   Value:  TObject;
                            const   lmMsgType:  TGSkLogMessageType)
                            : TGSkLog;
begin
  Result := LogValue('Result', Value, lmMsgType)
end { LogResult };


function  TGSkLog.LogResult(const   Value:  TComponent;
                            const   bDetailed:  Boolean;
                            const   lmMsgType:  TGSkLogMessageType)
                            : TGSkLog;
begin
  Result := LogValue('Result', Value, bDetailed, lmMsgType)
end { LogResult };


function  TGSkLog.LogResult(const   iValue:  IInterface;
                            const   lmMsgType:  TGSkLogMessageType)
                            : TGSkLog;
begin
  Result := LogValue('Result', iValue, lmMsgType)
end { LogResult };


function  TGSkLog.LogResult(const   nValue:  Currency;
                            const   ValType:  TGSkLogValueCurrType;
                            const   lmMsgType:  TGSkLogMessageType)
                            : TGSkLog;
begin
  Result := LogValue('Result', nValue, ValType, lmMsgType)
end { LogResult };


function  TGSkLog.LogSeparator(const   cSepChar:  Char;
                               const   nSepLen:  Integer;
                               const   lmMsgType:  TGSkLogMessageType)
                               : TGSkLog;
var
  bSaveLogMsgTime:  Boolean;
  setShowMsgTypes:  TGSkLogMessageTypes;

begin  { LogSeparator }
  { Save and clear flags }
  bSaveLogMsgTime := fbLogMessageTime;   fbLogMessageTime := False;
  setShowMsgTypes := fsetShowMsgTypes;   fsetShowMsgTypes := [];
  { Log separator }
  Result := LogMessage(StringOfChar(cSepChar, nSepLen), lmMsgType);
  { Restore flags }
  fbLogMessageTime := bSaveLogMsgTime;
  fsetShowMsgTypes := setShowMsgTypes
end { LogSeparator };


function  TGSkLog.LogValue(const   sName:  string;
                           const   vValue:  Variant;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
var
  sValue:  string;
  nInd:    Integer;
  lBuf:    TStringList;

begin  { LogValue }
  Result := Self;
  if  CanLog(lmMsgType)  then
    if  VarIsArray(vValue)  then
      begin
        BeginLog();
        try
          LogMessage(sName + ':', lmMsgType);
          {-}
          nInd := Max(Length(IntToStr(VarArrayLowBound(vValue, 1))),
                      Length(IntToStr(VarArrayHighBound(vValue, 1))));  // max # of digits
          sValue := Format('[%%%0:d.%0:dd]', [nInd]);  // e.g. '[%2.2d]' etc.
          {-}
          BeginLevel();
          for  nInd := VarArrayLowBound(vValue, 1)  to  VarArrayHighBound(vValue, 1)  do
            LogValue(Format(sValue, [nInd]), vValue[nInd], lmMsgType);
          EndLevel()
        finally
          EndLog()
        end { try-finally }
      end { VarIsArray(vValue) }
    else
      begin
        sValue := VariantToString(vValue, fbChkCtrlChars);
        if  not fbChkCtrlChars
            and VarIsStr(vValue)  then
          begin
            nInd := Length(sValue);
            if  nInd > 0  then
              repeat
                if  CharInSet(sValue[nInd], {gcsetControlChars}gcsetNewLine)  then
                  Break;
                Dec(nInd)
              until  nInd = 0
            else
              sValue := '""'   // empty string
          end { fbChkCtrlChars }
        else
          nInd := 0;
        BeginLog();
        try
          if  nInd = 0  then
            InternalLogMessage('%s = %s', [sName, sValue], lmMsgType)
          else
            begin
              lBuf := TStringList.Create();
              try
                lBuf.Text := sValue;
                LogValue(sName, lBuf, False, lmMsgType)
              finally
                lBuf.Free()
              end { try-finally }
            end { nInd <> 0 }
        finally
          EndLog()
        end { try-finally }
      end { not VarIsArray(vValue) }
end { LogValue };


function  TGSkLog.LogValue(const   sName:  string;
                           const   nValue:  Int64;
                           const   ValType:  TGSkLogValueNumType;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
var
  sBuf:  string;

  function  IntToBinTrim(const   nValue:  UInt64)
                         : string;
  begin
    Result := IntToBin(nValue);
    while  StartsStr('0000 ', Result)  do
      Delete(Result, 1, 5)
  end { IntToBinTrim };

begin  { LogValue }
  case  ValType  of
    vtDec:
      sBuf := Format('%d', [nValue]);
    vtHex:
      sBuf := Format('$%x', [nValue]);
    vtBin:
      sBuf := IntToBin(nValue);
    vtDecHex:
      sBuf := Format('%0:d // HEX: $%0:x', [nValue]);
    vtHexDec:
      sBuf := Format('$%0:x // DEC: %0:d', [nValue]);
    vtHexBin:
      sBuf := Format('$%0:x // BIN: %1:s', [nValue,  IntToBinTrim(nValue)])
  end { case ValType };
  Result := LogMessage('%s = %s', [sName, sBuf], lmMsgType)
end { LogValue };


function  TGSkLog.LogValue(const   sName:  string;
                           const   Memory;
                           const   nMemSize:  Cardinal;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
const
  cnLen = 16;

type
  TBytes = packed array[0..MaxInt-1] of  Byte;

var
  nInd:  Integer;
  sBufH, sBufS:  string;
  cBuf:  Char;
  lMem:  TBytes absolute Memory;
  bCont:  Boolean;
  bSaveMsgTime:  Boolean;

begin  { LogValue }
  Result := Self;
  if  CanLog(lmMsgType)
      and (nMemSize > 0)  then
    begin
      sBufH := '';
      sBufS := '';
      BeginLog();
      try
        if  sName <> ''  then
          InternalLogMessage(sName + ':', lmMsgType);
        bSaveMsgTime := fbLogMessageTime;   fbLogMessageTime := False;
        try
          bCont := False;
          for  nInd := 0  to  nMemSize - 1  do
            begin
              cBuf := Chr(lMem[nInd]);
              sBufH := Format('%s %2.2x', [sBufH, Ord(cBuf)]);
              if  CharInSet(cBuf, gcsetControlChars)  then
                cBuf := '·';
              sBufS := sBufS + cBuf;
              if  Length(sBufS) = cnLen  then
                begin
                  InternalLogMessage('%s | %s', [sBufH, sBufS], lmMsgType);
                  sBufH := '';
                  sBufS := '';
                  bCont := True
                end { Length(sBufS) = cnLen }
            end { for nInd };
          if  sBufH <> ''  then
            if  bCont  then
              InternalLogMessage('%-*s | %s', [cnLen * 3, sBufH, sBufS], lmMsgType)
            else
              InternalLogMessage('%s | %s', [sBufH, sBufS], lmMsgType)
        finally
          fbLogMessageTime := bSaveMsgTime
        end { try-finally }
      finally
        EndLog()
      end { try-finally }
    end { CanLog(lmMsgType) and nMemSize > 0 }
end { LogValue };


function  TGSkLog.LogValue(const   sName:  string;
                           const   Value:  TStrings;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
begin
  Result := LogValue(sName, Value, 0, MaxInt, True, lmMsgType)
end { LogValue };


function  TGSkLog.LogValue(const   sName:  string;
                           const   Value:  TStrings;
                           const   bLineNumbers:  Boolean;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
begin
  Result := LogValue(sName, Value, 0, MaxInt, bLineNumbers, lmMsgType)
end { LogValue };


function  TGSkLog.LogValue(const   sName:  string;
                           const   Value:  TStrings;
                           const   nFromIdx, nToIdx:  Integer;
                           const   bLineNumbers:  Boolean;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
var
  nInd:  Integer;
  bTmp:  Boolean;
  sFmt:  string;

begin  { LogValue }
  Result := Self;
  if  CanLog(lmMsgType)  then
    begin
      BeginLog();
      try
        if  Value = nil  then
          LogValue(sName, TObject(Value), lmMsgType)
        else
          begin
            LogMessage(sName + ' = [', lmMsgType);
            if  not bLineNumbers  then
              BeginLevel();
            bTmp := LogMessageTime;
            LogMessageTime := False;
            try
              if  bLineNumbers  then
                sFmt := '%0:3d. %1:s'
              else
                sFmt := '%1:s';
              for  nInd := Max(nFromIdx, 0)  to  Min(nToIdx, Value.Count - 1)  do
                LogMessage(sFmt, [nInd, Value.Strings[nInd]], lmMsgType);
              if  not bLineNumbers  then
                EndLevel();
              LogMessage(']', lmMsgType)
            finally
              LogMessageTime := bTmp
            end { try-finally }
          end { Value <> nil }
      finally
        EndLog()
      end { try-finally }
    end { CanLog(lmMsgType) }
end { LogValue };


function  TGSkLog.LogValue(const   sName:  string;
                           const   Value:  TObject;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
begin
  Result := LogMessage(sName + ' = ' + ObjectToString(Value), lmMsgType)
end { LogValue };


function  TGSkLog.LogValue(const   sName:  string;
                           const   iValue:  IInterface;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
begin
  Result := LogMessage(sName + ' = ' + InterfaceToString(iValue), lmMsgType)
end { LogValue };


function  TGSkLog.LogValue(const   sName:  string;
                           const   nValue:  Currency;
                           const   ValType:  TGSkLogValueCurrType;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
var
  sFormat:  string;

begin  { LogValue }
  case  ValType  of
    ctNumber,
    ctMoney:
      begin
        sFormat := ',##0.00##';
        if  ValType = ctMoney  then
          sFormat := Format('%s" %s"', [sFormat, FormatSettings.CurrencyString]);
      end { ctNumber, ctMoney };
    ctPercent:
      sFormat := '#0.##"%"'
  end { case ValType };
  Result := LogMessage(sName + ' = ' + FormatFloat(sFormat, nValue),
                       lmMsgType)
end { LogValue };


function  TGSkLog.LogValue(const   sName:  string;
                           const   Value:  TComponent;
                           const   bDetailed:  Boolean;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
var
  sBuf:  string;

begin  { LogValue }
  if  Value = nil  then
    sBuf := '*nil*'
  else if  bDetailed  then
    sBuf := ComponentToStr(Value)
  else
    sBuf := ComponentDisplayName(Value, True);
  Result := LogMessage(sName + ' = ' + sBuf, lmMsgType)
end { LogValue };


function  TGSkLog.LogValue(const   sName:  string;
                           const   guidValue:  TGUID;
                           const   lmMsgType:  TGSkLogMessageType)
                           : TGSkLog;
begin
  Result := LogMessage(sName + ' = ' + GUIDToString(guidValue), lmMsgType)
end { LogValue };


function  TGSkLog.LogValueIf(const   bCond:  Boolean;
                             const   sName:  string;
                             const   nValue:  Int64;
                             const   ValType:  TGSkLogValueNumType;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  if  bCond  then
    LogValue(sName, nValue, ValType, lmMsgType);
  Result := Self
end { LogValueIf };


function  TGSkLog.LogValueIf(const   bCond:  Boolean;
                             const   sName:  string;
                             const   vValue:  Variant;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  if  bCond  then
    LogValue(sName, vValue, lmMsgType);
  Result := Self
end { LogValueIf };


function  TGSkLog.LogValueIf(const   bCond:  Boolean;
                             const   sName:  string;
                             const   nValue:  Currency;
                             const   ValType:  TGSkLogValueCurrType;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  if  bCond  then
    LogValue(sName, nValue, ValType, lmMsgType);
  Result := Self
end { LogValueIf };


function  TGSkLog.LogValueIf(const   bCond:  Boolean;
                             const   sName:  string;
                             const   Value:  TStrings;
                             const   bLineNumbers:  Boolean;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  if  bCond  then
    LogValue(sName, Value, bLineNumbers, lmMsgType);
  Result := Self
end { LogValueIf };


function  TGSkLog.LogValueIf(const   bCond:  Boolean;
                             const   sName:  string;
                             const   Value:  TStrings;
                             const   nFromIdx, nToIdx:  Integer;
                             const   bLineNumbers:  Boolean;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  if  bCond  then
    LogValue(sName, Value, nFromIdx, nToIdx, bLineNumbers, lmMsgType);
  Result := Self
end { LogValueIf };


function  TGSkLog.LogValueIf(const   bCond:  Boolean;
                             const   sName:  string;
                             const   Memory;
                             const   nMemSize:  Cardinal;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  if  bCond  then
    LogValue(sName, Memory, nMemSize, lmMsgType);
  Result := Self
end { LogValueIf };


function  TGSkLog.LogValueIf(const   bCond:  Boolean;
                             const   sName:  string;
                             const   Value:  TStrings;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  if  bCond  then
    LogValue(sName, Value, lmMsgType);
  Result := Self
end { LogValueIf };


function  TGSkLog.LogValueIf(const   bCond:  Boolean;
                             const   sName:  string;
                             const   iValue:  IInterface;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  if  bCond  then
    LogValue(sName, iValue, lmMsgType);
  Result := Self
end { LogValueIf };


function  TGSkLog.LogValueIf(const   bCond:  Boolean;
                             const   sName:  string;
                             const   guidValue:  TGUID;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  if  bCond  then
    LogValue(sName, guidValue, lmMsgType);
  Result := Self
end { LogValueIf };


function  TGSkLog.LogValueIf(const   bCond:  Boolean;
                             const   sName:  string;
                             const   Value:  TObject;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  if  bCond  then
    LogValue(sName, Value, lmMsgType);
  Result := Self
end { LogValueIf };


function  TGSkLog.LogValueIf(const   bCond:  Boolean;
                             const   sName:  string;
                             const   Value:  TComponent;
                             const   bDetailed:  Boolean;
                             const   lmMsgType:  TGSkLogMessageType)
                             : TGSkLog;
begin
  if  bCond  then
    LogValue(sName, Value, bDetailed, lmMsgType);
  Result := Self
end { LogValueIf };


procedure  TGSkLog.Notification(AComponent:  TComponent;
                                Operation:  TOperation);
begin
  inherited;
  if  AComponent is TGSkCustomLogAdapter  then
    case  Operation  of
      opInsert:
        if  AComponent.Owner = Self  then
          AddLogAdapter(AComponent as TGSkCustomLogAdapter);
      opRemove:
        if  fAdapters.IndexOf(AComponent) >= 0  then
          RemoveLogAdapter(AComponent as TGSkCustomLogAdapter)
    end { case Operation }
end { Notification };


class function  TGSkLog.ObjectToString(const   Value:  TObject)
                                       : string;
begin
  if  Value <> nil  then
    begin
      try
        if  IsPublishedProp(Value, 'Name')  then
          begin
            Result := GetPropValue(Value, 'Name');
            if  Result <> ''  then
              Result := Result + ': '
          end { IsPublishedProp(...) }
        else
          Result := ''
      except
        Result := ''
      end { try-except };
      {$IF CompilerVersion > 22}
        Result := Result + Value.QualifiedClassName()
      {$ELSE}
        Result := Result + Value.ClassName()
      {$IFEND}
    end { Value <> nil }
  else
    Result := '*nil*';
end { ObjectToString };


procedure  TGSkLog.RemoveLogAdapter(const   nAdapter:  Integer);

var
  lAdapter:  TGSkCustomLogAdapter;

begin  { RemoveLogAdapter }
  lAdapter := fAdapters.Items[nAdapter];
  lAdapter.RemoveFreeNotification(Self);
  fAdapters.Delete(nAdapter);
  FreeInternalAdapter(lAdapter)
end { RemoveLogAdapter };


procedure  TGSkLog.RemoveLogAdapter(const   LogAdapter:  TGSkCustomLogAdapter);
begin
  CheckAdapter(LogAdapter);
  LogAdapter.RemoveFreeNotification(Self);
  fAdapters.Remove(LogAdapter);
  FreeInternalAdapter(LogAdapter)
end { RemoveLogAdapter };


{$IFDEF DEBUG}

procedure  TGSkLog.SetLogFilter(const   Value:  TGSkLogMessageType);
begin
  fLogFilter := Value   // if required, one can set breakpoint here
end { SetLogFilter };

{$ENDIF DEBUG}


procedure  TGSkLog.SetLogFullName(const   sName:  string);

var
  nInd:  Integer;

begin  { SetLogFullName }
  fsLogFullName := TGSkCustomLogAdapter.EvalLogName(sName, True);
  for  nInd := fAdapters.Count - 1  downto  0  do
    fAdapters[nInd].LogFullName := fsLogFullName
end { SetLogFullName };


procedure  TGSkLog.SetLogName(const   sName:  string);

var
  nInd:  Integer;

begin  { SetLogName }
  fsLogName := TGSkCustomLogAdapter.EvalLogName(sName, False);
  for  nInd := fAdapters.Count - 1  downto  0  do
    fAdapters[nInd].LogName := fsLogName
end { SetLogName };


procedure  TGSkLog.SetModuleName({const} sName:  string);

var
  nInd:  Integer;

begin  { SetModuleName }
  fsModuleName := sName;
  if  sName = ''  then
    sName := GetModuleName();   // do adapterów przekaże wartość domyślną
  for  nInd := fAdapters.Count - 1  downto  0  do
    fAdapters[nInd].ModuleName := sName
end { SetModuleName };


procedure  TGSkLog.SetOffset(const   nOffset:  Cardinal);
begin
  fnOffset := nOffset
end { SetOffset };


procedure  TGSkLog.SetStartup(const   bValue:  Boolean);
begin
  fbStartup := bValue
end { SetStartup };


class function  TGSkLog.VariantToString(const   vValue:  Variant;
                                        const   bChkCtrlChars:  Boolean)
                                        : string;
var
  lValue:  record
           case  Char  of
             'M':  (nValueCurr:  Currency);
             'W':  (nValueWord:  LongWord);
             'I':  (nValueInt:   Int64);
           end { Char };

begin  { VariantToString }
  try
    case  VarType(vValue)  of
      varUnknown:
        Result := '<unknown>';
      varEmpty:
        Result := '<empty>';
      varNull:
        Result := '<null>';
      varSmallInt, varInteger, varShortInt, varInt64:
        begin
          lValue.nValueInt := vValue;
          Result := IntToStr(lValue.nValueInt);
        end { varSmallInt, varInteger, varShortInt, varInt64 };
      varByte, varWord, varLongWord:
        begin
          lValue.nValueWord := vValue;
          Result := Format('%u', [lValue.nValueWord]);
        end { varByte, varWord, varLongWord };
      varSingle, varDouble:
        Result := FloatToStr(vValue);
      varCurrency:
        begin
          // Result := Format('%m', [vValue]);  -- run-time error
          lValue.nValueCurr := vValue;
          Result := Format('%m', [lValue.nValueCurr])
        end { varCurrency };
      varDate:
        Result := DateTimeToString(vValue);
      varBoolean:
        Result := BooleanToString(vValue);
      varString:
        begin
          Result := AnsiStringToString(AnsiString(vValue));
          if  bChkCtrlChars  then
            Result := QuoteStr(Result)
        end { varString }
      else
        if  bChkCtrlChars  then
          Result := QuoteStr(vValue)
        else
          Result := vValue
    end { case VarType(vValue) }
  except
    on  eErr : Exception  do
      Result := Format('TGSkLog.VariantToString exception %s: %s',
                       [eErr.ClassName(), eErr.Message])
  end { try-except };
end { VariantToString };


function  TGSkLog.LogResultExit(const   nValue:  Int64;
                                const   sMsg:  string;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(nValue, lmMsgType);
    LogExit(sMsg, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };


function  TGSkLog.LogResultExit(const   vValue:  Variant;
                                const   sMsg:  string;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(vValue, lmMsgType);
    LogExit(sMsg, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };


function  TGSkLog.LogResultExit(const   nValue:  Int64;
                                const   sMsgFmt:  string;
                                const   MsgArgs:  array of const;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(nValue, lmMsgType);
    LogExit(sMsgFmt, MsgArgs, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };


function  TGSkLog.LogResultExit(const   vValue:  Variant;
                                const   sMsgFmt:  string;
                                const   MsgArgs:  array of const;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(vValue, lmMsgType);
    LogExit(sMsgFmt, MsgArgs, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };


function  TGSkLog.LogResultExit(const   iValue:  IInterface;
                                const   sMsg:  string;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(iValue, lmMsgType);
    LogExit(sMsg, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };


function  TGSkLog.LogResultExit(const   Value:  TObject;
                                const   sMsg:  string;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(Value, lmMsgType);
    LogExit(sMsg, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };


function  TGSkLog.LogResultExit(const   Value:  TStrings;
                                const   sMsg:  string;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(Value, lmMsgType);
    LogExit(sMsg, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };


function  TGSkLog.LogResultExit(const   iValue:  IInterface;
                                const   sMsgFmt:  string;
                                const   MsgArgs:  array of const;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(iValue, lmMsgType);
    LogExit(sMsgFmt, MsgArgs, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };


function  TGSkLog.LogResultExit(const   nValue:  Currency;
                                const   sMsg:  string;
                                const   ValType:  TGSkLogValueCurrType;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(nValue, ValType, lmMsgType);
    LogExit(sMsg, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };


function  TGSkLog.LogResultExit(const   Value:  TObject;
                                const   sMsgFmt:  string;
                                const   MsgArgs:  array of const;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(Value, lmMsgType);
    LogExit(sMsgFmt, MsgArgs, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };


function  TGSkLog.LogResultExit(const   Value:  TComponent;
                                const   sMsg:  string;
                                const   bDetailed:  Boolean;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(Value, bDetailed, lmMsgType);
    LogExit(sMsg, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };


function  TGSkLog.LogResultExit(const   Value:  TStrings;
                                const   sMsgFmt:  string;
                                const   MsgArgs:  array of const;
                                const   lmMsgType:  TGSkLogMessageType)
                                : TGSkLog;
begin
  Result := Self;
  fSynchro.Enter();
  try
    LogResult(Value, lmMsgType);
    LogExit(sMsgFmt, MsgArgs, lmMsgType)
  finally
    fSynchro.Leave()
  end { try-finally }
end { LogResultExit };

{$ENDREGION 'TGSkLog'}


{$REGION 'TGSkCustomLogAdapter'}

procedure  TGSkCustomLogAdapter.Assign(Source:  TPersistent);
begin
  if  not(Source is TGSkCustomLogAdapter)  then
    inherited
end { Assign };


procedure  TGSkCustomLogAdapter.BeforeDestruction();
begin
  inherited;
  if  fEndSessionMsg <> nil  then
    try
      CallAbstractMethod(BeginLog, Self, 'BeginLog');
      try
        if  mfLogTime in fEndSessionMsg.Flags  then
          fEndSessionMsg.MessageTime := Now();
        LogMessage(fEndSessionMsg)
      finally
        CallAbstractMethod(EndLog, Self, 'EndLog')
      end { try-finally }
    finally
      FreeAndNil(fEndSessionMsg)
    end { try-finally; fEndSessionMsg <> nil };
end { BeforeDestruction };


constructor  TGSkCustomLogAdapter.Create(Owner:  TComponent);
begin
  inherited
end { Create };


class function  TGSkCustomLogAdapter.EvalLogName(const   sName:  string;
                                                 const   bFull:  Boolean)
                                                 : string;
begin
  Result := sName;
  if  Result = ''  then
    begin
      Result := GetModuleFileName();
      if  not bFull  then
        Result := ExtractFileName(Result);
      Result := ChangeFileExt(Result, '')
    end { if Result = '' }
end { EvalLogName };


function  TGSkCustomLogAdapter.GetLogFullName() : string;
begin
  if  fsLogFullName = ''  then
    fsLogFullName := EvalLogName('', True);
  Result := fsLogFullName
end { GetLogFullName };


function  TGSkCustomLogAdapter.GetLogName() : string;
begin
  if  fsLogName = ''  then
    fsLogName := EvalLogName('', False);
  Result := fsLogName;
end { GetLogName };


procedure  TGSkCustomLogAdapter.LogMessage(const   Msg:  TGSkLogMessage);
begin
  if  mfSession in Msg.Flags  then
    begin
      Assert(fEndSessionMsg = nil);
      fEndSessionMsg := Msg.Clone();
      fEndSessionMsg.MessageText := Format(gcsSessionStop, [GetModuleFileName()]);
      fEndSessionMsg.Flags := fEndSessionMsg.Flags - [mfMemo, mfSession, mfInit]
    end { mfSession in Msg.Flags }
end { LogMessage };


procedure  TGSkCustomLogAdapter.Notification(AComponent:  TComponent;
                                             Operation:  TOperation);
begin
  inherited;
  if  (AComponent = fParent)
      and (Operation = opRemove)  then
    fParent := nil
end { Notification };


procedure  TGSkCustomLogAdapter.SetLogFullName(const   sName:  string);
begin
  fsLogFullName := EvalLogName(sName, True)
end { SetLogFullName };


procedure  TGSkCustomLogAdapter.SetLogName(const   sName:  string);
begin
  fsLogName := EvalLogName(sName, False)
end { SetLogName };


procedure  TGSkCustomLogAdapter.SetParent(const   Value:  TGSkLog);
begin
  if  fParent <> nil  then
    fParent.RemoveFreeNotification(Self);
  fParent := Value;
  if  fParent <> nil  then
    fParent.FreeNotification(Self)
end { SetParent };

{$ENDREGION 'TGSkCustomLogAdapter'}


{$REGION 'TGSkLogMessageMarker'}

constructor  TGSkLogMessageMarker.Create(const   nLogLevel, nLogOffset:  Cardinal);
begin
  inherited Create();
  fnLogLevel := nLogLevel;
  fnLogOffset := nLogOffset
end { Create };

{$ENDREGION 'TGSkLogMessageMarker'}


{$REGION 'TGSkLogMessage'}

function  TGSkLogMessage.Clone() : TGSkLogMessage;
begin
  Result := TGSkLogMessage.Create(MessageText, Prefix, MessageType,
                                  MessageOffset, ShowMessageTypes, Flags,
                                  MessageTime, ThreadID)
end { Clone };


{$IF CompilerVersion >= 22}  // XE

class constructor  TGSkLogMessage.Create();
begin
  fnThreadIDLen := 4   // by default, format ThreadID as 4-digit hex number
end { Create };

{$ELSE}

class procedure  TGSkLogMessage.ClassCreate();
begin
  fnThreadIDLen := 4   // by default, format ThreadID as 4-digit hex number
end { ClassCreate };

{$IFEND}


constructor  TGSkLogMessage.Create(const   sMessage, sPrefix:  string;
                                   const   mtMessageType:  TGSkLogMessageType;
                                   const   nOffset:  Cardinal;
                                   const   setShowTypes:  TGSkLogMessageTypes;
                                   const   setFlags:  TGSkLogMessageFlags;
                                   const   tmMessageTime:  TDateTime;
                                   const   nThreadID:  DWORD);
begin
  inherited Create();
  fsMessage := sMessage;
  fsPrefix := sPrefix;
  fmtMessageType := mtMessageType;
  fnMessageOffset := nOffset;
  fsetShowTypes := setShowTypes;
  fsetFlags := setFlags;
  {-}
  if  tmMessageTime <> 0.0  then
    ftmMessageTime := tmMessageTime
  else if  mfLogTime in setFlags  then
    ftmMessageTime := Now();
  {-}
  if  nThreadID <> High(DWORD)  then
    fnThreadID := nThreadID
  else if  mfLogThreadID in setFlags  then
    fnThreadID := GetCurrentThreadID()
end { Create };


function  TGSkLogMessage.FormatLogTime() : string;

var
  nY, nM, nD, nHr, nMn, nSc, nMs:  Word;

begin  { FormatLogTime }
  DecodeDateTime(ftmMessageTime, nY, nM, nD, nHr, nMn, nSc, nMs);
  Result := Format('%4.4d-%2.2d-%2.2d%3d:%2.2d:%2.2d.%3.3d',
                   [nY, nM, nD, nHr, nMn, nSc, nMs])
end { FormatLogTime };


function  TGSkLogMessage.FormatThreadID() : string;
begin
  if  ThreadID <> 0  then
    begin
      Result := Format('%*.4x', [fnThreadIDLen, ThreadID]);
      if  Length(Result) > fnThreadIDLen  then
        fnThreadIDLen := Length(Result)
    end { ThreadID <> 0 }
  else
    Result := Spaces(fnThreadIDLen)
end { FormatThreadID };


function  TGSkLogMessage.MessageToStr(const   bType, bTime, bThreadID, bPrefix:  Boolean)
                                       : string;
var
  sBuf:  string;

begin  { MessageToStr }
  if  (fsMessage = '')
      and (fmtMessageType = lmInformation)  then
    Result := ''
  else
    begin
      // X: yyy-mm-dd hh:mm:ss.ttt PREFIX Message
      if  bType  then
        Result := MessageTypeToChar() + ' '
      else
        Result := '';
      if  bTime  then
        begin
          sBuf := FormatLogTime() + ' ';
          if  ftmMessageTime = 0  then
            sBuf := StringOfChar(' ', Length(sBuf));
          Result := Result + sBuf
        end { if bTime };
      if  bThreadID  then
        Result := Result + FormatThreadID() + ' ';
      if  bPrefix
          and (fsPrefix <> '')  then
        Result := Result + fsPrefix + ' ';
      Result := TrimRight(Result
                            + StringOfChar(' ', fnMessageOffset mod cnMaxMessageOffset)
                            + fsMessage)
    end { fsMessage <> '' or fmtMessageType <> lmInformation }
end { MessageToStr };


class function  TGSkLogMessage.MessageTypeToChar(const   MessageType:  TGSkLogMessageType)
                                                 : Char;

const
  cMsgType:  array[TGSkLogMessageType]  of Char = (
               '·',   // lmDebugLowLevel
               '-',   // lmDebugDetails
               '—',   // lmDebug
               ' ',   // lmInformation
               'W',   // lmWarning
               'E');  // lmError

begin  { MessageTypeToChar }
  Result := cMsgType[MessageType]
end { MessageTypeToChar };


function  TGSkLogMessage.MessageTypeToChar() : Char;
begin
  {$IFDEF ShowLogLevel}
    Result := MessageTypeToChar(fmtMessageType)
  {$ELSE}
    if   fmtMessageType in fsetShowTypes  then
      Result := MessageTypeToChar(fmtMessageType)
    else
      Result := ' '
  {$ENDIF}
end { MessageTypeAsChar };

{$ENDREGION 'TGSkLogMessage'}


{$REGION 'TGSkLogMessages'}

procedure  TGSkLogMessages.AddMessage(const   LogMessage:  TGSkCustomLogMessage);
begin
  fMessages.Add(LogMessage)
end { AddMessage };


procedure  TGSkLogMessages.Clear();

var
  bReset:  Boolean;
  nInd:  Integer;

begin  { Clear }
  // Assert(fMessages.OwnsObjects);
  // fMessages.Clear()   // obiekty wiadomości zostaną zwolnione
  { Zdarzyło się, że w powyższej instrukcji wystąpił błąd. Nie udało mi się znaleźć
    przyczyny. Dlatego zastąpiłem ją poniższym kodem.
    Zwalniam obiekty z listy. Ewentualne błędy zwalniania obiektów ignoruję.
    Jeżeli wystąpił błąd, to resetuję listę obiektów. }
  bReset := False;
  for  nInd := fMessages.Count - 1  downto  0  do
    try
      fMessages.Delete(nInd)
    except
      bReset := True;
    end { try-except };
  if  bReset  then
    begin
      try
        Assert(not fMessages.OwnsObjects);
        fMessages.Free();
      except
        { OK }
      end { try-except };
      fMessages := TObjectList.Create(False)
    end { if bReset }
end { Clear };


constructor  TGSkLogMessages.Create();
begin
  inherited;
  fMessages := TObjectList.Create(True)
end { Create };


function  TGSkLogMessages.CustomMessageCount() : Integer;
begin
  Result := fMessages.Count
end { CustomMessageCount };


destructor  TGSkLogMessages.Destroy();
begin
  Clear();
  fMessages.Free();
  inherited
end { Destroy };


function  TGSkLogMessages.FirstMessage() : TGSkLogMessage;

var
  nInd:  Integer;
  nCnt:  Integer;

begin  { FirstMessage }
  nCnt := fMessages.Count;
  nInd := 0;
  while  (nInd < nCnt)
         and IsLogMessageMarker(nInd)  do
    Inc(nInd);
  if  nInd >= nCnt  then
    Result := nil
  else
    Result := fMessages.Items[nInd] as TGSkLogMessage;
  fnCurrMsg := nInd
end { FirstMessage };


function  TGSkLogMessages.HasMassages() : Boolean;
begin
  Result := fMessages.Count > 0
end { HasMassages };


function  TGSkLogMessages.IsEmpty() : Boolean;
begin
  Result := fMessages.Count = 0
end { IsEmpty };


function  TGSkLogMessages.IsLogMessageMarker(const   nIndex:  Integer)
                                             : Boolean;
begin
  Result := fMessages.Items[nIndex] is TGSkLogMessageMarker
end { IsLogMessageMarker };


function  TGSkLogMessages.LastMessage() : TGSkLogMessage;

var
  nInd:  Integer;

begin  { LastMessage }
  nInd := fMessages.Count - 1;
  while  (nInd >= 0)
         and IsLogMessageMarker(nInd)  do
    Dec(nInd);
  if  nInd < 0  then
    Result := nil
  else
    Result := fMessages.Items[nInd] as TGSkLogMessage
end { LastMessage };


function  TGSkLogMessages.LastMessageLogLevel(var   nOffset:  Cardinal)
                                              : Cardinal;
var
  lMsg:  TGSkCustomLogMessage;

begin  { LastMessageLogLevel }
  Result := 0;
  with  fMessages  do
    if  Count > 0  then
      begin
        lMsg := TGSkCustomLogMessage(Items[Count - 1]);
        if  lMsg is TGSkLogMessageMarker  then
          with  TGSkLogMessageMarker(lMsg)  do
            begin
              nOffset := LogOffset;
              Result := LogLevel
            end { with; lMsg is TGSkLogMessageMarker }
      end { Count > 0 }
end { LastMessageLogLevel };


function  TGSkLogMessages.LogMessage(const   nIndex:  Integer)
                                     : TGSkLogMessage;
begin
  Result := fMessages.Items[nIndex] as TGSkLogMessage
end { LogMessage };


function  TGSkLogMessages.NextMessage() : TGSkLogMessage;
begin
  Result := nil;
  Inc(fnCurrMsg);
  if  fnCurrMsg < fMessages.Count  then
    Result := fMessages.Items[fnCurrMsg] as TGSkLogMessage
  else
    Dec(fnCurrMsg)
end { NextMessage };


procedure  TGSkLogMessages.RemoveLastMessage();
begin
  Assert(fMessages.Count <> 0);
  fMessages.OwnsObjects := True;
  fMessages.Delete(fMessages.Count - 1);   // obiekt jest automatycznie zwalniany
  fMessages.OwnsObjects := False
end { RemoveLastMessage };

{$ENDREGION 'TGSkLogMessages'}


initialization
  gLogObjects := TObjectList.Create(False);
  gStdAssertProc := AssertErrorProc;
  AssertErrorProc := LogAssertError;
  {$IF CompilerVersion < 22} // XE
    TGSkLogMessage.ClassCreate();
  {$IFEND}


finalization
  AssertErrorProc := gStdAssertProc;
  gLogObjects.Free()


end.
