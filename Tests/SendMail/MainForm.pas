unit MainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  IdReplySMTP, IdStack;

type
  TfrmMain = class(TForm)
    btnSend: TButton;
    btnClose: TButton;
    procedure btnCloseClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses
  GSkPasLib.SendMail;

{$R *.dfm}

procedure TfrmMain.btnCloseClick(Sender: TObject);
begin
  Close
end;

procedure TfrmMain.btnSendClick(Sender: TObject);

var
  lMail:  TGSkSendMail;
  lBody:  TGSkMailBodyBuilders;
  lTmp:   TStringList;

begin
  lBody := TGSkMailBodyBuilders.Create();
  try
    { Prepare message body }
    lBody.Header(1, 'Nag��wek nr 1')
         .Paragraph('To be or not to be...')
         .Header(2, 'Podtytul')
         .Paragraph('oto jest pytanie!', 1)
         .HorizontalLine()
         .Header(3, 'Lista wypunktowana')
         .UnorderedListStart()
         .ListItem('Pierwszy')
         .ListItem('Drugi')
         .ListItem('Trzeci')
         .UnorderedListEnd()
         .Header(3, 'Lista numerowana')
         .OrderedListStart()
         .ListItem('Pierwsza')
         .ListItem('Druga')
         .ListItem('Trzecia')
         .OrderedListEnd()
         .Paragraph('Taki test...')
         .Paragraph('(wi�cej nie wymy�li�em)');

    { For message structure inspection }
    lTmp := TStringList.Create();
    try
      lTmp.Text := lBody.HTML;
      lTmp.SaveToFile('Message.html', TEncoding.UTF8);
      lTmp.Text := lBody.MarkDown;
      lTmp.SaveToFile('Message.md', TEncoding.UTF8);
    finally
      lTmp.Free()
    end { try-finally };

    { Send e-mail }
    lMail := TGSkSendMail.Create('poczta.jantar.pl', 'gskoczylas', 'WKgbmrm2412',
                                 '"Grze�ko" <gskoczylas@jantar.pl>');
    try
{}{}
      lMail.Recipients.Add('gskoczylas@gmail.com');
      lMail.CC.Add('gskoczylas@tangomocja.com');
{}
      lMail.BodyHTML := lBody.HTML;
      lMail.BodyMarkDown := lBody.MarkDown;
      lMail.Subject := 'Test wysy�ania poczty';
      try
        lMail.SendMail()
      except
        //on  eSocked : EidSockedError  do
        on  eRepSMTP : EIdSMTPReplyError  do
          MessageDlg(Format('IdReplySMTP %d: %s - %s',
                            [eRepSMTP.ErrorCode, eRepSMTP.Message,
                             eRepSMTP.EnhancedCode.ReplyAsStr]),
                     mtWarning, [mbOK], 0);
        on  eSocket : EIdSocketError  do
          MessageDlg(Format('EIdSocketError %d: %s',
                            [eSocket.LastError, eSocket.Message]),
                     mtWarning, [mbOK], 0);
        on  eErr : Exception  do
          MessageDlg(eErr.ClassName() + ': ' + eErr.Message,
                     mtError, [mbOK], 0);
      end;
    finally
      lMail.Free()
    end { try-finally }
  finally
    lBody.Free()
  end;
end;

end.

