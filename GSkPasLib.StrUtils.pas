﻿{: @abstract Podprogramy działające na napisach.

   @seealso(GSkPasLib.Tokens) }
unit  GSkPasLib.StrUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ————————————————————————————————       *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *

 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    WinAPI.Windows, System.StrUtils, System.Math, System.SysUtils, System.Classes,
    System.Types, System.AnsiStrings;
    {$LEGACYIFEND ON}
  {$ELSE}
    Windows, StrUtils, Math, SysUtils, Classes,
    Types;
  {$IFEND}


type
  {: @abstract Różne standardy kodowania polskich liter diakrytycznych.

     @value(cp1250    Kodowanie ANSI / Windows-1250.)
     @value(cp852     Kodowanie OEM / DOS-852 / Latin 2.)
     @value(cp8859_2  Kodowanie ISO-8859-2.)
     @value(cpMazovia Kodowanie MAZOVIA.)
     @value(cpASCII   Kodowanie ASCII; polskie litery zamienione na ich
                      łacińskie odpowiedniki bez „gonków”.)

     @seeAlso ConvertCP }
  TPolishCodePage = (cp1250, cp852, cp8859_2, cpMazovia, cpASCII);

  {: Flagi sterujące działaniem funkcji @link(IntToBin).

     @value(bfGrouping Binarna wartość jest grupowana po 4 cyfry.)
     @value(bfTrimEmptyGroups Z wyniku usuwane są początkowe grupy składające się
                              z czterech zer.)
     @value(bfTrimLeadingZeros Z wyniku usuwane są początkowe zera.)

     @seeAlso(TIntToBinFlags)
     @seeAlso(IntToBin) }
  TIntToBinFlag = (bfGrouping, bfTrimEmptyGroups, bfTrimLeadingZeros);
  {: Flagi sterujące działaniem funkcji @link(IntToBin).
     @seeAlso(TIntToBinFlag)
     @seeAlso(IntToBin) }
  TIntToBinFlags = set of  TIntToBinFlag;

  {: @abstract Baza konwersji liczb na napis.

     Parametr typu @name występuje w funkcjach @link(NumberToStr) oraz
     @link(StrToNumber). }
  TNumberToStrBase = 2..94;   // Ord(#126) - Ord(' ')

  TTrimMode = (tmLeft, tmRight);
  TTrimModes = set of  TTrimMode;


const
  {: @abstract Alias strony kodowej WINDOWS-1250 / ANSI. }
  cpWindows   = cp1250;
  {: @abstract Alias strony kodowej WINDOWS-1250 / ANSI. }
  cpANSI      = cp1250;
  {: @abstract Alias strony kodowej DOS-852 / Latin 2. }
  cpLatin2    = cp852;
  {: @abstract Alias strony kodowej DOS-852 / Latin 2. }
  cpIBM852    = cp852;
 {: @abstract Alias strony kodowej ISO-8859-2. }
  cpIsoLatin2 = cp8859_2;


{: @abstract(Konwersja polskich liter diakrytycznych między różnymi standardami
             kodowania.)

   @param sText  Tekst poddawany konwersji.
   @param cpFrom Źródłowy standard kodowania.
   @param cpTo   Docelowy standard kodowania.

   @returns(Wynikiem funkcji jest napis przekazany w parametrze @code(sText),
            w którym wszystkie polskie litery diakrytyczne (to te z „gonkami”)
            zakodowane w standardzie @code(cpFrom) są zastąpione ich
            odpowiednikami zakodowanymi w standardzie @code(cpTo).
            @br @br
            @bold(Uwaga!) Funkcja @name konwertuje tylko polskie litery
            diakrytyczne. Litery innych języków pozostają nie zmienione.) }
function  ConvertCP(const   sText:  AnsiString;
                    const   cpFrom, cpTo:  TPolishCodePage)
                    : AnsiString;                                    overload;

{: @abstract(Konwersja polskich liter diakrytycznych między różnymi standardami
             kodowania.)

   @param chChar Znak poddawany konwersji.
   @param cpFrom Źródłowy standard kodowania.
   @param cpTo   Docelowy standard kodowania.

   @returns(Wynikiem funkcji jest znak przekazany w parametrze @code(chChar),
            zamieniony na jego odpowiednik w docelowym standardzie kodowania.
            @bold(Uwaga!) Funkcja @name konwertuje tylko polskie litery
            diakrytyczne. Litery innych języków pozostają nie zmienione.) }
function  ConvertCP(const   chChar:  AnsiChar;
                    const   cpFrom, cpTo:  TPolishCodePage)
                    : AnsiChar;                                      overload;

{: @abstract(Konwersja tekstu ze standardu kodowania stosowanego w Windows
             na standard OEM.)

   Funkcja @name przekodowuje tekst źródłowy na tekst zakodowany według standardu
   OEM. Na przykład jeżeli w Windows ustawiona zostanie lokalizacja jako
   @italic(Polska) to funkcja przekoduje tekst ze standardu Windows-1250 na
   CP-852 (Latin 2).

   @seeAlso(OEMToString) }
function  StringToOEM(const   sText:  string)
                      : AnsiString;

{: @abstract(Konwersja tekstu ze standardu kodowania OEM na standard kodowania
             stosowany w Windows.)

   Funkcja @name przekoduje tekst źródłowy zakodowany według standardu OEM na
   tekst zakodowany według standardu Windows. Na przykład jeżeli w Windows
   ustawiona zostanie lokalizacja jako @italic(Polska) to funkcja przekoduje
   tekst ze standardu CP-852 (Latin 2) na Windows-1250.

   @seeAlso(StringToOEM) }
function  OEMToString(const   sOEM:  AnsiString)
                      : string;


{: @abstract Uzupełnia napis poprzedzając go wskazanym znakiem z lewej strony.

   @param(sText Tekst źródłowy. )
   @param(nNewLength Długość napisu wynikowego. )
   @param(cFillChar Znak, jakim będzie uzupełniany tekst źródłowy. )
   @param(bForceLength Jeżeli jest ustawiony i napis źródłowy jest zbyt długi to
                       zostanie on obcięty do @code(nNewLength) początkowych
                       znaków. )

   @returns(Tekst @code(sTekst) uzupełniony z lewej strony znakami
            @code(cFillChar) do długości @code(nNewLength). Jeżeli ten tekst
            jest dłuższy to, w zależności od parametru @code(bForceLength),
            albo oryginalny napis, albo pierwsze @code(nNewLength) znaków tego
            napisu.)

   @seealso(JustLeft) }
function  PadLeft(const   sText:  string;
                  const   nNewLength:  Integer;
                  const   cFillChar:  Char = ' ';
                  const   bForceLength:  Boolean = False)
                  : string;

{: @abstract Uzupełnia napis poprzedzając go wskazanym znakiem z prawej strony.

   @param(sText Tekst źródłowy. )
   @param(nNewLength Długość napisu wynikowego. )
   @param(cFillChar Znak, jakim będzie uzupełniany tekst źródłowy. )
   @param(bForceLength Jeżeli parametr jest ustawiony i napis źródłowy jest zbyt
                       długi to napis wynikowy zostanie on obcięty do @code(nNewLength)
                       początkowych znaków. )

   @returns(Tekst @code(sTekst) uzupełniony z prawej strony znakami
            @code(cFillChar) do długości @code(nNewLength). Jeżeli ten tekst
            jest dłuższy to, w zależności od parametru @code(bForceLength),
            albo oryginalny napis, albo pierwsze @code(nNewLength) znaków tego
            napisu.)

   @seealso(JustRight) }
function  PadRight(const   sText:  string;
                   const   nNewLength:  Integer;
                   const   cFillChar:  Char = ' ';
                   const   bForceLength:  Boolean = False)
                   : string;

{: @abstract Uzupełnia napis wskazanym znakiem z obu stron.

   @param(sText Tekst źródłowy. )
   @param(nNewLength Długość napisu wynikowego. )
   @param(cFillChar Znak, jakim będzie uzupełniany tekst źródłowy. )
   @param(bForceLength Jeżeli parametr jest ustawiony i napis źródłowy jest zbyt
                       długi to napis wynikowy zostanie on obcięty do @code(nNewLength)
                       początkowych znaków. )

   @returns(Tekst @code(sTekst) uzupełniony z obu stron znakami
            @code(cFillChar) do długości @code(nNewLength). Jeżeli ten tekst
            jest dłuższy to, w zależności od parametru @code(bForceLength),
            albo oryginalny napis, albo pierwsze @code(nNewLength) znaków tego
            napisu.)

   @seealso(JustCenter) }
function  PadCenter(const   sText:  string;
                    const   nNewLength:  Integer;
                    const   cFillChar:  Char = ' ';
                    const   bForceLength:  Boolean = False)
                    : string;

{: @abstract Dosuwa napis do lewej strony.

   @param(sText Tekst źródłowy. )
   @param(nNewLength Długość napisu wynikowego. Jeżeli ma wartość zero to napis
                     wynikowy będzie miał taką samą długość jak napis źródłowy.)
   @param(cFillChar Znak, jakim będzie uzupełniany tekst źródłowy. )
   @param(bForceLength Jeżeli parametr jest ustawiony i napis źródłowy jest zbyt
                       długi to napis wynikowy zostanie on obcięty do @code(nNewLength)
                       początkowych znaków. )

   @returns(Tekst @code(sTekst) dosunięty do lewej strony i, w razie potrzeby,
            uzupełniony znakami @code(cFillChar) do długości @code(nNewLength).
            Jeżeli ten tekst jest dłuższy to, w zależności od parametru
            @code(bForceLength), albo oryginalny napis, albo pierwsze
            @code(nNewLength) znaków tego napisu.)

   @seealso(PadLeft) }
function  JustLeft(const   sText:  string;
                   const   nNewLength:  Integer = 0;
                   const   cFillChar:  Char = ' ';
                   const   bForceLength:  Boolean = False)
                   : string;

{: @abstract Dosuwa napis do prawej strony.

   @param(sText Tekst źródłowy. )
   @param(nNewLength Długość napisu wynikowego. Jeżeli ma wartość zero to napis
                     wynikowy będzie miał taką samą długość jak napis źródłowy.)
   @param(cFillChar Znak, jakim będzie uzupełniany tekst źródłowy. )
   @param(bForceLength Jeżeli jest ustawiony i napis źródłowy jest zbyt długi to
                       zostanie on obcięty do @code(nNewLength) początkowych
                       znaków. )

   @returns(Tekst @code(sTekst) dosunięty do prawej strony i, w razie potrzeby,
            uzupełniony znakami @code(cFillChar) do długości @code(nNewLength).
            Jeżeli ten tekst jest dłuższy to, w zależności od parametru
            @code(bForceLength), albo oryginalny napis, albo pierwsze
            @code(nNewLength) znaków tego napisu.)

   @seealso(PadRight) }
function  JustRight(const   sText:  string;
                    const   nNewLength:  Integer = 0;
                    const   cFillChar:  Char = ' ';
                    const   bForceLength:  Boolean = False)
                    : string;

{: @abstract Dosuwa napis do środka.

   @param(sText Tekst źródłowy. )
   @param(nNewLength Długość napisu wynikowego. Jeżeli ma wartość zero to napis
                     wynikowy będzie miał taką samą długość jak napis źródłowy.)
   @param(cFillChar Znak, jakim będzie uzupełniany tekst źródłowy. )
   @param(bForceLength Jeżeli jest ustawiony i napis źródłowy jest zbyt długi to
                       zostanie on obcięty do @code(nNewLength) początkowych
                       znaków. )

   @returns(Tekst @code(sTekst) dosunięty do środka i, w razie potrzeby,
            uzupełniony znakami @code(cFillChar) do długości @code(nNewLength).
            Jeżeli ten tekst jest dłuższy to, w zależności od parametru
            @code(bForceLength), albo oryginalny napis, albo pierwsze
            @code(nNewLength) znaków tego napisu.)

   @seealso(PadCenter) }
function  JustCenter(const   sText:  string;
                     const   nNewLength:  Integer = 0;
                     const   cFillChar:  Char = ' ';
                     const   bForceLength:  Boolean = False)
                     : string;

{: @abstract Usuwa z napisu początkowe znaki.

   @param(sText Analizowany tekst.)
   @param(cRemove Znak do usunięcia.)
   @returns(Wynikiem funkcji jest napis @code(sText) z usuniętymi wszystkimi
            początkowymi znakami @code(cRemove).) }
function  TrimLeftChar(const   sText:  string;
                       const   cRemove:  Char = ' ')
                       : string;                                     overload;

{: @abstract Usuwa z napisu początkowe znaki.

   @param(sText Analizowany tekst.)
   @param(setRemove Znaki do usunięcia.)
   @returns(Wynikiem funkcji jest napis @code(sText) z usuniętymi wszystkimi
            początkowymi znakami należącymi do zbioru @code(setRemove).) }
function  TrimLeftChar(const   sText:  string;
                       const   setRemove:  TSysCharSet)
                       : string;                                     overload;

{: @abstract Usuwa z napisu końcowe znaki.

   @param(sText Analizowany tekst.)
   @param(cRemove Znak do usunięcia.)
   @returns(Wynikiem funkcji jest napis @code(sText) z usuniętymi wszystkimi
            końcowymi znakami @code(cRemove).) }
function  TrimRightChar(const   sText:  string;
                        const   cRemove:  Char = ' ')
                        : string;                                    overload;

{: @abstract Usuwa z napisu końcowe znaki.

   @param(sText Analizowany tekst.)
   @param(setRemove Znaki do usunięcia.)
   @returns(Wynikiem funkcji jest napis @code(sText) z usuniętymi wszystkimi
            końcowymi znakami należącymi do zbioru @code(setRemove).) }
function  TrimRightChar(const   sText:  string;
                        const   setRemove:  TSysCharSet)
                        : string;                                    overload;

{: @abstract Usuwa z napisu początkowe i końcowe znaki.

   @param(sText Analizowany tekst.)
   @param(cRemove Znak do usunięcia.)
   @returns(Wynikiem funkcji jest napis @code(sText) z usuniętymi wszystkimi
            początkowymi i końcowymi znakami @code(cRemove).) }
function  TrimChar(const   sText:  string;
                   const   cRemove:  Char = ' ')
                   : string;                                         overload;

{: @abstract Usuwa z napisu początkowe i końcowe znaki.

   @param(sText Analizowany tekst.)
   @param(setRemove Znaki do usunięcia.)
   @returns(Wynikiem funkcji jest napis @code(sText) z usuniętymi wszystkimi
            początkowymi i końcowymi znakami należącymi do zbioru
            @code(setRemove).) }
function  TrimChar(const   sText:  string;
                   const   setRemove:  TSysCharSet)
                   : string;                                         overload;

{: @abstract Usuwa początkowe puste wiersze.

   Procedura usuwa z @code(Lines) wszystkie początkowe puste wiersze. }
procedure  TrimLinesBeginning(const   Lines:  TStrings);

{: @abstract Usuwa końcowe puste wiersze.

   Procedura usuwa z @code(Lines) wszystkie końcowe puste wiersze. }
procedure  TrimLinesEnding(const   Lines:  TStrings);

{: @abstract Usuwa początkowe i końcowe puste wiersze.

   Procedura usuwa z @code(Lines) wszystkie początkowe i końcowe puste wiersze.}
procedure  TrimLines(const   Lines:  TStrings);


{: @abstract Zwraca tekst w odmianie zgodnej z językiem polskim.

   Funkcja @name może służy do przedstawiania jakiegoć zwrotu w odmianie
   związanej z liczbą, na przykład:
   @longCode(#
     writeln(nRazem, ' ',
             Odmiana(nRazem, 'złoty', 'złote', 'złotych'));
   #)

   @param(nIle Wartość, dla której zostanie dobrany odpowiedni tekst.)
   @param(sInf1 Zwrot dla parametru @code(nIle = 1). )
   @param(sInf2 Zwrot dla parametru @code(nIle = 2). )
   @param(sInd5 Zwrot dla parametru @code(nIle = 5). )

   Niekiedy odmiana dla @code(nIle = 2) oraz @code(nIle = 5) jest taka sama.
   W takim przypadku parametr @code(sInf5) można pominąć.

   @returns(Wynikiem działania funkcji @name jest jeden z parametrów
            @code(sInf1), @code(sInf2) lub @code(sInf5) --- w zależności od
            wartości parametru @code(nIle).) }
function  Odmiana(nIle:  integer;
                  const  sInf1, sInf2:  string;
                  const  sInf5:  string = '')
                  : string;


{: @abstract Formatuje wilkość pamięci w postaci napisu.

   Przedstawia pamić w bajtach, kilobajtach, megabajtach, gigabajtach lub
   terabajtach. Dobiera optymalną jednostkę w zależności od wielkości pamięci.

   @param nMemory Ilość pamięci --- w bajtach.
   @param bTrimZero Wskazuje czy z wynikowego napisu usuwać nieznaczące zera.

   @returns Napis reprezentujący pamić. }
function  FormatMemory(const   nMemory:  Int64;
                       const   bTrimZero:  Boolean = False)
                       : string;


{: @abstract Sprawdza, czy wskazany tekst zawiera wskazane znaki.

   Funkcja @name sprawdza, czy w tekście @code(sText) występują znaki
   @code(setChars).

   @param(sText Sprawdzany tekst.)
   @param(setChars Zestaw znaków do sprawdzenia.)
   @param(bCheckAll Czy sprawdzać wszystkie znaki w tekście.)

   @returns(Wynik funkcji zależy od parametru @code(bCheckAll). Jeżeli ma on
            wartość @true, to funkcja sprawdza, czy napis @code(sText)
            zawiera wyłącznie znaki @code(setChars).

            Jeżeli parametr @code(bCheckAll) ma wartość @false, to funkcja
            sprawdza, czy w sprawdzanym takście występuje choć jeden spośród
            znaków @code(setChars).)


   @seeAlso(CountChars) }
function  TextContainsChars(const   sText:  string;
                            const   setChars:  TSysCharSet;
                            const   bCheckAll:  Boolean)
                            : Boolean;

{: @abstract Funkcja zwraca liczbę wystąpień znaków w tekście.

   @param(sText Tekst źródłowy.)
   @param(setChars Zestaw znaków do policzenia.)

   @returns(Wynikiem funkcji jest informacja, ile razy w tekście występują
            znaki ze wskazanego zbioru znaków.)

   @seeAlso(TextContainsChars)
   @seeAlso(CharsOnly) }
function  CountChars(const   sText:  string;
                     const   setChars:  TSysCharSet)
                     : Integer;


{: @abstract Ze wskazanego napisu zwraca tylko wskazane znaki.

   Funkcja @name zwraca tekst wskazany w parametrze, z którego usuwa wszystkie
   znak oprócz wskazanych.

   @param(sText Tekst źródłowy.)
   @param(setChars Znaki do pozostawienia.)

   @returns(Wynikiem funkcji jest napis wskazany w parametrze @code(sText),
            w którym pozostały tylko znaki wskazane w parametrze @code(setChars).)

   @longCode(#
     const   sNIP = 'PL 937-21-88-329';
     writeln(CharsOnly(sNIP, ['0'..'9']));   // '9372188329'
   #)

   @seeAlso(CharsBut)
   @seeAlso(TextContainsChars)
   @seeAlso(CountChars) }
function  CharsOnly(const   sText:  string;
                    const   setChars:  TSysCharSet)
                    : string;                                        overload;

{: @abstract Ze wskazanego napisu zwraca tylko wskazane znaki.

   Funkcja @name zwraca tekst wskazany w parametrze, z którego usuwa wszystkie
   znak oprócz wskazanych w drugim napisie.

   Tę formę funkcji należy używać wtedy, gdy chcemy z napisu usunąć znaki
   nie będące znakami ASCII.

   @param(sText Tekst źródłowy.)
   @param(sChars Znaki do pozostawienia.)

   @returns(Wynikiem funkcji jest napis wskazany w parametrze @code(sText),
            w którym pozostały tylko znaki wskazane w parametrze @code(sChars).)

   @seeAlso(CharsBut)
   @seeAlso(TextContainsChars)
   @seeAlso(CountChars) }
function  CharsOnly(const   sText:  string;
                    const   sChars:  string)
                    : string;                                        overload;


{: @abstract Ze wskazanego napisu zwraca wszystkie znaki oprócz wskazanych.

   Funkcja @name zwraca tekst wskazany w parametrze, z którego usuwa wszystkie
   wskazane znaki oprócz wskazanych.

   @param(sText Tekst źródłowy.)
   @param(setChars Znaki do pozostawienia.)

   @returns(Wynikiem funkcji jest napis wskazany w parametrze @code(sText),
            z pominięciem znaków wskazanych w parametrze @code(setChars).)

   @seeAlso(CharsOnly)
   @seeAlso(TextContainsChars) }
function  CharsBut(const   sText:  string;
                   const   setChars:  TSysCharSet)
                   : string;                                         overload;

{: @abstract Ze wskazanego napisu zwraca wszystkie znaki oprócz wskazanych.

   Funkcja @name zwraca tekst wskazany w parametrze, z którego usuwa wszystkie
   wskazane znaki oprócz znaków wskazanych przez drugi parametr.

   Tę formę funkcji należy używać wtedy, gdy chcemy z napisu usunąć znaki
   nie będące znakami ASCII.

   @param(sText Tekst źródłowy.)
   @param(sChars Znaki do pozostawienia.)

   @returns(Wynikiem funkcji jest napis wskazany w parametrze @code(sText),
            z pominięciem znaków wskazanych w parametrze @code(sChars).)

   @seeAlso(CharsOnly)
   @seeAlso(TextContainsChars) }
function  CharsBut(const   sText:  string;
                   const   sChars:  string)
                   : string;                                         overload;


{: @abstract Funkcja @name przedstawia wartość słownie.

   Funkcja @name zamienia wskazaną wartość na jej zapis słownie --- zgodnie z
   regułami języka polskiego.

   @param(nAmount Wartość podlegająca konwersji.)
   @param(bCurrency Domyślnie wartość wskazana w parametrze @code(nAmount)
                    traktowana jest jako kwota. Jeżeli parametr @name ma wartość
                    @false to konwersji podlega tylko częśś całkowita liczby.)

   Zależność wyniku działania funkcji od wartości parametru @code(bCurrency)
   ilustrują następujące przykłady:
   @longCode(#
     wart := 1234567.89;

     ShowMessage(InWords(wart));
     // Wyświetli '1 milion 234 tysiące 567 zł 89 gr'

     ShowMessage(InWords(wart, False));
     // Wyświetli '1 milion 234 tysiące 567'
   #)

   @returns(Wynikiem funkcji jest wskazana wartość zapisana słownie --- zgodnie
            z zasadami języka polskiego.) }
function  InWords(nAmount:  Currency;
                  const   bCurrency:  Boolean = True)
                  : string;


{: @abstract Ujmuje wskazany napis w separatory.

   Funkcja @name wstawia na początku i końcu separatory. Jeżeli znak separatora
   występuje wewnątrz napisu to jest zamieniany na podwojny separator. Znaki
   sterujące zapisywane są jako kod znaku.

   Działanie funkcji @name jest podobne do działania standardowej funkcji
   @code(QuotedStr) z modułu @code(SysUtils). Funkcja @name różni się tym, że
   można wskazać znak ogranicznika napisu. Domyślnie jest to cudzysłów, czyli
   tak samo jak w funkcji @code(QuotedStr).

   Przykłady:
   @longCode(#
     Write(QuoteStr('Firma informatyczna', '"'));   // "Firma informatyczna"
     Write(QuoteStr('Firma "Rekord"', '"'));        // "Firma ""Rekord"""
   #)

   @seeAlso(CondQuoteStr) }
function  QuoteStr(const   sText:  string;
                   const   chDelimiter:  Char = '''')
                   : string;

{: @abstract Warunkowo ujmuje wskazany napis w separatory.

   Funkcja @name działa tak samo jak funkcja @link(QuoteStr). Dodatkowo sprawdza
   czy w napisie występuje separator lub znaki sterujące. Jeżli w napisie nie ma
   ani znaków sterujących, ani znaku separatora to wynikiem funkcji jest
   oryginalny napis @code(sText).

   Przykłady:
   @longCode(#
     Write(CondQuoteStr('Firma informatyczna', '"'));   // Firma informatyczna
     Write(CondQuoteStr('Firma "Rekord"', '"'));        // "Firma ""Rekord"""
     Write(CondQuoteStr('', '"'));                      // ""
   #)

   @seeAlso(QuoteStr) }
function  CondQuoteStr(const   sText:  string;
                       const   chDelimiter:  Char = '''')
                       : string;

{: @abstract Konwersja liczby na ciąg cyfr binarnych.

   @param(nValue Wartość podlegająca konwersji.)
   @param(nDigits Liczba oczekiwanych cyfr binarnych. Jeżeli wynik zawiera mniej
                  cyfr, to są one uzupełniane nieznaczącymi zerami.)
   @param(setFlags @link(TIntToBinFlags Flagi) sterujące formatowaniem wyniku.)

   Przykłady: @longCode(#
     IntToBin($12) = '0001 0010'
     IntToBin($12, 8, False) = '00010010'
     IntToBin($12, 1) = '1 0010'
     IntToBin($12, 6) = '01 0010'  #)

   @seeAlso(BinToInt)
   @seeAlso(NumberToStr) }
function  IntToBin({const} nValue:  Byte;
                   const   nDigits:  Integer = 8;
                   const   setFlags:  TIntToBinFlags = [bfGrouping])
                   : string;                                         overload;

{: @abstract Konwersja liczby na ciąg cyfr binarnych.

   @param(nValue Wartość podlegająca konwersji.)
   @param(nDigits Liczba oczekiwanych cyfr binarnych. Jeżeli wynik zawiera mniej
                  cyfr, to są one uzupełniane nieznaczącymi zerami.)
   @param(setFlags @link(TIntToBinFlags Flagi) sterujące formatowaniem wyniku.)

   Przykłady: @longCode(#
     IntToBin($12) = '0000 0000 0001 0010'
     IntToBin($12, 8, False) = '0000000000010010'
     IntToBin($12, 1) = '1 0010'
     IntToBin($12, 6) = '01 0010'  #)

   @seeAlso(BinToInt)
   @seeAlso(NumberToStr) }
function  IntToBin(const   nValue:  Word;
                   {const} nDigits:  Integer = 16;
                   const   setFlags:  TIntToBinFlags = [bfGrouping])
                   : string;                                         overload;

{: @abstract Konwersja liczby na ciąg cyfr binarnych.

   @param(nValue Wartość podlegająca konwersji.)
   @param(nDigits Liczba oczekiwanych cyfr binarnych. Jeżeli wynik zawiera mniej
                  cyfr, to są one uzupełniane nieznaczącymi zerami.)
   @param(setFlags @link(TIntToBinFlags Flagi) sterujące formatowaniem wyniku.)

   Przykłady: @longCode(#
     IntToBin($12) = '0000 0000 0000 0000 0000 0000 0001 0010'
     IntToBin($12, 8, False) = '00000000000000000000000000010010'
     IntToBin($12, 1) = '1 0010'
     IntToBin($12, 6) = '01 0010'  #)

   @seeAlso(BinToInt)
   @seeAlso(NumberToStr) }
function  IntToBin(const   nValue:  LongWord;
                   {const} nDigits:  Integer = 32;
                   const   setFlags:  TIntToBinFlags = [bfGrouping])
                   : string;                                         overload;

{: @abstract Konwersja liczby na ciąg cyfr binarnych.

   @param(nValue Wartość podlegająca konwersji.)
   @param(nDigits Liczba oczekiwanych cyfr binarnych. Jeżeli wynik zawiera mniej
                  cyfr, to są one uzupełniane nieznaczącymi zerami.)
   @param(setFlags @link(TIntToBinFlags Flagi) sterujące formatowaniem wyniku.)

   Przykłady: @longCode(#
     IntToBin($12) = '0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0001 0010'
     IntToBin($12, 8, False) = '0000000000000000000000000000000000000000000000000000000000010010'
     IntToBin($12, 1) = '1 0010'
     IntToBin($12, 6) = '01 0010'  #)

   @seeAlso(BinToInt)
   @seeAlso(NumberToStr) }
function  IntToBin(const   nValue:  UInt64;
                   {const} nDigits:  Integer = 64;
                   const   setFlags:  TIntToBinFlags = [bfGrouping])
                   : string;                                         overload;

{: @abstract Konwersja ciągu cyfr binarnych na liczbę

   Funkcja @name analizuje wskazany napis jako liczbę w zapisie binarnym.
   Analizowane są wyłącznie znaki @code('0') oraz @code('1'). Pozostałe znaki
   są ignorowane.

   Przykłady: @longCode(#
     BinToInt('10010') = $12
     BinToInt('1 0010') = $12
     BinToInt('0001-0010-0011-0100') = $1234
     BinToInt('Tekst bez cyfr') = $0  #)

   @seeAlso(IntToBin)
   @seeAlso(StrToNumber) }
function  BinToInt(const   sValue:  string)
                   : Int64;

{: @abstract Konwersja liczby na napis danej podstawie.

   @param(nValue Liczba poddawana konwersji.)
   @param(nBase Baza konwersji.)
   @param(nDigits Minimalna liczba cyfr wyniku. W razie potrzeby wynikowy napis
                  zostanie uzupełniony nieznaczącymi zerami.)

   Jeżeli baza jest mniejsza lub równa 10, to wynikiem jest napis składający się
   z ciągu cyfr.

   Jeżeli baza jest mniejsza lub równa 36, to wynikiem jest napis składający się
   z ciągu cyfr i małych liter alfabetu łacińskiego.

   Jeżeli baza jest większa niż 36, to wynikiem jest napis składający się ze znaków
   od „0” wzwyż.

   Dla bazy równej „2” winikiem funkcji jest taki sam napis, jaki jest wynikiem
   wywołania funkcji @code(IntToBin(nValue, 64, [bfTrimLeadingZeros])).

   Dla bazy równej „10” wynikiem funkcji jest taki sam napis, jaki jest wynikiem
   wywołania standardowej funkcji @code(IntToStr(nValue)).

   @seeAlso(StrToNumber)
   @seeAlso(IntToBin) }
function  NumberToStr({const} nValue:  UInt64;
                      const   nBase:  TNumberToStrBase;
                      const   nDigits:  Byte = 0)
                      : string;

{: @abstract Konwersja napisu na liczbę o zadanej bazie.

   Jeżeli baza jest równa „10”, to efekt działania tej funkcji jest taki sam,
   jak wynik wywołania standardowej funkcji @code(StrToInt(sValue)).

   @seeAlso(NumberToStr)
   @seeAlso(BinToInt)
   @seeAlso(StrToCurrency) }
function  StrToNumber(const   sValue:  string;
                      const   nBase:  TNumberToStrBase)
                      : UInt64;

{: @abstract Konwersja napisu na wartość typu Currency.

   Funkcja @name zamienia napis na wartość typu @code(Currency). Napis może
   zawierać kropkę lub przecinek dziesiętny.

   @seeAlso(StrToNumber) }
function  StrToCurrency(const   sValue:  string)
                        : Currency;

{: @abstract Informuje, czy tekst zawiera poprawną liczbę całkowitą. }
function  IsValidInteger(const   sText:  string)
                         : Boolean;                                  inline;

{: @abstract Informuje, czy tekst zawiera poprawną liczbę całkowitą typu @code(Int64). }
function  IsValidInt64(const   sText:  string)
                       : Boolean;                                    inline;

{: @abstract Funkcja przekształca napisy wskazane w parametrze na dynamiczną tablicę napisów. }
function  MakeStringDynArray(const   sData:  array of string)
                             : TStringDynArray;

{: @abstract Funkcja zwraca napis składający się ze wskazanej liczby odstępów. }
function  Spaces(const   nCount:  Integer)
                 : string;                                           inline;

{: @abstract Zamienia wszystkie apostrofy na parę apostrofów. }
function  StrToSQL(const   sText:  string;
                   const   setTrimMode:  TTrimModes = [tmLeft, tmRight];
                   const   bQuote:  Boolean = True)
                   : string;                                         overload;
function  StrToSQL(const   sText:  string;
                   const   bQuote:  Boolean)
                   : string;                                         overload; inline;

{: @abstract Zamienia napis @code(AnsiString) na @code(UnicodeString).

   Standardowa konwersja zmienia znaki z przedziału $80 do $A0 na inne znaki Unicode.
   Funkcja @name przywraca oryginalne znaki z tego przedziału. }
function  AnsiStringToString(const   sInput:  AnsiString)
                             : string;

{: @abstract Sprawdza, czy wszystkie znaki należą do wskazanego zbioru/

   Funkcja @name sprawdza, czy wszystkie znaki ze wskazanego napisu są ze wskazanego
   zbioru znaków. }
function  AreAllCharsFromSet(const   sData:  string;
                             const   setChars:  TSysCharSet)
                             : Boolean;

implementation


uses
  GSkPasLib.ASCII, GSkPasLib.PasLib;


const
  ConvArr:  array[TPolishCodePage] of  string[18] = (   // https://pl.wikipedia.org/wiki/Kodowanie_polskich_znaków


           // 'Ą'  + 'Ć'  + 'Ę'  + 'Ł'  + 'Ń'  + 'Ó'  + 'Ś'  + 'Ź'  + 'Ż'  + 'ą'  + 'ć'  + 'ę'  + 'ł'  + 'ń'  + 'ó'  + 'ś'  + 'ź'  + 'ż',
              #$a5 + #$c6 + #$ca + #$a3 + #$d1 + #$d3 + #$8c + #$8f + #$af + #$b9 + #$e6 + #$ea + #$b3 + #$f1 + #$f3 + #$9c + #$9f + #$bf,   // cp1250
              #164 + #143 + #168 + #157 + #227 + #224 + #151 + #141 + #189 + #165 + #134 + #169 + #136 + #228 + #162 + #152 + #171 + #190,   // cp852
              #161 + #198 + #202 + #163 + #209 + #211 + #166 + #172 + #175 + #177 + #230 + #234 + #179 + #241 + #243 + #182 + #188 + #191,   // cp8859_2
              #143 + #149 + #144 + #156 + #165 + #163 + #152 + #160 + #161 + #134 + #141 + #145 + #146 + #164 + #162 + #158 + #166 + #167,   // cpMazovia
              'A'  + 'C'  + 'E'  + 'L'  + 'N'  + 'O'  + 'S'  + 'Z'  + 'Z'  + 'a'  + 'c'  + 'e'  + 'l'  + 'n'  + 'o'  + 's'  + 'z'  + 'z');   // cpASCII


function  ConvertCP(const   sText:  AnsiString;
                    const   cpFrom, cpTo:  TPolishCodePage)
                    : AnsiString;
var
  nInd, nPos:  Integer;

begin { ConvertCP }
  Result := sText;
  if  cpFrom <> cpTo  then
    for  nInd := 1  to  Length(Result)  do
      begin
        nPos := Pos(Result[nInd], ConvArr[cpFrom]);
        if  nPos <> 0  then
          Result[nInd] := ConvArr[cpTo][nPos]
      end { for nInd }
end { ConvertCP };


function  ConvertCP(const   chChar:  AnsiChar;
                    const   cpFrom, cpTo:  TPolishCodePage)
                    : AnsiChar;
var
  nPos:  Integer;

begin  { ConvertCP }
  nPos := Pos(chChar, ConvArr[cpFrom]);
  if  nPos = 0  then
    Result := chChar
  else
    Result := ConvArr[cpTo][nPos]
end { ConvertCP };


function  StringToOEM(const   sText:  string)
                      : AnsiString;
begin
  if  sText <> ''  then
    begin
      SetLength(Result, Length(sText) * 2);
      CharToOem(PChar(sText), PAnsiChar(Result));
      {$IFDEF DEBUG}
        Assert(System.AnsiStrings.StrLen(PAnsiChar(Result)) <= Cardinal(Length(Result)));
      {$ENDIF DEBUG}
      SetLength(Result, System.AnsiStrings.StrLen(PAnsiChar(Result)))
    end { sText <> '' }
  else
    Result := ''
end { StringToOEM };


function  OEMToString(const   sOEM:  AnsiString)
                      : string;
begin
  SetLength(Result, Length(sOEM));
  if  sOEM <> ''  then
    OemToChar(PAnsiChar(sOEM), PChar(Result))
end { OEMToString };


function  PadLeft(const   sText:  string;
                  const   nNewLength:  Integer;
                  const   cFillChar:  Char;
                  const   bForceLength:  Boolean)
                  : string;
var
  nLen:  Integer;

begin  { PadLeft }
  nLen := Length(sText);
  if  nLen < nNewLength   then
    Result := StringOfChar(cFillChar, nNewLength - nLen)
              + sText
  else if  (nLen = nNewLength)
           or not bForceLength  then
    Result := sText
  else
    Result := LeftStr(sText, nNewLength)
end { PadLeft };


function  PadRight(const   sText:  string;
                   const   nNewLength:  Integer;
                   const   cFillChar:  Char;
                   const   bForceLength:  Boolean)
                   : string;
var
  nLen:  Integer;

begin  { PadRight }
  nLen := Length(sText);
  if  nLen < nNewLength  then
    Result := sText
              + StringOfChar(cFillChar, nNewLength - nLen)
  else if  (nLen = nNewLength)
           or not bForceLength  then
    Result := sText
  else
    Result := LeftStr(sText, nNewLength)
end { PadRight };


function  PadCenter(const   sText:  string;
                    const   nNewLength:  Integer;
                    const   cFillChar:  Char;
                    const   bForceLength:  Boolean)
                    : string;
var
  nLen:  Integer;

begin  { PadCenter }
  nLen := Length(sText);
  if  nLen < nNewLength  then
    begin
      Result := StringOfChar(cFillChar,
                             (nNewLength - nLen) div 2)
                + sText;
      Result := Result
                + StringOfChar(cFillChar,
                               nNewLength - Length(Result))
    end { nLen < nNewLength }
  else if  (nLen = nNewLength)
           or not bForceLength  then
    Result := sText
  else
    Result := LeftStr(sText, nNewLength)
end { PadCenter };


function  JustLeft(const   sText:  string;
                   const   nNewLength:  Integer;
                   const   cFillChar:  Char;
                   const   bForceLength:  Boolean)
                   : string;
begin
  Result := PadRight(Trim(sText),
                     IfThen(nNewLength <> 0, nNewLength, Length(sText)),
                     cFillChar, bForceLength)
end { JustLeft };


function  JustRight(const   sText:  string;
                    const   nNewLength:  Integer;
                    const   cFillChar:  Char;
                    const   bForceLength:  Boolean)
                    : string;
begin
  Result := PadLeft(Trim(sText),
                    IfThen(nNewLength <> 0, nNewLength, Length(sText)),
                    cFillChar, bForceLength)
end { JustRight };


function  JustCenter(const   sText:  string;
                     const   nNewLength:  Integer;
                     const   cFillChar:  Char;
                     const   bForceLength:  Boolean)
                     : string;
begin
  Result := PadCenter(Trim(sText),
                      IfThen(nNewLength <> 0, nNewLength, Length(sText)),
                      cFillChar, bForceLength)
end { JustCenter };


function  TrimLeftChar(const   sText:  string;
                       const   cRemove:  Char)
                       : string;
begin
  Result := TrimLeftChar(sText, [cRemove])
end { TrimLeftChar };


function  TrimLeftChar(const   sText:  string;
                       const   setRemove:  TSysCharSet)
                       : string;
begin
  Result := sText;
  while  (Result <> '')
         and CharInSet(Result[1], setRemove)  do
    Delete(Result, 1, 1)
end { TrimLeftChar };


function  TrimRightChar(const   sText:  string;
                        const   cRemove:  Char)
                        : string;
begin
  Result := TrimRightChar(sText, [cRemove])
end { TrimRightChar };


function  TrimRightChar(const   sText:  string;
                        const   setRemove:  TSysCharSet)
                        : string;
begin
  Result := sText;
  while  (Result <> '')
         and CharInSet(Result[Length(Result)], setRemove)  do
    SetLength(Result, Pred(Length(Result)))
end { TrimRightChar };


function  TrimChar(const   sText:  string;
                   const   cRemove:  Char)
                   : string;
begin
  Result := TrimChar(sText, [cRemove])
end { TrimChar };


function  TrimChar(const   sText:  string;
                   const   setRemove:  TSysCharSet)
                   : string;
begin
  Result := TrimRightChar(TrimLeftChar(sText, setRemove), setRemove)
end { TrimChar };


procedure  TrimLinesBeginning(const   Lines:  TStrings);
begin
  with  Lines  do
    while  (Count <> 0)
           and (Trim(Strings[0]) = '')  do
      Delete(0)
end { TrimLinesBeginning };


procedure  TrimLinesEnding(const   Lines:  TStrings);

var
  nLast:  Integer;

begin  { TrimLinesEnding }
  with  Lines  do
    begin
      nLast := Count - 1;
      while  (Count <> 0)
             and (Trim(Strings[nLast]) = '')  do
        begin
          Delete(nLast);
          Dec(nLast)
        end { while }
    end { with Lines }
end { TrimLinesEnding };


procedure  TrimLines(const   Lines:  TStrings);
begin
  TrimLinesBeginning(Lines);
  TrimLinesEnding(Lines)
end { TrimLines };


function  Odmiana(nIle:  integer;
                  const  sInf1, sInf2, sInf5:  string)
                  : string;
begin
  nIle := Abs(nIle);
  if  nIle = 1  then
    Result := sInf1
  else if  (sInf5 = '')
           or (((nIle mod 10) in [2..4])
               and not((nIle mod 100) in [12..14]))  then
    Result := sInf2
  else
    Result := sInf5
end { Odmiana };


function  FormatMemory(const   nMemory:  Int64;
                       const   bTrimZero:  Boolean)
                       : string;
const
  cn1kB = Int64(1024);
  cn1MB = Int64(1024) * cn1kB;
  cn1GB = Int64(1024) * cn1MB;
  cn1TB = Int64(1024) * cn1GB;

var
  sFmt:  string;
  sPrc:  string;
  nMem:  Extended;

begin  { FormatMemory }
  if  nMemory < cn1kB  then
    begin
      nMem := nMemory;
      sFmt := '';
      sPrc := ''
    end { B }
  else if  nMemory < cn1MB  then
    begin
      nMem := nMemory / cn1kB;
      sPrc := '.0';
      sFmt := 'k'
    end { kB }
  else if  nMemory < cn1GB  then
    begin
      nMem := nMemory / cn1MB;
      sPrc := '.0';
      sFmt := 'M'
    end { MB }
  else if  nMemory < cn1TB  then
    begin
      nMem := nMemory / cn1GB;
      sPrc := '.0';
      sFmt := 'G'
    end { GB }
  else
    begin
      nMem := nMemory / cn1TB;
      sPrc := '.0';
      sFmt := 'T'
    end { TB };
  Result := FormatFloat('###,##0' + sPrc, nMem);
  if  bTrimZero  then
    if  RightStr(Result, 2) = FormatSettings.DecimalSeparator + '0'  then
      Result := LeftStr(Result, Length(Result) - 2);
  Result := Result + ' ' + sFmt + 'B'
end { FormatMemory };


function  InWords(nAmount:  Currency;
                  const   bCurrency:  Boolean)
                  : string;
const
  cUnits1:  array[1..19] of string = (
              'jeden',
              'dwa',
              'trzy',
              'cztery',
              'pięć',
              'sześć',
              'siedem',
              'osiem',
              'dziewięć',
              'dziesięć',
              'jedenaście',
              'dwanaście',
              'trzynaście',
              'czternaście',
              'piętnaście',
              'szesnaście',
              'siedemnaście',
              'osiemnaście',
              'dziewiętnaście');
  cUnits2:  array[2..9] of string = (
              'dwadzieścia',
              'trzydzieści',
              'czterdzieści',
              'pięćdziesiąt',
              'sześćdziesiąt',
              'siedemdziesiąt',
              'osiemdziesiąt',
              'dziewięćdziesiąt');
  cUnits3:  array[1..9] of string = (
              'sto',
              'dwieście',
              'trzysta',
              'czterysta',
              'pięćset',
              'sześćset',
              'siedemset',
              'osiemset',
              'dziewięćset');

type
  TScope = array[1..3] of string;

const
  cNamesE3:  TScope = ('tysiąc',    //  1
                       'tysiące',   //  2..4, 22..24, 32..34, 42..44, ...
                       'tysięcy');  //  5..21, 25..31, 35..41, ...
  cNamesE6:  TScope = ('milion',
                       'miliony',
                       'milionów');
  cNamesE9:  TScope = ('miliard',
                       'miliardy',
                       'miliardów');
  cNamesE12: TScope = ('bilion',
                       'biliony',
                       'bilionów');

var
  nPos:  Integer;
  nAmountFract:  Integer;

  function  InWordsNNN(nAmount:  Cardinal)
                       : string;
  begin
    Assert(nAmount < 1000);
    if  nAmount >= 100  then
      Result := cUnits3[nAmount div 100]
    else
      Result := '';
    nAmount := nAmount mod 100;
    if  nAmount >= 20  then
      begin
        Result := Result + ' ' + cUnits2[nAmount div 10];
        nAmount := nAmount mod 10
      end { nAmount >= 20 };
    Assert(nAmount < 20);
    if  nAmount > 0  then
      Result := Result + ' ' + cUnits1[nAmount];
  end { InWordsNNN };

  function  ScopeName(nAmount:  Cardinal;
                      Names:  TScope)
                      : string;
  begin
    Assert(nAmount < 1000);
    if  nAmount = 0  then
      Result := ''
    else if  nAmount = 1  then
      Result := Names[1]
    else if  ((nAmount mod 10) in [2..4])
             and not ((nAmount mod 100) in [12..14])  then
      Result := Names[2]
    else
      Result := Names[3]
  end { ScopeName };

  procedure  ConvertScope(var   nAmount:  Currency;
                          nScope:  Currency;
                          NameOfScope:  TScope;
                          var   sOutput: string);
  var
    nTmp:  Cardinal;

  begin  { ConvertScope }
    nTmp := Trunc(nAmount / nScope);
    sOutput := sOutput + ' '
               + InWordsNNN(nTmp) + ' ' + ScopeName(nTmp, NameOfScope);
    nAmount := nAmount - nTmp * nScope
  end { ConvertScope };

begin  { InWords }
  if  nAmount >= 0  then
    begin
      nAmountFract := Round(Frac(nAmount)
                        * IntPower(10,
                                   {$IF CompilerVersion < gcnCompilerVersionDelphiXE}
                                     CurrencyDecimals
                                   {$ELSE}
                                     FormatSettings.CurrencyDecimals
                                   {$IFEND}));
      nAmount := Trunc(nAmount);
      if  nAmount = 0  then
        Result := 'zero'
      else
        begin
          Result := '';
          Assert(nAmount < 1E15);
          ConvertScope(nAmount, 1E12, cNamesE12, Result);   //  Biliony
          ConvertScope(nAmount, 1E9,  cNamesE9,  Result);   //  Miliardy
          ConvertScope(nAmount, 1E6,  cNamesE6,  Result);   //  Miliony
          ConvertScope(nAmount, 1E3,  cNamesE3,  Result);   //  Tysiące
          Result := Result + ' ' + InWordsNNN(Trunc(nAmount))
        end { nAmount <> 0 };
      Result := Trim(Result);
      nPos := Pos('  ', Result);
      while  nPos > 0  do
        begin
          Delete(Result, nPos, 1);
          nPos := Pos('  ', Result)
        end { while nPos > 0 };
      if  bCurrency  then
        Result := Format('%s zł %2.2u gr', [Result, nAmountFract])
    end { nAmount >= 0 }
  else
    Result := 'minus ' + InWords(-nAmount, bCurrency)
end { InWords };



function  TextContainsChars(const   sText:  string;
                            const   setChars:  TSysCharSet;
                            const   bCheckAll:  Boolean)
                            : Boolean;
var
  nInd:  Integer;

begin  { TextContainsChars }
  Result := False;
  if  bCheckAll  then
    for  nInd := 1  to  Length(sText)  do
      begin
        Result := CharInSet(sText[nInd], setChars);
        if  not Result  then
          Break
      end { for nInd }
  else
    for  nInd := 1  to  Length(sText)  do
      begin
        Result := CharInSet(sText[nInd], setChars);
        if  Result  then
          Break
      end { for nInd }
end { TextContainsChars };


function  CountChars(const   sText:  string;
                     const   setChars:  TSysCharSet)
                     : Integer;
var
  nInd:  Integer;

begin  { CountChars }
  Result := 0;
  for  nInd := 1  to  Length(sText)  do
    if  CharInSet(sText[nInd], setChars)  then
      Inc(Result)
end { CountChars };


function  CharsOnly(const   sText:  string;
                    const   setChars:  TSysCharSet)
                    : string;
var
  nInp, nOut:  Integer;

begin  { CharsOnly }
  SetLength(Result, Length(sText));
  nOut := 0;
  for  nInp := 1  to  Length(sText)  do
    if  CharInSet(sText[nInp], setChars)  then
      begin
        Inc(nOut);
        Result[nOut] := sText[nInp]
      end { if; for };
  SetLength(Result, nOut)
end { CharsOnly };


function  CharsOnly(const   sText:  string;
                    const   sChars:  string)
                    : string;
var
  nInd:  Integer;

begin  { CharsOnly }
  Result := sText;
  for  nInd :=  Length(Result)  downto  1  do
    if  StrScan(PChar(sChars), Result[nInd]) = nil  then
      Delete(Result, nInd, 1)
end { CharsOnly };


function  CharsBut(const   sText:  string;
                   const   setChars:  TSysCharSet)
                   : string;
var
  nInp, nOut:  Integer;

begin  { CharsBut }
  SetLength(Result, Length(sText));
  nOut := 0;
  for  nInp := 1  to  Length(sText)  do
    if  not (CharInSet(sText[nInp], setChars))  then
      begin
        Inc(nOut);
        Result[nOut] := sText[nInp]
      end { if; for };
  SetLength(Result, nOut)
end { CharsBut };


function  CharsBut(const   sText:  string;
                   const   sChars:  string)
                   : string;
var
  nInd:  Integer;

begin  { CharsBut }
  Result := sText;
  for  nInd :=  Length(Result)  downto  1  do
    if  StrScan(PChar(sChars), Result[nInd]) <> nil  then
      Delete(Result, nInd, 1)
end { CharsBut };



{$REGION 'QutoteStr'}

function  QuoteStr(const   sText:  string;
                   const   chDelimiter:  Char)
                   : string;
var
  nInd:  Integer;
  bInStr:  Boolean;

begin  { QuoteStr }
{ --> zamiast takiego kodu:
  Result := AnsiQuotedStr(sText, cDelimiter);
  Assert(Result[1] = cDelimiter);
  Assert(Result[Length(Result)] = cDelimiter);
  --> lepszy jest poniższy kod, bo ten wyżej nie zwraca uwagi na kody sterujące }
  if  sText = ''  then
    Result := chDelimiter + chDelimiter
  else
    begin
      Result := '';
      bInStr := False;
      for  nInd := 1  to  Length(sText)  do
        if  CharInSet(sText[nInd], gcsetControlCharsEx)  then
          begin
            if  bInStr  then
              begin
                Result := Result + chDelimiter;
                bInStr := False
              end { bInStr };
            Result := Format('%s#%d', [Result, Ord(sText[nInd])])
          end { sText[nInd] in gcsetControlChars }
        else
          begin
            if  not bInStr  then
              begin
                Result := Result + chDelimiter;
                bInStr := True
              end { not bInStr };
            Result := Result + sText[nInd];
            if  sText[nInd] = chDelimiter  then
              Result := Result + chDelimiter;
          end { sText[nInd] not in gcsetControlChars };
      if  bInStr  then
        Result := Result + chDelimiter
    end { sText <> '' }
end { QuoteStr };


function  CondQuoteStr(const   sText:  string;
                       const   chDelimiter:  Char)
                       : string;
begin
  if  (sText <> '')
      and ((sText[1] = ' ')
           or (sText[Length(sText)] = ' ')
           or TextContainsChars(sText,
                                gcsetControlChars + [chDelimiter],
                                False))  then
    if  CharInSet(chDelimiter, gcsetControlChars)  then
      Result := QuoteStr(sText)
    else
      Result := QuoteStr(sText, chDelimiter)
  else if  (sText = '')
           and (chDelimiter <> #0)  then
    Result := chDelimiter + chDelimiter
  else
    Result := sText
end { CondQuoteStr };

{$ENDREGION 'QutoteStr'}


{$REGION 'IntToBin'}

procedure  ProcessIntToBinResult(var     sValue:  string;
                                 const   setFlags:  TIntToBinFlags);
var
  nInd:  Integer;

begin  { ProcessIntToBinResult }
  if  bfGrouping in setFlags  then
    begin
      nInd := Length(sValue) - 3;
      while  nInd > 1  do
        begin
          Insert(' ', sValue, nInd);
          Dec(nInd, 4)
        end { while nInd > 1 }
    end { bfGrouping in setFlags };
  if  bfTrimEmptyGroups in setFlags  then
    while  StartsStr('0000 ', sValue)  do
      Delete(sValue, 1, 5);
  if  bfTrimLeadingZeros in setFlags  then
    begin
      nInd := 1;
      while  (nInd < Length(sValue))
             and CharInSet(sValue[nInd], ['0', ' '])  do
        Inc(nInd);
      if  nInd > 1  then
        Delete(sValue, 1, nInd - 1)
    end { bfTrimLeadingZeros in setFlags }
end { ProcessIntToBinResult };


function  IntToBin({const} nValue:  Byte;
                   const   nDigits:  Integer;
                   const   setFlags:  TIntToBinFlags)
                   : string;
var
  nInd:  Integer;

begin  { IntToBin }
  Result := StringOfChar('0', 8);
  for  nInd := 8  downto  1  do
    begin
      if  Odd(nValue)  then
        Result[nInd] := '1';
      nValue := nValue shr 1
    end { for nInd };
  if  nDigits in [0..7]  then
    begin
      nInd := Pos('1', LeftStr(Result, 8 - nDigits));
      if  nInd = 0  then
        nInd := 9 - nDigits;
      Delete(Result, 1, nInd - 1)
    end { nDigits in [0..7] };
  ProcessIntToBinResult(Result, setFlags)
end { IntToBin };


function  IntToBin(const   nValue:  Word;
                   {const} nDigits:  Integer;
                   const   setFlags:  TIntToBinFlags)
                   : string;
begin
  Result := IntToBin(WordRec(nValue).Hi, Max(nDigits - 8, 0), []);
  if  Result <> ''  then
    nDigits := 8;
  Result := Result + IntToBin(WordRec(nValue).Lo, nDigits, []);
  ProcessIntToBinResult(Result, setFlags)
end { IntToBin };


function  IntToBin(const   nValue:  LongWord;
                   {const} nDigits:  Integer;
                   const   setFlags:  TIntToBinFlags)
                   : string;
begin
  Result := IntToBin(LongRec(nValue).Hi, Max(nDigits - 16, 0), []);
  if  Result <> ''  then
    nDigits := 16;
  Result := Result + IntToBin(LongRec(nValue).Lo, nDigits, []);
  ProcessIntToBinResult(Result, setFlags)
end { IntToBin };


function  IntToBin(const   nValue:  UInt64;
                   {const} nDigits:  Integer;
                   const   setFlags:  TIntToBinFlags)
                   : string;
begin
  Result := IntToBin(Int64Rec(nValue).Hi, Max(nDigits - 32, 0), []);
  if  Result <> ''  then
    nDigits := 32;
  Result := Result + IntToBin(Int64Rec(nValue).Lo, nDigits, []);
  ProcessIntToBinResult(Result, setFlags)
end { IntToBin };


function  BinToInt(const   sValue:  string)
                   : Int64;
var
  nInd:  Integer;
  chDg:  Char;

begin  { BinToInt }
  Result := 0;
  for  nInd := 1  to  Length(sValue)  do
    begin
      chDg := sValue[nInd];
      case  chDg  of
        '0', '1':
          begin
            Result := Result shl 1;
            if  chDg = '1'  then
              Result := Result or $1;
          end { ['0', '1'] };
        ' ':
          { OK };
        else
          raise  EConvertError.CreateFmt('''%s'' is not correct binary number (char. #%d)',
                                         [sValue, nInd])
      end { case chDg }
    end { for nInd }
end { BinToInt };

{$ENDREGION 'IntToBin'}


function  NumberToStr({const} nValue:  UInt64;
                      const   nBase:  TNumberToStrBase;
                      const   nDigits:  Byte = 0)
                      : string;

var
  fnDigitToChar: TFunc<Byte, Char>;


  function  DigitToChar(const   nDigit:  Byte)
                        : Char;
  begin
    Result := Chr(Ord(Succ(' ')) + nDigit)
  end { DigitToChar };

begin  { NumberToStr }
  if  nBase <= 63   then
    fnDigitToChar := function(nDigit:  Byte)
                              : Char
                       begin
                         if  nDigit < 10  then
                           Result := Chr(Ord('0') + nDigit)
                         else if  nDigit < 36  then
                           Result := Chr(Ord('A') + nDigit - 10)
                         else
                           Result := Chr(Ord('a') + nDigit - 36)
                       end
  else
    fnDigitToChar := function(nDigit:  Byte)
                              : Char
                       begin
                         Result := Chr(Ord(' ') + 1 + nDigit)
                       end;
  {-}
  Result := '';
  repeat
    Result := fnDigitToChar(nValue mod nBase) + Result;
    nValue := nValue div nBase;
  until  nValue = 0;
  {-}
  if  nDigits > Length(Result)  then
    if  nBase <= 63  then
      Result := StringOfChar('0', nDigits - Length(Result))
                + Result
    else
      Result := StringOfChar(Succ(' '), nDigits - Length(Result))
                + Result
end { NumberToStr };


function  StrToNumber(const   sValue:  string;
                      const   nBase:  TNumberToStrBase)
                      : UInt64;
var
  nInd:  Integer;
  fnCharToDigit:  TFunc<Char, Byte>;

begin  { StrToNumber }
  if  nBase < 63 then
    fnCharToDigit := function(chValue:  Char)
                              : Byte
      begin
        case  chValue  of
          '0'..'9':
            Result := Ord(chValue) - Ord('0');
          'A'..'Z':
            Result := Ord(chValue) - Ord('A') + 10;
          else
            Result := Ord(chValue) - Ord('a') + 36
        end { case chValue }
      end
  else
    fnCharToDigit := function(chValue:  Char)
                              : Byte
      begin
        Result := Ord(chValue) - Ord(Succ(' '))
      end ;
  {-}
  Result := 0;
  for  nInd :=  1  to  Length(sValue)  do
    Result := Result * nBase + fnCharToDigit(sValue[nInd])
end { StrToNumber };


function  StrToCurrency(const   sValue:  string)
                        : Currency;
var
  sBuf:  string;

begin  { StrToCurrency }
  case  FormatSettings.DecimalSeparator  of
    '.':  sBuf := StringReplace(sValue, ',', '.', []);
    ',':  sBuf := StringReplace(sValue, '.', ',', []);
    else  sBuf := sValue
  end { case FormatSettings.DecimalSeparator };
  Result := StrToCurr(sBuf)
end { StrToCurrency };


function  IsValidInteger(const   sText:  string)
                         : Boolean;
var
  nDummy:  Integer;

begin  { IsValidInteger }
  Result := TryStrToInt(sText, nDummy)
end { IsValidInteger };


function  IsValidInt64(const   sText:  string)
                       : Boolean;
var
  nDummy:  Int64;

begin  { IsValidInt64 }
  Result := TryStrToInt64(sText, nDummy)
end { IsValidInt64 };


function  MakeStringDynArray(const   sData:  array of string)
                             : TStringDynArray;
var
  nInd:  Integer;

begin  { MakeStringDynArray }
  SetLength(Result, Length(sData));
  for  nInd := Low(sData)  to  High(sData)  do
    Result[nInd] := sData[nInd]
end { MakeStringDynArray };


function  Spaces(const   nCount:  Integer)
                 : string;
begin
  Result := StringOfChar(' ', nCount)
end { Spaces };


function  StrToSQL(const   sText:  string;
                   const   setTrimMode:  TTrimModes;
                   const   bQuote:  Boolean)
                   : string;
begin
  if  tmLeft in setTrimMode  then
    Result := TrimLeft(Result);
  if  tmRight in setTrimMode  then
    Result := TrimRight(Result);
  Result := StringReplace(sText, '''', '''''', [rfReplaceAll]);
  if  bQuote  then
    Result := '''' + Result + ''''
end { StrToSQL };


function  StrToSQL(const   sText:  string;
                   const   bQuote:  Boolean)
                   : string;
begin
  Result := StrToSQL(sText, [tmLeft, tmRight], bQuote)
end { StrToSQL };


function  AnsiStringToString(const   sInput:  AnsiString)
                             : string;
var
  nInd:  Integer;

begin  { AnsiStringToString }
  Result := UnicodeString(sInput);
  for  nInd := 1  to  Length(sInput)  do
    if  sInput[nInd] in [#$80..#$A0, #$FF]  then
      Result[nInd] := Chr(Ord(sInput[nInd]))
end { AnsiStringToString };


function  AreAllCharsFromSet(const   sData:  string;
                             const   setChars:  TSysCharSet)
                             : Boolean;
var
  nInd:  Integer;

begin  { AreAllCharsFromSet }
  Result := False;
  for  nInd :=  1  to  Length(sData)  do
    if  not CharInSet(sData[nInd], setChars)  then
      Exit;
  Result := True
end { AreAllCharsFromSet };


end.

