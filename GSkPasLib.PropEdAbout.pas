﻿{: @exclude
   Wewnętrznych modułów biblioteki nie komentuję (na razie).  }
unit  GSkPasLib.PropEdAbout;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  DesignIntf, DesignEditors, SysUtils;


type
  TPropEdAboutGSkPasLib = class(TPropertyEditor)
  public
    function  GetAttributes() : TPropertyAttributes;                 override;
    function  GetValue() : string;                                   override;
    function  GetName() : string;                                    override;
    procedure  Edit();                                               override;
  end { TPropEdAboutGSkPasLib };


implementation


uses
  GSkPasLib.PropEdAboutForm, GSkPasLib.PasLibInternals;


{ TPropertyAboutGSkPasLib }

procedure  TPropEdAboutGSkPasLib.Edit();

var
  sClass:  string;
  nInd:  Integer;

begin  { Edit };
  inherited;
  if  PropCount > 0  then
    begin
      sClass := GetComponent(0).ClassName;
      for  nInd := 1  to  PropCount - 1  do
        if  not GetComponent(nInd).ClassNameIs(sClass)  then
          begin
            sClass := '';
            Break
          end
    end { PropCount > 0 }
  else
    sClass := '';
  TPropEdAboutForm.Execute(sClass)
end { Edit };


function  TPropEdAboutGSkPasLib.GetAttributes() : TPropertyAttributes;
begin
  Result := [paDialog, paReadOnly];
end { GetAttributes };


function  TPropEdAboutGSkPasLib.GetName() : string;
begin
  Result := gcsAboutLib
end { GetName };


function  TPropEdAboutGSkPasLib.GetValue() : string;
begin
  Result := gcsAboutLib
end { GetValue };


end.
