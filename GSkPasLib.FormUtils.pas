﻿{: @abstract Moduł @name zawiera podprogramy związane z oknami.

   W tym module są funkcje związane z oknami oraz funkcje związane z aplikacją. }
unit  GSkPasLib.FormUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ————————————————————————————————       *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$Warn SYMBOL_PLATFORM off }


uses
  {$IF CompilerVersion > 22} // XE
    VCL.Forms, System.SysUtils, System.Classes, System.Types, VCL.Menus, VCL.ActnList,
    VCL.Controls, System.SyncObjs, WinAPI.Windows;
  {$ELSE}
    Forms, SysUtils, Classes, Types, Menus, ActnList,
    Controls, SyncObjs, Windows;
  {$IFEND}


const
  {: @exclude } mrNone     = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrNone;
  {: @exclude } mrOk       = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrOk;
  {: @exclude } mrCancel   = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrCancel;
  {: @exclude } mrAbort    = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrAbort;
  {: @exclude } mrRetry    = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrRetry;
  {: @exclude } mrIgnore   = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrIgnore;
  {: @exclude } mrYes      = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrYes;
  {: @exclude } mrNo       = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrNo;
  {: @exclude } mrAll      = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrAll;
  {: @exclude } mrNoToAll  = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrNoToAll;
  {: @exclude } mrYesToAll = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.mrYesToAll;


type
  {: @exclude } TModalResult = {$IF CompilerVersion > 22}VCL.{$IFEND}Controls.TModalResult;


{: @abstract Zwraca klucz Registry związany ze wskazanym oknem.

   @param(Form Okno, dla którego wyliczany jest klucz Registry.)
   @param(bWithScreenSize Wskazuje, czy klucz Registry ma zawiera również
                          aktualną rozdzielczość ekranu.)
   @param(sLastName Opcjonalna dodatkowa nazwa dołączana na końcu klucza
                    Registry.) }
function  GetFormRegistryKey(const   Form:  TComponent;
                             const   bWithScreenSize:  Boolean = False;
                             const   sLastName:  string = '')
                             : string;

{: @abstract Sprawdza położenie okna na ekranie.

   Procedura sprawdza czy wskazane okno jest całkowicie widoczne na ekranie.
   Jeżeli część okna wystaje poza ekran to przesuwa okno tak aby zmieściło się
   w całości. Jeżeli to nie wystarcza i obramowanie okna pozwala to zmienia
   rozmiary okna tak, aby w całości mieściło się na ekranie.

   Procedura uwzględnia obszar zajmowany przez systemowy pasek zadań. }
procedure  CheckFormPos(const   Form:  TCustomForm);


{: @abstract Zwraca współrzędne wskazanego okna. }
function  GetFormRect(const   Form:  TCustomForm)
                      : TRect;

{: @abstract Określa współrzędne wskazanego okna.

   Procedura definiuje współrzędne wskazanego okna. Następnie weryfikuje pozycję
   (i wymiary) okna przy pomocy procedury @link(CheckFormPos). }
procedure  SetFormRect(const   Form:  TCustomForm;
                       const   Rect:  TRect);

{: @abstract Ukrywa nieużywane pozycje menu.

   Procedura analizuje wskazaną pozycję menu oraz wszystkie podrzędne pozycje
   menu. Ukrywane są te pozycje menu, które nie mają zdefiniowanej obsługi
   zdarzenia @code(OnClick), nie mają zdefiniowanej właściwości @code(Action)
   oraz nie mają żadnej używanej pozycji menu podrzędnego.  }
procedure  HideUnusedMenuItems(const   mnMenuItem:  TMenuItem);

{: @abstract Szuka okna o wskazanej nazwie.

   Funkcja @name szuka wśród okien aplikacji okna o wskazanej nazwie.

   @param sFormName Nazwa szukanego okna.

   @returns(Wynikiem działania funkcji jest znalezione okno. Jeżeli w aplikacji
            nie ma okna o wskazanej nazwie to wynikiem funkcji jest @nil.)

   @seeAlso(FindDataModule)
   @seeAlso(CreateForm)
   @seeAlso(ExecForm)  }
function  FindForm(const   sFormName:  TComponentName)
                   : TForm;

{: @abstract Szuka modułu danych o wskazanej naziwe.

   Funkcja @name szuka wśród modułów danych aplikacji modułu o podanej nazwie.

   @param sDataModuleName Nazwa szukanego modułu danych.

   @returns(Wynikiem działania funkcji jest znaleziony moduł danych. Jeżeli
            w aplikacji niema modułu danych o wskazanej nazwie to wynikiem
            działania funkcji jest @nil.)

   @seeAlso(FindForm)
   @seeAlso(CreateDataModule)
   @seeAlso(ExecForm)  }
function  FindDataModule(const   sDataModuleName:  TComponentName)
                         : TDataModule;

{: @abstract Tworzy moduł danych --- jeżeli jeszcze nie istnieje.

   Procedura @name sprawdza czy w aplikacji istnieje już moduł danych o
   wskazanej nazwie. Jeżeli nie ma takiego modułu danych to go tworzy.

   @param(DataClass Klasa modułu danych, który należy utworzyć, jeżeli odpowiedni
                    moduł danych jeszcze nie istnieje w aplikacji.)
   @param(DataReference Wskazuje zmienną, pod którą będzie zapamiętany nowo
                        utworzony moduł danych.)
   @param(DataName Nazwa modułu danych --- jeżeli w aplikacji nie ma modułu
                   danych o takiej nazwie to zostanie utworzony moduł typu
                   @code(DataClass) i zapamiętany w zmiennej @code(DataReference).)
   @param(Owner Wskazuje właściciela tworzonego modułu danych. Jeżeli ten
                parametr nie zostanie wskazany lub ma wartość @nil to
                właścicielem tworzonego modułu danych będzie standardowy obiekt
                @code(Application).)

   @seeAlso(CreateForm)
   @seeAlso(ExecForm)
   @seeAlso(TzzGSkCustomDataModule)  }
procedure  CreateDataModule(        DataClass:     TComponentClass;
                            var     DataReference;
                            const   DataName:      TComponentName;
                                    Owner:         TComponent    = nil);

{: @abstract Tworzy okno i moduł danych --- jeżeli jeszcze nie istnieją.

   Procedura @name sprawdza czy w aplikacji istnieje okno o nazwie
   @code(FormName). Jeżeli takie okno nie istnieje to procedura utworzy okno
   typu @code(FormClass) i zapamięta go w zmiennej @code(Reference).

   Najpierw analizowany jest wskazany moduł danych. W drugiej kolejności
   sprawdzane jest wskazane okno.

   Jeżeli okno jest potomkiem klasy @link(TzzGSkCustomForm) to jego właściwość
   @link(TzzGSkCustomForm.DataModule) wskazuje moduł danych @code(DataReference).

   @param(FormClass Klasa okna, które należy utworzyć, jeżeli odpowiednie okno
                    jeszcze w aplikacji nie istnieje.)
   @param(Reference Jeżeli okno będzie tworzone to zostanie zapamiętane w
                    zmiennej @code(Reference).)
   @param(FormName Nazwa okna. Jeżeli takie okno w aplikacji nie istnieje to
                   zostanie utworzone okno klasy @code(FormClass) i zapamiętane
                   w @code(Reference).)
   @param(DataClass Klasa modułu danych, który należy utworzyć, jeżeli odpowiedni
                    moduł danych jeszcze w aplikacji nie istnieje.)
   @param(DataReference Jeżeli moduł danych będzie tworzony to zostanie
                        zapamiętany w zmiennej @code(DataReference).)
   @param(DataName Nazwa modułu danych. Jeżeli taki moduł w aplikacji nie
                   istnieje to zostanie utworzony moduł klasy @code(DataClass)
                   i zapamiętany w @code(DataReference).

                   Jeżeli parametr @code(DataName) jest pusty to procedura @name
                   szuka modułu o nazwie takiej samej jak nazwa okna (parametr
                   @code(FormName)), ale z przyrostkiem „@code(_Dane)”.)
   @param(Owner Właściciel nowo tworzonych obiektów (jeżeli są tworzone). Jeżeli
                parametr @code(Owner) nie został wskazany lub ma wartość @nil to
                właścicielem tworzonych obiektów jest standardowy obiekt
                @code(Application).)
   @param(CreateAction Akcja, której wykonanie spowodowało utworzenie okna lub
                       modułu danych. Jeżeli tworzone okno jest potomkiem
                       klasy @link(TzzGSkCustomForm) to jego właściwość
                       @link(TzzGSkCustomForm.CreateAction) wskazuje akcję
                       @code(CreateAction).)

   @seeAlso(CreateDataModule)
   @seeAlso(ExecForm)  }
procedure  CreateForm(        FormClass:     TComponentClass;   // TFormClass;
                      var     Reference;
                      const   FormName:      TComponentName;
                              DataClass:     TComponentClass;
                      var     DataReference;
                      const   DataName:     TComponentName = '';
                              Owner:         TComponent    = nil;
                      const   CreateAction:  TCustomAction = nil);   overload;

{: @abstract Tworzy okno --- jeżeli jeszcze nie istnieje.

   Procedura @name sprawdza czy w aplikacji istnieje okno o nazwie
   @code(FormName). Jeżeli takie okno nie istnieje to procedura utworzy okno
   typu @code(FormClass) i zapamięta go w zmiennej @code(Reference).

   @param(FormClass Klasa okna, które należy utworzyć, jeżeli odpowiednie okno
                    jeszcze w aplikacji nie istnieje.)
   @param(Reference Jeżeli okno będzie tworzone to zostanie zapamiętane w
                    zmiennej @code(Reference).)
   @param(FormName Nazwa okna. Jeżeli takie okno w aplikacji nie istnieje to
                   zostanie utworzone okno klasy @code(FormClass) i zapamiętane
                   w @code(Reference).)
   @param(Owner Właściciel nowo tworzonych obiektów (jeżeli są tworzone). Jeżeli
                parametr @code(Owner) nie został wskazany lub ma wartość @nil to
                właścicielem tworzonych obiektów jest standardowy obiekt
                @code(Application).)
   @param(CreateAction Akcja, której wykonanie spowodowało utworzenie okna lub
                       modułu danych. Jeżeli tworzone okno jest potomkiem
                       klasy @link(TzzGSkCustomForm) to jego właściwość
                       @link(TzzGSkCustomForm.CreateAction) wskazuje akcję
                       @code(CreateAction).)

   @seeAlso(CreateDataModule)
   @seeAlso(ExecForm)  }
procedure  CreateForm(        FormClass:     TComponentClass;   // TFormClass;
                      var     Reference;
                      const   FormName:      TComponentName;
                              Owner:         TComponent    = nil;
                      const   CreateAction:  TCustomAction = nil);   overload;

{: @abstract Wyświetla wskazane okno i przekazuje do niego sterowanie.

   Funkcja @name wyświetla wskazane okno.

   @param(Form Okno do wyświetlenia.)
   @param(bModal Wskazuje czy okno ma być modalne.)

   @returns(Jeżeli okno jest wyświetlane jako modalne to wynik funkcji jest taki
                   sam jak wynik standardowej funkcji @code(ShowModal).
                   W przeciwnym razie wynikiem jest standardowa wartość
                   @code(mrNone).)

   @seeAlso(CreateForm)
   @seeAlso(ExecForm)  }
function  ShowForm(const   Form:    TForm;
                   const   bModal:  Boolean = False)
                   : TModalResult;

{: @abstract Wyświetla wskazane okno i przekazuje do niego sterowanie.

   Funkcja @name najpierw sprawdza czy istnieje wskazane okno i moduł danych.
   W razie potrzeby tworzy je. Wykorzystuje do tego procedurę @link(CreateForm).

   Następnie wyświetla okno i przekazuje do niego sterowanie. Wykonuje to przy
   pomocy funkcji @link(ShowForm).

   Znaczenie parametrów jest takie samo jak w procedurze @link(CreateForm) i
   funkcji @link(ShowForm).

   Wynikiem funkcji jest taki sam jak wynik funkcji @link(ShowForm).

   @seeAlso(CreateForm)
   @seeAlso(ShowForm)  }
function  ExecForm(        FormClass:     TFormClass;
                   var     Reference;
                   const   FormName:      TComponentName;
                           DataClass:     TComponentClass;
                   var     DataReference;
                   const   DataName:      TComponentName= '';
                           Owner:         TComponent    = nil;
                   const   CreateAction:  TCustomAction = nil;
                   const   bShowModal:    Boolean       = False)
                   : TModalResult;                                   overload;

{: @abstract Wyświetla wskazane okno i przekazuje do niego sterowanie.

   Funkcja @name najpierw sprawdza czy istnieje wskazane okno i moduł danych.
   W razie potrzeby tworzy je. Wykorzystuje do tego procedurę @link(CreateForm).

   Następnie wyświetla okno i przekazuje do niego sterowanie. Wykonuje to przy
   pomocy funkcji @link(ShowForm).

   Znaczenie parametrów jest takie samo jak w procedurze @link(CreateForm) i
   funkcji @link(ShowForm).

   Wynikiem funkcji jest taki sam jak wynik funkcji @link(ShowForm).

   @seeAlso(CreateForm)
   @seeAlso(ShowForm)  }
function  ExecForm(        FormClass:     TFormClass;
                   var     Reference;
                   const   FormName:      TComponentName;
                           Owner:         TComponent    = nil;
                   const   CreateAction:  TCustomAction = nil;
                   const   bShowModal:    Boolean       = False)
                   : TModalResult;                                   overload;


implementation


uses
  GSkPasLib.CustomComponents, GSkPasLib.AppUtils, GSkPasLib.PasLib;


var
  gSynchro:  TCriticalSection;


function  GetFormRegistryKey(const   Form:  TComponent;
                             const   bWithScreenSize:  Boolean;
                             const   sLastName:  string)
                             : string;
var
  lSuperior:  TComponent;
  nInd:  Integer;

begin  { GetFormRegistryKey }
  Assert(Form <> nil);
  Result := '';
  lSuperior := Form.Owner;
  while  (lSuperior <> Application)
         and (lSuperior <> nil)  do
    begin
      if  lSuperior <> Application.MainForm  then
        Result := Format('\%s%s',
                         [TGSkCustomComponent.ComponentDisplayName(lSuperior),
                          Result]);
      lSuperior := lSuperior.Owner;
    end { while };
  Assert(Result <> '\');
  Result := GetAppRegistryKey() + Result;
  Result := Format('%s\%s',
                   [Result, TGSkCustomComponent.ComponentDisplayName(Form)]);
  if  bWithScreenSize  then
    begin
      with  Screen.Monitors[0]  do
        Result := Result + Format('\%dx%d', [Width, Height]);
      for  nInd := 1  to  Screen.MonitorCount - 1  do
        with  Screen.Monitors[nInd]  do
          Result := Result + Format('-%dx%d', [Width, Height])
    end { bWithScreenSize };
  if  sLastName <> ''  then
    Result := Format('%s\%s',
                     [Result, sLastName])
end { GetFormRegistryKey };


procedure  CheckFormPos(const   Form:  TCustomForm);

var
  lDesktop:  TRect;

begin  { CheckFormPos }
  with  Form  do
    begin
      lDesktop := Screen.MonitorFromPoint(Point(Left, Top)).WorkareaRect;
      if  BorderStyle in [bsSizeable, bsSizeToolWin]  then
        begin
          if  Width > lDesktop.Right  then
            Width := lDesktop.Right;
          if  Height > lDesktop.Bottom  then
            Height := lDesktop.Bottom
        end { BorderStyle in [bsSizeable, bsSizeToolWin] };
      if  Left + Width > lDesktop.Right  then
        Left := lDesktop.Right - Width;
      if  Top + Height > lDesktop.Bottom  then
        Top := lDesktop.Bottom - Height;
      if  Top < lDesktop.Top  then
        Top := lDesktop.Top;
      if  Left < lDesktop.Left  then
        Left := lDesktop.Left
    end { with Form }
end { CheckFormPos };


procedure  CreateDataModule(        DataClass:     TComponentClass;
                            var     DataReference;
                            const   DataName:      TComponentName;
                                    Owner:         TComponent);
begin
  if  (DataName <> '')
      or (FindDataModule(DataName) = nil)  then
    begin
      CreateForm(DataClass, DataReference, DataName, Owner);  // Application.CreateForm(DataClass, DataReference);
      TDataModule(DataReference).Name := DataName
    end { if };
end { CreateDataModule };


procedure  CreateForm(        FormClass:     TComponentClass;   // TFormClass;
                      var     Reference;
                      const   FormName:      TComponentName;
                              Owner:         TComponent;
                      const   CreateAction:  TCustomAction);
var
  Instance:  TComponent;
  FormCaption:  TCaption;

begin  { CreateForm }
  gSynchro.Enter();
  try
    if  (FormName = '')
        or (FindForm(FormName) = nil)  then
      begin
        Instance := TComponent(FormClass.NewInstance());
        TComponent(Reference) := Instance;
        try
          if  Owner = nil  then
            begin
              Owner := Screen.ActiveForm;
              if  Owner = nil  then
                Owner := Application
            end { Owner = nil };
          Instance.Create(Owner);
          { Jeżeli własności "Name" i "Caption" są identyczne to zmiana "Name"
            powoduje automatycznie identyczną zmianę własności "Caption". Aby
            temu przeciwdziałać, zapamiętuję tytuł okna i po zmianie jego nazwy
            przywracam oryginalny tytuł okna.                                 }
          FormCaption := TForm(Reference).Caption;
          Instance.Name := FormName;
          TForm(Reference).Caption := FormCaption
        except
          TComponent(Reference) := nil;
          Instance.Free();
          raise
        end { try-except }
      end { FindFomd() = nil };
    if  Assigned(CreateAction)  then
      TForm(Reference).Perform(gcnMsgSetProp,
                               gcnMsgSetPropAction,
                               Integer(CreateAction));
  finally
    gSynchro.Leave();
  end { try-finally }
end { CreateForm };


procedure  CreateForm(        FormClass:     TComponentClass;   // TFormClass;
                      var     Reference;
                      const   FormName:      TComponentName;
                              DataClass:     TComponentClass;
                      var     DataReference;
                      const   DataName:     TComponentName;
                              Owner:         TComponent;
                      const   CreateAction:  TCustomAction);
begin
  if  DataName <> ''  then
    begin
      CreateDataModule(DataClass, DataReference, DataName, Owner);
      CreateForm(FormClass, Reference, FormName, Owner, CreateAction);
      TForm(Reference).Perform(gcnMsgSetProp,
                               gcnMsgSetPropDataModule,
                               Integer(DataReference))
    end { DataName <> '' }
  else
    CreateForm(FormClass, Reference, FormName, DataClass, DataReference,
               FormName + gcsDataModuleSuffix, Owner, CreateAction)
end { CreateForm };


function  ExecForm(        FormClass:     TFormClass;
                   var     Reference;
                   const   FormName:      TComponentName;
                           Owner:         TComponent;
                   const   CreateAction:  TCustomAction;
                   const   bShowModal:    Boolean)
                   : TModalResult;
begin  { ExecForm }
  try
    if  Assigned(CreateAction)  then
      begin
        CreateAction.Enabled := False;
        CreateAction.Checked := False;
        CreateAction.Update()
      end { Assigned(CreateAction) };
    CreateForm(FormClass, Reference, FormName, Owner, CreateAction);
    Result := ShowForm(TForm(Reference), bShowModal)
  finally
    if  Assigned(CreateAction)  then
      CreateAction.Enabled := True
  end { try-finally }
end { ExecForm };


function  ExecForm(        FormClass:     TFormClass;
                   var     Reference;
                   const   FormName:      TComponentName;
                           DataClass:     TComponentClass;
                   var     DataReference;
                   const   DataName:      TComponentName;
                           Owner:         TComponent;
                   const   CreateAction:  TCustomAction;
                   const   bShowModal:    Boolean)
                   : TModalResult;
begin  { ExecForm }
  if  DataName <> ''  then
    try
      if  Assigned(CreateAction)  then
        begin
          CreateAction.Enabled := False;
          CreateAction.Checked := False;
          CreateAction.Update()
        end { Assigned(CreateAction) };
      CreateForm(FormClass, Reference, FormName,
                 DataClass, DataReference, DataName,
                 Owner, CreateAction);
      Result := ShowForm(TForm(Reference), bShowModal);
    finally
      if  Assigned(CreateAction)  then
        CreateAction.Enabled := True
    end { try-finally; DataName <> '' }
  else
    Result := ExecForm(FormClass, Reference, FormName,
                       DataClass, DataReference, FormName + gcsDataModuleSuffix,
                       Owner, CreateAction, bShowModal)
end { ExecForm };


function  GetFormRect(const   Form:  TCustomForm)
                      : TRect;
begin
  with  Form  do
    Result := Bounds(Left, Top, Width, Height)
end { GetFormRect };


procedure  SetFormRect(const   Form:  TCustomForm;
                       const   Rect:  TRect);
begin
  with  Form  do
    begin
      Left := Rect.Left;
      Top := Rect.Top;
      if  Form.BorderStyle in [bsSizeable, bsSizeToolWin]  then
        begin
          Width := Rect.Right - Left;
          Height := Rect.Bottom - Top
        end { BorderStyle in [...] }
    end { with Form };
  CheckFormPos(Form)
end { SetFormRect };


procedure  HideUnusedMenuItems(const   mnMenuItem:  TMenuItem);

var
  nInd:  integer;
  bHide:  Boolean;

begin  { HideUnusedMenuItems }
  bHide := (mnMenuItem.Count = 0);
  for  nInd := mnMenuItem.Count - 1  downto  0  do
    begin
      HideUnusedMenuItems(mnMenuItem[nInd]);
      if  mnMenuItem[nInd].Visible  then
        bHide := False
    end { for nInd };
  if  bHide  then
    begin
      if  mnMenuItem.Visible
          and not Assigned(mnMenuItem.Action)
          and not Assigned(mnMenuItem.OnClick)  then
        mnMenuItem.Visible := False
    end { mnMenuItem.Count = 0 }
end { HideUnusedMenuItems };


function  FindForm(const   sFormName:  TComponentName)
                   : TForm;
var
  nInd:  integer;

begin  { FindForm }
  for  nInd := Screen.FormCount - 1  downto  0  do
    begin
      Result := Screen.Forms[nInd];
      if  SameText(Result.Name, sFormName)  then
        Exit
    end { for nInd };
  Result := nil
end { FindForm };


function  FindDataModule(const   sDataModuleName:  TComponentName)
                         : TDataModule;
var
  nInd:  Integer;

begin  { FindDataModule }
  for  nInd := Screen.DataModuleCount - 1  downto  0  do
    begin
      Result := Screen.DataModules[nInd];
      if  SameText(Result.Name, sDataModuleName)  then
        Exit
    end { for nInd };
  Result := nil
end { FindDataModule };


function  ShowForm(const   Form:    TForm;
                   const   bModal:  Boolean)
                   : TModalResult;
begin
  Result := mrNone;
  if  bModal  then
    try
      Application.NormalizeTopMosts();
      Result := Form.ShowModal();
      // Assert(Assigned(Screen.ActiveForm));
      // CheckOSError(PostMessage(Screen.ActiveForm.Handle,
      //                          gcnMsgSetProp, gcnMsgSetPropFocus,
      //                          Integer(Screen.ActiveForm)));
      // ^^^^^^^^^^^^ Usunąłem, bo generowało błędy.
       { Okno niby już jest aktywne, ale nadal jakaś wewnętrzna flaga pokazuje,
         że nie ma do niego dostępu. Jest to chyba jaka pozostałość po oknie
         modalnym. Próbowałem to ominąć wstawiając Application.ProcessMessages,
         ale to nie pomogło.  }
    finally
      Application.RestoreTopMosts()
    end { try-finally }
  else
    Form.Show();
end { ShowForm };


initialization
  gSynchro := TCriticalSection.Create();


finalization
  gSynchro.Free()


end.

