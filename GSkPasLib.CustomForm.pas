﻿{: @abstract Definicja okna bazowego.

   Moduł @name zawiera definicję okna bazowego. Jest ono potomkiem standardowego
   typu @code(TForm) --- wprowadza do niego szereg usprawnień.

   Wszystkie okna typowej aplikacji powinny być potomkami (bezpośrednimi lub
   pośrednimi) klasy @link(TzzGSkCustomForm).  }
unit  GSkPasLib.CustomForm;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


// {$IfNDef  NoXPManifest }
//   {$R WindowsXP.res }
// {$EndIf  -NoXPManifest }


uses
  {$IfDef MemCheck }
    MemCheck,
  {$EndIf MemCheck }
  {$IF CompilerVersion > 22}
    WinAPI.Windows, WinAPI.Messages,
    System.SysUtils, System.Variants, System.Classes,
    VCL.Graphics, VCL.Controls, VCL.Forms, VCL.Dialogs, VCL.ActnList,
    IBX.IBDataBase, IBX.IBCustomDataSet, IBX.IBQuery,
  {$ELSE}
    Windows, Messages,
    SysUtils, Variants, Classes,
    Graphics, Controls, Forms, Dialogs, ActnList,
    IBDataBase, IBCustomDataSet, IBQuery,
  {$IFEND}
  JvResources,
  {$IFDEF GetText}
    JvGnugettext,
  {$ENDIF GetText}
  GSkPasLib.PasLib, GSkPasLib.ParamSaver;


type
  {: @abstract Okno bazowe.

     Wszystkie okna typowej aplikacji powinny być bezpośrednimi lub pośrednimi
     potomkami okna @name.

     Okno @name jest bezpośrednim potomkiem klasy @code(TForm). Wprowadza ono
     szereg usprawnień:
     @unorderedList(@itemSpacing(Compact)
       @item(Automatycznie zapamiętuje i przywraca położenie okna na ekranie.)
       @item(Jeżeli okno jest typu @code(bsSizeable) lub @code(bsSizeToolWin) to
             automatycznie zapamiętuje również szerokość i wysokość okna.)
       @item(Automatycznie sprawdza czy okno mieści się w całości na ekranie; w
             razie potrzeby przesuwa okno tak aby było w całości widoczne na
             ekranie.)
       @item(Proponuje usystematyzowany sposób zapamiętywania i przywracania
             parametrów związanych z oknem --- udostępnia oddzielne metody dla
             parametrów zależnych od rozdzielczości ekranu i oddzielne metody
             dla pozostałych parametrów.)
     )  }
  TzzGSkCustomForm = class(TForm)
  private
    class var
      fbNoAskTermn:   Boolean;
    var
      ffnOnClose:     TCloseEvent;
      fbFormShown:    Boolean;
      fDataModule:    TDataModule;
      fCreateAction:  TCustomAction;
    function  GetBorderStyle() : TFormBorderStyle;
    procedure  SetCreateAction(const   Value:  TCustomAction);
    procedure  OnCloseEvent(Sender:  TObject;
                            var   CloseAction:  TCloseAction);
    procedure  WMSysCommand(var   Msg: TWMSysCommand);     message WM_SYSCOMMAND;
    procedure  MsgSetProp(var   Msg:  TMessage);           message gcnMsgSetProp;
  protected
    {: @exclude }
    procedure  InternalReadParams();
    {: @exclude }
    procedure  InternalWriteParams();
    {: @exclude }
    procedure  SetBorderStyle(const   Value:  TFormBorderStyle);     virtual;

    {: @abstract Definiuje moduł danych.

       Metoda @name jest wykorzystywana przez bibliotekę do wskazania modułu
       danych związanego z danym oknem. Wskazane jest, aby to był moduł utworzony
       na bazie @link(TzzGSkCustomDataModule), ale nie jest to konieczne.

       @seeAlso(DataModule)
       @seeAlso(TzzGSkCustomDataModule)  }
    procedure  SetDataModule(const   Value:  TDataModule);           virtual;

    {: @abstract Odczytuje parametry okna.

       Metoda @name służy do pobierania parametrów okna. Przed odczytaniem
       parametrów należ najpierw otworzyć odpowiedni klucz.

       Standardowo parametry zapamiętywane są w kontekście użytkownika
       zalogowanego w systemie. Dlatego należy je wykorzystywać głównie do
       zapamiętywania kontekstu danych itp.

       Metoda @name jest wywołana po wygenerowaniu zdarzenia @code(OnCreate),
       ale przed wygenerowaniem zdarzenia @code(OnShow).

       @seeAlso(ReadParamsScr)
       @seeAlso(WriteParams)  }
    procedure  ReadParams(const   Saver:  TGSkCustomParamSaver);     dynamic;

    {: @abstract Odczytuje parametry okna zależne od rozdzielczości ekranu.

       Metoda @name służy do pobierania parametrów okna zależnych od
       rozdzielczości ekranu. Odpowiedni klucz już @bold(jest) otwarty.

       Standardowo parametry zapamiętywane są w kontekście użytkownika
       zalogowanego w systemie. Dlatego należy je wykorzystywać głównie do
       zapamiętywania informacji zależnych od rozdzielczości ekranu, na przykład
       proporcje paneli okna itp.

       Metoda @name jest wywołana po wygenerowaniu zdarzenia @code(OnCreate),
       ale przed wygenerowaniem zdarzenia @code(OnShow).

       @seeAlso(ReadParams)
       @seeAlso(WriteParamsScr)  }
    procedure  ReadParamsScr(const   Saver:  TGSkCustomParamSaver);  dynamic;

    {: @abstract Zapamiętuje parametry okna.

       Metoda @name służy do zapamiętywania parametrów okna. Przed zapisaniem
       parametrów należy najpierw otworzyć odpowiedni klucz.

       Standardowo parametry zapamiętywane są w kontekście użytkownika
       zalogowanego w systemie. Dlatego należy je wykorzystywać głównie do
       zapamiętywania kontekstu danych itp.

       Metoda @name jest wywoływana po wygenerowaniu zdarzenia @code(OnClose),
       ale przed wygenerowaniem zdarzenia @code(OnDestroy).

       @seeAlso(WriteParamsScr)
       @seeAlso(ReadParams)  }
    procedure  WriteParams(const   Saver:  TGSkCustomParamSaver);    dynamic;

    {: @abstract Zapamiętuje parametry okna zależne od rozdzielczości ekranu.

       Metoda @name służy do zapamiętania parametrów okna zależnych od
       rozdzielczości ekranu. Odpowiedni klucz już @bold(jest) otwarty.

       Standardowo parametry zapamiętywane są w kontekście użytkownika
       zalogowanego w systemie. Dlatego należy je wykorzystywać głównie do
       zapamiętywania informacji zależnych od rozdzielczości ekranu, na przykład
       proporcje paneli okna itp.

       Metoda @name jest wywoływana po wygenerowaniu zdarzenia @code(OnClose),
       ale przed wygenerowaniem zdarzenia @code(OnDestroy).

       @seeAlso(WriteParams)
       @seeAlso(ReadParamsScr)  }
    procedure  WriteParamsScr(const   Saver:  TGSkCustomParamSaver); dynamic;

    {: @exclude }
    procedure  DoCreate();                                           override;
    {: @exclude }
    procedure  DoShow();                                             override;
    {: @exclude }
    procedure  KeyDown(var   nKey:  Word;
                       setShift:  TShiftState);                      override;
    {: @exclude }
    procedure  Notification(Component:  TComponent;
                            Operation:  TOperation);                 override;

  public

    {: @exclude }
    function  CloseQuery() : Boolean;                                override;

    { public properties }
    {: @abstract Czy okno było otwarte.

       Właściwość informuje czy okno było już otwarte. Ta wartość ma nieco inne
       znaczenie niż standardowa wartość @code(Visible). Po wyświetleniu okna
       właściwości @name oraz @code(Visible) mają wartość @true. Jeżeli okno
       zostanie ukryte (@code(Hide())) to właściwość @code(Visible) ma wartość
       @false, ale właściwość @name nadal ma wartość @true. Dopiero po
       zamknięciu okna (@code(Close())) właściwość @name ma również wartość
       @false.  }
    property  FormShown:  Boolean
              read  fbFormShown;

    {: @abstract Moduł danych skojarzony z oknem.

       Właściwość @name wskazuje moduł danych skojarzony z danym oknem. Wskazane
       jest aby to był moduł utworzony na bazie @link(TzzGSkCustomDataModule),
       ale nie jest to konieczne.  }
    property  DataModule:  TDataModule
              read  FDataModule
              write SetDataModule;

    {: @abstract Akcja, której wykonanie spowodowało utworzenie okna.

       Właściwość @name wskazuje akcję, której wykonanie spowodowało powstanie
       danego okna.  }
    property  CreateAction:  TCustomAction
              read  FCreateAction
              write SetCreateAction;

    {: @abstract Czy potwierdzać zakończenie działania aplikacji.

       Jeżeli zamykane jest główne okno programu, to standardowo użytkownik jest
       proszony o potwierdzenie zakończenia działania programu. W niektórych
       przypadkach jest to niewskazane, na przykład gdy program ma być uruchamiany
       wraz z Windows oraz działać do zamknięcia Windows). W takim przypadku wartości
       @name można przypisać @True, aby przy zamykaniu Windows program nie pytał
       użytkownika o zgodę na zakończenie działania programu. }
    class property  DoNotConfirmAppTerminate:  Boolean
                    read  fbNoAskTermn
                    write fbNoAskTermn;

  published

   {: @exclude }
    property  BorderStyle:  TFormBorderStyle
              read  GetBorderStyle
              write SetBorderStyle;
  end { TGSkCustomForm };


// var
//   TGSkCustomForm:  TGSkCustomForm;


implementation


uses
  GSkPasLib.Dialogs, GSkPasLib.ComponentUtils, GSkPasLib.FormUtils;


type
  PCloseAction = ^TCloseAction;


{$IFDEF GetText}

var
  gYes:  string = '&Yes';
  gNo:   string = '&No';

{$ENDIF GetText}


{$R *.dfm}


{$REGION 'Local subroutines'}

procedure  CloseQuerySubForm(const   fForm:  TControl;
                             const   pResult:  Pointer);
begin
  PBoolean(pResult)^ := (fForm as TCustomForm).CloseQuery()
end { CloseSubForm };


procedure  CloseSubForm(const   fForm:  TControl;
                        const   pResult:  Pointer);
begin
  if  fForm is TzzGSkCustomForm  then
    begin
      with  fForm as TzzGSkCustomForm  do
        if  FormShown  then
          DoClose(PCloseAction(pResult)^)
    end { fForm is TzzGSkCustomForm }
  else
    if  (fForm as TForm).Visible  then
      TzzGSkCustomForm(fForm).DoClose(PCloseAction(pResult)^)
end { CloseSubForm };


procedure  ReleaseDatabaseResources(const   Comp:  TComponent;
                                    const   pData:  Pointer);
begin
  if  Comp is TIBDataSet  then
    with  Comp as TIBDataSet  do
      while  Prepared  do
        UnPrepare()
  else if  Comp is TIBQuery  then
    with  Comp as TIBQuery  do
      while  Prepared  do
        UnPrepare()
  else if  Comp is TIBTransaction  then
    with  Comp as TIBTransaction  do
      if  Active  then
        Commit()
end { ReleaseDatabaseResources };


procedure  ShowSubForm(const   fForm:  TComponent;
                       const   pResult:  Pointer);
begin
  if  fForm is TzzGSkCustomForm  then
    begin
      with  fForm as TzzGSkCustomForm  do
        if  FormShown  then
          Visible := Boolean(pResult)
    end { fForm is TzzGSkCustomForm }
  else
    (fForm as TForm).Visible := Boolean(pResult)
end { ShowSubForm };

{$ENDREGION 'Local subroutines'}


{$REGION 'TGSkCustomForm'}

function  TzzGSkCustomForm.CloseQuery() : Boolean;

const
  cnTimeout = 90;   // [s]
  cnYes = 1;
  cnNo  = 2;

begin  { CloseQuery }
  Result := inherited CloseQuery();
  if  Result  then
    begin
      if  (Self = Application.MainForm)
          and not DoNotConfirmAppTerminate  then
        {$IFDEF GetText}
          Result := MsgDlgEx(dgettext('GSkPasLib', 'Confirm'),
                             dgettext('GSkPasLib', 'Do you really want to exit?'),
                             mtConfirmation, [gYes, gNo], [cnYes, cnNo],
                             nil, cnTimeout, 500, 1, 1) = cnYes;
        {$ELSE}
          Result := MsgDlg('Czy rzeczywiście chcesz zakończyć pracę programu?',
                           mtConfirmation, mbYesNo, nil,
                           mbNo, mbNo, cnTimeout) = mrYes;
        {$ENDIF}
      if  Result  then
        ForEachControl(Self, CloseQuerySubForm, [Self],
                       TCustomForm, Addr(Result), False)
    end { if }
end { CloseQuery };


procedure  TzzGSkCustomForm.DoCreate();
begin
  inherited;
  if  BorderStyle = bsSingle  then
    BorderIcons := BorderIcons - [biMaximize];
  InternalReadParams();
  ffnOnClose := OnClose;
  OnClose := OnCloseEvent;
  {$IFDEF GetText}
    TranslateComponent(Self);
  {$ENDIF GetText}
end { DoCreate };


procedure  TzzGSkCustomForm.DoShow();
begin
  inherited;
  fbFormShown := True
end { DoShow };


function  TzzGSkCustomForm.GetBorderStyle() : TFormBorderStyle;
begin
  Result := inherited BorderStyle
end { GetBorderStyle };


procedure  TzzGSkCustomForm.InternalReadParams();

var
  lSaver:  TGSkCustomParamSaver;

begin  { InternalReadParams }
  lSaver := CreateStdParamSaver();
  try
    if  lSaver.OpenKeyReadOnly(GetFormRegistryKey(Self, True))  then
      try
        try
          ReadParamsScr(lSaver)
        except
          { OK }
        end { try-except }
      finally
        lSaver.CloseKey()
      end { try-finally };
    try
      ReadParams(lSaver)
    except
      { OK }
    end { try-except }
  finally
    lSaver.Free()
  end { try-finally }
end { InternalReadParams };


procedure  TzzGSkCustomForm.InternalWriteParams();

var
  lSaver:  TGSkCustomParamSaver;
  lCursor:  TCursor;

begin  { InternalWriteParams }
  lCursor := Screen.Cursor;
  try
    Screen.Cursor := crHourGlass;
    lSaver := CreateStdParamSaver();
    try
      lSaver.BeginUpdate();
      try
        if  lSaver.OpenKey(GetFormRegistryKey(Self, True))  then
          try
            try
              WriteParamsScr(lSaver)
            except
              { OK }
            end { try-except }
          finally
            lSaver.CloseKey()
          end { try-finally };
        try
          WriteParams(lSaver)
        except
          { OK }
        end { try-except }
      finally
        lSaver.EndUpdate()
      end { try-finally }
    finally
      lSaver.Free()
    end { try-finally }
  finally
    Screen.Cursor := lCursor
  end { try-finally }
end { InternalWriteParams };


procedure  TzzGSkCustomForm.KeyDown(var   nKey:  Word;
                                    setShift:  TShiftState);

  procedure  KillMessage(Wind:  HWnd;
                         nMsg:  Integer);
  var
    Msg:  TMsg;

  begin  { KillMessage }
    Msg.Message := 0;
    if  PeekMessage(Msg, Wind, nMsg, nMsg, pm_Remove)
        and (Msg.Message = WM_QUIT)                  then
      PostQuitMessage(Msg.wParam);
  end { KillMessage };

begin  { KeyDown }
  if  setShift = [ssCtrl]  then
    case  nKey  of
      VK_F4:
        if  Self <> Application.MainForm  then
          begin
            Close();
            nKey := 0
          end { [Ctrl+F4] }
    end { [Ctrl] }
  else if  setShift = [ssAlt]  then
    case  nKey  of
      VK_F4:
        begin
          Application.MainForm.Close();
          nKey := 0
        end { [Alt+F4] }
    end { case Key };
  if  nKey = 0  then
    KillMessage(Handle, WM_CHAR);
  inherited
end { KeyDown };


procedure  TzzGSkCustomForm.MsgSetProp(var   Msg:  TMessage);
begin
  Assert(Msg.Msg =  gcnMsgSetProp);
  case  Msg.wParam  of
    gcnMsgSetPropModalResult:
      ModalResult := Msg.lParam;
    gcnMsgSetPropDataModule:
      DataModule := TDataModule(Msg.lParam);
    gcnMsgSetPropAction:
      CreateAction := TAction(Msg.lParam);
    gcnMsgSetPropFocus:
      ActiveControl := TWinControl(Msg.lParam)
  end { case MsgSetProp }
end { MsgSetProp };


procedure  TzzGSkCustomForm.Notification(Component:  TComponent;
                                         Operation:  TOperation);
begin
  inherited;
  if  Operation = opRemove  then
    if  Component = FDataModule  then
      DataModule := nil
    else if  Component = FCreateAction  then
      CreateAction := nil
end { Notification };


procedure  TzzGSkCustomForm.OnCloseEvent(Sender:  TObject;
                                         var   CloseAction:  TCloseAction);
begin
  if  Assigned(ffnOnClose)  then
    ffnOnClose(Sender, CloseAction);   // <-- inherited;
  if  CloseAction = caNone  then
    Exit;
  try
    ForEachControl(Self, CloseSubForm, [Self], TCustomForm,
                   Addr(CloseAction), False);
    ForEachComponent(Self, ReleaseDatabaseResources);
    InternalWriteParams()
  except
    on  EAbort  do
      begin
        CloseAction := caNone;
        raise
      end { EAbort }
    else
      raise
  end { try-except };
  fbFormShown := False
end { OnCloseEvent };


procedure  TzzGSkCustomForm.ReadParams(const   Saver:  TGSkCustomParamSaver);
begin
  { OK }
end { ReadParams };


procedure  TzzGSkCustomForm.ReadParamsScr(const   Saver:  TGSkCustomParamSaver);
begin
  with  Saver  do
    try
      Top := ReadInteger(gcsParFormTop, Top);
      Left := ReadInteger(gcsParFormLeft, Left);
      if  BorderStyle in [bsSizeable, bsSizeToolWin]  then
        begin
          Width := ReadInteger(gcsParFormWidth, Width);
          Height := ReadInteger(gcsParFormHeight, Height);
          if  TWindowState(ReadInteger(gcsParFormState,
                                       Ord(WindowState))) = wsMaximized  then
            WindowState := wsMaximized
          else
            WindowState := wsNormal
        end { BorderStyle in [...] };
      CheckFormPos(Self)
    except
      { OK }
    end { try-except }
end { ReadParamsScr };


procedure  TzzGSkCustomForm.SetBorderStyle(const   Value:  TFormBorderStyle);
begin
  inherited BorderStyle := Value;
  if  Value in [bsSizeable, bsSizeToolWin]  then
    BorderIcons := BorderIcons + [biMaximize]
  else
    BorderIcons := BorderIcons - [biMaximize]
end { SetBorderStyle };


procedure  TzzGSkCustomForm.SetCreateAction(const   Value:  TCustomAction);
begin
  if  FCreateAction <> nil  then
    FCreateAction.RemoveFreeNotification(Self);
  FCreateAction := Value;
  if  FCreateAction <> nil  then
    FCreateAction.FreeNotification(Self)
end { SetCreateAction };


procedure  TzzGSkCustomForm.SetDataModule(const   Value:  TDataModule);
begin
  if  FDataModule <> nil  then
    FDataModule.RemoveFreeNotification(Self);
  FDataModule := Value;
  if  FDataModule <> nil  then
    FDataModule.FreeNotification(Self)
end { SetDataModule };


procedure  TzzGSkCustomForm.WMSysCommand(var   Msg:  TWMSysCommand);

var
  bShow:  Boolean;
  nCmdType:  Integer;

begin  { WMSysCommand }
  //  inherited; -- później
  try
    nCmdType := Msg.CmdType and $FFF0;
    if  (nCmdType = SC_MINIMIZE)
        or (nCmdType = SC_MAXIMIZE)
        or (nCmdType = SC_RESTORE)  then
      begin
        bShow := (nCmdType <> SC_MINIMIZE);
        ForEachComponent(Self, ShowSubForm, [Self], TForm, Pointer(bShow))
      end { Msg.CmdType in [SC_MINIMIZE, SC_MAXIMIZE, SC_RESTORE] }
  finally
    inherited
  end { try-finally }
end { WMSysCommand };


procedure  TzzGSkCustomForm.WriteParams(const   Saver:  TGSkCustomParamSaver);
begin
  { OK }
end { WriteParams };


procedure  TzzGSkCustomForm.WriteParamsScr(const   Saver:  TGSkCustomParamSaver);
begin
  with  Saver  do
    try
      WriteInteger(gcsParFormState, Ord(WindowState));
      if  WindowState = wsNormal  then
        begin
          WriteInteger(gcsParFormTop, Top);
          WriteInteger(gcsParFormLeft, Left);
          WriteInteger(gcsParFormWidth, Width);
          WriteInteger(gcsParFormHeight, Height)
        end { WindowState = wsNormal }
    except
      { OK }
    end { try-except }
end { WriteParamsScr };

{$ENDREGION 'TzzGSkCustomForm'}


initialization
  {$IFDEF MemCheck }
    if  IsInsideDelphi()  then
      if  not IsMemCheckActive()  then
        MemChk();
  {$ENDIF MemCheck }
  {$IFDEF GetText}
    gYes := dgettext('GSkPasLib', 'Yes');
    gNo  := dgettext('GSkPasLib', 'No');
  {$ENDIF GetText}


end.
