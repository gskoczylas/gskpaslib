﻿{: @exclude }
unit  GSkPasLib.PasLibInternals;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    System.SysUtils, WinAPI.Windows
  {$ELSE}
    SysUtils, Windows
  {$IFEND}{$IFDEF GetText},
  JvGnugettext{$ENDIF GetText};


const
  gcsLibName = 'GSkPasLib';
  gcsStdHelpDir = 'p:\GSkLib\Doc';
  gcsStdHelpFile = 'index.html';
  gcsAboutLib = 'O „' + gcsLibName + '”';


type
  EGSkPasLibError = class(Exception)
  private
    fsErrInFunction:  string;
    fsErrMessage:     string;
  public
    constructor  Create(const   sName, sMessage:  string);
    constructor  CreateFmt(const   sName, sFormat:  string;
                           const   Arguments:  array of const);
    property  ErrorInFunction:  string
              read  fsErrInFunction;
    property  ErrorMessage:  string
              read  fsErrMessage;
  end { EGSkPasLibError };

  EGSkPasLibErrorEx = class(EGSkPasLibError)
  private
    var
      fnErrorCode:  Integer;
  public
    constructor  Create(const   sMethod:  string;
                        const   nErrCode:  Integer;
                        const   sErrMsg:  string = '');
    constructor  CreateFmt(const   sMethod:  string;
                           const   nErrCode:  Integer;
                           const   sErrMsgFmt:  string;
                           const   ErrMsgArgs:  array of const);
    {-}
    property  ErrorCode:  Integer
              read  fnErrorCode;
  end { EGSkPasLibErrorEx };


{: @abstract Sprawdza czy program działa w systemie Windows NT, 2000 lub późniejszym.

   Procedura sprawdza czy program działą w systemie Windows NT, 2000, XP lub
   późniejszym. Jeżeli program działa w systemie Windows 95, 98 lub Me
   to generowany jest wyjątek @link(EGSkPasLibError).

   @param(sSubroutine Nazwa podprogramu wywołującego procedurę @name.
                      W przypadku błędu ta nazwa będzie częścią komunikatu błędu
                      i pozwoli łatwiej zlokalizować problem.)  }
procedure  CheckWindowsNT(const   sSubroutine:  string);


implementation


uses
  GSkPasLib.PasLib;


{$REGION 'Global subroutines'}

procedure  CheckWindowsNT(const   sSubroutine:  string);
begin
  if  Win32Platform <> VER_PLATFORM_WIN32_NT  then
    raise  EGSkPasLibError.Create(sSubroutine,
                                  {$IFDEF GetText}
                                    dgettext('GSkPasLib',
                                             'Function can not be used in this version of the operating system')
                                  {$ELSE}
                                    'Funkcja nie może być używana w tej wersji systemu operacyjnego'
                                  {$ENDIF -GetText});
end { CheckWindowsNT };

{$ENDREGION 'Global subroutines'}


{$REGION 'EGSkPasLibError'}

constructor  EGSkPasLibError.Create(const   sName, sMessage:  string);
begin
  inherited CreateFmt('%s: %s', [sName, sMessage])
end { Create };


constructor  EGSkPasLibError.CreateFmt(const   sName, sFormat:  string;
                                       const   Arguments:  array of const);
begin
  inherited CreateFmt('%s: %s',
                      [sName,
                       Format(sFormat, Arguments)])
end { CreateFmt };

{$ENDREGION 'EGSkPasLibError'}


{$REGION 'EGSkPasLibErrorEx'}

constructor  EGSkPasLibErrorEx.Create(const   sMethod:  string;
                                      const   nErrCode:  Integer;
                                      const   sErrMsg:  string);
begin
  inherited Create(sMethod, sErrMsg);
  fnErrorCode := nErrCode
end { Create };


constructor  EGSkPasLibErrorEx.CreateFmt(const   sMethod:  string;
                                         const   nErrCode:  Integer;
                                         const   sErrMsgFmt:  string;
                                         const   ErrMsgArgs:  array of const);
begin
  inherited CreateFmt(sMethod, sErrMsgFmt, ErrMsgArgs);
  fnErrorCode := nErrCode
end { CreateFmt };

{$ENDREGION 'EGSkPasLibErrorEx'}


end.

