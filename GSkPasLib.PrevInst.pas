﻿{: @abstract Moduł @name zawiera definicję komponentu @link(TGSkPrevInst).  }
unit  GSkPasLib.PrevInst;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GSkPasLib.CustomComponents;


type
  {: @abstract Możliwe akcje po przekroczeniu limitu instancji.

     Typ @name definiuje możliwe akcje jeżeli limit aktywnych instancji programu
     zostanie przekroczony.

     @value(piRestPrev Powoduje uaktywnienie poprzedniej instancji programu.)
     @value(piQuitPrev Powoduje zakończenie działania poprzedniej instancji
                       programu.)
     @value(piQuitCurr Powoduje zakończenie działania bieżącej (nadmiarowej)
                       instancji programu.)  }
  TPrevInstAction = (piRestPrev, piQuitPrev, piQuitCurr);
  {: @abstract Zbiór możliwych akcji po przekroczeniu limitu instancji.

     Typ @name definiuje akcje do wykonania jeżeli limit aktywnych instancji
     programu przekroczy zadany limit.  }
  TPrevInstActions = set of TPrevInstAction;
  TInstancesCount = Byte;
  {: @abstract Zdarzenie generowane po przekroczeniu limitu instancji.

     Zdarzenie typu @name generowane jest jeżeli bieżąca instancja przekracza
     zadany limit jednocześnie działających instancji programu.

     @param(nInstanceCount Liczba aktywnych instancji programu (łącznie
                           z bieżącą, nadmiarową instancją).)
     @param(Actions Zbiór akcji. Procedura obsługi zdarzenia może zmienić
                    zawartość tego zbioru.)  }
  TOnLimitExceededEvent = procedure(Sender:  TObject;
                                    nInstanceCount:  TInstancesCount;
                                    var   Actions:  TPrevInstActions)
                                      of object;


const
  {: Domyślnie dopuszczalna jest jedna instancja programu. }
  gcnDefInstancesLimit = 1;
  {: Domyślnie aktywowana jest poprzednia instancja programu, a bieżąca instancja
     programu kończy natychmiast działanie. }
  gcsetDefLimitExceededAction = [piRestPrev, piQuitCurr];


type
  {: @abstract Obiekt kontroluje liczbę jednocześnie aktywnych instancji programu.

     Ten obiekt należy położyć na głównym oknie programu. W najprostszym
     przypadku wystarczy zdefiniować właściwości @link(InstancesLimit) oraz
     @link(LimitExceededAction) komponentu. Jeżeli potrzeba poinformować
     użytkownika o przekroczeniu limitu aktywnych instancji programu i zapytać
     o dalsze działania to należy napisać procedurę obsługującą zdarzenie
     @link(OnLimitExceded).  }
  TGSkPrevInst = class(TGSkCustomComponent)
  private
    var
      fbChecked:              Boolean;
      fInstancesLimit:        TInstancesCount;
      FOnLimitExceeded:       TOnLimitExceededEvent;
      fLimitExceededAction:  TPrevInstActions;
      fhPrevInst:            HWND;
      fbAutoCheck:           Boolean;
    procedure  SetInstancesLimit(const   nLimit:  TInstancesCount);
  protected
    {: @abstract Sprawdza liczbę działających instancji programu.

       Wszystkie kontrole komponent @name wykonuje podczas jego ładowania
       do pamięci. Najlepiej gdy komponent jest położony na głównym oknie
       programu. Wtedy będzie wykonany podczas uruchamiania programu.

       Jeżeli liczba działających instancji programu, łącznie z bieżącą,
       przekroczy limit określony przez właściwość @link(InstancesLimit)
       to wywoływana jest metoda @link(LimitExceeded). Standardowo jedynym
       jej działaniem jest wygenerowanie zdarzenia @link(OnLimitExceded).  }
    procedure  Loaded();                                             override;
  public
    constructor  Create(AOwner:  TComponent);                        override;
    {: @abstract Informuje o liczbie aktywnych instancji programu.

       Funkcja uwzględnia również bieżącą instancję programu.  }
    function  InstancesCount():  TInstancesCount;
    {: @abstract Sprawdza, czy liczba instancji aplikacji nie przekracza limitu.

       @seeAlso(InstancesLimit)
       @seeAlso(LimitExceededAction)
       @seeAlso(OnLimitExceded)  }
    procedure  CheckInstances();
    {: Generuje zdarzenie @link(OnLimitExceded).

       Metoda @name wywoływana jest jeżeli liczba działających instancji
       programu, łącznie z bieżącą, przekroczy limit określony przez właściwość
       @link(InstancesLimit).

       Standardowo jedynym działaniem metody @name jest wygenerowanie zdarzenia
       @link(OnLimitExceded).  }
    procedure  LimitExceeded(const   nInstancesCount:  TInstancesCount);
  published
    {: @abstract Definiuję limit jednocześnie aktywnych instancji programu.

       Po przekroczeniu tego limitu generowane jest zdarzenie
       @link(OnLimitExceded) lub podejmowane są działania określone przez
       właściwość @link(LimitExceededAction).  }
    property  InstancesLimit:  TInstancesCount
              read  fInstancesLimit
              write SetInstancesLimit
              default gcnDefInstancesLimit;
    property  AutoCheck:  Boolean
              read  fbAutoCheck
              write fbAutoCheck
              default True;
    {: @abstract(Zdarzenie generowane po przekroczeniu liczby aktywnych
                 instancji programu.)

       To zdarzenie jest generowane jeżeli liczba aktywnych instancji programu
       (łącznie z bieżącą) przekracza limit określony przez właściwość
       @link(InstancesLimit).  }
    property  OnLimitExceded:  TOnLimitExceededEvent
              read  FOnLimitExceeded
              write FOnLimitExceeded;
    {: @abstract(Działania podejmowane po przekroczeniu liczby aktywnych
                 instancji programu.)

       Jeżeli zdefiniowana jest procedura obsługi zdarzenia @link(OnLimitExceded)
       to właściwość @name definiuje wartości domyślne odpowiedniego parametru 
       tej procedury.  }
    property  LimitExceededAction:  TPrevInstActions
              read  fLimitExceededAction
              write fLimitExceededAction
              default gcsetDefLimitExceededAction;
  end { TGSkPrevInst };


implementation


//uses


const
  cnErrLevQuitPrev = 0;
  cnErrLevQuitCurr = 0;


type
  TParam = record
             sAppClassName, sAppName:  string;  //  Application ClassName and Title
             nCount:  TInstancesCount;          //  Count of instances (including current)
             hPrev:  HWND;                      //  Prev. instance (last found)
           end { TCurrApp };
  TParamPtr = ^TParam;
  TFnGetName = function(hForm:  HWND;
                        lpClassName:  {$IFDEF UNICODE}PWideChar{$ELSE}PChar{$ENDIF};
                        nMaxCount:  integer)
                        : integer;   stdcall;


function  GetName(const   hForm:  HWND;
                  const   FnGetName:  TFnGetName;
                  const   sDefault:  string = '')
                  : string;
var
  {$IFDEF UNICODE}
    pszBuf:  PWideChar;
  {$ELSE}
    pszBuf:  PChar;
  {$ENDIF}

begin  { GetName }
  Result := '';
  pszBuf := StrAlloc(255);
  try
    if  FnGetName(hForm, pszBuf, StrBufSize(pszBuf)) > 0  then
      Result := pszBuf
    else if  sDefault <> ''  then
      Result := sDefault
    else
      RaiseLastOSError()
  finally
    StrDispose(pszBuf)
  end { try-finally }
end { GetName };


constructor  TGSkPrevInst.Create(AOwner:  TComponent);
begin
  inherited;
  fInstancesLimit := gcnDefInstancesLimit;
  fLimitExceededAction := gcsetDefLimitExceededAction;
  fbAutoCheck := True
end { Create };


procedure  TGSkPrevInst.SetInstancesLimit(const   nLimit:  TInstancesCount);
begin
  fInstancesLimit := nLimit;
  CheckInstances()
end { SetInstancesLimie };


procedure  TGSkPrevInst.Loaded();
begin
  inherited;
  if  not (csDesigning in ComponentState)  then
    if  fbAutoCheck
        and not fbChecked  then
      begin
        CheckInstances();
        fbChecked := true
      end { if }
end { Loaded };


procedure  TGSkPrevInst.CheckInstances();

var
  nInstCnt:  TInstancesCount;

begin  { CheckInstances }
  nInstCnt := InstancesCount();
  if  nInstCnt > fInstancesLimit  then
    LimitExceeded(nInstCnt)
end { CheckInstances };


procedure  TGSkPrevInst.LimitExceeded(const   nInstancesCount:  TInstancesCount);

var
  setActions:  TPrevInstActions;
  hPopup:  HWND;

begin  { LimitExceeded }
  { What to do }
  setActions := fLimitExceededAction;
  if  Assigned(FOnLimitExceeded)  then
    FOnLimitExceeded(Self, nInstancesCount, setActions);
  { Do it }
  if  piRestPrev in setActions  then   //  Restore previous instance
    begin
      hPopup := GetLastActivePopup(fhPrevInst);
      BringWindowToTop(fhPrevInst);
      if  IsIconic(hPopup)  then
        ShowWindow(hPopup, SW_RESTORE)
      else
        BringWindowToTop(hPopup);
      SetForegroundWindow(hPopup)
    end { piRestPrev in Actions };
  if  piQuitPrev in setActions  then   //  Quit previous instance
    PostMessage(fhPrevInst, WM_QUIT, cnErrLevQuitPrev, 0);
  if  piQuitCurr in setActions  then   //  Quit current instance
    begin
      Application.Terminate();
      // Abort()   -- powoduje błędy w innych modułach biblioteki i programów
    end { piQuitCurr in Actions };
end { LimitExceeded };


function  CheckWindow(hWindow: HWnd;
                      pParam:  TParamPtr)
                      : BOOL;  stdcall;
begin  { CheckWindow }
  with  pParam^  do
    if  (GetName(hWindow, GetClassName) = sAppClassName)
        and (GetName(hWindow, GetWindowText, Application.Title) = sAppName)  then
      begin
        Inc(nCount);
        if  hWindow <> Application.Handle  then
          hPrev := hWindow
      end ;
  Result := True
end { CheckWindow };


function  TGSkPrevInst.InstancesCount():  TInstancesCount;

var
  Param:  TParam;

begin  { InstancesCount }
  with  Param  do
    begin
      if  Application.MainForm <> nil  then
        begin
          sAppClassName := GetName(Application.MainForm.Handle, GetClassName);
          sAppName := GetName(Application.MainForm.Handle, GetWindowText)
        end { Application.MainForm <> nil }
      else
        begin
          sAppClassName := GetName(Application.Handle, GetClassName);
          sAppName := GetName(Application.Handle, GetWindowText, Application.Title)  // TODO 4 : Poprawić to
          { W „OnCreate” i w „OnLoad” główne okno nie jest jeszcze przypisane (w Delphi XE).
            Wywołanie „GetWindowText” z „Application.Handle” zwraca błąd 87 (Windows 8).
            Próbowałem to obejść zwracając w takim przypadku „Application.Title”,
            ale to chyba nie działa tak, jak potrzeba. }
        end { Application.MainForm = nil };
      nCount := 0;
      hPrev := 0
    end { with Param };
  EnumWindows(@CheckWindow, Integer(@Param));
  fhPrevInst := Param.hPrev;
  Result := Param.nCount
end { InstancesCount };


end.

