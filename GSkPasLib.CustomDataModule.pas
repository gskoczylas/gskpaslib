﻿{: @abstract Prototyp modułu danych.

   Aplikacje zamiast modułów typu @code(TDataModule) powinny używać moduły typu
   potomnego od zdefiniowanej w module @name klasy @link(TzzGSkCustomDataModule).

   @seeAlso(GSkPasLib.CustomMainDataModule)  }
unit  GSkPasLib.CustomDataModule;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IfDef MemCheck }
    MemCheck,
  {$EndIf MemCheck }
  Classes;


type
  {: @abstract Prototyp modułu danych.

     Aplikacje zamiast modułów typu @code(TDataModule) powinny używać moduły
     typu potomnego od klasy @name.

     W aplikacji można użyć dowolnie wiele modułów danych będących potomkami
     klasy @name.

     @seeAlso(TzzGSkCustomMainDataModule)  }
  TzzGSkCustomDataModule = class(TDataModule)
  end { TzzGSkCustomDataModule };


// var
//   zzGSkCustomDataModule:  TzzGSkCustomDataModule;


implementation





{$R *.dfm}


initialization
  {$IfDef MemCheck }
    if  IsInsideDelphi()  then
      if  not IsMemCheckActive()  then
        MemChk();
  {$EndIf MemCheck }


end.
