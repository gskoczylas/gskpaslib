﻿{: @exclude
   Wewnętrznych modułów biblioteki nie komentuję (na razie).  }
unit  GSkPasLib.CompEditCustomDialog;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  DesignIntf, DesignEditors, SysUtils;


type
  TCompEdCustomDialog = class(TDefaultEditor)
  public
    function  GetVerbCount() : Integer;                              override;
    function  GetVerb(Index:  Integer)
                      : string;                                      override;
    procedure  ExecuteVerb(Index:  Integer);                         override;
    procedure  Edit();                                               override;
  end { TCompEdCustomDialog };


implementation


uses
  GSkPasLib.CustomComponents, GSkPasLib.Dialogs;


{ TCompEdCustomDialog }

procedure  TCompEdCustomDialog.Edit();
begin
  inherited;
  ExecuteVerb(0)
end { Edit };


procedure  TCompEdCustomDialog.ExecuteVerb(Index:  Integer);
begin
  inherited;
  if  Index <> 0  then
    Exit;
  if  Component is TGSkCustomDialogFB then
    MsgDlg('Execute() = %s',
           [BoolToStr((Component as TGSkCustomDialogFB).Execute(),
                      True)],
           mtInformation)
  else if  Component is TGSkCustomDialogFI  then
    MsgDlg('Execute() = %d',
           [(Component as TGSkCustomDialogFI).Execute()],
           mtInformation)
  else if  Component is TGSkCustomDialogP  then
    (Component as TGSkCustomDialogP).Execute()
end { ExecuteVerb };


function  TCompEdCustomDialog.GetVerb(Index:  Integer)
                                      : string;
begin
  case  Index  of
    0:
      Result := 'Test…'
  else
      Result := '—'
  end { case Index }
end { GetVerb };


function  TCompEdCustomDialog.GetVerbCount() : Integer;
begin
  Result := 1
end { GetVerbCont };


end.
