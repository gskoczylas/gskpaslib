﻿{: @abstract Rozszerzenie możliwości standardowej klasy @code(TThread). }
unit  GSkPasLib.Thread;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  Classes, Windows, SyncObjs, SysUtils;


const
  wrSignaled  = SyncObjs.wrSignaled;
  wrTimeout   = SyncObjs.wrTimeout;
  wrAbandoned = SyncObjs.wrAbandoned;
  wrError     = SyncObjs.wrError;


type
  TWaitResult = SyncObjs.TWaitResult;

  {: @abstract Rozszerzenie możliwości standardowej klasy @code(TThread). }
  TGSkThreadHlp = class helper for TThread
  public

    {: @abstract Czekanie na zakończenie działania wątku.

       Metoda @name zatrzymuje bieżący wątek do czasu, aż zakończony zostanie
       odpowiedni wątek. Ta wersja metody działa identycznie, jak analogiczna
       metoda w standardowej klasie @code(TThread).  }
    function  WaitFor() : LongWord;                                  overload;

    {: @abstract Czeka na ustawienie obiektu synchronizacji.

       Metoda @name zatrzymuje bieżący wątek do czasu, aż wskazany w parametrze
       obiekt synchronizacji znajdzie się w stanie zasygnalizowanym.

       @param(SynchObj Obiekt synchronizacji. @br @br
                       Może to być obiekt typu @code(TEvent), @code(TMutex),
                       @code(@link(GSkPasLib.SynchroUtils.TGSkMutex TGSkMutex)) lub
                       @code(@link(GSkPasLib.SynchroUtils.TGSkSemaphore TGSkSemaphore)).)
       @param(nTimeout Czas oczekiwania w milisekundach.)  }
    function  WaitFor(const   SynchObj:  THandleObject;
                      const   nTimeout:  LongWord = INFINITE)
                      : TWaitResult;                                 overload;

    {: @abstract Czeka na ustawienie jednego z obiektów.

       Metoda @name zatrzymuje bieżący wątek do czasu, aż wskazany w parametrze
       obiekt synchronizacji znajdzie się w stanie zasygnalizowanym.

       @param(SynchObj Tablica obiektów typu @code(TEvent).)
       @param(nSynch Indeks obiektu, który został ustawiony.)
       @param(bWaitAll Czy czekać na ustawienie wszystkich obiektów? Jeżeli ma
                       wartość @false, to czekanie kończy się po ustawieniu
                       któregokolwiek obiektu.)
       @param(nTimeout Czas oczekiwania w milisekundach.)  }
    function  WaitFor(const   SynchObj:  array of TEvent;
                      out     nSynch:  Integer;
                      const   bWaitAll:  Boolean = False;
                      const   nTimeout:  LongWord = INFINITE)
                      : TWaitResult;                                 overload;
  end { TGSkThreadHlp };


implementation


{$REGION 'TGSkThreadHlp'}

function  TGSkThreadHlp.WaitFor() : LongWord;
begin
  Result := inherited WaitFor()
end { WaitFor };


function  TGSkThreadHlp.WaitFor(const   SynchObj:  THandleObject;
                                const   nTimeout:  LongWord)
                                : TWaitResult;
begin
  case  WaitForSingleObject(SynchObj.Handle, nTimeout) of
    WAIT_ABANDONED:
      Result := wrAbandoned;
    WAIT_OBJECT_0:
      Result := wrSignaled;
    WAIT_TIMEOUT:
      Result := wrTimeout;
    WAIT_FAILED:
      Result := wrError;
    else { WAIT_FAILED }
      Result := wrError
    end { case }
end { WaitFor };


function  TGSkThreadHlp.WaitFor(const   SynchObj:  array of TEvent;
                                out     nSynch:  Integer;
                                const   bWaitAll:  Boolean;
                                const   nTimeout:  LongWord)
                                : TWaitResult;
var
  pHandles:  PWOHandleArray;
  nSynchCount:  LongWord;
  nInd:  Integer;
  nRes:  DWORD;

begin  { WaitFor }
  { WAIT_OBJECT_0    =   0
    WAIT_ABANDONED_0 = 128
    MAXIMUM_WAIT_OBJECTS =  64
    WAIT_OBJECT_0 + MAXIMUM_WAIT_OBJECTS    = 128
    WAIT_ABANDONED_0 + MAXIMUM_WAIT_OBJECTS = 192 }
  nSynchCount := Length(SynchObj);
  if  (nSynchCount = 0)
      and (nSynchCount > MAXIMUM_WAIT_OBJECTS)  then
    raise  ERangeError.CreateFmt('Liczba obiektów synchronizacji (%d) musi być z przedziału 0..%d',
                                 [nSynchCount, MAXIMUM_WAIT_OBJECTS]);
  GetMem(pHandles, nSynchCount  * SizeOf(THandle));
  try
    for  nInd := High(SynchObj)  downto  Low(SynchObj)  do
      pHandles^[nInd] := SynchObj[nInd].Handle;
    nRes := WaitForMultipleObjects(Length(SynchObj), pHandles, bWaitAll, nTimeout);
    case  nRes  of
      WAIT_FAILED:
        Result := wrError;
      WAIT_TIMEOUT:
        Result := wrTimeout;
      else
        if  nRes in [WAIT_OBJECT_0..(WAIT_OBJECT_0 + nSynchCount - 1)]  then
          begin
            nSynch := nRes - WAIT_OBJECT_0;
            Result := wrSignaled
          end
        else if  nRes in [WAIT_ABANDONED_0..(WAIT_ABANDONED_0 + nSynchCount - 1)]  then
          begin
            nSynch := nRes - WAIT_ABANDONED_0;
            Result := wrAbandoned
          end
        else
          Result := wrError
    end { case nRes }
  finally
    FreeMem(pHandles)
  end { try-finally }
end { WaitFor };

{$ENDREGION 'TGSkThreadHlp'}


end.
