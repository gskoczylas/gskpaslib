﻿{: @exclude
   Wewnętrznych modułów biblioteki nie komentuję (na razie).

   @abstract Definiuje edytor komponentu TGSkDataEmbedded.  }
unit  GSkPasLib.CompEdDataEmbedded;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  SysUtils, Classes,
  GSkPasLib.CustomComponentEditors;


type
  TCompEdDataEmbedded = class(TGSkComponentEditor)
  public
    function  GetVerbCount() : Integer;                              override;
    function  GetVerb(Index:  Integer)
                      : string;                                      override;
    procedure  ExecuteVerb(Index:  Integer);                         override;
  end { TCompEdDataEmbedded };


implementation


uses
  GSkPasLib.DataEmbedded, GSkPasLib.PropEdDataEmbedded, GSkPasLib.PasLibReg;


{ TCompEdDataEmbedded }

procedure  TCompEdDataEmbedded.ExecuteVerb(Index:  Integer);

var
  lFile:  TFileStream;

begin  { ExecuteVerb }
  try
    if  Index <= gcnStdVerbMaxInd  then
      inherited ExecuteVerb(Index)
    else
      case  Index  of
        gcnStdVerbMaxInd + 1:
          begin
            lFile := GetData();
            if  lFile <> nil  then
              try
                (Component as TGSkDataEmbedded).Data := lFile;
                Designer.Modified()
              finally
                lFile.Free()
              end { try-finally }
          end { gcnStdVerbMaxInd + 1 }
      end { case Index }
  except
    on  eErr : Exception  do
      ShowDesingError(eErr)
  end { try-except }
end { ExecuteVerb };


function  TCompEdDataEmbedded.GetVerb(Index:  Integer)
                                      : string;
begin
  if  Index <= gcnStdVerbMaxInd  then
    Result := inherited GetVerb(Index)
  else
    case  Index  of
      gcnStdVerbMaxInd + 1:
        Result := csDataEmbedded + '...';
      else
        Result := Format('[%d]', [Index])
    end { case Index }
end { GetVerb };


function  TCompEdDataEmbedded.GetVerbCount() : Integer;
begin
  Result := inherited GetVerbCount() + 1
end { GetVerbCount };


end.
