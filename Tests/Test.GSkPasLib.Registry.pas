﻿unit  Test.GSkPasLib.Registry;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}


uses
  TestFrameWork, Registry;


type
  TRegistry_Test = class(TTestCase)
  published
    procedure  VerifyRead();
  end { TStrUtils_Test };


implementation


uses
  GSkPasLib.Classes;


{$REGION 'TRegistry_Test'}

procedure  TRegistry_Test.VerifyRead();

const
  csKey = 'Software\Grzegorz Skoczylas\GSkPasLib';

begin  { VerifyRead }
  with  TRegistry.Create()  do
    try
      if  OpenKey(csKey, True)  then
        try
          CheckEquals(5, ReadInteger('None', 5), 'Pobieranie nieistniejącego klucza');
          WriteInteger('Value', 7);
          CheckEquals(7, ReadInteger('Value', 0), 'Pobieranie istniejącego klucza');
          DeleteValue('Value');
          CheckEquals(0, ReadFloat('Missing'), 'Nieistniejący klucz, bez wartości domyślnej');
        finally
          CloseKey();
          DeleteKey(csKey)
        end { try-finally }
    finally
      Free()
    end { try-finally }
end { VerifyRead };

{$ENDREGION 'TRegistry_Test'}


initialization
  RegisterTest(TRegistry_Test.Suite())


end.

