﻿{: @abstract Współpraca z kolejką drukowania Windows. }
unit  GSkPasLib.PrintQueue;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


uses
  WinAPI.Windows,
  System.Win.Registry,
  System.SysUtils;


type
  {: Wyjątek przesyłania pliku do kolejki drukowania Windows.

     Taki wyjątek jest generowany, jeżeli nie ma możliwości przesłania pliku
     danego typu do kolejki drukowania Windows.

     @seeAlso(SendToPrintQueue) }
  EPrintQueue = class(Exception);


{: @abstract Przesyła wskazany plik do kolejki drukowania Windows.

   Procedura @name przesyła wskazany w parametrze plik do kolejki drukowania Windows.
   W tym celu wykonuje takie same działania, jakie są wykonywanwe wtedu, gdy
   w Eksploratorze plików klikniesz ikonę pliku prawym przyciskiem myszy
   i z wyświetlonego menu wybierzesz „Drukuj” lub „Print”. Do realizacji tej
   funkcji Windows wykorzystuje program zarejestrowany w Windows do przetwarzania
   plików danego typu.

   Na przykład w przypadku dokumentów PDF najczęściej używany jest program
   @italic(Adobe Acrobat Reader) lub @italic(Fixit Reader), itp.

   Jeżeli w Windows nie ma zarejestrowanego programu do przetwarzania plików
   danego typu, to w powyższym menu nie ma opcji „Drukuj” lub „Print”.

   Procedura @name działa tak samo, jak Windows. Jeżeli w Windows nie ma programu
   zarejestrowanego do drukowania plików danego typu, to procedura nie jest
   w stanie przesłać takiego pliku do kolejki drukowania. W takim przypadku
   procedura generuje wyjątek @link(EPrintQueue).

   @param (sFilePath Ścieżka dostępu do pliku, który powinien zostać wysłany
                     do kolejki drukowania Windows)

   @seeAlso(EPrintQueue) }
procedure  SendToPrintQueue(const   sFilePath:  string);             overload;

{: @abstract Przesyła wskazany plik do kolejki drukowania Windows.

   Procedura @name przesyła wskazany w parametrze plik do kolejki drukowania Windows.
   W tym celu wykonuje takie same działania, jakie są wykonywanwe wtedu, gdy
   w Eksploratorze plików klikniesz ikonę pliku prawym przyciskiem myszy
   i z wyświetlonego menu wybierzesz „Drukuj” lub „Print”. Do realizacji tej
   funkcji Windows wykorzystuje program zarejestrowany w Windows do przetwarzania
   plików danego typu.

   Na przykład w przypadku dokumentów PDF najczęściej używany jest program
   @italic(Adobe Acrobat Reader) lub @italic(Fixit Reader), itp.

   Jeżeli w Windows nie ma zarejestrowanego programu do przetwarzania plików
   danego typu, to w powyższym menu nie ma opcji „Drukuj” lub „Print”.

   Procedura @name działa tak samo, jak Windows. Jeżeli w Windows nie ma programu
   zarejestrowanego do drukowania plików danego typu, to procedura nie jest
   w stanie przesłać takiego pliku do kolejki drukowania. W takim przypadku
   procedura generuje wyjątek @link(EPrintQueue).

   @param(sFilePath Ścieżka dostępu do pliku, który powinien zostać wysłany
                    do kolejki drukowania Windows)
   @param(nExitCode Ten parametr zwraca kod zakończenia działania programu,
                    który został użyty do przetwarzania wskazanego pliku.)

   @seeAlso(EPrintQueue) }
procedure  SendToPrintQueue(const   sFilePath:  string;
                            out     nExitCode:  LongWord);           overload;


implementation


uses
  GSkPasLib.SystemUtils;


const
  csErrMsgFmt = 'There is no programme registered in Windows for the printing of files of the type "%s"';


{$REGION 'Local subroutines'}

function  MakeRegistryHKCR() : TRegistry;
begin
  Result := TRegistry.Create(KEY_READ);
  Result.RootKey:= HKEY_CLASSES_ROOT
end { MakeRegistryHKCR };


function  GetAppIdForFileExt(const   sFileExt:  string)
                             : string;
begin
  with   MakeRegistryHKCR()  do
    try
      if  OpenKey('\' + sFileExt, False)  then
        try
          Result := ReadString('');
          if  Result = ''  then
            raise  EPrintQueue.CreateFmt(csErrMsgFmt, [sFileExt])
        finally
          CloseKey()
        end { try-finally }
      else
        raise  EPrintQueue.CreateFmt(csErrMsgFmt, [sFileExt])
    finally
      Free()
    end { try-finally }
end { GetAppIdForFileExt };


function  GetPrintCommand(const   sAppId, sFileExt:  string)
                          : string;
begin  { GetPrintCommand }
  with  MakeRegistryHKCR()  do
    try
      if  OpenKey('\' + sAppId + '\shell\print\command', False)  then
        try
          Result := ReadString('');
          if  Result = ''  then
            raise  EPrintQueue.CreateFmt(csErrMsgFmt, [sFileExt])
        finally
          CloseKey()
        end { try-finally }
      else
        raise  EPrintQueue.CreateFmt(csErrMsgFmt, [sFileExt])
    finally
      Free()
    end { try-finally }
end { 'GetPrintCommand' };


procedure  SplitCommand(const   sCommand:  string;
                        out     sProgram:  string;
                        out     sParams:  string);
var
  nInd:  Integer;

begin  { SplitCommand }
  if  sCommand[1] = '"'  then
    begin
      nInd := Pos('"', sCommand, 2);
      sProgram := Copy(sCommand, 2, nInd - 2);
      sParams := Copy(sCommand, nInd + 1, MaxInt)
    end { "program" params }
  else
    begin
      nInd := Pos(' ', sCommand);
      if  nInd = 0  then
        nInd := Length(sCommand) + 1;
      sProgram := Copy(sCommand, nInd  - 1);
      sParams := Copy(sCommand, nInd + 1, MaxInt)
    end { program params };
  sParams := TrimLeft(sParams)
end { SplitCommand };


procedure  ExecCmd(const   sProgram, sParams:  string;
                   const   pnExitCode:  PLongWord);
var
  nResultCode:  LongWord;

begin  { ExecCmd }
  nResultCode := ShellExec(sProgram, sParams, '', '', SW_HIDE,
                           wmWaitForTerminate, pnExitCode);
  if  nResultCode <> NO_ERROR  then
    raise  EPrintQueue.CreateFmt('[%u] %s',
                                 [nResultCode, SysErrorMessage(nResultCode)])
end { ExecCmd };


procedure  ExecPrintCmd(const   sCommand:  string;
                        const   sFilePath:  string;
                        const   pnExitCode:  PLongWord);
var
  sProgram:  string;
  sParams:   string;

begin  { ExecPrintCmd }
  SplitCommand(sCommand, sProgram, sParams);
  ExecCmd(sProgram,
          StringReplace(sParams, '%1', sFilePath, [rfReplaceAll]),
          pnExitCode)
end { ExecPrintCmd };


procedure  SendToPrintQueue(const   sFilePath:  string;
                            const   pnExitCode:  PLongWord);         overload;
var
  sFileExt:  string;
  sAppID:    string;
  sPrintCmd: string;

begin  { SendToPrintQueue }
  if  not FileExists(sFilePath)  then
    raise  EPrintQueue.CreateFmt('File "%s" does not exist', [sFilePath]);
  sFileExt := ExtractFileExt(sFilePath);
  sAppID := GetAppIdForFileExt(sFileExt);
  sPrintCmd := GetPrintCommand(sAppID, sFileExt);
  ExecPrintCmd(sPrintCmd, sFilePath, pnExitCode)
end { SendToPrintQueue };


{$ENDREGION 'Local subroutines'}


{$REGION 'Global subroutines'}

procedure  SendToPrintQueue(const   sFilePath:  string);
begin
  SendToPrintQueue(sFilePath, nil)
end { SendToPrintQueue };


procedure  SendToPrintQueue(const   sFilePath:  string;
                            out     nExitCode:  LongWord);
begin
  SendToPrintQueue(sFilePath, Addr(nExitCode))
end { SendToPrintQueue };


{$ENDREGION 'Global subroutines'}


end.
