﻿unit  Test.GSkPasLib.TimeUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}


uses
  TestFrameWork, System.SysUtils, System.DateUtils, GSkPasLib.TimeUtils;


type
  TTimeUtils_Test = class(TTestCase)
  published
    procedure  VerifyTickBetween();
    procedure  VerifyTickSpan();
    procedure  VerifyTicksToStr();
  end { TStrUtils_Test };


implementation


{ TTimeUtils_Test }

procedure  TTimeUtils_Test.VerifyTickBetween();
begin
  CheckEquals(TickBetween(200, 300),
              100,
              'TickBetween(200, 300) = 100');
  CheckEquals(TickBetween(300, 200),
              $ffffffff - 300 + 200,
              'TickBetween(300, 200) = 4294967195 {$ffffffff - 300 + 200}')
end { VerifyTickBetween };


procedure  TTimeUtils_Test.VerifyTickSpan();
begin
  CheckEquals(TickSpan(200, 300), 100, 'TickSpan(200, 300) = 100');
  CheckEquals(TickSpan(300, 200), 100, 'TickSpan(300, 200) = 100')
end { VerifyTickSpan };


procedure  TTimeUtils_Test.VerifyTicksToStr();

  function  TimeToMS(const   nHr, nMn, nSc, nMS:  Word)
                     : UInt64;
  begin
    Result := TimeToMilliseconds(EncodeDateTime(2000, 1, 1, nHr, nMn, nSc, nMS))
  end { TimeToMS };

begin  { VerifyTicksToStr }
  FormatSettings.DecimalSeparator := '.';
  CheckEquals('24:00:00', TicksToStr(MillisPerDay, True));
  CheckEquals('24:00:00', TicksToStr(MillisPerDay, False));
  CheckEquals('1:00:00', TicksToStr(MillisPerHour, True));
  CheckEquals('1:00:00', TicksToStr(MillisPerHour, False));
  CheckEquals('1 m 0 s', TicksToStr(MillisPerMinute, True));
  CheckEquals('1 m 0 s', TicksToStr(MillisPerMinute, False));
  CheckEquals('1 s', TicksToStr(MillisPerSecond, True));
  CheckEquals('1 s', TicksToStr(MillisPerSecond, False));
  {-}
  CheckEquals('1:00:00.123', TicksToStr(MillisPerHour + 123, True));
  CheckEquals('1:00:00', TicksToStr(MillisPerHour + 123, False));
  CheckEquals('1 m 0.123 s', TicksToStr(MillisPerMinute + 123, True));
  CheckEquals('1 m 0 s', TicksToStr(MillisPerMinute + 123, False));
  CheckEquals('1.123 s', TicksToStr(MillisPerSecond + 123, True));
  CheckEquals('1 s', TicksToStr(MillisPerSecond + 123, False));
  {-}
  CheckEquals('1:00:00.567', TicksToStr(MillisPerHour + 567, True));
  CheckEquals('1:00:01', TicksToStr(MillisPerHour + 567, False));
  CheckEquals('1 m 0.567 s', TicksToStr(MillisPerMinute + 567, True));
  CheckEquals('1 m 1 s', TicksToStr(MillisPerMinute + 567, False));
  CheckEquals('1.567 s', TicksToStr(MillisPerSecond + 567, True));
  CheckEquals('2 s', TicksToStr(MillisPerSecond + 567, False));
  {-}
  CheckEquals('1:11:00', TicksToStr(TimeToMS(1, 10, 59, 567), False));
  CheckEquals('2:00:00', TicksToStr(TimeToMS(1, 59, 59, 567), False));
  {-}
  CheckEquals('30 s', TicksToStr(30000, False));
  CheckEquals('21 s', TicksToStr(20578, False));
  CheckEquals('20 s', TicksToStr(19578, False));
  CheckEquals('19 s', TicksToStr(18578, False));
end { VerifyTicksToStr };


initialization
  RegisterTest(TTimeUtils_Test.Suite())


end.

