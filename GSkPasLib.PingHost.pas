﻿unit  GSkPasLib.PingHost;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     —————————————————————————————————      *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 **********************************************)

{$I Conditionals.inc }

interface


const
  gcnDefaultPingTimeout = 4000;   // 4000 ms = 4 s


type
  IGSkPingInterface = interface
  {private}
    function  GetErrorMessage() : string;
    function  GetErrorCode() : Integer;
  {public}
    function  PingHost(const   sHost:  string;
                       const   nTimeoutMS:  Cardinal = gcnDefaultPingTimeout)
                       : Boolean;
    {-}
    property  ErrorMessage:  string
              read  GetErrorMessage;
    property  ErrorCode:  Integer
              read  GetErrorCode;
  end { IGSkPingInterface };


implementation


end.

