﻿unit  Test.GSkPasLib.ComponentUtils;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


uses
  {$IF CompilerVersion > 22} // XE
    System.UITypes,
  {$IFEND}
  TestFrameWork, GSkPasLib.ComponentUtils,
  Graphics, Windows;


{$TYPEINFO ON}


type
  TConvert_Test = class(TTestCase)
  published
    procedure  VerifyFontConv();
  end { TConvert_Test };



implementation


uses
  GSkPasLib.Dialogs;


{ TConvert_Test }

procedure  TConvert_Test.VerifyFontConv();

var
  sBuf:  string;
  lFont1, lFont2:  TFont;

begin  { VerifyFontConv }
  lFont1 := TFont.Create();
  lFont2 := TFont.Create();
  try
    lFont1.Charset := EASTEUROPE_CHARSET;
    lFont1.Color := clBlue;
    lFont1.Name := 'Arial';
    lFont1.Size := 11;
    lFont1.Style := [fsBold, fsItalic, fsUnderline, fsStrikeOut];
    sBuf := FontToString(lFont1);
    MsgDlg(sBuf, mtInformation, [mbOk], nil, mbDefault, mbDefault, 3, 0);
    StringToFont(sBuf, lFont2);
    Check(lFont1.Charset = lFont2.Charset, 'Font.Charset');
    Check(lFont1.Color = lFont2.Color, 'Font.Color');
    Check(lFont1.Name = lFont2.Name, 'Font.Name');
    Check(lFont1.Size = lFont2.Size, 'Font.Size');
    Check(lFont1.Style = lFont2.Style, 'Font.Style');
  finally
    lFont1.Free();
    lFont2.Free()
  end { try-finally }
end { VerifyFontConv };


initialization
  RegisterTest(TConvert_Test.Suite())


end.
