﻿unit  Test.GSkPasLib.Tokens;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}


uses
  SysUtils, StrUtils, Math,
  GSkPasLib.Tokens, TestFrameWork;


type
  TTokens_Test = class(TTestCase)
  published
    procedure  VerifyChrTokens();
    procedure  VerifyStrTokens();
    procedure  VerifyChrNoTextTokens();
    procedure  VerifyStrNoTextTokens();
    procedure  VerifyChrNoDelimTokens();
    procedure  VerifyStrNoDelimTokens();
    procedure  VerifyChrEmptyTokens();
    procedure  VerifyStrEmptyTokens();
  end { TControlUtils_Test };


implementation


{ TTokens_Test }


procedure  TTokens_Test.VerifyChrNoTextTokens();

var
  nInd:  Integer;
  sBuf:  string;
  nBuf:  Integer;

{$Hints off}
begin  { VerifyChrNoTextTokens }
  with  TGSkCharTokens.Create(nil)  do
    try
      Text := '';
      SetDelimiters(['*']);
      for  nInd := 0  to  Count - 1  do
        begin
          sBuf := Token[nInd];
          nBuf := TokenPos[nInd];
          sBuf := TokenSep[nInd]
        end
    finally
      Free
    end { try-finally }
end { VerifyChrNoTextTokens };
{$Hints on}


procedure  TTokens_Test.VerifyChrTokens();

const
  ccTokenSep1 = '|';
  ccTokenSep2 = '+';
  csToken0   = 'Pierwszy';
  csToken1   = 'Drugi';
  csToken2   = '';         // pusty
  csToken3   = 'Czwarty';

var
  nInd:  Integer;

begin  { VerifyChrTokens }
  with  TGSkCharTokens.Create(nil)  do
    try
      Text := csToken0
                + ccTokenSep1 + csToken1
                + ccTokenSep2 + csToken2
                + cctokenSep1 + csToken3;  // 1|2+|4
      SetDelimiters([ccTokenSep1, ccTokenSep2]);
      AllowEmptyTokens := True;
      Check(Count = 4);
      Check(Token[0] = csToken0);
      Check(Token[1] = csToken1);
      Check(Token[2] = csToken2);
      Check(Token[3] = csToken3);
      Check(TokenSep[0] = ccTokenSep1);
      Check(TokenSep[1] = ccTokenSep2);
      Check(TokenSep[2] = ccTokenSep1);
      Check(TokenSep[3] = #0);
      AllowEmptyTokens := False;
      Assert(csToken2 = '');
      Check(Count = 3);
      Check(Token[0] = csToken0);
      Check(Token[1] = csToken1);
      Check(Token[2] = csToken3);
      Check(TokenSep[0] = ccTokenSep1);
      Check(TokenSep[1] = ccTokenSep2);
      Check(TokenSep[2] = #0);
      Text := StringOfChar(ccTokenSep1, 3);
      SetDelimiter(ccTokenSep1);
      AllowEmptyTokens := True;
      Check(Count = 4);
      for  nInd := 0  to  3  do
        Check(Token[nInd] = '');
      Check(TokenSep[0] = ccTokenSep1);
      Check(TokenSep[1] = ccTokenSep1);
      Check(TokenSep[2] = ccTokenSep1);
      Check(TokenSep[3] = #0);
      AllowEmptyTokens := False;
      Check(Count = 0)
    finally
      Free()
    end { try-finally }
end { VerifyChrTokens };


procedure  TTokens_Test.VerifyStrTokens();

const
  csTokenSep1 = '|-|';
  csTokenSep2 = '|-+-|';
  csToken0   = 'Pierwszy';
  csToken1   = 'Drugi';
  csToken2   = '';         // pusty
  csToken3   = 'Czwarty';

var
  nInd:  Integer;

begin  { VerifyStringTokens }
  with  TGSkStringTokens.Create(nil)  do
    try
      Text := csToken0
                + csTokenSep1 + csToken1
                + csTokenSep2 + csToken2
                + cstokenSep1 + csToken3;  // 1|2+|4
      SetDelimiters([csTokenSep1, csTokenSep2]);
      AllowEmptyTokens := True;
      Check(Count = 4);
      Check(Token[0] = csToken0);
      Check(Token[1] = csToken1);
      Check(Token[2] = csToken2);
      Check(Token[3] = csToken3);
      Check(TokenSep[0] = csTokenSep1);
      Check(TokenSep[1] = csTokenSep2);
      Check(TokenSep[2] = csTokenSep1);
      Check(TokenSep[3] = '');
      AllowEmptyTokens := False;
      Assert(csToken2 = '');
      Check(Count = 3);
      Check(Token[0] = csToken0);
      Check(Token[1] = csToken1);
      Check(Token[2] = csToken3);
      Check(TokenSep[0] = csTokenSep1);
      Check(TokenSep[1] = csTokenSep2);
      Check(TokenSep[2] = '');
      Text := DupeString(csTokenSep1, 3);
      SetDelimiter(csTokenSep1);
      AllowEmptyTokens := True;
      Check(Count = 4);
      for  nInd := 0  to  3  do
        Check(Token[nInd] = '');
      Check(TokenSep[0] = csTokenSep1);
      Check(TokenSep[1] = csTokenSep1);
      Check(TokenSep[2] = csTokenSep1);
      AllowEmptyTokens := False;
      Check(Count = 0)
    finally
      Free()
    end { try-finally }
end { VerifyStrTokens };


procedure  TTokens_Test.VerifyStrNoTextTokens();

var
  nInd:  Integer;
  sBuf:  string;
  nBuf:  Integer;

{$Hints off }
begin  { VerifyStrNoTextTokens }
  with  TGSkStringTokens.Create(nil)  do
    try
      Text := '';
      SetDelimiter('*');
      for  nInd := 0  to  Count - 1  do
        begin
          sBuf := Token[nInd];
          nBuf := TokenPos[nInd];
          sBuf := TokenSep[nInd]
        end
    finally
      Free
    end { try-finally }
end { VerifyStrNoTextTokens };
{$Hints on }


procedure  TTokens_Test.VerifyChrEmptyTokens();

const
  cchSep = #9;   // TAB
  csSeg1 = 'First Segment';
  csSeg2 = 'Second Segment';

var
  lSeg:  TGSkCharTokens;

  procedure  CheckRes(const   nSegments:  Integer;
                      const   Segments:  array of string);
  var
    nInd:  Integer;

  begin  { CheckRes }
    Check(nSegments = lSeg.Count,
          Format('Expected %d segment(s) but found %d segment(s)',
                 [nSegments, lSeg.Count]));
    for  nInd := Low(Segments)  to  Min(High(Segments), Pred(lSeg.Count))  do
      Check(Segments[nInd] = lSeg.Token[nInd],
            Format('Token %d: expected "%s" but found "%s"',
                   [nInd, Segments[nInd], lSeg.Token[nInd]]))
  end { CheckRes };

begin  { VerifyChrEmptyTokens }
  lSeg := TGSkCharTokens.Create(nil);
  with  lSeg  do
    try
      SetDelimiter(cchSep);
      // first segment is empty
      Text := cchSep + csSeg1 + cchSep + csSeg2;
      AllowEmptyTokens := False;
      CheckRes(2, [csSeg1, csSeg2]);
      AllowEmptyTokens := True;
      CheckRes(3, ['', csSeg1, csSeg2]);
      // middle segment is empty
      Text := csSeg1 + cchSep + cchSep + csSeg2;
      AllowEmptyTokens := False;
      CheckRes(2, [csSeg1, csSeg2]);
      AllowEmptyTokens := True;
      CheckRes(3, [csSeg1, '', csSeg2]);
      // last segment is empty
      Text := csSeg1 + cchSep + csSeg2 + cchSep;
      AllowEmptyTokens := False;
      CheckRes(2, [csSeg1, csSeg2]);
      AllowEmptyTokens := True;
      CheckRes(3, [csSeg1, csSeg2, '']);
    finally
      Free()
    end { try-finally };
  with  TGSkCharTokens.Create(nil)  do
    try
      SetDelimiter(#9);
      AllowEmptyTokens := True;
      Text := 'ASINFOR.DIRECTIO:5'#9'TIR GROUPE (BON CADEAU DIRECT'#9;
      CheckEquals(Count, 3);
      CheckEquals(Token[0], 'ASINFOR.DIRECTIO:5');
      CheckEquals(Token[1], 'TIR GROUPE (BON CADEAU DIRECT');
      CheckEquals(Token[2], '')
    finally
      Free()
    end { try-finally }
end { VerifyChrEmptyTokens };


procedure  TTokens_Test.VerifyChrNoDelimTokens();

var
  nInd:  Integer;
  sBuf:  string;
  nBuf:  Integer;

{$Hints off}
begin  { VerifyChrNoDelimTokens }
  with  TGSkCharTokens.Create(nil)  do
    try
      Text := 'To jest tekst';
      SetDelimiters([]);
      for  nInd := 0  to  Count - 1  do
        begin
          sBuf := Token[nInd];
          nBuf := TokenPos[nInd];
          sBuf := TokenSep[nInd]
        end
    finally
      Free
    end { try-finally }
end { VerifyChrNoDelimTokens };
{$Hints on}


procedure  TTokens_Test.VerifyStrEmptyTokens();

const
  csSep  = #13#10;   // CR LF
  csSeg1 = 'First Segment';
  csSeg2 = 'Second Segment';

var
  lSeg:  TGSkStringTokens;

  procedure  CheckRes(const   nSegments:  Integer;
                      const   Segments:  array of string);
  var
    nInd:  Integer;

  begin  { CheckRes }
    Check(nSegments = lSeg.Count,
          Format('Expected %d segment(s) but found %d segment(s)',
                 [nSegments, lSeg.Count]));
    for  nInd := Low(Segments)  to  Min(High(Segments), Pred(lSeg.Count))  do
      Check(Segments[nInd] = lSeg.Token[nInd],
            Format('Token %d: expected "%s" but found "%s"',
                   [nInd, Segments[nInd], lSeg.Token[nInd]]))
  end { CheckRes };

begin  { VerifyStrEmptyTokens }
  lSeg := TGSkStringTokens.Create(nil);
  with  lSeg  do
    try
      SetDelimiter(csSep);
      // first segment is empty
      Text := csSep + csSeg1 + csSep + csSeg2;
      AllowEmptyTokens := False;
      CheckRes(2, [csSeg1, csSeg2]);
      AllowEmptyTokens := True;
      CheckRes(3, ['', csSeg1, csSeg2]);
      // middle segment is empty
      Text := csSeg1 + csSep + csSep + csSeg2;
      AllowEmptyTokens := False;
      CheckRes(2, [csSeg1, csSeg2]);
      AllowEmptyTokens := True;
      CheckRes(3, [csSeg1, '', csSeg2]);
      // last segment is empty
      Text := csSeg1 + csSep + csSeg2 + csSep;
      AllowEmptyTokens := False;
      CheckRes(2, [csSeg1, csSeg2]);
      AllowEmptyTokens := True;
      CheckRes(3, [csSeg1, csSeg2, '']);
    finally
      Free()
    end { try-finally };
  with  TGSkStringTokens.Create(nil)  do
    try
      SetDelimiter(#9);
      AllowEmptyTokens := True;
      Text := 'ASINFOR.DIRECTIO:5'#9'TIR GROUPE (BON CADEAU DIRECT'#9;
      CheckEquals(Count, 3);
      CheckEquals(Token[0], 'ASINFOR.DIRECTIO:5');
      CheckEquals(Token[1], 'TIR GROUPE (BON CADEAU DIRECT');
      CheckEquals(Token[2], '')
    finally
      Free()
    end { try-finally }
end { VerifyStrEmptyTokens };


procedure  TTokens_Test.VerifyStrNoDelimTokens();

var
  nInd:  Integer;
  sBuf:  string;
  nBuf:  Integer;

{$Hints off }
begin  { VerifyStrNoDelimTokens }
  with  TGSkStringTokens.Create(nil)  do
    try
      Text := 'To jest tekst';
      SetDelimiters([]);
      for  nInd := 0  to  Count - 1  do
        begin
          sBuf := Token[nInd];
          nBuf := TokenPos[nInd];
          sBuf := TokenSep[nInd]
        end
    finally
      Free
    end { try-finally }
end { VerifyStrNoDelimTokens };
{$Hints on }


initialization
  RegisterTest(TTokens_Test.Suite())


end.
