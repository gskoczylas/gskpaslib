﻿unit  Test.GSkPasLib.Classes;

(**********************************************
 *                                            *
 *              G S k P a s L i b             *
 *     ——————————————————————————————————     *
 *  Copyright © 1998-2025 Grzegorz Skoczylas  *
 *                                            *
 *        mgr Grzegorz Skoczylas              *
 *            gskoczylas+GSkPasLib@gmail.com  *
 *                                            *
 *                +------------+              *
 *                | DUNIT Test |              *
 *                ~~~~~~~~~~~~~~              *
 *                                            *
 **********************************************)

{$I Conditionals.inc }


interface


{$TYPEINFO ON}


uses
  TestFramework, Classes, SysUtils, Registry,
  GSkPasLib.Classes;


type
  TStreamEx_Test = class(TTestCase)
  strict private
    var
      fStream:  TStringStream;
  public
    procedure  SetUp();                                              override;
    procedure  TearDown();                                           override;
  published
    procedure  TestStreamEx();
  end { TStreamEx_Test };
  
  TRegistry_Test = class(TTestCase)
  published
    procedure  VerifyRead();
  end { TRegistry_Test };


implementation


{$REGION 'TStreamEx_Test'}

procedure  TStreamEx_Test.SetUp();
begin
  inherited;
  fStream := TStringStream.Create('');
end { SetUp };


procedure  TStreamEx_Test.TearDown();
begin
  inherited;
  fStream.Free()
end { TearDown };


procedure  TStreamEx_Test.TestStreamEx();

const
  csTestStr:  string = 'To jest test';

var
  pszTestStr:  PChar;
  sBuf:  string;

begin  { TestStreamEx }
  CheckTrue(fStream.Eof);
  fStream.WriteDataChar('G');
  CheckTrue(fStream.Eof);
  fStream.Seek(0, soBeginning);
  CheckFalse(fStream.Eof);
  CheckEquals('G', fStream.ReadDataChar());
  fStream.Size := 0;
  CheckTrue(fStream.Eof);
  fStream.WriteDataString(csTestStr);
  fStream.Seek(0, soBeginning);
  sBuf := fStream.ReadDataString(Length(csTestStr));
  CheckEquals(csTestStr, sBuf);
  fStream.Size := 0;
  pszTestStr := PChar(csTestStr);
  fStream.WriteDataString(pszTestStr);
  fStream.Seek(0, soBeginning);
  sBuf := fStream.ReadDataString(Length(csTestStr));
  CheckEquals(csTestStr, sBuf)
end { TestStreamEx };

{$ENDREGION 'TStreamEx_Test'}


{$REGION 'TRegistry_Test'}

procedure  TRegistry_Test.VerifyRead();

const
  csKey = 'Software\Grzegorz Skoczylas\GSkPasLib';

begin  { VerifyRead }
  with  TRegistry.Create()  do
    try
      if  OpenKey(csKey, True)  then
        try
          CheckEquals(5, ReadInteger('None', 5), 'Pobieranie nieistniejącego klucza');
          WriteInteger('Value', 7);
          CheckEquals(7, ReadInteger('Value', 0), 'Pobieranie istniejącego klucza');
          DeleteValue('Value');
          CheckEquals(0, ReadFloat('Missing'), 'Nieistniejący klucz, bez wartości domyślnej');
        finally
          CloseKey();
          DeleteKey(csKey)
        end { try-finally }
    finally
      Free()
    end { try-finally }
end { VerifyRead };

{$ENDREGION 'TRegistry_Test'}


initialization
  RegisterTest(TStreamEx_Test.Suite());
  RegisterTest(TRegistry_Test.Suite())

  
end.
